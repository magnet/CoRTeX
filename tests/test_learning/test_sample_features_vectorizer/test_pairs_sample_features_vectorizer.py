# -*- coding: utf-8 -*-

import unittest

from cortex.learning.sample_features_vectorizer.pairs import (MentionPairSampleFeaturesVectorizer, 
                                                                ExtendedMentionPairSampleFeaturesVectorizer, 
                                                                SuperExtendedMentionPairSampleFeaturesVectorizer)
from cortex.learning.ml_features.hierarchy.pairs import PairGramTypeHierarchy#, OverlapPairGramTypeHierarchy
from cortex.learning.ml_features.hierarchy.mentions import MentionGramTypeHierarchy, MentionNoneHierarchy
from cortex.tools import Mapping
from cortex.learning.ml_features.mention_pair_sample_features_vectorizer import _create_mention_pair_sample_feature_vectorizer
from cortex.learning.ml_features.extended_mention_pair_sample_features_vectorizer import _create_extended_mention_pair_sample_feature_vectorizer
from cortex.learning.ml_features.super_extended_mention_pair_sample_features_vectorizer import _create_super_extended_mention_pair_sample_feature_vectorizer

create_mention_pair_sample_feature_vectorizer_fct = _create_mention_pair_sample_feature_vectorizer
create_extended_mention_pair_sample_feature_vectorizer_fct = _create_extended_mention_pair_sample_feature_vectorizer
create_super_extended_mention_pair_sample_feature_vectorizer_fct = _create_super_extended_mention_pair_sample_feature_vectorizer

#@unittest.skip
class TestMentionPairSampleFeaturesVectorizer(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        (args, kwargs), attribute_name_expected_result_pairs = get_mention_pair_sample_features_vectorizer_test___init___data()
        
        # Test
        sample_features_vectorizer = MentionPairSampleFeaturesVectorizer(*args, **kwargs)
        for attribute_name, expected_result in attribute_name_expected_result_pairs:
            actual_result = getattr(sample_features_vectorizer, attribute_name)
            self.assertEqual(expected_result, actual_result, attribute_name)
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        data = get_mention_pair_sample_features_vectorizer_test__inner_eq_data()
        
        # Test
        for sample_features_vectorizer1, sample_features_vectorizer2, expected_result in data:
            self.assertEqual(expected_result, sample_features_vectorizer1._inner_eq(sample_features_vectorizer2))
    
    #@unittest.skip
    def test_from_configuration(self):
        #Test parameters
        configuration, expected_result = get_mention_pair_sample_features_vectorizer_test_from_configuration_data()
        
        # Test
        actual_result = MentionPairSampleFeaturesVectorizer.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        pair_hierarchy_factory_configuration = PairGramTypeHierarchy.define_factory_configuration() #Overlap
        with_singleton_features = True
        with_anaphoricity_features = False
        quantize = True
        args = tuple()
        kwargs = {"pair_hierarchy_factory_configuration": pair_hierarchy_factory_configuration, 
                  "with_singleton_features": with_singleton_features,
                  "with_anaphoricity_features": with_anaphoricity_features,
                  "quantize": quantize, 
                  }
        expected_result = Mapping({"pair_hierarchy": pair_hierarchy_factory_configuration,
                                   "with_singleton_features": with_singleton_features,
                                   "with_anaphoricity_features": with_anaphoricity_features,
                                   "quantize": quantize,
                                   })
        
        # Test
        actual_result = MentionPairSampleFeaturesVectorizer.define_configuration(*args, **kwargs)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip
    def test_vectorize(self):
        # Test parameters
        _, sample_features_vectorizer = get_mention_pair_sample_features_vectorizer_test_from_configuration_data() #configuration
        
        # Test
        _ = sample_features_vectorizer.vectorize #encode_method



#@unittest.skip
class TestExtendedMentionPairSampleFeaturesVectorizer(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        (args, kwargs), attribute_name_expected_result_pairs = get_extended_mention_pair_sample_features_vectorizer_test___init___data()
        
        # Test
        sample_features_vectorizer = ExtendedMentionPairSampleFeaturesVectorizer(*args, **kwargs)
        for attribute_name, expected_result in attribute_name_expected_result_pairs:
            actual_result = getattr(sample_features_vectorizer, attribute_name)
            self.assertEqual(expected_result, actual_result, attribute_name)
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        data = get_extended_mention_pair_sample_features_vectorizer_test__inner_eq_data()
        
        # Test
        for sample_features_vectorizer1, sample_features_vectorizer2, expected_result in data:
            self.assertEqual(expected_result, sample_features_vectorizer1._inner_eq(sample_features_vectorizer2))
    
    #@unittest.skip
    def test_from_configuration(self):
        #Test parameters
        configuration, expected_result = get_extended_mention_pair_sample_features_vectorizer_test_from_configuration_data()
        
        # Test
        actual_result = ExtendedMentionPairSampleFeaturesVectorizer.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = ExtendedMentionPairSampleFeaturesVectorizer
        pair_hierarchy_factory_configuration = PairGramTypeHierarchy.define_factory_configuration() #Overlap
        mention_hierarchy_factory_configuration = MentionNoneHierarchy.define_factory_configuration()
        with_singleton_features = True
        with_anaphoricity_features = False
        quantize = True
        args = tuple()
        kwargs = {"pair_hierarchy_factory_configuration": pair_hierarchy_factory_configuration, 
                  "mention_hierarchy_factory_configuration": mention_hierarchy_factory_configuration, 
                  "with_singleton_features": with_singleton_features,
                  "with_anaphoricity_features": with_anaphoricity_features,
                  "quantize": quantize, 
                  }
        expected_result = Mapping({"pair_hierarchy": pair_hierarchy_factory_configuration,
                                   "mention_hierarchy": mention_hierarchy_factory_configuration, 
                                   "with_singleton_features": with_singleton_features,
                                   "with_anaphoricity_features": with_anaphoricity_features,
                                   "quantize": quantize,
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip
    def test_vectorize(self):
        # Test parameters
        _, sample_features_vectorizer = get_extended_mention_pair_sample_features_vectorizer_test_from_configuration_data() #configuration
        
        # Test
        _ = sample_features_vectorizer.vectorize #vectorize_method



#@unittest.skip
class TestSuperExtendedMentionPairSampleFeaturesVectorizer(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        (args, kwargs), attribute_name_expected_result_pairs = get_super_extended_mention_pair_sample_features_vectorizer_test___init___data()
        
        # Test
        sample_features_vectorizer = SuperExtendedMentionPairSampleFeaturesVectorizer(*args, **kwargs)
        for attribute_name, expected_result in attribute_name_expected_result_pairs:
            actual_result = getattr(sample_features_vectorizer, attribute_name)
            #print(attribute_name)
            if attribute_name == "_features_vectorizer":
                self.assertEqual(expected_result._numeric_column_names, actual_result.numeric_column_names)
                self.assertTrue(*expected_result._inner_eq(actual_result))
            self.assertEqual(expected_result, actual_result, attribute_name)
    



def get_mention_pair_sample_features_vectorizer_test___init___data():
    attribute_name_expected_result_pairs = []
    
    pair_hierarchy = PairGramTypeHierarchy()
    attribute_name_expected_result_pairs.append(("pair_hierarchy", pair_hierarchy))
    
    with_singleton_features = False
    attribute_name_expected_result_pairs.append(("with_singleton_features", with_singleton_features))
    
    with_anaphoricity_features = True
    attribute_name_expected_result_pairs.append(("with_anaphoricity_features", with_anaphoricity_features))
    
    quantize = True
    attribute_name_expected_result_pairs.append(("quantize", quantize))
    
    #strict = True
    #attribute_name_expected_result_pairs.append(("strict", strict))
    
    configuration = Mapping({"useless_field": "useless_value"})
    expected_configuration = Mapping({"pair_hierarchy": Mapping({"name": pair_hierarchy.NAME, "config": pair_hierarchy.configuration}), 
                                    "with_singleton_features": with_singleton_features, 
                                    "with_anaphoricity_features": with_anaphoricity_features, 
                                    "quantize": quantize, 
                                    #"strict": strict, 
                                    "useless_field": "useless_value"})
    attribute_name_expected_result_pairs.append(("configuration", expected_configuration))
    
    expected__features_vectorizer = create_mention_pair_sample_feature_vectorizer_fct(pair_hierarchy, 
                                                                                with_singleton_features, 
                                                                                with_anaphoricity_features, 
                                                                                quantize,
                                                                                #strict
                                                                                True
                                                                                )
    attribute_name_expected_result_pairs.append(("_features_vectorizer", expected__features_vectorizer))
    
    args = tuple()
    kwargs = {"pair_hierarchy": pair_hierarchy, "with_singleton_features": with_singleton_features, 
              "with_anaphoricity_features": with_anaphoricity_features, "quantize": quantize, 
              #"strict": strict, 
              "configuration": configuration}
    
    return (args, kwargs), attribute_name_expected_result_pairs

def get_mention_pair_sample_features_vectorizer_test__inner_eq_data():
    data = []
    
    (args, kwargs), _ = get_mention_pair_sample_features_vectorizer_test___init___data() #attribute_name_expected_result_pairs
    
    sample_features_vectorizer1 = MentionPairSampleFeaturesVectorizer(*args, **kwargs)
    sample_features_vectorizer2 = MentionPairSampleFeaturesVectorizer(*args, **kwargs)
    expected_result = (True, None)
    data.append((sample_features_vectorizer1, sample_features_vectorizer2, expected_result))
    
    kwargs["with_anaphoricity_features"] = False
    sample_features_vectorizer3 = MentionPairSampleFeaturesVectorizer(*args, **kwargs)
    expected_result = (False, "with_anaphoricity_features")
    data.append((sample_features_vectorizer1, sample_features_vectorizer3, expected_result))
    
    return data

def get_mention_pair_sample_features_vectorizer_test_from_configuration_data():
    attribute_name_expected_result_pairs = []
    
    pair_hierarchy = PairGramTypeHierarchy()
    attribute_name_expected_result_pairs.append(("pair_hierarchy", pair_hierarchy))
    
    with_singleton_features = False
    attribute_name_expected_result_pairs.append(("with_singleton_features", with_singleton_features))
    
    with_anaphoricity_features = True
    attribute_name_expected_result_pairs.append(("with_anaphoricity_features", with_anaphoricity_features))
    
    quantize = True
    attribute_name_expected_result_pairs.append(("quantize", quantize))
    
    #strict = True
    #attribute_name_expected_result_pairs.append(("strict", strict))
    
    configuration = Mapping({"useless_field": "useless_value"})
    expected_configuration = Mapping({"pair_hierarchy": Mapping({"name": pair_hierarchy.NAME, "config": pair_hierarchy.configuration}), 
                                    "with_singleton_features": with_singleton_features, 
                                    "with_anaphoricity_features": with_anaphoricity_features, 
                                    "quantize": quantize, 
                                    #"strict": strict, 
                                    "useless_field": "useless_value"})
    attribute_name_expected_result_pairs.append(("configuration", expected_configuration))
    
    expected__features_vectorizer = create_mention_pair_sample_feature_vectorizer_fct(pair_hierarchy, 
                                                                                with_singleton_features, 
                                                                                with_anaphoricity_features, 
                                                                                quantize, 
                                                                                #strict
                                                                                True
                                                                                )
    attribute_name_expected_result_pairs.append(("_features_vectorizer", expected__features_vectorizer))
    
    args = tuple()
    kwargs = {"pair_hierarchy": pair_hierarchy, "with_singleton_features": with_singleton_features, 
              "with_anaphoricity_features": with_anaphoricity_features, "quantize": quantize, 
              #"strict": strict, 
              "configuration": configuration}
    expected_result = MentionPairSampleFeaturesVectorizer(*args, **kwargs)
    
    configuration = Mapping({"useless_field": "useless_value",
                           "pair_hierarchy": Mapping({"name": pair_hierarchy.NAME, "config": pair_hierarchy.configuration}), 
                            "with_singleton_features": with_singleton_features, 
                            "with_anaphoricity_features": with_anaphoricity_features, 
                            "quantize": quantize, 
                            #"strict": strict
                            })
    
    return configuration, expected_result



def get_extended_mention_pair_sample_features_vectorizer_test___init___data():
    attribute_name_expected_result_pairs = []
    
    pair_hierarchy = PairGramTypeHierarchy()
    attribute_name_expected_result_pairs.append(("pair_hierarchy", pair_hierarchy))
    
    mention_hierarchy = MentionGramTypeHierarchy()
    attribute_name_expected_result_pairs.append(("mention_hierarchy", mention_hierarchy))
    
    with_singleton_features = False
    attribute_name_expected_result_pairs.append(("with_singleton_features", with_singleton_features))
    
    with_anaphoricity_features = True
    attribute_name_expected_result_pairs.append(("with_anaphoricity_features", with_anaphoricity_features))
    
    quantize = True
    attribute_name_expected_result_pairs.append(("quantize", quantize))
    
    #strict = True
    #attribute_name_expected_result_pairs.append(("strict", strict))
    
    configuration = Mapping({"useless_field": "useless_value"})
    expected_configuration = Mapping({"pair_hierarchy": Mapping({"name": pair_hierarchy.NAME, "config": pair_hierarchy.configuration}), 
                                      "mention_hierarchy": Mapping({"name": mention_hierarchy.NAME, "config": mention_hierarchy.configuration}), 
                                      "with_singleton_features": with_singleton_features, 
                                      "with_anaphoricity_features": with_anaphoricity_features, 
                                      "quantize": quantize, 
                                      #"strict": strict, 
                                      "useless_field": "useless_value"})
    attribute_name_expected_result_pairs.append(("configuration", expected_configuration))
    
    expected__features_vectorizer = create_extended_mention_pair_sample_feature_vectorizer_fct(pair_hierarchy, 
                                                                                         mention_hierarchy, 
                                                                                         with_singleton_features, 
                                                                                         with_anaphoricity_features, 
                                                                                         quantize, 
                                                                                         #strict
                                                                                         True
                                                                                         )
    attribute_name_expected_result_pairs.append(("_features_vectorizer", expected__features_vectorizer))
    
    args = tuple()
    kwargs = {"pair_hierarchy": pair_hierarchy, "mention_hierarchy": mention_hierarchy, 
              "with_singleton_features": with_singleton_features, 
              "with_anaphoricity_features": with_anaphoricity_features, "quantize": quantize, 
              #"strict": strict, 
              "configuration": configuration}
    
    return (args, kwargs), attribute_name_expected_result_pairs

def get_extended_mention_pair_sample_features_vectorizer_test__inner_eq_data():
    data = []
    
    (args, kwargs), _ = get_extended_mention_pair_sample_features_vectorizer_test___init___data() #attribute_name_expected_result_pairs
    
    sample_features_vectorizer1 = ExtendedMentionPairSampleFeaturesVectorizer(*args, **kwargs)
    sample_features_vectorizer2 = ExtendedMentionPairSampleFeaturesVectorizer(*args, **kwargs)
    expected_result = (True, None)
    data.append((sample_features_vectorizer1, sample_features_vectorizer2, expected_result))
    
    kwargs["with_anaphoricity_features"] = False
    sample_features_vectorizer3 = ExtendedMentionPairSampleFeaturesVectorizer(*args, **kwargs)
    expected_result = (False, "with_anaphoricity_features")
    data.append((sample_features_vectorizer1, sample_features_vectorizer3, expected_result))
    
    return data

def get_extended_mention_pair_sample_features_vectorizer_test_from_configuration_data():
    attribute_name_expected_result_pairs = []
    
    pair_hierarchy = PairGramTypeHierarchy()
    attribute_name_expected_result_pairs.append(("pair_hierarchy", pair_hierarchy))
    
    mention_hierarchy = MentionGramTypeHierarchy()
    attribute_name_expected_result_pairs.append(("mention_hierarchy", mention_hierarchy))
    
    with_singleton_features = False
    attribute_name_expected_result_pairs.append(("with_singleton_features", with_singleton_features))
    
    with_anaphoricity_features = True
    attribute_name_expected_result_pairs.append(("with_anaphoricity_features", with_anaphoricity_features))
    
    quantize = True
    attribute_name_expected_result_pairs.append(("quantize", quantize))
    
    #strict = True
    #attribute_name_expected_result_pairs.append(("strict", strict))
    
    configuration = Mapping({"useless_field": "useless_value"})
    expected_configuration = Mapping({"pair_hierarchy": Mapping({"name": pair_hierarchy.NAME, "config": pair_hierarchy.configuration}), 
                                      "mention_hierarchy": Mapping({"name": mention_hierarchy.NAME, "config": mention_hierarchy.configuration}), 
                                      "with_singleton_features": with_singleton_features, 
                                      "with_anaphoricity_features": with_anaphoricity_features, 
                                      "quantize": quantize, 
                                      #"strict": strict, 
                                      "useless_field": "useless_value"})
    attribute_name_expected_result_pairs.append(("configuration", expected_configuration))
    
    expected__features_vectorizer = create_mention_pair_sample_feature_vectorizer_fct(pair_hierarchy, 
                                                                                with_singleton_features, 
                                                                                with_anaphoricity_features, 
                                                                                quantize, 
                                                                                #strict
                                                                                True
                                                                                )
    attribute_name_expected_result_pairs.append(("_features_vectorizer", expected__features_vectorizer))
    
    args = tuple()
    kwargs = {"pair_hierarchy": pair_hierarchy, "mention_hierarchy": mention_hierarchy, 
              "with_singleton_features": with_singleton_features, 
              "with_anaphoricity_features": with_anaphoricity_features, "quantize": quantize,
              #"strict": strict, 
              "configuration": configuration}
    expected_result = ExtendedMentionPairSampleFeaturesVectorizer(*args, **kwargs)
    
    configuration = Mapping({"useless_field": "useless_value",
                             "pair_hierarchy": Mapping({"name": pair_hierarchy.NAME, "config": pair_hierarchy.configuration}), 
                             "mention_hierarchy": Mapping({"name": mention_hierarchy.NAME, "config": mention_hierarchy.configuration}), 
                             "with_singleton_features": with_singleton_features, 
                             "with_anaphoricity_features": with_anaphoricity_features, 
                             "quantize": quantize, #"strict": strict
                             })
    
    return configuration, expected_result



def get_super_extended_mention_pair_sample_features_vectorizer_test___init___data():
    (args, kwargs), attribute_name_expected_result_pairs0 = get_extended_mention_pair_sample_features_vectorizer_test___init___data()
    
    (pair_hierarchy, mention_hierarchy, with_singleton_features, with_anaphoricity_features, 
     quantize, strict) = (d[1] for d in attribute_name_expected_result_pairs0[:6])
    expected__features_vectorizer = create_super_extended_mention_pair_sample_feature_vectorizer_fct(pair_hierarchy, 
                                                                                                 mention_hierarchy, 
                                                                                                 with_singleton_features, 
                                                                                                 with_anaphoricity_features, 
                                                                                                 quantize, 
                                                                                                 strict)
    attribute_name_expected_result_pairs = list(attribute_name_expected_result_pairs0[:6])
    attribute_name_expected_result_pairs.append(("_features_vectorizer", expected__features_vectorizer))
    
    return (args, kwargs), attribute_name_expected_result_pairs


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()