# -*- coding: utf-8 -*-

import unittest

from cortex.learning.sample_features_vectorizer import (create_sample_features_vectorizer_from_factory_configuration, 
                                                        get_normalized_vectorizer_factory_configuration, 
                                                        MentionPairSampleFeaturesVectorizer, 
                                                        ExtendedMentionPairSampleFeaturesVectorizer, 
                                                        SuperExtendedMentionPairSampleFeaturesVectorizer, 
                                                        )
from cortex.tools import Mapping

class Test(unittest.TestCase):

    def test_create_sample_features_vectorizer_from_factory_configuration(self):
        # Test parameters
        data = get_test_create_sample_features_vectorizer_from_factory_configuration_data()
        
        # Test
        for configuration, expected_result in data:
            actual_result = create_sample_features_vectorizer_from_factory_configuration(configuration)
            self.assertTrue(*expected_result._inner_eq(actual_result))
    
    def test_get_normalized_vectorizer_factory_configuration(self):
        # Test parameters
        with_singleton_features = True
        with_anaphoricity_features = False
        quantize = True
        configuration = Mapping({"pair_hierarchy": Mapping({"name": "pairnone",}), 
                                 "with_singleton_features": with_singleton_features, 
                                 "with_anaphoricity_features": with_anaphoricity_features, 
                                 "quantize": quantize, 
                                 "strict": True, 
                                 "useless_field": "useless_value",
                                 })
        vectorizer_configuration = Mapping({"name": "mentionpair", "config": configuration})
        
        expected_result = Mapping({"name": "MentionPairSampleFeaturesVectorizer", 
                                   "config": Mapping({"pair_hierarchy": Mapping({"name": "PairNoneHierarchy", 
                                                                                 "config": Mapping(),
                                                                                 }), 
                                                     "with_singleton_features": with_singleton_features, 
                                                     "with_anaphoricity_features": with_anaphoricity_features, 
                                                     "quantize": quantize, 
                                                     #"strict": True, 
                                                     }),
                                   })
        
        # Test
        actual_result = get_normalized_vectorizer_factory_configuration(vectorizer_configuration)
        self.assertTrue(*type(expected_result)._inner_eq(expected_result, actual_result))




def get_test_create_sample_features_vectorizer_from_factory_configuration_data():
    data = []
    
    # MentionPairSample
    with_singleton_features = True
    with_anaphoricity_features = False
    quantize = True
    configuration = Mapping({"pair_hierarchy": Mapping({"name": "pairnone"}), 
                             "with_singleton_features": with_singleton_features, 
                             "with_anaphoricity_features": with_anaphoricity_features, 
                             "quantize": quantize, "strict": True, 
                             "useless_field": "useless_value"})
    expected_result = MentionPairSampleFeaturesVectorizer.from_configuration(configuration)
    
    configuration = Mapping({"name": "mentionpair", "config": configuration})
    
    data.append((configuration, expected_result))
    
    # ExtendedMentionPairSample
    with_singleton_features = True
    with_anaphoricity_features = False
    quantize = True
    configuration = Mapping({"pair_hierarchy": Mapping({"name": "pairnone"}), 
                             "mention_hierarchy": Mapping({"name": "mentionnone"}), 
                             "with_singleton_features": with_singleton_features, 
                             "with_anaphoricity_features": with_anaphoricity_features, 
                             "quantize": quantize, "strict": True, 
                             "useless_field": "useless_value"})
    expected_result = ExtendedMentionPairSampleFeaturesVectorizer.from_configuration(configuration)
    
    configuration = Mapping({"name": "extendedmentionpair", "config": configuration})
    
    data.append((configuration, expected_result))
    
    # SuperExtendedMentionPairSample
    with_singleton_features = True
    with_anaphoricity_features = False
    quantize = True
    configuration = Mapping({"pair_hierarchy": Mapping({"name": "pairnone"}), 
                             "mention_hierarchy": Mapping({"name": "mentionnone"}), 
                             "with_singleton_features": with_singleton_features, 
                             "with_anaphoricity_features": with_anaphoricity_features, 
                             "quantize": quantize, "strict": True, 
                             "useless_field": "useless_value"})
    expected_result = SuperExtendedMentionPairSampleFeaturesVectorizer.from_configuration(configuration)
    
    configuration = Mapping({"name": "superextendedmentionpair", "config": configuration})
    
    data.append((configuration, expected_result))
    
    return data
    

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()