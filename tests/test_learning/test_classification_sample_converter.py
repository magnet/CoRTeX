# -*- coding: utf-8 -*-

import unittest

import os
import tempfile
from collections import OrderedDict
import numpy
from scipy import sparse
from collections import defaultdict

from cortex.parameters import ENCODING
from cortex.learning.classification_sample_converter import ClassificationSampleConverter
from cortex.tools.ml_features import (NumericFeature, CategoricalFeature, SampleVectorizer, 
                                        create_product_features_from_groups_definition)
from cortex.utils import are_array_equal


class MockClassificationSample(object):
    def __init__(self, label, x):
        self.label = label
        self.x = x
    def contextual_sample_hash(self):
        return hash(self.x)


class TestClassificationSampleConverter(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        labels = ("a", "b", "c")
        features_vectorizer = get_simple_features_vectorizer()
        expected__coded_label2label = ("a", "b", "c")
        expected__label2coded_label = OrderedDict((("a",0), ("b",1), ("c",2)))
        expected_features_names = ("test_numeric_feature",  
                                    "[test_categorical_feature1]__(yes)",
                                    "[test_categorical_feature1]__(no)",
                                    "[test_categorical_feature2]__(yep)",
                                    "[test_categorical_feature2]__(nope)", 
                                    "[product_[test_categorical_feature1]_[test_categorical_feature2]]__((yes)_(yep))",
                                    "[product_[test_categorical_feature1]_[test_categorical_feature2]]__((yes)_(nope))",
                                    "[product_[test_categorical_feature1]_[test_categorical_feature2]]__((no)_(yep))",
                                    "[product_[test_categorical_feature1]_[test_categorical_feature2]]__((no)_(nope))",
                                    "[product_[test_categorical_feature2]_[test_categorical_feature1]]__((yep)_(yes))",
                                    "[product_[test_categorical_feature2]_[test_categorical_feature1]]__((yep)_(no))",
                                    "[product_[test_categorical_feature2]_[test_categorical_feature1]]__((nope)_(yes))",
                                    "[product_[test_categorical_feature2]_[test_categorical_feature1]]__((nope)_(no))")
        expected__feature_name2feature_index = OrderedDict((name, index+1) for index, name in enumerate(expected_features_names))
        
        attribute_name_expected_result_pairs = (("_coded_label2label", expected__coded_label2label),
                                                ("_label2coded_label", expected__label2coded_label),
                                                ("_feature_name2feature_index", expected__feature_name2feature_index),
                                                ("features_names", expected_features_names),
                                                ("features_nb", 13),
                                                )
        
        # Test
        classification_sample_vectorizer = ClassificationSampleConverter(labels, features_vectorizer)
        for attribute_name, expected_value in attribute_name_expected_result_pairs:
            actual_value = getattr(classification_sample_vectorizer, attribute_name)
            print(attribute_name)
            self.assertEqual(expected_value, actual_value)
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        features = []
        ## Numeric feature
        compute_fct = lambda x: -x**2
        feature = NumericFeature("test_numeric_feature", compute_fct)
        features.append(feature)
        ## Categorical feature 1
        compute_fct = lambda x: "yes" if -x**2 < -26 else "no"
        possible_values = ("yes", "no")
        strict = True
        feature = CategoricalFeature("test_categorical_feature1", compute_fct, possible_values, strict=strict)
        features.append(feature)
        new_features_vectorizer = SampleVectorizer(features)
        
        labels1 = ("NEG", "POS")
        features_vectorizer1 = get_simple_features_vectorizer()
        labels2 = ("NEG", "POS", "NEG", "POS", "POS")
        features_vectorizer2 = get_simple_features_vectorizer()
        labels3 = ("NEG", "POS")
        features_vectorizer3 = new_features_vectorizer
        
        classification_sample_vectorizer1 = ClassificationSampleConverter(labels1, features_vectorizer1)
        classification_sample_vectorizer2 = ClassificationSampleConverter(labels2, features_vectorizer2)
        classification_sample_vectorizer3 = ClassificationSampleConverter(labels3, features_vectorizer3)
        
        # Test
        self.assertTrue(*classification_sample_vectorizer1._inner_eq(classification_sample_vectorizer2))
        self.assertEqual((False, "_sample_features_vectorizer"), classification_sample_vectorizer1._inner_eq(classification_sample_vectorizer3))
    
    #@unittest.skip
    def test_decode_coded_label(self):
        #Test parameters
        labels = ("NEG", "POS")
        features_vectorizer = get_simple_features_vectorizer()
        classification_sample_vectorizer = ClassificationSampleConverter(labels, features_vectorizer)
        coded_label = "3"
        
        # Test
        with self.assertRaises(ValueError):
            classification_sample_vectorizer.decode_label(coded_label)
    
    #@unittest.skip
    def test_convert(self):
        # Test parameters
        labels = ("NEG", "POS")
        features_vectorizer = get_simple_features_vectorizer(sample=True)
        classification_sample_vectorizer = ClassificationSampleConverter(labels, features_vectorizer)
        classification_sample1 = MockClassificationSample(labels[1], 3)
        classification_sample2 = MockClassificationSample(labels[0], -5)
        classification_sample3 = MockClassificationSample(labels[1], 5)
        classification_samples = (classification_sample1, classification_sample2, classification_sample3)
        def _gen():
            for cs in classification_samples:
                yield cs
        expected_data = sparse.csr_matrix([[-9, 0,1, 0,1, 0,0,0,1, 0,0,0,1], 
                                           [-25, 0,1, 1,0, 0,0,1,0, 0,1,0,0], 
                                           [-25, 0,1, 0,1, 0,0,0,1, 0,0,0,1]], dtype=numpy.float)
        expected_labels = numpy.array([1,0,1])
        
        # Test
        with self.assertRaises(TypeError):
            classification_sample_vectorizer.convert(_gen())
        actual_data, actual_labels = classification_sample_vectorizer.convert(classification_samples)
        self.assertEqual(type(expected_data), type(actual_data))
        self.assertEqual(expected_data.shape, actual_data.shape)
        self.assertEqual(expected_data.dtype, actual_data.dtype)
        self.assertTrue(are_array_equal(expected_data.todense(), actual_data.todense()))
        self.assertTrue(are_array_equal(expected_labels, actual_labels))

    #@unittest.skip
    def test_convert_with_memoization2(self):
        from unittest.mock import MagicMock
        from cortex.api.document import Document
        # Test parameters
        labels = ("NEG", "POS")
        features_vectorizer = get_simple_features_vectorizer(sample=True)
        #hash_test1 = str(72281598486758557720813999999476025486) # Correspond to string "test1"
        hash_test1 = str(149829174924887743411345212536640394872)
        #hash_test2 = str(98364297223971956060659010912005025261) # Correspond to string "test2"
        hash_test2 = str(232296139462133954868013644226795517471)
        classification_sample_vectorizer = ClassificationSampleConverter(labels, features_vectorizer)
        classification_sample1 = MockClassificationSample(labels[1], 3)
        classification_sample1.document = Document("test1", "test1")
        classification_sample1.document.grunt = "1"
        #classification_sample1.document.hash_string = MagicMock(side_effect=[hash_test1, hash_test1])
        #classification_sample1.document.hash_string = PropertyMock()
        #classification_sample1.document.hash_string.return_value = hash_test1
        classification_sample1.to_hash_string = MagicMock(side_effect=lambda: "MP_((1, 4),(9, 14))")
        classification_sample2 = MockClassificationSample(labels[0], -5)
        classification_sample2.document = Document("test2", "test2")
        classification_sample2.document.grunt = "2"
        #classification_sample2.document = Mock()
        #classification_sample2.document.hash_string = MagicMock(side_effect=[hash_test2, hash_test2])
        #classification_sample2.document.hash_string = PropertyMock()
        #classification_sample2.document.hash_string.return_value = hash_test2
        classification_sample2.to_hash_string = MagicMock(side_effect=lambda: "MP_((1, 4),(9, 14))")
        classification_sample3 = MockClassificationSample(labels[1], 5)
        classification_sample3.document = Document("test1", "test1")
        classification_sample3.document.grunt = "3"
        #classification_sample3.document = Mock()
        #classification_sample3.document.hash_string = MagicMock(side_effect=[hash_test1, hash_test1])
        #classification_sample3.document.hash_string = PropertyMock()
        #classification_sample3.document.hash_string.return_value = hash_test1
        classification_sample3.to_hash_string = MagicMock(side_effect=lambda: "MP_((3, 4),(9, 14))")
        classification_samples = (classification_sample1, classification_sample2, classification_sample3)
        def _gen():
            for cs in classification_samples:
                yield cs
        expected_data = sparse.csr_matrix([[-9, 0,1, 0,1, 0,0,0,1, 0,0,0,1], 
                                           [-25, 0,1, 1,0, 0,0,1,0, 0,1,0,0], 
                                           [-25, 0,1, 0,1, 0,0,0,1, 0,0,0,1]], dtype=numpy.float)
        expected_labels = numpy.array([1,0,1])
        
        expected_cache_before = defaultdict(dict)
        cache_test1 = {"MP_((1, 4),(9, 14))": sparse.csr_matrix([[-9, 0,1, 0,1, 0,0,0,1, 0,0,0,1],], dtype=numpy.float),
                       "MP_((3, 4),(9, 14))": sparse.csr_matrix([[-25, 0,1, 0,1, 0,0,0,1, 0,0,0,1],], dtype=numpy.float),
                       }
        cache_test2 = {"MP_((1, 4),(9, 14))": sparse.csr_matrix([[-25, 0,1, 1,0, 0,0,1,0, 0,1,0,0],], dtype=numpy.float),
                       }
        expected_cache_after = defaultdict(dict)
        expected_cache_after[hash_test1] = cache_test1
        expected_cache_after[hash_test2] = cache_test2
        def _compare_document_cache(expected, actual):
            expected_keys = set(expected.keys())
            actual_keys = set(actual.keys())
            if not expected_keys == actual_keys:
                return False, "document_cache_keys: {} v. {}".format(expected_keys, actual_keys)
            for key in expected_keys:
                expected_value = expected[key]
                actual_value = actual[key]
                self.assertEqual(type(expected_value), type(actual_value))
                if not isinstance(actual_value, sparse.csr_matrix):
                    return False, "document_cache value: key = '{}', wrong type '{}', expected type = '{}'".format(key, type(actual_value), sparse.csr_matrix)
                if not are_array_equal(expected_value.todense(), actual_value.todense()):
                    return False, "document_cache value: key = '{}', wrong value '{}', expected value = '{}'".format(key, expected_value.todense(), actual_value.todense())
            return True, None
        def _compare_cache(expected, actual):
            expected_keys = set(expected.keys())
            actual_keys = set(actual.keys())
            if not expected_keys == actual_keys:
                return False, "cache_keys: {} v. {}".format(expected_keys, actual_keys)
            for key in expected_keys:
                expected_value = expected[key]
                actual_value = actual[key]
                test, message = _compare_document_cache(expected_value, actual_value)
                if not test:
                    return False, "cache value: key = '{}': values are not equal, for the '{}' reason".format(key, message)
            return True, None
        actual_cache = classification_sample_vectorizer._document_hash2cache
        
        # Test
        ## Test that the cache is in the expected state
        self.assertTrue(*_compare_cache(expected_cache_before, actual_cache))
        ## Get results the first time
        actual_data, actual_labels = classification_sample_vectorizer.convert_with_memoization(classification_samples)
        ## Check that it is equal to the expected data
        self.assertEqual(type(expected_data), type(actual_data))
        self.assertEqual(expected_data.shape, expected_data.shape)
        self.assertEqual(expected_data.dtype, expected_data.dtype)
        self.assertTrue(are_array_equal(expected_data.todense(), actual_data.todense()))
        self.assertTrue(are_array_equal(expected_labels, actual_labels))
        ## Test that the cache has been modified in the expected way
        self.assertTrue(*_compare_cache(expected_cache_after, actual_cache))
        
        ## Get results a second time, but check that the vectorizer has not been called
        classification_sample_vectorizer._sample_features_vectorizer = None
        actual_data2, actual_labels2 = classification_sample_vectorizer.convert_with_memoization(classification_samples)
        # Check that it is equal to the expected data
        self.assertEqual(type(expected_data), type(actual_data2))
        self.assertEqual(expected_data.shape, actual_data2.shape)
        self.assertEqual(expected_data.dtype, actual_data2.dtype)
        self.assertTrue(are_array_equal(expected_data.todense(), actual_data2.todense()))
        self.assertTrue(are_array_equal(expected_labels, actual_labels2))
    
    #@unittest.skip
    def test_convert_to_svmlight_file(self):
        # Test parameters
        labels = ("NEG", "POS")
        features_vectorizer = get_simple_features_vectorizer(sample=True)
        classification_sample_vectorizer = ClassificationSampleConverter(labels, features_vectorizer)
        classification_sample1 = MockClassificationSample(labels[1], 3)
        classification_sample2 = MockClassificationSample(labels[0], -5)
        classification_sample3 = MockClassificationSample(labels[1], 5)
        classification_samples = (classification_sample1, classification_sample2, classification_sample3)
        
        expected_result = "1 1:-9.0 3:1.0 5:1.0 9:1.0 13:1.0\n0 1:-25.0 3:1.0 4:1.0 8:1.0 11:1.0\n1 1:-25.0 3:1.0 5:1.0 9:1.0 13:1.0\n"
        
        # Test
        temp_file_path = tempfile.mktemp()
        try:
            classification_sample_vectorizer.convert_to_svmlight_file(classification_samples, temp_file_path)
            with open(temp_file_path, "r", encoding=ENCODING) as f:
                actual_result = f.read()
        finally:
            if os.path.isfile(temp_file_path):
                os.remove(temp_file_path)
        self.assertEqual(expected_result, actual_result)


def get_simple_features_vectorizer(sample=False):
    features = []
    ## Numeric feature
    compute_fct = lambda x: -x**2
    if sample:
        compute_fct = lambda sample: -(sample.x)**2
    feature = NumericFeature("test_numeric_feature", compute_fct)
    features.append(feature)
    ## Categorical feature 1
    compute_fct = lambda x: "yes" if -x**2 < -26 else "no"
    if sample:
        compute_fct = lambda sample: "yes" if -(sample.x)**2 < -26 else "no"
    possible_values = ("yes", "no")
    strict = True
    feature = CategoricalFeature("test_categorical_feature1", compute_fct, possible_values, strict=strict)
    features.append(feature)
    ## Categorical feature 2
    compute_fct = lambda x: "yep" if x < 0 else "nope"
    if sample:
        compute_fct = lambda sample: "yep" if sample.x < 0 else "nope"
    possible_values = ("yep", "nope")
    strict = True
    feature = CategoricalFeature("test_categorical_feature2", compute_fct, possible_values, strict=strict)
    features.append(feature)
    ## Group features
    first_group = (("test_categorical_feature1",), ("test_categorical_feature2",))
    second_group = (("test_categorical_feature2",), ("test_categorical_feature1",))
    features_groups = (first_group, second_group, )
    group_features = group_features = create_product_features_from_groups_definition(features_groups, features)
    ## Finally, the FeatureVectorizer instance
    features = features + group_features
    features_vectorizer = SampleVectorizer(features)
    
    return features_vectorizer


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()