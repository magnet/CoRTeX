# -*- coding: utf-8 -*-

import unittest

from cortex.api.document import Document
from cortex.api.document_buffer import DocumentBuffer
from cortex.learning.samples import MentionPairSample



class TestMentionPairSample(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        document_buffer = DocumentBuffer()
        mentions_data = {(1,4): {"ident": "1"}, (9,14): {"ident": "2"}}
        raw_text = "\nThis is a test ."
        ## Doc1
        doc1 = Document("doc", raw_text)
        document_buffer.initialize_anew(doc1)
        document_buffer.add_mentions(mentions_data)
        document_buffer.flush()
        ## Doc2
        doc2 = Document("doc", raw_text)
        document_buffer.initialize_anew(doc2)
        document_buffer.add_mentions(mentions_data)
        document_buffer.flush()
        ## Samples
        sample_doc1 = MentionPairSample(doc1.mentions[0], doc1.mentions[1])
        sample_doc2 = MentionPairSample(doc2.mentions[0], doc2.mentions[1])
        
        # Test
        hash1 = sample_doc1.contextual_sample_hash()
        hash2 = sample_doc2.contextual_sample_hash()
        self.assertNotEqual(hash1, hash2)
    
    #@unittest.skip
    def test_to_hash_string(self):
        # Test parameters
        document_buffer = DocumentBuffer()
        mentions_data = {(1,4): {"ident": "1"}, (9,14): {"ident": "2"}}
        raw_text = "\nThis is a test ."
        ## Doc1
        doc1 = Document("doc", raw_text)
        document_buffer.initialize_anew(doc1)
        document_buffer.add_mentions(mentions_data)
        document_buffer.flush()
        ## Doc2
        doc2 = Document("doc", raw_text)
        document_buffer.initialize_anew(doc2)
        document_buffer.add_mentions(mentions_data)
        document_buffer.flush()
        ## Samples
        sample_doc1 = MentionPairSample(doc1.mentions[0], doc1.mentions[1])
        sample_doc2 = MentionPairSample(doc2.mentions[0], doc2.mentions[1])
        
        ## Expected results
        expected_hash_string1 = "MP_((1, 4),(9, 14))"
        expected_hash_string2 = "MP_((1, 4),(9, 14))"
        
        # Test
        hash_string1 = sample_doc1.to_hash_string()
        hash_string2 = sample_doc2.to_hash_string()
        self.assertEqual(expected_hash_string1, hash_string1)
        self.assertEqual(expected_hash_string2, hash_string2)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()