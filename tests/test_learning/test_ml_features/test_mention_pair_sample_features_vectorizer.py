# -*- coding: utf-8 -*-

import unittest
import os
import itertools
import numpy as np
from scipy import sparse

from cortex.utils.memoize import Memoized
from cortex.tools import Mapping
from cortex.coreference.generator.pairs import MentionPairSampleGenerator
from cortex.learning.ml_features.hierarchy.pairs import PairNoneHierarchy, PairGramTypeHierarchy#, OverlapPairGramTypeHierarchy
from cortex.learning.ml_features.mention_pair_sample_features_vectorizer import _create_mention_pair_sample_feature_vectorizer


class Test__create_mention_pair_sample_feature_vectorizer(unittest.TestCase):
    
    #@unittest.skip
    def test_PairNoneHierarchy(self):
        # Test parameters
        hierarchy = PairNoneHierarchy()
        with_singleton_features = False
        with_anaphoricity_features = False
        quantize = False
        strict = False
        mention_pair_sample_vectorizer = _create_mention_pair_sample_feature_vectorizer(hierarchy, 
                                                                                  with_singleton_features, 
                                                                                  with_anaphoricity_features, 
                                                                                  quantize,
                                                                                  strict=strict)
        
        test_document = get_test_document_data()
        configuration = Mapping({"pair_filter": Mapping({"name": "acceptall"})})
        mention_pair_generator = MentionPairSampleGenerator.from_configuration(configuration)
        #expected_result = (((1,9), (14,42), POS_CLASS),((14,42), (27,42), NEG_CLASS),((1,9), (27,42), NEG_CLASS),((27,42), (46,47), NEG_CLASS),((14,42), (46,47), POS_CLASS),((1,9), (46,47), POS_CLASS),((46,47), (68,75), NEG_CLASS),((27,42), (68,75), POS_CLASS),((14,42), (68,75), NEG_CLASS),((1,9), (68,75), NEG_CLASS),((68,75), (83,84), NEG_CLASS),((46,47), (83,84), POS_CLASS),((27,42), (83,84), NEG_CLASS),((14,42), (83,84), POS_CLASS),((1,9), (83,84), POS_CLASS),((83,84), (114,120), NEG_CLASS),((68,75), (114,120), NEG_CLASS),((46,47), (114,120), NEG_CLASS),((27,42), (114,120), NEG_CLASS),((14,42), (114,120), NEG_CLASS),((1,9), (114,120), NEG_CLASS),)
        documents = (test_document,)
        mention_pair_sample = tuple(itertools.islice(mention_pair_generator.generate_training_set(documents),1))[0]
        
        expected_numeric_features_names = get_expected_numeric_features_names_PairNoneHierarchy_data_data()
        expected_total_numeric_features_nb = 1273
        data = [ 1.        ,  1.        ,  1.        ,  1.        ,  1.        ,
        1.        ,  1.        ,  1.        ,  1.        ,  1.        ,
        1.        ,  1.        ,  1.        ,  1.        ,  0.16666667,
        1.        ,  1.        ,  1.        ,  1.        ,  1.        ,
        1.        ,  0.66666667,  1.        ,  0.25      ,  0.33333333,
        0.16666667,  1.        ,  1.        ,  1.        ,  1.        ,
        1.        ,  1.        ,  1.        ,  1.        ,  1.        ,
        1.        ,  1.        ,  1.        ,  1.        ,  1.        ,
        1.        ,  0.16666667,  1.        ,  0.16666667,  1.        ,
        1.        ,  1.        ,  1.        ,  1.        ,  1.        ,
        1.        ,  1.        ,  1.        ,  1.        ,  0.16666667,
        0.16666667,  0.16666667,  0.16666667,  0.16666667,  0.16666667,
        0.16666667,  0.16666667,  0.16666667]
        columns = [   2,    7,   20,   30,   52,  220,  298,  299,  300,  301,  302,
        307,  310,  341,  343,  350,  356,  359,  362,  370,  399,  403,
        404,  440,  443,  445,  452,  481,  484,  489,  493,  494,  503,
        507,  508,  511,  566,  572,  626,  632,  668,  680,  722,  734,
        749,  750,  759,  760,  769,  770,  817,  820,  847,  850, 1109,
       1110, 1121, 1122, 1123, 1128, 1131, 1200, 1229]
        rows = list(0. for _ in range(len(data)))
        shape = (1,expected_total_numeric_features_nb)
        expected_encoded_sample_value = sparse.coo_matrix((data, (rows,columns)), shape=shape, dtype=np.float).tocsr()
        
        # Test 
        ## Consistency between features' numeric column names and numeric size
        for feature in mention_pair_sample_vectorizer._features:
            self.assertEqual(feature.numeric_size, len(feature.numeric_column_names))
        ## The encoder's numeric features names
        actual_numeric_features_names = mention_pair_sample_vectorizer.numeric_column_names
        self.assertEqual(expected_numeric_features_names, actual_numeric_features_names)
        ## Encoded sample(s)
        actual_encoded_sample_value = mention_pair_sample_vectorizer.vectorize_to_sparse_numeric(mention_pair_sample)
        self.assertTrue(np.allclose(expected_encoded_sample_value.todense(), actual_encoded_sample_value.todense()))
    
    #@unittest.skip
    def test_other_hierarchies(self):
        # Test parameters
        with_singleton_features = False
        with_anaphoricity_features = False
        quantize = False
        strict = False
        data = (PairGramTypeHierarchy(), 
                #OverlapPairGramTypeHierarchy(),
                )
        
        test_document = get_test_document_data()
        configuration = Mapping({"pair_filter": Mapping({"name": "acceptall"})})
        mention_pair_generator = MentionPairSampleGenerator.from_configuration(configuration)
        #expected_result = (((1,9), (14,42), POS_CLASS),((14,42), (27,42), NEG_CLASS),((1,9), (27,42), NEG_CLASS),((27,42), (46,47), NEG_CLASS),((14,42), (46,47), POS_CLASS),((1,9), (46,47), POS_CLASS),((46,47), (68,75), NEG_CLASS),((27,42), (68,75), POS_CLASS),((14,42), (68,75), NEG_CLASS),((1,9), (68,75), NEG_CLASS),((68,75), (83,84), NEG_CLASS),((46,47), (83,84), POS_CLASS),((27,42), (83,84), NEG_CLASS),((14,42), (83,84), POS_CLASS),((1,9), (83,84), POS_CLASS),((83,84), (114,120), NEG_CLASS),((68,75), (114,120), NEG_CLASS),((46,47), (114,120), NEG_CLASS),((27,42), (114,120), NEG_CLASS),((14,42), (114,120), NEG_CLASS),((1,9), (114,120), NEG_CLASS),)
        documents = (test_document,)
        mention_pair_sample = tuple(itertools.islice(mention_pair_generator.generate_training_set(documents),1))[0]
        
        expected_numeric_features_names_NonHierarchy_data = get_expected_numeric_features_names_PairNoneHierarchy_data_data()
        base_features_nb = len(expected_numeric_features_names_NonHierarchy_data)
        
        # Test
        for hierarchy in data:
            hierarchy_features_possible_values_nb = sum((len(t) for t in hierarchy.POSSIBLE_VALUES),0)
            mention_pair_sample_vectorizer = _create_mention_pair_sample_feature_vectorizer(hierarchy, 
                                                                                      with_singleton_features, 
                                                                                      with_anaphoricity_features, 
                                                                                      quantize,
                                                                                      strict=strict)
            expected_features_nb = hierarchy_features_possible_values_nb * base_features_nb
            
            ##
            for feature in mention_pair_sample_vectorizer._features:
                self.assertEqual(feature.numeric_size, len(feature.numeric_column_names))
            ##
            actual_features_nb = len(mention_pair_sample_vectorizer.numeric_column_names)
            self.assertEqual(expected_features_nb, actual_features_nb)
            
            ##
            _ = mention_pair_sample_vectorizer.vectorize_to_sparse_numeric(mention_pair_sample) #actual_encoded_sample_value




@Memoized
def get_test_document_data():
    from cortex.io.pivot_reader import PivotReader
    
    root_folder_path, file_name = os.path.split(__file__)
    file_name, _ = os.path.splitext(file_name)
    folder_path = os.path.join(root_folder_path, "{}_data".format(file_name))
    document_folder_path = os.path.join(folder_path, "mention_pair_generator_test_data", "ref")
    
    strict = True # We want to synchronize head tokens.
    test_document = PivotReader.parse(document_folder_path, strict=strict)
    
    return test_document

def get_expected_numeric_features_names_PairNoneHierarchy_data_data():
    root_folder_path, glop = os.path.split(__file__)
    file_name = "expected_numeric_features_names_PairNoneHierarchy.txt"
    folder_path = os.path.join(root_folder_path, "{}_data".format(glop.split(".")[0]))
    file_path = os.path.join(folder_path, file_name)
    with open(file_path, "r", encoding="utf-8") as f:
        expected_numeric_features_names = tuple(s.strip() for s in f.read().strip().split("\n"))
    return expected_numeric_features_names


'''
file_name = "expected_numeric_features_names_PairNoneHierarchy.txt"
root_folder_path, glop = os.path.split(__file__)
folder_path = os.path.join(root_folder_path, "{}_data".format(glop.split(".")[0]))
file_path = os.path.join(folder_path, file_name)
with open(file_path, "w", encoding="utf-8") as f:
    f.write("\n".join(actual_numeric_features_names))

print(repr(actual_encoded_sample_value.tocoo().shape))
print(repr(actual_encoded_sample_value.tocoo().data))
print(repr(actual_encoded_sample_value.tocoo().col))
decoded_non_zero_columns = mention_pair_sample_vectorizer.decode_numeric_vector(actual_encoded_sample_value)
for name, value in decoded_non_zero_columns:
    print("{:10.8f}:    {}".format(value, name))
'''


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()