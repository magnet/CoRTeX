# -*- coding: utf-8 -*-

import unittest
import os
import numpy as np
from scipy import sparse

from cortex.utils.memoize import Memoized
from cortex.learning.samples import SingletonSample, POS_CLASS
from cortex.learning.ml_features.hierarchy.mentions import MentionNoneHierarchy, MentionGramTypeHierarchy
from cortex.learning.ml_features.singleton_mention_sample_features_vectorizer import _create_singleton_mention_sample_feature_vectorizer


class Test__create_singleton_mention_sample_feature_vectorizer(unittest.TestCase):
    
    #@unittest.skip
    def test_MentionNoneHierarchy(self):
        # Test parameters
        hierarchy = MentionNoneHierarchy()
        quantize = False
        strict = False
        sample_vectorizer = _create_singleton_mention_sample_feature_vectorizer(hierarchy, quantize, 
                                                                          strict=strict)
        expected_total_numeric_features_nb = 83
        
        test_document = get_test_document_data()
        singleton_mention_sample = SingletonSample(test_document.mentions[0], label=POS_CLASS)
        
        data = [ 1.        ,  1.        ,  1.        ,  1.        ,  2.        ,
        0.5       ,  1/3,  2/3,  1.        ,  1.        ,
        0.25      ,  1.        ,  1.        ,  1.        ]
        columns = [ 4,  8,  9, 14, 21, 37, 38, 39, 40, 42, 51, 56, 69, 79]
        rows = list(0. for _ in range(len(data)))
        shape = (1,expected_total_numeric_features_nb)
        expected_encoded_sample_value = sparse.coo_matrix((data, (rows,columns)), shape=shape, dtype=np.float).tocsr()
        expected_numeric_features_names = get_expected_numeric_features_names_MentionNoneHierarchy_data()
        
        # Test
        ##
        for feature in sample_vectorizer._features:
            self.assertEqual(feature.numeric_size, len(feature.numeric_column_names))
        ##
        actual_numeric_features_names = sample_vectorizer.numeric_column_names
        self.assertEqual(expected_numeric_features_names, actual_numeric_features_names)
        ##
        actual_encoded_sample_value = sample_vectorizer.vectorize_to_sparse_numeric(singleton_mention_sample)
        self.assertTrue(np.allclose(expected_encoded_sample_value.todense(), actual_encoded_sample_value.todense()))
    
    #@unittest.skip
    def test_other_hierarchies(self):
        # Test parameters
        quantize = False
        strict = False
        data = (MentionGramTypeHierarchy(), 
                )
        
        test_document = get_test_document_data()
        singleton_mention_sample = SingletonSample(test_document.mentions[0], label=POS_CLASS)
        
        expected_numeric_features_names_NonHierarchy_data = get_expected_numeric_features_names_MentionNoneHierarchy_data()
        base_features_nb = len(expected_numeric_features_names_NonHierarchy_data)
        
        # Test
        for hierarchy in data:
            hierarchy_features_possible_values_nb = sum((len(t) for t in hierarchy.POSSIBLE_VALUES),0)
            sample_vectorizer = _create_singleton_mention_sample_feature_vectorizer(hierarchy, quantize, 
                                                                              strict=strict)
            expected_features_nb = hierarchy_features_possible_values_nb * base_features_nb
            
            ##
            for feature in sample_vectorizer._features:
                self.assertEqual(feature.numeric_size, len(feature.numeric_column_names))
            ##
            actual_features_nb = len(sample_vectorizer.numeric_column_names)
            self.assertEqual(expected_features_nb, actual_features_nb)
            
            ##
            _ = sample_vectorizer.vectorize_to_sparse_numeric(singleton_mention_sample) #actual_encoded_sample_value




@Memoized
def get_test_document_data():
    from cortex.io.pivot_reader import PivotReader
    
    root_folder_path, file_name = os.path.split(__file__)
    file_name, _ = os.path.splitext(file_name)
    folder_path = os.path.join(root_folder_path, "{}_data".format(file_name))
    document_folder_path = os.path.join(folder_path, "mention_pair_generator_test_data", "ref")
    
    strict = True # We want to synchronize head tokens.
    test_document = PivotReader.parse(document_folder_path, strict=strict)
    
    return test_document

def get_expected_numeric_features_names_MentionNoneHierarchy_data():
    root_folder_path, glop = os.path.split(__file__)
    file_name = "expected_numeric_features_names_MentionNoneHierarchy.txt"
    folder_path = os.path.join(root_folder_path, "{}_data".format(glop.split(".")[0]))
    file_path = os.path.join(folder_path, file_name)
    with open(file_path, "r", encoding="utf-8") as f:
        expected_numeric_features_names = tuple(s.strip() for s in f.read().strip().split("\n"))
    return expected_numeric_features_names


'''
file_name = "expected_numeric_features_names_MentionNoneHierarchy.txt"
root_folder_path, glop = os.path.split(__file__)
folder_path = os.path.join(root_folder_path, "{}_data".format(glop.split(".")[0]))
file_path = os.path.join(folder_path, file_name)
with open(file_path, "w", encoding="utf-8") as f:
    f.write("\n".join(actual_numeric_features_names))


print(actual_encoded_sample_value.tocoo().shape)
print(repr(actual_encoded_sample_value.tocoo().data))
print(repr(actual_encoded_sample_value.tocoo().col))
'''


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()