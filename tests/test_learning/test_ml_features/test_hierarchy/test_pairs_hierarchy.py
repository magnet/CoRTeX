# -*- coding: utf-8 -*-

import unittest

from cortex.learning.ml_features.hierarchy import create_hierarchy_from_factory_configuration
from cortex.learning.ml_features.hierarchy.pairs import PairNoneHierarchy, PairGramTypeHierarchy#, OverlapPairGramTypeHierarchy
from cortex.tools import Mapping

class TestPairGramTypeHierarchy(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        configuration = Mapping({"witness_field": "witness_value"})
        
        # Test
        pair_hierarchy = PairGramTypeHierarchy(configuration=configuration)
        self.assertEqual(configuration, pair_hierarchy.configuration)
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        pair_hierarchy1 = PairGramTypeHierarchy(configuration=Mapping({"witness_field": "witness_value"}))
        pair_hierarchy2 = PairGramTypeHierarchy(configuration=Mapping({"witness_field": "witness_value"}))
        pair_hierarchy3 = PairNoneHierarchy(configuration=Mapping({"witness_field": "witness_value"}))#OverlapPairGramTypeHierarchy
        
        # Test
        self.assertTrue(*pair_hierarchy1._inner_eq(pair_hierarchy2))
        self.assertEqual((False,"Class"), pair_hierarchy1._inner_eq(pair_hierarchy3))
    
    #@unittest.skip
    def test_from_configuration(self):
        # Test parameter
        configuration = Mapping({"useless_field": "useless_value"})
        expected_result = PairGramTypeHierarchy(configuration=configuration)
        
        # Test
        actual_result = PairGramTypeHierarchy.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        args = tuple()
        kwargs = {}
        expected_result = Mapping()
        
        # Test
        actual_result = PairGramTypeHierarchy.define_configuration(*args, **kwargs)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))



#@unittest.skip
class Test(unittest.TestCase):
    
    #@unittest.skip
    def test_create_hierarchy_from_factory_configuration(self):
        # Test parameters
        data = []
        
        configuration = Mapping({"name": "pairnone"})
        expected_result = PairNoneHierarchy(configuration=Mapping())
        data.append((configuration, expected_result))
        
        configuration = Mapping({"name": "pairgramtype"})
        expected_result = PairGramTypeHierarchy(configuration=Mapping())
        data.append((configuration, expected_result))
        '''
        configuration = Mapping({"name": "overlappairgramtype"})
        expected_result = OverlapPairGramTypeHierarchy(configuration=Mapping())
        data.append((configuration, expected_result))
        '''
        # Test
        for configuration, expected_result in data:
            actual_result = create_hierarchy_from_factory_configuration(configuration)
            self.assertTrue(*expected_result._inner_eq(actual_result))



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()