# -*- coding: utf-8 -*-

import unittest
import numpy
from scipy import sparse

from cortex.tools import Mapping
from cortex.utils.io import TemporaryDirectory
from cortex.learning.ml_model.classifier.online_binary_classifier import (PerceptronOnlineBinaryClassifier, 
                                                                          PAOnlineBinaryClassifier, 
                                                                          CWFullOnlineBinaryClassifier, 
                                                                          CWDiagOnlineBinaryClassifier, 
                                                                          AROWFullOnlineBinaryClassifier, 
                                                                          AROWDiagOnlineBinaryClassifier,
                                                                          )
from cortex.utils import are_array_equal

POS_CLASS_LABEL = "POS"
NEG_CLASS_LABEL = "NEG"

#@unittest.skip
class TestPerceptronOnlineBinaryClassifier(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        attribute_name_expected_result_pairs = []
        
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        #labels = (positive_class_label, negative_class_label)
        iteration_nb = 3
        alpha = 0.005
        avg = True
        warm_start = False
        configuration = Mapping({"witness_field": "witness_value"})
        expected__label2coef = {POS_CLASS_LABEL: 1.0, NEG_CLASS_LABEL: -1.0}
        expected_configuration = Mapping({"witness_field": "witness_value", 
                                          #"labels": labels, 
                                          "iteration_nb": iteration_nb, 
                                          "positive_class_label": positive_class_label, 
                                          "negative_class_label": negative_class_label, 
                                          "alpha": alpha, "avg": avg, "warm_start": warm_start})
        args = (positive_class_label, negative_class_label)
        kwargs = {"iteration_nb": iteration_nb, "alpha": alpha, "avg": avg, "warm_start": warm_start, 
                  "configuration": configuration}
        attribute_name_expected_result_pairs.append(("positive_class_label", positive_class_label))
        attribute_name_expected_result_pairs.append(("negative_class_label", negative_class_label))
        attribute_name_expected_result_pairs.append(("iteration_nb", iteration_nb))
        attribute_name_expected_result_pairs.append(("alpha", alpha))
        attribute_name_expected_result_pairs.append(("avg", avg))
        attribute_name_expected_result_pairs.append(("warm_start", warm_start))
        attribute_name_expected_result_pairs.append(("_label2coef", expected__label2coef))
        attribute_name_expected_result_pairs.append(("configuration", expected_configuration))
        
        # Test
        binary_classifier = PerceptronOnlineBinaryClassifier(*args, **kwargs)
        for attribute_name, expected_value in attribute_name_expected_result_pairs:
            actual_value = getattr(binary_classifier, attribute_name)
            self.assertEqual(expected_value, actual_value)
        
        self.assertEqual(None, binary_classifier.train_mode)
        binary_classifier.train_mode = True
        self.assertEqual(binary_classifier.avg, False)
        binary_classifier.train_mode = False
        self.assertEqual(binary_classifier.avg, binary_classifier._original_avg)
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        iteration_nb = 5
        alpha = 0.005
        avg = True
        warm_start = False
        
        args = (positive_class_label, negative_class_label)
        kwargs = {"iteration_nb": iteration_nb, "alpha": alpha, "avg": avg, "warm_start": warm_start, 
                  "configuration": Mapping({"witness_field": "witness_value", 
                                            "iteration_nb": iteration_nb, 
                                            "positive_class_label": positive_class_label, 
                                            "negative_class_label": negative_class_label, 
                                            "alpha": alpha, "avg": avg, "warm_start": warm_start})}
        data = []
        binary_classifier1 = PerceptronOnlineBinaryClassifier(*args, **kwargs)
        binary_classifier2 = PerceptronOnlineBinaryClassifier(*args, **kwargs)
        data.append(((binary_classifier1, binary_classifier2), True, None))
        
        binary_classifier3 = PerceptronOnlineBinaryClassifier(*args, **kwargs)
        binary_classifier3.coef_ = numpy.zeros((1,4), dtype=numpy.float)
        data.append(((binary_classifier1, binary_classifier3), False, "One of the compared element is lacking the 'coef_' attribute"))
        
        # Test
        for (binary_classifier1, binary_classifier2), expected_result, expected_message in data:
            actual_result, actual_message = binary_classifier1._inner_eq(binary_classifier2)
            self.assertEqual(expected_result, actual_result)
            self.assertEqual(expected_message, actual_message)
    
    #@unittest.skip
    def test_from_configuration(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        iteration_nb = 6
        alpha = 0.005
        avg = True
        warm_start = False
        
        configuration = Mapping({"positive_class_label": positive_class_label, 
                                 "negative_class_label": negative_class_label, 
                                 "iteration_nb": iteration_nb, 
                                 "alpha": alpha, "avg": avg, "warm_start": warm_start, 
                                 "witness_field": "witness_value"})
        
        args = (positive_class_label, negative_class_label)
        kwargs = {"iteration_nb": iteration_nb, "alpha": alpha, "avg": avg, "warm_start": warm_start, 
                  "configuration": Mapping({"witness_field": "witness_value", 
                                            "iteration_nb": iteration_nb, 
                                           "positive_class_label": positive_class_label, 
                                           "negative_class_label": negative_class_label, 
                                           "alpha": alpha, "avg": avg, "warm_start": warm_start})}
        expected_result = PerceptronOnlineBinaryClassifier(*args, **kwargs)
        
        # Test
        actual_result = PerceptronOnlineBinaryClassifier.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = PerceptronOnlineBinaryClassifier
        positive_class_label = "test_pos"
        negative_class_label = "test_neg"
        iteration_nb = 42
        alpha = 0.0675
        avg = True
        warm_start = True
        args = (positive_class_label, negative_class_label)
        kwargs = {"iteration_nb": iteration_nb,
                  "alpha": alpha,
                  "avg": avg,
                  "warm_start": warm_start, 
                  }
        expected_result = Mapping({"positive_class_label": positive_class_label,
                                   "negative_class_label": negative_class_label,
                                   "iteration_nb": iteration_nb,
                                   "alpha": alpha,
                                   "avg": avg,
                                   "warm_start": warm_start,
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip
    def test_init_model_and_can_predict(self):
        # Test parameters
        attribute_name_expected_result_pairs = []
        
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        binary_classifier = PerceptronOnlineBinaryClassifier(positive_class_label, negative_class_label)
        features_nb = 7
        expected_coef_ = numpy.zeros((1,features_nb), dtype=numpy.float)
        expected__cumulative_coef_ = numpy.zeros((1,features_nb), dtype=numpy.float)
        attribute_name_expected_result_pairs.append(("coef_", expected_coef_))
        attribute_name_expected_result_pairs.append(("_cumulative_coef_", expected__cumulative_coef_))
        
        # Test
        for attribute_name, _ in attribute_name_expected_result_pairs: #expected_value
            with self.assertRaises(AttributeError):
                getattr(binary_classifier, attribute_name)
        self.assertFalse(binary_classifier.can_predict())
        binary_classifier.init_model(features_nb)
        for attribute_name, expected_value in attribute_name_expected_result_pairs:
            actual_value = getattr(binary_classifier, attribute_name)
            self.assertTrue(are_array_equal(expected_value, actual_value))
        self.assertTrue(binary_classifier.can_predict())
    
    #@unittest.skip
    def test__label2sign(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        binary_classifier = PerceptronOnlineBinaryClassifier(positive_class_label, negative_class_label)
        
        data = (("POS", 1.0), 
                ("NEG", -1.0),
                )
        
        # Test
        for label, expected_value in data:
            actual_value = binary_classifier._label2sign(label)
            self.assertEqual(expected_value, actual_value)
    
    #@unittest.skip
    def test__get_weights(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        binary_classifier = PerceptronOnlineBinaryClassifier(positive_class_label, negative_class_label)
        binary_classifier.coef_ = numpy.array([[1,-2,3,-4]], dtype=numpy.float)
        binary_classifier._cumulative_coef_ = numpy.array([[3,-6,9,-12]], dtype=numpy.float)
        data = (({"avg": False}, numpy.array([[1,-2,3,-4]], dtype=numpy.float)), 
                ({"avg": True}, numpy.array([[3,-6,9,-12]], dtype=numpy.float)), 
                )
        expected_result = numpy.array([[1,-2,3,-4]], dtype=numpy.float)
        
        # Test
        for kwargs, expected_result in data:
            actual_result = binary_classifier._get_weights(**kwargs)
            self.assertTrue(are_array_equal(expected_result, actual_result))
    
    #@unittest.skip
    def test_get_weights(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        binary_classifier = PerceptronOnlineBinaryClassifier(positive_class_label, negative_class_label)
        features_nb = 7
        coef_ = numpy.zeros((1,features_nb), dtype=numpy.float)
        cumulative_coef_ = numpy.zeros((1,features_nb), dtype=numpy.float)
        expected_coef_ = numpy.zeros((1,features_nb), dtype=numpy.float)
        expected__cumulative_coef_ = numpy.zeros((1,features_nb), dtype=numpy.float)
        
        # Test
        self.assertEqual(binary_classifier.get_weights(), None)
        self.assertEqual(binary_classifier.get_cumulative_weights(), None)
        binary_classifier.coef_ = coef_
        binary_classifier._cumulative_coef_ = cumulative_coef_
        self.assertTrue(are_array_equal(binary_classifier.get_weights(), expected_coef_))
        self.assertTrue(are_array_equal(expected__cumulative_coef_, expected__cumulative_coef_))
    
    #@unittest.skip
    def test__predict_score(self):
        # avg = False
        ## Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        binary_classifier = PerceptronOnlineBinaryClassifier(positive_class_label, negative_class_label)
        coef_ = numpy.array([[1,-2,3,-4]], dtype=numpy.float)
        _cumulative_coef_ = numpy.array([[-3,6,-9,12]], dtype=numpy.float)
        binary_classifier.coef_ = coef_
        binary_classifier._cumulative_coef_ = _cumulative_coef_
        X = numpy.array([[0,1,0,1], [1,0,1,0]], dtype=numpy.float)
        avg = False
        expected_scores = numpy.array([-6.0,4.0], dtype=numpy.float)
        
        ## Test
        actual_scores = binary_classifier._predict_score(X, avg=avg)
        self.assertTrue(are_array_equal(expected_scores, actual_scores))
        
        # avg = True
        ## Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        binary_classifier = PerceptronOnlineBinaryClassifier(positive_class_label, negative_class_label)
        coef_ = numpy.array([[1,-2,3,-4]], dtype=numpy.float)
        _cumulative_coef_ = numpy.array([[-3,6,-9,12]], dtype=numpy.float)
        binary_classifier.coef_ = coef_
        binary_classifier._cumulative_coef_ = _cumulative_coef_
        X = numpy.array([[0,1,0,1], [1,0,1,0]], dtype=numpy.float)
        avg = True
        expected_scores = numpy.array([18.0,-12.0], dtype=numpy.float)
        
        ## Test
        actual_scores = binary_classifier._predict_score(X, avg=avg)
        self.assertTrue(are_array_equal(expected_scores, actual_scores))
        
        # With sparse input
        ## Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        binary_classifier = PerceptronOnlineBinaryClassifier(positive_class_label, negative_class_label)
        coef_ = numpy.array([[1,-2,3,-4]], dtype=numpy.float)
        _cumulative_coef_ = numpy.array([[-3,6,-9,12]], dtype=numpy.float)
        binary_classifier.coef_ = coef_
        binary_classifier._cumulative_coef_ = _cumulative_coef_
        X = sparse.csr_matrix(numpy.array([[0,1,0,1], [1,0,1,0]], dtype=numpy.float))
        avg = True
        expected_scores = numpy.array([18.0,-12.0], dtype=numpy.float)
        
        ## Test
        actual_scores = binary_classifier._predict_score(X, avg=avg)
        self.assertTrue(are_array_equal(expected_scores, actual_scores))
    
    #@unittest.skip
    def test_decode_score(self):
        ## Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        binary_classifier = PerceptronOnlineBinaryClassifier(positive_class_label, negative_class_label)
        scores = numpy.array([-6.0,4.0], dtype=numpy.float)
        expected_labels = (NEG_CLASS_LABEL,POS_CLASS_LABEL)
        
        ## Test
        actual_labels = tuple(binary_classifier.decode_score(scores))
        self.assertEqual(expected_labels, actual_labels)
    
    #@unittest.skip
    def test_classify(self):
        # avg = False
        ## Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        avg = False
        binary_classifier = PerceptronOnlineBinaryClassifier(positive_class_label, negative_class_label, avg=avg)
        coef_ = numpy.array([[1,-2,3,-4]], dtype=numpy.float)
        _cumulative_coef_ = numpy.array([[-3,6,-9,12]], dtype=numpy.float)
        binary_classifier.coef_ = coef_
        binary_classifier._cumulative_coef_ = _cumulative_coef_
        X = numpy.array([[0,1,0,1], [1,0,1,0]], dtype=numpy.float)
        expected_labels = numpy.array((NEG_CLASS_LABEL,POS_CLASS_LABEL))
        
        ## Test
        actual_labels = binary_classifier.classify(X)
        self.assertTrue(all(ev == av for ev, av in zip(expected_labels, actual_labels)))
        
        # avg = True
        ## Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        avg = True
        binary_classifier = PerceptronOnlineBinaryClassifier(positive_class_label, negative_class_label, avg=avg)
        coef_ = numpy.array([[1,-2,3,-4]], dtype=numpy.float)
        _cumulative_coef_ = numpy.array([[-3,6,-9,12]], dtype=numpy.float)
        binary_classifier.coef_ = coef_
        binary_classifier._cumulative_coef_ = _cumulative_coef_
        X = numpy.array([[0,1,0,1], [1,0,1,0]], dtype=numpy.float)
        expected_labels = numpy.array((POS_CLASS_LABEL,NEG_CLASS_LABEL))
        
        ## Test
        actual_labels = binary_classifier.classify(X)
        self.assertTrue(all(ev == av for ev, av in zip(expected_labels, actual_labels)))
    
    #@unittest.skip
    def test__update(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        alpha = 2.0
        binary_classifier = PerceptronOnlineBinaryClassifier(positive_class_label, negative_class_label, 
                                                       alpha=alpha)
        features_nb = 4
        data = []
        x = numpy.array([0,1,0,1], dtype=numpy.float)
        true_label = POS_CLASS_LABEL
        prediction = NEG_CLASS_LABEL, -0.6
        data.append((x, true_label, prediction))
        x = numpy.array([0,1,1,0], dtype=numpy.float)
        true_label = NEG_CLASS_LABEL
        prediction = POS_CLASS_LABEL, 2.0
        data.append((x, true_label, prediction))
        expected_coef_ = numpy.array([[0.0,0.0,-2.0,2.0]], dtype=numpy.float)#numpy.array([[0.0,2.0,0.0,2.0]], dtype=numpy.float)
        expected__cumulative_coef_ = numpy.array([[0.0,2.0,-2.0,4.0]], dtype=numpy.float)
        
        # Test
        with self.assertRaises(AttributeError):
            binary_classifier._update(x, true_label, prediction)
        binary_classifier.init_model(features_nb)
        for x, true_label, prediction in data:
            binary_classifier._update(x, true_label, prediction)
        actual_coef_ = binary_classifier.coef_
        actual__cumulative_coef_ = binary_classifier._cumulative_coef_
        self.assertTrue(are_array_equal(expected_coef_,actual_coef_))
        self.assertTrue(are_array_equal(expected__cumulative_coef_,actual__cumulative_coef_))
    
    #@unittest.skip
    def test_learn(self):
        # warm_start = False
        ## Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        iteration_nb = 2
        alpha = 2.0
        warm_start = False
        binary_classifier = PerceptronOnlineBinaryClassifier(positive_class_label, 
                                                                      negative_class_label, 
                                                                      iteration_nb=iteration_nb, 
                                                                      alpha=alpha, 
                                                                      warm_start=warm_start)
        X = numpy.array([[0.,1.,0.,1.],[0.,1.,1.,0.]], dtype=numpy.float)
        Y = numpy.array((POS_CLASS_LABEL, NEG_CLASS_LABEL))
        binary_classifier.coef_ = numpy.array([[1,2,3,4]], dtype=numpy.float) # To catch the effect of warm_start = False
        binary_classifier._cumulative_coef_ = numpy.array([[1,2,3,4]], dtype=numpy.float) # To catch the effect of warm_start = False
        expected_coef_ = numpy.array([[0.0,0.0,-2.0,2.0]], dtype=numpy.float)#numpy.array([[0.0,2.0,0.0,2.0]], dtype=numpy.float)
        expected__cumulative_coef_ = numpy.array([[0.0,6.0,-2.0,8.0]], dtype=numpy.float)#numpy.array([[0.0,6.0,-2.0,8.0]], dtype=numpy.float)
        
        ## Test
        binary_classifier.learn(X, Y)
        actual_coef_ = binary_classifier.coef_
        actual__cumulative_coef_ = binary_classifier._cumulative_coef_
        self.assertTrue(are_array_equal(expected_coef_,actual_coef_))
        self.assertTrue(are_array_equal(expected__cumulative_coef_,actual__cumulative_coef_))
        
        # warm_start = True
        ## Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        iteration_nb = 2
        alpha = 2.0
        warm_start = True
        binary_classifier = PerceptronOnlineBinaryClassifier(positive_class_label, 
                                                                      negative_class_label, 
                                                                      iteration_nb=iteration_nb, 
                                                                      alpha=alpha, 
                                                                      warm_start=warm_start)
        X = numpy.array([[0.,1.,0.,1.],[0.,1.,1.,0.]], dtype=numpy.float)
        Y = numpy.array((POS_CLASS_LABEL, NEG_CLASS_LABEL))
        binary_classifier.coef_ = numpy.array([[1,2,3,4]], dtype=numpy.float) # To catch the effect of warm_start = False
        binary_classifier._cumulative_coef_ = numpy.array([[1,2,3,4]], dtype=numpy.float) # To catch the effect of warm_start = False
        expected_coef_ = numpy.array([[1,-2,-1,4]], dtype=numpy.float)#numpy.array([[0.0,2.0,0.0,2.0]], dtype=numpy.float)
        expected__cumulative_coef_ = numpy.array([[5,2,7,20]], dtype=numpy.float)
        
        ## Test
        binary_classifier.learn(X, Y)
        actual_coef_ = binary_classifier.coef_
        actual__cumulative_coef_ = binary_classifier._cumulative_coef_
        self.assertTrue(are_array_equal(expected_coef_,actual_coef_))
        self.assertTrue(are_array_equal(expected__cumulative_coef_,actual__cumulative_coef_))
    
    #@unittest.skip
    def test_learn_and_can_predict(self):
        # warm_start = False
        ## Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        iteration_nb = 2
        alpha = 2.0
        warm_start = False
        binary_classifier = PerceptronOnlineBinaryClassifier(positive_class_label, 
                                                                      negative_class_label, 
                                                                      iteration_nb=iteration_nb, 
                                                                      alpha=alpha, 
                                                                      warm_start=warm_start)
        X = numpy.array([[0.,1.,0.,1.],[0.,1.,1.,0.]], dtype=numpy.float)
        Y = numpy.array((POS_CLASS_LABEL, NEG_CLASS_LABEL))
        
        ## Test
        binary_classifier.learn(X, Y)
        self.assertTrue(binary_classifier.can_predict())
    
    #@unittest.skip
    def test__update_sum(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        alpha = 2.0
        features_nb = 4
        
        binary_classifier = PerceptronOnlineBinaryClassifier(positive_class_label, negative_class_label, 
                                                       alpha=alpha)
        data = []
        x = numpy.array([0,1,0,1], dtype=numpy.float)
        loss = 12.0
        data.append((x, loss))
        x = -numpy.array([0,1,1,0], dtype=numpy.float)
        loss = 6.0
        data.append((x, loss))
        expected_coef_ = numpy.array([[0.0,0.0,-2.0,2.0]], dtype=numpy.float)#numpy.array([[0.0,2.0,0.0,2.0]], dtype=numpy.float)
        expected__cumulative_coef_ = numpy.array([[0.0,2.0,-2.0,4.0]], dtype=numpy.float)
        
        # Test
        with self.assertRaises(AttributeError):
            binary_classifier._update_sum(x, loss)
        binary_classifier.init_model(features_nb)
        for x, loss in data:
            binary_classifier._update_sum(x, loss)
        actual_coef_ = binary_classifier.coef_
        actual__cumulative_coef_ = binary_classifier._cumulative_coef_
        self.assertTrue(are_array_equal(expected_coef_,actual_coef_))
        self.assertTrue(are_array_equal(expected__cumulative_coef_,actual__cumulative_coef_))
    
    #@unittest.skip
    def test_update_sum(self):
        # Test parameters
        data = []
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        alpha = 2.0
        features_nb = 4
        
        # Dense vectors
        ## Parameters
        binary_classifier = PerceptronOnlineBinaryClassifier(positive_class_label, negative_class_label, 
                                                       alpha=alpha)
        binary_classifier.init_model(features_nb)
        positive_class_encoded_samples = [numpy.array([0,1,0,1], dtype=numpy.float),]
        negative_class_encoded_samples = [numpy.array([0,1,1,0], dtype=numpy.float),]
        loss = 13.0
        args = (positive_class_encoded_samples, negative_class_encoded_samples, loss)
        expected_coef_ = numpy.array([[0.0,0.0,-2.0,2.0]], dtype=numpy.float)#numpy.array([[0.0,2.0,0.0,2.0]], dtype=numpy.float)
        expected__cumulative_coef_ = numpy.array([[0.0,0.0,-2.0,2.0]], dtype=numpy.float)
        data.append((binary_classifier, args, (expected_coef_, expected__cumulative_coef_)))
        
        # Sparse vectors
        ## Parameters
        binary_classifier = PerceptronOnlineBinaryClassifier(positive_class_label, negative_class_label, 
                                                       alpha=alpha)
        binary_classifier.init_model(features_nb)
        positive_class_encoded_samples = [sparse.csr_matrix(numpy.array([0,1,0,1], dtype=numpy.float)),]
        negative_class_encoded_samples = [sparse.csr_matrix(numpy.array([0,1,1,0], dtype=numpy.float)),]
        loss = 13.0
        args = (positive_class_encoded_samples, negative_class_encoded_samples, loss)
        expected_coef_ = numpy.array([[0.0,0.0,-2.0,2.0]], dtype=numpy.float)#numpy.array([[0.0,2.0,0.0,2.0]], dtype=numpy.float)
        expected__cumulative_coef_ = numpy.array([[0.0,0.0,-2.0,2.0]], dtype=numpy.float)
        data.append((binary_classifier, args, (expected_coef_, expected__cumulative_coef_)))
        
        # Test
        for binary_classifier, args, (expected_coef_, expected__cumulative_coef_) in data:
            binary_classifier.update_sum(*args)
            actual_coef_ = binary_classifier.coef_
            actual__cumulative_coef_ = binary_classifier._cumulative_coef_
            self.assertTrue(are_array_equal(expected_coef_,actual_coef_))
            self.assertTrue(are_array_equal(expected__cumulative_coef_,actual__cumulative_coef_))
    
    #@unittest.skip
    def test_save_load_model(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        alpha = 2.0
        warm_start = True
        iteration_nb = 1
        binary_classifier = PerceptronOnlineBinaryClassifier(positive_class_label, 
                                                             negative_class_label, 
                                                             alpha=alpha, 
                                                             warm_start=warm_start,
                                                             iteration_nb=iteration_nb)
        X = numpy.array([[0.,1.,0.,1.],[0.,1.,1.,0.]], dtype=numpy.float)
        Y = numpy.array((POS_CLASS_LABEL, NEG_CLASS_LABEL))
        def _learn_fct(binary_classifier):
            binary_classifier.learn(X, Y)
            binary_classifier.learn(X, Y)
            expected_arrays = {"coef_": numpy.array([[0.0,0.0,-2.0,2.0]], dtype=numpy.float), 
                               "_cumulative_coef_": numpy.array([[0.0,6.0,-2.0,8.0]], dtype=numpy.float)}
            actual_arrays = {"coef_": binary_classifier.coef_, 
                             "_cumulative_coef_": binary_classifier._cumulative_coef_}
            for attribute_name in actual_arrays.keys():
                expected_array_value = expected_arrays[attribute_name]
                actual_array_value = actual_arrays[attribute_name]
                self.assertTrue(are_array_equal(expected_array_value, actual_array_value), attribute_name)
            expected_arrays = actual_arrays
            return expected_arrays
        
        # Test
        with TemporaryDirectory() as temp_dir:
            folder_path = temp_dir.temporary_folder_path
            with self.assertRaises(ValueError):
                binary_classifier.save_model(folder_path)
            expected_arrays = _learn_fct(binary_classifier)
            binary_classifier.save_model(folder_path)
            new_binary_classifier = PerceptronOnlineBinaryClassifier(positive_class_label, negative_class_label, 
                                                                     alpha=alpha, warm_start=warm_start,
                                                                     iteration_nb=iteration_nb)
            new_binary_classifier.load_model(folder_path)
        actual_arrays = {"coef_": new_binary_classifier.coef_, 
                         "_cumulative_coef_": new_binary_classifier._cumulative_coef_}
        self.assertEqual(expected_arrays.keys(), actual_arrays.keys())
        for attribute_name in actual_arrays.keys():
            expected_array_value = expected_arrays[attribute_name]
            actual_array_value = actual_arrays[attribute_name]
            self.assertTrue(are_array_equal(expected_array_value, actual_array_value), attribute_name)



#@unittest.skip
class TestPAOnlineBinaryClassifier(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        attribute_name_expected_result_pairs = []
        
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        #labels = (positive_class_label, negative_class_label)
        iteration_nb = 3
        C = 50.09
        avg = True
        warm_start = False
        configuration = Mapping({"witness_field": "witness_value"})
        expected__label2coef = {POS_CLASS_LABEL: 1.0, NEG_CLASS_LABEL: -1.0}
        expected_configuration = Mapping({"witness_field": "witness_value", 
                                          "iteration_nb": iteration_nb, 
                                          #"labels": labels, 
                                          "positive_class_label": positive_class_label, 
                                          "negative_class_label": negative_class_label, 
                                          "C": C, "avg": avg, "warm_start": warm_start})
        args = (positive_class_label, negative_class_label)
        kwargs = {"iteration_nb": iteration_nb, "C": C, "avg": avg, "warm_start": warm_start, 
                  "configuration": configuration}
        attribute_name_expected_result_pairs.append(("positive_class_label", positive_class_label))
        attribute_name_expected_result_pairs.append(("negative_class_label", negative_class_label))
        attribute_name_expected_result_pairs.append(("iteration_nb", iteration_nb))
        attribute_name_expected_result_pairs.append(("C", C))
        attribute_name_expected_result_pairs.append(("avg", avg))
        attribute_name_expected_result_pairs.append(("warm_start", warm_start))
        attribute_name_expected_result_pairs.append(("_label2coef", expected__label2coef))
        attribute_name_expected_result_pairs.append(("configuration", expected_configuration))
        
        # Test
        binary_classifier = PAOnlineBinaryClassifier(*args, **kwargs)
        for attribute_name, expected_value in attribute_name_expected_result_pairs:
            actual_value = getattr(binary_classifier, attribute_name)
            self.assertEqual(expected_value, actual_value)
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        iteration_nb = 5
        C = 50.09
        avg = True
        warm_start = False
        
        args = (positive_class_label, negative_class_label)
        kwargs = {"iteration_nb": iteration_nb, "C": C, "avg": avg, "warm_start": warm_start, 
                  "configuration": Mapping({"witness_field": "witness_value", 
                                            "iteration_nb": iteration_nb, 
                                           "positive_class_label": positive_class_label, 
                                           "negative_class_label": negative_class_label, 
                                           "C": C, "avg": avg, "warm_start": warm_start})}
        data = []
        binary_classifier1 = PAOnlineBinaryClassifier(*args, **kwargs)
        binary_classifier2 = PAOnlineBinaryClassifier(*args, **kwargs)
        data.append(((binary_classifier1, binary_classifier2), True, None))
        
        kwargs["C"] = 158.76
        binary_classifier3 = PAOnlineBinaryClassifier(*args, **kwargs)
        data.append(((binary_classifier1, binary_classifier3), False, "C"))
        
        # Test
        for (binary_classifier1, binary_classifier2), expected_result, expected_message in data:
            actual_result, actual_message = binary_classifier1._inner_eq(binary_classifier2)
            self.assertEqual(expected_result, actual_result)
            self.assertEqual(expected_message, actual_message)
    
    #@unittest.skip
    def test_from_configuration(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        iteration_nb = 6
        C = 59.76
        avg = True
        warm_start = False
        
        configuration = Mapping({"positive_class_label": positive_class_label, 
                                 "negative_class_label": negative_class_label, 
                                 "iteration_nb": iteration_nb, 
                                 "C": C, "avg": avg, "warm_start": warm_start, 
                                 "witness_field": "witness_value"})
        
        args = (positive_class_label, negative_class_label)
        kwargs = {"iteration_nb": iteration_nb, "C": C, "avg": avg, "warm_start": warm_start, 
                  "configuration": Mapping({"witness_field": "witness_value", 
                                            "iteration_nb": iteration_nb, 
                                           "positive_class_label": positive_class_label, 
                                           "negative_class_label": negative_class_label, 
                                           "C": C, "avg": avg, "warm_start": warm_start})}
        expected_result = PAOnlineBinaryClassifier(*args, **kwargs)
        
        # Test
        actual_result = PAOnlineBinaryClassifier.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = PAOnlineBinaryClassifier
        positive_class_label = "test_pos"
        negative_class_label = "test_neg"
        iteration_nb = 42
        C = 14.0675
        avg = True
        warm_start = True
        args = (positive_class_label, negative_class_label)
        kwargs = {"iteration_nb": iteration_nb,
                  "C": C,
                  "avg": avg,
                  "warm_start": warm_start, 
                  }
        expected_result = Mapping({"positive_class_label": positive_class_label,
                                   "negative_class_label": negative_class_label,
                                   "iteration_nb": iteration_nb,
                                   "C": C,
                                   "avg": avg,
                                   "warm_start": warm_start,
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip
    def test__update(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        C = 50.09
        binary_classifier = PAOnlineBinaryClassifier(positive_class_label, negative_class_label, C=C)
        features_nb = 4
        data = []
        x = numpy.array([0,1,0,1], dtype=numpy.float)
        true_label = POS_CLASS_LABEL
        prediction = NEG_CLASS_LABEL, -0.6
        data.append((x, true_label, prediction))
        x = numpy.array([0,1,1,0], dtype=numpy.float)
        true_label = NEG_CLASS_LABEL
        prediction = POS_CLASS_LABEL, 2.0
        data.append((x, true_label, prediction))
        
        # Test
        with self.assertRaises(AttributeError):
            binary_classifier._update(x, true_label, prediction)
        binary_classifier.init_model(features_nb)
        for x, true_label, prediction in data:
            binary_classifier._update(x, true_label, prediction)
        actual_coef = binary_classifier.coef_
        self.assertTrue(numpy.sum(numpy.abs(actual_coef)) > 0)
    
    #@unittest.skip
    def test__update_sum(self):
        # Dense vectors
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        C = 50.09
        binary_classifier = PAOnlineBinaryClassifier(positive_class_label, negative_class_label, C=C)
        features_nb = 4
        data = []
        x = numpy.array([0,1,0,1], dtype=numpy.float)
        loss = 12.0
        data.append((x, loss))
        x = -numpy.array([0,1,1,0], dtype=numpy.float)
        loss = 6.0
        data.append((x, loss))
        
        # Test
        with self.assertRaises(AttributeError):
            binary_classifier._update_sum(x, loss)
        binary_classifier.init_model(features_nb)
        for x, loss in data:
            binary_classifier._update_sum(x, loss)
        actual_coef = binary_classifier.coef_
        self.assertTrue(numpy.sum(numpy.abs(actual_coef)) > 0)
    
    #@unittest.skip
    def test_learn_and_can_predict(self):
        # warm_start = False
        ## Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        C = 50.09
        binary_classifier = PAOnlineBinaryClassifier(positive_class_label, negative_class_label, C=C)
        X = numpy.array([[0.,1.,0.,1.],[0.,1.,1.,0.]], dtype=numpy.float)
        Y = numpy.array((POS_CLASS_LABEL, NEG_CLASS_LABEL))
        
        ## Test
        binary_classifier.learn(X, Y)
        self.assertTrue(binary_classifier.can_predict())
    
    #@unittest.skip
    def test_save_load_model(self):
        # Test parameters
        positive_class_label = "test_pos"
        negative_class_label = "test_neg"
        iteration_nb = 42
        C = 14.0675
        avg = True
        warm_start = True
        configuration = Mapping({"positive_class_label": positive_class_label,
                                   "negative_class_label": negative_class_label,
                                   "iteration_nb": iteration_nb,
                                   "C": C,
                                   "avg": avg,
                                   "warm_start": warm_start,
                                   })
        binary_classifier = PAOnlineBinaryClassifier.from_configuration(configuration)
        X = numpy.array([[0.,1.,0.,1.],[0.,1.,1.,0.]], dtype=numpy.float)
        Y = numpy.array((positive_class_label, negative_class_label))
        binary_classifier.learn(X, Y)
        binary_classifier.learn(X, Y)
        expected_arrays = {"coef_": binary_classifier.coef_, 
                           "_cumulative_coef_": binary_classifier._cumulative_coef_}
        
        with TemporaryDirectory() as temp_dir:
            folder_path = temp_dir.temporary_folder_path
            binary_classifier.save_model(folder_path)
            new_binary_folder = PAOnlineBinaryClassifier.from_configuration(configuration)
            new_binary_folder.load_model(folder_path)
        actual_arrays = {"coef_": new_binary_folder.coef_, 
                         "_cumulative_coef_": new_binary_folder._cumulative_coef_}
        self.assertEqual(expected_arrays.keys(), actual_arrays.keys())
        for attribute_name in actual_arrays.keys():
            expected_array_value = expected_arrays[attribute_name]
            actual_array_value = actual_arrays[attribute_name]
            self.assertTrue(are_array_equal(expected_array_value, actual_array_value), attribute_name)



#@unittest.skip
class TestCWFullOnlineBinaryClassifier(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        attribute_name_expected_result_pairs = []
        
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        #labels = (positive_class_label, negative_class_label)
        iteration_nb = 3
        eta = 0.9
        linearization = True
        warm_start = False
        configuration = Mapping({"witness_field": "witness_value"})
        expected__label2coef = {POS_CLASS_LABEL: 1.0, NEG_CLASS_LABEL: -1.0}
        expected_configuration = Mapping({"witness_field": "witness_value", 
                                          "iteration_nb": iteration_nb, 
                                          #"labels": labels, 
                                          "positive_class_label": positive_class_label, 
                                          "negative_class_label": negative_class_label, 
                                          "eta": eta, "linearization": linearization, 
                                          "warm_start": warm_start, "avg": False})
        args = (positive_class_label, negative_class_label)
        kwargs = {"iteration_nb": iteration_nb, "eta": eta, "linearization": linearization, 
                  "warm_start": warm_start, "configuration": configuration}
        attribute_name_expected_result_pairs.append(("positive_class_label", positive_class_label))
        attribute_name_expected_result_pairs.append(("negative_class_label", negative_class_label))
        attribute_name_expected_result_pairs.append(("iteration_nb", iteration_nb))
        attribute_name_expected_result_pairs.append(("eta", eta))
        attribute_name_expected_result_pairs.append(("linearization", linearization))
        attribute_name_expected_result_pairs.append(("warm_start", warm_start))
        attribute_name_expected_result_pairs.append(("_label2coef", expected__label2coef))
        attribute_name_expected_result_pairs.append(("configuration", expected_configuration))
        
        # Test
        binary_classifier = CWFullOnlineBinaryClassifier(*args, **kwargs)
        for attribute_name, expected_value in attribute_name_expected_result_pairs:
            actual_value = getattr(binary_classifier, attribute_name)
            self.assertEqual(expected_value, actual_value)
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        iteration_nb = 5
        eta = 0.9
        linearization = True
        warm_start = False
        
        args = (positive_class_label, negative_class_label)
        kwargs = {"iteration_nb": iteration_nb, "eta": eta, "linearization": linearization, 
                  "warm_start": warm_start, 
                  "configuration": Mapping({"witness_field": "witness_value", 
                                            "iteration_nb": iteration_nb, 
                                           "positive_class_label": positive_class_label, 
                                           "negative_class_label": negative_class_label, 
                                           "eta": eta, "linearization": linearization, 
                                           "warm_start": warm_start, "avg": False})}
        data = []
        binary_classifier1 = CWFullOnlineBinaryClassifier(*args, **kwargs)
        binary_classifier2 = CWFullOnlineBinaryClassifier(*args, **kwargs)
        data.append(((binary_classifier1, binary_classifier2), True, None))
        
        binary_classifier3 = CWFullOnlineBinaryClassifier(*args, **kwargs)
        binary_classifier3._sigma = numpy.zeros((1,4), dtype=numpy.float)
        data.append(((binary_classifier1, binary_classifier3), False, "One of the compared element is lacking the '_sigma' attribute"))
        
        # Test
        for (binary_classifier1, binary_classifier2), expected_result, expected_message in data:
            actual_result, actual_message = binary_classifier1._inner_eq(binary_classifier2)
            self.assertEqual(expected_result, actual_result)
            self.assertEqual(expected_message, actual_message)
    
    #@unittest.skip
    def test_from_configuration(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        iteration_nb = 6
        eta = 0.9
        linearization = True
        warm_start = False
        
        configuration = Mapping({"positive_class_label": positive_class_label, 
                                 "negative_class_label": negative_class_label, 
                                 "iteration_nb": iteration_nb, 
                                 "eta": eta, "linearization": linearization, "avg": False, 
                                 "warm_start": warm_start, "witness_field": "witness_value"})
        
        args = (positive_class_label, negative_class_label)
        kwargs = {"iteration_nb": iteration_nb, "eta": eta, "linearization": linearization, "warm_start": warm_start, 
                  "configuration": Mapping({"witness_field": "witness_value", 
                                            "iteration_nb": iteration_nb, 
                                            "positive_class_label": positive_class_label, 
                                            "negative_class_label": negative_class_label, 
                                            "eta": eta, "linearization": linearization, 
                                            "warm_start": warm_start, "avg": False})}
        expected_result = CWFullOnlineBinaryClassifier(*args, **kwargs)
        
        # Test
        actual_result = CWFullOnlineBinaryClassifier.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = CWFullOnlineBinaryClassifier
        positive_class_label = "test_pos"
        negative_class_label = "test_neg"
        iteration_nb = 42
        eta = 0.55
        linearization = True
        warm_start = True
        args = (positive_class_label, negative_class_label)
        kwargs = {"iteration_nb": iteration_nb,
                  "eta": eta,
                  "linearization": linearization,
                  "warm_start": warm_start, 
                  }
        expected_result = Mapping({"positive_class_label": positive_class_label,
                                   "negative_class_label": negative_class_label,
                                   "iteration_nb": iteration_nb,
                                   "eta": eta,
                                   "linearization": linearization,
                                   "warm_start": warm_start,
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip
    def test_init_model_and_can_predict(self):
        # Test parameters
        attribute_name_expected_result_pairs = []
        
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        eta = 0.9
        linearization = True
        binary_classifier = CWFullOnlineBinaryClassifier(positive_class_label, negative_class_label, eta=eta, linearization=linearization)
        features_nb = 4
        expected_coef_ = numpy.array([[0.,0.,0.,0.]], dtype=numpy.float)
        expected__sigma = numpy.array([[1.,0.,0.,0.],[0.,1.,0.,0.],[0.,0.,1.,0.],[0.,0.,0.,1.]],dtype=numpy.float)
        attribute_name_expected_result_pairs.append(("coef_", expected_coef_))
        attribute_name_expected_result_pairs.append(("_sigma", expected__sigma))
        
        # Test
        for attribute_name, _ in attribute_name_expected_result_pairs: #expected_value
            with self.assertRaises(AttributeError):
                getattr(binary_classifier, attribute_name)
        self.assertFalse(binary_classifier.can_predict())
        binary_classifier.init_model(features_nb)
        for attribute_name, expected_value in attribute_name_expected_result_pairs:
            actual_value = getattr(binary_classifier, attribute_name)
            self.assertTrue(are_array_equal(expected_value, actual_value))
        self.assertTrue(binary_classifier.can_predict())
    
    #@unittest.skip
    def test__update(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        eta = 0.9
        linearization = True
        binary_classifier = CWFullOnlineBinaryClassifier(positive_class_label, negative_class_label, eta=eta, linearization=linearization)
        features_nb = 4
        data = []
        x = numpy.array([0,1,0,1], dtype=numpy.float)
        true_label = POS_CLASS_LABEL
        prediction = NEG_CLASS_LABEL, -0.6
        data.append((x, true_label, prediction))
        x = numpy.array([0,1,1,0], dtype=numpy.float)
        true_label = NEG_CLASS_LABEL
        prediction = POS_CLASS_LABEL, 2.0
        data.append((x, true_label, prediction))
        
        # Test
        with self.assertRaises(AttributeError):
            binary_classifier._update(x, true_label, prediction)
        binary_classifier.init_model(features_nb)
        for x, true_label, prediction in data:
            binary_classifier._update(x, true_label, prediction)
        actual_coef = binary_classifier.coef_
        self.assertTrue(numpy.sum(numpy.abs(actual_coef)) > 0)
    
    #@unittest.skip
    def test__update_sum(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        eta = 0.9
        linearization = True
        binary_classifier = CWFullOnlineBinaryClassifier(positive_class_label, negative_class_label, 
                                                         eta=eta, linearization=linearization)
        features_nb = 4
        data = []
        x = numpy.array([0,1,0,1], dtype=numpy.float)
        loss = 12.0
        data.append((x, loss))
        x = -numpy.array([0,1,1,0], dtype=numpy.float)
        loss = 6.0
        data.append((x, loss))
        
        # Test
        with self.assertRaises(AttributeError):
            binary_classifier._update_sum(x, loss)
        binary_classifier.init_model(features_nb)
        for x, loss in data:
            binary_classifier._update_sum(x, loss)
        actual_coef = binary_classifier.coef_
        self.assertTrue(numpy.sum(numpy.abs(actual_coef)) > 0)
    
    #@unittest.skip
    def test_learn_and_can_predict(self):
        # warm_start = False
        ## Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        eta = 0.9
        linearization = True
        binary_classifier = CWFullOnlineBinaryClassifier(positive_class_label, negative_class_label, 
                                                         eta=eta, linearization=linearization)
        X = numpy.array([[0.,1.,0.,1.],[0.,1.,1.,0.]], dtype=numpy.float)
        Y = numpy.array((POS_CLASS_LABEL, NEG_CLASS_LABEL))
        
        ## Test
        binary_classifier.learn(X, Y)
        self.assertTrue(binary_classifier.can_predict())
    
    #@unittest.skip
    def test_save_load_model(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        eta = 0.9
        linearization = True
        warm_start = False
        configuration = Mapping({"positive_class_label": positive_class_label, 
                                 "negative_class_label": negative_class_label, 
                                 "eta": eta, "linearization": linearization, "avg": False, 
                                 "warm_start": warm_start, "witness_field": "witness_value"})
        binary_classifier = CWFullOnlineBinaryClassifier.from_configuration(configuration)
        X = numpy.array([[0.,1.,0.,1.],[0.,1.,1.,0.]], dtype=numpy.float)
        Y = numpy.array((POS_CLASS_LABEL, NEG_CLASS_LABEL))
        binary_classifier.learn(X, Y)
        binary_classifier.learn(X, Y)
        expected_arrays = {"coef_": binary_classifier.coef_, 
                           "_sigma": binary_classifier._sigma}
        
        with TemporaryDirectory() as temp_dir:
            folder_path = temp_dir.temporary_folder_path
            binary_classifier.save_model(folder_path)
            new_binary_folder = CWFullOnlineBinaryClassifier.from_configuration(configuration)
            new_binary_folder.load_model(folder_path)
        actual_arrays = {"coef_": new_binary_folder.coef_, 
                         "_sigma": new_binary_folder._sigma}
        self.assertEqual(expected_arrays.keys(), actual_arrays.keys())
        for attribute_name in actual_arrays.keys():
            expected_array_value = expected_arrays[attribute_name]
            actual_array_value = actual_arrays[attribute_name]
            self.assertTrue(are_array_equal(expected_array_value, actual_array_value), attribute_name)



#@unittest.skip
class TestCWDiagOnlineBinaryClassifier(unittest.TestCase):
    
    #@unittest.skip
    def test_init_model_and_can_predict(self):
        # Test parameters
        attribute_name_expected_result_pairs = []
        
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        eta = 0.9
        linearization = True
        binary_classifier = CWDiagOnlineBinaryClassifier(positive_class_label, negative_class_label, eta=eta, linearization=linearization)
        features_nb = 4
        expected_coef_ = numpy.array([[0.,0.,0.,0.]], dtype=numpy.float)
        expected__sigma = numpy.array([1.,1.,1.,1.], dtype=numpy.float)
        attribute_name_expected_result_pairs.append(("coef_", expected_coef_))
        attribute_name_expected_result_pairs.append(("_sigma", expected__sigma))
        
        # Test
        for attribute_name, _ in attribute_name_expected_result_pairs: #expected_value
            with self.assertRaises(AttributeError):
                getattr(binary_classifier, attribute_name)
        self.assertFalse(binary_classifier.can_predict())
        binary_classifier.init_model(features_nb)
        for attribute_name, expected_value in attribute_name_expected_result_pairs:
            actual_value = getattr(binary_classifier, attribute_name)
            self.assertTrue(are_array_equal(expected_value, actual_value))
        #raise Exception()
        self.assertTrue(binary_classifier.can_predict())
    
    #@unittest.skip
    def test__update(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        eta = 0.9
        linearization = True
        binary_classifier = CWDiagOnlineBinaryClassifier(positive_class_label, negative_class_label, eta=eta, linearization=linearization)
        features_nb = 4
        data = []
        x = numpy.array([0,1,0,1], dtype=numpy.float)
        true_label = POS_CLASS_LABEL
        prediction = NEG_CLASS_LABEL, -0.6
        data.append((x, true_label, prediction))
        x = numpy.array([0,1,1,0], dtype=numpy.float)
        true_label = NEG_CLASS_LABEL
        prediction = POS_CLASS_LABEL, 2.0
        data.append((x, true_label, prediction))
        
        # Test
        with self.assertRaises(AttributeError):
            binary_classifier._update(x, true_label, prediction)
        binary_classifier.init_model(features_nb)
        for x, true_label, prediction in data:
            binary_classifier._update(x, true_label, prediction)
        actual_coef = binary_classifier.coef_
        self.assertTrue(numpy.sum(numpy.abs(actual_coef)) > 0)
    
    #@unittest.skip
    def test__update_sum(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        eta = 0.9
        linearization = True
        binary_classifier = CWDiagOnlineBinaryClassifier(positive_class_label, negative_class_label, eta=eta, linearization=linearization)
        features_nb = 4
        data = []
        x = numpy.array([0,1,0,1], dtype=numpy.float)
        loss = 12.0
        data.append((x, loss))
        x = -numpy.array([0,1,1,0], dtype=numpy.float)
        loss = 6.0
        data.append((x, loss))
        
        # Test
        with self.assertRaises(AttributeError):
            binary_classifier._update_sum(x, loss)
        binary_classifier.init_model(features_nb)
        for x, loss in data:
            binary_classifier._update_sum(x, loss)
        actual_coef = binary_classifier.coef_
        self.assertTrue(numpy.sum(numpy.abs(actual_coef)) > 0)



#@unittest.skip
class TestAROWFullOnlineBinaryClassifier(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        attribute_name_expected_result_pairs = []
        
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        #labels = (positive_class_label, negative_class_label)
        iteration_nb = 3
        r = 0.9
        warm_start = False
        configuration = Mapping({"witness_field": "witness_value"})
        expected__label2coef = {POS_CLASS_LABEL: 1.0, NEG_CLASS_LABEL: -1.0}
        expected_configuration = Mapping({"witness_field": "witness_value", 
                                          "iteration_nb": iteration_nb, 
                                          #"labels": labels, 
                                          "positive_class_label": positive_class_label, 
                                          "negative_class_label": negative_class_label, 
                                          "r": r, "warm_start": warm_start, "avg": False})
        args = (positive_class_label, negative_class_label)
        kwargs = {"iteration_nb": iteration_nb, "r": r, "warm_start": warm_start, 
                  "configuration": configuration}
        attribute_name_expected_result_pairs.append(("positive_class_label", positive_class_label))
        attribute_name_expected_result_pairs.append(("negative_class_label", negative_class_label))
        attribute_name_expected_result_pairs.append(("iteration_nb", iteration_nb))
        attribute_name_expected_result_pairs.append(("r", r))
        attribute_name_expected_result_pairs.append(("warm_start", warm_start))
        attribute_name_expected_result_pairs.append(("_label2coef", expected__label2coef))
        attribute_name_expected_result_pairs.append(("configuration", expected_configuration))
        
        # Test
        binary_classifier = AROWFullOnlineBinaryClassifier(*args, **kwargs)
        for attribute_name, expected_value in attribute_name_expected_result_pairs:
            actual_value = getattr(binary_classifier, attribute_name)
            self.assertEqual(expected_value, actual_value)
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        iteration_nb = 5
        r = 0.9
        warm_start = False
        
        args = (positive_class_label, negative_class_label)
        kwargs = {"iteration_nb": iteration_nb, "r": r, "warm_start": warm_start, 
                  "configuration": Mapping({"witness_field": "witness_value", 
                                            "iteration_nb": iteration_nb, 
                                           "positive_class_label": positive_class_label, 
                                           "negative_class_label": negative_class_label, 
                                           "r": r, "warm_start": warm_start, "avg": False})}
        data = []
        binary_classifier1 = AROWFullOnlineBinaryClassifier(*args, **kwargs)
        binary_classifier2 = AROWFullOnlineBinaryClassifier(*args, **kwargs)
        data.append(((binary_classifier1, binary_classifier2), True, None))
        
        kwargs["r"] = 12.69
        binary_classifier3 = AROWFullOnlineBinaryClassifier(*args, **kwargs)
        data.append(((binary_classifier1, binary_classifier3), False, "r"))
        
        # Test
        for (binary_classifier1, binary_classifier2), expected_result, expected_message in data:
            actual_result, actual_message = binary_classifier1._inner_eq(binary_classifier2)
            self.assertEqual(expected_result, actual_result)
            self.assertEqual(expected_message, actual_message)
    
    #@unittest.skip
    def test_from_configuration(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        iteration_nb = 6
        r = 0.9
        warm_start = False
        
        configuration = Mapping({"positive_class_label": positive_class_label, 
                                 "negative_class_label": negative_class_label, 
                                 "iteration_nb": iteration_nb, 
                                 "r": r, "avg": False, 
                                 "warm_start": warm_start, "witness_field": "witness_value"})
        
        args = (positive_class_label, negative_class_label)
        kwargs = {"iteration_nb": iteration_nb, "r": r, "warm_start": warm_start, 
                  "configuration": Mapping({"witness_field": "witness_value", 
                                            "positive_class_label": positive_class_label, 
                                            "negative_class_label": negative_class_label, 
                                            "iteration_nb": iteration_nb, 
                                            "r": r, "warm_start": warm_start, "avg": False})}
        expected_result = AROWFullOnlineBinaryClassifier(*args, **kwargs)
        
        # Test
        actual_result = AROWFullOnlineBinaryClassifier.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = AROWFullOnlineBinaryClassifier
        positive_class_label = "test_pos"
        negative_class_label = "test_neg"
        iteration_nb = 42
        r = 2.5
        warm_start = True
        args = (positive_class_label, negative_class_label)
        kwargs = {"iteration_nb": iteration_nb,
                  "r": r,
                  "warm_start": warm_start, 
                  }
        expected_result = Mapping({"positive_class_label": positive_class_label,
                                   "negative_class_label": negative_class_label,
                                   "iteration_nb": iteration_nb,
                                   "r": r,
                                   "warm_start": warm_start,
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip
    def test_init_model_and_can_predict(self):
        # Test parameters
        attribute_name_expected_result_pairs = []
        
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        r = 0.9
        binary_classifier = AROWFullOnlineBinaryClassifier(positive_class_label, negative_class_label, r=r)
        features_nb = 4
        expected_coef_ = numpy.array([[0.,0.,0.,0.]], dtype=numpy.float)
        expected__sigma = numpy.array([[1.,0.,0.,0.],[0.,1.,0.,0.],[0.,0.,1.,0.],[0.,0.,0.,1.]],dtype=numpy.float)
        attribute_name_expected_result_pairs.append(("coef_", expected_coef_))
        attribute_name_expected_result_pairs.append(("_sigma", expected__sigma))
        
        # Test
        for attribute_name, _ in attribute_name_expected_result_pairs: #expected_value
            with self.assertRaises(AttributeError):
                getattr(binary_classifier, attribute_name)
        self.assertFalse(binary_classifier.can_predict())
        binary_classifier.init_model(features_nb)
        for attribute_name, expected_value in attribute_name_expected_result_pairs:
            actual_value = getattr(binary_classifier, attribute_name)
            self.assertTrue(are_array_equal(expected_value, actual_value))
        self.assertTrue(binary_classifier.can_predict())
    
    #@unittest.skip
    def test__update(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        r = 0.9
        binary_classifier = AROWFullOnlineBinaryClassifier(positive_class_label, negative_class_label, r=r)
        features_nb = 4
        data = []
        x = numpy.array([0,1,0,1], dtype=numpy.float)
        true_label = POS_CLASS_LABEL
        prediction = NEG_CLASS_LABEL, -0.6
        data.append((x, true_label, prediction))
        x = numpy.array([0,1,1,0], dtype=numpy.float)
        true_label = NEG_CLASS_LABEL
        prediction = POS_CLASS_LABEL, 2.0
        data.append((x, true_label, prediction))
        
        # Test
        with self.assertRaises(AttributeError):
            binary_classifier._update(x, true_label, prediction)
        binary_classifier.init_model(features_nb)
        for x, true_label, prediction in data:
            binary_classifier._update(x, true_label, prediction)
        actual_coef = binary_classifier.coef_
        self.assertTrue(numpy.sum(numpy.abs(actual_coef)) > 0)
    
    #@unittest.skip
    def test__update_sum(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        r = 0.9
        binary_classifier = AROWFullOnlineBinaryClassifier(positive_class_label, negative_class_label, r=r)
        features_nb = 4
        data = []
        x = numpy.array([0,1,0,1], dtype=numpy.float)
        loss = 12.0
        data.append((x, loss))
        x = -numpy.array([0,1,1,0], dtype=numpy.float)
        loss = 6.0
        data.append((x, loss))
        
        # Test
        with self.assertRaises(AttributeError):
            binary_classifier._update_sum(x, loss)
        binary_classifier.init_model(features_nb)
        for x, loss in data:
            binary_classifier._update_sum(x, loss)
        actual_coef = binary_classifier.coef_
        self.assertTrue(numpy.sum(numpy.abs(actual_coef)) > 0)
    
    #@unittest.skip
    def test_learn_and_can_predict(self):
        # warm_start = False
        ## Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        r = 0.9
        binary_classifier = AROWFullOnlineBinaryClassifier(positive_class_label, negative_class_label, r=r)
        X = numpy.array([[0.,1.,0.,1.],[0.,1.,1.,0.]], dtype=numpy.float)
        Y = numpy.array((POS_CLASS_LABEL, NEG_CLASS_LABEL))
        
        ## Test
        binary_classifier.learn(X, Y)
        self.assertTrue(binary_classifier.can_predict())
    
    #@unittest.skip
    def test_save_load_model(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        r = 0.9
        warm_start = False
        configuration = Mapping({"positive_class_label": positive_class_label, 
                                 "negative_class_label": negative_class_label, 
                                 "r": r, "avg": False, 
                                 "warm_start": warm_start, "witness_field": "witness_value"})
        binary_classifier = AROWFullOnlineBinaryClassifier.from_configuration(configuration)
        X = numpy.array([[0.,1.,0.,1.],[0.,1.,1.,0.]], dtype=numpy.float)
        Y = numpy.array((POS_CLASS_LABEL, NEG_CLASS_LABEL))
        binary_classifier.learn(X, Y)
        binary_classifier.learn(X, Y)
        expected_arrays = {"coef_": binary_classifier.coef_, 
                           "_sigma": binary_classifier._sigma}
        
        with TemporaryDirectory() as temp_dir:
            folder_path = temp_dir.temporary_folder_path
            binary_classifier.save_model(folder_path)
            new_binary_folder = AROWFullOnlineBinaryClassifier.from_configuration(configuration)
            new_binary_folder.load_model(folder_path)
        actual_arrays = {"coef_": new_binary_folder.coef_, 
                         "_sigma": new_binary_folder._sigma}
        self.assertEqual(expected_arrays.keys(), actual_arrays.keys())
        for attribute_name in actual_arrays.keys():
            expected_array_value = expected_arrays[attribute_name]
            actual_array_value = actual_arrays[attribute_name]
            self.assertTrue(are_array_equal(expected_array_value, actual_array_value), attribute_name)



#@unittest.skip
class TestAROWDiagOnlineBinaryClassifier(unittest.TestCase):
    
    #@unittest.skip
    def test_init_model_and_can_predict(self):
        # Test parameters
        attribute_name_expected_result_pairs = []
        
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        r = 0.9
        binary_classifier = AROWDiagOnlineBinaryClassifier(positive_class_label, negative_class_label, r=r)
        features_nb = 4
        expected_coef_ = numpy.array([[0.,0.,0.,0.]], dtype=numpy.float)
        expected__sigma = numpy.array([1.,1.,1.,1.], dtype=numpy.float)
        attribute_name_expected_result_pairs.append(("coef_", expected_coef_))
        attribute_name_expected_result_pairs.append(("_sigma", expected__sigma))
        
        # Test
        for attribute_name, _ in attribute_name_expected_result_pairs: #expected_value
            with self.assertRaises(AttributeError):
                getattr(binary_classifier, attribute_name)
        self.assertFalse(binary_classifier.can_predict())
        binary_classifier.init_model(features_nb)
        for attribute_name, expected_value in attribute_name_expected_result_pairs:
            actual_value = getattr(binary_classifier, attribute_name)
            self.assertTrue(are_array_equal(expected_value, actual_value))
        self.assertTrue(binary_classifier.can_predict())
    
    #@unittest.skip
    def test__update(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        r = 0.9
        binary_classifier = AROWDiagOnlineBinaryClassifier(positive_class_label, negative_class_label, r=r)
        features_nb = 4
        data = []
        x = numpy.array([0,1,0,1], dtype=numpy.float)
        true_label = POS_CLASS_LABEL
        prediction = NEG_CLASS_LABEL, -0.6
        data.append((x, true_label, prediction))
        x = numpy.array([0,1,1,0], dtype=numpy.float)
        true_label = NEG_CLASS_LABEL
        prediction = POS_CLASS_LABEL, 2.0
        data.append((x, true_label, prediction))
        
        # Test
        with self.assertRaises(AttributeError):
            binary_classifier._update(x, true_label, prediction)
        binary_classifier.init_model(features_nb)
        for x, true_label, prediction in data:
            binary_classifier._update(x, true_label, prediction)
        actual_coef = binary_classifier.coef_
        self.assertTrue(numpy.sum(numpy.abs(actual_coef)) > 0)
    
    #@unittest.skip
    def test__update_sum(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        r = 0.9
        binary_classifier = AROWDiagOnlineBinaryClassifier(positive_class_label, negative_class_label, r=r)
        features_nb = 4
        data = []
        x = numpy.array([0,1,0,1], dtype=numpy.float)
        loss = 12.0
        data.append((x, loss))
        x = -numpy.array([0,1,1,0], dtype=numpy.float)
        loss = 6.0
        data.append((x, loss))
        
        # Test
        with self.assertRaises(AttributeError):
            binary_classifier._update_sum(x, loss)
        binary_classifier.init_model(features_nb)
        for x, loss in data:
            binary_classifier._update_sum(x, loss)
        actual_coef = binary_classifier.coef_
        self.assertTrue(numpy.sum(numpy.abs(actual_coef)) > 0)



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()