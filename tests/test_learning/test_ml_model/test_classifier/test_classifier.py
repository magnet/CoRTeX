# -*- coding: utf-8 -*-

import unittest

from cortex.learning.ml_model.classifier.online_binary_classifier import (PerceptronOnlineBinaryClassifier, 
                                                                          PAOnlineBinaryClassifier, 
                                                                          CWFullOnlineBinaryClassifier, 
                                                                          CWDiagOnlineBinaryClassifier, 
                                                                          AROWFullOnlineBinaryClassifier, 
                                                                          AROWDiagOnlineBinaryClassifier,
                                                                          )
from cortex.learning.ml_model.classifier.sklearn_binary_classifier import (LogisticRegressionSKLEARNBinaryClassifier, 
                                                                           PerceptronSKLEARNBinaryClassifier,
                                                                           PassiveAggressiveSKLEARNBinaryClassifier, 
                                                                           SVSKLEARNBinaryClassifier,
                                                                           )
from cortex.learning.ml_model.classifier import create_classifier_from_factory_configuration
from cortex.tools import Mapping

class Test(unittest.TestCase):

    def test_create_classifier_from_factory_configuration(self):
        # Test parameters
        data = get_test_create_classifier_from_factory_configuration_data()
        
        # Test
        for configuration, expected_result in data:
            actual_result = create_classifier_from_factory_configuration(configuration)
            self.assertTrue(*expected_result._inner_eq(actual_result))

def get_test_create_classifier_from_factory_configuration_data():
    data = []
    
    # Online
    ## PerceptronOnlineBinaryClassifier
    positive_class_label = "POS"
    negative_class_label = "NEG" 
    avg = True
    warm_start = False 
    alpha = 0.234
    configuration = Mapping({"positive_class_label": positive_class_label, 
                             "negative_class_label": negative_class_label, 
                             "avg": avg, "warm_start": warm_start, "alpha": alpha})
    expected_result = PerceptronOnlineBinaryClassifier.from_configuration(configuration)
    configuration = Mapping({"name": "perceptrononlinebinary", "config": configuration})
    data.append((configuration, expected_result))
    
    ## PAOnlineBinaryClassifier
    positive_class_label = "POS"
    negative_class_label = "NEG" 
    avg = True
    warm_start = False 
    C = 2
    configuration = Mapping({"positive_class_label": positive_class_label, 
                             "negative_class_label": negative_class_label, 
                             "avg": avg, "warm_start": warm_start, "C": C, 
                             })
    expected_result = PAOnlineBinaryClassifier.from_configuration(configuration)
    configuration = Mapping({"name": "paonlinebinary", "config": configuration})
    data.append((configuration, expected_result))
    
    ## CWFullOnlineBinaryClassifier
    positive_class_label = "POS"
    negative_class_label = "NEG" 
    eta = 0.8
    linearization = True
    configuration = Mapping({"positive_class_label": positive_class_label, 
                             "negative_class_label": negative_class_label, 
                             "eta": eta, "linearization": linearization,
                             })
    expected_result = CWFullOnlineBinaryClassifier.from_configuration(configuration)
    configuration = Mapping({"name": "cwfullonlinebinary", "config": configuration})
    data.append((configuration, expected_result))
    
    ## CWDiagOnlineBinaryClassifier
    positive_class_label = "POS"
    negative_class_label = "NEG" 
    eta = 0.8
    linearization = True
    configuration = Mapping({"positive_class_label": positive_class_label, 
                             "negative_class_label": negative_class_label, 
                             "eta": eta, "linearization": linearization,
                             })
    expected_result = CWDiagOnlineBinaryClassifier.from_configuration(configuration)
    configuration = Mapping({"name": "cwdiagonlinebinary", "config": configuration})
    data.append((configuration, expected_result))
    
    ## AROWFullOnlineBinaryClassifier
    positive_class_label = "POS"
    negative_class_label = "NEG" 
    r = 1.5
    warm_start = False 
    configuration = Mapping({"positive_class_label": positive_class_label, 
                             "negative_class_label": negative_class_label, 
                             "r": r, "warm_start": warm_start, 
                             })
    expected_result = AROWFullOnlineBinaryClassifier.from_configuration(configuration)
    configuration = Mapping({"name": "arowfullonlinebinary", "config": configuration})
    data.append((configuration, expected_result))
    
    ## AROWDiagOnlineBinaryClassifier
    positive_class_label = "POS"
    negative_class_label = "NEG" 
    r = 1.5
    warm_start = False 
    configuration = Mapping({"positive_class_label": positive_class_label, 
                             "negative_class_label": negative_class_label, 
                             "r": r, "warm_start": warm_start, 
                             })
    expected_result = AROWDiagOnlineBinaryClassifier.from_configuration(configuration)
    configuration = Mapping({"name": "arowdiagonlinebinary", "config": configuration})
    data.append((configuration, expected_result))
    
    # SKLEARN
    ## LogisticRegression
    positive_class_label = "POS"
    negative_class_label = "NEG"
    penalty = "l2"
    dual = True
    tol = 1e-3
    C = 1.5
    fit_intercept = True
    intercept_scaling = 1
    class_weight = "balanced"
    random_state = 84
    solver = "liblinear"
    max_iter = 42
    multi_class = "ovr"
    configuration = Mapping({"witness_field": "witness_value", 
                              "positive_class_label": positive_class_label, 
                              "negative_class_label": negative_class_label,
                              "penalty": penalty,
                              "dual": dual,
                              "tol": tol,
                              "C": C,
                              "fit_intercept": fit_intercept,
                              "intercept_scaling": intercept_scaling,
                              "class_weight": class_weight,
                              "random_state": random_state,
                              "solver": solver,
                              "max_iter": max_iter,
                              "multi_class": multi_class,
                              })
    expected_result = LogisticRegressionSKLEARNBinaryClassifier.from_configuration(configuration)
    configuration = Mapping({"name": "logisticregressionsklearnbinary", "config": configuration})
    data.append((configuration, expected_result))
    
    ## Perceptron
    positive_class_label = "POS"
    negative_class_label = "NEG"
    penalty = "l2"
    alpha = 0.001
    fit_intercept = True
    n_iter = 3
    eta0 = 1.5
    random_state = 84
    class_weight = "balanced"
    shuffle = False
    configuration = Mapping({"witness_field": "witness_value", 
                              "positive_class_label": positive_class_label, 
                              "negative_class_label": negative_class_label,
                              "penalty": penalty,
                              "alpha": alpha,
                              "fit_intercept": fit_intercept,
                              "n_iter": n_iter,
                              "eta0": eta0,
                              "random_state": random_state,
                              "class_weight": class_weight,
                              "shuffle": shuffle,
                              })
    expected_result = PerceptronSKLEARNBinaryClassifier.from_configuration(configuration)
    configuration = Mapping({"name": "perceptronsklearnbinary", "config": configuration})
    data.append((configuration, expected_result))
    
    ## PassiveAggressive
    positive_class_label = "POS"
    negative_class_label = "NEG"
    C = 1.5
    fit_intercept = True
    max_iter = 51
    tol = 1e-3
    loss = "squared_hinge"
    average = 11
    random_state = 84
    class_weight = "balanced"
    shuffle = False
    configuration = Mapping({"witness_field": "witness_value", 
                              "positive_class_label": positive_class_label, 
                              "negative_class_label": negative_class_label,
                              "C": C,
                              "fit_intercept": fit_intercept,
                              "max_iter": max_iter,
                              "tol": tol, 
                              "loss": loss, 
                              "average": average,
                              "random_state": random_state,
                              "class_weight": class_weight,
                              "shuffle": shuffle,
                              })
    expected_result = PassiveAggressiveSKLEARNBinaryClassifier.from_configuration(configuration)
    configuration = Mapping({"name": "pasklearnbinary", "config": configuration})
    data.append((configuration, expected_result))
    
    ## SVM
    positive_class_label = "POS"
    negative_class_label = "NEG"
    C = 1.5
    kernel = "poly"
    degree = 4
    gamma = "auto"
    coef0 = 0.5
    probability = True
    shrinking = False
    cache_size = 20
    max_iter = -1
    tol = 1e-4
    random_state = 84
    class_weight = "balanced"
    configuration = Mapping({"witness_field": "witness_value", 
                              "positive_class_label": positive_class_label, 
                              "negative_class_label": negative_class_label,
                              "C": C,
                              "kernel": kernel,
                              "degree": degree,
                              "gamma": gamma, 
                              "coef0": coef0,
                              "probability": probability,
                              "shrinking": shrinking,
                              "cache_size": cache_size,
                              "max_iter": max_iter,
                              "tol": tol,
                              "random_state": random_state,
                              "class_weight": class_weight,
                              })
    expected_result = SVSKLEARNBinaryClassifier.from_configuration(configuration)
    configuration = Mapping({"name": "svsklearnbinary", "config": configuration})
    data.append((configuration, expected_result))
    
    return data


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()