# -*- coding: utf-8 -*-

import unittest
import numpy

from cortex.tools import Mapping
from cortex.utils.io import TemporaryDirectory
from cortex.learning.ml_model.classifier.sklearn_binary_classifier import (LogisticRegressionSKLEARNBinaryClassifier, 
                                                                           PerceptronSKLEARNBinaryClassifier,
                                                                           PassiveAggressiveSKLEARNBinaryClassifier, 
                                                                           SVSKLEARNBinaryClassifier, 
                                                                           )
from cortex.utils import are_array_equal

POS_CLASS_LABEL = "POS"
NEG_CLASS_LABEL = "NEG"

#@unittest.skip
class TestLogisticRegressionSKLEARNBinaryClassifier(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        attribute_name_expected_result_pairs = []
        
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        penalty = "l2"
        dual = True
        tol = 1e-3
        C = 1.5
        fit_intercept = True
        intercept_scaling = 1
        class_weight = "balanced"
        random_state = 84
        solver = "liblinear"
        max_iter = 42
        multi_class = "ovr"
        configuration = Mapping({"witness_field": "witness_value"})
        
        expected_configuration = Mapping({"witness_field": "witness_value", 
                                          "positive_class_label": positive_class_label, 
                                          "negative_class_label": negative_class_label,
                                          "penalty": penalty,
                                          "dual": dual,
                                          "tol": tol,
                                          "C": C,
                                          "fit_intercept": fit_intercept,
                                          "intercept_scaling": intercept_scaling,
                                          "class_weight": class_weight,
                                          "random_state": random_state,
                                          "solver": solver,
                                          "max_iter": max_iter,
                                          "multi_class": multi_class,
                                          })
        
        args = (positive_class_label, negative_class_label)
        kwargs = {"penalty": penalty,
                  "dual": dual,
                  "tol": tol,
                  "C": C,
                  "fit_intercept": fit_intercept,
                  "intercept_scaling": intercept_scaling,
                  "class_weight": class_weight,
                  "random_state": random_state,
                  "solver": solver,
                  "max_iter": max_iter,
                  "multi_class": multi_class, 
                  "configuration": configuration,
                  }
        attribute_name_expected_result_pairs.append(("penalty", penalty))
        attribute_name_expected_result_pairs.append(("dual", dual))
        attribute_name_expected_result_pairs.append(("tol", tol))
        attribute_name_expected_result_pairs.append(("C", C))
        attribute_name_expected_result_pairs.append(("fit_intercept", fit_intercept))
        attribute_name_expected_result_pairs.append(("intercept_scaling", intercept_scaling))
        attribute_name_expected_result_pairs.append(("class_weight", class_weight))
        attribute_name_expected_result_pairs.append(("random_state", random_state))
        attribute_name_expected_result_pairs.append(("solver", solver))
        attribute_name_expected_result_pairs.append(("max_iter", max_iter))
        attribute_name_expected_result_pairs.append(("multi_class", multi_class))
        attribute_name_expected_result_pairs.append(("configuration", expected_configuration))
        
        # Test
        binary_classifier = LogisticRegressionSKLEARNBinaryClassifier(*args, **kwargs)
        for attribute_name, expected_value in attribute_name_expected_result_pairs:
            actual_value = getattr(binary_classifier, attribute_name)
            self.assertEqual(expected_value, actual_value)
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        penalty = "l2"
        dual = True
        tol = 1e-3
        C = 1.5
        fit_intercept = True
        intercept_scaling = 1
        class_weight = "balanced"
        random_state = 84
        solver = "liblinear"
        max_iter = 42
        multi_class = "ovr"
        
        args = (positive_class_label, negative_class_label)
        kwargs = {"penalty": penalty,
                  "dual": dual,
                  "tol": tol,
                  "C": C,
                  "fit_intercept": fit_intercept,
                  "intercept_scaling": intercept_scaling,
                  "class_weight": class_weight,
                  "random_state": random_state,
                  "solver": solver,
                  "max_iter": max_iter,
                  "multi_class": multi_class, 
                  }
        data = []
        binary_classifier1 = LogisticRegressionSKLEARNBinaryClassifier(*args, **kwargs)
        binary_classifier2 = LogisticRegressionSKLEARNBinaryClassifier(*args, **kwargs)
        data.append(((binary_classifier1, binary_classifier2), True, None))
        
        binary_classifier3 = LogisticRegressionSKLEARNBinaryClassifier(*args, **kwargs)
        binary_classifier3.coef_ = numpy.zeros((1,4), dtype=numpy.float)
        data.append(((binary_classifier1, binary_classifier3), False, "One of the compared element is lacking the 'coef_' attribute"))
        
        # Test
        for (binary_classifier1, binary_classifier2), expected_result, expected_message in data:
            actual_result, actual_message = binary_classifier1._inner_eq(binary_classifier2)
            self.assertEqual(expected_result, actual_result)
            self.assertEqual(expected_message, actual_message)
    
    #@unittest.skip
    def test_from_configuration(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        penalty = "l2"
        dual = True
        tol = 1e-3
        C = 1.5
        fit_intercept = True
        intercept_scaling = 1
        class_weight = "balanced"
        random_state = 84
        solver = "liblinear"
        max_iter = 42
        multi_class = "ovr"
        configuration = Mapping({"witness_field": "witness_value", 
                                          "positive_class_label": positive_class_label, 
                                          "negative_class_label": negative_class_label,
                                          "penalty": penalty,
                                          "dual": dual,
                                          "tol": tol,
                                          "C": C,
                                          "fit_intercept": fit_intercept,
                                          "intercept_scaling": intercept_scaling,
                                          "class_weight": class_weight,
                                          "random_state": random_state,
                                          "solver": solver,
                                          "max_iter": max_iter,
                                          "multi_class": multi_class,
                                          })
        args = (positive_class_label, negative_class_label)
        kwargs = {"penalty": penalty,
                  "dual": dual,
                  "tol": tol,
                  "C": C,
                  "fit_intercept": fit_intercept,
                  "intercept_scaling": intercept_scaling,
                  "class_weight": class_weight,
                  "random_state": random_state,
                  "solver": solver,
                  "max_iter": max_iter,
                  "multi_class": multi_class, 
                  "configuration": Mapping({"witness_field": "witness_value"}),
                  }
        expected_result = LogisticRegressionSKLEARNBinaryClassifier(*args, **kwargs)
        
        # Test
        actual_result = LogisticRegressionSKLEARNBinaryClassifier.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = LogisticRegressionSKLEARNBinaryClassifier
        positive_class_label = "test_pos"
        negative_class_label = "test_neg"
        penalty = "l1"
        dual = True
        tol = 0.05
        C = 2.5
        fit_intercept = False
        intercept_scaling = 0.5
        class_weight = "balanced"
        random_state = 43
        solver = "saga"
        max_iter = 45
        multi_class = "multinominal"
        args = (positive_class_label, negative_class_label)
        kwargs = {"penalty": penalty,
                   "dual": dual,
                   "tol": tol,
                   "C": C,
                   "fit_intercept": fit_intercept,
                   "intercept_scaling": intercept_scaling,
                   "class_weight": class_weight,
                   "random_state": random_state,
                   "solver": solver,
                   "max_iter": max_iter,
                   "multi_class": multi_class,
                  }
        expected_result = Mapping({"positive_class_label": positive_class_label,
                                   "negative_class_label": negative_class_label,
                                   "penalty": penalty,
                                   "dual": dual,
                                   "tol": tol,
                                   "C": C,
                                   "fit_intercept": fit_intercept,
                                   "intercept_scaling": intercept_scaling,
                                   "class_weight": class_weight,
                                   "random_state": random_state,
                                   "solver": solver,
                                   "max_iter": max_iter,
                                   "multi_class": multi_class,
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip
    def test_save_load_model(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        penalty = "l2"
        dual = True
        tol = 1e-3
        C = 1.5
        fit_intercept = True
        intercept_scaling = 1
        class_weight = "balanced"
        random_state = 84
        solver = "liblinear"
        max_iter = 42
        multi_class = "ovr"
        args = (positive_class_label, negative_class_label)
        kwargs = {"penalty": penalty,
                  "dual": dual,
                  "tol": tol,
                  "C": C,
                  "fit_intercept": fit_intercept,
                  "intercept_scaling": intercept_scaling,
                  "class_weight": class_weight,
                  "random_state": random_state,
                  "solver": solver,
                  "max_iter": max_iter,
                  "multi_class": multi_class, 
                  }
        binary_classifier = LogisticRegressionSKLEARNBinaryClassifier(*args, **kwargs)
        X = numpy.array([[0.,1.,0.,1.],[0.,1.,1.,0.]], dtype=numpy.float)
        Y = numpy.array((POS_CLASS_LABEL, NEG_CLASS_LABEL))
        
        def _learn_fct(binary_classifier):
            binary_classifier.fit(X, Y)
            expected_arrays = {"coef_": numpy.array([[0.0, -7.15796335e-05, -5.49070327e-01, 5.48998748e-01]], dtype=numpy.float), 
                               "intercept_": numpy.array([-7.15796335e-05], dtype=numpy.float)}
            actual_arrays = {"coef_": binary_classifier.coef_, 
                             "intercept_": binary_classifier.intercept_}
            self.assertEqual(expected_arrays.keys(), actual_arrays.keys())
            for attribute_name in actual_arrays.keys():
                expected_array_value = expected_arrays[attribute_name]
                actual_array_value = actual_arrays[attribute_name]
                print(actual_array_value)
                self.assertTrue(are_array_equal(expected_array_value, actual_array_value), attribute_name)
            expected_arrays = actual_arrays
            return expected_arrays
        
        # Test
        with TemporaryDirectory() as temp_dir:
            folder_path = temp_dir.temporary_folder_path
            with self.assertRaises(ValueError):
                binary_classifier.save_model(folder_path)
            expected_arrays = _learn_fct(binary_classifier)
            binary_classifier.save_model(folder_path)
            new_binary_classifier = LogisticRegressionSKLEARNBinaryClassifier(*args, **kwargs)
            new_binary_classifier.load_model(folder_path)
        actual_arrays = {"coef_": new_binary_classifier.coef_, 
                         "intercept_": new_binary_classifier.intercept_}
        self.assertEqual(expected_arrays.keys(), actual_arrays.keys())
        for attribute_name in actual_arrays.keys():
            expected_array_value = expected_arrays[attribute_name]
            actual_array_value = actual_arrays[attribute_name]
            self.assertTrue(are_array_equal(expected_array_value, actual_array_value), attribute_name)
    
    #@unittest.skip
    def test_can_predict(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        penalty = "l2"
        dual = True
        tol = 1e-3
        C = 1.5
        fit_intercept = True
        intercept_scaling = 1
        class_weight = "balanced"
        random_state = 84
        solver = "liblinear"
        max_iter = 42
        multi_class = "ovr"
        args = (positive_class_label, negative_class_label)
        kwargs = {"penalty": penalty,
                  "dual": dual,
                  "tol": tol,
                  "C": C,
                  "fit_intercept": fit_intercept,
                  "intercept_scaling": intercept_scaling,
                  "class_weight": class_weight,
                  "random_state": random_state,
                  "solver": solver,
                  "max_iter": max_iter,
                  "multi_class": multi_class, 
                  }
                                                             
        X = numpy.array([[0.,1.,0.,1.],[0.,1.,1.,0.]], dtype=numpy.float)
        Y = numpy.array((POS_CLASS_LABEL, NEG_CLASS_LABEL))
        
        # Test
        binary_classifier1 = LogisticRegressionSKLEARNBinaryClassifier(*args, **kwargs)
        self.assertFalse(binary_classifier1.can_predict())
        binary_classifier1.fit(X, Y)
        self.assertTrue(binary_classifier1.can_predict())
    
    #@unittest.skip
    def test_predict_decision_function_class_scores(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        penalty = "l2"
        dual = True
        tol = 1e-3
        C = 1.5
        fit_intercept = True
        intercept_scaling = 1
        class_weight = "balanced"
        random_state = 84
        solver = "liblinear"
        max_iter = 42
        multi_class = "ovr"
        args = (positive_class_label, negative_class_label)
        kwargs = {"penalty": penalty,
                  "dual": dual,
                  "tol": tol,
                  "C": C,
                  "fit_intercept": fit_intercept,
                  "intercept_scaling": intercept_scaling,
                  "class_weight": class_weight,
                  "random_state": random_state,
                  "solver": solver,
                  "max_iter": max_iter,
                  "multi_class": multi_class, 
                  }
        binary_classifier = LogisticRegressionSKLEARNBinaryClassifier(*args, **kwargs)
                                                             
        X_train = numpy.array([[0.,1.,0.,1.],[0.,1.,1.,0.]], dtype=numpy.float)
        Y_train = numpy.array((POS_CLASS_LABEL, NEG_CLASS_LABEL))
        binary_classifier.fit(X_train, Y_train)
        # coef_ = numpy.array([[0.0, -7.15796335e-05, -5.49070327e-01, 5.48998748e-01]], dtype=numpy.float)
        # intercept_ = numpy.array([-7.15796335e-05], dtype=numpy.float)
        
        X = X_train
        score1 = -7.15796335e-05 + 5.48998748e-01 - 7.15796335e-05
        score2 = -7.15796335e-05 - 5.49070327e-01 - 7.15796335e-05
        expected_scores = numpy.array([[-score1, score1], [-score2, score2]], dtype=numpy.float)
        
        # Test
        actual_scores = binary_classifier.predict_decision_function_class_scores(X)
        self.assertTrue(are_array_equal(expected_scores, actual_scores))
    
    #@unittest.skip
    def test_predict_class_scores(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        penalty = "l2"
        dual = True
        tol = 1e-3
        C = 1.5
        fit_intercept = True
        intercept_scaling = 1
        class_weight = "balanced"
        random_state = 84
        solver = "liblinear"
        max_iter = 42
        multi_class = "ovr"
        args = (positive_class_label, negative_class_label)
        kwargs = {"penalty": penalty,
                  "dual": dual,
                  "tol": tol,
                  "C": C,
                  "fit_intercept": fit_intercept,
                  "intercept_scaling": intercept_scaling,
                  "class_weight": class_weight,
                  "random_state": random_state,
                  "solver": solver,
                  "max_iter": max_iter,
                  "multi_class": multi_class, 
                  }
        binary_classifier = LogisticRegressionSKLEARNBinaryClassifier(*args, **kwargs)
                                                             
        X_train = numpy.array([[0.,1.,0.,1.],[0.,1.,1.,0.]], dtype=numpy.float)
        Y_train = numpy.array((POS_CLASS_LABEL, NEG_CLASS_LABEL))
        binary_classifier.fit(X_train, Y_train)
        # coef_ = numpy.array([[0.0, -7.15796335e-05, -5.49070327e-01, 5.48998748e-01]], dtype=numpy.float)
        # intercept_ = numpy.array([-7.15796335e-05], dtype=numpy.float)
        
        X = X_train
        score1 = -7.15796335e-05 + 5.48998748e-01 - 7.15796335e-05
        score2 = -7.15796335e-05 - 5.49070327e-01 - 7.15796335e-05
        proba11 = 1 / (numpy.exp(-score1) + 1)
        proba12 = 1 - proba11
        proba21 = 1 / (numpy.exp(-score2) + 1)
        proba22 = 1 - proba21
        expected_scores = numpy.array([[proba12, proba11], [proba22, proba21]], dtype=numpy.float)
        
        # Test
        actual_scores = binary_classifier.predict_class_scores(X)
        self.assertTrue(are_array_equal(expected_scores, actual_scores))




#@unittest.skip
class TestPerceptronSKLEARNBinaryClassifier(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        attribute_name_expected_result_pairs = []
        
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        penalty = "l2"
        alpha = 0.001
        max_iter = 51
        tol = 1e-3
        fit_intercept = True
        eta0 = 1.5
        random_state = 84
        class_weight = "balanced"
        shuffle = False
        configuration = Mapping({"witness_field": "witness_value"})
        
        expected_configuration = Mapping({"witness_field": "witness_value", 
                                          "positive_class_label": positive_class_label, 
                                          "negative_class_label": negative_class_label,
                                          "penalty": penalty,
                                          "alpha": alpha,
                                          "max_iter": max_iter,
                                          "tol": tol, 
                                          "fit_intercept": fit_intercept,
                                          "eta0": eta0,
                                          "random_state": random_state,
                                          "class_weight": class_weight,
                                          "shuffle": shuffle,
                                          })
        
        args = (positive_class_label, negative_class_label)
        kwargs = {"penalty": penalty,
                  "alpha": alpha,
                  "max_iter": max_iter, 
                  "tol": tol, 
                  "fit_intercept": fit_intercept,
                  "eta0": eta0,
                  "random_state": random_state,
                  "class_weight": class_weight,
                  "shuffle": shuffle,
                  "configuration": configuration,
                  }
        attribute_name_expected_result_pairs.append(("penalty", penalty))
        attribute_name_expected_result_pairs.append(("alpha", alpha))
        attribute_name_expected_result_pairs.append(("max_iter", max_iter))
        attribute_name_expected_result_pairs.append(("tol", tol))
        attribute_name_expected_result_pairs.append(("fit_intercept", fit_intercept))
        attribute_name_expected_result_pairs.append(("eta0", eta0))
        attribute_name_expected_result_pairs.append(("random_state", random_state))
        attribute_name_expected_result_pairs.append(("class_weight", class_weight))
        attribute_name_expected_result_pairs.append(("shuffle", shuffle))
        attribute_name_expected_result_pairs.append(("configuration", expected_configuration))
        
        # Test
        binary_classifier = PerceptronSKLEARNBinaryClassifier(*args, **kwargs)
        for attribute_name, expected_value in attribute_name_expected_result_pairs:
            actual_value = getattr(binary_classifier, attribute_name)
            self.assertEqual(expected_value, actual_value)
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        penalty = "l2"
        alpha = 0.001
        max_iter = 51
        tol = 1e-3
        fit_intercept = True
        eta0 = 1.5
        random_state = 84
        class_weight = "balanced"
        shuffle = False
        configuration = Mapping({"witness_field": "witness_value"})
        
        args = (positive_class_label, negative_class_label)
        kwargs = {"penalty": penalty,
                  "alpha": alpha,
                  "max_iter": max_iter, 
                  "tol": tol,
                  "fit_intercept": fit_intercept,
                  "eta0": eta0,
                  "random_state": random_state,
                  "class_weight": class_weight,
                  "shuffle": shuffle,
                  "configuration": configuration,
                  }
        data = []
        binary_classifier1 = PerceptronSKLEARNBinaryClassifier(*args, **kwargs)
        binary_classifier2 = PerceptronSKLEARNBinaryClassifier(*args, **kwargs)
        data.append(((binary_classifier1, binary_classifier2), True, None))
        
        binary_classifier3 = PerceptronSKLEARNBinaryClassifier(*args, **kwargs)
        binary_classifier3.coef_ = numpy.zeros((1,4), dtype=numpy.float)
        data.append(((binary_classifier1, binary_classifier3), False, "One of the compared element is lacking the 'coef_' attribute"))
        
        # Test
        for (binary_classifier1, binary_classifier2), expected_result, expected_message in data:
            actual_result, actual_message = binary_classifier1._inner_eq(binary_classifier2)
            self.assertEqual(expected_result, actual_result)
            self.assertEqual(expected_message, actual_message)
    
    #@unittest.skip
    def test_from_configuration(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        penalty = "l2"
        alpha = 0.001
        max_iter = 51
        tol = 1e-3
        fit_intercept = True
        eta0 = 1.5
        random_state = 84
        class_weight = "balanced"
        shuffle = False
        
        configuration = Mapping({"witness_field": "witness_value", 
                                  "positive_class_label": positive_class_label, 
                                  "negative_class_label": negative_class_label,
                                  "penalty": penalty,
                                  "alpha": alpha,
                                  "max_iter": max_iter, 
                                  "tol": tol,
                                  "fit_intercept": fit_intercept,
                                  "eta0": eta0,
                                  "random_state": random_state,
                                  "class_weight": class_weight,
                                  "shuffle": shuffle,
                                  })
        
        args = (positive_class_label, negative_class_label)
        kwargs = {"penalty": penalty,
                  "alpha": alpha,
                  "max_iter": max_iter, 
                  "tol": tol,
                  "fit_intercept": fit_intercept,
                  "eta0": eta0,
                  "random_state": random_state,
                  "class_weight": class_weight,
                  "shuffle": shuffle,
                  "configuration": Mapping({"witness_field": "witness_value"}),
                  }
        expected_result = PerceptronSKLEARNBinaryClassifier(*args, **kwargs)
        
        # Test
        actual_result = PerceptronSKLEARNBinaryClassifier.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = PerceptronSKLEARNBinaryClassifier
        positive_class_label = "test_pos"
        negative_class_label = "test_neg"
        penalty = "l1"
        alpha = 0.02
        max_iter = 57
        tol = 2e-3
        fit_intercept = False
        eta0 = 1.5
        random_state = 41
        class_weight = "balanced"
        shuffle = False
        args = (positive_class_label, negative_class_label)
        kwargs = {"penalty": penalty,
                  "alpha": alpha,
                  "max_iter": max_iter,
                  "tol": tol,
                  "fit_intercept": fit_intercept,
                  "eta0": eta0,
                  "random_state": random_state,
                  "class_weight": class_weight,
                  "shuffle": shuffle,
                  }
        expected_result = Mapping({"positive_class_label": positive_class_label,
                                   "negative_class_label": negative_class_label,
                                   "penalty": penalty,
                                   "alpha": alpha,
                                    "max_iter": max_iter,
                                    "tol": tol,
                                    "fit_intercept": fit_intercept,
                                    "eta0": eta0,
                                    "random_state": random_state,
                                    "class_weight": class_weight,
                                    "shuffle": shuffle,
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip
    def test_predict_class_scores(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        penalty = "l2"
        alpha = 0.001
        max_iter = 51
        tol = 1e-3
        fit_intercept = True
        eta0 = 1.5
        random_state = 84
        class_weight = "balanced"
        shuffle = False
        args = (positive_class_label, negative_class_label)
        kwargs = {"penalty": penalty,
                  "alpha": alpha,
                  "max_iter": max_iter, 
                  "tol": tol,
                  "fit_intercept": fit_intercept,
                  "eta0": eta0,
                  "random_state": random_state,
                  "class_weight": class_weight,
                  "shuffle": shuffle,
                  "configuration": Mapping({"witness_field": "witness_value"}),
                  }
        binary_classifier = PerceptronSKLEARNBinaryClassifier(*args, **kwargs)
                                                             
        X_train = numpy.array([[0.,1.,0.,1.],[0.,1.,1.,0.]], dtype=numpy.float)
        Y_train = numpy.array((POS_CLASS_LABEL, NEG_CLASS_LABEL))
        binary_classifier.fit(X_train, Y_train)
        binary_classifier.coef_ = numpy.array([[0.0, -7.15796335e-05, -5.49070327e-01, 5.48998748e-01]], dtype=numpy.float)
        binary_classifier.intercept_ = numpy.array([-7.15796335e-05], dtype=numpy.float)
        
        X = X_train
        score1 = -7.15796335e-05 + 5.48998748e-01 - 7.15796335e-05
        score2 = -7.15796335e-05 - 5.49070327e-01 - 7.15796335e-05
        expected_scores = numpy.array([[-score1, score1], [-score2, score2]], dtype=numpy.float)
        
        # Test
        actual_scores = binary_classifier.predict_class_scores(X)
        self.assertTrue(are_array_equal(expected_scores, actual_scores))



#@unittest.skip
class TestPassiveAggressiveSKLEARNBinaryClassifier(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        attribute_name_expected_result_pairs = []
        
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        C = 1.5
        fit_intercept = True
        max_iter = 51
        tol = 1e-3
        loss = "squared_hinge"
        average = 11
        random_state = 84
        class_weight = "balanced"
        shuffle = False
        configuration = Mapping({"witness_field": "witness_value"})
        
        expected_configuration = Mapping({"witness_field": "witness_value", 
                                          "positive_class_label": positive_class_label, 
                                          "negative_class_label": negative_class_label,
                                          "C": C,
                                          "fit_intercept": fit_intercept,
                                          "max_iter": max_iter,
                                          "tol": tol, 
                                          "loss": loss, 
                                          "average": average,
                                          "random_state": random_state,
                                          "class_weight": class_weight,
                                          "shuffle": shuffle,
                                          })
        
        args = (positive_class_label, negative_class_label)
        kwargs = {"C": C,
                  "fit_intercept": fit_intercept,
                  "max_iter": max_iter, 
                  "tol": tol, 
                  "loss": loss, 
                  "average": average,
                  "random_state": random_state,
                  "class_weight": class_weight,
                  "shuffle": shuffle,
                  "configuration": configuration,
                  }
        attribute_name_expected_result_pairs.append(("C", C))
        attribute_name_expected_result_pairs.append(("fit_intercept", fit_intercept))
        attribute_name_expected_result_pairs.append(("max_iter", max_iter))
        attribute_name_expected_result_pairs.append(("tol", tol))
        attribute_name_expected_result_pairs.append(("loss", loss))
        attribute_name_expected_result_pairs.append(("average", average))
        attribute_name_expected_result_pairs.append(("random_state", random_state))
        attribute_name_expected_result_pairs.append(("class_weight", class_weight))
        attribute_name_expected_result_pairs.append(("shuffle", shuffle))
        attribute_name_expected_result_pairs.append(("configuration", expected_configuration))
        
        # Test
        binary_classifier = PassiveAggressiveSKLEARNBinaryClassifier(*args, **kwargs)
        for attribute_name, expected_value in attribute_name_expected_result_pairs:
            actual_value = getattr(binary_classifier, attribute_name)
            self.assertEqual(expected_value, actual_value)
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        C = 1.5
        fit_intercept = True
        max_iter = 51
        tol = 1e-3
        loss = "squared_hinge"
        average = 11
        random_state = 84
        class_weight = "balanced"
        shuffle = False
        configuration = Mapping({"witness_field": "witness_value"})
        
        args = (positive_class_label, negative_class_label)
        kwargs = {"C": C,
                  "fit_intercept": fit_intercept,
                  "max_iter": max_iter, 
                  "tol": tol, 
                  "loss": loss, 
                  "average": average,
                  "random_state": random_state,
                  "class_weight": class_weight,
                  "shuffle": shuffle,
                  "configuration": configuration,
                  }
        data = []
        binary_classifier1 = PassiveAggressiveSKLEARNBinaryClassifier(*args, **kwargs)
        binary_classifier2 = PassiveAggressiveSKLEARNBinaryClassifier(*args, **kwargs)
        data.append(((binary_classifier1, binary_classifier2), True, None))
        
        binary_classifier3 = PassiveAggressiveSKLEARNBinaryClassifier(*args, **kwargs)
        binary_classifier3.coef_ = numpy.zeros((1,4), dtype=numpy.float)
        data.append(((binary_classifier1, binary_classifier3), False, "One of the compared element is lacking the 'coef_' attribute"))
        
        # Test
        for (binary_classifier1, binary_classifier2), expected_result, expected_message in data:
            actual_result, actual_message = binary_classifier1._inner_eq(binary_classifier2)
            self.assertEqual(expected_result, actual_result)
            self.assertEqual(expected_message, actual_message)
    
    #@unittest.skip
    def test_from_configuration(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        C = 1.5
        fit_intercept = True
        max_iter = 51
        tol = 1e-3
        loss = "squared_hinge"
        average = 11
        random_state = 84
        class_weight = "balanced"
        shuffle = False
        
        configuration = Mapping({"witness_field": "witness_value", 
                                  "positive_class_label": positive_class_label, 
                                  "negative_class_label": negative_class_label,
                                  "C": C,
                                  "fit_intercept": fit_intercept,
                                  "max_iter": max_iter,
                                  "tol": tol, 
                                  "loss": loss, 
                                  "average": average,
                                  "random_state": random_state,
                                  "class_weight": class_weight,
                                  "shuffle": shuffle,
                                  })
        
        args = (positive_class_label, negative_class_label)
        kwargs = {"C": C,
                  "fit_intercept": fit_intercept,
                  "max_iter": max_iter, 
                  "tol": tol, 
                  "loss": loss, 
                  "average": average,
                  "random_state": random_state,
                  "class_weight": class_weight,
                  "shuffle": shuffle,
                  "configuration": Mapping({"witness_field": "witness_value"}),
                  }
        expected_result = PassiveAggressiveSKLEARNBinaryClassifier(*args, **kwargs)
        
        # Test
        actual_result = PassiveAggressiveSKLEARNBinaryClassifier.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = PassiveAggressiveSKLEARNBinaryClassifier
        positive_class_label = "test_pos"
        negative_class_label = "test_neg"
        C = 1.5
        fit_intercept = False
        max_iter = 47
        tol = 2e-2
        loss = "squared_hinge"
        average = True
        random_state = 9
        class_weight = "balanced"
        shuffle = False
        args = (positive_class_label, negative_class_label)
        kwargs = {"C": C,
                  "fit_intercept": fit_intercept,
                  "max_iter": max_iter,
                  "tol": tol,
                  "loss": loss,
                  "average": average,
                  "random_state": random_state,
                  "class_weight": class_weight,
                  "shuffle": shuffle,
                  }
        expected_result = Mapping({"positive_class_label": positive_class_label,
                                   "negative_class_label": negative_class_label,
                                   "C": C,
                                   "fit_intercept": fit_intercept,
                                   "max_iter": max_iter,
                                   "tol": tol,
                                   "loss": loss,
                                   "average": average,
                                   "random_state": random_state,
                                   "class_weight": class_weight,
                                   "shuffle": shuffle,
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip
    def test_predict_class_scores(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        C = 1.5
        fit_intercept = True
        max_iter = 51
        tol = 1e-3
        loss = "squared_hinge"
        average = 11
        random_state = 84
        class_weight = "balanced"
        shuffle = False
        args = (positive_class_label, negative_class_label)
        kwargs = {"C": C,
                  "fit_intercept": fit_intercept,
                  "max_iter": max_iter, 
                  "tol": tol, 
                  "loss": loss, 
                  "average": average,
                  "random_state": random_state,
                  "class_weight": class_weight,
                  "shuffle": shuffle,
                  "configuration": Mapping({"witness_field": "witness_value"}),
                  }
        binary_classifier = PassiveAggressiveSKLEARNBinaryClassifier(*args, **kwargs)
                                                             
        X_train = numpy.array([[0.,1.,0.,1.],[0.,1.,1.,0.]], dtype=numpy.float)
        Y_train = numpy.array((POS_CLASS_LABEL, NEG_CLASS_LABEL))
        binary_classifier.fit(X_train, Y_train)
        binary_classifier.coef_ = numpy.array([[0.0, -7.15796335e-05, -5.49070327e-01, 5.48998748e-01]], dtype=numpy.float)
        binary_classifier.intercept_ = numpy.array([-7.15796335e-05], dtype=numpy.float)
        
        X = X_train
        score1 = -7.15796335e-05 + 5.48998748e-01 - 7.15796335e-05
        score2 = -7.15796335e-05 - 5.49070327e-01 - 7.15796335e-05
        expected_scores = numpy.array([[-score1, score1], [-score2, score2]], dtype=numpy.float)
        
        # Test
        actual_scores = binary_classifier.predict_class_scores(X)
        self.assertTrue(are_array_equal(expected_scores, actual_scores))



#@unittest.skip
class TestSVSKLEARNBinaryClassifier(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        attribute_name_expected_result_pairs = []
        
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        C = 1.5
        kernel = "poly"
        degree = 4
        gamma = "auto"
        coef0 = 0.5
        probability = True
        shrinking = False
        cache_size = 20
        max_iter = -1
        tol = 1e-4
        random_state = 84
        class_weight = "balanced"
        configuration = Mapping({"witness_field": "witness_value"})
        
        expected_configuration = Mapping({"witness_field": "witness_value", 
                                          "positive_class_label": positive_class_label, 
                                          "negative_class_label": negative_class_label,
                                          "C": C,
                                          "kernel": kernel,
                                          "degree": degree,
                                          "gamma": gamma, 
                                          "coef0": coef0,
                                          "probability": probability,
                                          "shrinking": shrinking,
                                          "cache_size": cache_size,
                                          "max_iter": max_iter,
                                          "tol": tol,
                                          "random_state": random_state,
                                          "class_weight": class_weight,
                                          })
        
        args = (positive_class_label, negative_class_label)
        kwargs = {"C": C,
                  "kernel": kernel,
                  "degree": degree,
                  "gamma": gamma, 
                  "coef0": coef0,
                  "probability": probability,
                  "shrinking": shrinking,
                  "cache_size": cache_size,
                  "max_iter": max_iter, 
                  "tol": tol, 
                  "random_state": random_state,
                  "class_weight": class_weight,
                  "configuration": configuration,
                  }
        attribute_name_expected_result_pairs.append(("C", C))
        attribute_name_expected_result_pairs.append(("kernel", kernel))
        attribute_name_expected_result_pairs.append(("degree", degree))
        attribute_name_expected_result_pairs.append(("gamma", gamma))
        attribute_name_expected_result_pairs.append(("coef0", coef0))
        attribute_name_expected_result_pairs.append(("probability", probability))
        attribute_name_expected_result_pairs.append(("shrinking", shrinking))
        attribute_name_expected_result_pairs.append(("cache_size", cache_size))
        attribute_name_expected_result_pairs.append(("max_iter", max_iter))
        attribute_name_expected_result_pairs.append(("tol", tol))
        attribute_name_expected_result_pairs.append(("random_state", random_state))
        attribute_name_expected_result_pairs.append(("class_weight", class_weight))
        attribute_name_expected_result_pairs.append(("configuration", expected_configuration))
        
        # Test
        binary_classifier = SVSKLEARNBinaryClassifier(*args, **kwargs)
        for attribute_name, expected_value in attribute_name_expected_result_pairs:
            actual_value = getattr(binary_classifier, attribute_name)
            self.assertEqual(expected_value, actual_value)
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        C = 1.5
        kernel = "poly"
        degree = 4
        gamma = "auto"
        coef0 = 0.5
        probability = True
        shrinking = False
        cache_size = 20
        max_iter = -1
        tol = 1e-4
        random_state = 84
        class_weight = "balanced"
        configuration = Mapping({"witness_field": "witness_value"})
        
        args = (positive_class_label, negative_class_label)
        kwargs = {"C": C,
                  "kernel": kernel,
                  "degree": degree,
                  "gamma": gamma, 
                  "coef0": coef0,
                  "probability": probability,
                  "shrinking": shrinking,
                  "cache_size": cache_size,
                  "max_iter": max_iter, 
                  "tol": tol, 
                  "random_state": random_state,
                  "class_weight": class_weight,
                  "configuration": configuration,
                  }
        data = []
        binary_classifier1 = SVSKLEARNBinaryClassifier(*args, **kwargs)
        binary_classifier2 = SVSKLEARNBinaryClassifier(*args, **kwargs)
        data.append(((binary_classifier1, binary_classifier2), True, None))
        
        binary_classifier3 = SVSKLEARNBinaryClassifier(*args, **kwargs)
        binary_classifier3.intercept_ = numpy.zeros((1,1), dtype=numpy.float)
        data.append(((binary_classifier1, binary_classifier3), False, "One of the compared element is lacking the 'intercept_' attribute"))
        
        # Test
        for (binary_classifier1, binary_classifier2), expected_result, expected_message in data:
            actual_result, actual_message = binary_classifier1._inner_eq(binary_classifier2)
            self.assertEqual(expected_result, actual_result)
            self.assertEqual(expected_message, actual_message)
    
    #@unittest.skip
    def test_from_configuration(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        C = 1.5
        kernel = "poly"
        degree = 4
        gamma = "auto"
        coef0 = 0.5
        probability = True
        shrinking = False
        cache_size = 20
        max_iter = -1
        tol = 1e-4
        random_state = 84
        class_weight = "balanced"
        
        configuration = Mapping({"witness_field": "witness_value", 
                                  "positive_class_label": positive_class_label, 
                                  "negative_class_label": negative_class_label,
                                  "C": C,
                                  "kernel": kernel,
                                  "degree": degree,
                                  "gamma": gamma, 
                                  "coef0": coef0,
                                  "probability": probability,
                                  "shrinking": shrinking,
                                  "cache_size": cache_size,
                                  "max_iter": max_iter,
                                  "tol": tol,
                                  "random_state": random_state,
                                  "class_weight": class_weight,
                                  })
        
        args = (positive_class_label, negative_class_label)
        kwargs = {"C": C,
                  "kernel": kernel,
                  "degree": degree,
                  "gamma": gamma, 
                  "coef0": coef0,
                  "probability": probability,
                  "shrinking": shrinking,
                  "cache_size": cache_size,
                  "max_iter": max_iter, 
                  "tol": tol, 
                  "random_state": random_state,
                  "class_weight": class_weight,
                  "configuration": Mapping({"witness_field": "witness_value"}),
                  }
        expected_result = SVSKLEARNBinaryClassifier(*args, **kwargs)
        
        # Test
        actual_result = SVSKLEARNBinaryClassifier.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = SVSKLEARNBinaryClassifier
        positive_class_label = "test_pos"
        negative_class_label = "test_neg"
        C = 1.5
        kernel = "poly"
        degree = 4
        gamma = "auto"
        coef0 = 0.5
        probability = True
        shrinking = False
        cache_size = 20
        max_iter = -1
        tol = 1e-4
        random_state = 84
        class_weight = "balanced"
        args = (positive_class_label, negative_class_label)
        kwargs = {"C": C,
                  "kernel": kernel,
                  "degree": degree,
                  "gamma": gamma, 
                  "coef0": coef0,
                  "probability": probability,
                  "shrinking": shrinking,
                  "cache_size": cache_size,
                  "max_iter": max_iter, 
                  "tol": tol, 
                  "random_state": random_state,
                  "class_weight": class_weight,
                  }
        expected_result = Mapping({"positive_class_label": positive_class_label,
                                   "negative_class_label": negative_class_label,
                                   "C": C,
                                   "kernel": kernel,
                                   "degree": degree,
                                   "gamma": gamma, 
                                   "coef0": coef0,
                                   "probability": probability,
                                   "shrinking": shrinking,
                                   "cache_size": cache_size,
                                   "max_iter": max_iter, 
                                   "tol": tol, 
                                   "random_state": random_state,
                                   "class_weight": class_weight,
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip
    def test_can_predict(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        C = 1.5
        kernel = "poly"
        degree = 4
        gamma = "auto"
        coef0 = 0.5
        probability = True
        shrinking = False
        cache_size = 20
        max_iter = -1
        tol = 1e-4
        random_state = 84
        class_weight = "balanced"
        args = (positive_class_label, negative_class_label)
        kwargs = {"C": C,
                  "kernel": kernel,
                  "degree": degree,
                  "gamma": gamma, 
                  "coef0": coef0,
                  "probability": probability,
                  "shrinking": shrinking,
                  "cache_size": cache_size,
                  "max_iter": max_iter, 
                  "tol": tol, 
                  "random_state": random_state,
                  "class_weight": class_weight,
                  }
        ## Unfit
        binary_classifier1 = SVSKLEARNBinaryClassifier(*args, **kwargs)
        ## Fit
        binary_classifier = SVSKLEARNBinaryClassifier(*args, **kwargs)
        binary_classifier.classes_ = numpy.array([POS_CLASS_LABEL, NEG_CLASS_LABEL])
        binary_classifier.support_ = numpy.array([1, 0], dtype=numpy.int32)
        binary_classifier.n_support_ = numpy.array([1, 1], dtype=numpy.int32)
        binary_classifier.support_vectors_ = numpy.array([[0., 1., 1., 0.], [0., 1., 0., 1.]], dtype=numpy.float)
        binary_classifier._dual_coef_ = numpy.array([[-1.46285714,  1.46285714]], dtype=numpy.float)
        binary_classifier._intercept_ = numpy.array([-0.], dtype=numpy.float)
        binary_classifier._sparse = False
        binary_classifier.shape_fit_ = (2,4)
        binary_classifier.probA_ = numpy.array([], dtype=numpy.float64)
        binary_classifier.probB_ = numpy.array([], dtype=numpy.float64)
        binary_classifier._gamma = 1/3
        binary_classifier.class_weight_ = numpy.array([1., 1.], dtype=numpy.float)
        binary_classifier.dual_coef_ = numpy.array([[-1.46285714,  1.46285714]], dtype=numpy.float)
        binary_classifier.fit_status_ = 0
        binary_classifier.intercept_ = numpy.array([-0.], dtype=numpy.float)                   
        binary_classifier2 = binary_classifier
        
        # Test
        ## Unfit
        expected_result = False
        actual_result = binary_classifier1.can_predict()
        self.assertEqual(expected_result, actual_result)
        ## Fit
        expected_result = True
        actual_result = binary_classifier2.can_predict()
        self.assertEqual(expected_result, actual_result)
        
    #@unittest.skip
    def test_predict_class_scores(self):
        # Test parameters
        positive_class_label = POS_CLASS_LABEL
        negative_class_label = NEG_CLASS_LABEL
        C = 1.5
        kernel = "poly"
        degree = 4
        gamma = "auto"
        coef0 = 0.5
        probability = True
        shrinking = False
        cache_size = 20
        max_iter = -1
        tol = 1e-4
        random_state = 84
        class_weight = "balanced"
        args = (positive_class_label, negative_class_label)
        kwargs = {"C": C,
                  "kernel": kernel,
                  "degree": degree,
                  "gamma": gamma, 
                  "coef0": coef0,
                  "probability": probability,
                  "shrinking": shrinking,
                  "cache_size": cache_size,
                  "max_iter": max_iter, 
                  "tol": tol, 
                  "random_state": random_state,
                  "class_weight": class_weight,
                  }
        binary_classifier = SVSKLEARNBinaryClassifier(*args, **kwargs)
        binary_classifier.classes_ = numpy.array([POS_CLASS_LABEL, NEG_CLASS_LABEL])
        binary_classifier.support_ = numpy.array([1, 0], dtype=numpy.int32)
        binary_classifier.n_support_ = numpy.array([1, 1], dtype=numpy.int32)
        binary_classifier.support_vectors_ = numpy.array([[0., 1., 1., 0.], [0., 1., 0., 1.]], dtype=numpy.float)
        binary_classifier._dual_coef_ = numpy.array([[-1.46285714,  1.46285714]], dtype=numpy.float)
        binary_classifier._intercept_ = numpy.array([-0.], dtype=numpy.float)
        binary_classifier._sparse = False
        binary_classifier.shape_fit_ = (2,4)
        binary_classifier.probA_ = numpy.array([], dtype=numpy.float64)
        binary_classifier.probB_ = numpy.array([], dtype=numpy.float64)
        binary_classifier._gamma = 1/3
        
        X = numpy.array([[0.,1.,0.,1.],[0.,1.,1.,0.]], dtype=numpy.float)
        expected_scores = numpy.array([[ 2.00465608, -2.00465608], [-2.00465608,  2.00465608]], dtype=numpy.float)
        
        # Test
        actual_scores = binary_classifier.predict_class_scores(X)
        self.assertTrue(are_array_equal(expected_scores, actual_scores))




if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()