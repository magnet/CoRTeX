# -*- coding: utf-8 -*-

import unittest

from cortex.learning.model_interface import (create_model_interface_from_factory_configuration, 
                                               OnlineInterface,
                                               SKLEARNInterface)
from cortex.tools import Mapping

class Test(unittest.TestCase):

    def test_create_model_interface_from_factory_configuration(self):
        # Test parameters
        data = get_test_create_model_interface_from_factory_configuration_data()
        
        # Test
        for configuration, expected_result in data:
            actual_result = create_model_interface_from_factory_configuration(configuration)
            self.assertTrue(*expected_result._inner_eq(actual_result))



def get_test_create_model_interface_from_factory_configuration_data():
    data = []
    
    # Online
    model_interface_configuration = Mapping({"witness_field0": "witness_value0",
                                             "sample_features_vectorizer": Mapping({"name": "mock"}),
                                             "classifier": Mapping({"name": "perceptrononlinebinary",
                                                                    "config": Mapping(),
                                                                    }),
                                             })
    configuration = Mapping({"name": "online", "config": model_interface_configuration})
    expected_result = OnlineInterface.from_configuration(model_interface_configuration)
    data.append((configuration, expected_result))
    
    # sklearn
    model_interface_configuration = Mapping({"witness_field0": "witness_value0",
                                             "sample_features_vectorizer": Mapping({"name": "mock"}),
                                             "classifier": Mapping({"name": "logisticregressionsklearnbinary",
                                                                   "config": Mapping(),
                                                                   }),
                                            })
    configuration = Mapping({"name": "sklearn", "config": model_interface_configuration})
    expected_result = SKLEARNInterface.from_configuration(model_interface_configuration)
    data.append((configuration, expected_result))
    
    return data
    


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()