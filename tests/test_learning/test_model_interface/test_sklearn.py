# -*- coding: utf-8 -*-

import unittest

import os
import numpy

from cortex.parameters import ENCODING
from cortex.learning.ml_model.classifier import create_classifier_from_factory_configuration
from cortex.learning.model_interface.sklearn import SKLEARNInterface
from cortex.learning.sample_features_vectorizer import (create_sample_features_vectorizer_from_factory_configuration, 
                                                        ExtendedMentionPairSampleFeaturesVectorizer)
from cortex.learning.ml_features.hierarchy.pairs import PairGramTypeHierarchy #OverlapPairGramTypeHierarchy
from cortex.learning.ml_features.hierarchy.mentions import MentionNoneHierarchy
from cortex.learning.classification_sample_converter import ClassificationSampleConverter

from cortex.learning.samples import POS_CLASS, NEG_CLASS
from cortex.learning.prediction import ClassificationPrediction
from cortex.utils.io import TemporaryDirectory
from cortex.tools import Mapping

class MockClassificationSample(object):
    def __init__(self, label, x):
        self.label = label
        self.x = x
    def contextual_sample_hash(self):
        return hash(self.x)


class TestSKLEARNInterface(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        (args, kwargs), attribute_name_expected_result_pairs = get_interface_test___init___data()
        
        # Test
        sample_features_vectorizer = SKLEARNInterface(*args, **kwargs)
        for attribute_name, expected_result in attribute_name_expected_result_pairs:
            actual_result = getattr(sample_features_vectorizer, attribute_name)
            self.assertEqual(expected_result, actual_result, attribute_name)
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        data = get_interface_test__inner_eq_data()
        
        # Test
        for instance1, instance2, expected_result in data:
            self.assertEqual(expected_result, instance1._inner_eq(instance2))
    
    #@unittest.skip
    def test_from_configuration(self):
        #Test parameters
        configuration, expected_result = get_interface_test_from_configuration_data()
        
        # Test
        actual_result = SKLEARNInterface.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test 1
        ## Test parameter
        TEST_CLASS = SKLEARNInterface
        
        pair_hierarchy_factory_configuration = PairGramTypeHierarchy.define_factory_configuration() #Overlap
        mention_hierarchy_factory_configuration = MentionNoneHierarchy.define_factory_configuration()
        with_singleton_features = True
        with_anaphoricity_features = False
        quantize = True
        args_1 = tuple()
        kwargs_1 = {"pair_hierarchy_factory_configuration": pair_hierarchy_factory_configuration, 
                    "mention_hierarchy_factory_configuration": mention_hierarchy_factory_configuration, 
                    "with_singleton_features": with_singleton_features,
                    "with_anaphoricity_features": with_anaphoricity_features,
                    "quantize": quantize, 
                    }
        create_sample_features_vectorizer_from_factory_configuration_configuration =\
         ExtendedMentionPairSampleFeaturesVectorizer.define_factory_configuration(*args_1, **kwargs_1)
        
        C = 1.5
        fit_intercept = False
        max_iter = 47
        tol = 2e-2
        loss = "squared_hinge"
        average = True
        random_state = 9
        class_weight = "balanced"
        shuffle = False
        create_classifier_from_factory_configuration_configuration = Mapping({"name": "PassiveAggressiveSKLEARNBinaryClassifier", 
                                                    "config": Mapping({"C": C,
                                                                       "fit_intercept": fit_intercept,
                                                                       "max_iter": max_iter,
                                                                       "tol": tol,
                                                                       "loss": loss,
                                                                       "average": average,
                                                                       "random_state": random_state,
                                                                       "class_weight": class_weight,
                                                                       "shuffle": shuffle,
                                                                       }),
                                                    })
        
        args = (create_sample_features_vectorizer_from_factory_configuration_configuration, create_classifier_from_factory_configuration_configuration)
        kwargs = {}
        expected_result = Mapping({"sample_features_vectorizer": create_sample_features_vectorizer_from_factory_configuration_configuration,
                                   "classifier": create_classifier_from_factory_configuration_configuration,
                                   })
        
        ## Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
        
        # Test 2
        ## Test parameter
        TEST_CLASS = SKLEARNInterface
        
        pair_hierarchy_factory_configuration = PairGramTypeHierarchy.define_factory_configuration() #Overlap
        mention_hierarchy_factory_configuration = MentionNoneHierarchy.define_factory_configuration()
        with_singleton_features = True
        with_anaphoricity_features = False
        quantize = True
        args_1 = tuple()
        kwargs_1 = {"pair_hierarchy_factory_configuration": pair_hierarchy_factory_configuration, 
                    "mention_hierarchy_factory_configuration": mention_hierarchy_factory_configuration, 
                    "with_singleton_features": with_singleton_features,
                    "with_anaphoricity_features": with_anaphoricity_features,
                    "quantize": quantize, 
                    }
        create_sample_features_vectorizer_from_factory_configuration_configuration =\
         ExtendedMentionPairSampleFeaturesVectorizer.define_factory_configuration(*args_1, **kwargs_1)
        
        iteration_nb = 42
        C = 14.0675
        avg = True
        warm_start = True
        create_classifier_from_factory_configuration_configuration = Mapping({"name": "PAOnlineBinaryClassifier",
                                                    "config": Mapping({"iteration_nb": iteration_nb,
                                                                       "C": C,
                                                                       "avg": avg,
                                                                       "warm_start": warm_start, 
                                                                       }),
                                                    })
        
        args = (create_sample_features_vectorizer_from_factory_configuration_configuration, create_classifier_from_factory_configuration_configuration)
        kwargs = {}
        
        ## Test
        with self.assertRaises(ValueError):
            _ = TEST_CLASS.define_configuration(*args, **kwargs) #configuration
    
    #@unittest.skip
    def test_learn(self):
        # Test parameters
        data = get_interface_test_learn_data()
        
        # Test
        for (interface, classification_samples), (expected_features_names, expected_interface) in data:
            actual_features_names = tuple(interface._classification_sample_converter.features_names)
            self.assertEqual(expected_features_names, actual_features_names)
            interface.learn(classification_samples)
            self.assertTrue(*expected_interface._inner_eq(interface))
            ## Test that an exception is indeed raised when trying to learn from an empty iterable
            with self.assertRaises(ValueError):
                interface.learn(tuple())
    
    #@unittest.skip
    def test__decode_class_scores(self):
        # Test parameters
        configuration, _ = get_interface_test_from_configuration_data() #expected_result
        configuration.sample_features_vectorizer = Mapping({"name": "mock"})
        interface = SKLEARNInterface.from_configuration(configuration)
        ## In order to use the _decode_class_scores method, the interface must possess a model, that is why we make it learn one before
        classification_sample1 = MockClassificationSample(POS_CLASS, 3)
        classification_sample2 = MockClassificationSample(NEG_CLASS, -5)
        classification_sample3 = MockClassificationSample(POS_CLASS, 5)
        classification_samples = (classification_sample1, classification_sample2, classification_sample3)
        interface.learn(classification_samples)
        predicting_interface = interface
        predicted_scores = numpy.array([[0.3, 0.7], [0.8, 0.2], [0.49, 0.51]])
        expected_result = (ClassificationPrediction((("1", 0.7), ("-1", 0.3))), 
                           ClassificationPrediction((("1", 0.2), ("-1", 0.8))), 
                           ClassificationPrediction((("1",0.51), ("-1", 0.49))),
                           )
        
        # Test
        actual_result = predicting_interface._decode_class_scores(predicted_scores)
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_predict(self):
        # Test parameters
        (interface, predict_classification_samples), expected_result = get_interface_test_predict_data()
        
        # Test
        actual_result = interface.predict(predict_classification_samples)
        self.assertEqual(expected_result, actual_result)
        
    #@unittest.skip
    def test_save_model(self):
        # Test parameters
        configuration, _ = get_interface_test_from_configuration_data() #expected_result
        configuration.sample_features_vectorizer = Mapping({"name": "mock", "config": Mapping({"inverse_order": False})})
        interface = SKLEARNInterface.from_configuration(configuration)
        interface._classifier.classes_ = numpy.array([0,1], dtype=numpy.int)
        interface._classifier.intercept_ = numpy.array([0.12485939], dtype=numpy.float)
        interface._classifier.coef_ = numpy.array([[0.0100344, 0., 0.12485939, -0.2930915, 0.41795089, 0., 0., -0.2930915, 0.41795089, 0., -0.2930915,  0., 0.41795089]], dtype=numpy.float)
        interface._classifier.n_iter_ = 4
        expected_features_names_file_content = "test_numeric_feature\n[test_categorical_feature1]__(yes)\n[test_categorical_feature1]__(no)\n[test_categorical_feature2]__(yep)\n[test_categorical_feature2]__(nope)\n[product_[test_categorical_feature1]_[test_categorical_feature2]]__((yes)_(yep))\n[product_[test_categorical_feature1]_[test_categorical_feature2]]__((yes)_(nope))\n[product_[test_categorical_feature1]_[test_categorical_feature2]]__((no)_(yep))\n[product_[test_categorical_feature1]_[test_categorical_feature2]]__((no)_(nope))\n[product_[test_categorical_feature2]_[test_categorical_feature1]]__((yep)_(yes))\n[product_[test_categorical_feature2]_[test_categorical_feature1]]__((yep)_(no))\n[product_[test_categorical_feature2]_[test_categorical_feature1]]__((nope)_(yes))\n[product_[test_categorical_feature2]_[test_categorical_feature1]]__((nope)_(no))"
        expected_classifier = interface._classifier
        
        # Test
        with TemporaryDirectory() as temp_dir:
            temp_folder_path = temp_dir.temporary_folder_path
            folder_path = temp_folder_path
            features_names_file_path = os.path.join(folder_path, interface._FEATURES_NAMES_FILE_NAME)
            self.assertFalse(os.path.exists(features_names_file_path))
            interface.save_model(folder_path)
            with open(features_names_file_path, "r", encoding=ENCODING) as f:
                actual_features_names_file_content = f.read()
            classifier = SKLEARNInterface.from_configuration(configuration)._classifier
            classifier.load_model(temp_folder_path)
        self.assertEqual(expected_features_names_file_content, actual_features_names_file_content)
        self.assertTrue(*SKLEARNInterface._inner_eq(expected_classifier, classifier))
    
    #@unittest.skip
    def test_load_model(self):
        # Test parameters
        folder_path = os.path.join(os.path.dirname(__file__), "data", "_sklearn_classifier")
        configuration, _ = get_interface_test_from_configuration_data() #expected_result
        configuration.sample_features_vectorizer = Mapping({"name": "mock", "config": Mapping({"inverse_order": False})})
        interface = SKLEARNInterface.from_configuration(configuration)
        interface._classifier.classes_ = numpy.array([0,1], dtype=numpy.int)
        interface._classifier.intercept_ = numpy.array([0.12485939], dtype=numpy.float)
        interface._classifier.coef_ = numpy.array([[0.0100344, 0., 0.12485939, -0.2930915, 0.41795089, 0., 0., -0.2930915, 0.41795089, 0., -0.2930915,  0., 0.41795089]], dtype=numpy.float)
        interface._classifier.n_iter_ = numpy.array([4])
        #interface._classifier.save_model(folder_path)
        expected_interface = interface
        
        interface = SKLEARNInterface.from_configuration(configuration)
        
        # Test
        self.assertFalse(*expected_interface._inner_eq(interface))
        interface.load_model(folder_path)
        self.assertTrue(*expected_interface._inner_eq(interface))



def get_interface_test___init___data():
    attribute_name_expected_result_pairs = []
    
    with_singleton_features = False
    with_anaphoricity_features = True
    quantize = True
    strict = True
    sample_features_vectorizer_configuration = Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy"}), 
                                                        "with_singleton_features": with_singleton_features, 
                                                        "with_anaphoricity_features": with_anaphoricity_features, 
                                                        "quantize": quantize, 
                                                        "strict": strict, 
                                                        "useless_field": "useless_value"})
    sample_features_vectorizer = create_sample_features_vectorizer_from_factory_configuration(Mapping({"name": "mentionpair", 
                                                                            "config": sample_features_vectorizer_configuration})
                                                                   )
    expected_classification_sample_converter = ClassificationSampleConverter(SKLEARNInterface._LABELS, sample_features_vectorizer)
    attribute_name_expected_result_pairs.append(("_classification_sample_converter", expected_classification_sample_converter))
    
    penalty = "l2"
    dual = True
    tol = 1e-3
    C = 1.5
    fit_intercept = True
    intercept_scaling = 1
    class_weight = "balanced"
    random_state = 84
    solver = "liblinear"
    max_iter = 42
    multi_class = "ovr"
    expected_classifier_configuration = Mapping({"witness_field": "witness_value", 
                                                "penalty": penalty,
                                                "dual": dual,
                                                "tol": tol,
                                                "C": C,
                                                "fit_intercept": fit_intercept,
                                                "intercept_scaling": intercept_scaling,
                                                "class_weight": class_weight,
                                                "random_state": random_state,
                                                "solver": solver,
                                                "max_iter": max_iter,
                                                "multi_class": multi_class,
                                                })
    classifier_configuration = Mapping(expected_classifier_configuration)
    classifier_configuration.positive_class_label = 1
    classifier_configuration.negative_class_label = 0
    classifier_configuration = Mapping({"name": "logisticregressionsklearnbinary", "config": classifier_configuration})
    expected_classifier = create_classifier_from_factory_configuration(classifier_configuration)
    expected_classifier.configuration = expected_classifier_configuration
    attribute_name_expected_result_pairs.append(("_classifier", expected_classifier))
    
    expected_configuration = Mapping({"witness_field": "witness_value", 
                                      "sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                             "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                "with_singleton_features": with_singleton_features, 
                                                                                                "with_anaphoricity_features": with_anaphoricity_features, 
                                                                                                "quantize": quantize, 
                                                                                                "strict": strict, 
                                                                                                "useless_field": "useless_value"})
                                                                             }),
                                      "classifier": Mapping({"name": "LogisticRegressionSKLEARNBinaryClassifier", 
                                                             "config": expected_classifier_configuration})
                                    })
    attribute_name_expected_result_pairs.append(("configuration", expected_configuration))
    
    sample_features_vectorizer = create_sample_features_vectorizer_from_factory_configuration(Mapping({"name": "mentionpair", 
                                                                            "config": sample_features_vectorizer_configuration})
                                                             )
    configuration = Mapping({"witness_field": "witness_value"})
    args = (sample_features_vectorizer, classifier_configuration,)
    kwargs = {"configuration": configuration}
    
    return (args, kwargs), attribute_name_expected_result_pairs

def get_interface_test__inner_eq_data():
    data = []
    
    (args, kwargs), _ = get_interface_test___init___data() #attribute_name_expected_result_pairs
    
    instance1 = SKLEARNInterface(*args, **kwargs)
    instance2 = SKLEARNInterface(*args, **kwargs)
    expected_result = (True, None)
    data.append((instance1, instance2, expected_result))
    
    with_singleton_features = False
    with_anaphoricity_features = True
    quantize = False
    sample_features_vectorizer_configuration = Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy"}), 
                                                   "with_singleton_features": with_singleton_features, 
                                                   "with_anaphoricity_features": with_anaphoricity_features, 
                                                   "quantize": quantize, 
                                                   "useless_field": "useless_value"})
    sample_features_vectorizer = create_sample_features_vectorizer_from_factory_configuration(Mapping({"name": "mentionpair", 
                                                                    "config": sample_features_vectorizer_configuration})
                                                             )
    args = (sample_features_vectorizer,) + args[1:]
    sample_features_vectorizer3 = SKLEARNInterface(*args, **kwargs)
    expected_result = (False, "_classification_sample_converter")
    data.append((instance1, sample_features_vectorizer3, expected_result))
    
    return data

def get_interface_test_from_configuration_data():
    with_singleton_features = False
    with_anaphoricity_features = True
    quantize = True
    sample_features_vectorizer_configuration = Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy"}), 
                                                   "with_singleton_features": with_singleton_features, 
                                                   "with_anaphoricity_features": with_anaphoricity_features, 
                                                   "quantize": quantize, 
                                                   "useless_field": "useless_value"})
    sample_features_vectorizer = create_sample_features_vectorizer_from_factory_configuration(Mapping({"name": "mentionpair", 
                                                                    "config": sample_features_vectorizer_configuration})
                                                             )
    
    penalty = "l2"
    dual = True
    tol = 1e-3
    C = 1.5
    fit_intercept = True
    intercept_scaling = 1
    class_weight = "balanced"
    random_state = 84
    solver = "liblinear"
    max_iter = 42
    multi_class = "ovr"
    classifier_configuration = Mapping({"witness_field": "witness_value", 
                                                "penalty": penalty,
                                                "dual": dual,
                                                "tol": tol,
                                                "C": C,
                                                "fit_intercept": fit_intercept,
                                                "intercept_scaling": intercept_scaling,
                                                "class_weight": class_weight,
                                                "random_state": random_state,
                                                "solver": solver,
                                                "max_iter": max_iter,
                                                "multi_class": multi_class,
                                                })
    classifier_configuration.positive_class_label = 1
    classifier_configuration.negative_class_label = 0
    classifier_configuration = Mapping({"name": "logisticregressionsklearnbinary", "config": classifier_configuration})
    
    configuration = Mapping({"witness_field": "witness_value"})
    args = (sample_features_vectorizer, classifier_configuration,)
    kwargs = {"configuration": configuration}
    expected_result = SKLEARNInterface(*args, **kwargs)
    
    configuration = Mapping({"witness_field": "witness_value", 
                             "sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                    "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                       "with_singleton_features": with_singleton_features, 
                                                                                       "with_anaphoricity_features": with_anaphoricity_features, 
                                                                                       "quantize": quantize, 
                                                                                       "useless_field": "useless_value"})
                                                          }),
                             "classifier": classifier_configuration,
                           })
    
    return configuration, expected_result

def get_interface_test_learn_data():
    data = []
    
    classification_sample1 = MockClassificationSample(POS_CLASS, 3)
    classification_sample2 = MockClassificationSample(NEG_CLASS, -5)
    classification_sample3 = MockClassificationSample(POS_CLASS, 5)
    classification_samples = (classification_sample1, classification_sample2, classification_sample3)
    
    # Test with identical features order
    penalty = "l2"
    dual = False
    tol = 1e-4
    C = 1.0
    fit_intercept = True
    intercept_scaling = 1
    class_weight = None
    random_state = None
    solver = "liblinear"
    max_iter = 100
    multi_class = "ovr"
    classifier_configuration = Mapping({"witness_field": "witness_value", 
                                                "penalty": penalty,
                                                "dual": dual,
                                                "tol": tol,
                                                "C": C,
                                                "fit_intercept": fit_intercept,
                                                "intercept_scaling": intercept_scaling,
                                                "class_weight": class_weight,
                                                "random_state": random_state,
                                                "solver": solver,
                                                "max_iter": max_iter,
                                                "multi_class": multi_class,
                                                })
    classifier_configuration.positive_class_label = 1
    classifier_configuration.negative_class_label = 0
    
    configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "mock", "config": Mapping({"inverse_order": False})}),
                             "classifier": Mapping({"name": "logisticregressionsklearnbinary", "config": classifier_configuration}),
                             })
    interface = SKLEARNInterface.from_configuration(configuration)
    expected_interface = SKLEARNInterface.from_configuration(configuration)
    expected_features_names = ("test_numeric_feature",  
                                "[test_categorical_feature1]__(yes)",
                                "[test_categorical_feature1]__(no)",
                                "[test_categorical_feature2]__(yep)",
                                "[test_categorical_feature2]__(nope)", 
                                "[product_[test_categorical_feature1]_[test_categorical_feature2]]__((yes)_(yep))",
                                "[product_[test_categorical_feature1]_[test_categorical_feature2]]__((yes)_(nope))",
                                "[product_[test_categorical_feature1]_[test_categorical_feature2]]__((no)_(yep))",
                                "[product_[test_categorical_feature1]_[test_categorical_feature2]]__((no)_(nope))",
                                "[product_[test_categorical_feature2]_[test_categorical_feature1]]__((yep)_(yes))",
                                "[product_[test_categorical_feature2]_[test_categorical_feature1]]__((yep)_(no))",
                                "[product_[test_categorical_feature2]_[test_categorical_feature1]]__((nope)_(yes))",
                                "[product_[test_categorical_feature2]_[test_categorical_feature1]]__((nope)_(no))")
    expected_interface._classifier.classes_ = numpy.array([0,1], dtype=numpy.int)
    expected_interface._classifier.intercept_ = numpy.array([0.12485939], dtype=numpy.float)
    expected_interface._classifier.coef_ = numpy.array([[0.0100344, 0., 0.12485939, -0.2930915, 0.41795089, 0., 0., -0.2930915, 0.41795089, 0., -0.2930915,  0., 0.41795089]], dtype=numpy.float)
    expected_interface._classifier.n_iter_ = numpy.array([4])
    data.append(((interface, classification_samples), (expected_features_names, expected_interface)))
    """
    penalty='l2', dual=False, tol=1e-4, C=1.0, 
    fit_intercept=True, intercept_scaling=1, 
     class_weight=None, 
    random_state=None, solver='liblinear', 
     max_iter=100, 
    multi_class='ovr'
    """
    # TODO: carry out the test with a sample_features_vectorizer whose features are not in the same order!
    # Test with different features order
    configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "mock", "config": Mapping({"inverse_order": True})}),
                             "classifier": Mapping({"name": "logisticregressionsklearnbinary", "config": classifier_configuration}),
                             })
    interface = SKLEARNInterface.from_configuration(configuration)
    expected_interface = SKLEARNInterface.from_configuration(configuration)
    expected_features_names = ("[test_categorical_feature2]__(yep)",
                                "[test_categorical_feature2]__(nope)", 
                                "[test_categorical_feature1]__(yes)",
                                "[test_categorical_feature1]__(no)",
                                "test_numeric_feature", 
                                "[product_[test_categorical_feature1]_[test_categorical_feature2]]__((yes)_(yep))",
                                "[product_[test_categorical_feature1]_[test_categorical_feature2]]__((yes)_(nope))",
                                "[product_[test_categorical_feature1]_[test_categorical_feature2]]__((no)_(yep))",
                                "[product_[test_categorical_feature1]_[test_categorical_feature2]]__((no)_(nope))",
                                "[product_[test_categorical_feature2]_[test_categorical_feature1]]__((yep)_(yes))",
                                "[product_[test_categorical_feature2]_[test_categorical_feature1]]__((yep)_(no))",
                                "[product_[test_categorical_feature2]_[test_categorical_feature1]]__((nope)_(yes))",
                                "[product_[test_categorical_feature2]_[test_categorical_feature1]]__((nope)_(no))")
    expected_interface._classifier.classes_ = numpy.array([0,1], dtype=numpy.int)
    expected_interface._classifier.intercept_ = numpy.array([0.12485939], dtype=numpy.float)
    expected_interface._classifier.coef_ = numpy.array([[-0.2930915, 0.41795089, 0., 0.12485939, 0.0100344, 0., 0., -0.2930915, 0.41795089, 0., -0.2930915,  0., 0.41795089]], dtype=numpy.float)
    expected_interface._classifier.n_iter_ = numpy.array([4])
    data.append(((interface, classification_samples), (expected_features_names, expected_interface)))
    
    return data

def get_interface_test_predict_data():
    data = get_interface_test_learn_data()
    (interface, learning_classification_samples), _ = data[0] #(expected_features_names, expected_interface)
    interface.learn(learning_classification_samples)
    
    predict_classification_sample1 = MockClassificationSample(POS_CLASS, -7)
    predict_classification_sample2 = MockClassificationSample(NEG_CLASS, 9)
    predict_classification_samples = (predict_classification_sample1, predict_classification_sample2)
        
    expected_result = (ClassificationPrediction((("-1",0.6592418491764849),("1",0.3407581508235151))), 
                       ClassificationPrediction((("-1",0.5670869451686658),("1",0.4329130548313342))),
                       )
    
    return (interface, predict_classification_samples), expected_result



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()