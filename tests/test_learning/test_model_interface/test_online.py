# -*- coding: utf-8 -*-

import unittest

import os
import numpy
from scipy import sparse

from cortex.parameters import ENCODING
from cortex.learning.model_interface.online import OnlineInterface
from cortex.learning.ml_model.classifier import create_classifier_from_factory_configuration
from cortex.learning.sample_features_vectorizer import (create_sample_features_vectorizer_from_factory_configuration,
                                                        ExtendedMentionPairSampleFeaturesVectorizer)
from cortex.learning.ml_features.hierarchy.pairs import PairGramTypeHierarchy #OverlapPairGramTypeHierarchy
from cortex.learning.ml_features.hierarchy.mentions import MentionNoneHierarchy
from cortex.learning.classification_sample_converter import ClassificationSampleConverter
from cortex.learning.samples import POS_CLASS, NEG_CLASS
from cortex.learning.prediction import ClassificationPrediction
from cortex.tools import Mapping
from cortex.utils.io import TemporaryDirectory


class MockClassificationSample(object):
    def __init__(self, label, x):
        self.label = label
        self.x = x
    def contextual_sample_hash(self):
        return hash(self.x)


class TestOnlineInterface(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        (args, kwargs), attribute_name_expected_result_pairs = get_interface_test___init___data()
        
        # Test
        sample_features_vectorizer = OnlineInterface(*args, **kwargs)
        for attribute_name, expected_result in attribute_name_expected_result_pairs:
            actual_result = getattr(sample_features_vectorizer, attribute_name)
            if attribute_name == "configuration":
                print(type(expected_result)._inner_eq(expected_result.sample_features_vectorizer, actual_result.sample_features_vectorizer))
            self.assertEqual(expected_result, actual_result, attribute_name)
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        data = get_interface_test__inner_eq_data()
        
        # Test
        for instance1, instance2, expected_result in data:
            self.assertEqual(expected_result, instance1._inner_eq(instance2))
    
    #@unittest.skip
    def test_from_configuration(self):
        #Test parameters
        configuration, expected_result = get_interface_test_from_configuration_data()
        
        # Test
        actual_result = OnlineInterface.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test 1
        ## Test parameter
        TEST_CLASS = OnlineInterface
        
        pair_hierarchy_factory_configuration = PairGramTypeHierarchy.define_factory_configuration() #Overlap
        mention_hierarchy_factory_configuration = MentionNoneHierarchy.define_factory_configuration()
        with_singleton_features = True
        with_anaphoricity_features = False
        quantize = True
        args_1 = tuple()
        kwargs_1 = {"pair_hierarchy_factory_configuration": pair_hierarchy_factory_configuration, 
                    "mention_hierarchy_factory_configuration": mention_hierarchy_factory_configuration, 
                    "with_singleton_features": with_singleton_features,
                    "with_anaphoricity_features": with_anaphoricity_features,
                    "quantize": quantize, 
                    }
        create_sample_features_vectorizer_from_factory_configuration_configuration =\
         ExtendedMentionPairSampleFeaturesVectorizer.define_factory_configuration(*args_1, **kwargs_1)
        
        iteration_nb = 42
        C = 14.0675
        avg = True
        warm_start = True
        create_classifier_from_factory_configuration_configuration = Mapping({"name": "PAOnlineBinaryClassifier",
                                                    "config": Mapping({"iteration_nb": iteration_nb,
                                                                       "C": C,
                                                                       "avg": avg,
                                                                       "warm_start": warm_start, 
                                                                       }),
                                                    })
        
        args = (create_sample_features_vectorizer_from_factory_configuration_configuration, create_classifier_from_factory_configuration_configuration)
        kwargs = {}
        expected_result = Mapping({"sample_features_vectorizer": create_sample_features_vectorizer_from_factory_configuration_configuration,
                                   "classifier": create_classifier_from_factory_configuration_configuration,
                                   })
        
        ## Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
        
        # Test 2
        ## Test parameter
        TEST_CLASS = OnlineInterface
        
        pair_hierarchy_factory_configuration = PairGramTypeHierarchy.define_factory_configuration() #Overlap
        mention_hierarchy_factory_configuration = MentionNoneHierarchy.define_factory_configuration()
        with_singleton_features = True
        with_anaphoricity_features = False
        quantize = True
        args_1 = tuple()
        kwargs_1 = {"pair_hierarchy_factory_configuration": pair_hierarchy_factory_configuration, 
                    "mention_hierarchy_factory_configuration": mention_hierarchy_factory_configuration, 
                    "with_singleton_features": with_singleton_features,
                    "with_anaphoricity_features": with_anaphoricity_features,
                    "quantize": quantize, 
                    }
        create_sample_features_vectorizer_from_factory_configuration_configuration =\
         ExtendedMentionPairSampleFeaturesVectorizer.define_factory_configuration(*args_1, **kwargs_1)
        
        C = 1.5
        fit_intercept = False
        max_iter = 47
        tol = 2e-2
        loss = "squared_hinge"
        average = True
        random_state = 9
        class_weight = "balanced"
        shuffle = False
        create_classifier_from_factory_configuration_configuration = Mapping({"name": "PassiveAggressiveSKLEARNBinaryClassifier", 
                                                    "config": Mapping({"C": C,
                                                                       "fit_intercept": fit_intercept,
                                                                       "max_iter": max_iter,
                                                                       "tol": tol,
                                                                       "loss": loss,
                                                                       "average": average,
                                                                       "random_state": random_state,
                                                                       "class_weight": class_weight,
                                                                       "shuffle": shuffle,
                                                                       }),
                                                    })
        
        args = (create_sample_features_vectorizer_from_factory_configuration_configuration, create_classifier_from_factory_configuration_configuration)
        kwargs = {}
        
        ## Test
        with self.assertRaises(ValueError):
            _ = TEST_CLASS.define_configuration(*args, **kwargs) #configuration
    
    #@unittest.skip
    def test_learn(self):
        # Test parameters
        (interface, classification_samples), expected_interface = get_interface_test_learn_data()
        
        # Test
        self.assertFalse(*expected_interface._inner_eq(interface))
        interface.learn(classification_samples)
        self.assertTrue(*expected_interface._inner_eq(interface))
        ## Test that an exception is indeed raised when trying to learn from an empty iterable
        with self.assertRaises(ValueError):
            interface.learn(tuple())
    
    #@unittest.skip
    def test_learn_sum(self):
        # Test parameters
        (interface, args), expected_interface = get_interface_test_learn_sum_data()
        
        # Test
        self.assertFalse(*expected_interface._inner_eq(interface))
        interface.learn_sum(*args)
        self.assertTrue(*expected_interface._inner_eq(interface))
    
    #@unittest.skip
    def test__decode_class_scores(self):
        # Test parameters
        configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "mock", "config": Mapping({"inverse_order": False})}),
                                 "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                        "config": Mapping({"avg": True, 
                                                                           "warm_start": False, 
                                                                           "alpha": 1.52, 
                                                                           "useless_field": "useless_value"}),
                                                        }),
                                 })
        interface = OnlineInterface.from_configuration(configuration)
        ## In order to use the _decode_class_scores method, the interface must possess a model, that is why we make it learn one before
        classification_sample1 = MockClassificationSample(POS_CLASS, 3)
        classification_sample2 = MockClassificationSample(NEG_CLASS, -5)
        classification_sample3 = MockClassificationSample(POS_CLASS, 5)
        classification_samples = (classification_sample1, classification_sample2, classification_sample3)
        interface.learn(classification_samples)
        predicting_interface = interface
        predicted_scores = [[-0.3, 0.3], [0.8, -0.8], [-0.49, 0.49]]
        expected_result = (ClassificationPrediction(((OnlineInterface._POS_LABEL, 0.3), (OnlineInterface._NEG_LABEL, -0.3))), 
                           ClassificationPrediction(((OnlineInterface._NEG_LABEL, 0.8), (OnlineInterface._POS_LABEL, -0.8))), 
                           ClassificationPrediction(((OnlineInterface._POS_LABEL, 0.49),(OnlineInterface._NEG_LABEL, -0.49))),
                           )
        
        # Test
        actual_result = predicting_interface._decode_class_scores(predicted_scores)
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_predict(self):
        # Test parameters
        (interface, predict_classification_samples), expected_result = get_interface_test_predict_data()
        
        # Test
        actual_result = interface.predict(predict_classification_samples)
        print(expected_result)
        print(actual_result)
        self.assertEqual(expected_result, actual_result)
        
    #@unittest.skip
    def test_save_model(self):
        # Test parameters
        interface, (expected_features_names_file_content, expected_classifier) = get_interface_test_save_model_data()
        
        # Test
        with TemporaryDirectory() as temp_dir:
            temp_folder_path = temp_dir.temporary_folder_path
            folder_path = temp_folder_path
            features_names_file_path = os.path.join(folder_path, interface._FEATURES_NAMES_FILE_NAME)
            model_file_path = os.path.join(folder_path, interface._classifier._MODEL_FILE_NAME)
            
            self.assertFalse(os.path.exists(features_names_file_path))
            self.assertFalse(os.path.exists(model_file_path))
            interface.save_model(folder_path)
            with open(features_names_file_path, "r", encoding=ENCODING) as f:
                actual_features_names_file_content = f.read()
            classifier = type(expected_classifier).from_configuration(expected_classifier.configuration)
            classifier.load_model(temp_folder_path)
        self.assertEqual(expected_features_names_file_content, actual_features_names_file_content)
        self.assertTrue(*expected_classifier._inner_eq(classifier))
    
    #@unittest.skip
    def test_load_model(self):
        # Test parameters
        (interface, (features_names_file_content, classifier)), expected_interface = get_interface_test_load_model_data()
        
        # Test
        with TemporaryDirectory() as temp_dir:
            temp_folder_path = temp_dir.temporary_folder_path
            folder_path = temp_folder_path
            features_names_file_path = os.path.join(folder_path, interface._FEATURES_NAMES_FILE_NAME)
            with open(features_names_file_path, "w", encoding=ENCODING) as f:
                f.write(features_names_file_content)
            classifier.save_model(folder_path)
            
            self.assertFalse(*expected_interface._inner_eq(interface))
            interface.load_model(folder_path)
            self.assertTrue(*expected_interface._inner_eq(interface))



def get_interface_test___init___data():
    attribute_name_expected_result_pairs = []
    
    with_singleton_features = False
    with_anaphoricity_features = True
    quantize = True
    strict = True
    sample_features_vectorizer_configuration = Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy"}), 
                                                     "with_singleton_features": with_singleton_features, 
                                                     "with_anaphoricity_features": with_anaphoricity_features, 
                                                     "quantize": quantize, 
                                                     "strict": strict, 
                                                     "useless_field": "useless_value"})
    sample_features_vectorizer = create_sample_features_vectorizer_from_factory_configuration(Mapping({"name": "mentionpair", 
                                                                      "config": sample_features_vectorizer_configuration})
                                                             )
    expected_classification_sample_converter = ClassificationSampleConverter(OnlineInterface._LABELS, sample_features_vectorizer)
    attribute_name_expected_result_pairs.append(("_classification_sample_converter", expected_classification_sample_converter))
    
    iteration_nb = 2
    avg = True
    warm_start = False
    alpha = 0.25
    expected_classifier_configuration = Mapping({"iteration_nb": iteration_nb, 
                                                 "avg": avg, 
                                                 "warm_start": warm_start, 
                                                 "alpha": alpha, 
                                                 "useless_field": "useless_value"})
    classifier_configuration = Mapping(expected_classifier_configuration)
    classifier_configuration.positive_class_label = 1
    classifier_configuration.negative_class_label = 0
    classifier_configuration = Mapping({"name": "perceptrononlinebinary", "config": classifier_configuration})
    expected_classifier = create_classifier_from_factory_configuration(classifier_configuration)
    expected_classifier.configuration = expected_classifier_configuration
    attribute_name_expected_result_pairs.append(("_classifier",expected_classifier))
    
    expected_configuration = Mapping({"witness_field": "witness_value", 
                                      "sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                          "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                             "with_singleton_features": with_singleton_features, 
                                                                                             "with_anaphoricity_features": with_anaphoricity_features, 
                                                                                             "quantize": quantize, 
                                                                                             "strict": strict, 
                                                                                             "useless_field": "useless_value"})
                                                                          }),
                                      "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                             "config": Mapping({"iteration_nb": iteration_nb, 
                                                                                "avg": avg, 
                                                                                "warm_start": warm_start, 
                                                                                "alpha": alpha, 
                                                                                "useless_field": "useless_value"})
                                                             })
                                      })
    attribute_name_expected_result_pairs.append(("configuration", expected_configuration))
    
    sample_features_vectorizer = create_sample_features_vectorizer_from_factory_configuration(Mapping({"name": "mentionpair", 
                                                                      "config": sample_features_vectorizer_configuration})
                                                             )
    configuration = Mapping({"witness_field": "witness_value"})
    args = (sample_features_vectorizer, classifier_configuration,)
    kwargs = {"configuration": configuration}
    
    return (args, kwargs), attribute_name_expected_result_pairs

def get_interface_test__inner_eq_data():
    data = []
    
    (args, kwargs), _ = get_interface_test___init___data() #attribute_name_expected_result_pairs
    
    instance1 = OnlineInterface(*args, **kwargs)
    instance2 = OnlineInterface(*args, **kwargs)
    expected_result = (True, None)
    data.append((instance1, instance2, expected_result))
    
    avg = True
    warm_start = False
    alpha = 1.52
    classifier_configuration = Mapping({"avg": avg, 
                                        "warm_start": warm_start, 
                                        "alpha": alpha, 
                                        "useless_field": "useless_value"})
    classifier_configuration.positive_class_label = POS_CLASS
    classifier_configuration.negative_class_label = NEG_CLASS
    classifier_configuration = Mapping({"name": "perceptrononlinebinary", "config": classifier_configuration})
    args = args[:1] + (classifier_configuration,)
    sample_features_vectorizer3 = OnlineInterface(*args, **kwargs)
    expected_result = (False, "_classifier")
    data.append((instance1, sample_features_vectorizer3, expected_result))
    
    return data

def get_interface_test_from_configuration_data():
    with_singleton_features = False
    with_anaphoricity_features = True
    quantize = True
    sample_features_vectorizer_configuration = Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy"}), 
                                                     "mention_hierarchy": Mapping({"name": "MentionGramTypeHierarchy"}), 
                                                     "with_singleton_features": with_singleton_features, 
                                                     "with_anaphoricity_features": with_anaphoricity_features, 
                                                     "quantize": quantize, 
                                                     "useless_field": "useless_value"})
    sample_features_vectorizer = create_sample_features_vectorizer_from_factory_configuration(Mapping({"name": "extendedmentionpair", 
                                                                      "config": sample_features_vectorizer_configuration})
                                                             )
    
    avg = True
    warm_start = False
    alpha = 1.52
    classifier_configuration = Mapping({"avg": avg, 
                                        "warm_start": warm_start, 
                                        "alpha": alpha, 
                                        "useless_field": "useless_value"})
    classifier_configuration = Mapping({"name": "perceptrononlinebinary", "config": classifier_configuration})
    
    configuration = Mapping({"witness_field": "witness_value"})
    args = (sample_features_vectorizer,classifier_configuration)
    kwargs = {"configuration": configuration}
    expected_result = OnlineInterface(*args, **kwargs)
    
    configuration = Mapping({"witness_field": "witness_value", 
                            "sample_features_vectorizer": Mapping({"name": "extendedmentionpair",
                                                                "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy"}), 
                                                                                   "mention_hierarchy": Mapping({"name": "MentionGramTypeHierarchy"}), 
                                                                                   "with_singleton_features": with_singleton_features, 
                                                                                   "with_anaphoricity_features": with_anaphoricity_features, 
                                                                                   "quantize": quantize, 
                                                                                   "useless_field": "useless_value"})
                                                          }),
                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                    "config": Mapping({"avg": avg, 
                                                                       "warm_start": warm_start, 
                                                                       "alpha": alpha, 
                                                                       "useless_field": "useless_value"}),
                                                    })
                           })
    
    return configuration, expected_result

def get_interface_test_learn_data():
    classification_sample1 = MockClassificationSample(POS_CLASS, 3)
    classification_sample2 = MockClassificationSample(NEG_CLASS, -5)
    classification_sample3 = MockClassificationSample(POS_CLASS, 5)
    classification_samples = (classification_sample1, classification_sample2, classification_sample3)
    
    avg = True
    warm_start = False
    alpha = 1.52
    iteration_nb = 1
    configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "mock", "config": Mapping({"inverse_order": False})}),
                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                    "config": Mapping({"avg": avg, 
                                                                       "warm_start": warm_start, 
                                                                       "alpha": alpha, 
                                                                       "iteration_nb": iteration_nb, 
                                                                       "useless_field": "useless_value"}),
                                                    }),
                             })
    interface = OnlineInterface.from_configuration(configuration)
    expected_interface = OnlineInterface.from_configuration(configuration)
    expected_classifier_select_configuration = Mapping(configuration.classifier)
    expected_classifier_select_configuration.config.positive_class_label = 1
    expected_classifier_select_configuration.config.negative_class_label = 0
    expected_classifier = create_classifier_from_factory_configuration(expected_classifier_select_configuration)
    expected_classifier.coef_ = numpy.array([[-51.68,   0.  ,   3.04,   0.  ,   3.04,   0.  ,   0.  ,   0.  , 3.04,   0.  ,   0.  ,   0.  ,   3.04]], dtype=numpy.float)
    expected_classifier._cumulative_coef_ = numpy.array([[-79.04,   0.  ,   6.08,   0.  ,   6.08,   0.  ,   0.  ,   0.  , 6.08,   0.  ,   0.  ,   0.  ,   6.08]], dtype=numpy.float)
    expected_interface._classifier = expected_classifier
    
    return (interface, classification_samples), expected_interface

def get_interface_test_learn_sum_data():
    positive_class_samples = (MockClassificationSample(POS_CLASS, 3), MockClassificationSample(POS_CLASS, 5))
    negative_class_samples = (MockClassificationSample(NEG_CLASS, -5),)
    compute_loss = 5
    
    # Test with sparse vectors
    avg = True
    warm_start = False
    alpha = 1.52
    configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "mock", "config": Mapping({"inverse_order": False})}),
                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                    "config": Mapping({"avg": avg, 
                                                                       "warm_start": warm_start, 
                                                                       "alpha": alpha, 
                                                                       "useless_field": "useless_value"}),
                                                    }),
                             })
    interface = OnlineInterface.from_configuration(configuration)
    features_nb = 13 #interface._classification_sample_converter.numeric_column_nb #13 for Mock
    interface.initialize_model()
    positive_class_encoded_samples = (sparse.csr_matrix([[-9, 0,1, 0,1, 0,0,0,1, 0,0,0,1],], dtype=numpy.float),
                                      sparse.csr_matrix([[-25, 0,1, 0,1, 0,0,0,1, 0,0,0,1],], dtype=numpy.float),
                                      )
    negative_class_encoded_samples = (sparse.csr_matrix([[-25, 0,1, 1,0, 0,0,1,0, 0,1,0,0],], dtype=numpy.float),
                                      )
    expected_interface = OnlineInterface.from_configuration(configuration)
    expected_classifier_select_configuration = Mapping(configuration.classifier)
    expected_classifier_select_configuration.config.positive_class_label = 1
    expected_classifier_select_configuration.config.negative_class_label = 0
    expected_classifier = create_classifier_from_factory_configuration(expected_classifier_select_configuration)
    expected_classifier.init_model(features_nb)
    expected_classifier.update_sum(positive_class_encoded_samples, negative_class_encoded_samples, compute_loss)
    expected_interface._classifier = expected_classifier
    
    args = (positive_class_samples, negative_class_samples, compute_loss)
    
    return (interface, args), expected_interface

def get_interface_test_predict_data():
    (interface, learning_classification_samples), _ = get_interface_test_learn_data() #(expected_features_names, expected_interface)
    interface.learn(learning_classification_samples)
    
    predict_classification_sample1 = MockClassificationSample(POS_CLASS, -7)
    predict_classification_sample2 = MockClassificationSample(NEG_CLASS, 9)
    predict_classification_samples = (predict_classification_sample1, predict_classification_sample2)
    expected_result = (ClassificationPrediction(((OnlineInterface._POS_LABEL,3872.9599999999996), (OnlineInterface._NEG_LABEL, -3872.9599999999996))), 
                       ClassificationPrediction(((OnlineInterface._POS_LABEL,6408.32),(OnlineInterface._NEG_LABEL,-6408.32))),
                       )
    
    return (interface, predict_classification_samples), expected_result

def get_interface_test_save_model_data():
    avg = True
    warm_start = False
    alpha = 1.52
    configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "mock", "config": Mapping({"inverse_order": False})}),
                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                    "config": Mapping({"avg": avg, 
                                                                       "warm_start": warm_start, 
                                                                       "alpha": alpha, 
                                                                       "useless_field": "useless_value"}),
                                                    }),
                             })
    interface = OnlineInterface.from_configuration(configuration)
    classifier_select_configuration = Mapping(configuration.classifier)
    classifier_select_configuration.config.positive_class_label = 1
    classifier_select_configuration.config.negative_class_label = 0
    classifier = create_classifier_from_factory_configuration(classifier_select_configuration)
    classifier.coef_ = numpy.array([[-51.68,   0.  ,   3.04,   0.  ,   3.04,   0.  ,   0.  ,   0.  , 3.04,   0.  ,   0.  ,   0.  ,   3.04]], dtype=numpy.float)
    classifier._cumulative_coef_ = numpy.array([[-79.04,   0.  ,   6.08,   0.  ,   6.08,   0.  ,   0.  ,   0.  , 6.08,   0.  ,   0.  ,   0.  ,   6.08]], dtype=numpy.float)
    interface._classifier = classifier
    
    expected_features_names_file_content = "test_numeric_feature\n[test_categorical_feature1]__(yes)\n[test_categorical_feature1]__(no)\n[test_categorical_feature2]__(yep)\n[test_categorical_feature2]__(nope)\n[product_[test_categorical_feature1]_[test_categorical_feature2]]__((yes)_(yep))\n[product_[test_categorical_feature1]_[test_categorical_feature2]]__((yes)_(nope))\n[product_[test_categorical_feature1]_[test_categorical_feature2]]__((no)_(yep))\n[product_[test_categorical_feature1]_[test_categorical_feature2]]__((no)_(nope))\n[product_[test_categorical_feature2]_[test_categorical_feature1]]__((yep)_(yes))\n[product_[test_categorical_feature2]_[test_categorical_feature1]]__((yep)_(no))\n[product_[test_categorical_feature2]_[test_categorical_feature1]]__((nope)_(yes))\n[product_[test_categorical_feature2]_[test_categorical_feature1]]__((nope)_(no))"
    expected_classifier = create_classifier_from_factory_configuration(classifier_select_configuration)
    expected_classifier.coef_ = numpy.array([[-51.68,   0.  ,   3.04,   0.  ,   3.04,   0.  ,   0.  ,   0.  , 3.04,   0.  ,   0.  ,   0.  ,   3.04]], dtype=numpy.float)
    expected_classifier._cumulative_coef_ = numpy.array([[-79.04,   0.  ,   6.08,   0.  ,   6.08,   0.  ,   0.  ,   0.  , 6.08,   0.  ,   0.  ,   0.  ,   6.08]], dtype=numpy.float)
    
    return interface, (expected_features_names_file_content, expected_classifier)

def get_interface_test_load_model_data():
    avg = True
    warm_start = False
    alpha = 1.52
    configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "mock", "config": Mapping({"inverse_order": False})}),
                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                    "config": Mapping({"avg": avg, 
                                                                       "warm_start": warm_start, 
                                                                       "alpha": alpha, 
                                                                       "useless_field": "useless_value"}),
                                                    }),
                             })
    interface = OnlineInterface.from_configuration(configuration)
    
    expected_interface = OnlineInterface.from_configuration(configuration)
    expected_classifier_select_configuration = Mapping(configuration.classifier)
    expected_classifier_select_configuration.config.positive_class_label = 1
    expected_classifier_select_configuration.config.negative_class_label = 0
    expected_classifier = create_classifier_from_factory_configuration(expected_classifier_select_configuration)
    expected_classifier.coef_ = numpy.array([[-51.68,   0.  ,   3.04,   0.  ,   3.04,   0.  ,   0.  ,   0.  , 3.04,   0.  ,   0.  ,   0.  ,   3.04]], dtype=numpy.float)
    expected_classifier._cumulative_coef_ = numpy.array([[-79.04,   0.  ,   6.08,   0.  ,   6.08,   0.  ,   0.  ,   0.  , 6.08,   0.  ,   0.  ,   0.  ,   6.08]], dtype=numpy.float)
    expected_interface._classifier = expected_classifier
    
    features_names_file_content = "test_numeric_feature\n[test_categorical_feature1]__(yes)\n[test_categorical_feature1]__(no)\n[test_categorical_feature2]__(yep)\n[test_categorical_feature2]__(nope)\n[product_[test_categorical_feature1]_[test_categorical_feature2]]__((yes)_(yep))\n[product_[test_categorical_feature1]_[test_categorical_feature2]]__((yes)_(nope))\n[product_[test_categorical_feature1]_[test_categorical_feature2]]__((no)_(yep))\n[product_[test_categorical_feature1]_[test_categorical_feature2]]__((no)_(nope))\n[product_[test_categorical_feature2]_[test_categorical_feature1]]__((yep)_(yes))\n[product_[test_categorical_feature2]_[test_categorical_feature1]]__((yep)_(no))\n[product_[test_categorical_feature2]_[test_categorical_feature1]]__((nope)_(yes))\n[product_[test_categorical_feature2]_[test_categorical_feature1]]__((nope)_(no))"
    classifier = create_classifier_from_factory_configuration(expected_classifier_select_configuration)
    classifier.coef_ = numpy.array([[-51.68,   0.  ,   3.04,   0.  ,   3.04,   0.  ,   0.  ,   0.  , 3.04,   0.  ,   0.  ,   0.  ,   3.04]], dtype=numpy.float)
    classifier._cumulative_coef_ = numpy.array([[-79.04,   0.  ,   6.08,   0.  ,   6.08,   0.  ,   0.  ,   0.  , 6.08,   0.  ,   0.  ,   0.  ,   6.08]], dtype=numpy.float)
    
    return (interface, (features_names_file_content, classifier)), expected_interface



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()