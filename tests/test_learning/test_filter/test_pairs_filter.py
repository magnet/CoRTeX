# -*- coding: utf-8 -*-

import unittest

from cortex.learning.filter.pairs import (AcceptAllPairFilter, RecallPairFilter, DistancePairFilter, 
                                             SameGenderPairFilter, SameNumberPairFilter, 
                                             SameEntityTypePairFilter, NotCopulaPairFilter, 
                                             PairFiltersCombination, create_pair_filter_from_factory_configuration)
from cortex.tools import Mapping
from cortex.utils.memoize import Memoized

#@unittest.skip
class TestAcceptAllPairFilter(unittest.TestCase):
    pass


#@unittest.skip
class TestRecallPairFilter(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        attribute_name_expected_result_pairs = []
    
        max_mention_distance =  9
        attribute_name_expected_result_pairs.append(("max_mention_distance",max_mention_distance))
        
        configuration = Mapping({"witness_field": "witness_value"})
        attribute_name_expected_result_pairs.append(("configuration",configuration))
        
        args = (max_mention_distance,)
        kwargs = {"configuration": configuration}
        
        # Test
        pair_filter = RecallPairFilter(*args, **kwargs)
        for attribute_name, expected_result in attribute_name_expected_result_pairs:
            self.assertEqual(expected_result, getattr(pair_filter, attribute_name), attribute_name)
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        max_mention_distance =  9
        pair_filter1 = RecallPairFilter(max_mention_distance, configuration=Mapping({"witness_field": "witness_value"}))
        pair_filter2 = RecallPairFilter(max_mention_distance, configuration=Mapping({"witness_field": "witness_value"}))
        max_mention_distance =  5
        pair_filter3 = RecallPairFilter(max_mention_distance, configuration=Mapping({"witness_field": "witness_value"}))
        
        # Test
        self.assertTrue(*pair_filter1._inner_eq(pair_filter2))
        self.assertEqual((False,"max_mention_distance"), pair_filter1._inner_eq(pair_filter3))
    
    #@unittest.skip
    def test_from_configuration(self):
        # Test parameters
        max_mention_distance =  9
        configuration = Mapping({"max_mention_distance": max_mention_distance, "useless_field": "useless_value"})
        expected_result = RecallPairFilter(max_mention_distance, configuration=Mapping({"useless_field": "useless_value"}))
        
        # Test
        actual_result = RecallPairFilter.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_accept(self):
        # Test parameters
        mention_pairs_iterable, pair_filter, expected_result = get_recall_pair_filter_test_accept_data()
        
        # Test
        actual_result = tuple(pair_filter.accept(am, sm) for am, sm in mention_pairs_iterable)
        self.assertEqual(expected_result, actual_result)


#@unittest.skip
class TestDistancePairFilter(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        attribute_name_expected_result_pairs = []
    
        max_mention_distance =  9
        attribute_name_expected_result_pairs.append(("max_mention_distance",max_mention_distance))
        
        configuration = Mapping({"witness_field": "witness_value"})
        attribute_name_expected_result_pairs.append(("configuration",configuration))
        
        args = (max_mention_distance,)
        kwargs = {"configuration": configuration}
        
        # Test
        pair_filter = DistancePairFilter(*args, **kwargs)
        for attribute_name, expected_result in attribute_name_expected_result_pairs:
            self.assertEqual(expected_result, getattr(pair_filter, attribute_name), attribute_name)
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        max_mention_distance =  9
        pair_filter1 = DistancePairFilter(max_mention_distance, configuration=Mapping({"witness_field": "witness_value"}))
        pair_filter2 = DistancePairFilter(max_mention_distance, configuration=Mapping({"witness_field": "witness_value"}))
        max_mention_distance =  5
        pair_filter3 = DistancePairFilter(max_mention_distance, configuration=Mapping({"witness_field": "witness_value"}))
        
        # Test
        self.assertTrue(*pair_filter1._inner_eq(pair_filter2))
        self.assertEqual((False,"max_mention_distance"), pair_filter1._inner_eq(pair_filter3))
    
    #@unittest.skip
    def test_from_configuration(self):
        # Test parameters
        max_mention_distance =  9
        configuration = Mapping({"max_mention_distance": max_mention_distance, "useless_field": "useless_value"})
        expected_result = DistancePairFilter(max_mention_distance, configuration=Mapping({"useless_field": "useless_value"}))
        
        # Test
        actual_result = DistancePairFilter.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        max_mention_distance = 18.98
        args = (max_mention_distance,)
        kwargs = {}
        expected_result = Mapping({"max_mention_distance": max_mention_distance})
        
        # Test
        actual_result = RecallPairFilter.define_configuration(*args, **kwargs)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip
    def test_accept(self):
        # Test parameters
        mention_pairs_iterable, pair_filter, expected_result = get_distance_pair_filter_test_accept_data()
        
        # Test
        actual_result = tuple(pair_filter.accept(am, sm) for am, sm in mention_pairs_iterable)
        self.assertEqual(expected_result, actual_result)


#@unittest.skip
class TestSameEntityTypePairFilter(unittest.TestCase):
    
    #@unittest.skip
    def test_accept(self):
        # Test parameters
        mention_pairs_iterable, pair_filter, expected_result = get_same_entity_type_pair_filter_test_accept_data()
        
        # Test
        actual_result = tuple(pair_filter.accept(am, sm) for am, sm in mention_pairs_iterable)
        self.assertEqual(expected_result, actual_result)


#@unittest.skip
class TestSameGenderPairFilter(unittest.TestCase):
    
    #@unittest.skip
    def test_accept(self):
        # Test parameters
        mention_pairs_iterable, pair_filter, expected_result = get_same_gender_pair_filter_test_accept_data()
        
        # Test
        actual_result = tuple(pair_filter.accept(am, sm) for am, sm in mention_pairs_iterable)
        self.assertEqual(expected_result, actual_result)


#@unittest.skip
class TestSameNumberPairFilter(unittest.TestCase):
    
    #@unittest.skip
    def test_accept(self):
        # Test parameters
        mention_pairs_iterable, pair_filter, expected_result = get_same_number_pair_filter_test_accept_data()
        
        # Test
        actual_result = tuple(pair_filter.accept(am, sm) for am, sm in mention_pairs_iterable)
        self.assertEqual(expected_result, actual_result)


#@unittest.skip
class TestNotCopulaPairFilter(unittest.TestCase):
    
    #@unittest.skip
    def test_accept(self):
        # Test parameters
        mention_pairs_iterable, pair_filter, expected_result = get_not_copula_pair_filter_test_accept_data()
        
        # Test
        actual_result = tuple(pair_filter.accept(am, sm) for am, sm in mention_pairs_iterable)
        self.assertEqual(expected_result, actual_result)



#@unittest.skip
class TesPairFiltersCombination(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        pair_filter1 = AcceptAllPairFilter(configuration=Mapping({"witness_field2": "witness_value2"}))
        pair_filters = (pair_filter1, )
        configuration = Mapping({"witness_field3": "witness_value3"})
        expected_configuration = Mapping({"filters": [Mapping({"name": "AcceptAllPairFilter", 
                                                               "config": Mapping({"witness_field2": "witness_value2"})}),
                                                      ],
                                        "witness_field3": "witness_value3"
                                        })
        
        # Test
        actual_pair_filters_combination = PairFiltersCombination(pair_filters, configuration=configuration)
        self.assertEqual(pair_filters, actual_pair_filters_combination.filters)
        self.assertEqual(expected_configuration, actual_pair_filters_combination.configuration)
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        pair_filters1 = (AcceptAllPairFilter(configuration=Mapping({"witness_field": "witness_value"})), 
                        AcceptAllPairFilter(configuration=Mapping({"witness_field2": "witness_value2"})),
                        )
        configuration = Mapping({"witness_field3": "witness_value3"})
        pair_filters_combination1 = PairFiltersCombination(pair_filters1, configuration=configuration)
        pair_filters_combination2 = PairFiltersCombination(pair_filters1, configuration=configuration)
        pair_filters2 = (AcceptAllPairFilter(configuration=Mapping({"witness_field4": "witness_value4"})), 
                        AcceptAllPairFilter(configuration=Mapping({"witness_field2": "witness_value2"})),
                        )
        pair_filters_combination3 = PairFiltersCombination(pair_filters2, configuration=configuration)
        
        # Test
        self.assertTrue(*pair_filters_combination1._inner_eq(pair_filters_combination2))
        self.assertEqual((False,"filters"), pair_filters_combination1._inner_eq(pair_filters_combination3))
    
    #@unittest.skip
    def test_from_configuration(self):
        # Test parameters
        configuration = Mapping({"filters": [Mapping({"name": "AcceptAllPairFilter", 
                                                      "config": Mapping({"witness_field2": "witness_value2"})}),
                                             ],
                               "witness_field3": "witness_value3"
                               })
        pair_filters = (AcceptAllPairFilter(configuration=Mapping({"witness_field2": "witness_value2"})), 
                        )
        expected_result = PairFiltersCombination(pair_filters, configuration=Mapping({"witness_field3": "witness_value3"}))
        
        # Test
        actual_result = PairFiltersCombination.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        data = []
        ## Test 1
        args = tuple()
        kwargs = {}
        expected_result = Mapping({"filters": [AcceptAllPairFilter.define_factory_configuration()]})
        data.append(((args, kwargs), expected_result))
        ## Test 2
        create_pair_filter_from_factory_configuration_configurations = [RecallPairFilter.define_factory_configuration(5)]
        args = tuple()
        kwargs = {"create_pair_filter_from_factory_configuration_configurations": create_pair_filter_from_factory_configuration_configurations}
        expected_result = Mapping({"filters": create_pair_filter_from_factory_configuration_configurations})
        
        # Test
        for (args, kwargs), expected_result in data:
            actual_result = PairFiltersCombination.define_configuration(*args, **kwargs)
            self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip
    def test_accept(self):
        # Test parameters
        mention_pairs_iterable, pair_filter, expected_result = get_pair_filters_combination_test_accept_data()
        
        # Test
        actual_result = tuple(pair_filter.accept(am, sm) for am, sm in mention_pairs_iterable)
        self.assertEqual(expected_result, actual_result)



#@unittest.skip
class Test(unittest.TestCase):
    
    #@unittest.skip
    def test_create_pair_filter_from_factory_configuration(self):
        # Test parameters
        data = []
        
        configuration = Mapping({"name": "acceptall", "config": Mapping({"witness_field2": "witness_value2"})})
        expected_result = AcceptAllPairFilter(configuration=Mapping({"witness_field2": "witness_value2"}))
        data.append((configuration, expected_result))
        
        max_mention_distance = 42
        configuration = Mapping({"name": "recall", "config": Mapping({"witness_field2": "witness_value2", "max_mention_distance": max_mention_distance})})
        expected_result = RecallPairFilter(max_mention_distance, configuration=Mapping({"witness_field2": "witness_value2", "max_mention_distance": max_mention_distance}))
        data.append((configuration, expected_result))
        
        max_mention_distance = 45
        configuration = Mapping({"name": "distance", "config": Mapping({"witness_field2": "witness_value2", "max_mention_distance": max_mention_distance})})
        expected_result = DistancePairFilter(max_mention_distance, configuration=Mapping({"witness_field2": "witness_value2", "max_mention_distance": max_mention_distance}))
        data.append((configuration, expected_result))
        
        configuration = Mapping({"name": "sameentitytype", "config": Mapping({"witness_field2": "witness_value2"})})
        expected_result = SameEntityTypePairFilter(configuration=Mapping({"witness_field2": "witness_value2"}))
        data.append((configuration, expected_result))
        
        configuration = Mapping({"name": "samegender", "config": Mapping({"witness_field2": "witness_value2"})})
        expected_result = SameGenderPairFilter(configuration=Mapping({"witness_field2": "witness_value2"}))
        data.append((configuration, expected_result))
        
        configuration = Mapping({"name": "samenumber", "config": Mapping({"witness_field2": "witness_value2"})})
        expected_result = SameNumberPairFilter(configuration=Mapping({"witness_field2": "witness_value2"}))
        data.append((configuration, expected_result))
        
        configuration = Mapping({"name": "notcopula", "config": Mapping({"witness_field2": "witness_value2"})})
        expected_result = NotCopulaPairFilter(configuration=Mapping({"witness_field2": "witness_value2"}))
        data.append((configuration, expected_result))
        
        max_mention_distance = 69
        configuration = Mapping({"name": "PairFiltersCombination", 
                               "config": Mapping({"filters": [Mapping({"name": "DistancePairFilter", 
                                                                       "config": Mapping({"max_mention_distance": max_mention_distance})}),
                                                              Mapping({"name": "sameentitytype",}),
                                                              ],
                                "witness_field3": "witness_value3",
                                })
                               })
        filters = (DistancePairFilter.from_configuration(Mapping({"max_mention_distance": max_mention_distance})),
                   SameEntityTypePairFilter.from_configuration(Mapping({})),
                   )
        expected_result = PairFiltersCombination(filters, configuration=Mapping({"witness_field3": "witness_value3"}))
        data.append((configuration, expected_result))
        
        # Test
        for configuration, expected_result in data:
            actual_result = create_pair_filter_from_factory_configuration(configuration)
            self.assertTrue(*expected_result._inner_eq(actual_result))



@Memoized
def get_test_document_data():
    import os
    from cortex.io.pivot_reader import PivotReader
    
    root_folder_path, file_name = os.path.split(__file__)
    file_name, _ = os.path.splitext(file_name)
    folder_path = os.path.join(root_folder_path, "{}_data".format(file_name))
    document_folder_path = os.path.join(folder_path, "mention_pair_filter_test_data", "ref")
    
    strict = True # We want to synchronize head tokens.
    test_document = PivotReader.parse(document_folder_path, strict=strict)
    
    return test_document

def get_recall_pair_filter_test_accept_data():
    test_document = get_test_document_data()
    
    max_mention_distance = 5
    pair_filter = RecallPairFilter.from_configuration(Mapping({"max_mention_distance": max_mention_distance}))
    mention_extent_pairs = (((1,9), (14,42), True),
                            ((14,42), (27,42), True),
                            ((1,9), (27,42), True),
                            ((27,42), (46,47), True),
                            ((14,42), (46,47), True),
                            ((1,9), (46,47), True),
                            ((46,47), (68,75), True),
                            ((27,42), (68,75), True),
                            ((14,42), (68,75), True),
                            ((1,9), (68,75), True),
                            ((68,75), (83,84), True),
                            ((46,47), (83,84), True),
                            ((27,42), (83,84), True),
                            ((14,42), (83,84), True),
                            ((1,9), (83,84), True),
                            ((83,84), (114,120), True),
                            ((68,75), (114,120), True),
                            ((46,47), (114,120), True),
                            ((27,42), (114,120), True),
                            ((14,42), (114,120), True),
                            ((1,9), (114,120), False),
                           )
    expected_result = tuple(b for _,_,b in mention_extent_pairs)
    mention_pairs_iterable = ((test_document.extent2mention[ae], test_document.extent2mention[se]) for ae,se,_ in mention_extent_pairs)
    
    return mention_pairs_iterable, pair_filter, expected_result

def get_distance_pair_filter_test_accept_data():
    test_document = get_test_document_data()
    
    max_mention_distance = 2
    pair_filter = DistancePairFilter.from_configuration(Mapping({"max_mention_distance": max_mention_distance}))
    mention_extent_pairs = (((1,9), (14,42), True),
                            ((14,42), (27,42), True),
                            ((1,9), (27,42), True),
                            ((27,42), (46,47), True),
                            ((14,42), (46,47), True),
                            ((1,9), (46,47), False),
                            ((46,47), (68,75), True),
                            ((27,42), (68,75), True),
                            ((14,42), (68,75), False),
                            ((1,9), (68,75), False),
                            ((68,75), (83,84), True),
                            ((46,47), (83,84), True),
                            ((27,42), (83,84), False),
                            ((14,42), (83,84), False),
                            ((1,9), (83,84), False),
                            ((83,84), (114,120), True),
                            ((68,75), (114,120), True),
                            ((46,47), (114,120), False),
                            ((27,42), (114,120), False),
                            ((14,42), (114,120), False),
                            ((1,9), (114,120), False),
                            )
    expected_result = tuple(b for _,_,b in mention_extent_pairs)
    mention_pairs_iterable = ((test_document.extent2mention[ae], test_document.extent2mention[se]) for ae,se,_ in mention_extent_pairs)
    
    return mention_pairs_iterable, pair_filter, expected_result

def get_same_entity_type_pair_filter_test_accept_data():
    test_document = get_test_document_data()
    
    pair_filter = SameEntityTypePairFilter.from_configuration(Mapping({}))
    mention_extent_pairs = (((1,9), (14,42), True),
                            ((14,42), (27,42), True),
                            ((1,9), (27,42), True),
                            ((27,42), (46,47), True),
                            ((14,42), (46,47), True),
                            ((1,9), (46,47), True),
                            ((46,47), (68,75), True),
                            ((27,42), (68,75), True),
                            ((14,42), (68,75), True),
                            ((1,9), (68,75), True),
                            ((68,75), (83,84), True),
                            ((46,47), (83,84), True),
                            ((27,42), (83,84), True),
                            ((14,42), (83,84), True),
                            ((1,9), (83,84), True),
                            ((83,84), (114,120), True),
                            ((68,75), (114,120), True),
                            ((46,47), (114,120), True),
                            ((27,42), (114,120), True),
                            ((14,42), (114,120), False),
                            ((1,9), (114,120), False),
                            )
    expected_result = tuple(b for _,_,b in mention_extent_pairs)
    mention_pairs_iterable = ((test_document.extent2mention[ae], test_document.extent2mention[se]) for ae,se,_ in mention_extent_pairs)
    
    return mention_pairs_iterable, pair_filter, expected_result


def get_same_gender_pair_filter_test_accept_data():
    test_document = get_test_document_data()
    
    pair_filter = SameGenderPairFilter.from_configuration(Mapping({}))
    mention_extent_pairs = (((1,9), (14,42), True),
                            ((14,42), (27,42), True),
                            ((1,9), (27,42), True),
                            ((27,42), (46,47), True),
                            ((14,42), (46,47), True),
                            ((1,9), (46,47), True),
                            ((46,47), (68,75), True),
                            ((27,42), (68,75), True),
                            ((14,42), (68,75), True),
                            ((1,9), (68,75), True),
                            ((68,75), (83,84), True),
                            ((46,47), (83,84), True),
                            ((27,42), (83,84), True),
                            ((14,42), (83,84), True),
                            ((1,9), (83,84), True),
                            ((83,84), (114,120), False),
                            ((68,75), (114,120), False),
                            ((46,47), (114,120), False),
                            ((27,42), (114,120), False),
                            ((14,42), (114,120), False),
                            ((1,9), (114,120), False),
                            )
    expected_result = tuple(b for _,_,b in mention_extent_pairs)
    mention_pairs_iterable = ((test_document.extent2mention[ae], test_document.extent2mention[se]) for ae,se,_ in mention_extent_pairs)
    
    return mention_pairs_iterable, pair_filter, expected_result


def get_same_number_pair_filter_test_accept_data():
    test_document = get_test_document_data()
    
    pair_filter = SameNumberPairFilter.from_configuration(Mapping({}))
    mention_extent_pairs = (((1,9), (14,42), True),
                            ((14,42), (27,42), True),
                            ((1,9), (27,42), True),
                            ((27,42), (46,47), True),
                            ((14,42), (46,47), True),
                            ((1,9), (46,47), True),
                            ((46,47), (68,75), True),
                            ((27,42), (68,75), True),
                            ((14,42), (68,75), True),
                            ((1,9), (68,75), True),
                            ((68,75), (83,84), True),
                            ((46,47), (83,84), True),
                            ((27,42), (83,84), True),
                            ((14,42), (83,84), True),
                            ((1,9), (83,84), True),
                            ((83,84), (114,120), True),
                            ((68,75), (114,120), True),
                            ((46,47), (114,120), True),
                            ((27,42), (114,120), True),
                            ((14,42), (114,120), True),
                            ((1,9), (114,120), True),
                            )
    expected_result = tuple(b for _,_,b in mention_extent_pairs)
    mention_pairs_iterable = ((test_document.extent2mention[ae], test_document.extent2mention[se]) for ae,se,_ in mention_extent_pairs)
    
    return mention_pairs_iterable, pair_filter, expected_result


def get_not_copula_pair_filter_test_accept_data():
    test_document = get_test_document_data()
    
    pair_filter = NotCopulaPairFilter.from_configuration(Mapping({}))
    mention_extent_pairs = (((1,9), (14,42), True),
                            ((14,42), (27,42), True),
                            ((1,9), (27,42), True),
                            ((27,42), (46,47), True),
                            ((14,42), (46,47), True),
                            ((1,9), (46,47), True),
                            ((46,47), (68,75), True),
                            ((27,42), (68,75), True),
                            ((14,42), (68,75), True),
                            ((1,9), (68,75), True),
                            ((68,75), (83,84), True),
                            ((46,47), (83,84), True),
                            ((27,42), (83,84), True),
                            ((14,42), (83,84), True),
                            ((1,9), (83,84), True),
                            ((83,84), (114,120), True),
                            ((68,75), (114,120), True),
                            ((46,47), (114,120), True),
                            ((27,42), (114,120), True),
                            ((14,42), (114,120), True),
                            ((1,9), (114,120), True),
                            )
    expected_result = tuple(b for _,_,b in mention_extent_pairs)
    mention_pairs_iterable = ((test_document.extent2mention[ae], test_document.extent2mention[se]) for ae,se,_ in mention_extent_pairs)
    
    return mention_pairs_iterable, pair_filter, expected_result


def get_pair_filters_combination_test_accept_data():
    test_document = get_test_document_data()
    
    max_mention_distance = 2
    configuration = Mapping({"filters": [Mapping({"name": "DistancePairFilter", 
                                                  "config": Mapping({"max_mention_distance": max_mention_distance})}),
                                         Mapping({"name": "SameGenderPairFilter",}),
                                        ],
                           })
    pair_filter = PairFiltersCombination.from_configuration(configuration)
    mention_extent_pairs = (((1,9), (14,42), True),
                            ((14,42), (27,42), True),
                            ((1,9), (27,42), True),
                            ((27,42), (46,47), True),
                            ((14,42), (46,47), True),
                            ((1,9), (46,47), False),
                            ((46,47), (68,75), True),
                            ((27,42), (68,75), True),
                            ((14,42), (68,75), False),
                            ((1,9), (68,75), False),
                            ((68,75), (83,84), True),
                            ((46,47), (83,84), True),
                            ((27,42), (83,84), False),
                            ((14,42), (83,84), False),
                            ((1,9), (83,84), False),
                            ((83,84), (114,120), False),
                            ((68,75), (114,120), False),
                            ((46,47), (114,120), False),
                            ((27,42), (114,120), False),
                            ((14,42), (114,120), False),
                            ((1,9), (114,120), False),
                            )
    expected_result = tuple(b for _,_,b in mention_extent_pairs)
    mention_pairs_iterable = ((test_document.extent2mention[ae], test_document.extent2mention[se]) for ae,se,_ in mention_extent_pairs)
    
    return mention_pairs_iterable, pair_filter, expected_result


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()