# -*- coding: utf-8 -*-

import unittest
 
from cortex.learning.filter.mentions import (AcceptAllMentionFilter, NonReferentialMentionFilter, 
                                               MentionFiltersCombination, create_mention_filter_from_factory_configuration)
from cortex.tools import Mapping

class TestNonReferentialMentionFilter(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        mention_filter_config = Mapping()
        referential_probability_threshold = 0.3
        expected_result_filter = 0.3
        expected_result_config = 0.3
        
        # Test
        mention_filter = NonReferentialMentionFilter(referential_probability_threshold=referential_probability_threshold
                                                     , configuration=mention_filter_config)
        self.assertEqual(mention_filter_config.referential_probability_threshold, expected_result_config)
        self.assertEqual(mention_filter.referential_probability_threshold, expected_result_filter)
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        mention_filter1 = NonReferentialMentionFilter(referential_probability_threshold=0.3, configuration=Mapping({"witness_field": "witness_value"}))
        mention_filter2 = NonReferentialMentionFilter(referential_probability_threshold=0.3, configuration=Mapping({"witness_field": "witness_value"}))
        mention_filter3 = NonReferentialMentionFilter(referential_probability_threshold=0.31, configuration=Mapping({"witness_field": "witness_value"}))
        
        # Test
        self.assertTrue(*mention_filter1._inner_eq(mention_filter2))
        self.assertEqual((False,"referential_probability_threshold"), mention_filter1._inner_eq(mention_filter3))
    
    #@unittest.skip
    def test_from_configuration(self):
        # Test parameters
        configuration = Mapping({"referential_probability_threshold": 0.3, "useless_field": "useless_value"})
        expected_result = NonReferentialMentionFilter(referential_probability_threshold=0.3, 
                                                      configuration=Mapping({"useless_field": "useless_value"}))
        
        # Test
        actual_result = NonReferentialMentionFilter.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        referential_probability_threshold = 0.76
        args = tuple()
        kwargs = {"referential_probability_threshold": referential_probability_threshold}
        expected_result = Mapping({"referential_probability_threshold": referential_probability_threshold})
        
        # Test
        actual_result = NonReferentialMentionFilter.define_configuration(*args, **kwargs)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    


class TesMentionFiltersCombination(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        referential_probability_threshold = 0.3
        mention_filter1 = NonReferentialMentionFilter(referential_probability_threshold=referential_probability_threshold
                                                      , configuration=Mapping({"witness_field": "witness_value"}))
        mention_filter2 = AcceptAllMentionFilter(configuration=Mapping({"witness_field2": "witness_value2"}))
        mention_filters = (mention_filter1, mention_filter2)
        configuration = Mapping({"witness_field3": "witness_value3"})
        expected_configuration = Mapping({"filters": [Mapping({"name": "NonReferentialMentionFilter", 
                                                               "config": Mapping({"referential_probability_threshold": 0.3, "witness_field": "witness_value"})}), 
                                                      Mapping({"name": "AcceptAllMentionFilter", 
                                                               "config": Mapping({"witness_field2": "witness_value2"})}),
                                                      ],
                                          "witness_field3": "witness_value3"
                                        })
        
        # Test
        actual_mention_filters_combination = MentionFiltersCombination(mention_filters, configuration=configuration)
        self.assertEqual(mention_filters, actual_mention_filters_combination.filters)
        self.assertEqual(expected_configuration, actual_mention_filters_combination.configuration)
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        referential_probability_threshold = 0.3
        mention_filters1 = (NonReferentialMentionFilter(referential_probability_threshold=referential_probability_threshold, 
                                                        configuration=Mapping({"witness_field": "witness_value"})), 
                            AcceptAllMentionFilter(configuration=Mapping({"witness_field2": "witness_value2"})),
                            )
        configuration = Mapping({"witness_field3": "witness_value3"})
        mention_filters_combination1 = MentionFiltersCombination(mention_filters1, configuration=configuration)
        mention_filters_combination2 = MentionFiltersCombination(mention_filters1, configuration=configuration)
        mention_filters2 = (NonReferentialMentionFilter(referential_probability_threshold=referential_probability_threshold, 
                                                        configuration=Mapping({"witness_field3": "witness_value3"})), 
                            AcceptAllMentionFilter(configuration=Mapping({"witness_field2": "witness_value2"})),
                            )
        mention_filters_combination3 = MentionFiltersCombination(mention_filters2, configuration=configuration)
        
        # Test
        self.assertTrue(*mention_filters_combination1._inner_eq(mention_filters_combination2))
        self.assertEqual((False,"filters"), mention_filters_combination1._inner_eq(mention_filters_combination3))
    
    #@unittest.skip
    def test_from_configuration(self):
        # Test parameters
        configuration = Mapping({"filter": [Mapping({"name": "NonReferentialMentionFilter", 
                                                    "config": Mapping({"referential_probability_threshold": 0.3, 
                                                                     "witness_field": "witness_value"})}), 
                                            Mapping({"name": "AcceptAllMentionFilter", 
                                                     "config": Mapping({"witness_field2": "witness_value2"})}),
                                            ],
                               "witness_field3": "witness_value3"
                               })
        mention_filters = (NonReferentialMentionFilter(referential_probability_threshold=0.3, 
                                                        configuration=Mapping({"witness_field": "witness_value"})), 
                            AcceptAllMentionFilter(configuration=Mapping({"witness_field2": "witness_value2"})),
                            )
        expected_result = MentionFiltersCombination(mention_filters, configuration=configuration)
        
        # Test
        actual_result = MentionFiltersCombination.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        data = []
        ## Test 1
        args = tuple()
        kwargs = {}
        expected_result = Mapping({"filters": [AcceptAllMentionFilter.define_factory_configuration()]})
        data.append(((args, kwargs), expected_result))
        ## Test 2
        create_mention_filter_from_factory_configuration_configurations = [NonReferentialMentionFilter.define_factory_configuration(0.34)]
        args = tuple()
        kwargs = {"create_mention_filter_from_factory_configuration_configurations": create_mention_filter_from_factory_configuration_configurations}
        expected_result = Mapping({"filters": create_mention_filter_from_factory_configuration_configurations})
        
        # Test
        for (args, kwargs), expected_result in data:
            actual_result = MentionFiltersCombination.define_configuration(*args, **kwargs)
            self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))



#@unittest.skip
class Test(unittest.TestCase):
    
    #@unittest.skip
    def test_create_mention_filter_from_factory_configuration(self):
        # Test parameters
        data = []
        
        referential_probability_threshold = 0.75
        configuration = Mapping({"name": "nonreferential", 
                               "config": Mapping({"referential_probability_threshold": referential_probability_threshold, 
                                                "witness_field": "witness_value"})})
        expected_result = NonReferentialMentionFilter.from_configuration(Mapping({"referential_probability_threshold": referential_probability_threshold, 
                                                                                "witness_field": "witness_value"}))
        data.append((configuration, expected_result))
        
        configuration = Mapping({"name": "MentionFiltersCombination", 
                               "config": Mapping({"filters": [Mapping({"name": "nonreferential", 
                                                                                     "config": Mapping({"referential_probability_threshold": 0.75, 
                                                                                                              "witness_field": "witness_value"})
                                                                                     }), 
                                                                              Mapping({"name": "acceptall", 
                                                                                     "config": Mapping({"witness_field4": "witness_value4"})
                                                                                     }),
                                                                              ],
                                                "witness_field3": "witness_value3",})
                               })
        filters = (NonReferentialMentionFilter.from_configuration(Mapping({"referential_probability_threshold": 0.75, "witness_field": "witness_value"})),
                   AcceptAllMentionFilter.from_configuration(Mapping({"witness_field4": "witness_value4"})),
                   )
        expected_result = MentionFiltersCombination(filters, configuration=Mapping({"witness_field3": "witness_value3"}))
        data.append((configuration, expected_result))
        
        # Test
        for configuration, expected_result in data:
            actual_result = create_mention_filter_from_factory_configuration(configuration)
            self.assertTrue(*expected_result._inner_eq(actual_result))



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()