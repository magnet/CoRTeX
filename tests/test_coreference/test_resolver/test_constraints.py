# -*- coding: utf-8 -*-

import unittest
import os

from cortex.tools import Mapping
from cortex.utils.memoize import Memoized

from cortex.coreference.resolver.structured_pairs import ConstrainedEdgesDefinerMixIn

#@unittest.skip
class TestConstrainedEdgesDefiner(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        #Test parameters
        (args, kwargs), attribute_name_expected_result_pairs = get_constrained_edges_definer_test___init___data()
        
        # Test
        resolver = ConstrainedEdgesDefinerMixIn(*args, **kwargs)
        for attribute_name, expected_result in attribute_name_expected_result_pairs:
            actual_result = getattr(resolver, attribute_name)
            self.assertEqual(expected_result, actual_result, attribute_name)
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        data = get_constrained_edges_definer_test__inner_eq_data()
        
        # Test
        for resolver1, resolver2, expected_result in data:
            self.assertEqual(expected_result, resolver1._inner_eq(resolver2))
    
    #@unittest.skip
    def test_from_configuration(self):
        # Test parameters
        configuration, expected_result = get_constrained_edges_definer_test_from_configuration_data()
        
        # Test
        actual_result = ConstrainedEdgesDefinerMixIn.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = ConstrainedEdgesDefinerMixIn
        
        constraints_definition = [("anaphoricity", tuple(), {"entity_head_extents": None}),
                                  ("animated", tuple(), {"entity_head_extents": None}),
                                  ]
        
        args = tuple()
        kwargs = {"constraints_definition": constraints_definition,
                  }
        expected_result = Mapping({"constraints_definition": constraints_definition,
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip
    def test_get_constraints(self):
        # Test parameters
        data = get_constrained_edges_definer_test_get_constraints_data()
        
        # Test
        for constrained_edges_definer, (args, kwargs), (expected_positive_edges, expected_negative_edges) in data:
            actual_positive_edges, actual_negative_edges = constrained_edges_definer.get_constraints(*args, **kwargs)
            self.assertEqual(expected_positive_edges, actual_positive_edges)
            self.assertEqual(expected_negative_edges, actual_negative_edges)



def get_test_document_data():
    from cortex.io.pivot_reader import PivotReader
    
    folder_path = os.path.join(os.path.dirname(__file__), "data", )
    document_folder_path = os.path.join(folder_path, "resolver_test_data", "ref")
    
    strict = True # We want to synchronize head tokens.
    test_document = PivotReader.parse(document_folder_path, strict=strict)
    
    return test_document

@Memoized
def get_memoized_test_document_data():
    return get_test_document_data()

def get_constrained_edges_definer_test___init___data():
    attribute_name_expected_result_pairs = []
    
    constraints_definition = (("anaphoricity", tuple(), {}), 
                              ("rules_english_sieves", tuple(), {}),
                              )
    expected_constraints_definition = tuple(constraints_definition)
    attribute_name_expected_result_pairs.append(("constraints_definition",expected_constraints_definition))
    
    configuration = Mapping({"test_field": "test_value"})
    expected_configuration = Mapping({"test_field": "test_value", "constraints_definition": constraints_definition})
    attribute_name_expected_result_pairs.append(("configuration",expected_configuration))
    
    args = (constraints_definition,)
    kwargs = {"configuration": configuration}
    
    return (args, kwargs), attribute_name_expected_result_pairs


def get_constrained_edges_definer_test__inner_eq_data():
    data = []
    
    (args, kwargs), _ = get_constrained_edges_definer_test___init___data() #attribute_name_expected_result_pairs
    
    constrained_edges_definer1 = ConstrainedEdgesDefinerMixIn(*args, **kwargs)
    constrained_edges_definer2 = ConstrainedEdgesDefinerMixIn(*args, **kwargs)
    expected_result = (True, None)
    data.append((constrained_edges_definer1, constrained_edges_definer2, expected_result))
    
    args = (args[0][:1],)
    constrained_edges_definer3 = ConstrainedEdgesDefinerMixIn(*args, **kwargs)
    expected_result = (False, "constraints_definition")
    data.append((constrained_edges_definer1, constrained_edges_definer3, expected_result))
    
    return data


def get_constrained_edges_definer_test_from_configuration_data():
    constraints_definition = (("anaphoricity", tuple(), {}), 
                              ("rules_english_sieves", tuple(), {}),
                              )
    configuration = Mapping({"test_field": "test_value"})
    
    args = (constraints_definition,)
    kwargs = {"configuration": configuration}
    expected_result = ConstrainedEdgesDefinerMixIn(*args, **kwargs)
    
    configuration = Mapping({"test_field": "test_value", "constraints_definition": constraints_definition})
    
    return configuration, expected_result


def get_constrained_edges_definer_test_get_constraints_data():
    data = []
    
    document = get_memoized_test_document_data()
    entity_head_extents = None
    args = (document,)
    kwargs = {}#{"entity_head_extents": entity_head_extents}
    
    #
    constraints_definition = (("rules_english_sieves", tuple(), {}),
                              ("anaphoricity", tuple(), {}),
                              ("proper_match", tuple(), {}),
                              ("animated", tuple(), {}),
                              ("embedding", tuple(), {}),
                              )
    constrained_edges_definer = ConstrainedEdgesDefinerMixIn(constraints_definition)
    expected_positive_edges = tuple()
    expected_negative_edges = (((14, 42), (27, 42)),)
    data.append((constrained_edges_definer, (args, kwargs), (expected_positive_edges, expected_negative_edges)))
    
    return data



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()