# -*- coding: utf-8 -*-

import unittest
import os

from cortex.coreference.resolver import create_resolver_from_factory_configuration, load_resolver
from cortex.coreference.resolver.pairs import (SoonResolver, 
                                               McCarthyLehnertResolver, 
                                               NgCardieResolver, 
                                               HACResolver,  
                                               OraclePairResolver, 
                                               OraclePairScoreResolver, 
                                               )
from cortex.coreference.resolver.rules import RulesResolver
from cortex.coreference.resolver.structured_pairs import (MSTResolver, 
                                                          HybridBestFirstMSTResolver, 
                                                          ConstrainedMSTResolver, 
                                                          ExtendedMSTResolver, 
                                                          TransitiveClosureStructureResolver, 
                                                          PositiveGraphStructureResolver, 
                                                          ClosestFirstDocumentStructuredResolver,
                                                          ExtendedClosestFirstDocumentStructuredResolver,
                                                          BestFirstDocumentStructuredResolver,
                                                          ExtendedBestFirstDocumentStructuredResolver,
                                                          ConstrainedBestFirstDocumentStructuredResolver,
                                                          ConstrainedExtendedBestFirstDocumentStructuredResolver,
                                                          )
from cortex.tools import Mapping


class Test(unittest.TestCase):
    
    #@unittest.skip
    def test_create_resolver_from_factory_configuration(self):
        # Test parameters
        data = get_test_create_resolver_from_factory_configuration_data()
        
        # Test
        for configuration, expected_result in data:
            actual_result = create_resolver_from_factory_configuration(configuration)
            self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_load_resolver(self):
        #Test parameters
        data = get_test_load_resolver_data()
        
        # Test
        for archive_file_path, expected_resolver in data:
            actual_resolver = load_resolver(archive_file_path)
            self.assertTrue(*expected_resolver._inner_eq(actual_resolver))


def get_test_create_resolver_from_factory_configuration_data():
    data = []
    
    # SoonResolver
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                          "with_singleton_features": False, 
                                                                                                          "with_anaphoricity_features": False, 
                                                                                                          "quantize": True, 
                                                                                                          "strict": True,
                                                                                                          "useless_field": "useless_value"})
                                                                                 }),
                                             "classifier": Mapping({"name": "logisticregressionsklearnbinary", 
                                                                   "config": Mapping({"penalty": "l2",
                                                                                    "dual": True,
                                                                                    "tol": 1e-3,
                                                                                    "C": 1.5,
                                                                                    "fit_intercept": True,
                                                                                    "intercept_scaling": 1,
                                                                                    "class_weight": "balanced",
                                                                                    "random_state": 84,
                                                                                    "solver": "liblinear",
                                                                                    "max_iter": 42,
                                                                                    "multi_class": "ovr",
                                                                                    })
                                                                    }),
                                                   "witness_field0": "witness_value0",
                                                   })
    model_interface_configuration = Mapping({"name": "SKLEARNInterface", "config": model_interface_configuration})
    generator_configuration = Mapping([("witness_field7", "witness_value7"),
                                         ("pair_filter", Mapping({"name": "PairFiltersCombination", 
                                                                    "config": Mapping({"filters": [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                          "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                          }),
                                                                                                                       ],
                                                                                     "witness_field6": "witness_value6",
                                                                                     }),
                                                                    })
                                          ),
                                         ])
    generator_configuration = Mapping({"name": "SoonGenerator", "config": generator_configuration})
    decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    decoder_configuration = Mapping({"name": "ClosestFirstCoreferenceDecoder", "config": decoder_configuration})
    
    configuration = Mapping({"model_interface": model_interface_configuration,
                           "generator": generator_configuration,
                           "decoder": decoder_configuration,
                           "witness_field9": "witness_value9",
                           })
    expected_result = SoonResolver.from_configuration(configuration)
    configuration = Mapping({"name": "soon", "config": configuration})
    data.append((configuration, expected_result))
    
    # McCarthyLehnertResolver
    with_singleton_features = False
    with_anaphoricity_features = False
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                      "with_singleton_features": with_singleton_features, 
                                                                                                      "with_anaphoricity_features": with_anaphoricity_features, 
                                                                                                      "quantize": True, 
                                                                                                      "useless_field": "useless_value"})
                                                                                 }),
                                             "classifier": Mapping({"name": "logisticregressionsklearnbinary", 
                                                                   "config": Mapping({"penalty": "l2",
                                                                                    "dual": True,
                                                                                    "tol": 1e-3,
                                                                                    "C": 1.5,
                                                                                    "fit_intercept": True,
                                                                                    "intercept_scaling": 1,
                                                                                    "class_weight": "balanced",
                                                                                    "random_state": 84,
                                                                                    "solver": "liblinear",
                                                                                    "max_iter": 42,
                                                                                    "multi_class": "ovr",
                                                                                    })
                                                                    }),
                                           })
    model_interface_configuration = Mapping({"name": "SKLEARNInterface", "config": model_interface_configuration})
    generator_configuration = Mapping([("pair_filter", Mapping({"name": "acceptall"})),])
    generator_configuration = Mapping({"name": "MentionPairSampleGenerator", "config": generator_configuration})
    decoder_configuration = Mapping({"name": "AggressiveMergeCoreferenceDecoder"})
    mention_filter_configuration = Mapping({"name": "NonReferentialMentionFilter", 
                                              "config": Mapping({"referential_probability_threshold": 0.3}),
                                              })
    
    configuration = Mapping({"model_interface": model_interface_configuration,
                               "generator": generator_configuration,
                               "decoder": decoder_configuration,
                               "mention_filter": mention_filter_configuration,
                               "witness_field9": "witness_value9",
                               })
    expected_result = McCarthyLehnertResolver.from_configuration(configuration)
    configuration = Mapping({"name": "mccarthylehnert", "config": configuration})
    data.append((configuration, expected_result))
    
    # NgCardieResolver
    with_singleton_features = False
    with_anaphoricity_features = False
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                      "with_singleton_features": with_singleton_features, 
                                                                                                      "with_anaphoricity_features": with_anaphoricity_features, 
                                                                                                      "quantize": True, 
                                                                                                      "useless_field": "useless_value"})
                                                                                 }),
                                             "classifier": Mapping({"name": "logisticregressionsklearnbinary", 
                                                                   "config": Mapping({"penalty": "l2",
                                                                                    "dual": True,
                                                                                    "tol": 1e-3,
                                                                                    "C": 1.5,
                                                                                    "fit_intercept": True,
                                                                                    "intercept_scaling": 1,
                                                                                    "class_weight": "balanced",
                                                                                    "random_state": 84,
                                                                                    "solver": "liblinear",
                                                                                    "max_iter": 42,
                                                                                    "multi_class": "ovr",
                                                                                    })
                                                                    }),
                                           })
    model_interface_configuration = Mapping({"name": "SKLEARNInterface", "config": model_interface_configuration})
    generator_configuration = Mapping([("pair_filter", Mapping({"name": "acceptall"})),])
    generator_configuration = Mapping({"name": "NgCardieGenerator", "config": generator_configuration})
    decoder_configuration = Mapping({"name": "BestFirstCoreferenceDecoder"})
    mention_filter_configuration = Mapping({"name": "NonReferentialMentionFilter", 
                                              "config": Mapping({"referential_probability_threshold": 0.3}),
                                              })
    
    configuration = Mapping({"model_interface": model_interface_configuration,
                           "generator": generator_configuration,
                           "decoder": decoder_configuration,
                           "mention_filter": mention_filter_configuration,
                           "witness_field9": "witness_value9",
                           })
    expected_result = NgCardieResolver.from_configuration(configuration)
    configuration = Mapping({"name": "ngcardie", "config": configuration})
    data.append((configuration, expected_result))
    
    # HACResolver
    with_singleton_features = False
    with_anaphoricity_features = False
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                      "with_singleton_features": with_singleton_features, 
                                                                                                      "with_anaphoricity_features": with_anaphoricity_features, 
                                                                                                      "quantize": True, 
                                                                                                      "useless_field": "useless_value"})
                                                                                 }),
                                             "classifier": Mapping({"name": "logisticregressionsklearnbinary", 
                                                                   "config": Mapping({"penalty": "l2",
                                                                                    "dual": True,
                                                                                    "tol": 1e-3,
                                                                                    "C": 1.5,
                                                                                    "fit_intercept": True,
                                                                                    "intercept_scaling": 1,
                                                                                    "class_weight": "balanced",
                                                                                    "random_state": 84,
                                                                                    "solver": "liblinear",
                                                                                    "max_iter": 42,
                                                                                    "multi_class": "ovr",
                                                                                    })
                                                                    }),
                                           })
    model_interface_configuration = Mapping({"name": "SKLEARNInterface", "config": model_interface_configuration})
    generator_configuration = Mapping([("pair_filter", Mapping({"name": "acceptall"})),])
    generator_configuration = Mapping({"name": "MentionPairSampleGenerator", "config": generator_configuration})
    decoder_configuration = Mapping({"clustering_type": "single_link"})
    decoder_configuration = Mapping({"name": "HACCoreferenceDecoder", "config": decoder_configuration})
    mention_filter_configuration = Mapping({"name": "NonReferentialMentionFilter", 
                                          "config": Mapping({"referential_probability_threshold": 0.3}),
                                          })
    
    configuration = Mapping({"model_interface": model_interface_configuration,
                           "generator": generator_configuration,
                           "decoder": decoder_configuration,
                           "mention_filter": mention_filter_configuration,
                           "witness_field9": "witness_value9",
                           })
    expected_result = HACResolver.from_configuration(configuration)
    configuration = Mapping({"name": "hac", "config": configuration})
    data.append((configuration, expected_result))
    
    # OraclePairResolver
    generator_configuration = Mapping([("witness_field7", "witness_value7"),
                                     ("pair_filter", Mapping({"name": "PairFiltersCombination", 
                                                            "config": Mapping({"filters": [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                  "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                  }),
                                                                                                       ],
                                                                                 "witness_field6": "witness_value6",
                                                                                 }),
                                                            })),
                                     ])
    generator_configuration = Mapping({"name": "MentionPairSampleGenerator", "config": generator_configuration})    
    configuration = Mapping({"generator": generator_configuration,
                           "witness_field9": "witness_value9",
                           })
    expected_result = OraclePairResolver.from_configuration(configuration)
    configuration = Mapping({"name": "oraclepair", "config": configuration})
    data.append((configuration, expected_result))
    
    # OraclePairScoreResolver
    with_singleton_features = False
    with_anaphoricity_features = False
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                  "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping({"witness_field2": "witness_value2"})}), 
                                                                                                   "with_singleton_features": with_singleton_features, 
                                                                                                   "with_anaphoricity_features": with_anaphoricity_features, 
                                                                                                   "quantize": True, 
                                                                                                   "strict": True,
                                                                                                   "useless_field": "useless_value"})
                                                                                 }),
                                             "classifier": Mapping({"name": "logisticregressionsklearnbinary", 
                                                                   "config": Mapping({"penalty": "l2",
                                                                                    "dual": True,
                                                                                    "tol": 1e-3,
                                                                                    "C": 1.5,
                                                                                    "fit_intercept": True,
                                                                                    "intercept_scaling": 1,
                                                                                    "class_weight": "balanced",
                                                                                    "random_state": 84,
                                                                                    "solver": "liblinear",
                                                                                    "max_iter": 42,
                                                                                    "multi_class": "ovr",
                                                                                    })
                                                                    }),
                                           "witness_field0": "witness_value0",
                                           })
    model_interface_configuration = Mapping({"name": "SKLEARNInterface", "config": model_interface_configuration})
    
    generator_configuration = Mapping([("witness_field7", "witness_value7"),
                                         ("pair_filter", Mapping({"name": "PairFiltersCombination", 
                                                                "config": Mapping({"filters": [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                      "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                      }),
                                                                                                               ],
                                                                                     "witness_field6": "witness_value6",
                                                                                 }),
                                                                })
                                          ),
                                         ])
    generator_configuration = Mapping({"name": "MentionPairSampleGenerator", "config": generator_configuration})
    
    decoder_configuration = Mapping()
    decoder_configuration = Mapping({"name": "OracleAggressiveMergeCoreferenceDecoder", "config": decoder_configuration})
    
    configuration = Mapping({"model_interface": model_interface_configuration,
                             "generator": generator_configuration,
                             "decoder": decoder_configuration, 
                             "witness_field9": "witness_value9",
                            })
    expected_result = OraclePairScoreResolver.from_configuration(configuration)
    configuration = Mapping({"name": "oraclepairscore", "config": configuration})
    data.append((configuration, expected_result))
    
    # RulesResolver
    rules_type = "english_sieves"
    rules_resolve_args = None
    rules_resolve_kwargs = {"sieve_filter_anaphoric": True}
    configuration = Mapping({"rules_type": rules_type,
                             "rules_resolve_args": rules_resolve_args,
                             "rules_resolve_kwargs": rules_resolve_kwargs,
                             "witness_field9": "witness_value9",
                            })
    expected_result = RulesResolver.from_configuration(configuration)
    configuration = Mapping({"name": "rules", "config": configuration})
    data.append((configuration, expected_result))
    
    
    ## MST
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "with_singleton_features": False, 
                                                                                                    "with_anaphoricity_features": False, 
                                                                                                    "quantize": True, 
                                                                                                    "strict": True,
                                                                                                    "useless_field": "useless_value"})
                                                                                 }), 
                                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                    "config": Mapping({"iteration_nb": 2, 
                                                                                       "avg": True, 
                                                                                       "warm_start": False, 
                                                                                       "alpha": 1.52, 
                                                                                       "useless_field": "useless_value"}),
                                                                    })
                                            })
    generator_configuration = Mapping([("pair_filter", Mapping([("name", "PairFiltersCombination"), 
                                                               ("config", Mapping([("filters", [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                       "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                       }),
                                                                                                                   ]),
                                                                                   ("witness_field6", "witness_value6"),
                                                                                   ])
                                                                ),
                                                                ])
                                      ),
                                     ("witness_field7", "witness_value7"),
                                     ])
    
    decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    root_loss = 1.2
    update_mode = "ML"
    configuration = Mapping({"model_interface": Mapping((("name", "OnlineInterface"), ("config", model_interface_configuration))),
                              "generator": Mapping((("name", "MentionPairSampleGenerator"), ("config", generator_configuration))),
                              "decoder": Mapping((("name", "MSTPrimCoreferenceDecoder"), ("config", decoder_configuration))),
                              "root_loss": root_loss, 
                              "update_mode": update_mode, 
                              "witness_field9": "witness_value9",
                             })
    expected_result = MSTResolver.from_configuration(configuration)
    configuration = Mapping({"name": "mst", "config": configuration})
    data.append((configuration, expected_result))
    
    # ExtendedMST
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "ExtendedMentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "mention_hierarchy": Mapping({"name": "mentiongramtype", "config": Mapping()}), 
                                                                                                    "with_singleton_features": False, 
                                                                                                    "with_anaphoricity_features": False, 
                                                                                                    "quantize": True, 
                                                                                                    "strict": True,
                                                                                                    "useless_field": "useless_value"})
                                                                                 }), 
                                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                    "config": Mapping({"iteration_nb": 2, 
                                                                                       "avg": True, 
                                                                                       "warm_start": False, 
                                                                                       "alpha": 1.52, 
                                                                                       "useless_field": "useless_value"}),
                                                                    })
                                            })
    generator_configuration = Mapping([("pair_filter", Mapping([("name", "PairFiltersCombination"), 
                                                               ("config", Mapping([("filters", [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                       "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                       }),
                                                                                                                   ]),
                                                                                   ("witness_field6", "witness_value6"),
                                                                                   ])
                                                                ),
                                                                ])
                                      ),
                                     ("witness_field7", "witness_value7"),
                                     ])
    
    decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    root_loss = 1.2
    update_mode = "ML"
    configuration = Mapping({"model_interface": Mapping((("name", "OnlineInterface"), ("config", model_interface_configuration))),
                              "generator": Mapping((("name", "ExtendedMentionPairSampleGenerator"), ("config", generator_configuration))),
                              "decoder": Mapping((("name", "MSTPrimCoreferenceDecoder"), ("config", decoder_configuration))),
                              "root_loss": root_loss, 
                              "update_mode": update_mode, 
                              "witness_field9": "witness_value9",
                             })
    expected_result = ExtendedMSTResolver.from_configuration(configuration)
    configuration = Mapping({"name": "extendedmst", "config": configuration})
    data.append((configuration, expected_result))
    
    ## HybridBestFirstMST
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "with_singleton_features": False, 
                                                                                                    "with_anaphoricity_features": False, 
                                                                                                    "quantize": True, 
                                                                                                    "strict": True,
                                                                                                    "useless_field": "useless_value"})
                                                                                 }), 
                                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                    "config": Mapping({"iteration_nb": 2, 
                                                                                       "avg": True, 
                                                                                       "warm_start": False, 
                                                                                       "alpha": 1.52, 
                                                                                       "useless_field": "useless_value"}),
                                                                    })
                                            })
    generator_configuration = Mapping([("pair_filter", Mapping([("name", "PairFiltersCombination"), 
                                                               ("config", Mapping([("filters", [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                       "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                       }),
                                                                                                                   ]),
                                                                                   ("witness_field6", "witness_value6"),
                                                                                   ])
                                                                ),
                                                                ])
                                      ),
                                     ("witness_field7", "witness_value7"),
                                     ])
    
    decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    root_loss = 1.2
    update_mode = "ML"
    configuration = Mapping({"model_interface": Mapping((("name", "OnlineInterface"), ("config", model_interface_configuration))),
                              "generator": Mapping((("name", "MentionPairSampleGenerator"), ("config", generator_configuration))),
                              "decoder": Mapping((("name", "HybridBestFirstMSTCoreferenceDecoder"), ("config", decoder_configuration))),
                              "root_loss": root_loss, 
                              "update_mode": update_mode, 
                              "witness_field9": "witness_value9",
                             })
    expected_result = HybridBestFirstMSTResolver.from_configuration(configuration)
    configuration = Mapping({"name": "hybridbestfirstmst", "config": configuration})
    data.append((configuration, expected_result))
    
    ## ConstrainedMST
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "with_singleton_features": False, 
                                                                                                    "with_anaphoricity_features": False, 
                                                                                                    "quantize": True, 
                                                                                                    "strict": True,
                                                                                                    "useless_field": "useless_value"})
                                                                                 }), 
                                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                    "config": Mapping({"iteration_nb": 2, 
                                                                                       "avg": True, 
                                                                                       "warm_start": False, 
                                                                                       "alpha": 1.52, 
                                                                                       "useless_field": "useless_value"}),
                                                                    })
                                            })
    generator_configuration = Mapping([("pair_filter", Mapping([("name", "PairFiltersCombination"), 
                                                               ("config", Mapping([("filters", [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                       "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                       }),
                                                                                                                   ]),
                                                                                   ("witness_field6", "witness_value6"),
                                                                                   ])
                                                                ),
                                                                ])
                                      ),
                                     ("witness_field7", "witness_value7"),
                                     ])
    decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    
    root_loss = 1.2
    
    update_mode = "PB"
    
    constraints_definition = (("rules_english_sieves", tuple(), {}),
                              ("anaphoricity", tuple(), {}),
                              ("proper_match", tuple(), {}),
                              ("animated", tuple(), {}),
                              ("embedding", tuple(), {}),
                              )
    
    configuration = Mapping({"model_interface": Mapping((("name", "OnlineInterface"), ("config", model_interface_configuration))),
                              "generator": Mapping((("name", "MentionPairSampleGenerator"), ("config", generator_configuration))),
                              "decoder": Mapping((("name", "MSTKruskalCoreferenceDecoder"), ("config", decoder_configuration))),
                              "root_loss": root_loss, 
                              "update_mode": update_mode, 
                              "constraints_definition": constraints_definition, 
                              "witness_field9": "witness_value9",
                             })
    
    expected_result = ConstrainedMSTResolver.from_configuration(configuration)
    
    configuration = Mapping({"name": "constrainedmst", "config": configuration})
    data.append((configuration, expected_result))
    
    ## TransitiveClosureStructureResolver
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "with_singleton_features": False, 
                                                                                                    "with_anaphoricity_features": False, 
                                                                                                    "quantize": True, 
                                                                                                    "strict": True,
                                                                                                    "useless_field": "useless_value"})
                                                                                 }), 
                                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                    "config": Mapping({"iteration_nb": 2, 
                                                                                       "avg": True, 
                                                                                       "warm_start": False, 
                                                                                       "alpha": 1.52, 
                                                                                       "useless_field": "useless_value"}),
                                                                    })
                                            })
    generator_configuration = Mapping([("pair_filter", Mapping([("name", "PairFiltersCombination"), 
                                                               ("config", Mapping([("filters", [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                       "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                       }),
                                                                                                                   ]),
                                                                                   ("witness_field6", "witness_value6"),
                                                                                   ])
                                                                ),
                                                                ])
                                      ),
                                     ("witness_field7", "witness_value7"),
                                     ])
    decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    
    configuration = Mapping({"model_interface": Mapping((("name", "OnlineInterface"), ("config", model_interface_configuration))),
                              "generator": Mapping((("name", "MentionPairSampleGenerator"), ("config", generator_configuration))),
                              "decoder": Mapping((("name", "AggressiveMergeCoreferenceDecoder"), ("config", decoder_configuration))),
                              "witness_field9": "witness_value9",
                             })
    
    expected_result = TransitiveClosureStructureResolver.from_configuration(configuration)
    
    configuration = Mapping({"name": "transitiveclosurestructure", "config": configuration})
    data.append((configuration, expected_result))
    
    ## PositiveGraphStructureResolver
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "with_singleton_features": False, 
                                                                                                    "with_anaphoricity_features": False, 
                                                                                                    "quantize": True, 
                                                                                                    "strict": True,
                                                                                                    "useless_field": "useless_value"})
                                                                                 }), 
                                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                    "config": Mapping({"iteration_nb": 2, 
                                                                                       "avg": True, 
                                                                                       "warm_start": False, 
                                                                                       "alpha": 1.52, 
                                                                                       "useless_field": "useless_value"}),
                                                                    })
                                            })
    generator_configuration = Mapping([("pair_filter", Mapping([("name", "PairFiltersCombination"), 
                                                               ("config", Mapping([("filters", [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                           "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                           }),
                                                                                                       ]),
                                                                                   ("witness_field6", "witness_value6"),
                                                                                   ])
                                                                ),
                                                                ])
                                      ),
                                     ("witness_field7", "witness_value7"),
                                     ])
    decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    
    configuration = Mapping({"model_interface": Mapping((("name", "OnlineInterface"), ("config", model_interface_configuration))),
                              "generator": Mapping((("name", "MentionPairSampleGenerator"), ("config", generator_configuration))),
                              "decoder": Mapping((("name", "AggressiveMergeCoreferenceDecoder"), ("config", decoder_configuration))),
                              "witness_field9": "witness_value9",
                             })
    
    expected_result = PositiveGraphStructureResolver.from_configuration(configuration)
    
    configuration = Mapping({"name": "positivegraphstructure", "config": configuration})
    data.append((configuration, expected_result))
    
    
    ## ClosestFirstDocumentStructuredResolver
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "with_singleton_features": False, 
                                                                                                    "with_anaphoricity_features": False, 
                                                                                                    "quantize": True, 
                                                                                                    "strict": True,
                                                                                                    "useless_field": "useless_value"})
                                                                                 }), 
                                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                    "config": Mapping({"iteration_nb": 2, 
                                                                                       "avg": True, 
                                                                                       "warm_start": False, 
                                                                                       "alpha": 1.52, 
                                                                                       "useless_field": "useless_value"}),
                                                                    })
                                            })
    generator_configuration = Mapping([("pair_filter", Mapping([("name", "PairFiltersCombination"), 
                                                               ("config", Mapping([("filters", [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                           "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                           }),
                                                                                                       ]),
                                                                                   ("witness_field6", "witness_value6"),
                                                                                   ])
                                                                ),
                                                                ])
                                      ),
                                     ("witness_field7", "witness_value7"),
                                     ])
    decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    
    root_loss = 1.2
    
    update_mode = "PB"
    
    configuration = Mapping({"model_interface": Mapping((("name", "OnlineInterface"), ("config", model_interface_configuration))),
                              "generator": Mapping((("name", "MentionPairSampleGenerator"), ("config", generator_configuration))),
                              "decoder": Mapping((("name", "ClosestFirstCoreferenceDecoder"), ("config", decoder_configuration))),
                              "root_loss": root_loss, 
                              "update_mode": update_mode, 
                              "witness_field9": "witness_value9",
                             })
    
    expected_result = ClosestFirstDocumentStructuredResolver.from_configuration(configuration)
    
    configuration = Mapping({"name": "closestfirstdocumentstructured", "config": configuration})
    data.append((configuration, expected_result))
    
    
    ## ExtendedClosestFirstDocumentStructuredResolver
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "ExtendedMentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "mention_hierarchy": Mapping({"name": "mentiongramtype", "config": Mapping()}), 
                                                                                                    "with_singleton_features": False, 
                                                                                                    "with_anaphoricity_features": False, 
                                                                                                    "quantize": True, 
                                                                                                    "strict": True,
                                                                                                    "useless_field": "useless_value"})
                                                                                 }), 
                                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                    "config": Mapping({"iteration_nb": 2, 
                                                                                       "avg": True, 
                                                                                       "warm_start": False, 
                                                                                       "alpha": 1.52, 
                                                                                       "useless_field": "useless_value"}),
                                                                    })
                                            })
    generator_configuration = Mapping([("pair_filter", Mapping([("name", "PairFiltersCombination"), 
                                                               ("config", Mapping([("filters", [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                           "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                           }),
                                                                                                       ]),
                                                                                   ("witness_field6", "witness_value6"),
                                                                                   ])
                                                                ),
                                                                ])
                                      ),
                                     ("witness_field7", "witness_value7"),
                                     ])
    decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    
    root_loss = 1.2
    
    update_mode = "PB"
    
    configuration = Mapping({"model_interface": Mapping((("name", "OnlineInterface"), ("config", model_interface_configuration))),
                              "generator": Mapping((("name", "ExtendedMentionPairSampleGenerator"), ("config", generator_configuration))),
                              "decoder": Mapping((("name", "ClosestFirstCoreferenceDecoder"), ("config", decoder_configuration))),
                              "root_loss": root_loss, 
                              "update_mode": update_mode, 
                              "witness_field9": "witness_value9",
                             })
    
    expected_result = ExtendedClosestFirstDocumentStructuredResolver.from_configuration(configuration)
    
    configuration = Mapping({"name": "extendedclosestfirstdocumentstructured", "config": configuration})
    data.append((configuration, expected_result))
    
    
    ## BestFirstDocumentStructuredResolver
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "ExtendedMentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "mention_hierarchy": Mapping({"name": "mentiongramtype", "config": Mapping()}), 
                                                                                                    "with_singleton_features": False, 
                                                                                                    "with_anaphoricity_features": False, 
                                                                                                    "quantize": True, 
                                                                                                    "strict": True,
                                                                                                    "useless_field": "useless_value"})
                                                                                 }), 
                                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                    "config": Mapping({"iteration_nb": 2, 
                                                                                       "avg": True, 
                                                                                       "warm_start": False, 
                                                                                       "alpha": 1.52, 
                                                                                       "useless_field": "useless_value"}),
                                                                    })
                                            })
    generator_configuration = Mapping([("pair_filter", Mapping([("name", "PairFiltersCombination"), 
                                                               ("config", Mapping([("filters", [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                           "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                           }),
                                                                                                       ]),
                                                                                   ("witness_field6", "witness_value6"),
                                                                                   ])
                                                                ),
                                                                ])
                                      ),
                                     ("witness_field7", "witness_value7"),
                                     ])
    decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    
    root_loss = 1.2
    
    update_mode = "PB"
    
    configuration = Mapping({"model_interface": Mapping((("name", "OnlineInterface"), ("config", model_interface_configuration))),
                              "generator": Mapping((("name", "MentionPairSampleGenerator"), ("config", generator_configuration))),
                              "decoder": Mapping((("name", "BestFirstCoreferenceDecoder"), ("config", decoder_configuration))),
                              "root_loss": root_loss, 
                              "update_mode": update_mode, 
                              "witness_field9": "witness_value9",
                             })
    
    expected_result = BestFirstDocumentStructuredResolver.from_configuration(configuration)
    
    configuration = Mapping({"name": "bestfirstdocumentstructured", "config": configuration})
    data.append((configuration, expected_result))
    
    
    ## ExtendedBestFirstDocumentStructuredResolver
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "ExtendedMentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "mention_hierarchy": Mapping({"name": "mentiongramtype", "config": Mapping()}), 
                                                                                                    "with_singleton_features": False, 
                                                                                                    "with_anaphoricity_features": False, 
                                                                                                    "quantize": True, 
                                                                                                    "strict": True,
                                                                                                    "useless_field": "useless_value"})
                                                                                 }), 
                                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                    "config": Mapping({"iteration_nb": 2, 
                                                                                       "avg": True, 
                                                                                       "warm_start": False, 
                                                                                       "alpha": 1.52, 
                                                                                       "useless_field": "useless_value"}),
                                                                    })
                                            })
    generator_configuration = Mapping([("pair_filter", Mapping([("name", "PairFiltersCombination"), 
                                                               ("config", Mapping([("filters", [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                           "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                           }),
                                                                                                       ]),
                                                                                   ("witness_field6", "witness_value6"),
                                                                                   ])
                                                                ),
                                                                ])
                                      ),
                                     ("witness_field7", "witness_value7"),
                                     ])
    decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    
    root_loss = 1.2
    
    update_mode = "PB"
    
    configuration = Mapping({"model_interface": Mapping((("name", "OnlineInterface"), ("config", model_interface_configuration))),
                              "generator": Mapping((("name", "ExtendedMentionPairSampleGenerator"), ("config", generator_configuration))),
                              "decoder": Mapping((("name", "ExtendedBestFirstCoreferenceDecoder"), ("config", decoder_configuration))),
                              "root_loss": root_loss, 
                              "update_mode": update_mode, 
                              "witness_field9": "witness_value9",
                             })
    
    expected_result = ExtendedBestFirstDocumentStructuredResolver.from_configuration(configuration)
    
    configuration = Mapping({"name": "extendedbestfirstdocumentstructured", "config": configuration})
    data.append((configuration, expected_result))
    
    
    ## ConstrainedBestFirstDocumentStructuredResolver
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "ExtendedMentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "mention_hierarchy": Mapping({"name": "mentiongramtype", "config": Mapping()}), 
                                                                                                    "with_singleton_features": False, 
                                                                                                    "with_anaphoricity_features": False, 
                                                                                                    "quantize": True, 
                                                                                                    "strict": True,
                                                                                                    "useless_field": "useless_value"})
                                                                                 }), 
                                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                    "config": Mapping({"iteration_nb": 2, 
                                                                                       "avg": True, 
                                                                                       "warm_start": False, 
                                                                                       "alpha": 1.52, 
                                                                                       "useless_field": "useless_value"}),
                                                                    })
                                            })
    generator_configuration = Mapping([("pair_filter", Mapping([("name", "PairFiltersCombination"), 
                                                               ("config", Mapping([("filters", [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                       "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                       }),
                                                                                                                   ]),
                                                                                   ("witness_field6", "witness_value6"),
                                                                                   ])
                                                                ),
                                                                ])
                                      ),
                                     ("witness_field7", "witness_value7"),
                                     ])
    decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    
    root_loss = 1.2
    
    update_mode = "PB"
    
    constraints_definition = (("rules_english_sieves", tuple(), {}),
                              ("anaphoricity", tuple(), {}),
                              ("proper_match", tuple(), {}),
                              ("animated", tuple(), {}),
                              ("embedding", tuple(), {}),
                              )
    
    configuration = Mapping({"model_interface": Mapping((("name", "OnlineInterface"), ("config", model_interface_configuration))),
                              "generator": Mapping((("name", "MentionPairSampleGenerator"), ("config", generator_configuration))),
                              "decoder": Mapping((("name", "ConstrainedBestFirstCoreferenceDecoder"), ("config", decoder_configuration))),
                              "root_loss": root_loss, 
                              "update_mode": update_mode, 
                              "constraints_definition": constraints_definition, 
                              "witness_field9": "witness_value9",
                             })
    
    expected_result = ConstrainedBestFirstDocumentStructuredResolver.from_configuration(configuration)
    
    configuration = Mapping({"name": "constrainedbestfirstdocumentstructured", "config": configuration})
    data.append((configuration, expected_result))
    
    ## ConstrainedExtendedBestFirstDocumentStructuredResolver
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "ExtendedMentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "mention_hierarchy": Mapping({"name": "mentiongramtype", "config": Mapping()}), 
                                                                                                    "with_singleton_features": False, 
                                                                                                    "with_anaphoricity_features": False, 
                                                                                                    "quantize": True, 
                                                                                                    "strict": True,
                                                                                                    "useless_field": "useless_value"})
                                                                                 }), 
                                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                    "config": Mapping({"iteration_nb": 2, 
                                                                                       "avg": True, 
                                                                                       "warm_start": False, 
                                                                                       "alpha": 1.52, 
                                                                                       "useless_field": "useless_value"}),
                                                                    })
                                            })
    generator_configuration = Mapping([("pair_filter", Mapping([("name", "PairFiltersCombination"), 
                                                               ("config", Mapping([("filters", [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                       "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                       }),
                                                                                                                   ]),
                                                                                   ("witness_field6", "witness_value6"),
                                                                                   ])
                                                                ),
                                                                ])
                                      ),
                                     ("witness_field7", "witness_value7"),
                                     ])
    decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    
    root_loss = 1.2
    
    update_mode = "PB"
    
    constraints_definition = (("rules_english_sieves", tuple(), {}),
                              ("anaphoricity", tuple(), {}),
                              ("proper_match", tuple(), {}),
                              ("animated", tuple(), {}),
                              ("embedding", tuple(), {}),
                              )
    
    configuration = Mapping({"model_interface": Mapping((("name", "OnlineInterface"), ("config", model_interface_configuration))),
                              "generator": Mapping((("name", "ExtendedMentionPairSampleGenerator"), ("config", generator_configuration))),
                              "decoder": Mapping((("name", "ConstrainedExtendedBestFirstCoreferenceDecoder"), ("config", decoder_configuration))),
                              "root_loss": root_loss, 
                              "update_mode": update_mode, 
                              "constraints_definition": constraints_definition, 
                              "witness_field9": "witness_value9",
                             })
    
    expected_result = ConstrainedExtendedBestFirstDocumentStructuredResolver.from_configuration(configuration)
    
    configuration = Mapping({"name": "constrainedextendedbestfirstdocumentstructured", "config": configuration})
    data.append((configuration, expected_result))
    
    return data




def get_test_load_resolver_data():
    data = []
    
    # SoonResolver
    model_folder_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "pairs", "train_test_sklearn_model_interface")
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                   "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping({})}), 
                                                                                                       "with_singleton_features": False, 
                                                                                                       "with_anaphoricity_features": False, 
                                                                                                       "quantize": True, 
                                                                                                       "strict": True, 
                                                                                                    })
                                                                                }),
                                             "classifier": Mapping({"name": "logisticregressionsklearnbinary", 
                                                                   "config": Mapping({"penalty": "l2",
                                                                                    "dual": False,
                                                                                    "tol": 1e-4,
                                                                                    "C": 1.0,
                                                                                    "fit_intercept": True,
                                                                                    "intercept_scaling": 1,
                                                                                    "class_weight": None,
                                                                                    "random_state": None,
                                                                                    "solver": "liblinear",
                                                                                    "max_iter": 100,
                                                                                    "multi_class": "ovr",
                                                                                    })
                                                                    }),
                                              })
    model_interface_configuration = Mapping([("name", "SKLEARNInterface"), 
                                               ("config", model_interface_configuration),
                                               ])
    generator_configuration = Mapping([("pair_filter", Mapping({"name": "acceptall"})),])
    generator_configuration = Mapping([("name", "SoonGenerator"), 
                                         ("config", generator_configuration),
                                         ])
    decoder_configuration = Mapping({"name": "ClosestFirstCoreferenceDecoder"})
    
    configuration = Mapping([("model_interface", model_interface_configuration),
                             ("generator", generator_configuration),
                             ("decoder", decoder_configuration),
                             ("witness_field9", "witness_value9"),
                            ])
    expected_resolver = SoonResolver.from_configuration(configuration)
    expected_resolver._model_interface.load_model(model_folder_path)
    
    archive_file_name = "test_soon_resolver_load.zip"
    archive_file_path = os.path.join(os.path.dirname(__file__), "data", "select_", "test_load_resolver", archive_file_name)
    
    data.append((archive_file_path, expected_resolver))
    
    # RulesResolver
    rules_type = "english_sieves"
    rules_resolve_args = None
    rules_resolve_kwargs = {"sieve_filter_anaphoric": True}
    configuration = Mapping({"rules_type": rules_type,
                              "rules_resolve_args": rules_resolve_args,
                              "rules_resolve_kwargs": rules_resolve_kwargs,
                              "witness_field9": "witness_value9",
                             })
    expected_resolver = RulesResolver.from_configuration(configuration)
    
    archive_file_name = "test_rules_resolver_load.zip"
    archive_file_path = os.path.join(os.path.dirname(__file__), "data", "select_", "test_load_resolver", archive_file_name)
    
    data.append((archive_file_path, expected_resolver))
    
    return data


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()