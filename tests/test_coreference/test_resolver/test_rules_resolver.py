# -*- coding: utf-8 -*-

import unittest
import os

from cortex.coreference.resolver.rules import RulesResolver
from cortex.api.coreference_partition import CoreferencePartition
from cortex.tools import Mapping
from cortex.utils.io import TemporaryDirectory, unzip_archive_to_folder
from cortex.utils.memoize import Memoized
from cortex.parameters import ENCODING

#@unittest.skip
class TestRulesResolver(unittest.TestCase):
    
    #@unittest.skip    
    def test___init__(self):
        #Test parameters
        (args, kwargs), attribute_name_expected_result_pairs = get_soon_resolver_test___init___data()
        
        # Test
        resolver = RulesResolver(*args, **kwargs)
        for attribute_name, expected_result in attribute_name_expected_result_pairs:
            actual_result = getattr(resolver, attribute_name)
            self.assertEqual(expected_result, actual_result, attribute_name)
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        data = get_soon_resolver_test__inner_eq_data()
        
        # Test
        for resolver1, resolver2, expected_result in data:
            self.assertEqual(expected_result, resolver1._inner_eq(resolver2))
    
    #@unittest.skip    
    def test_from_configuration(self):
        #Test parameters
        configuration, expected_result = get_soon_resolver_test_from_configuration_data()
        
        # Test
        actual_result = RulesResolver.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = RulesResolver
        
        rules_type = "english_sieves"
        rules_resolve_args = None
        rules_resolve_kwargs = {"sieve_filter_anaphoric": True}
        
        args = (rules_type,)
        kwargs = {"rules_resolve_args": rules_resolve_args,
                  "rules_resolve_kwargs": rules_resolve_kwargs,
                  }
        expected_result = Mapping({"rules_type": rules_type,
                                   "rules_resolve_args": rules_resolve_args if rules_resolve_args is not None else tuple(),
                                   "rules_resolve_kwargs": rules_resolve_kwargs if rules_resolve_kwargs is not None else dict(),
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip
    def test_resolve(self):
        # Test parameters
        data = get_soon_resolver_test_resolve_data()
        
        # Test
        for document, resolver, kwargs, expected_result in data:
            actual_result = resolver.resolve(document, **kwargs)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_save(self):
        #Test parameters
        resolver, expected_config_file_content = get_test_rules_resolver_save_data()
        
        # Test
        with TemporaryDirectory() as temp_dir:
            temporary_folder_path = temp_dir.temporary_folder_path
            archive_file_name = "test_save_resolver"
            folder_path = temporary_folder_path
            
            archive_file_path = resolver.save(folder_path, archive_file_name)
            
            with TemporaryDirectory(temporary_root_folder_path=temporary_folder_path) as temp_dir2:
                extract_folder_path = temp_dir2.temporary_folder_path
                unzip_archive_to_folder(archive_file_path, extract_folder_path)
                actual_config_file_path = os.path.join(extract_folder_path, "configuration.txt")
                with open(actual_config_file_path, "r", encoding=ENCODING) as f:
                    actual_config_file_content = f.read()
        self.assertEqual(expected_config_file_content, actual_config_file_content)



def get_test_document_data():
    from cortex.io.pivot_reader import PivotReader
    
    folder_path = os.path.join(os.path.dirname(__file__), "data", "rules")
    document_folder_path = os.path.join(folder_path, "rules_resolver_test_data", "ref")
    
    strict = True # We want to synchronize head tokens.
    test_document = PivotReader.parse(document_folder_path, strict=strict)
    
    return test_document

@Memoized
def get_memoized_test_document_data():
    return get_test_document_data()


def get_soon_resolver_test___init___data():
    attribute_name_expected_result_pairs = []
    
    rules_type = "english_sieves"
    attribute_name_expected_result_pairs.append(("rules_type",rules_type))
    
    rules_resolve_args = None
    attribute_name_expected_result_pairs.append(("rules_resolve_args", tuple()))
    
    rules_resolve_kwargs = {"sieve_filter_anaphoric": True}
    attribute_name_expected_result_pairs.append(("rules_resolve_kwargs",rules_resolve_kwargs))
    
    configuration = Mapping({"witness_field9": "witness_value9"})
    
    expected_configuration = Mapping({"rules_type": rules_type,
                                      "rules_resolve_args": tuple(),
                                      "rules_resolve_kwargs": rules_resolve_kwargs,
                                      "witness_field9": "witness_value9",
                                     })
    
    attribute_name_expected_result_pairs.append(("configuration", expected_configuration))
    
    args = (rules_type,)
    kwargs = {"rules_resolve_args": rules_resolve_args, "rules_resolve_kwargs": rules_resolve_kwargs, 
              "configuration": configuration}
    
    return (args, kwargs), attribute_name_expected_result_pairs

def get_soon_resolver_test__inner_eq_data():
    data = []
    
    (args, kwargs), _ = get_soon_resolver_test___init___data() #attribute_name_expected_result_pairs
    
    resolver1 = RulesResolver(*args, **kwargs)
    resolver2 = RulesResolver(*args, **kwargs)
    expected_result = (True, None)
    data.append((resolver1, resolver2, expected_result))
    
    kwargs["rules_resolve_kwargs"] = {"sieve_filter_anaphoric": False}
    resolver3 = RulesResolver(*args, **kwargs)
    expected_result = (False, "rules_resolve_kwargs")
    data.append((resolver1, resolver3, expected_result))
    
    return data

def get_soon_resolver_test_from_configuration_data():
    rules_type = "english_sieves"
    rules_resolve_args = None
    rules_resolve_kwargs = {"sieve_filter_anaphoric": True}
    configuration = Mapping({"witness_field9": "witness_value9"})
    
    args = (rules_type,)
    kwargs = {"rules_resolve_args": rules_resolve_args, "rules_resolve_kwargs": rules_resolve_kwargs, 
              "configuration": configuration}
    expected_result = RulesResolver(*args, **kwargs)
    
    configuration = Mapping({"rules_type": rules_type,
                              "rules_resolve_args": rules_resolve_args,
                              "rules_resolve_kwargs": Mapping(rules_resolve_kwargs),
                              "witness_field9": "witness_value9",
                             })
    
    return configuration, expected_result

def get_soon_resolver_test_resolve_data():
    data = []
    
    test_document = get_memoized_test_document_data()
    
    rules_type = "english_sieves"
    rules_resolve_args = None
    rules_resolve_kwargs = {"sieve_filter_anaphoric": True}
    configuration = Mapping({"rules_type": rules_type,
                              "rules_resolve_args": rules_resolve_args,
                              "rules_resolve_kwargs": rules_resolve_kwargs,
                              "witness_field9": "witness_value9",
                             })
    resolver = RulesResolver.from_configuration(configuration)
    
    kwargs = {}
    
    pos_coref_pairs = tuple()
    #excluded_pairs = ((46,47), (83,84))
    excluded_pairs = tuple()
    expected_result = CoreferencePartition(mention_extents=tuple(m.extent for m in test_document.mentions if m.extent not in excluded_pairs))
    for left_extent, right_extent in pos_coref_pairs:
        left_mention = test_document.extent2mention[left_extent]
        right_mention = test_document.extent2mention[right_extent]
        expected_result.join(left_mention.extent, right_mention.extent)
    
    data.append((test_document, resolver, kwargs, expected_result))
    
    return data

def get_test_rules_resolver_save_data():
    rules_type = "english_sieves"
    rules_resolve_args = None
    rules_resolve_kwargs = {"sieve_filter_anaphoric": True}
    configuration = Mapping((("rules_type", rules_type),
                              ("rules_resolve_args", rules_resolve_args),
                              ("rules_resolve_kwargs", rules_resolve_kwargs),
                              ("witness_field9", "witness_value9"),
                             ))
    resolver = RulesResolver.from_configuration(configuration)
    
    expected_config_file_content =\
    """name : 'RulesResolver'
config :
{
    rules_type : 'english_sieves'
    rules_resolve_args : [ ]
    rules_resolve_kwargs :
    {
        sieve_filter_anaphoric : True
    }
    witness_field9 : 'witness_value9'
}
"""
    
    #archive_file_name = "test_rules_resolver_load2"
    #archive_folder_path = os.path.join(os.path.dirname(__file__), "data", "select_", "test_load_resolver")
    #resolver.save(archive_folder_path, archive_file_name)
    
    return resolver, expected_config_file_content


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()