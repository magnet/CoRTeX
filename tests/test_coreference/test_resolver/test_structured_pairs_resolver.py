# -*- coding: utf-8 -*-

import unittest
from unittest import mock
import os
import numpy

from cortex.api.document import Document
from cortex.api.markable import Mention, NULL_MENTION
from cortex.coreference.resolver.structured_pairs import (MSTResolver, 
                                                          ExtendedMSTResolver, 
                                                          HybridBestFirstMSTResolver, 
                                                          ConstrainedMSTResolver, 
                                                          TransitiveClosureStructureResolver, 
                                                          PositiveGraphStructureResolver, 
                                                          ClosestFirstDocumentStructuredResolver, 
                                                          ExtendedClosestFirstDocumentStructuredResolver, 
                                                          BestFirstDocumentStructuredResolver, 
                                                          ExtendedBestFirstDocumentStructuredResolver, 
                                                          ConstrainedBestFirstDocumentStructuredResolver, 
                                                          ConstrainedExtendedBestFirstDocumentStructuredResolver,
                                                          )
from cortex.learning.parameters import POS_CLASS, NEG_CLASS
from cortex.learning.model_interface.online import OnlineInterface
from cortex.learning.model_interface import create_model_interface_from_factory_configuration
from cortex.learning.ml_features.hierarchy.pairs import PairGramTypeHierarchy #OverlapPairGramTypeHierarchy
from cortex.learning.ml_features.hierarchy.mentions import MentionNoneHierarchy
from cortex.learning.sample_features_vectorizer.pairs import ExtendedMentionPairSampleFeaturesVectorizer
from cortex.learning.filter.pairs import RecallPairFilter
from cortex.coreference.generator import create_generator_from_factory_configuration
from cortex.coreference.generator.pairs import MentionPairSampleGenerator, ExtendedMentionPairSampleGenerator
from cortex.coreference.decoder import create_decoder_from_factory_configuration
from cortex.coreference.decoder.mst import (MSTPrimCoreferenceDecoder, 
                                            HybridBestFirstMSTCoreferenceDecoder, 
                                            MSTKruskalCoreferenceDecoder)
from cortex.coreference.decoder.pairs import (AggressiveMergeCoreferenceDecoder, 
                                              ClosestFirstCoreferenceDecoder, 
                                              ConstrainedBestFirstCoreferenceDecoder)
from cortex.api.coreference_partition import CoreferencePartition
from cortex.tools.configuration import Mapping
from cortex.utils import are_array_equal
from cortex.utils.memoize import Memoized



class MockPrediction(object):
    def __init__(self, POS_class_score):
        self.POS_class_score = POS_class_score
    def get_label_score(self, value):
        return self.POS_class_score

class MockSample(object):
    def __init__(self, extent_pair, label=POS_CLASS):
        self.extent_pair = extent_pair
        self.label = label
    def __repr__(self):
        return str(self.extent_pair)

class MockMention(object):
    def __init__(self, extent):
        self.extent = extent


#@unittest.skip
class Test(unittest.TestCase):
    
    #@unittest.skip
    def test__compute_tree_loss(self):
        # Test parameters
        predicted_tree_edges = [(1,2), 
                                (1,3), 
                                #(2,3), 
                                (2,4),
                                (3,5),
                                (4,1),
                                (4,5),
                                (4,6),
                                #(5,1),
                                (6,3),
                                ]
        ground_truth_tree_edges = [(1,2), 
                                   #(1,3),
                                   (2,3), 
                                   (2,4),
                                   #(3,5),
                                   #(4,1),
                                   (4,5),
                                   (4,6),
                                   (5,1),
                                   (6,3),
                                  ]
        root = None
        r_loss = 1.0
        expected_result = 1.+1.+1. + 1.+1.
        
        # Test
        actual_result = MSTResolver._compute_tree_loss(predicted_tree_edges, ground_truth_tree_edges, root, r_loss)
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__add_loss_value_to_weights(self):
        # Test parameters
        predicted_tree_possible_edge2weight = {(1,2): 1.0, 
                                               (2,3): 2.0, 
                                               (2,4): 3.0,
                                               (4,5): 4.0,
                                               (4,6): 5.0,
                                               (5,1): 6.0,
                                               (6,3): 7.0,
                                              }
        ground_truth_tree_edges = {(1,2), 
                                   (1,3), 
                                   (2,4),
                                   (3,5),
                                   (4,1),
                                   (4,5),
                                   (4,6),
                                   (6,3),
                                   }
        root = None
        r_loss = 1.0
        expected_result = {(1,2): 1.0, 
                           (2,3): 2.0+1.0, 
                           (2,4): 3.0,
                           (4,5): 4.0,
                           (4,6): 5.0,
                           (5,1): 6.0+1.0,
                           (6,3): 7.0,
                          } 
        
        # Test
        actual_result = MSTResolver._add_loss_value_to_weights(predicted_tree_possible_edge2weight, 
                                                               ground_truth_tree_edges, root, r_loss)
        self.assertEqual(expected_result, actual_result)
    

#@unittest.skip
class TestMSTResolver(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        #Test parameters
        (args, kwargs), attribute_name_expected_result_pairs = get_mst_resolver_test___init___data()
        
        # Test
        resolver = MSTResolver(*args, **kwargs)
        for attribute_name, expected_result in attribute_name_expected_result_pairs:
            actual_result = getattr(resolver, attribute_name)
            self.assertEqual(expected_result, actual_result, attribute_name)
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        data = get_mst_resolver_test__inner_eq_data()
        
        # Test
        for resolver1, resolver2, expected_result in data:
            self.assertEqual(expected_result, resolver1._inner_eq(resolver2))
    
    #@unittest.skip    
    def test_from_configuration(self):
        # Test parameters
        configuration, expected_result = get_mst_resolver_test_from_configuration_data()
        
        # Test
        actual_result = MSTResolver.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = MSTResolver
        
        pair_hierarchy_factory_configuration = PairGramTypeHierarchy.define_factory_configuration()
        mention_hierarchy_factory_configuration = MentionNoneHierarchy.define_factory_configuration()
        with_singleton_features = True
        with_anaphoricity_features = False
        quantize = True
        args_1 = tuple()
        kwargs_1 = {"pair_hierarchy_factory_configuration": pair_hierarchy_factory_configuration, 
                    "mention_hierarchy_factory_configuration": mention_hierarchy_factory_configuration, 
                    "with_singleton_features": with_singleton_features,
                    "with_anaphoricity_features": with_anaphoricity_features,
                    "quantize": quantize, 
                    }
        sample_features_vectorizer_factory_configuration =\
         ExtendedMentionPairSampleFeaturesVectorizer.define_factory_configuration(*args_1, **kwargs_1)
        
        root_loss = 1.5
        update_mode = "ML"
        
        iteration_nb = 42
        C = 14.0675
        avg = True
        warm_start = True
        classifier_factory_configuration = Mapping({"name": "PAOnlineBinaryClassifier",
                                                    "config": Mapping({"iteration_nb": iteration_nb,
                                                                       "C": C,
                                                                       "avg": avg,
                                                                       "warm_start": warm_start, 
                                                                       }),
                                                    })
        
        generator_pair_filter_factory_configuration = RecallPairFilter.define_factory_configuration(max_mention_distance=7.9)
        
        args = tuple()
        kwargs = {"sample_features_vectorizer_factory_configuration": sample_features_vectorizer_factory_configuration,
                  "root_loss": root_loss,
                  "update_mode": update_mode,
                  "classifier_factory_configuration": classifier_factory_configuration,
                  "generator_pair_filter_factory_configuration": generator_pair_filter_factory_configuration,
                  }
        expected_result = Mapping({"generator": Mapping({"name": "MentionPairSampleGenerator",
                                                         "config": Mapping({"pair_filter": generator_pair_filter_factory_configuration,
                                                                            }),
                                                         }),
                                   "decoder": Mapping({"name": "MSTPrimCoreferenceDecoder",
                                                       "config": Mapping(),
                                                       }),
                                   "model_interface": Mapping({"name": "OnlineInterface",
                                                               "config": Mapping({"sample_features_vectorizer": sample_features_vectorizer_factory_configuration,
                                                                                  "classifier": Mapping({"name": "PAOnlineBinaryClassifier",
                                                                                                         "config": Mapping({"iteration_nb": iteration_nb,
                                                                                                                            "C": C,
                                                                                                                            "avg": avg,
                                                                                                                            "warm_start": warm_start, 
                                                                                                                            }),
                                                                                                         }),
                                                                                  }),
                                                               }),
                                   "root_loss": root_loss,
                                   "update_mode": update_mode,
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        #print(expected_result._model_interface)
        #print(actual_result._model_interface)
        #self.assertTrue(*Mapping._inner_eq(expected_result._model_interface, actual_result._model_interface))
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip
    def test__compute_true_edges(self):
        # Test parameters
        resolver, (document, scores, true_scores, cluster_scores), expected_result = get_mst_resolver_test__compute_true_edges_data()
        
        # Test
        actual_result = resolver._compute_ground_truth_graph_edges(document, scores, true_scores, cluster_scores)
        expected_result.sort()
        actual_result.sort()
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__compute_predicted_edges_from_edges_loss_scores(self):
        # Test parameters
        resolver, (document, loss_scores), expected_result = get_mst_resolver_test__compute_predicted_edges_from_edges_loss_scores_data()
        
        # Test
        actual_result = resolver._compute_predicted_tree_edges_from_edge2loss_score(document, loss_scores)
        expected_result.sort()
        actual_result.sort()
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__compute_predicted_edges(self):
        # Test parameters
        _, (document, extended_edges_scores, _, _), true_edges = get_mst_resolver_test__compute_true_edges_data()
        resolver, (_, _), _ = get_mst_resolver_test__compute_predicted_edges_from_edges_loss_scores_data()
        expected_result = [((0, 0), (1, 9)), ((1, 9), (83, 84)), ((1, 9), (114, 120)), 
                           ((14, 42), (114, 120)), ((27, 42), (68, 75)), ((27, 42), (114, 120)), 
                           ((46, 47), (83, 84))]
        
        # Test
        actual_result = resolver._compute_predicted_graph_edges(document, extended_edges_scores, true_edges)
        expected_result.sort()
        actual_result.sort()
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__compute_prediction_pair_scores(self):
        # Test parameters
        resolver, document, expected_extended_edges_scores = get_mst_resolver_test__compute_prediction_pair_scores_data()
        
        # Test
        actual_extended_edges_scores = resolver._compute_prediction_phase_extended_mention_pair_scores(document)
        compare_float_values_dict(self, expected_extended_edges_scores, actual_extended_edges_scores)
        #self.assertEqual(expected_extended_edges_scores, actual_extended_edges_scores)
    
    #@unittest.skip
    def test__compute_training_pair_scores(self):
        # Test parameters
        resolver, document, expected_results = get_mst_resolver_test__compute_training_pair_scores_data()
        
        # Test
        actual_results = resolver._compute_training_phase_extended_mention_pair_scores(document)
        (expected_extended_edges_scores, expected_true_extended_edges_scores, 
         expected_true_coreference_edges_scores) = expected_results
        (actual_extended_edges_scores, actual_true_extended_edges_scores, 
         actual_true_coreference_edges_scores) = actual_results
        compare_float_values_dict(self, expected_extended_edges_scores, actual_extended_edges_scores)
        compare_float_values_dict(self, expected_true_extended_edges_scores, actual_true_extended_edges_scores)
        compare_float_values_dict(self, expected_true_coreference_edges_scores, actual_true_coreference_edges_scores)
        '''
        self.assertEqual(expected_extended_edges_scores, actual_extended_edges_scores)
        self.assertEqual(expected_true_extended_edges_scores, actual_true_extended_edges_scores)
        self.assertEqual(expected_true_coreference_edges_scores, actual_true_coreference_edges_scores)
        '''
    
    #@unittest.skip
    def test__compute_loss_for_edges_prediction(self):
        # Test parameters
        ne_extent = NULL_MENTION.extent
        root_loss = 1.5
        extended_edges_scores = {(ne_extent,(1,2)): 2.9, ((1,2),(3,4)): -45, ((3,4),(5,6)): 36, (ne_extent, (5,6)): 9, }
        true_mst_edges = (((1,2),(3,4)), (ne_extent, (5,6)))
        data = []
        # update_mode == "ML"
        update_mode = "ML"
        expected_result = {(ne_extent,(1,2)): 4.4, ((1,2),(3,4)): -45, ((3,4),(5,6)): 37.0, (ne_extent, (5,6)): 9, }
        data.append((update_mode, expected_result)) 
        # update_mode = "PB"
        update_mode = "PB"
        expected_result = {(ne_extent,(1,2)): 2.9, ((1,2),(3,4)): -45, ((3,4),(5,6)): 36.0, (ne_extent, (5,6)): 9, }
        data.append((update_mode, expected_result)) 
        
        # Tests
        for update_mode, expected_result in data:
            ## Test parameters
            model_interface = mock.create_autospec(OnlineInterface)
            model_interface.configuration = {}
            generator = mock.create_autospec(MentionPairSampleGenerator)
            generator.configuration = {}
            decoder = mock.create_autospec(MSTPrimCoreferenceDecoder)
            decoder.configuration = {}
            configuration = None
            resolver = MSTResolver(model_interface, generator, decoder, root_loss, update_mode, configuration=configuration)
        
            ## Test
            actual_result = resolver._compute_edge2loss_score_for_predicted_tree_edges_computation(extended_edges_scores, true_mst_edges)
            self.assertEqual(expected_result, actual_result)

    #@unittest.skip
    def test__compute_graph_loss(self):
        # Test parameters
        resolver, (true_edges, predicted_edges), expected_result = get_mst_resolver_test__compute_graph_loss_data()
        
        # Test
        actual_result = resolver._compute_graph_loss(true_edges, predicted_edges)
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__compute_edges_scores_map(self):
        # Test parameters
        model_interface = mock.create_autospec(OnlineInterface)
        model_interface.configuration = {}
        generator = mock.create_autospec(MentionPairSampleGenerator)
        generator.configuration = {}
        decoder = mock.create_autospec(MSTPrimCoreferenceDecoder)
        decoder.configuration = {}
        update_mode = "ML"
        root_loss = 1.5
        configuration = None
        resolver = MSTResolver(model_interface, generator, decoder, root_loss, update_mode, configuration=configuration)
        
        ml_samples = (MockSample(((1,2),(3,4))), 
                      MockSample((None,(3,4))), 
                      MockSample(((3,4),(5,6))),
                      )
        predictions = (MockPrediction(2.3),
                        MockPrediction(-9.8),
                        MockPrediction(147),
                        )
        model_interface.predict.side_effect = lambda ml_spls: predictions if ml_spls == ml_samples else None
        
        expected_result = {((1,2),(3,4)): 2.3, (None,(3,4)): -9.8, ((3,4),(5,6)): 147}
        
        # Test
        actual_result = resolver._predict_edge2score(ml_samples)
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__compute_extended_mention_pair_prediction_pair_scores(self):
        # Test parameters
        model_interface = mock.create_autospec(OnlineInterface)
        model_interface.configuration = {}
        generator = mock.create_autospec(MentionPairSampleGenerator)
        generator.configuration = {}
        decoder = mock.create_autospec(MSTPrimCoreferenceDecoder)
        decoder.configuration = {}
        update_mode = "ML"
        root_loss = 1.5
        configuration = None
        resolver = MSTResolver(model_interface, generator, decoder, root_loss, update_mode, configuration=configuration)
        
        document = mock.create_autospec(Document)
        ml_samples = (MockSample(((1,2),(3,4))), 
                      MockSample(((7,8),(3,4))), 
                      MockSample(((3,4),(5,6))),
                      )
        generator.generate_prediction_set.side_effect = lambda doc: iter(ml_samples) if doc == document else None
        predictions = (MockPrediction(2.3),
                        MockPrediction(-9.8),
                        MockPrediction(147),
                        )
        model_interface.predict.side_effect = lambda ml_spls: predictions if ml_spls == ml_samples else None
        
        expected_result = {((1,2),(3,4)): 2.3, ((7,8),(3,4)): -9.8, ((3,4),(5,6)): 147}
        
        # Test
        actual_result = resolver._compute_naturally_extended_mention_pair_prediction_pair_scores(document)
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__compute_mention_pair_prediction_pair_scores(self):
        # Test parameters
        model_interface = mock.create_autospec(OnlineInterface)
        model_interface.configuration = {}
        generator = mock.create_autospec(MentionPairSampleGenerator)
        generator.configuration = {}
        decoder = mock.create_autospec(MSTPrimCoreferenceDecoder)
        decoder.configuration = {}
        update_mode = "ML"
        root_loss = 1.5
        configuration = None
        resolver = MSTResolver(model_interface, generator, decoder, root_loss, update_mode, configuration=configuration)
        ne_extent = NULL_MENTION.extent
        
        document = mock.create_autospec(Document)
        document.mentions = (MockMention((1,2)), 
                             MockMention((3,4)), 
                             MockMention((5,6)),
                             )
        ml_samples = (MockSample(((1,2),(3,4))), 
                      MockSample(((7,8),(3,4))), 
                      MockSample(((3,4),(5,6))),
                      )
        generator.generate_prediction_set.side_effect = lambda doc: iter(ml_samples) if doc == document else None
        predictions = (MockPrediction(2.3),
                        MockPrediction(-9.8),
                        MockPrediction(147),
                        )
        model_interface.predict.side_effect = lambda ml_spls: predictions if ml_spls == ml_samples else None
        
        expected_result = {((1,2),(3,4)): 2.3, ((7,8),(3,4)): -9.8, ((3,4),(5,6)): 147, 
                           (ne_extent,(1,2)): 0, (ne_extent,(3,4)): 0, (ne_extent,(5,6)): 0,
                           }
        
        # Test
        actual_result = resolver._compute_artificially_extended_mention_pair_prediction_pair_scores(document)
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__generate_extended_mention_pair_edge_samples(self):
        # Test parameters
        model_interface = mock.create_autospec(OnlineInterface)
        model_interface.configuration = {}
        generator = mock.create_autospec(MentionPairSampleGenerator)
        generator.configuration = {}
        decoder = mock.create_autospec(MSTPrimCoreferenceDecoder)
        decoder.configuration = {}
        update_mode = "ML"
        root_loss = 1.5
        configuration = None
        resolver = MSTResolver(model_interface, generator, decoder, root_loss, update_mode, configuration=configuration)
        ne_extent = NULL_MENTION.extent
        
        edges = (((1,2),(3,4)), (ne_extent,(3,4)), ((3,4),(5,6)),)
        document = mock.create_autospec(Document)
        document.extent2mention = {(1,2): MockMention((1,2)), 
                                    (3,4): MockMention((3,4)), 
                                    (5,6): MockMention((5,6)),
                                    }
        ml_samples = (MockSample(((1,2),(3,4))), 
                      MockSample((ne_extent,(3,4))), 
                      MockSample(((3,4),(5,6))),
                      )
        edge2sample = {sample.extent_pair: sample for sample in ml_samples}
        generator.generate_sample_from_mention_pair = lambda *mention_pair, train=True: edge2sample[tuple(mention.extent if train else None for mention in mention_pair)]
        
        expected_result = list(ml_samples)
        
        # Test
        actual_result = resolver._generate_extended_mention_pair_edge_samples(document, edges)
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__generate_mention_pair_edge_samples(self):
        # Test parameters
        model_interface = mock.create_autospec(OnlineInterface)
        model_interface.configuration = {}
        generator = mock.create_autospec(MentionPairSampleGenerator)
        generator.configuration = {}
        decoder = mock.create_autospec(MSTPrimCoreferenceDecoder)
        decoder.configuration = {}
        update_mode = "ML"
        root_loss = 1.5
        configuration = None
        resolver = MSTResolver(model_interface, generator, decoder, root_loss, update_mode, configuration=configuration)
        ne_extent = NULL_MENTION.extent
        
        edges = (((1,2),(3,4)), (ne_extent,(3,4)), ((3,4),(5,6)),)
        document = mock.create_autospec(Document)
        document.extent2mention = {(1,2): MockMention((1,2)), 
                                    (3,4): MockMention((3,4)), 
                                    (5,6): MockMention((5,6)),
                                    }
        ml_samples = (MockSample(((1,2),(3,4))), 
                      #MockSample((ne_extent,(3,4))), 
                      MockSample(((3,4),(5,6))),
                      )
        edge2sample = {sample.extent_pair: sample for sample in ml_samples}
        generator.generate_sample_from_mention_pair = lambda *mention_pair, train=True: edge2sample[tuple(mention.extent if train else None for mention in mention_pair)] if ne_extent not in tuple(mention.extent for mention in mention_pair) else None
        
        expected_result = list(ml_samples)
        
        # Test
        actual_result = resolver._generate_mention_pair_edge_samples(document, edges)
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_train_document(self):
        # Test parameters
        resolver, document, expected_model_interface = get_mst_resolver_test_train_document_data()
        
        # Test
        self.assertFalse(*expected_model_interface._inner_eq(resolver._model_interface))
        resolver.train_document(document)
        resolver.train_document(document)
        #model_folder_path = os.path.join(os.path.dirname(__file__), "data", "mst", "train_test_online_model_interface_perceptron_model")
        #resolver._model_interface.save_model(model_folder_path)
        self.assertTrue(*expected_model_interface._inner_eq(resolver._model_interface))
    
    #@unittest.skip
    def test_train(self):
        # Test parameters
        resolver, documents_iterable, kwargs, expected_model_interface = get_mst_resolver_test_train_data()
        documents = tuple(documents_iterable)
        _create_iterable = lambda items: (item for item in items)
        
        # Test
        self.assertFalse(*expected_model_interface._inner_eq(resolver._model_interface))
        resolver.train(documents, **kwargs) # Test that we can learn using an iterable instead of a collection
        self.assertTrue(*expected_model_interface._inner_eq(resolver._model_interface))
        resolver.train(documents, **kwargs) # Test that we can learn anew after learning a first time
    
    #@unittest.skip
    def test_resolve(self):
        # Test parameters
        data = get_mst_resolver_test_resolve_data()
        
        # Test
        for document, resolver, kwargs, expected_result in data:
            actual_result = resolver.resolve(document, **kwargs)
            self.assertEqual(expected_result, actual_result)



#@unittest.skip
class TestExtendedMSTResolver(unittest.TestCase):
    
    # FIXME: create a 'test___init__' and a 'test_from_configuration'
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = ExtendedMSTResolver
        
        pair_hierarchy_factory_configuration = PairGramTypeHierarchy.define_factory_configuration()
        mention_hierarchy_factory_configuration = MentionNoneHierarchy.define_factory_configuration()
        with_singleton_features = True
        with_anaphoricity_features = False
        quantize = True
        args_1 = tuple()
        kwargs_1 = {"pair_hierarchy_factory_configuration": pair_hierarchy_factory_configuration, 
                    "mention_hierarchy_factory_configuration": mention_hierarchy_factory_configuration, 
                    "with_singleton_features": with_singleton_features,
                    "with_anaphoricity_features": with_anaphoricity_features,
                    "quantize": quantize, 
                    }
        sample_features_vectorizer_factory_configuration =\
         ExtendedMentionPairSampleFeaturesVectorizer.define_factory_configuration(*args_1, **kwargs_1)
        
        root_loss = 1.5
        update_mode = "ML"
        
        iteration_nb = 42
        C = 14.0675
        avg = True
        warm_start = True
        classifier_factory_configuration = Mapping({"name": "PAOnlineBinaryClassifier",
                                                    "config": Mapping({"iteration_nb": iteration_nb,
                                                                       "C": C,
                                                                       "avg": avg,
                                                                       "warm_start": warm_start, 
                                                                       }),
                                                    })
        
        generator_pair_filter_factory_configuration = RecallPairFilter.define_factory_configuration(max_mention_distance=7.9)
        
        args = tuple()
        kwargs = {"sample_features_vectorizer_factory_configuration": sample_features_vectorizer_factory_configuration,
                  "root_loss": root_loss,
                  "update_mode": update_mode,
                  "classifier_factory_configuration": classifier_factory_configuration,
                  "generator_pair_filter_factory_configuration": generator_pair_filter_factory_configuration,
                  }
        expected_result = Mapping({"generator": Mapping({"name": "ExtendedMentionPairSampleGenerator",
                                                         "config": Mapping({"pair_filter": generator_pair_filter_factory_configuration,
                                                                            }),
                                                         }),
                                   "decoder": Mapping({"name": "MSTPrimCoreferenceDecoder",
                                                       "config": Mapping(),
                                                       }),
                                   "model_interface": Mapping({"name": "OnlineInterface",
                                                               "config": Mapping({"sample_features_vectorizer": sample_features_vectorizer_factory_configuration,
                                                                                  "classifier": Mapping({"name": "PAOnlineBinaryClassifier",
                                                                                                         "config": Mapping({"iteration_nb": iteration_nb,
                                                                                                                            "C": C,
                                                                                                                            "avg": avg,
                                                                                                                            "warm_start": warm_start, 
                                                                                                                            }),
                                                                                                         }),
                                                                                  }),
                                                               }),
                                   "root_loss": root_loss,
                                   "update_mode": update_mode,
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        #print(expected_result._model_interface)
        #print(actual_result._model_interface)
        #self.assertTrue(*Mapping._inner_eq(expected_result._model_interface, actual_result._model_interface))
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip
    def test__compute_training_pair_scores(self):
        # Test parameters
        #resolver, document, expected_results = get_mst_extended_resolver_test__compute_training_pair_scores_data()
        
        model_interface = mock.create_autospec(OnlineInterface)
        model_interface.configuration = {}
        generator = mock.create_autospec(ExtendedMentionPairSampleGenerator)
        generator.configuration = {}
        decoder = mock.create_autospec(MSTPrimCoreferenceDecoder)
        decoder.configuration = {}
        update_mode = "ML"
        root_loss = 1.5
        configuration = None
        resolver = ExtendedMSTResolver(model_interface, generator, decoder, root_loss, update_mode, configuration=configuration)
        ne_extent = NULL_MENTION.extent
        
        document = mock.create_autospec(Document)
        ml_samples = (MockSample((ne_extent,(3,4)), label=POS_CLASS), 
                      MockSample(((1,2),(3,4)), label=POS_CLASS), 
                      MockSample(((7,8),(3,4)), label=NEG_CLASS), 
                      MockSample(((3,4),(5,6)), label=POS_CLASS),
                      MockSample((ne_extent,(5,6)), label=NEG_CLASS),
                      )
        generator.generate_training_set.side_effect = lambda docs: iter(ml_samples) if docs == (document,) else None
        predictions = (MockPrediction(4.6),
                       MockPrediction(2.3),
                       MockPrediction(-9.8),
                       MockPrediction(147),
                       MockPrediction(-294),
                        )
        model_interface.predict.side_effect = lambda ml_spls: predictions if ml_spls == ml_samples else None
        
        expected_extended_edges_scores = {(ne_extent,(3,4)): 4.6, ((1,2),(3,4)): 2.3, 
                                          ((7,8),(3,4)): -9.8, ((3,4),(5,6)): 147, 
                                          (ne_extent,(5,6)): -294,
                                          }
        expected_true_extended_edges_scores = {(ne_extent,(3,4)): 4.6, ((1,2),(3,4)): 2.3, ((3,4),(5,6)): 147,}
        expected_true_coreference_edges_scores = {((1,2),(3,4)): 2.3, ((3,4),(5,6)): 147,}
        
        # Test
        actual_results = resolver._compute_training_phase_extended_mention_pair_scores(document)
        (actual_extended_edges_scores, actual_true_extended_edges_scores, 
         actual_true_coreference_edges_scores) = actual_results
        self.assertEqual(expected_extended_edges_scores, actual_extended_edges_scores)
        self.assertEqual(expected_true_extended_edges_scores, actual_true_extended_edges_scores)
        self.assertEqual(expected_true_coreference_edges_scores, actual_true_coreference_edges_scores)



#@unittest.skip
class TestHybridBestFirstMSTResolver(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        #Test parameters
        (args, kwargs), attribute_name_expected_result_pairs = get_hybrid_best_first_mst_resolver_test___init___data()
        
        # Test
        resolver = HybridBestFirstMSTResolver(*args, **kwargs)
        for attribute_name, expected_result in attribute_name_expected_result_pairs:
            actual_result = getattr(resolver, attribute_name)
            self.assertEqual(expected_result, actual_result, attribute_name)
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = HybridBestFirstMSTResolver
        
        pair_hierarchy_factory_configuration = PairGramTypeHierarchy.define_factory_configuration()
        mention_hierarchy_factory_configuration = MentionNoneHierarchy.define_factory_configuration()
        with_singleton_features = True
        with_anaphoricity_features = False
        quantize = True
        args_1 = tuple()
        kwargs_1 = {"pair_hierarchy_factory_configuration": pair_hierarchy_factory_configuration, 
                    "mention_hierarchy_factory_configuration": mention_hierarchy_factory_configuration, 
                    "with_singleton_features": with_singleton_features,
                    "with_anaphoricity_features": with_anaphoricity_features,
                    "quantize": quantize, 
                    }
        sample_features_vectorizer_factory_configuration =\
         ExtendedMentionPairSampleFeaturesVectorizer.define_factory_configuration(*args_1, **kwargs_1)
        
        root_loss = 1.5
        update_mode = "ML"
        
        iteration_nb = 42
        C = 14.0675
        avg = True
        warm_start = True
        classifier_factory_configuration = Mapping({"name": "PAOnlineBinaryClassifier",
                                                    "config": Mapping({"iteration_nb": iteration_nb,
                                                                       "C": C,
                                                                       "avg": avg,
                                                                       "warm_start": warm_start, 
                                                                       }),
                                                    })
        
        generator_pair_filter_factory_configuration = RecallPairFilter.define_factory_configuration(max_mention_distance=7.9)
        
        args = tuple()
        kwargs = {"sample_features_vectorizer_factory_configuration": sample_features_vectorizer_factory_configuration,
                  "root_loss": root_loss,
                  "update_mode": update_mode,
                  "classifier_factory_configuration": classifier_factory_configuration,
                  "generator_pair_filter_factory_configuration": generator_pair_filter_factory_configuration,
                  }
        expected_result = Mapping({"generator": Mapping({"name": "MentionPairSampleGenerator",
                                                         "config": Mapping({"pair_filter": generator_pair_filter_factory_configuration,
                                                                            }),
                                                         }),
                                   "decoder": Mapping({"name": "HybridBestFirstMSTCoreferenceDecoder",
                                                       "config": Mapping(),
                                                       }),
                                   "model_interface": Mapping({"name": "OnlineInterface",
                                                               "config": Mapping({"sample_features_vectorizer": sample_features_vectorizer_factory_configuration,
                                                                                  "classifier": Mapping({"name": "PAOnlineBinaryClassifier",
                                                                                                         "config": Mapping({"iteration_nb": iteration_nb,
                                                                                                                            "C": C,
                                                                                                                            "avg": avg,
                                                                                                                            "warm_start": warm_start, 
                                                                                                                            }),
                                                                                                         }),
                                                                                  }),
                                                               }),
                                   "root_loss": root_loss,
                                   "update_mode": update_mode,
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        #print(expected_result._model_interface)
        #print(actual_result._model_interface)
        #self.assertTrue(*Mapping._inner_eq(expected_result._model_interface, actual_result._model_interface))
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip
    def test__compute_predicted_edges_from_edges_loss_scores(self):
        # Test parameters
        resolver, (document, loss_scores), expected_result = get_hybrid_best_first_mst_resolver_test__compute_predicted_edges_from_edges_loss_scores_data()
        
        # Test
        actual_result = resolver._compute_predicted_tree_edges_from_edge2loss_score(document, loss_scores)
        expected_result.sort()
        actual_result.sort()
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__compute_predicted_edges(self):
        # Test parameters
        _, (document, extended_edges_scores, _, _), true_edges = get_hybrid_best_first_mst_resolver_test__compute_true_edges_data()
        resolver, (_, _), _ = get_hybrid_best_first_mst_resolver_test__compute_predicted_edges_from_edges_loss_scores_data()
        expected_result = [((0, 0), (114, 120)), ((1, 9), (46, 47)), ((1, 9), (114, 120)), 
                           ((14, 42), (114, 120)), ((27, 42), (68, 75)), ((27, 42), (114, 120)), 
                           ((46, 47), (83, 84))]
        
        # Test
        actual_result = resolver._compute_predicted_graph_edges(document, extended_edges_scores, true_edges)
        expected_result.sort()
        actual_result.sort()
        self.assertEqual(expected_result, actual_result)



#@unittest.skip
class TestConstrainedMSTResolver(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        #Test parameters
        (args, kwargs), attribute_name_expected_result_pairs = get_constrained_mst_resolver_test___init___data()
        
        # Test
        resolver = ConstrainedMSTResolver(*args, **kwargs)
        for attribute_name, expected_result in attribute_name_expected_result_pairs:
            actual_result = getattr(resolver, attribute_name)
            self.assertEqual(expected_result, actual_result, attribute_name)
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        data = get_constrained_mst_resolver_test__inner_eq_data()
        
        # Test
        for resolver1, resolver2, expected_result in data:
            self.assertEqual(expected_result, resolver1._inner_eq(resolver2))
    
    #@unittest.skip    
    def test_from_configuration(self):
        # Test parameters
        configuration, expected_result = get_constrained_mst_resolver_test_from_configuration_data()
        
        # Test
        actual_result = ConstrainedMSTResolver.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = ConstrainedMSTResolver
        
        pair_hierarchy_factory_configuration = PairGramTypeHierarchy.define_factory_configuration()
        mention_hierarchy_factory_configuration = MentionNoneHierarchy.define_factory_configuration()
        with_singleton_features = True
        with_anaphoricity_features = False
        quantize = True
        args_1 = tuple()
        kwargs_1 = {"pair_hierarchy_factory_configuration": pair_hierarchy_factory_configuration, 
                    "mention_hierarchy_factory_configuration": mention_hierarchy_factory_configuration, 
                    "with_singleton_features": with_singleton_features,
                    "with_anaphoricity_features": with_anaphoricity_features,
                    "quantize": quantize, 
                    }
        sample_features_vectorizer_factory_configuration =\
         ExtendedMentionPairSampleFeaturesVectorizer.define_factory_configuration(*args_1, **kwargs_1)
        
        root_loss = 1.5
        update_mode = "ML"
        
        iteration_nb = 42
        C = 14.0675
        avg = True
        warm_start = True
        classifier_factory_configuration = Mapping({"name": "PAOnlineBinaryClassifier",
                                                    "config": Mapping({"iteration_nb": iteration_nb,
                                                                       "C": C,
                                                                       "avg": avg,
                                                                       "warm_start": warm_start, 
                                                                       }),
                                                    })
        
        generator_pair_filter_factory_configuration = RecallPairFilter.define_factory_configuration(max_mention_distance=7.9)
        
        constraints_definition = [("anaphoricity", tuple(), {"entity_head_extents": None}),
                                  ("animated", tuple(), {"entity_head_extents": None}),
                                  ]
        
        args = tuple()
        kwargs = {"sample_features_vectorizer_factory_configuration": sample_features_vectorizer_factory_configuration,
                  "root_loss": root_loss,
                  "update_mode": update_mode,
                  "classifier_factory_configuration": classifier_factory_configuration,
                  "generator_pair_filter_factory_configuration": generator_pair_filter_factory_configuration,
                  "constraints_definition": constraints_definition,
                  }
        expected_result = Mapping({"generator": Mapping({"name": "MentionPairSampleGenerator",
                                                         "config": Mapping({"pair_filter": generator_pair_filter_factory_configuration,
                                                                            }),
                                                         }),
                                   "decoder": Mapping({"name": "MSTKruskalCoreferenceDecoder",
                                                       "config": Mapping(),
                                                       }),
                                   "model_interface": Mapping({"name": "OnlineInterface",
                                                               "config": Mapping({"sample_features_vectorizer": sample_features_vectorizer_factory_configuration,
                                                                                  "classifier": Mapping({"name": "PAOnlineBinaryClassifier",
                                                                                                         "config": Mapping({"iteration_nb": iteration_nb,
                                                                                                                            "C": C,
                                                                                                                            "avg": avg,
                                                                                                                            "warm_start": warm_start, 
                                                                                                                            }),
                                                                                                         }),
                                                                                  }),
                                                               }),
                                   "root_loss": root_loss,
                                   "update_mode": update_mode,
                                   "constraints_definition": constraints_definition,
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        #print(expected_result._model_interface)
        #print(actual_result._model_interface)
        #self.assertTrue(*Mapping._inner_eq(expected_result._model_interface, actual_result._model_interface))
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip
    def test__compute_predicted_edges_from_edges_loss_scores(self):
        # Test parameters
        resolver, (document, loss_scores), expected_result = get_constrained_mst_resolver_test__compute_predicted_edges_from_edges_loss_scores_data()
        
        # Test
        actual_result = resolver._compute_predicted_tree_edges_from_edge2loss_score(document, loss_scores)
        expected_result.sort()
        actual_result.sort()
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__compute_true_edges(self):
        # Test parameters
        (resolver, 
         (document, extended_edges_scores, true_extended_edges_scores, true_coreference_edges_scores), 
         expected_result) = get_constrained_mst_resolver_test__compute_true_edges_data()
        
        # Test
        actual_result = resolver._compute_ground_truth_graph_edges(document, extended_edges_scores, 
                                                     true_extended_edges_scores, 
                                                     true_coreference_edges_scores)
        expected_result.sort()
        actual_result.sort()
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_resolve(self):
        # Test parameters
        data = get_constrained_mst_resolver_test_resolve_data()
        
        # Test
        for document, resolver, kwargs, expected_result in data:
            actual_result = resolver.resolve(document, **kwargs)
            self.assertEqual(expected_result, actual_result)



#@unittest.skip
class TestTransitiveClosureStructureResolver(unittest.TestCase):
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = TransitiveClosureStructureResolver
        
        pair_hierarchy_factory_configuration = PairGramTypeHierarchy.define_factory_configuration()
        mention_hierarchy_factory_configuration = MentionNoneHierarchy.define_factory_configuration()
        with_singleton_features = True
        with_anaphoricity_features = False
        quantize = True
        args_1 = tuple()
        kwargs_1 = {"pair_hierarchy_factory_configuration": pair_hierarchy_factory_configuration, 
                    "mention_hierarchy_factory_configuration": mention_hierarchy_factory_configuration, 
                    "with_singleton_features": with_singleton_features,
                    "with_anaphoricity_features": with_anaphoricity_features,
                    "quantize": quantize, 
                    }
        sample_features_vectorizer_factory_configuration =\
         ExtendedMentionPairSampleFeaturesVectorizer.define_factory_configuration(*args_1, **kwargs_1)
        
        iteration_nb = 42
        C = 14.0675
        avg = True
        warm_start = True
        classifier_factory_configuration = Mapping({"name": "PAOnlineBinaryClassifier",
                                                    "config": Mapping({"iteration_nb": iteration_nb,
                                                                       "C": C,
                                                                       "avg": avg,
                                                                       "warm_start": warm_start, 
                                                                       }),
                                                    })
        
        generator_pair_filter_factory_configuration = RecallPairFilter.define_factory_configuration(max_mention_distance=7.9)
        
        args = tuple()
        kwargs = {"sample_features_vectorizer_factory_configuration": sample_features_vectorizer_factory_configuration,
                  "classifier_factory_configuration": classifier_factory_configuration,
                  "generator_pair_filter_factory_configuration": generator_pair_filter_factory_configuration,
                  }
        expected_result = Mapping({"generator": Mapping({"name": "MentionPairSampleGenerator",
                                                         "config": Mapping({"pair_filter": generator_pair_filter_factory_configuration,
                                                                            }),
                                                         }),
                                   "decoder": Mapping({"name": "AggressiveMergeCoreferenceDecoder",
                                                       "config": Mapping(),
                                                       }),
                                   "model_interface": Mapping({"name": "OnlineInterface",
                                                               "config": Mapping({"sample_features_vectorizer": sample_features_vectorizer_factory_configuration,
                                                                                  "classifier": Mapping({"name": "PAOnlineBinaryClassifier",
                                                                                                         "config": Mapping({"iteration_nb": iteration_nb,
                                                                                                                            "C": C,
                                                                                                                            "avg": avg,
                                                                                                                            "warm_start": warm_start, 
                                                                                                                            }),
                                                                                                         }),
                                                                                  }),
                                                               }),
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        #print(expected_result._model_interface)
        #print(actual_result._model_interface)
        #self.assertTrue(*Mapping._inner_eq(expected_result._model_interface, actual_result._model_interface))
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip
    def test__compute_true_edges(self):
        # Test parameters
        model_interface = mock.create_autospec(OnlineInterface)
        model_interface.configuration = {}
        generator = mock.create_autospec(MentionPairSampleGenerator)
        generator.configuration = {}
        decoder = mock.create_autospec(AggressiveMergeCoreferenceDecoder)
        decoder.configuration = {}
        configuration = None
        resolver = TransitiveClosureStructureResolver(model_interface, generator, decoder, configuration=configuration)
        
        document = Document("test", "aaaaaaaaaaaaaaaaaaa")
        mentions = (Mention("test1", (1,2), document), #MockMention((1,2)), 
                    Mention("test1", (3,4), document), #MockMention((3,4)), 
                    Mention("test1", (5,6), document), #MockMention((5,6)),
                    Mention("test1", (7,8), document), #MockMention((7,8)),
                    Mention("test1", (9,10), document), #MockMention((9,10)),
                    )
        entities = (((1,2),(5,6),), 
                    ((7,8),(9,10),), 
                    )
        coreference_partition = CoreferencePartition(entities=entities)
        document.extent2mention = {mention.extent: mention for mention in mentions}
        document.coreference_partition = coreference_partition
        expected_result = [((1,2),(5,6)), ((7,8),(9,10))]
        
        # Test
        actual_result = resolver._compute_ground_truth_graph_edges(document, tuple(), tuple(), tuple())
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__compute_predicted_edges(self):
        # Test parameters
        model_interface = mock.create_autospec(OnlineInterface)
        model_interface.configuration = {}
        generator = mock.create_autospec(MentionPairSampleGenerator)
        generator.configuration = {}
        decoder = mock.create_autospec(AggressiveMergeCoreferenceDecoder)
        decoder.configuration = {}
        configuration = None
        resolver = TransitiveClosureStructureResolver(model_interface, generator, decoder, configuration=configuration)
        ne_extent = NULL_MENTION.extent
        
        document = Document("test", "aaaaaaaaaaaaaaaaaaa")
        mentions = (Mention("test1", (1,2), document), #MockMention((1,2)), 
                    Mention("test1", (3,4), document), #MockMention((3,4)), 
                    Mention("test1", (5,6), document), #MockMention((5,6)),
                    Mention("test1", (7,8), document), #MockMention((7,8)),
                    Mention("test1", (9,10), document), #MockMention((9,10)),
                    )
        document.mentions = mentions
        extended_edges_scores = {(ne_extent,(1,2)): 0., (ne_extent,(3,4)): 0., (ne_extent,(5,6)): 0., (ne_extent,(7,8)): 0., (ne_extent,(9,10)): 0., 
                                 ((1,2),(3,4)): 0.5, ((1,2),(5,6)): -0.6, ((1,2),(7,8)): -0.6, ((1,2),(9,10)): -0.6, 
                                 ((3,4),(5,6)): 0.2, ((3,4),(7,8)): -0.6, ((3,4),(9,10)): -0.6, 
                                 ((5,6),(7,8)): -0.6, ((5,6),(9,10)): -0.6, 
                                 ((7,8),(9,10)): 0.3}
        expected_result = [((1,2),(3,4)), 
                           ((1,2),(5,6)), 
                           ((3,4),(5,6)), 
                           ((7,8),(9,10)),
                           ]
        
        # Test
        actual_result = resolver._compute_predicted_graph_edges(document, extended_edges_scores, tuple())
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__compute_graph_loss(self):
        # Test parameters
        model_interface = mock.create_autospec(OnlineInterface)
        model_interface.configuration = {}
        generator = mock.create_autospec(MentionPairSampleGenerator)
        generator.configuration = {}
        decoder = mock.create_autospec(AggressiveMergeCoreferenceDecoder)
        decoder.configuration = {}
        configuration = None
        resolver = TransitiveClosureStructureResolver(model_interface, generator, decoder, configuration=configuration)
        
        true_edges = [((1,2),(3,4)), 
                      ((1,2),(5,6)), 
                      ((7,8),(9,10))]
        predicted_edges = [((1,2),(5,6)), 
                           ((3,4),(5,6)), 
                           ((7,8),(9,10)),
                           ]
        expected_result = 2
        
        # Test
        actual_result = resolver._compute_graph_loss(true_edges, predicted_edges)
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__get_update_model_data(self):
        # Test parameters
        document = get_test_document_data()
        model_interface = create_model_interface_from_factory_configuration(Mapping({"name": "online",
                                                         "config": Mapping({"sample_features_vectorizer": Mapping({"name": "mock", 
                                                                                                      "config": Mapping({"for_mention_pair_sample": True})}),
                                                                 "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                                        "config": Mapping({"avg": True, 
                                                                                                           "warm_start": False, 
                                                                                                           "alpha": 0.1}),
                                                                                        })
                                                                  })
                                                          })
                                                 )
        generator = create_generator_from_factory_configuration(Mapping({"name": "mentionpair"}))
        decoder = create_decoder_from_factory_configuration(Mapping({"name": "aggressivemerge"}))
        configuration = Mapping()
        resolver = TransitiveClosureStructureResolver(model_interface, generator, decoder, configuration=configuration)
        resolver._model_interface._classifier.coef_ = numpy.array([[1,2,-3,-4]], dtype=numpy.float)
        resolver._model_interface._classifier._cumulative_coef_ = resolver._model_interface._classifier.coef_
        expected_true_edge_ML_sample_pair_extents = [((46, 47), (83, 84)), 
                                                     ((27, 42), (68, 75)), 
                                                     ((14, 42), (83, 84)), ((14, 42), (46, 47)), 
                                                     ((1, 9), (83, 84)), ((1, 9), (46, 47)), 
                                                     ((1, 9), (14, 42)),
                                                     ]
        expected_predicted_edge_ML_sample_pair_extents = [((83, 84), (114, 120)), 
                                                          ((68, 75), (114, 120)), ((68, 75), (83, 84)), 
                                                          ((46, 47), (114, 120)), ((46, 47), (83, 84)), 
                                                          ((46, 47), (68, 75)), 
                                                          ((27, 42), (114, 120)), ((27, 42), (83, 84)), 
                                                          ((27, 42), (68, 75)), ((27, 42), (46, 47)), 
                                                          ((14, 42), (114, 120)), ((14, 42), (83, 84)), 
                                                          ((14, 42), (68, 75)), ((14, 42), (46, 47)), 
                                                          ((14, 42), (27, 42)), 
                                                          ((1, 9), (114, 120)), ((1, 9), (83, 84)), 
                                                          ((1, 9), (68, 75)), ((1, 9), (46, 47)), 
                                                          ((1, 9), (27, 42)), ((1, 9), (14, 42)),
                                                          ]


        expected_loss = -71.258342613226063
        _transf = lambda samples: sorted((sample.extent_pair for sample in samples), reverse=True)
        
        # Test
        self.maxDiff = None
        actual_true_edge_ML_samples, actual_predicted_edge_ML_samples, actual_loss = resolver._carry_out_training_prediction_step(document)
        actual_true_edge_ML_sample_pair_extents = _transf(actual_true_edge_ML_samples)
        actual_predicted_edge_ML_sample_pair_extents = _transf(actual_predicted_edge_ML_samples)
        self.assertEqual(expected_true_edge_ML_sample_pair_extents, actual_true_edge_ML_sample_pair_extents)
        self.assertEqual(expected_predicted_edge_ML_sample_pair_extents, actual_predicted_edge_ML_sample_pair_extents)
        self.assertEqual(expected_loss, actual_loss)



#@unittest.skip
class TestPositiveGraphStructureResolver(unittest.TestCase):
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = PositiveGraphStructureResolver
        
        pair_hierarchy_factory_configuration = PairGramTypeHierarchy.define_factory_configuration()
        mention_hierarchy_factory_configuration = MentionNoneHierarchy.define_factory_configuration()
        with_singleton_features = True
        with_anaphoricity_features = False
        quantize = True
        args_1 = tuple()
        kwargs_1 = {"pair_hierarchy_factory_configuration": pair_hierarchy_factory_configuration, 
                    "mention_hierarchy_factory_configuration": mention_hierarchy_factory_configuration, 
                    "with_singleton_features": with_singleton_features,
                    "with_anaphoricity_features": with_anaphoricity_features,
                    "quantize": quantize, 
                    }
        sample_features_vectorizer_factory_configuration =\
         ExtendedMentionPairSampleFeaturesVectorizer.define_factory_configuration(*args_1, **kwargs_1)
        
        iteration_nb = 42
        C = 14.0675
        avg = True
        warm_start = True
        classifier_factory_configuration = Mapping({"name": "PAOnlineBinaryClassifier",
                                                    "config": Mapping({"iteration_nb": iteration_nb,
                                                                       "C": C,
                                                                       "avg": avg,
                                                                       "warm_start": warm_start, 
                                                                       }),
                                                    })
        
        generator_pair_filter_factory_configuration = RecallPairFilter.define_factory_configuration(max_mention_distance=7.9)
        
        args = tuple()
        kwargs = {"sample_features_vectorizer_factory_configuration": sample_features_vectorizer_factory_configuration,
                  "classifier_factory_configuration": classifier_factory_configuration,
                  "generator_pair_filter_factory_configuration": generator_pair_filter_factory_configuration,
                  }
        expected_result = Mapping({"generator": Mapping({"name": "MentionPairSampleGenerator",
                                                         "config": Mapping({"pair_filter": generator_pair_filter_factory_configuration,
                                                                            }),
                                                         }),
                                   "decoder": Mapping({"name": "AggressiveMergeCoreferenceDecoder",
                                                       "config": Mapping(),
                                                       }),
                                   "model_interface": Mapping({"name": "OnlineInterface",
                                                               "config": Mapping({"sample_features_vectorizer": sample_features_vectorizer_factory_configuration,
                                                                                  "classifier": Mapping({"name": "PAOnlineBinaryClassifier",
                                                                                                         "config": Mapping({"iteration_nb": iteration_nb,
                                                                                                                            "C": C,
                                                                                                                            "avg": avg,
                                                                                                                            "warm_start": warm_start, 
                                                                                                                            }),
                                                                                                         }),
                                                                                  }),
                                                               }),
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        #print(expected_result._model_interface)
        #print(actual_result._model_interface)
        #self.assertTrue(*Mapping._inner_eq(expected_result._model_interface, actual_result._model_interface))
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip
    def test__compute_predicted_edges(self):
        # Test parameters
        model_interface = mock.create_autospec(OnlineInterface)
        model_interface.configuration = {}
        generator = mock.create_autospec(MentionPairSampleGenerator)
        generator.configuration = {}
        decoder = mock.create_autospec(AggressiveMergeCoreferenceDecoder)
        decoder.configuration = {}
        configuration = None
        resolver = PositiveGraphStructureResolver(model_interface, generator, decoder, configuration=configuration)
        ne_extent = NULL_MENTION.extent
        
        document = Document("test", "aaaaaaaaaaaaaaaaaaa")
        mentions = (Mention("test1", (1,2), document), #MockMention((1,2)), 
                    Mention("test1", (3,4), document), #MockMention((3,4)), 
                    Mention("test1", (5,6), document), #MockMention((5,6)),
                    Mention("test1", (7,8), document), #MockMention((7,8)),
                    Mention("test1", (9,10), document), #MockMention((9,10)),
                    )
        document.mentions = mentions
        extended_edges_scores = {(ne_extent,(1,2)): 0., (ne_extent,(3,4)): 0., (ne_extent,(5,6)): 0., (ne_extent,(7,8)): 0., (ne_extent,(9,10)): 0., 
                                 ((1,2),(3,4)): 0.5, ((1,2),(5,6)): -0.6, ((1,2),(7,8)): -0.6, ((1,2),(9,10)): -0.6, 
                                 ((3,4),(5,6)): 0.2, ((3,4),(7,8)): -0.6, ((3,4),(9,10)): -0.6, 
                                 ((5,6),(7,8)): -0.6, ((5,6),(9,10)): -0.6, 
                                 ((7,8),(9,10)): 0.3}
        expected_result = [((1,2),(3,4)), 
                           ((3,4),(5,6)), 
                           ((7,8),(9,10)),
                           ]
        
        # Test
        actual_result = resolver._compute_predicted_graph_edges(document, extended_edges_scores, tuple())
        self.assertEqual(expected_result, actual_result)



#@unittest.skip
class TestClosestFirstDocumentStructuredResolver(unittest.TestCase):
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = ClosestFirstDocumentStructuredResolver
        
        pair_hierarchy_factory_configuration = PairGramTypeHierarchy.define_factory_configuration()
        mention_hierarchy_factory_configuration = MentionNoneHierarchy.define_factory_configuration()
        with_singleton_features = True
        with_anaphoricity_features = False
        quantize = True
        args_1 = tuple()
        kwargs_1 = {"pair_hierarchy_factory_configuration": pair_hierarchy_factory_configuration, 
                    "mention_hierarchy_factory_configuration": mention_hierarchy_factory_configuration, 
                    "with_singleton_features": with_singleton_features,
                    "with_anaphoricity_features": with_anaphoricity_features,
                    "quantize": quantize, 
                    }
        sample_features_vectorizer_factory_configuration =\
         ExtendedMentionPairSampleFeaturesVectorizer.define_factory_configuration(*args_1, **kwargs_1)
        
        root_loss = 1.5
        
        iteration_nb = 42
        C = 14.0675
        avg = True
        warm_start = True
        classifier_factory_configuration = Mapping({"name": "PAOnlineBinaryClassifier",
                                                    "config": Mapping({"iteration_nb": iteration_nb,
                                                                       "C": C,
                                                                       "avg": avg,
                                                                       "warm_start": warm_start, 
                                                                       }),
                                                    })
        
        generator_pair_filter_factory_configuration = RecallPairFilter.define_factory_configuration(max_mention_distance=7.9)
        
        args = tuple()
        kwargs = {"sample_features_vectorizer_factory_configuration": sample_features_vectorizer_factory_configuration,
                  "root_loss": root_loss,
                  "classifier_factory_configuration": classifier_factory_configuration,
                  "generator_pair_filter_factory_configuration": generator_pair_filter_factory_configuration,
                  }
        expected_result = Mapping({"generator": Mapping({"name": "MentionPairSampleGenerator",
                                                         "config": Mapping({"pair_filter": generator_pair_filter_factory_configuration,
                                                                            }),
                                                         }),
                                   "decoder": Mapping({"name": "ClosestFirstCoreferenceDecoder",
                                                       "config": Mapping(),
                                                       }),
                                   "model_interface": Mapping({"name": "OnlineInterface",
                                                               "config": Mapping({"sample_features_vectorizer": sample_features_vectorizer_factory_configuration,
                                                                                  "classifier": Mapping({"name": "PAOnlineBinaryClassifier",
                                                                                                         "config": Mapping({"iteration_nb": iteration_nb,
                                                                                                                            "C": C,
                                                                                                                            "avg": avg,
                                                                                                                            "warm_start": warm_start, 
                                                                                                                            }),
                                                                                                         }),
                                                                                  }),
                                                               }),
                                   "root_loss": root_loss,
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        #print(expected_result._decoder)
        #print(actual_result._decoder)
        #self.assertTrue(*Mapping._inner_eq(expected_result._decoder.config, actual_result._decoder.config))
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip
    def test__compute_true_edges(self):
        # Test parameters
        model_interface = mock.create_autospec(OnlineInterface)
        model_interface.configuration = {}
        generator = mock.create_autospec(MentionPairSampleGenerator)
        generator.configuration = {}
        decoder = mock.create_autospec(ClosestFirstCoreferenceDecoder)
        decoder.configuration = {}
        root_loss = 1.5
        configuration = None
        resolver = ClosestFirstDocumentStructuredResolver(model_interface, generator, decoder, root_loss, 
                                                          configuration=configuration)
        
        ne_extent = NULL_MENTION.extent
        
        document = Document("test", "aaaaaaaaaaaaaaaaaaa")
        mentions = (Mention("test1", (1,2), document),
                    Mention("test1", (3,4), document),
                    Mention("test1", (5,6), document),
                    Mention("test1", (7,8), document), 
                    Mention("test1", (9,10), document),
                    )
        entities = (((1,2),(5,6),), 
                    ((7,8),(9,10),), 
                    )
        coreference_partition = CoreferencePartition(entities=entities)
        document.extent2mention = {mention.extent: mention for mention in mentions}
        document.coreference_partition = coreference_partition
        expected_result = [(ne_extent,(1,2),), ((1,2),(5,6)), (ne_extent,(7,8)), ((7,8),(9,10))]
        
        # Test
        actual_result = resolver._compute_ground_truth_graph_edges(document, tuple(), tuple(), tuple())
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__compute_predicted_edges(self):
        # Test parameters
        model_interface = mock.create_autospec(OnlineInterface)
        model_interface.configuration = {}
        generator = mock.create_autospec(MentionPairSampleGenerator)
        generator.configuration = {}
        decoder = mock.create_autospec(ClosestFirstCoreferenceDecoder)
        decoder.configuration = {}
        root_loss = 1.5
        configuration = None
        resolver = ClosestFirstDocumentStructuredResolver(model_interface, generator, decoder, root_loss, 
                                                          configuration=configuration)
        ne_extent = NULL_MENTION.extent
        
        document = Document("test", "aaaaaaaaaaaaaaaaaaa")
        mentions = (Mention("test1", (1,2), document),
                    Mention("test1", (3,4), document),
                    Mention("test1", (5,6), document),
                    Mention("test1", (7,8), document),
                    Mention("test1", (9,10), document),
                    )
        document.mentions = mentions
        extended_edges_scores = {(ne_extent,(1,2)): 0., (ne_extent,(3,4)): 0., (ne_extent,(5,6)): 0., (ne_extent,(7,8)): 0., (ne_extent,(9,10)): 0., 
                                 ((1,2),(3,4)): 0.5, ((1,2),(5,6)): -0.6, ((1,2),(7,8)): -0.6, ((1,2),(9,10)): -0.6, 
                                 ((3,4),(5,6)): 0.2, ((3,4),(7,8)): -0.6, ((3,4),(9,10)): -0.6, 
                                 ((5,6),(7,8)): -0.6, ((5,6),(9,10)): -0.6, 
                                 ((7,8),(9,10)): 0.3}
        expected_result = [(ne_extent,(1,2)),
                           ((1,2),(3,4)), 
                           ((3,4),(5,6)), 
                           (ne_extent,(7,8)),
                           ((7,8),(9,10)),
                           ]
        
        # Test
        actual_result = resolver._compute_predicted_graph_edges(document, extended_edges_scores, tuple())
        self.assertEqual(expected_result, actual_result)



#@unittest.skip
class TestExtendedClosestFirstDocumentStructuredResolver(unittest.TestCase):
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = ExtendedClosestFirstDocumentStructuredResolver
        
        pair_hierarchy_factory_configuration = PairGramTypeHierarchy.define_factory_configuration()
        mention_hierarchy_factory_configuration = MentionNoneHierarchy.define_factory_configuration()
        with_singleton_features = True
        with_anaphoricity_features = False
        quantize = True
        args_1 = tuple()
        kwargs_1 = {"pair_hierarchy_factory_configuration": pair_hierarchy_factory_configuration, 
                    "mention_hierarchy_factory_configuration": mention_hierarchy_factory_configuration, 
                    "with_singleton_features": with_singleton_features,
                    "with_anaphoricity_features": with_anaphoricity_features,
                    "quantize": quantize, 
                    }
        sample_features_vectorizer_factory_configuration =\
         ExtendedMentionPairSampleFeaturesVectorizer.define_factory_configuration(*args_1, **kwargs_1)
        
        root_loss = 1.5
        
        iteration_nb = 42
        C = 14.0675
        avg = True
        warm_start = True
        classifier_factory_configuration = Mapping({"name": "PAOnlineBinaryClassifier",
                                                    "config": Mapping({"iteration_nb": iteration_nb,
                                                                       "C": C,
                                                                       "avg": avg,
                                                                       "warm_start": warm_start, 
                                                                       }),
                                                    })
        
        generator_pair_filter_factory_configuration = RecallPairFilter.define_factory_configuration(max_mention_distance=7.9)
        
        args = tuple()
        kwargs = {"sample_features_vectorizer_factory_configuration": sample_features_vectorizer_factory_configuration,
                  "root_loss": root_loss,
                  "classifier_factory_configuration": classifier_factory_configuration,
                  "generator_pair_filter_factory_configuration": generator_pair_filter_factory_configuration,
                  }
        expected_result = Mapping({"generator": Mapping({"name": "ExtendedMentionPairSampleGenerator",
                                                         "config": Mapping({"pair_filter": generator_pair_filter_factory_configuration,
                                                                            }),
                                                         }),
                                   "decoder": Mapping({"name": "ClosestFirstCoreferenceDecoder",
                                                       "config": Mapping(),
                                                       }),
                                   "model_interface": Mapping({"name": "OnlineInterface",
                                                               "config": Mapping({"sample_features_vectorizer": sample_features_vectorizer_factory_configuration,
                                                                                  "classifier": Mapping({"name": "PAOnlineBinaryClassifier",
                                                                                                         "config": Mapping({"iteration_nb": iteration_nb,
                                                                                                                            "C": C,
                                                                                                                            "avg": avg,
                                                                                                                            "warm_start": warm_start, 
                                                                                                                            }),
                                                                                                         }),
                                                                                  }),
                                                               }),
                                   "root_loss": root_loss,
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        #print(expected_result._decoder)
        #print(actual_result._decoder)
        #self.assertTrue(*Mapping._inner_eq(expected_result._decoder.config, actual_result._decoder.config))
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip
    def test__compute_predicted_edges(self):
        # Test parameters
        model_interface = mock.create_autospec(OnlineInterface)
        model_interface.configuration = {}
        generator = mock.create_autospec(ExtendedMentionPairSampleGenerator)
        generator.configuration = {}
        decoder = mock.create_autospec(ClosestFirstCoreferenceDecoder)
        decoder.configuration = {}
        root_loss = 1.5
        configuration = None
        resolver = ExtendedClosestFirstDocumentStructuredResolver(model_interface, generator, decoder, 
                                                                  root_loss, configuration=configuration)
        ne_extent = NULL_MENTION.extent
        
        document = Document("test", "aaaaaaaaaaaaaaaaaaa")
        mentions = (Mention("test1", (1,2), document),
                    Mention("test1", (3,4), document),
                    Mention("test1", (5,6), document),
                    Mention("test1", (7,8), document),
                    Mention("test1", (9,10), document),
                    )
        document.mentions = mentions
        extended_edges_scores = {(ne_extent,(1,2)): 0., (ne_extent,(3,4)): 0.6, (ne_extent,(5,6)): 0., (ne_extent,(7,8)): 0., (ne_extent,(9,10)): 0., 
                                 ((1,2),(3,4)): 0.5, ((1,2),(5,6)): -0.6, ((1,2),(7,8)): -0.6, ((1,2),(9,10)): -0.6, 
                                 ((3,4),(5,6)): 0.2, ((3,4),(7,8)): -0.6, ((3,4),(9,10)): -0.6, 
                                 ((5,6),(7,8)): -0.6, ((5,6),(9,10)): -0.6, 
                                 ((7,8),(9,10)): 0.3}
        expected_result = [(ne_extent,(1,2)),
                           (ne_extent,(3,4)), 
                           ((3,4),(5,6)), 
                           (ne_extent,(7,8)),
                           ((7,8),(9,10)),
                           ]
        
        # Test
        actual_result = resolver._compute_predicted_graph_edges(document, extended_edges_scores, tuple())
        self.assertEqual(expected_result, actual_result)



#@unittest.skip
class TestBestFirstDocumentStructuredResolver(unittest.TestCase):
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = BestFirstDocumentStructuredResolver
        
        pair_hierarchy_factory_configuration = PairGramTypeHierarchy.define_factory_configuration()
        mention_hierarchy_factory_configuration = MentionNoneHierarchy.define_factory_configuration()
        with_singleton_features = True
        with_anaphoricity_features = False
        quantize = True
        args_1 = tuple()
        kwargs_1 = {"pair_hierarchy_factory_configuration": pair_hierarchy_factory_configuration, 
                    "mention_hierarchy_factory_configuration": mention_hierarchy_factory_configuration, 
                    "with_singleton_features": with_singleton_features,
                    "with_anaphoricity_features": with_anaphoricity_features,
                    "quantize": quantize, 
                    }
        sample_features_vectorizer_factory_configuration =\
         ExtendedMentionPairSampleFeaturesVectorizer.define_factory_configuration(*args_1, **kwargs_1)
        
        root_loss = 1.5
        update_mode = "ML"
        
        iteration_nb = 42
        C = 14.0675
        avg = True
        warm_start = True
        classifier_factory_configuration = Mapping({"name": "PAOnlineBinaryClassifier",
                                                    "config": Mapping({"iteration_nb": iteration_nb,
                                                                       "C": C,
                                                                       "avg": avg,
                                                                       "warm_start": warm_start, 
                                                                       }),
                                                    })
        
        generator_pair_filter_factory_configuration = RecallPairFilter.define_factory_configuration(max_mention_distance=7.9)
        
        args = tuple()
        kwargs = {"sample_features_vectorizer_factory_configuration": sample_features_vectorizer_factory_configuration,
                  "root_loss": root_loss,
                  "update_mode": update_mode,
                  "classifier_factory_configuration": classifier_factory_configuration,
                  "generator_pair_filter_factory_configuration": generator_pair_filter_factory_configuration,
                  }
        expected_result = Mapping({"generator": Mapping({"name": "MentionPairSampleGenerator",
                                                         "config": Mapping({"pair_filter": generator_pair_filter_factory_configuration,
                                                                            }),
                                                         }),
                                   "decoder": Mapping({"name": "BestFirstCoreferenceDecoder",
                                                       "config": Mapping(),
                                                       }),
                                   "model_interface": Mapping({"name": "OnlineInterface",
                                                               "config": Mapping({"sample_features_vectorizer": sample_features_vectorizer_factory_configuration,
                                                                                  "classifier": Mapping({"name": "PAOnlineBinaryClassifier",
                                                                                                         "config": Mapping({"iteration_nb": iteration_nb,
                                                                                                                            "C": C,
                                                                                                                            "avg": avg,
                                                                                                                            "warm_start": warm_start, 
                                                                                                                            }),
                                                                                                         }),
                                                                                  }),
                                                               }),
                                   "root_loss": root_loss,
                                   "update_mode": update_mode,
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        #print(expected_result._decoder)
        #print(actual_result._decoder)
        #self.assertTrue(*Mapping._inner_eq(expected_result._decoder.config, actual_result._decoder.config))
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip
    def test__compute_true_edges(self):
        # Test parameters
        resolver, (test_document, scores, true_scores, true_coreference_edges_scores), expected_result = get_best_first_document_structured_resolver_test__compute_true_edges_data()
        
        # Test
        actual_result = resolver._compute_ground_truth_graph_edges(test_document, scores, true_scores, true_coreference_edges_scores)
        expected_result.sort()
        actual_result.sort()
        self.assertEqual(expected_result, actual_result)
        
    #@unittest.skip
    def test__compute_predicted_edges_from_edges_loss_scores(self):
        # Test parameters
        resolver, (document, loss_scores), expected_result = get_best_first_document_structured_resolver_test__compute_predicted_edges_from_edges_loss_scores_data()
        
        # Test
        actual_result = resolver._compute_predicted_tree_edges_from_edge2loss_score(document, loss_scores)
        expected_result.sort()
        actual_result.sort()
        self.assertEqual(expected_result, actual_result)



#@unittest.skip
class TestExtendedBestFirstDocumentStructuredResolver(unittest.TestCase):
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = ExtendedBestFirstDocumentStructuredResolver
        
        pair_hierarchy_factory_configuration = PairGramTypeHierarchy.define_factory_configuration()
        mention_hierarchy_factory_configuration = MentionNoneHierarchy.define_factory_configuration()
        with_singleton_features = True
        with_anaphoricity_features = False
        quantize = True
        args_1 = tuple()
        kwargs_1 = {"pair_hierarchy_factory_configuration": pair_hierarchy_factory_configuration, 
                    "mention_hierarchy_factory_configuration": mention_hierarchy_factory_configuration, 
                    "with_singleton_features": with_singleton_features,
                    "with_anaphoricity_features": with_anaphoricity_features,
                    "quantize": quantize, 
                    }
        sample_features_vectorizer_factory_configuration =\
         ExtendedMentionPairSampleFeaturesVectorizer.define_factory_configuration(*args_1, **kwargs_1)
        
        root_loss = 1.5
        update_mode = "ML"
        
        iteration_nb = 42
        C = 14.0675
        avg = True
        warm_start = True
        classifier_factory_configuration = Mapping({"name": "PAOnlineBinaryClassifier",
                                                    "config": Mapping({"iteration_nb": iteration_nb,
                                                                       "C": C,
                                                                       "avg": avg,
                                                                       "warm_start": warm_start, 
                                                                       }),
                                                    })
        
        generator_pair_filter_factory_configuration = RecallPairFilter.define_factory_configuration(max_mention_distance=7.9)
        
        args = tuple()
        kwargs = {"sample_features_vectorizer_factory_configuration": sample_features_vectorizer_factory_configuration,
                  "root_loss": root_loss,
                  "update_mode": update_mode,
                  "classifier_factory_configuration": classifier_factory_configuration,
                  "generator_pair_filter_factory_configuration": generator_pair_filter_factory_configuration,
                  }
        expected_result = Mapping({"generator": Mapping({"name": "ExtendedMentionPairSampleGenerator",
                                                         "config": Mapping({"pair_filter": generator_pair_filter_factory_configuration,
                                                                            }),
                                                         }),
                                   "decoder": Mapping({"name": "ExtendedBestFirstCoreferenceDecoder",
                                                       "config": Mapping(),
                                                       }),
                                   "model_interface": Mapping({"name": "OnlineInterface",
                                                               "config": Mapping({"sample_features_vectorizer": sample_features_vectorizer_factory_configuration,
                                                                                  "classifier": Mapping({"name": "PAOnlineBinaryClassifier",
                                                                                                         "config": Mapping({"iteration_nb": iteration_nb,
                                                                                                                            "C": C,
                                                                                                                            "avg": avg,
                                                                                                                            "warm_start": warm_start, 
                                                                                                                            }),
                                                                                                         }),
                                                                                  }),
                                                               }),
                                   "root_loss": root_loss,
                                   "update_mode": update_mode,
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        #print(expected_result._decoder)
        #print(actual_result._decoder)
        #self.assertTrue(*Mapping._inner_eq(expected_result._decoder.config, actual_result._decoder.config))
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip
    def test__compute_predicted_edges_from_edges_loss_scores(self):
        # Test parameters
        resolver, (document, loss_scores), expected_result = get_extended_best_first_document_structured_resolver_test__compute_predicted_edges_from_edges_loss_scores_data()
        
        # Test
        actual_result = resolver._compute_predicted_tree_edges_from_edge2loss_score(document, loss_scores)
        expected_result.sort()
        actual_result.sort()
        self.assertEqual(expected_result, actual_result)




#@unittest.skip
class TestConstrainedBestFirstDocumentStructuredResolver(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        #Test parameters
        (args, kwargs), attribute_name_expected_result_pairs = get_constrained_best_first_document_structured_resolver_test___init___data()
        
        # Test
        resolver = ConstrainedBestFirstDocumentStructuredResolver(*args, **kwargs)
        for attribute_name, expected_result in attribute_name_expected_result_pairs:
            actual_result = getattr(resolver, attribute_name)
            self.assertEqual(expected_result, actual_result, attribute_name)
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        data = get_constrained_best_first_document_structured_resolver_test__inner_eq_data()
        
        # Test
        for resolver1, resolver2, expected_result in data:
            self.assertEqual(expected_result, resolver1._inner_eq(resolver2))
    
    #@unittest.skip    
    def test_from_configuration(self):
        # Test parameters
        configuration, expected_result = get_constrained_best_first_document_structured_resolver_test_from_configuration_data()
        
        # Test
        actual_result = ConstrainedBestFirstDocumentStructuredResolver.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = ConstrainedBestFirstDocumentStructuredResolver
        
        pair_hierarchy_factory_configuration = PairGramTypeHierarchy.define_factory_configuration()
        mention_hierarchy_factory_configuration = MentionNoneHierarchy.define_factory_configuration()
        with_singleton_features = True
        with_anaphoricity_features = False
        quantize = True
        args_1 = tuple()
        kwargs_1 = {"pair_hierarchy_factory_configuration": pair_hierarchy_factory_configuration, 
                    "mention_hierarchy_factory_configuration": mention_hierarchy_factory_configuration, 
                    "with_singleton_features": with_singleton_features,
                    "with_anaphoricity_features": with_anaphoricity_features,
                    "quantize": quantize, 
                    }
        sample_features_vectorizer_factory_configuration =\
         ExtendedMentionPairSampleFeaturesVectorizer.define_factory_configuration(*args_1, **kwargs_1)
        
        root_loss = 1.5
        update_mode = "ML"
        
        iteration_nb = 42
        C = 14.0675
        avg = True
        warm_start = True
        classifier_factory_configuration = Mapping({"name": "PAOnlineBinaryClassifier",
                                                    "config": Mapping({"iteration_nb": iteration_nb,
                                                                       "C": C,
                                                                       "avg": avg,
                                                                       "warm_start": warm_start, 
                                                                       }),
                                                    })
        
        generator_pair_filter_factory_configuration = RecallPairFilter.define_factory_configuration(max_mention_distance=7.9)
        
        constraints_definition = [("anaphoricity", tuple(), {"entity_head_extents": None}),
                                  ("animated", tuple(), {"entity_head_extents": None}),
                                  ]
        
        args = tuple()
        kwargs = {"sample_features_vectorizer_factory_configuration": sample_features_vectorizer_factory_configuration,
                  "root_loss": root_loss,
                  "update_mode": update_mode,
                  "classifier_factory_configuration": classifier_factory_configuration,
                  "generator_pair_filter_factory_configuration": generator_pair_filter_factory_configuration,
                  "constraints_definition": constraints_definition,
                  }
        expected_result = Mapping({"generator": Mapping({"name": "MentionPairSampleGenerator",
                                                         "config": Mapping({"pair_filter": generator_pair_filter_factory_configuration,
                                                                            }),
                                                         }),
                                   "decoder": Mapping({"name": "ConstrainedBestFirstCoreferenceDecoder",
                                                       "config": Mapping(),
                                                       }),
                                   "model_interface": Mapping({"name": "OnlineInterface",
                                                               "config": Mapping({"sample_features_vectorizer": sample_features_vectorizer_factory_configuration,
                                                                                  "classifier": Mapping({"name": "PAOnlineBinaryClassifier",
                                                                                                         "config": Mapping({"iteration_nb": iteration_nb,
                                                                                                                            "C": C,
                                                                                                                            "avg": avg,
                                                                                                                            "warm_start": warm_start, 
                                                                                                                            }),
                                                                                                         }),
                                                                                  }),
                                                               }),
                                   "root_loss": root_loss,
                                   "update_mode": update_mode,
                                   "constraints_definition": constraints_definition,
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        #print(expected_result.constraints_definition)
        #print(actual_result.constraints_definition)
        #self.assertTrue(*Mapping._inner_eq(expected_result._model_interface, actual_result._model_interface))
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip
    def test__compute_true_edges(self):
        # Test parameters
        (resolver, 
         (document, extended_edges_scores, true_extended_edges_scores, true_coreference_edges_scores), 
         expected_result) = get_constrained_best_first_document_structured_resolver_test__compute_true_edges_data()
        
        # Test
        actual_result = resolver._compute_ground_truth_graph_edges(document, extended_edges_scores, 
                                                     true_extended_edges_scores, 
                                                     true_coreference_edges_scores)
        expected_result.sort()
        actual_result.sort()
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__compute_predicted_edges_from_edges_loss_scores(self):
        # Test parameters
        resolver, (document, loss_scores), expected_result = get_constrained_best_first_document_structured_resolver_test__compute_predicted_edges_from_edges_loss_scores_data()
        
        # Test
        actual_result = resolver._compute_predicted_tree_edges_from_edge2loss_score(document, loss_scores)
        expected_result.sort()
        actual_result.sort()
        self.assertEqual(expected_result, actual_result)
    


#@unittest.skip
class TestConstrainedExtendedBestFirstDocumentStructuredResolver(unittest.TestCase):
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = ConstrainedExtendedBestFirstDocumentStructuredResolver
        
        pair_hierarchy_factory_configuration = PairGramTypeHierarchy.define_factory_configuration()
        mention_hierarchy_factory_configuration = MentionNoneHierarchy.define_factory_configuration()
        with_singleton_features = True
        with_anaphoricity_features = False
        quantize = True
        args_1 = tuple()
        kwargs_1 = {"pair_hierarchy_factory_configuration": pair_hierarchy_factory_configuration, 
                    "mention_hierarchy_factory_configuration": mention_hierarchy_factory_configuration, 
                    "with_singleton_features": with_singleton_features,
                    "with_anaphoricity_features": with_anaphoricity_features,
                    "quantize": quantize, 
                    }
        sample_features_vectorizer_factory_configuration =\
         ExtendedMentionPairSampleFeaturesVectorizer.define_factory_configuration(*args_1, **kwargs_1)
        
        root_loss = 1.5
        update_mode = "ML"
        
        iteration_nb = 42
        C = 14.0675
        avg = True
        warm_start = True
        classifier_factory_configuration = Mapping({"name": "PAOnlineBinaryClassifier",
                                                    "config": Mapping({"iteration_nb": iteration_nb,
                                                                       "C": C,
                                                                       "avg": avg,
                                                                       "warm_start": warm_start, 
                                                                       }),
                                                    })
        
        generator_pair_filter_factory_configuration = RecallPairFilter.define_factory_configuration(max_mention_distance=7.9)
        
        constraints_definition = [("anaphoricity", tuple(), {"entity_head_extents": None}),
                                  ("animated", tuple(), {"entity_head_extents": None}),
                                  ]
        
        args = tuple()
        kwargs = {"sample_features_vectorizer_factory_configuration": sample_features_vectorizer_factory_configuration,
                  "root_loss": root_loss,
                  "update_mode": update_mode,
                  "classifier_factory_configuration": classifier_factory_configuration,
                  "generator_pair_filter_factory_configuration": generator_pair_filter_factory_configuration,
                  "constraints_definition": constraints_definition,
                  }
        expected_result = Mapping({"generator": Mapping({"name": "ExtendedMentionPairSampleGenerator",
                                                         "config": Mapping({"pair_filter": generator_pair_filter_factory_configuration,
                                                                            }),
                                                         }),
                                   "decoder": Mapping({"name": "ConstrainedExtendedBestFirstCoreferenceDecoder",
                                                       "config": Mapping(),
                                                       }),
                                   "model_interface": Mapping({"name": "OnlineInterface",
                                                               "config": Mapping({"sample_features_vectorizer": sample_features_vectorizer_factory_configuration,
                                                                                  "classifier": Mapping({"name": "PAOnlineBinaryClassifier",
                                                                                                         "config": Mapping({"iteration_nb": iteration_nb,
                                                                                                                            "C": C,
                                                                                                                            "avg": avg,
                                                                                                                            "warm_start": warm_start, 
                                                                                                                            }),
                                                                                                         }),
                                                                                  }),
                                                               }),
                                   "root_loss": root_loss,
                                   "update_mode": update_mode,
                                   "constraints_definition": constraints_definition,
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        #print(expected_result.constraints_definition)
        #print(actual_result.constraints_definition)
        #self.assertTrue(*Mapping._inner_eq(expected_result._model_interface, actual_result._model_interface))
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip
    def test__compute_predicted_edges_from_edges_loss_scores(self):
        # Test parameters
        resolver, (document, loss_scores), expected_result = get_constrained_extended_best_first_document_structured_resolver_test__compute_predicted_edges_from_edges_loss_scores_data()
        
        # Test
        actual_result = resolver._compute_predicted_tree_edges_from_edge2loss_score(document, loss_scores)
        expected_result.sort()
        actual_result.sort()
        self.assertEqual(expected_result, actual_result)




def compare_float_values_dict(test_case, expected_result, actual_result):
    test_case.assertEqual(type(expected_result), type(actual_result))
    test_case.assertEqual(set(expected_result), set(actual_result))
    keys = tuple(sorted(expected_result))
    expected_values = numpy.array(tuple(expected_result[k] for k in keys))
    actual_values = numpy.array(tuple(expected_result[k] for k in keys))
    test_case.assertTrue(are_array_equal(expected_values, actual_values))

def get_test_document_data():
    from cortex.io.pivot_reader import PivotReader
    
    folder_path = os.path.join(os.path.dirname(__file__), "data", )
    document_folder_path = os.path.join(folder_path, "resolver_test_data", "ref")
    
    strict = True # We want to synchronize head tokens.
    test_document = PivotReader.parse(document_folder_path, strict=strict)
    
    return test_document

@Memoized
def get_memoized_test_document_data():
    return get_test_document_data()


def get_mst_resolver_test___init___data():
    attribute_name_expected_result_pairs = []
    
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "ExtendedMentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "mention_hierarchy": Mapping({"name": "MentionGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "with_singleton_features": False, 
                                                                                                    "with_anaphoricity_features": False, 
                                                                                                    "quantize": True, 
                                                                                                    "strict": False, 
                                                                                                    "useless_field": "useless_value"})
                                                                                 }), 
                                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                    "config": Mapping({"iteration_nb": 2, 
                                                                                       "avg": True, 
                                                                                       "warm_start": False, 
                                                                                       "alpha": 1.52, 
                                                                                       "useless_field": "useless_value"}),
                                                                    })
                                            })
    model_interface = create_model_interface_from_factory_configuration(Mapping((("name", "OnlineInterface"), ("config", model_interface_configuration))))
    attribute_name_expected_result_pairs.append(("_model_interface",model_interface))
    
    generator_configuration = Mapping([("pair_filter", Mapping([("name", "PairFiltersCombination"), 
                                                               ("config", Mapping([("filters", [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                       "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                       }),
                                                                                                                   ]),
                                                                                   ("witness_field6", "witness_value6"),
                                                                                   ])
                                                                ),
                                                                ])
                                      ),
                                     ("witness_field7", "witness_value7"),
                                     ])
    generator = MentionPairSampleGenerator.from_configuration(generator_configuration)
    attribute_name_expected_result_pairs.append(("_generator",generator))
    
    decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    decoder = MSTPrimCoreferenceDecoder.from_configuration(decoder_configuration)
    attribute_name_expected_result_pairs.append(("_decoder",decoder))
    
    root_loss = 1.2
    attribute_name_expected_result_pairs.append(("root_loss",root_loss))
    
    update_mode = "ML"
    attribute_name_expected_result_pairs.append(("update_mode",update_mode))
    
    configuration = Mapping({"witness_field9": "witness_value9"})
    
    expected_model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "ExtendedMentionPairSampleFeaturesVectorizer",
                                                                                          "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                             "mention_hierarchy": Mapping({"name": "MentionGramTypeHierarchy", "config": Mapping()}), 
                                                                                                             "with_singleton_features": False, 
                                                                                                             "with_anaphoricity_features": False, 
                                                                                                             "quantize": True, 
                                                                                                             "strict": False, 
                                                                                                             "useless_field": "useless_value"})
                                                                                        }), 
                                                     "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                            "config": Mapping({"iteration_nb": 2, 
                                                                                               "avg": True, 
                                                                                               "warm_start": False, 
                                                                                               "alpha": 1.52, 
                                                                                               "useless_field": "useless_value"}),
                                                                            })
                                                      })
    expected_model_interface_configuration = Mapping({"name": "OnlineInterface", "config": expected_model_interface_configuration})
    expected_generator_configuration = Mapping([("pair_filter", Mapping([("name", "PairFiltersCombination"), 
                                                                           ("config", Mapping([("filters", [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                                   "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                                   }),
                                                                                                                               ]),
                                                                                               ("witness_field6", "witness_value6"),
                                                                                               ])
                                                                            ),
                                                                            ])
                                                  ),
                                                 ("witness_field7", "witness_value7"),
                                                 ])
    expected_generator_configuration = Mapping({"name": "MentionPairSampleGenerator", "config": expected_generator_configuration})
    expected_decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    expected_decoder_configuration = Mapping({"name": "MSTPrimCoreferenceDecoder", "config": expected_decoder_configuration})
    expected_configuration = Mapping({"model_interface": expected_model_interface_configuration,
                                      "generator": expected_generator_configuration,
                                      "decoder": expected_decoder_configuration,
                                      "root_loss": root_loss, 
                                      "update_mode": update_mode, 
                                      "witness_field9": "witness_value9",
                                     })
    
    attribute_name_expected_result_pairs.append(("configuration", expected_configuration))
    
    args = (model_interface, generator, decoder, root_loss, update_mode)
    kwargs = {"configuration": configuration}
    
    return (args, kwargs), attribute_name_expected_result_pairs

def get_mst_resolver_test__inner_eq_data():
    data = []
    
    (args, kwargs), _ = get_mst_resolver_test___init___data() #attribute_name_expected_result_pairs
    
    resolver1 = MSTResolver(*args, **kwargs)
    resolver2 = MSTResolver(*args, **kwargs)
    expected_result = (True, None)
    data.append((resolver1, resolver2, expected_result))
    
    update_mode = "PB"
    args3 = args[:-1] + (update_mode,)
    resolver3 = MSTResolver(*args3, **kwargs)
    expected_result = (False, "update_mode")
    data.append((resolver1, resolver3, expected_result))
    
    root_loss = 1.9
    args4 = args[:-2] + (root_loss,) + args[-1:]
    resolver4 = MSTResolver(*args4, **kwargs)
    expected_result = (False, "root_loss")
    data.append((resolver1, resolver4, expected_result))
    
    return data

def get_mst_resolver_test_from_configuration_data():
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "ExtendedMentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy"}), 
                                                                                                    "mention_hierarchy": Mapping({"name": "MentionGramTypeHierarchy"}), 
                                                                                                    "with_singleton_features": False, 
                                                                                                    "with_anaphoricity_features": False, 
                                                                                                    "quantize": True, 
                                                                                                    "useless_field": "useless_value"})
                                                                                 }), 
                                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                    "config": Mapping({"iteration_nb": 2, 
                                                                                       "avg": True, 
                                                                                       "warm_start": False, 
                                                                                       "alpha": 1.52, 
                                                                                       "useless_field": "useless_value"}),
                                                                    })
                                            })
    model_interface = create_model_interface_from_factory_configuration(Mapping((("name", "OnlineInterface"), ("config", model_interface_configuration))))
    
    generator_configuration = Mapping([("pair_filter", Mapping([("name", "PairFiltersCombination"), 
                                                               ("config", Mapping([("filters", [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                       "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                       }),
                                                                                                                   ]),
                                                                                   ("witness_field6", "witness_value6"),
                                                                                   ])
                                                                ),
                                                                ])
                                      ),
                                     ("witness_field7", "witness_value7"),
                                     ])
    generator = MentionPairSampleGenerator.from_configuration(generator_configuration)
    
    decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    decoder = MSTPrimCoreferenceDecoder.from_configuration(decoder_configuration)
    
    root_loss = 1.2
    
    update_mode = "ML"
    
    configuration = Mapping({"witness_field9": "witness_value9"})
    
    args = (model_interface, generator, decoder, root_loss, update_mode)
    kwargs = {"configuration": configuration}
    expected_result = MSTResolver(*args, **kwargs)
    
    configuration = Mapping({"model_interface": Mapping((("name", "OnlineInterface"), ("config", model_interface_configuration))),
                              "generator": Mapping((("name", "MentionPairSampleGenerator"), ("config", generator_configuration))),
                              "decoder": Mapping((("name", "MSTPrimCoreferenceDecoder"), ("config", decoder_configuration))),
                              "root_loss": root_loss, 
                              "update_mode": update_mode, 
                              "witness_field9": "witness_value9",
                             })
    
    return configuration, expected_result


def get_mst_resolver_test__compute_training_pair_scores_data():
    ne_extent = NULL_MENTION.extent
    
    # Parameters
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "ExtendedMentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy"}), 
                                                                                                    "mention_hierarchy": Mapping({"name": "MentionGramTypeHierarchy"}), 
                                                                                                    "with_singleton_features": False, 
                                                                                                    "with_anaphoricity_features": False, 
                                                                                                    "quantize": True, 
                                                                                                    "useless_field": "useless_value",
                                                                                                    "strict": True})
                                                                                 }), 
                                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                    "config": Mapping({"iteration_nb": 2, 
                                                                                       "avg": True, 
                                                                                       "warm_start": False, 
                                                                                       "alpha": 1.52, 
                                                                                       "useless_field": "useless_value"}),
                                                                    })
                                            })
    model_interface_configuration = Mapping({"name": "OnlineInterface", "config": model_interface_configuration})
    
    generator_configuration = Mapping({"pair_filter": Mapping({"name": "AcceptAllPairFilter"})})
    generator_configuration = Mapping({"name": "MentionPairSampleGenerator", "config": generator_configuration,})
    
    decoder_configuration = Mapping({"name": "MSTPrimCoreferenceDecoder"})
    root_loss = 1.2
    update_mode = "ML"
    configuration = Mapping({"model_interface": model_interface_configuration,
                              "generator": generator_configuration,
                              "decoder": decoder_configuration,
                              "root_loss": root_loss, 
                              "update_mode": update_mode,
                             })
    
    model_interface = create_model_interface_from_factory_configuration(model_interface_configuration)
    #model_interface_folder_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "mst", "train_test_online_model_interface")
    #model_interface.load_model(model_interface_folder_path)
    ''''''
    base_coef_ = 1*2*3*4*5*6*7*8*9 *numpy.ones((1,model_interface._classification_sample_converter.features_nb), dtype=numpy.float)
    model_interface._classifier.coef_ = 1* base_coef_
    model_interface._classifier._cumulative_coef_ = 2* base_coef_
    ''''''
    #test_document = get_memoized_test_document_data()
    #generator = create_generator_from_factory_configuration(generator_configuration)
    #ML_samples = tuple(generator.generate_training_set((test_document,)))
    #model_interface.learn(ML_samples)
    #print("\n".join("{}: {},".format(sample.extent_pair, prediction.get_score()) for sample, prediction in zip(ML_samples, model_interface.predict(ML_samples))))
    
    resolver = MSTResolver.from_configuration(configuration)
    resolver._model_interface = model_interface
    test_document = get_memoized_test_document_data()
    
    expected_extended_edges_scores = {((1, 9), (14, 42)): 36590400.0, #POS_CLASS
                                        ((14, 42), (27, 42)): 39666240.0,
                                        ((1, 9), (27, 42)): 35976960.0,
                                        ((27, 42), (46, 47)): 36605952.0,
                                        ((14, 42), (46, 47)): 37884672.0, #POS_CLASS
                                        ((1, 9), (46, 47)): 38308032.0, #POS_CLASS
                                        ((46, 47), (68, 75)): 28459872.0,
                                        ((27, 42), (68, 75)): 59241600.0, #POS_CLASS
                                        ((14, 42), (68, 75)): 38747520.0,
                                        ((1, 9), (68, 75)): 39170880.0,
                                        
                                        ((68, 75), (83, 84)): 28446912.0,
                                        ((46, 47), (83, 84)): 54359424.0, #POS_CLASS
                                        ((27, 42), (83, 84)): 39267072.0,
                                        ((14, 42), (83, 84)): 40545792.0, #POS_CLASS
                                        ((1, 9), (83, 84)): 40969152.0, #POS_CLASS
                                        ((83, 84), (114, 120)): 30435552.0,
                                        
                                        ((68, 75), (114, 120)): 31449600.0,
                                        ((46, 47), (114, 120)): 32945472.0,
                                        ((27, 42), (114, 120)): 41016960.0,
                                        ((14, 42), (114, 120)): 42174720.0,
                                        ((1, 9), (114, 120)): 42477120.0,
                                        
                                        (ne_extent, (1, 9)): 0.,
                                        (ne_extent, (14, 42)): 0.,
                                        (ne_extent, (27, 42)): 0.,
                                        (ne_extent, (46, 47)): 0.,
                                        (ne_extent, (68, 75)): 0.,
                                        (ne_extent, (83, 84)): 0.,
                                        (ne_extent, (114, 120)): 0.,
                                      }
    expected_true_extended_edges_scores = {((1, 9), (14, 42)): 36590400.0, #POS_CLASS
                                          ((14, 42), (46, 47)): 37884672.0, #POS_CLASS
                                          ((1, 9), (46, 47)): 38308032.0, #POS_CLASS
                                          ((27, 42), (68, 75)): 59241600.0, #POS_CLASS
                                          ((46, 47), (83, 84)): 54359424.0, #POS_CLASS
                                          ((14, 42), (83, 84)): 40545792.0, #POS_CLASS
                                          ((1, 9), (83, 84)): 40969152.0, #POS_CLASS
                                          (ne_extent, (1, 9)): 0.,
                                          (ne_extent, (14, 42)): 0.,
                                          (ne_extent, (27, 42)): 0.,
                                          (ne_extent, (46, 47)): 0.,
                                          (ne_extent, (68, 75)): 0.,
                                          (ne_extent, (83, 84)): 0.,
                                          (ne_extent, (114, 120)): 0.,
                                          }
    expected_true_coreference_edges_scores = {((1, 9), (14, 42)): 36590400.0, #POS_CLASS
                                              ((14, 42), (46, 47)): 37884672.0, #POS_CLASS
                                              ((1, 9), (46, 47)): 38308032.0, #POS_CLASS
                                              ((27, 42), (68, 75)): 59241600.0, #POS_CLASS
                                              ((46, 47), (83, 84)): 54359424.0, #POS_CLASS
                                              ((14, 42), (83, 84)): 40545792.0, #POS_CLASS
                                              ((1, 9), (83, 84)): 40969152.0, #POS_CLASS
                                              }
    expected_results = (expected_extended_edges_scores, 
                        expected_true_extended_edges_scores, 
                        expected_true_coreference_edges_scores)
    
    return resolver, test_document, expected_results


def get_mst_resolver_test__compute_prediction_pair_scores_data():
    ne_extent = NULL_MENTION.extent
    
    # Parameters
    resolver, test_document, _ = get_mst_resolver_test__compute_training_pair_scores_data() # expected_results
    expected_extended_edges_scores = {((1, 9), (14, 42)): 36590400.0,
                                                                        ((14, 42), (27, 42)): 39666240.0,
                                                                        ((1, 9), (27, 42)): 35976960.0,
                                                                        ((27, 42), (46, 47)): 36605952.0,
                                                                        ((14, 42), (46, 47)): 37884672.0,
                                                                        ((1, 9), (46, 47)): 38308032.0,
                                                                        ((46, 47), (68, 75)): 28459872.0,
                                                                        ((27, 42), (68, 75)): 59241600.0,
                                                                        ((14, 42), (68, 75)): 38747520.0,
                                                                        ((1, 9), (68, 75)): 39170880.0,
                                                                        
                                                                        ((68, 75), (83, 84)): 28446912.0,
                                                                        ((46, 47), (83, 84)): 54359424.0,
                                                                        ((27, 42), (83, 84)): 39267072.0,
                                                                        ((14, 42), (83, 84)): 40545792.0,
                                                                        ((1, 9), (83, 84)): 40969152.0,
                                                                        ((83, 84), (114, 120)): 30435552.0,
                                                                        
                                                                        ((68, 75), (114, 120)): 31449600.0,
                                                                        ((46, 47), (114, 120)): 32945472.0,
                                                                        ((27, 42), (114, 120)): 41016960.0,
                                                                        ((14, 42), (114, 120)): 42174720.0,
                                                                        ((1, 9), (114, 120)): 42477120.0,
                                                                        
                                                                        (ne_extent, (1, 9)): 0.,
                                                                        (ne_extent, (14, 42)): 0.,
                                                                        (ne_extent, (27, 42)): 0.,
                                                                        (ne_extent, (46, 47)): 0.,
                                                                        (ne_extent, (68, 75)): 0.,
                                                                        (ne_extent, (83, 84)): 0.,
                                                                        (ne_extent, (114, 120)): 0.,
                                                                      }
    
    return resolver, test_document, expected_extended_edges_scores


def get_mst_resolver_test__compute_true_edges_data():
    ne_extent = NULL_MENTION.extent
    
    resolver, test_document, _ = get_mst_resolver_test__compute_training_pair_scores_data() # expected_results
    scores = {((1, 9), (14, 42)): 36590400.0, #POS_CLASS
                ((14, 42), (27, 42)): 39666240.0,
                ((1, 9), (27, 42)): 35976960.0,
                ((27, 42), (46, 47)): 36605952.0,
                ((14, 42), (46, 47)): 37884672.0, #POS_CLASS
                ((1, 9), (46, 47)): 38308032.0, #POS_CLASS
                ((46, 47), (68, 75)): 28459872.0,
                ((27, 42), (68, 75)): 59241600.0, #POS_CLASS
                ((14, 42), (68, 75)): 38747520.0,
                ((1, 9), (68, 75)): 39170880.0,
                
                ((68, 75), (83, 84)): 28446912.0,
                ((46, 47), (83, 84)): 54359424.0, #POS_CLASS
                ((27, 42), (83, 84)): 39267072.0,
                ((14, 42), (83, 84)): 40545792.0, #POS_CLASS
                ((1, 9), (83, 84)): 40969152.0, #POS_CLASS
                ((83, 84), (114, 120)): 30435552.0,
                
                ((68, 75), (114, 120)): 31449600.0,
                ((46, 47), (114, 120)): 32945472.0,
                ((27, 42), (114, 120)): 41016960.0,
                ((14, 42), (114, 120)): 42174720.0,
                ((1, 9), (114, 120)): 42477120.0,
                
                (ne_extent, (1, 9)): 0.,
                (ne_extent, (14, 42)): 0.,
                (ne_extent, (27, 42)): 0.,
                (ne_extent, (46, 47)): 0.,
                (ne_extent, (68, 75)): 0.,
                (ne_extent, (83, 84)): 0.,
                (ne_extent, (114, 120)): 0.,
              }
    true_scores = {((1, 9), (14, 42)): 36590400.0, #POS_CLASS
                  ((14, 42), (46, 47)): 37884672.0, #POS_CLASS
                  ((1, 9), (46, 47)): 38308032.0, #POS_CLASS
                  ((27, 42), (68, 75)): 59241600.0, #POS_CLASS
                  ((46, 47), (83, 84)): 54359424.0, #POS_CLASS
                  ((14, 42), (83, 84)): 40545792.0, #POS_CLASS
                  ((1, 9), (83, 84)): 40969152.0, #POS_CLASS
                  (ne_extent, (1, 9)): 0.,
                  (ne_extent, (14, 42)): 0.,
                  (ne_extent, (27, 42)): 0.,
                  (ne_extent, (46, 47)): 0.,
                  (ne_extent, (68, 75)): 0.,
                  (ne_extent, (83, 84)): 0.,
                  (ne_extent, (114, 120)): 0.,
                  }
    cluster_scores = {((1, 9), (14, 42)): 36590400.0, #POS_CLASS # Nope
                      ((14, 42), (46, 47)): 37884672.0, #POS_CLASS #yep
                      ((1, 9), (46, 47)): 38308032.0, #POS_CLASS #yep
                      ((27, 42), (68, 75)): 59241600.0, #POS_CLASS #yep
                      ((46, 47), (83, 84)): 54359424.0, #POS_CLASS #yep
                      ((14, 42), (83, 84)): 40545792.0, #POS_CLASS #nope
                      ((1, 9), (83, 84)): 40969152.0, #POS_CLASS #nope
                      }
    expected_result = [(NULL_MENTION.extent, (1, 9)),
                       ((1, 9), (83, 84)), 
                       ((14, 42), (83, 84)),
                       ((46, 47), (83, 84)),  
                       (NULL_MENTION.extent, (27, 42)),
                       ((27, 42), (68, 75)),
                       (NULL_MENTION.extent, (114, 120)),
                       ]
    
    return resolver, (test_document, scores, true_scores, cluster_scores), expected_result


def get_mst_resolver_test__compute_predicted_edges_from_edges_loss_scores_data():
    ne_extent = NULL_MENTION.extent
    
    resolver, test_document, _ = get_mst_resolver_test__compute_training_pair_scores_data() # expected_results
    resolver.update_mode = "PB"
    loss_scores = {((0, 0), (14, 42)): 0.0, ((83, 84), (114, 120)): 30435552.0, 
                   ((1, 9), (83, 84)): 40969152.0, ((14, 42), (46, 47)): 37884672.0, 
                   ((0, 0), (68, 75)): 0.0, ((0, 0), (1, 9)): 0.0, ((1, 9), (68, 75)): 39170880.0, 
                   ((1, 9), (114, 120)): 42477120.0, ((14, 42), (27, 42)): 39666240.0, 
                   ((0, 0), (114, 120)): 0.0, ((0, 0), (83, 84)): 0.0, ((27, 42), (83, 84)): 39267072.0, 
                   ((1, 9), (14, 42)): 36590400.0, ((1, 9), (27, 42)): 35976960.0, 
                   ((1, 9), (46, 47)): 38308032.0, ((27, 42), (68, 75)): 59241600.0, 
                   ((68, 75), (114, 120)): 31449600.0, ((0, 0), (46, 47)): 0.0, 
                   ((46, 47), (68, 75)): 28459872.0, ((14, 42), (68, 75)): 38747520.0, 
                   ((68, 75), (83, 84)): 28446912.0, ((0, 0), (27, 42)): 0.0, 
                   ((46, 47), (83, 84)): 54359424.0, ((14, 42), (83, 84)): 40545792.0, 
                   ((27, 42), (114, 120)): 41016960.0, ((14, 42), (114, 120)): 42174720.0, 
                   ((46, 47), (114, 120)): 32945472.0, ((27, 42), (46, 47)): 36605952.0}
    #glop = resolver._compute_edge2loss_score_for_predicted_tree_edges_computation(scores, true_mst_edges)
    #print(glop)
    #from cortex.tools.graph.tree import prim_mst
    #print(prim_mst(tuple(m.extent for m in test_document.mentions), scores)[1])
    expected_result = [((1, 9), (114, 120)), 
                       ((14, 42), (114, 120)), 
                       ((27, 42), (114, 120)), 
                       ((27, 42), (68, 75)), 
                       ((1, 9), (83, 84)), 
                       ((46, 47), (83, 84)), 
                       (ne_extent, (1, 9))]
    
    return resolver, (test_document, loss_scores), expected_result


def get_mst_resolver_test__compute_graph_loss_data():
    ne_extent = NULL_MENTION.extent
    resolver, _, _ = get_mst_resolver_test__compute_training_pair_scores_data() # test_document, expected_results
    resolver.root_loss = 2.5
    true_edges = [(ne_extent, (1, 9)),
                   ((1, 9), (83, 84)), 
                   ((14, 42), (83, 84)),
                   ((46, 47), (83, 84)),  
                   (ne_extent, (27, 42)),
                   ((27, 42), (68, 75)),
                   (ne_extent, (114, 120)),
                   ]
    predicted_edges = [((1, 9), (114, 120)), 
                       ((14, 42), (114, 120)), 
                       ((27, 42), (114, 120)), 
                       ((27, 42), (68, 75)), 
                       ((1, 9), (83, 84)), 
                       ((46, 47), (83, 84)), 
                       (ne_extent, (1, 9))]
    
    expected_result = 1.+1.+1. + 1.+2.5+2.5
    
    return resolver, (true_edges, predicted_edges), expected_result


def get_mst_resolver_test_train_document_data():
    resolver, test_document, _ = get_mst_resolver_test__compute_training_pair_scores_data() # expected_results
    resolver = type(resolver).from_configuration(resolver.configuration)
    resolver._model_interface.initialize_model()
    
    expected_model_interface = create_model_interface_from_factory_configuration(resolver.configuration.model_interface)
    model_folder_path = os.path.join(os.path.dirname(__file__), "data", "mst", "train_test_online_model_interface_perceptron_model")
    expected_model_interface.load_model(model_folder_path)
    
    return resolver, test_document, expected_model_interface


def get_mst_resolver_test_train_data():
    resolver, test_document, _ = get_mst_resolver_test__compute_training_pair_scores_data() # expected_results
    resolver = type(resolver).from_configuration(resolver.configuration)
    resolver._model_interface.initialize_model()
    
    expected_model_interface = create_model_interface_from_factory_configuration(resolver.configuration.model_interface)
    model_folder_path = os.path.join(os.path.dirname(__file__), "data", "mst", "train_test_online_model_interface_perceptron_model")
    expected_model_interface.load_model(model_folder_path)
    
    documents_iterable = (d for d in (test_document,))
    
    kwargs = {}
    
    return resolver, documents_iterable, kwargs, expected_model_interface


def get_mst_resolver_test__pipeline_resolve_data():
    data = []
    
    test_document = get_memoized_test_document_data()
    
    entity_heads = None
    singletons = None
    with_singleton_features = False
    with_anaphoricity_features = False
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "ExtendedMentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy"}), 
                                                                                                    "mention_hierarchy": Mapping({"name": "MentionGramTypeHierarchy"}), 
                                                                                                    "with_singleton_features": with_singleton_features, 
                                                                                                    "with_anaphoricity_features": with_anaphoricity_features, 
                                                                                                    "quantize": True, 
                                                                                                    "useless_field": "useless_value"})
                                                                                 }), 
                                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                    "config": Mapping({"iteration_nb": 2, 
                                                                                       "avg": True, 
                                                                                       "warm_start": False, 
                                                                                       "alpha": 1.52, 
                                                                                       "useless_field": "useless_value"}),
                                                                    })
                                            })
    model_interface_configuration = Mapping({"name": "OnlineInterface", "config": model_interface_configuration})
    
    generator_configuration = Mapping({"pair_filter": Mapping({"name": "AcceptAllPairFilter"})})
    generator_configuration = Mapping({"name": "MentionPairSampleGenerator", "config": generator_configuration,})
    
    decoder_configuration = Mapping({"name": "MSTPrimCoreferenceDecoder"})
    root_loss = 1.2
    update_mode = "ML"
    configuration = Mapping({"model_interface": model_interface_configuration,
                              "generator": generator_configuration,
                              "decoder": decoder_configuration,
                              "root_loss": root_loss, 
                              "update_mode": update_mode,
                             })
    
    resolver = MSTResolver.from_configuration(configuration)
    
    model_folder_path = os.path.join(os.path.dirname(__file__), "data", "mst", "train_test_online_model_interface_perceptron_model")
    resolver._model_interface.load_model(model_folder_path)
    
    kwargs = {"entity_head_extents": entity_heads, "singleton_extents": singletons}
    
    pos_coref_pairs = (((1,9), (14,42)),
                       ((1,9), (27,42)),
                       ((27,42), (68,75)),
                       ((27,42), (46,47)),
                       ((46,47), (83,84)),
                       )
    #excluded_pairs = ((46,47), (83,84))
    excluded_pairs = tuple()
    expected_result = CoreferencePartition(mention_extents=tuple(m.extent for m in test_document.mentions if m.extent not in excluded_pairs))
    for left_extent, right_extent in pos_coref_pairs:
        left_mention = test_document.extent2mention[left_extent]
        right_mention = test_document.extent2mention[right_extent]
        expected_result.join(left_mention.extent, right_mention.extent)
    
    data.append((test_document, resolver, kwargs, expected_result))
    
    return data

def get_mst_resolver_test_resolve_data():
    data0 = get_mst_resolver_test__pipeline_resolve_data()
    data = []
    for test_document, resolver, kwargs, expected_result in data0:
        kwargs = {}
        data.append((test_document, resolver, kwargs, expected_result))
    return data



def get_hybrid_best_first_mst_resolver_test___init___data():
    attribute_name_expected_result_pairs = []
    
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "ExtendedMentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "mention_hierarchy": Mapping({"name": "MentionGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "with_singleton_features": False, 
                                                                                                    "with_anaphoricity_features": False, 
                                                                                                    "quantize": True, 
                                                                                                    "strict": False, 
                                                                                                    "useless_field": "useless_value"})
                                                                                 }), 
                                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                    "config": Mapping({"iteration_nb": 2, 
                                                                                       "avg": True, 
                                                                                       "warm_start": False, 
                                                                                       "alpha": 1.52, 
                                                                                       "useless_field": "useless_value"}),
                                                                    })
                                            })
    model_interface = create_model_interface_from_factory_configuration(Mapping((("name", "OnlineInterface"), ("config", model_interface_configuration))))
    attribute_name_expected_result_pairs.append(("_model_interface",model_interface))
    
    generator_configuration = Mapping([("pair_filter", Mapping([("name", "PairFiltersCombination"), 
                                                               ("config", Mapping([("filters", [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                       "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                       }),
                                                                                                                   ]),
                                                                                   ("witness_field6", "witness_value6"),
                                                                                   ])
                                                                ),
                                                                ])
                                      ),
                                     ("witness_field7", "witness_value7"),
                                     ])
    generator = MentionPairSampleGenerator.from_configuration(generator_configuration)
    attribute_name_expected_result_pairs.append(("_generator",generator))
    
    decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    decoder = HybridBestFirstMSTCoreferenceDecoder.from_configuration(decoder_configuration)
    attribute_name_expected_result_pairs.append(("_decoder",decoder))
    
    root_loss = 1.2
    attribute_name_expected_result_pairs.append(("root_loss",root_loss))
    
    update_mode = "ML"
    attribute_name_expected_result_pairs.append(("update_mode",update_mode))
    
    configuration = Mapping({"witness_field9": "witness_value9"})
    
    expected_model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "ExtendedMentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "mention_hierarchy": Mapping({"name": "MentionGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "with_singleton_features": False, 
                                                                                                    "with_anaphoricity_features": False, 
                                                                                                    "quantize": True, 
                                                                                                    "strict": True,
                                                                                                    "strict": False, 
                                                                                                    "useless_field": "useless_value"})
                                                                                        }), 
                                                     "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                            "config": Mapping({"iteration_nb": 2, 
                                                                                               "avg": True, 
                                                                                               "warm_start": False, 
                                                                                               "alpha": 1.52, 
                                                                                               "useless_field": "useless_value"}),
                                                                            })
                                                      })
    expected_model_interface_configuration = Mapping({"name": "OnlineInterface", "config": expected_model_interface_configuration})
    expected_generator_configuration = Mapping([("pair_filter", Mapping([("name", "PairFiltersCombination"), 
                                                                           ("config", Mapping([("filters", [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                                   "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                                   }),
                                                                                                                               ]),
                                                                                               ("witness_field6", "witness_value6"),
                                                                                               ])
                                                                            ),
                                                                            ])
                                                  ),
                                                 ("witness_field7", "witness_value7"),
                                                 ])
    expected_generator_configuration = Mapping({"name": "MentionPairSampleGenerator", "config": expected_generator_configuration})
    expected_decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    expected_decoder_configuration = Mapping({"name": "HybridBestFirstMSTCoreferenceDecoder", "config": expected_decoder_configuration})
    expected_configuration = Mapping({"model_interface": expected_model_interface_configuration,
                                      "generator": expected_generator_configuration,
                                      "decoder": expected_decoder_configuration,
                                      "root_loss": root_loss, 
                                      "update_mode": update_mode, 
                                      "witness_field9": "witness_value9",
                                     })
    
    attribute_name_expected_result_pairs.append(("configuration", expected_configuration))
    
    args = (model_interface, generator, decoder, root_loss, update_mode)
    kwargs = {"configuration": configuration}
    
    return (args, kwargs), attribute_name_expected_result_pairs


def get_hybrid_best_first_mst_resolver_test__compute_true_edges_data():
    ne_extent = NULL_MENTION.extent
    
    test_document = get_memoized_test_document_data()
    entity_heads = None
    singletons = None
    with_singleton_features = False
    with_anaphoricity_features = False
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "ExtendedMentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "mention_hierarchy": Mapping({"name": "MentionGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "with_singleton_features": with_singleton_features, 
                                                                                                    "with_anaphoricity_features": with_anaphoricity_features, 
                                                                                                    "quantize": True, 
                                                                                                    "useless_field": "useless_value"})
                                                                                 }), 
                                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                    "config": Mapping({"iteration_nb": 2, 
                                                                                       "avg": True, 
                                                                                       "warm_start": False, 
                                                                                       "alpha": 1.52, 
                                                                                       "useless_field": "useless_value"}),
                                                                    })
                                            })
    model_interface_configuration = Mapping({"name": "OnlineInterface", "config": model_interface_configuration})
    generator_configuration = Mapping({"pair_filter": Mapping({"name": "AcceptAllPairFilter"})})
    generator_configuration = Mapping({"name": "MentionPairSampleGenerator", "config": generator_configuration,})
    decoder_configuration = Mapping({"name": "HybridBestFirstMSTCoreferenceDecoder"})
    root_loss = 1.2
    update_mode = "ML"
    configuration = Mapping({"model_interface": model_interface_configuration,
                              "generator": generator_configuration,
                              "decoder": decoder_configuration,
                              "root_loss": root_loss, 
                              "update_mode": update_mode,
                             })
    resolver = HybridBestFirstMSTResolver.from_configuration(configuration)
    
    #(46,47) and (83,84) are pronouns
    scores = {((1, 9), (14, 42)): 36590400.0, #POS_CLASS
                ((14, 42), (27, 42)): 39666240.0,
                ((1, 9), (27, 42)): 35976960.0,
                ((27, 42), (46, 47)): 36605952.0,
                ((14, 42), (46, 47)): 37884672.0, #POS_CLASS
                ((1, 9), (46, 47)): 38308032.0, #POS_CLASS
                ((46, 47), (68, 75)): 28459872.0,
                ((27, 42), (68, 75)): 59241600.0, #POS_CLASS
                ((14, 42), (68, 75)): 38747520.0,
                ((1, 9), (68, 75)): 39170880.0,
                
                ((68, 75), (83, 84)): 28446912.0,
                ((46, 47), (83, 84)): 54359424.0, #POS_CLASS
                ((27, 42), (83, 84)): 39267072.0,
                ((14, 42), (83, 84)): 40545792.0, #POS_CLASS
                ((1, 9), (83, 84)): 40969152.0, #POS_CLASS
                ((83, 84), (114, 120)): 30435552.0,
                
                ((68, 75), (114, 120)): 31449600.0,
                ((46, 47), (114, 120)): 32945472.0,
                ((27, 42), (114, 120)): 41016960.0,
                ((14, 42), (114, 120)): 42174720.0,
                ((1, 9), (114, 120)): 42477120.0,
                
                (ne_extent, (1, 9)): 0.,
                (ne_extent, (14, 42)): 0.,
                (ne_extent, (27, 42)): 0.,
                (ne_extent, (46, 47)): 0.,
                (ne_extent, (68, 75)): 0.,
                (ne_extent, (83, 84)): 0.,
                (ne_extent, (114, 120)): 0.,
              }
    true_scores = {((1, 9), (14, 42)): 36590400.0, #POS_CLASS
                  ((14, 42), (46, 47)): 37884672.0, #POS_CLASS
                  ((1, 9), (46, 47)): 38308032.0, #POS_CLASS
                  ((27, 42), (68, 75)): 59241600.0, #POS_CLASS
                  ((46, 47), (83, 84)): 54359424.0, #POS_CLASS
                  ((14, 42), (83, 84)): 40545792.0, #POS_CLASS
                  ((1, 9), (83, 84)): 40969152.0, #POS_CLASS
                  (ne_extent, (1, 9)): 0.,
                  (ne_extent, (14, 42)): 0.,
                  (ne_extent, (27, 42)): 0.,
                  (ne_extent, (46, 47)): 0.,
                  (ne_extent, (68, 75)): 0.,
                  (ne_extent, (83, 84)): 0.,
                  (ne_extent, (114, 120)): 0.,
                  }
    cluster_scores = {((1, 9), (14, 42)): 36590400.0, #POS_CLASS
                      ((14, 42), (46, 47)): 37884672.0, #POS_CLASS 
                      ((1, 9), (46, 47)): 38308032.0, #POS_CLASS
                      ((27, 42), (68, 75)): 59241600.0, #POS_CLASS
                      ((46, 47), (83, 84)): 54359424.0, #POS_CLASS
                      ((14, 42), (83, 84)): 40545792.0, #POS_CLASS
                      ((1, 9), (83, 84)): 40969152.0, #POS_CLASS
                      }
    # (1, 9), (14, 42), (27, 42), (46, 47), (68, 75), (83, 84), (114, 120)
    #positive_edges = [((0,0), (1, 9)), ((0,0), (27, 42)), ((0,0), (114, 120)), ((1, 9), (46, 47)), ((46, 47), (83, 84))]
    #negative_edges = [((14, 42), (46, 47)), ((1, 9), (83, 84)), ((14, 42), (83, 84))]
    expected_result = [(ne_extent, (1, 9)), 
                       ((1, 9), (46, 47)), 
                       ((46, 47), (83, 84)),
                       ((1, 9), (14, 42)),
                       (ne_extent, (27, 42)), 
                       ((27, 42), (68, 75)),
                       (ne_extent, (114, 120)), 
                       ]
    
    return resolver, (test_document, scores, true_scores, cluster_scores), expected_result


def get_hybrid_best_first_mst_resolver_test__compute_predicted_edges_from_edges_loss_scores_data():
    ne_extent = NULL_MENTION.extent
    
    # update_mode = "PB"
    resolver, (test_document, scores, _, _), expected_result = get_hybrid_best_first_mst_resolver_test__compute_true_edges_data() # (, true_scores, cluster_scores)
    #true_mst_edges = expected_result
    # (1, 9), (14, 42), (27, 42), (46, 47), (68, 75), (83, 84), (114, 120)
    #positive_edges = [((1, 9), (46, 47)), ((46, 47), (83, 84))]
    #negative_edges = [((14, 42), (46, 47)), ((27, 42), (46, 47)), ((68, 75), (83, 84)), ((46, 47), (83, 84)), ((27, 42), (83, 84)), ((14, 42), (83, 84)), ((1, 9), (83, 84))]
    loss_scores = {((0, 0), (14, 42)): 0.0, ((83, 84), (114, 120)): 30435552.0, 
                   ((1, 9), (83, 84)): 40969152.0, ((14, 42), (46, 47)): 37884672.0, 
                   ((0, 0), (68, 75)): 0.0, ((0, 0), (1, 9)): 0.0, ((1, 9), (68, 75)): 39170880.0, 
                   ((1, 9), (114, 120)): 42477120.0, ((14, 42), (27, 42)): 39666240.0, 
                   ((0, 0), (114, 120)): 0.0, ((0, 0), (83, 84)): 0.0, ((27, 42), (83, 84)): 39267072.0, 
                   ((1, 9), (14, 42)): 36590400.0, ((1, 9), (27, 42)): 35976960.0, 
                   ((1, 9), (46, 47)): 38308032.0, ((27, 42), (68, 75)): 59241600.0, 
                   ((68, 75), (114, 120)): 31449600.0, ((0, 0), (46, 47)): 0.0, 
                   ((46, 47), (68, 75)): 28459872.0, ((14, 42), (68, 75)): 38747520.0, 
                   ((68, 75), (83, 84)): 28446912.0, ((0, 0), (27, 42)): 0.0, 
                   ((46, 47), (83, 84)): 54359424.0, ((14, 42), (83, 84)): 40545792.0, 
                   ((27, 42), (114, 120)): 41016960.0, ((14, 42), (114, 120)): 42174720.0, 
                   ((46, 47), (114, 120)): 32945472.0, ((27, 42), (46, 47)): 36605952.0}

    expected_result = [(ne_extent, (114, 120)), ((1, 9), (46, 47)), ((1, 9), (114, 120)), 
                       ((14, 42), (114, 120)), ((27, 42), (68, 75)), ((27, 42), (114, 120)), 
                       ((46, 47), (83, 84))]
    return resolver, (test_document, loss_scores), expected_result




def get_constrained_mst_resolver_test___init___data():
    attribute_name_expected_result_pairs = []
    
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "mention_hierarchy": Mapping({"name": "MentionGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "with_singleton_features": False, 
                                                                                                    "with_anaphoricity_features": False, 
                                                                                                    "quantize": True, 
                                                                                                    "strict": False, 
                                                                                                    "useless_field": "useless_value"})
                                                                                 }), 
                                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                    "config": Mapping({"iteration_nb": 2, 
                                                                                       "avg": True, 
                                                                                       "warm_start": False, 
                                                                                       "alpha": 1.52, 
                                                                                       "useless_field": "useless_value"}),
                                                                    })
                                            })
    model_interface = create_model_interface_from_factory_configuration(Mapping((("name", "OnlineInterface"), ("config", model_interface_configuration))))
    attribute_name_expected_result_pairs.append(("_model_interface",model_interface))
    
    generator_configuration = Mapping([("pair_filter", Mapping([("name", "PairFiltersCombination"), 
                                                               ("config", Mapping([("filters", [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                       "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                       }),
                                                                                                                   ]),
                                                                                   ("witness_field6", "witness_value6"),
                                                                                   ])
                                                                ),
                                                                ])
                                      ),
                                     ("witness_field7", "witness_value7"),
                                     ])
    generator = MentionPairSampleGenerator.from_configuration(generator_configuration)
    attribute_name_expected_result_pairs.append(("_generator",generator))
    
    decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    decoder = MSTKruskalCoreferenceDecoder.from_configuration(decoder_configuration)
    attribute_name_expected_result_pairs.append(("_decoder",decoder))
    
    root_loss = 1.2
    attribute_name_expected_result_pairs.append(("root_loss",root_loss))
    
    update_mode = "ML"
    attribute_name_expected_result_pairs.append(("update_mode",update_mode))
    
    constraints_definition = (("rules_english_sieves", tuple(), {}),
                              ("anaphoricity", tuple(), {}),
                              ("proper_match", tuple(), {}),
                              ("animated", tuple(), {}),
                              ("embedding", tuple(), {}),
                              )
    attribute_name_expected_result_pairs.append(("constraints_definition",constraints_definition))
    
    configuration = Mapping({"witness_field9": "witness_value9"})
    
    expected_model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "mention_hierarchy": Mapping({"name": "MentionGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "with_singleton_features": False, 
                                                                                                    "with_anaphoricity_features": False, 
                                                                                                    "quantize": True, 
                                                                                                    "strict": False, 
                                                                                                    "useless_field": "useless_value"})
                                                                                        }), 
                                                     "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                            "config": Mapping({"iteration_nb": 2, 
                                                                                               "avg": True, 
                                                                                               "warm_start": False, 
                                                                                               "alpha": 1.52, 
                                                                                               "useless_field": "useless_value"}),
                                                                            })
                                                      })
    expected_model_interface_configuration = Mapping({"name": "OnlineInterface", "config": expected_model_interface_configuration})
    expected_generator_configuration = Mapping([("pair_filter", Mapping([("name", "PairFiltersCombination"), 
                                                                           ("config", Mapping([("filters", [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                                   "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                                   }),
                                                                                                                               ]),
                                                                                               ("witness_field6", "witness_value6"),
                                                                                               ])
                                                                            ),
                                                                            ])
                                                  ),
                                                 ("witness_field7", "witness_value7"),
                                                 ])
    expected_generator_configuration = Mapping({"name": "MentionPairSampleGenerator", "config": expected_generator_configuration})
    expected_decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    expected_decoder_configuration = Mapping({"name": "MSTKruskalCoreferenceDecoder", "config": expected_decoder_configuration})
    expected_configuration = Mapping({"model_interface": expected_model_interface_configuration,
                                      "generator": expected_generator_configuration,
                                      "decoder": expected_decoder_configuration,
                                      "root_loss": root_loss, 
                                      "update_mode": update_mode, 
                                      "constraints_definition": constraints_definition, 
                                      "witness_field9": "witness_value9",
                                     })
    
    attribute_name_expected_result_pairs.append(("configuration", expected_configuration))
    
    args = (model_interface, generator, decoder, root_loss, update_mode, constraints_definition)
    kwargs = {"configuration": configuration}
    
    return (args, kwargs), attribute_name_expected_result_pairs

def get_constrained_mst_resolver_test__inner_eq_data():
    data = []
    
    (args, kwargs), _ = get_constrained_mst_resolver_test___init___data() #attribute_name_expected_result_pairs
    
    resolver1 = ConstrainedMSTResolver(*args, **kwargs)
    resolver2 = ConstrainedMSTResolver(*args, **kwargs)
    expected_result = (True, None)
    data.append((resolver1, resolver2, expected_result))
    
    update_mode = "PB"
    args3 = args[:-2] + (update_mode,) + args[-1:]
    resolver3 = ConstrainedMSTResolver(*args3, **kwargs)
    expected_result = (False, "update_mode")
    data.append((resolver1, resolver3, expected_result))
    
    root_loss = 1.9
    args4 = args[:-3] + (root_loss,) + args[-2:]
    resolver4 = ConstrainedMSTResolver(*args4, **kwargs)
    expected_result = (False, "root_loss")
    data.append((resolver1, resolver4, expected_result))
    
    constraints_definition = (("rules_english_sieves", tuple(), {}),)
    args5 = args[:-1] + (constraints_definition,)
    resolver5 = ConstrainedMSTResolver(*args5, **kwargs)
    expected_result = (False, "constraints_definition")
    data.append((resolver1, resolver5, expected_result))
    
    return data

def get_constrained_mst_resolver_test_from_configuration_data():
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "ExtendedMentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy"}), 
                                                                                                    "mention_hierarchy": Mapping({"name": "MentionGramTypeHierarchy"}), 
                                                                                                    "with_singleton_features": False, 
                                                                                                    "with_anaphoricity_features": False, 
                                                                                                    "quantize": True, 
                                                                                                    "useless_field": "useless_value"})
                                                                                 }), 
                                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                    "config": Mapping({"iteration_nb": 2, 
                                                                                       "avg": True, 
                                                                                       "warm_start": False, 
                                                                                       "alpha": 1.52, 
                                                                                       "useless_field": "useless_value"}),
                                                                    })
                                            })
    model_interface = create_model_interface_from_factory_configuration(Mapping((("name", "OnlineInterface"), ("config", model_interface_configuration))))
    
    generator_configuration = Mapping([("pair_filter", Mapping([("name", "PairFiltersCombination"), 
                                                               ("config", Mapping([("filters", [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                       "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                       }),
                                                                                                                   ]),
                                                                                   ("witness_field6", "witness_value6"),
                                                                                   ])
                                                                ),
                                                                ])
                                      ),
                                     ("witness_field7", "witness_value7"),
                                     ])
    generator = MentionPairSampleGenerator.from_configuration(generator_configuration)
    
    decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    decoder = MSTKruskalCoreferenceDecoder.from_configuration(decoder_configuration)
    
    root_loss = 1.2
    
    update_mode = "PB"
    
    constraints_definition = (("rules_english_sieves", tuple(), {}),
                              ("anaphoricity", tuple(), {}),
                              ("proper_match", tuple(), {}),
                              ("animated", tuple(), {}),
                              ("embedding", tuple(), {}),
                              )
    
    configuration = Mapping({"witness_field9": "witness_value9"})
    
    args = (model_interface, generator, decoder, root_loss, update_mode, constraints_definition)
    kwargs = {"configuration": configuration}
    expected_result = ConstrainedMSTResolver(*args, **kwargs)
    
    configuration = Mapping({"model_interface": Mapping((("name", "OnlineInterface"), ("config", model_interface_configuration))),
                              "generator": Mapping((("name", "MentionPairSampleGenerator"), ("config", generator_configuration))),
                              "decoder": Mapping((("name", "MSTKruskalCoreferenceDecoder"), ("config", decoder_configuration))),
                              "root_loss": root_loss, 
                              "update_mode": update_mode, 
                              "constraints_definition": constraints_definition, 
                              "witness_field9": "witness_value9",
                             })
    
    return configuration, expected_result


def get_constrained_mst_resolver_test__compute_true_edges_data():
    ne_extent = NULL_MENTION.extent
    
    test_document = get_memoized_test_document_data()
    configuration, _ = get_constrained_mst_resolver_test_from_configuration_data() # expected_results
    resolver = ConstrainedMSTResolver.from_configuration(configuration)
    
    scores = {((1, 9), (14, 42)): 36590400.0, #POS_CLASS
                ((14, 42), (27, 42)): 39666240.0,
                ((1, 9), (27, 42)): 35976960.0,
                ((27, 42), (46, 47)): 36605952.0,
                ((14, 42), (46, 47)): 37884672.0, #POS_CLASS
                ((1, 9), (46, 47)): 38308032.0, #POS_CLASS
                ((46, 47), (68, 75)): 28459872.0,
                ((27, 42), (68, 75)): 59241600.0, #POS_CLASS
                ((14, 42), (68, 75)): 38747520.0,
                ((1, 9), (68, 75)): 39170880.0,
                
                ((68, 75), (83, 84)): 28446912.0,
                ((46, 47), (83, 84)): 54359424.0, #POS_CLASS
                ((27, 42), (83, 84)): 39267072.0,
                ((14, 42), (83, 84)): 40545792.0, #POS_CLASS
                ((1, 9), (83, 84)): 40969152.0, #POS_CLASS
                ((83, 84), (114, 120)): 30435552.0,
                
                ((68, 75), (114, 120)): 31449600.0,
                ((46, 47), (114, 120)): 32945472.0,
                ((27, 42), (114, 120)): 41016960.0,
                ((14, 42), (114, 120)): 42174720.0,
                ((1, 9), (114, 120)): 42477120.0,
                
                (ne_extent, (1, 9)): 0.,
                (ne_extent, (14, 42)): 0.,
                (ne_extent, (27, 42)): 0.,
                (ne_extent, (46, 47)): 0.,
                (ne_extent, (68, 75)): 0.,
                (ne_extent, (83, 84)): 0.,
                (ne_extent, (114, 120)): 0.,
              }
    true_scores = {((1, 9), (14, 42)): 36590400.0, #POS_CLASS
                    ((14, 42), (46, 47)): 37884672.0, #POS_CLASS
                    ((1, 9), (46, 47)): 38308032.0, #POS_CLASS
                    ((27, 42), (68, 75)): 59241600.0, #POS_CLASS
                    ((46, 47), (83, 84)): 54359424.0, #POS_CLASS
                    ((14, 42), (83, 84)): 40545792.0, #POS_CLASS
                    ((1, 9), (83, 84)): 40969152.0, #POS_CLASS

                    (ne_extent, (1, 9)): 0.,
                    (ne_extent, (14, 42)): 0.,
                    (ne_extent, (27, 42)): 0.,
                    (ne_extent, (46, 47)): 0.,
                    (ne_extent, (68, 75)): 0.,
                    (ne_extent, (83, 84)): 0.,
                    (ne_extent, (114, 120)): 0.,
                  }
    cluster_scores = {((1, 9), (14, 42)): 36590400.0, #POS_CLASS
                        ((14, 42), (46, 47)): 37884672.0, #POS_CLASS
                        ((1, 9), (46, 47)): 38308032.0, #POS_CLASS
                        ((27, 42), (68, 75)): 59241600.0, #POS_CLASS
                        ((46, 47), (83, 84)): 54359424.0, #POS_CLASS
                        ((14, 42), (83, 84)): 40545792.0, #POS_CLASS
                        ((1, 9), (83, 84)): 40969152.0, #POS_CLASS
                      }
    expected_result = [((27, 42), (68,75)), 
                       ((46, 47), (83, 84)), 
                       ((1, 9), (83, 84)), 
                       ((14, 42), (83, 84)), 
                       (ne_extent, (1, 9)), 
                       (ne_extent, (27, 42)), 
                       (ne_extent, (114, 120)), 
                       ]
    
    return resolver, (test_document, scores, true_scores, cluster_scores), expected_result


def get_constrained_mst_resolver_test__compute_predicted_edges_from_edges_loss_scores_data():
    ne_extent = NULL_MENTION.extent
    
    # no constraints
    # FIXME: use constraints?
    test_document = get_memoized_test_document_data()
    configuration, _ = get_constrained_mst_resolver_test_from_configuration_data() # expected_results
    configuration.constraints_definition = tuple()
    resolver = ConstrainedMSTResolver.from_configuration(configuration)
    loss_scores = {((0, 0), (14, 42)): 0.0, ((83, 84), (114, 120)): 30435552.0, 
                   ((1, 9), (83, 84)): 40969152.0, ((14, 42), (46, 47)): 37884672.0, 
                   ((0, 0), (68, 75)): 0.0, ((0, 0), (1, 9)): 0.0, 
                   ((1, 9), (68, 75)): 39170880.0, ((1, 9), (114, 120)): 42477120.0, 
                   ((14, 42), (27, 42)): 39666240.0, ((0, 0), (114, 120)): 0.0, 
                   ((0, 0), (83, 84)): 0.0, ((27, 42), (83, 84)): 39267072.0, 
                   ((1, 9), (14, 42)): 36590400.0, ((1, 9), (27, 42)): 35976960.0, 
                   ((1, 9), (46, 47)): 38308032.0, ((27, 42), (68, 75)): 59241600.0, 
                   ((68, 75), (114, 120)): 31449600.0, ((0, 0), (46, 47)): 0.0, 
                   ((46, 47), (68, 75)): 28459872.0, ((14, 42), (68, 75)): 38747520.0, 
                   ((68, 75), (83, 84)): 28446912.0, ((0, 0), (27, 42)): 0.0, 
                   ((46, 47), (83, 84)): 54359424.0, ((14, 42), (83, 84)): 40545792.0, 
                   ((27, 42), (114, 120)): 41016960.0, ((14, 42), (114, 120)): 42174720.0, 
                   ((46, 47), (114, 120)): 32945472.0, ((27, 42), (46, 47)): 36605952.0}
    expected_result = [((1, 9), (114, 120)), 
                       ((14, 42), (114, 120)), 
                       ((27, 42), (114, 120)), 
                       ((27, 42), (68, 75)), 
                       ((1, 9), (83, 84)), 
                       ((46, 47), (83, 84)), 
                       (ne_extent, (114, 120))]
    
    return resolver, (test_document, loss_scores), expected_result

def get_constrained_mst_resolver_test__pipeline_resolve_data():
    data = []
    
    test_document = get_memoized_test_document_data()
    entity_heads = None
    singletons = None
    configuration, _ = get_constrained_mst_resolver_test_from_configuration_data() # expected_results
    resolver = ConstrainedMSTResolver.from_configuration(configuration)
    
    model_folder_path = os.path.join(os.path.dirname(__file__), "data", "mst", "train_test_online_model_interface_perceptron_model")
    resolver._model_interface.load_model(model_folder_path)
    
    kwargs = {"entity_head_extents": entity_heads, "singleton_extents": singletons}
    
    pos_coref_pairs = (((1,9), (14,42)),
                       ((1,9), (27,42)),
                       ((27,42), (68,75)),
                       ((27,42), (46,47)),
                       ((46,47), (83,84)),
                       #((83,84), (114,120)),
                       )
    #excluded_pairs = ((46,47), (83,84))
    excluded_pairs = tuple()
    expected_result = CoreferencePartition(mention_extents=tuple(m.extent for m in test_document.mentions if m.extent not in excluded_pairs))
    for left_extent, right_extent in pos_coref_pairs:
        left_mention = test_document.extent2mention[left_extent]
        right_mention = test_document.extent2mention[right_extent]
        expected_result.join(left_mention.extent, right_mention.extent)
    
    data.append((test_document, resolver, kwargs, expected_result))
    
    return data

def get_constrained_mst_resolver_test_resolve_data():
    data0 = get_constrained_mst_resolver_test__pipeline_resolve_data()
    data = []
    for test_document, resolver, kwargs, expected_result in data0:
        kwargs = {}
        data.append((test_document, resolver, kwargs, expected_result))
    return data

def get_best_first_document_structured_resolver_test__compute_true_edges_data():
    ne_extent = NULL_MENTION.extent
    
    test_document = get_memoized_test_document_data()
    entity_heads = None
    singletons = None
    with_singleton_features = False
    with_anaphoricity_features = False
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "mention_hierarchy": Mapping({"name": "MentionGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "with_singleton_features": with_singleton_features, 
                                                                                                    "with_anaphoricity_features": with_anaphoricity_features, 
                                                                                                    "quantize": True, 
                                                                                                    "useless_field": "useless_value"})
                                                                                 }), 
                                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                    "config": Mapping({"iteration_nb": 2, 
                                                                                       "avg": True, 
                                                                                       "warm_start": False, 
                                                                                       "alpha": 1.52, 
                                                                                       "useless_field": "useless_value"}),
                                                                    })
                                            })
    model_interface_configuration = Mapping({"name": "OnlineInterface", "config": model_interface_configuration})
    generator_configuration = Mapping({"pair_filter": Mapping({"name": "AcceptAllPairFilter"})})
    generator_configuration = Mapping({"name": "MentionPairSampleGenerator", "config": generator_configuration,})
    decoder_configuration = Mapping({"name": "BestFirstCoreferenceDecoder"})
    root_loss = 1.2
    update_mode = "ML"
    configuration = Mapping({"model_interface": model_interface_configuration,
                              "generator": generator_configuration,
                              "decoder": decoder_configuration,
                              "root_loss": root_loss, 
                              "update_mode": update_mode,
                             })
    resolver = BestFirstDocumentStructuredResolver.from_configuration(configuration)
    
    scores = {((1, 9), (14, 42)): 36590400.0, #POS_CLASS
                ((14, 42), (27, 42)): 39666240.0,
                ((1, 9), (27, 42)): 35976960.0,
                ((27, 42), (46, 47)): 36605952.0,
                ((14, 42), (46, 47)): 37884672.0, #POS_CLASS
                ((1, 9), (46, 47)): 38308032.0, #POS_CLASS
                ((46, 47), (68, 75)): 28459872.0,
                ((27, 42), (68, 75)): 59241600.0, #POS_CLASS
                ((14, 42), (68, 75)): 38747520.0,
                ((1, 9), (68, 75)): 39170880.0,
                
                ((68, 75), (83, 84)): 28446912.0,
                ((46, 47), (83, 84)): 54359424.0, #POS_CLASS
                ((27, 42), (83, 84)): 39267072.0,
                ((14, 42), (83, 84)): 40545792.0, #POS_CLASS
                ((1, 9), (83, 84)): 40969152.0, #POS_CLASS
                ((83, 84), (114, 120)): 30435552.0,
                
                ((68, 75), (114, 120)): 31449600.0,
                ((46, 47), (114, 120)): 32945472.0,
                ((27, 42), (114, 120)): 41016960.0,
                ((14, 42), (114, 120)): 42174720.0,
                ((1, 9), (114, 120)): 42477120.0,
                
                (ne_extent, (1, 9)): 0.,
                (ne_extent, (14, 42)): 0.,
                (ne_extent, (27, 42)): 0.,
                (ne_extent, (46, 47)): 0.,
                (ne_extent, (68, 75)): 0.,
                (ne_extent, (83, 84)): 0.,
                (ne_extent, (114, 120)): 0.,
              }
    true_scores = {((1, 9), (14, 42)): 36590400.0, #POS_CLASS
                  ((14, 42), (46, 47)): 37884672.0, #POS_CLASS
                  ((1, 9), (46, 47)): 38308032.0, #POS_CLASS
                  ((27, 42), (68, 75)): 59241600.0, #POS_CLASS
                  ((46, 47), (83, 84)): 54359424.0, #POS_CLASS
                  ((14, 42), (83, 84)): 40545792.0, #POS_CLASS
                  ((1, 9), (83, 84)): 40969152.0, #POS_CLASS
                  (ne_extent, (1, 9)): 0.,
                  (ne_extent, (14, 42)): 0.,
                  (ne_extent, (27, 42)): 0.,
                  (ne_extent, (46, 47)): 0.,
                  (ne_extent, (68, 75)): 0.,
                  (ne_extent, (83, 84)): 0.,
                  (ne_extent, (114, 120)): 0.,
                  }
    true_coreference_edges_scores = {((1, 9), (14, 42)): 36590400.0, #POS_CLASS
                                      ((14, 42), (46, 47)): 37884672.0, #POS_CLASS 
                                      ((1, 9), (46, 47)): 38308032.0, #POS_CLASS
                                      ((27, 42), (68, 75)): 59241600.0, #POS_CLASS
                                      ((46, 47), (83, 84)): 54359424.0, #POS_CLASS
                                      ((14, 42), (83, 84)): 40545792.0, #POS_CLASS
                                      ((1, 9), (83, 84)): 40969152.0, #POS_CLASS
                                      }
    
    # (1, 9), (14, 42), (27, 42), (46, 47), (68, 75), (83, 84), (114, 120)
    # (46,47) and (83,84) are pronouns
    # Document's coreference partition
    # 1,9 14,42 46,47 83,84
    # 27,42 68,75
    # 114,120
    expected_result = [(ne_extent, (1, 9)), 
                       ((1, 9), (14, 42)),
                       (ne_extent, (27, 42)), 
                       ((1, 9), (46, 47)), 
                       ((27, 42), (68, 75)),
                       ((46, 47), (83, 84)),
                       (ne_extent, (114, 120)), 
                       ]
    
    return resolver, (test_document, scores, true_scores, true_coreference_edges_scores), expected_result


def get_best_first_document_structured_resolver_test__compute_predicted_edges_from_edges_loss_scores_data():
    ne_extent = NULL_MENTION.extent
    
    test_document = get_memoized_test_document_data()
    entity_heads = None
    singletons = None
    with_singleton_features = False
    with_anaphoricity_features = False
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "mention_hierarchy": Mapping({"name": "MentionGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "with_singleton_features": with_singleton_features, 
                                                                                                    "with_anaphoricity_features": with_anaphoricity_features, 
                                                                                                    "quantize": True, 
                                                                                                    "useless_field": "useless_value"})
                                                                                 }), 
                                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                    "config": Mapping({"iteration_nb": 2, 
                                                                                       "avg": True, 
                                                                                       "warm_start": False, 
                                                                                       "alpha": 1.52, 
                                                                                       "useless_field": "useless_value"}),
                                                                    })
                                            })
    model_interface_configuration = Mapping({"name": "OnlineInterface", "config": model_interface_configuration})
    generator_configuration = Mapping({"pair_filter": Mapping({"name": "AcceptAllPairFilter"})})
    generator_configuration = Mapping({"name": "MentionPairSampleGenerator", "config": generator_configuration,})
    decoder_configuration = Mapping({"name": "BestFirstCoreferenceDecoder"})
    root_loss = 1.2
    update_mode = "ML"
    configuration = Mapping({"model_interface": model_interface_configuration,
                              "generator": generator_configuration,
                              "decoder": decoder_configuration,
                              "root_loss": root_loss, 
                              "update_mode": update_mode,
                             })
    resolver = BestFirstDocumentStructuredResolver.from_configuration(configuration)
    
    # (1, 9), (14, 42), (27, 42), (46, 47), (68, 75), (83, 84), (114, 120)
    loss_scores = {(ne_extent, (1, 9)): 0.0, 
                   
                   (ne_extent, (14, 42)): 0.0, 
                   ((1, 9), (14, 42)): 36590400.0, 
                   
                   (ne_extent, (27, 42)): 0.0, 
                   ((1, 9), (27, 42)):      35976960.0, 
                   ((14, 42), (27, 42)):    39666240.0, 
                   
                   (ne_extent, (46, 47)): 0.0, 
                   ((1, 9), (46, 47)):      38308032.0, 
                   ((14, 42), (46, 47)):    37884672.0, 
                   ((27, 42), (46, 47)):    36605952.0, 
                   
                   (ne_extent, (68, 75)): 0.0, 
                   ((1, 9), (68, 75)):      39170880.0, 
                   ((14, 42), (68, 75)):    38747520.0, 
                   ((27, 42), (68, 75)):    59241600.0, 
                   ((46, 47), (68, 75)):    28459872.0, 
                   
                   (ne_extent, (83, 84)): 0.0, 
                   ((1, 9), (83, 84)):      40969152.0, 
                   ((14, 42), (83, 84)):    40545792.0, 
                   ((27, 42), (83, 84)):    39267072.0, 
                   ((46, 47), (83, 84)):    54359424.0, 
                   ((68, 75), (83, 84)):    28446912.0, 
                   
                   (ne_extent, (114, 120)): 0.0, 
                   ((1, 9), (114, 120)):    42477120.0, 
                   ((14, 42), (114, 120)):  42174720.0, 
                   ((27, 42), (114, 120)):  41016960.0, 
                   ((46, 47), (114, 120)):  32945472.0, 
                   ((68, 75), (114, 120)):  31449600.0, 
                   ((83, 84), (114, 120)):  30435552.0, 
                   }

    expected_result = [(ne_extent, (1, 9)), 
                       ((1, 9), (14, 42)),
                       ((14, 42), (27, 42)),
                       ((1, 9), (46, 47)), 
                       ((27, 42), (68, 75)), 
                       ((46, 47), (83, 84)),
                       ((1, 9), (114, 120)), 
                       ]
    return resolver, (test_document, loss_scores), expected_result



def get_extended_best_first_document_structured_resolver_test__compute_predicted_edges_from_edges_loss_scores_data():
    ne_extent = NULL_MENTION.extent
    
    test_document = get_memoized_test_document_data()
    entity_heads = None
    singletons = None
    with_singleton_features = False
    with_anaphoricity_features = False
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "ExtendedMentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "mention_hierarchy": Mapping({"name": "MentionGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "with_singleton_features": with_singleton_features, 
                                                                                                    "with_anaphoricity_features": with_anaphoricity_features, 
                                                                                                    "quantize": True, 
                                                                                                    "useless_field": "useless_value"})
                                                                                 }), 
                                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                    "config": Mapping({"iteration_nb": 2, 
                                                                                       "avg": True, 
                                                                                       "warm_start": False, 
                                                                                       "alpha": 1.52, 
                                                                                       "useless_field": "useless_value"}),
                                                                    })
                                            })
    model_interface_configuration = Mapping({"name": "OnlineInterface", "config": model_interface_configuration})
    generator_configuration = Mapping({"pair_filter": Mapping({"name": "AcceptAllPairFilter"})})
    generator_configuration = Mapping({"name": "ExtendedMentionPairSampleGenerator", "config": generator_configuration,})
    decoder_configuration = Mapping({"name": "ExtendedBestFirstCoreferenceDecoder"})
    root_loss = 1.2
    update_mode = "ML"
    configuration = Mapping({"model_interface": model_interface_configuration,
                              "generator": generator_configuration,
                              "decoder": decoder_configuration,
                              "root_loss": root_loss, 
                              "update_mode": update_mode,
                             })
    resolver = ExtendedBestFirstDocumentStructuredResolver.from_configuration(configuration)
    
    # (1, 9), (14, 42), (27, 42), (46, 47), (68, 75), (83, 84), (114, 120)
    # (46,47) and (83,84) are pronouns
    loss_scores = {(ne_extent, (1, 9)): 10.0, 
                   
                   (ne_extent, (14, 42)): 10.0, 
                   ((1, 9), (14, 42)): 36590400.0, 
                   
                   (ne_extent, (27, 42)): 0.0, 
                   ((1, 9), (27, 42)):      35976960.0, 
                   ((14, 42), (27, 42)):    39666240.0, 
                   
                   (ne_extent, (46, 47)): 10.0, 
                   ((1, 9), (46, 47)):      38308032.0, 
                   ((14, 42), (46, 47)):    37884672.0, 
                   ((27, 42), (46, 47)):    36605952.0, 
                   
                   (ne_extent, (68, 75)): 10.0, 
                   ((1, 9), (68, 75)):      39170880.0, 
                   ((14, 42), (68, 75)):    38747520.0, 
                   ((27, 42), (68, 75)):    59241600.0, 
                   ((46, 47), (68, 75)):    28459872.0, 
                   
                   (ne_extent, (83, 84)): 10.0, 
                   ((1, 9), (83, 84)):      40969152.0, 
                   ((14, 42), (83, 84)):    40545792.0, 
                   ((27, 42), (83, 84)):    39267072.0, 
                   ((46, 47), (83, 84)):    54359424.0, 
                   ((68, 75), (83, 84)):    28446912.0, 
                   
                   (ne_extent, (114, 120)): 10.0, 
                   ((1, 9), (114, 120)):    42477120.0, 
                   ((14, 42), (114, 120)):  42174720.0, 
                   ((27, 42), (114, 120)):  41016960.0, 
                   ((46, 47), (114, 120)):  32945472.0, 
                   ((68, 75), (114, 120)):  31449600.0, 
                   ((83, 84), (114, 120)):  30435552.0, 
                   }

    expected_result = [(ne_extent, (1, 9)), 
                       ((1, 9), (14, 42)), 
                       ((14, 42), (27, 42)), 
                       ((1, 9), (46, 47)), 
                       ((27, 42), (68, 75)), 
                       ((46, 47), (83, 84)), 
                       ((1, 9), (114, 120)),
                       ]
    return resolver, (test_document, loss_scores), expected_result



def get_constrained_best_first_document_structured_resolver_test___init___data():
    attribute_name_expected_result_pairs = []
    
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "mention_hierarchy": Mapping({"name": "MentionGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "with_singleton_features": False, 
                                                                                                    "with_anaphoricity_features": False, 
                                                                                                    "quantize": True, 
                                                                                                    "strict": False, 
                                                                                                    "useless_field": "useless_value"})
                                                                                 }), 
                                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                    "config": Mapping({"iteration_nb": 2, 
                                                                                       "avg": True, 
                                                                                       "warm_start": False, 
                                                                                       "alpha": 1.52, 
                                                                                       "useless_field": "useless_value"}),
                                                                    })
                                            })
    model_interface = create_model_interface_from_factory_configuration(Mapping((("name", "OnlineInterface"), ("config", model_interface_configuration))))
    attribute_name_expected_result_pairs.append(("_model_interface",model_interface))
    
    generator_configuration = Mapping([("pair_filter", Mapping([("name", "PairFiltersCombination"), 
                                                               ("config", Mapping([("filters", [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                       "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                       }),
                                                                                                   ]),
                                                                                   ("witness_field6", "witness_value6"),
                                                                                   ])
                                                                ),
                                                                ])
                                      ),
                                     ("witness_field7", "witness_value7"),
                                     ])
    generator = MentionPairSampleGenerator.from_configuration(generator_configuration)
    attribute_name_expected_result_pairs.append(("_generator",generator))
    
    decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    decoder = ConstrainedBestFirstCoreferenceDecoder.from_configuration(decoder_configuration)
    attribute_name_expected_result_pairs.append(("_decoder",decoder))
    
    root_loss = 1.2
    attribute_name_expected_result_pairs.append(("root_loss",root_loss))
    
    update_mode = "ML"
    attribute_name_expected_result_pairs.append(("update_mode",update_mode))
    
    constraints_definition = (("rules_english_sieves", tuple(), {}),
                              ("anaphoricity", tuple(), {}),
                              ("proper_match", tuple(), {}),
                              ("animated", tuple(), {}),
                              ("embedding", tuple(), {}),
                              )
    attribute_name_expected_result_pairs.append(("constraints_definition",constraints_definition))
    
    configuration = Mapping({"witness_field9": "witness_value9"})
    
    expected_model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "mention_hierarchy": Mapping({"name": "MentionGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "with_singleton_features": False, 
                                                                                                    "with_anaphoricity_features": False, 
                                                                                                    "quantize": True, 
                                                                                                    "strict": False, 
                                                                                                    "useless_field": "useless_value"})
                                                                                        }), 
                                                     "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                            "config": Mapping({"iteration_nb": 2, 
                                                                                               "avg": True, 
                                                                                               "warm_start": False, 
                                                                                               "alpha": 1.52, 
                                                                                               "useless_field": "useless_value"}),
                                                                            })
                                                      })
    expected_model_interface_configuration = Mapping({"name": "OnlineInterface", "config": expected_model_interface_configuration})
    expected_generator_configuration = Mapping([("pair_filter", Mapping([("name", "PairFiltersCombination"), 
                                                                           ("config", Mapping([("filters", [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                                   "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                                   }),
                                                                                                                               ]),
                                                                                               ("witness_field6", "witness_value6"),
                                                                                               ])
                                                                            ),
                                                                            ])
                                                  ),
                                                 ("witness_field7", "witness_value7"),
                                                 ])
    expected_generator_configuration = Mapping({"name": "MentionPairSampleGenerator", "config": expected_generator_configuration})
    expected_decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    expected_decoder_configuration = Mapping({"name": "ConstrainedBestFirstCoreferenceDecoder", "config": expected_decoder_configuration})
    expected_configuration = Mapping({"model_interface": expected_model_interface_configuration,
                                      "generator": expected_generator_configuration,
                                      "decoder": expected_decoder_configuration,
                                      "root_loss": root_loss, 
                                      "update_mode": update_mode, 
                                      "constraints_definition": constraints_definition, 
                                      "witness_field9": "witness_value9",
                                     })
    
    attribute_name_expected_result_pairs.append(("configuration", expected_configuration))
    
    args = (model_interface, generator, decoder, root_loss, update_mode, constraints_definition)
    kwargs = {"configuration": configuration}
    
    return (args, kwargs), attribute_name_expected_result_pairs

def get_constrained_best_first_document_structured_resolver_test__inner_eq_data():
    data = []
    
    (args, kwargs), _ = get_constrained_best_first_document_structured_resolver_test___init___data() #attribute_name_expected_result_pairs
    
    resolver1 = ConstrainedBestFirstDocumentStructuredResolver(*args, **kwargs)
    resolver2 = ConstrainedBestFirstDocumentStructuredResolver(*args, **kwargs)
    expected_result = (True, None)
    data.append((resolver1, resolver2, expected_result))
    
    update_mode = "PB"
    args3 = args[:-2] + (update_mode,) + args[-1:]
    resolver3 = ConstrainedBestFirstDocumentStructuredResolver(*args3, **kwargs)
    expected_result = (False, "update_mode")
    data.append((resolver1, resolver3, expected_result))
    
    root_loss = 1.9
    args4 = args[:-3] + (root_loss,) + args[-2:]
    resolver4 = ConstrainedBestFirstDocumentStructuredResolver(*args4, **kwargs)
    expected_result = (False, "root_loss")
    data.append((resolver1, resolver4, expected_result))
    
    constraints_definition = (("rules_english_sieves", tuple(), {}),)
    args5 = args[:-1] + (constraints_definition,)
    resolver5 = ConstrainedBestFirstDocumentStructuredResolver(*args5, **kwargs)
    expected_result = (False, "constraints_definition")
    data.append((resolver1, resolver5, expected_result))
    
    return data

def get_constrained_best_first_document_structured_resolver_test_from_configuration_data():
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy"}), 
                                                                                                    "mention_hierarchy": Mapping({"name": "MentionGramTypeHierarchy"}), 
                                                                                                    "with_singleton_features": False, 
                                                                                                    "with_anaphoricity_features": False, 
                                                                                                    "quantize": True, 
                                                                                                    "useless_field": "useless_value"})
                                                                                 }), 
                                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                    "config": Mapping({"iteration_nb": 2, 
                                                                                       "avg": True, 
                                                                                       "warm_start": False, 
                                                                                       "alpha": 1.52, 
                                                                                       "useless_field": "useless_value"}),
                                                                    })
                                            })
    model_interface = create_model_interface_from_factory_configuration(Mapping((("name", "OnlineInterface"), ("config", model_interface_configuration))))
    
    generator_configuration = Mapping([("pair_filter", Mapping([("name", "PairFiltersCombination"), 
                                                               ("config", Mapping([("filters", [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                       "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                       }),
                                                                                                                   ]),
                                                                                   ("witness_field6", "witness_value6"),
                                                                                   ])
                                                                ),
                                                                ])
                                      ),
                                     ("witness_field7", "witness_value7"),
                                     ])
    generator = MentionPairSampleGenerator.from_configuration(generator_configuration)
    
    decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    decoder = ConstrainedBestFirstCoreferenceDecoder.from_configuration(decoder_configuration)
    
    root_loss = 1.2
    
    update_mode = "PB"
    
    constraints_definition = (("rules_english_sieves", tuple(), {}),
                              ("anaphoricity", tuple(), {}),
                              ("proper_match", tuple(), {}),
                              ("animated", tuple(), {}),
                              ("embedding", tuple(), {}),
                              )
    
    configuration = Mapping({"witness_field9": "witness_value9"})
    
    args = (model_interface, generator, decoder, root_loss, update_mode, constraints_definition)
    kwargs = {"configuration": configuration}
    expected_result = ConstrainedBestFirstDocumentStructuredResolver(*args, **kwargs)
    
    configuration = Mapping({"model_interface": Mapping((("name", "OnlineInterface"), ("config", model_interface_configuration))),
                              "generator": Mapping((("name", "MentionPairSampleGenerator"), ("config", generator_configuration))),
                              "decoder": Mapping((("name", "ConstrainedBestFirstCoreferenceDecoder"), ("config", decoder_configuration))),
                              "root_loss": root_loss, 
                              "update_mode": update_mode, 
                              "constraints_definition": constraints_definition, 
                              "witness_field9": "witness_value9",
                             })
    
    return configuration, expected_result


def get_constrained_best_first_document_structured_resolver_test__compute_true_edges_data():
    ne_extent = NULL_MENTION.extent
    
    test_document = get_memoized_test_document_data()
    configuration, _ = get_constrained_best_first_document_structured_resolver_test_from_configuration_data() # expected_results
    resolver = ConstrainedBestFirstDocumentStructuredResolver.from_configuration(configuration)
    
    scores = {((1, 9), (14, 42)): 36590400.0, #POS_CLASS
                ((14, 42), (27, 42)): 39666240.0,
                ((1, 9), (27, 42)): 35976960.0,
                ((27, 42), (46, 47)): 36605952.0,
                ((14, 42), (46, 47)): 37884672.0, #POS_CLASS
                ((1, 9), (46, 47)): 38308032.0, #POS_CLASS
                ((46, 47), (68, 75)): 28459872.0,
                ((27, 42), (68, 75)): 59241600.0, #POS_CLASS
                ((14, 42), (68, 75)): 38747520.0,
                ((1, 9), (68, 75)): 39170880.0,
                
                ((68, 75), (83, 84)): 28446912.0,
                ((46, 47), (83, 84)): 54359424.0, #POS_CLASS
                ((27, 42), (83, 84)): 39267072.0,
                ((14, 42), (83, 84)): 40545792.0, #POS_CLASS
                ((1, 9), (83, 84)): 40969152.0, #POS_CLASS
                ((83, 84), (114, 120)): 30435552.0,
                
                ((68, 75), (114, 120)): 31449600.0,
                ((46, 47), (114, 120)): 32945472.0,
                ((27, 42), (114, 120)): 41016960.0,
                ((14, 42), (114, 120)): 42174720.0,
                ((1, 9), (114, 120)): 42477120.0,
                
                (ne_extent, (1, 9)): 0.,
                (ne_extent, (14, 42)): 0.,
                (ne_extent, (27, 42)): 0.,
                (ne_extent, (46, 47)): 0.,
                (ne_extent, (68, 75)): 0.,
                (ne_extent, (83, 84)): 0.,
                (ne_extent, (114, 120)): 0.,
              }
    # (1,9), (14, 42), (27, 42), (46, 47), (68, 75), (83, 84), (114, 120)
    # (46, 47) and (83, 84) are pronouns
    true_scores = {((1, 9), (14, 42)): 36590400.0, #POS_CLASS
                    ((14, 42), (46, 47)): 37884672.0, #POS_CLASS
                    ((1, 9), (46, 47)): 38308032.0, #POS_CLASS
                    ((27, 42), (68, 75)): 59241600.0, #POS_CLASS
                    ((46, 47), (83, 84)): 54359424.0, #POS_CLASS
                    ((14, 42), (83, 84)): 40545792.0, #POS_CLASS
                    ((1, 9), (83, 84)): 40969152.0, #POS_CLASS

                    (ne_extent, (1, 9)): 0.,
                    (ne_extent, (14, 42)): 0.,
                    (ne_extent, (27, 42)): 0.,
                    (ne_extent, (46, 47)): 0.,
                    (ne_extent, (68, 75)): 0.,
                    (ne_extent, (83, 84)): 0.,
                    (ne_extent, (114, 120)): 0.,
                  }
    cluster_scores = {((1, 9), (14, 42)): 36590400.0, #POS_CLASS
                        ((14, 42), (46, 47)): 37884672.0, #POS_CLASS
                        ((1, 9), (46, 47)): 38308032.0, #POS_CLASS
                        ((27, 42), (68, 75)): 59241600.0, #POS_CLASS
                        ((46, 47), (83, 84)): 54359424.0, #POS_CLASS
                        ((14, 42), (83, 84)): 40545792.0, #POS_CLASS
                        ((1, 9), (83, 84)): 40969152.0, #POS_CLASS
                      }
    expected_result = [(ne_extent, (1, 9)), 
                       ((1, 9), (14, 42)),
                       ((1, 9), (46, 47)), 
                       ((46, 47), (83, 84)), 
                       
                       (ne_extent, (27, 42)), 
                       ((27, 42), (68, 75)), 
                       
                       (ne_extent, (114, 120)),
                       ]
    
    return resolver, (test_document, scores, true_scores, cluster_scores), expected_result


def get_constrained_best_first_document_structured_resolver_test__compute_predicted_edges_from_edges_loss_scores_data():
    ne_extent = NULL_MENTION.extent
    
    # (1,9), (14, 42), (27, 42), (46, 47), (68, 75), (83, 84), (114, 120)
    # (46, 47) and (83, 84) are pronouns
    # positive_edges = []
    # negative_edges = [((14, 42), (27, 42)),]
    test_document = get_memoized_test_document_data()
    configuration, _ = get_constrained_best_first_document_structured_resolver_test_from_configuration_data() # expected_results
    resolver = ConstrainedBestFirstDocumentStructuredResolver.from_configuration(configuration)
    
    loss_scores = {(ne_extent, (1, 9)): 10.0, 
                   
                   (ne_extent, (14, 42)): 10.0, 
                   ((1, 9), (14, 42)): 36590400.0, 
                   
                   (ne_extent, (27, 42)): 10.0, 
                   ((1, 9), (27, 42)):      35976960.0, 
                   ((14, 42), (27, 42)):    39666240.0, 
                   
                   (ne_extent, (46, 47)): 10.0, 
                   ((1, 9), (46, 47)):      38308032.0, 
                   ((14, 42), (46, 47)):    37884672.0, 
                   ((27, 42), (46, 47)):    36605952.0, 
                   
                   (ne_extent, (68, 75)): 10.0, 
                   ((1, 9), (68, 75)):      39170880.0, 
                   ((14, 42), (68, 75)):    38747520.0, 
                   ((27, 42), (68, 75)):    59241600.0, 
                   ((46, 47), (68, 75)):    28459872.0, 
                   
                   (ne_extent, (83, 84)): 10.0, 
                   ((1, 9), (83, 84)):      40969152.0, 
                   ((14, 42), (83, 84)):    40545792.0, 
                   ((27, 42), (83, 84)):    39267072.0, 
                   ((46, 47), (83, 84)):    54359424.0, 
                   ((68, 75), (83, 84)):    28446912.0, 
                   
                   (ne_extent, (114, 120)): 10.0, 
                   ((1, 9), (114, 120)):    42477120.0, 
                   ((14, 42), (114, 120)):  42174720.0, 
                   ((27, 42), (114, 120)):  41016960.0, 
                   ((46, 47), (114, 120)):  32945472.0, 
                   ((68, 75), (114, 120)):  31449600.0, 
                   ((83, 84), (114, 120)):  30435552.0, 
                   }
    expected_result = [(ne_extent, (1, 9)), 
                       ((1, 9), (14, 42)), 
                       ((1, 9), (27, 42)), 
                       ((1, 9), (46, 47)), 
                       ((27, 42), (68, 75)), 
                       ((46, 47), (83, 84)), 
                       ((1, 9), (114, 120))]
    
    return resolver, (test_document, loss_scores), expected_result



def get_constrained_extended_best_first_document_structured_resolver_test__compute_predicted_edges_from_edges_loss_scores_data():
    ne_extent = NULL_MENTION.extent
    
    # (1,9), (14, 42), (27, 42), (46, 47), (68, 75), (83, 84), (114, 120)
    # (46, 47) and (83, 84) are pronouns
    # positive_edges = []
    # negative_edges = [((14, 42), (27, 42)),]
    test_document = get_memoized_test_document_data()
    configuration, _ = get_constrained_best_first_document_structured_resolver_test_from_configuration_data() # expected_results
    configuration.generator.name = "extendedmentionpair"
    configuration.decoder.name = "constrainedextendedbestfirst"
    resolver = ConstrainedExtendedBestFirstDocumentStructuredResolver.from_configuration(configuration)
    
    loss_scores = {(ne_extent, (1, 9)): 10.0, 
                   
                   (ne_extent, (14, 42)): 10.0, 
                   ((1, 9), (14, 42)): 36590400.0, 
                   
                   (ne_extent, (27, 42)): 10.0, 
                   ((1, 9), (27, 42)):      35976960.0, 
                   ((14, 42), (27, 42)):    39666240.0, 
                   
                   (ne_extent, (46, 47)): 10.0, 
                   ((1, 9), (46, 47)):      38308032.0, 
                   ((14, 42), (46, 47)):    37884672.0, 
                   ((27, 42), (46, 47)):    36605952.0, 
                   
                   (ne_extent, (68, 75)): 10.0, 
                   ((1, 9), (68, 75)):      39170880.0, 
                   ((14, 42), (68, 75)):    38747520.0, 
                   ((27, 42), (68, 75)):    59241600.0, 
                   ((46, 47), (68, 75)):    28459872.0, 
                   
                   (ne_extent, (83, 84)): 10.0, 
                   ((1, 9), (83, 84)):      40969152.0, 
                   ((14, 42), (83, 84)):    40545792.0, 
                   ((27, 42), (83, 84)):    39267072.0, 
                   ((46, 47), (83, 84)):    54359424.0, 
                   ((68, 75), (83, 84)):    28446912.0, 
                   
                   (ne_extent, (114, 120)): 10.0, 
                   ((1, 9), (114, 120)):    42477120.0, 
                   ((14, 42), (114, 120)):  42174720.0, 
                   ((27, 42), (114, 120)):  41016960.0, 
                   ((46, 47), (114, 120)):  32945472.0, 
                   ((68, 75), (114, 120)):  31449600.0, 
                   ((83, 84), (114, 120)):  30435552.0, 
                   }
    expected_result = [(ne_extent, (1, 9)), 
                       ((1, 9), (14, 42)), 
                       ((1, 9), (27, 42)), 
                       ((1, 9), (46, 47)), 
                       ((27, 42), (68, 75)), 
                       ((46, 47), (83, 84)), 
                       ((1, 9), (114, 120))]
    
    return resolver, (test_document, loss_scores), expected_result



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()