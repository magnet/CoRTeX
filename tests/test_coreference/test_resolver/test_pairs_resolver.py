# -*- coding: utf-8 -*-

import unittest
import os
from collections import OrderedDict
import numpy

from cortex.coreference.resolver.pairs import (SoonResolver, 
                                               McCarthyLehnertResolver, NgCardieResolver,
                                               HACResolver, OraclePairResolver, OraclePairScoreResolver)
from cortex.coreference.generator import create_generator_from_factory_configuration
from cortex.coreference.generator.pairs import MentionPairSampleGenerator, SoonGenerator
from cortex.learning.model_interface import create_model_interface_from_factory_configuration
from cortex.learning.ml_features.hierarchy.pairs import PairGramTypeHierarchy #OverlapPairGramTypeHierarchy
from cortex.learning.ml_features.hierarchy.mentions import MentionNoneHierarchy
from cortex.learning.sample_features_vectorizer.pairs import ExtendedMentionPairSampleFeaturesVectorizer
from cortex.learning.filter.pairs import RecallPairFilter
from cortex.coreference.decoder import create_decoder_from_factory_configuration
from cortex.coreference.decoder.pairs import (ClosestFirstCoreferenceDecoder, 
                                              OracleAggressiveMergeCoreferenceDecoder, 
                                              )
from cortex.learning.model_interface import SKLEARNInterface
from cortex.api.coreference_partition import CoreferencePartition
from cortex.tools import Mapping
from cortex.utils.io import TemporaryDirectory, unzip_archive_to_folder
from cortex.utils import are_array_equal
from cortex.utils.memoize import Memoized
from cortex.parameters import ENCODING


#@unittest.skip  
class TestSoonResolver(unittest.TestCase):
    
    #@unittest.skip  
    def test___init__(self):
        #Test parameters
        (args, kwargs), attribute_name_expected_result_pairs = get_soon_resolver_test___init___data()
        
        # Test
        resolver = SoonResolver(*args, **kwargs)
        for attribute_name, expected_result in attribute_name_expected_result_pairs:
            actual_result = getattr(resolver, attribute_name)
            self.assertEqual(expected_result, actual_result, attribute_name)
    
    #@unittest.skip  
    def test__inner_eq(self):
        # Test parameters
        data = get_soon_resolver_test__inner_eq_data()
        
        # Test
        for resolver1, resolver2, expected_result in data:
            self.assertEqual(expected_result, resolver1._inner_eq(resolver2))
    
    #@unittest.skip      
    def test_from_configuration(self):
        #Test parameters
        configuration, expected_result = get_soon_resolver_test_from_configuration_data()
        
        # Test
        actual_result = SoonResolver.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = SoonResolver
        
        pair_hierarchy_factory_configuration = PairGramTypeHierarchy.define_factory_configuration()
        mention_hierarchy_factory_configuration = MentionNoneHierarchy.define_factory_configuration()
        with_singleton_features = True
        with_anaphoricity_features = False
        quantize = True
        args_1 = tuple()
        kwargs_1 = {"pair_hierarchy_factory_configuration": pair_hierarchy_factory_configuration, 
                    "mention_hierarchy_factory_configuration": mention_hierarchy_factory_configuration, 
                    "with_singleton_features": with_singleton_features,
                    "with_anaphoricity_features": with_anaphoricity_features,
                    "quantize": quantize, 
                    }
        sample_features_vectorizer_factory_configuration =\
         ExtendedMentionPairSampleFeaturesVectorizer.define_factory_configuration(*args_1, **kwargs_1)
        
        C = 1.5
        fit_intercept = False
        max_iter = 47
        tol = 2e-2
        loss = "squared_hinge"
        average = True
        random_state = 9
        class_weight = "balanced"
        shuffle = False
        classifier_factory_configuration = Mapping({"name": "PassiveAggressiveSKLEARNBinaryClassifier", 
                                                    "config": Mapping({"C": C,
                                                                       "fit_intercept": fit_intercept,
                                                                       "max_iter": max_iter,
                                                                       "tol": tol,
                                                                       "loss": loss,
                                                                       "average": average,
                                                                       "random_state": random_state,
                                                                       "class_weight": class_weight,
                                                                       "shuffle": shuffle,
                                                                       }),
                                                    })
        
        generator_pair_filter_factory_configuration = RecallPairFilter.define_factory_configuration(max_mention_distance=7.9)
        
        args = tuple()
        kwargs = {"sample_features_vectorizer_factory_configuration": sample_features_vectorizer_factory_configuration,
                  "classifier_factory_configuration": classifier_factory_configuration,
                  "generator_pair_filter_factory_configuration": generator_pair_filter_factory_configuration,
                  }
        expected_result = Mapping({"generator": Mapping({"name": "SoonGenerator",
                                                         "config": Mapping({"pair_filter": generator_pair_filter_factory_configuration,
                                                                            }),
                                                         }),
                                   "decoder": Mapping({"name": "ClosestFirstCoreferenceDecoder",
                                                       "config": Mapping(),
                                                       }),
                                   "model_interface": Mapping({"name": "SKLEARNInterface",
                                                               "config": Mapping({"sample_features_vectorizer": sample_features_vectorizer_factory_configuration,
                                                                                  "classifier": Mapping({"name": "PassiveAggressiveSKLEARNBinaryClassifier",
                                                                                                         "config": Mapping({"C": C,
                                                                                                                            "fit_intercept": fit_intercept,
                                                                                                                            "max_iter": max_iter,
                                                                                                                            "tol": tol,
                                                                                                                            "loss": loss,
                                                                                                                            "average": average,
                                                                                                                            "random_state": random_state,
                                                                                                                            "class_weight": class_weight,
                                                                                                                            "shuffle": shuffle,
                                                                                                                            }),
                                                                                                         }),
                                                                                  }),
                                                               }),
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        #print(expected_result._model_interface.config.sample_features_vectorizer)
        #print(actual_result._model_interface.config.sample_features_vectorizer)
        #self.assertTrue(*Mapping._inner_eq(expected_result._model_interface.config.sample_features_vectorizer, actual_result._model_interface.config.sample_features_vectorizer))
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip  
    def test_train(self):
        # Test parameters
        (resolver, documents_iterable, kwargs, expected_model_interface) = get_soon_resolver_test_train_data()
        documents = tuple(documents_iterable)
        _create_iterable = lambda items: (item for item in items)
        
        # Test
        self.assertFalse(*expected_model_interface._inner_eq(resolver._model_interface))
        resolver.train(_create_iterable(documents), **kwargs) # Test that we can learn using an iterable instead of a collection
        #model_folder_path = os.path.join(os.path.dirname(__file__), "data", "pairs", "train_test_sklearn_model_interface")
        #resolver._model_interface.save_model(model_folder_path)
        self.assertTrue(*expected_model_interface._inner_eq(resolver._model_interface))
        resolver.train(_create_iterable(documents), **kwargs) # Test that we can learn anew after learning a first time
    
    #@unittest.skip  
    def test__predict_coreference_scores(self):
        # Test parameters
        data = get_soon_resolver_test__predict_coreference_scores_data()
        
        # Test
        for pair_classification_samples_iterable, resolver, kwargs, expected_result in data:
            actual_result = resolver._predict_coreference_scores(pair_classification_samples_iterable, **kwargs)
            #self.assertEqual(expected_result, actual_result)
            compare_float_values_dict(self, expected_result, actual_result)
    
    #@unittest.skip  
    def test_resolve(self):
        # Test parameters
        data = get_soon_resolver_test_resolve_data()
        
        # Test
        for document, resolver, kwargs, expected_result in data:
            actual_result = resolver.resolve(document, **kwargs)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip  
    def test_save(self):
        #Test parameters
        (resolver, test_sklearn_classifier, (expected_config_file_content, 
                                             (expected_sklearn_classifier, 
                                              expected_feature_names_file_content))) = get_test_soon_resolver_save_data()
        
        # Test
        with TemporaryDirectory() as temp_dir:
            temporary_folder_path = temp_dir.temporary_folder_path
            archive_file_name = "test_save_resolver"
            folder_path = temporary_folder_path
            
            archive_file_path = resolver.save(folder_path, archive_file_name)
            
            with TemporaryDirectory(temporary_root_folder_path=temporary_folder_path) as temp_dir2:
                extract_folder_path = temp_dir2.temporary_folder_path
                unzip_archive_to_folder(archive_file_path, extract_folder_path)
                actual_config_file_path = os.path.join(extract_folder_path, "configuration.txt")
                actual_features_names_file_path = os.path.join(extract_folder_path, 
                                                                       "model_interface_data", 
                                                                       SKLEARNInterface._FEATURES_NAMES_FILE_NAME)
                actual_model_data_folder_path = os.path.join(extract_folder_path, "model_interface_data")
                with open(actual_config_file_path, "r", encoding=ENCODING) as f:
                    actual_config_file_content = f.read()
                test_sklearn_classifier.load_model(actual_model_data_folder_path)
                with open(actual_features_names_file_path, "r", encoding=ENCODING) as f:
                    actual_feature_names_file_content = f.read()
        self.assertEqual(expected_feature_names_file_content, actual_feature_names_file_content)
        self.assertTrue(expected_sklearn_classifier._inner_eq(test_sklearn_classifier))
        self.maxDiff = None
        self.assertEqual(expected_config_file_content, actual_config_file_content)




#@unittest.skip  
class TestMcCarthyLehnertResolver(unittest.TestCase):
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = McCarthyLehnertResolver
        
        pair_hierarchy_factory_configuration = PairGramTypeHierarchy.define_factory_configuration()
        mention_hierarchy_factory_configuration = MentionNoneHierarchy.define_factory_configuration()
        with_singleton_features = True
        with_anaphoricity_features = False
        quantize = True
        args_1 = tuple()
        kwargs_1 = {"pair_hierarchy_factory_configuration": pair_hierarchy_factory_configuration, 
                    "mention_hierarchy_factory_configuration": mention_hierarchy_factory_configuration, 
                    "with_singleton_features": with_singleton_features,
                    "with_anaphoricity_features": with_anaphoricity_features,
                    "quantize": quantize, 
                    }
        sample_features_vectorizer_factory_configuration =\
         ExtendedMentionPairSampleFeaturesVectorizer.define_factory_configuration(*args_1, **kwargs_1)
        
        C = 1.5
        fit_intercept = False
        max_iter = 47
        tol = 2e-2
        loss = "squared_hinge"
        average = True
        random_state = 9
        class_weight = "balanced"
        shuffle = False
        classifier_factory_configuration = Mapping({"name": "PassiveAggressiveSKLEARNBinaryClassifier", 
                                                    "config": Mapping({"C": C,
                                                                       "fit_intercept": fit_intercept,
                                                                       "max_iter": max_iter,
                                                                       "tol": tol,
                                                                       "loss": loss,
                                                                       "average": average,
                                                                       "random_state": random_state,
                                                                       "class_weight": class_weight,
                                                                       "shuffle": shuffle,
                                                                       }),
                                                    })
        
        generator_pair_filter_factory_configuration = RecallPairFilter.define_factory_configuration(max_mention_distance=7.9)
        
        args = tuple()
        kwargs = {"sample_features_vectorizer_factory_configuration": sample_features_vectorizer_factory_configuration,
                  "classifier_factory_configuration": classifier_factory_configuration,
                  "generator_pair_filter_factory_configuration": generator_pair_filter_factory_configuration,
                  }
        expected_result = Mapping({"generator": Mapping({"name": "MentionPairSampleGenerator",
                                                         "config": Mapping({"pair_filter": generator_pair_filter_factory_configuration,
                                                                            }),
                                                         }),
                                   "decoder": Mapping({"name": "AggressiveMergeCoreferenceDecoder",
                                                       "config": Mapping(),
                                                       }),
                                   "model_interface": Mapping({"name": "SKLEARNInterface",
                                                               "config": Mapping({"sample_features_vectorizer": sample_features_vectorizer_factory_configuration,
                                                                                  "classifier": Mapping({"name": "PassiveAggressiveSKLEARNBinaryClassifier",
                                                                                                         "config": Mapping({"C": C,
                                                                                                                            "fit_intercept": fit_intercept,
                                                                                                                            "max_iter": max_iter,
                                                                                                                            "tol": tol,
                                                                                                                            "loss": loss,
                                                                                                                            "average": average,
                                                                                                                            "random_state": random_state,
                                                                                                                            "class_weight": class_weight,
                                                                                                                            "shuffle": shuffle,
                                                                                                                            }),
                                                                                                         }),
                                                                                  }),
                                                               }),
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip      
    def test(self):
        #Test parameters
        test_document = get_memoized_test_document_data()
        
        #use_entity_heads = False
        #use_singletons = False
        #entity_heads = None
        #singletons = None
        with_singleton_features = False
        with_anaphoricity_features = False
        model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                                          "with_singleton_features": with_singleton_features, 
                                                                                                                          "with_anaphoricity_features": with_anaphoricity_features, 
                                                                                                                          "quantize": True, 
                                                                                                                          "useless_field": "useless_value"})
                                                                                                 }),
                                                 "classifier": Mapping({"name": "LogisticRegressionSKLEARNBinaryClassifier", 
                                                                        "config": DEFAULT_LOGISTIC_REGRESSION_SKLEARN_BINARY_CLASSIFIER_CONFIG,
                                                                        }), 
                                                 })
        model_interface_configuration = Mapping({"name": "SKLEARNInterface", "config": model_interface_configuration})
        generator_configuration = Mapping([("pair_filter", Mapping({"name": "acceptall"})),])
        generator_configuration = Mapping({"name": "MentionPairSampleGenerator", "config": generator_configuration})
        decoder_configuration = Mapping({"name": "AggressiveMergeCoreferenceDecoder"})
        mention_filter_configuration = Mapping({"name": "NonReferentialMentionFilter", 
                                                "config": Mapping({"referential_probability_threshold": 0.3}),
                                                })
        
        configuration = Mapping({"model_interface": model_interface_configuration,
                                 "generator": generator_configuration,
                                 "decoder": decoder_configuration,
                                 "mention_filter": mention_filter_configuration,
                                 "witness_field9": "witness_value9",
                                 })
        
        documents = (test_document,)
        
        train_args = tuple()
        train_kwargs = {}#{"use_entity_heads": use_entity_heads, "use_singletons": use_singletons}
        
        resolve_args = tuple()
        resolve_kwargs = {}#{"entity_heads": entity_heads, "singletons": singletons}
        
        # Test
        resolver = McCarthyLehnertResolver.from_configuration(configuration)
        resolver.train(documents, *train_args, **train_kwargs)
        for document in documents:
            resolver.resolve(document, *resolve_args, **resolve_kwargs)



#@unittest.skip  
class TestNgCardieResolver(unittest.TestCase):
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = NgCardieResolver
        
        pair_hierarchy_factory_configuration = PairGramTypeHierarchy.define_factory_configuration()
        mention_hierarchy_factory_configuration = MentionNoneHierarchy.define_factory_configuration()
        with_singleton_features = True
        with_anaphoricity_features = False
        quantize = True
        args_1 = tuple()
        kwargs_1 = {"pair_hierarchy_factory_configuration": pair_hierarchy_factory_configuration, 
                    "mention_hierarchy_factory_configuration": mention_hierarchy_factory_configuration, 
                    "with_singleton_features": with_singleton_features,
                    "with_anaphoricity_features": with_anaphoricity_features,
                    "quantize": quantize, 
                    }
        sample_features_vectorizer_factory_configuration =\
         ExtendedMentionPairSampleFeaturesVectorizer.define_factory_configuration(*args_1, **kwargs_1)
        
        C = 1.5
        fit_intercept = False
        max_iter = 47
        tol = 2e-2
        loss = "squared_hinge"
        average = True
        random_state = 9
        class_weight = "balanced"
        shuffle = False
        classifier_factory_configuration = Mapping({"name": "PassiveAggressiveSKLEARNBinaryClassifier", 
                                                    "config": Mapping({"C": C,
                                                                       "fit_intercept": fit_intercept,
                                                                       "max_iter": max_iter,
                                                                       "tol": tol,
                                                                       "loss": loss,
                                                                       "average": average,
                                                                       "random_state": random_state,
                                                                       "class_weight": class_weight,
                                                                       "shuffle": shuffle,
                                                                       }),
                                                    })
        
        generator_pair_filter_factory_configuration = RecallPairFilter.define_factory_configuration(max_mention_distance=7.9)
        
        args = tuple()
        kwargs = {"sample_features_vectorizer_factory_configuration": sample_features_vectorizer_factory_configuration,
                  "classifier_factory_configuration": classifier_factory_configuration,
                  "generator_pair_filter_factory_configuration": generator_pair_filter_factory_configuration,
                  }
        expected_result = Mapping({"generator": Mapping({"name": "NgCardieGenerator",
                                                         "config": Mapping({"pair_filter": generator_pair_filter_factory_configuration,
                                                                            }),
                                                         }),
                                   "decoder": Mapping({"name": "BestFirstCoreferenceDecoder",
                                                       "config": Mapping(),
                                                       }),
                                   "model_interface": Mapping({"name": "SKLEARNInterface",
                                                               "config": Mapping({"sample_features_vectorizer": sample_features_vectorizer_factory_configuration,
                                                                                  "classifier": Mapping({"name": "PassiveAggressiveSKLEARNBinaryClassifier",
                                                                                                         "config": Mapping({"C": C,
                                                                                                                            "fit_intercept": fit_intercept,
                                                                                                                            "max_iter": max_iter,
                                                                                                                            "tol": tol,
                                                                                                                            "loss": loss,
                                                                                                                            "average": average,
                                                                                                                            "random_state": random_state,
                                                                                                                            "class_weight": class_weight,
                                                                                                                            "shuffle": shuffle,
                                                                                                                            }),
                                                                                                         }),
                                                                                  }),
                                                               }),
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip      
    def test(self):
        #Test parameters
        test_document = get_memoized_test_document_data()
    
        #use_entity_heads = False
        #use_singletons = False
        #entity_heads = None
        #singletons = None
        with_singleton_features = False
        with_anaphoricity_features = False
        model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                                          "with_singleton_features": with_singleton_features, 
                                                                                                                          "with_anaphoricity_features": with_anaphoricity_features, 
                                                                                                                          "quantize": True, 
                                                                                                                          "useless_field": "useless_value"})
                                                                                                 }),
                                                 "classifier": Mapping({"name": "LogisticRegressionSKLEARNBinaryClassifier", 
                                                                        "config": DEFAULT_LOGISTIC_REGRESSION_SKLEARN_BINARY_CLASSIFIER_CONFIG,
                                                                        }), 
                                                 })
        model_interface_configuration = Mapping({"name": "SKLEARNInterface", "config": model_interface_configuration})
        generator_configuration = Mapping([("pair_filter", Mapping({"name": "acceptall"})),])
        generator_configuration = Mapping({"name": "NgCardieGenerator", "config": generator_configuration})
        decoder_configuration = Mapping({"name": "BestFirstCoreferenceDecoder"})
        mention_filter_configuration = Mapping({"name": "NonReferentialMentionFilter", 
                                                "config": Mapping({"referential_probability_threshold": 0.3}),
                                                })
        
        configuration = Mapping({"model_interface": model_interface_configuration,
                                 "generator": generator_configuration,
                                 "decoder": decoder_configuration,
                                 "mention_filter": mention_filter_configuration,
                                 "witness_field9": "witness_value9",
                                })
        
        documents = (test_document,)
        
        train_args = tuple()
        train_kwargs = {}#{"use_entity_heads": use_entity_heads, "use_singletons": use_singletons}
        
        resolve_args = tuple()
        resolve_kwargs = {}#{"entity_heads": entity_heads, "singletons": singletons}
        
        # Test
        resolver = NgCardieResolver.from_configuration(configuration)
        resolver.train(documents, *train_args, **train_kwargs)
        for document in documents:
            resolver.resolve(document, *resolve_args, **resolve_kwargs)



#@unittest.skip  
class TestHACResolver(unittest.TestCase):
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = HACResolver
        
        pair_hierarchy_factory_configuration = PairGramTypeHierarchy.define_factory_configuration()
        mention_hierarchy_factory_configuration = MentionNoneHierarchy.define_factory_configuration()
        with_singleton_features = True
        with_anaphoricity_features = False
        quantize = True
        args_1 = tuple()
        kwargs_1 = {"pair_hierarchy_factory_configuration": pair_hierarchy_factory_configuration, 
                    "mention_hierarchy_factory_configuration": mention_hierarchy_factory_configuration, 
                    "with_singleton_features": with_singleton_features,
                    "with_anaphoricity_features": with_anaphoricity_features,
                    "quantize": quantize, 
                    }
        sample_features_vectorizer_factory_configuration =\
         ExtendedMentionPairSampleFeaturesVectorizer.define_factory_configuration(*args_1, **kwargs_1)
        
        C = 1.5
        fit_intercept = False
        max_iter = 47
        tol = 2e-2
        loss = "squared_hinge"
        average = True
        random_state = 9
        class_weight = "balanced"
        shuffle = False
        classifier_factory_configuration = Mapping({"name": "PassiveAggressiveSKLEARNBinaryClassifier", 
                                                    "config": Mapping({"C": C,
                                                                       "fit_intercept": fit_intercept,
                                                                       "max_iter": max_iter,
                                                                       "tol": tol,
                                                                       "loss": loss,
                                                                       "average": average,
                                                                       "random_state": random_state,
                                                                       "class_weight": class_weight,
                                                                       "shuffle": shuffle,
                                                                       }),
                                                    })
        
        generator_pair_filter_factory_configuration = RecallPairFilter.define_factory_configuration(max_mention_distance=7.9)
        
        clustering_type = "single_link"
        
        args = tuple()
        kwargs = {"sample_features_vectorizer_factory_configuration": sample_features_vectorizer_factory_configuration,
                  "classifier_factory_configuration": classifier_factory_configuration,
                  "generator_pair_filter_factory_configuration": generator_pair_filter_factory_configuration,
                  "clustering_type": clustering_type,
                  }
        expected_result = Mapping({"generator": Mapping({"name": "MentionPairSampleGenerator",
                                                         "config": Mapping({"pair_filter": generator_pair_filter_factory_configuration,
                                                                            }),
                                                         }),
                                   "decoder": Mapping({"name": "HACCoreferenceDecoder",
                                                       "config": Mapping({"clustering_type": clustering_type,}),
                                                       }),
                                   "model_interface": Mapping({"name": "SKLEARNInterface",
                                                               "config": Mapping({"sample_features_vectorizer": sample_features_vectorizer_factory_configuration,
                                                                                  "classifier": Mapping({"name": "PassiveAggressiveSKLEARNBinaryClassifier",
                                                                                                         "config": Mapping({"C": C,
                                                                                                                            "fit_intercept": fit_intercept,
                                                                                                                            "max_iter": max_iter,
                                                                                                                            "tol": tol,
                                                                                                                            "loss": loss,
                                                                                                                            "average": average,
                                                                                                                            "random_state": random_state,
                                                                                                                            "class_weight": class_weight,
                                                                                                                            "shuffle": shuffle,
                                                                                                                            }),
                                                                                                         }),
                                                                                  }),
                                                               }),
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip      
    def test(self):
        #Test parameters
        test_document = get_memoized_test_document_data()
    
        #use_entity_heads = False
        #use_singletons = False
        #entity_heads = None
        #singletons = None
        with_singleton_features = False
        with_anaphoricity_features = False
        model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                                          "with_singleton_features": with_singleton_features, 
                                                                                                                          "with_anaphoricity_features": with_anaphoricity_features, 
                                                                                                                          "quantize": True, 
                                                                                                                          "useless_field": "useless_value"})
                                                                                                 }),
                                                 "classifier": Mapping({"name": "LogisticRegressionSKLEARNBinaryClassifier", 
                                                                        "config": DEFAULT_LOGISTIC_REGRESSION_SKLEARN_BINARY_CLASSIFIER_CONFIG,
                                                                        }), 
                                                 })
        model_interface_configuration = Mapping({"name": "SKLEARNInterface", "config": model_interface_configuration})
        generator_configuration = Mapping([("pair_filter", Mapping({"name": "acceptall"})),])
        generator_configuration = Mapping({"name": "MentionPairSampleGenerator", "config": generator_configuration})
        decoder_configuration = Mapping({"clustering_type": "single_link"})
        decoder_configuration = Mapping({"name": "HACCoreferenceDecoder", "config": decoder_configuration})
        mention_filter_configuration = Mapping({"name": "NonReferentialMentionFilter", 
                                                "config": Mapping({"referential_probability_threshold": 0.3}),
                                                })
        
        configuration = Mapping({"model_interface": model_interface_configuration,
                                 "generator": generator_configuration,
                                 "decoder": decoder_configuration,
                                 "mention_filter": mention_filter_configuration,
                                 "witness_field9": "witness_value9",
                                 })
        
        documents = (test_document,)
        
        train_args = tuple()
        train_kwargs = {}#{"use_entity_heads": use_entity_heads, "use_singletons": use_singletons}
        
        resolve_args = tuple()
        resolve_kwargs = {}#{"sys_mentions": sys_mentions, "entity_heads": entity_heads, "singletons": singletons}
        
        # Test
        resolver = HACResolver.from_configuration(configuration)
        resolver.train(documents, *train_args, **train_kwargs)
        for document in documents:
            resolver.resolve(document, *resolve_args, **resolve_kwargs)



#@unittest.skip  
class TestOraclePairResolver(unittest.TestCase):
    
    #@unittest.skip      
    def test___init__(self):
        #Test parameters
        (args, kwargs), attribute_name_expected_result_pairs = get_oracle_pair_resolver_test___init___data()
        
        # Test
        resolver = OraclePairResolver(*args, **kwargs)
        for attribute_name, expected_result in attribute_name_expected_result_pairs:
            actual_result = getattr(resolver, attribute_name)
            self.assertEqual(expected_result, actual_result, attribute_name)
    
    #@unittest.skip  
    def test__inner_eq(self):
        # Test parameters
        data = get_oracle_pair_resolver_test__inner_eq_data()
        
        # Test
        for resolver1, resolver2, expected_result in data:
            self.assertEqual(expected_result, resolver1._inner_eq(resolver2))
    
    #@unittest.skip      
    def test_from_configuration(self):
        #Test parameters
        configuration, expected_result = get_oracle_pair_resolver_test_from_configuration_data()
        
        # Test
        actual_result = OraclePairResolver.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = OraclePairResolver
        
        generator_pair_filter_factory_configuration = RecallPairFilter.define_factory_configuration(max_mention_distance=7.9)
        
        args = tuple()
        kwargs = {"generator_pair_filter_factory_configuration": generator_pair_filter_factory_configuration,
                  }
        expected_result = Mapping({"generator": Mapping({"name": "MentionPairSampleGenerator",
                                                         "config": Mapping({"pair_filter": generator_pair_filter_factory_configuration,
                                                                            }),
                                                         }),
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip  
    def test_resolve(self):
        # Test parameters
        data = get_oracle_pair_resolver_test_resolve_data()
        
        # Test
        for (sys_document, ref_document), resolver, kwargs, expected_result in data:
            actual_result = resolver.resolve(sys_document, ref_document, **kwargs)
            self.assertEqual(expected_result, actual_result)


#@unittest.skip    
class TestOraclePairScoreResolver(unittest.TestCase):
    
    #@unittest.skip     
    def test___init__(self):
        #Test parameters
        (args, kwargs), attribute_name_expected_result_pairs = get_oracle_pair_score_resolver_test___init___data()
        
        # Test
        resolver = OraclePairScoreResolver(*args, **kwargs)
        for attribute_name, expected_result in attribute_name_expected_result_pairs:
            self.assertEqual(expected_result, getattr(resolver, attribute_name), attribute_name)
    
    #@unittest.skip   
    def test__inner_eq(self):
        # Test parameters
        data = get_oracle_pair_score_resolver_test__inner_eq_data()
        
        # Test
        for resolver1, resolver2, expected_result in data:
            self.assertEqual(expected_result, resolver1._inner_eq(resolver2))
    
    #@unittest.skip       
    def test_from_configuration(self):
        #Test parameters
        configuration, expected_result = get_oracle_pair_score_resolver_test_from_configuration_data()
        
        # Test
        actual_result = OraclePairScoreResolver.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = OraclePairScoreResolver
        
        pair_hierarchy_factory_configuration = PairGramTypeHierarchy.define_factory_configuration()
        mention_hierarchy_factory_configuration = MentionNoneHierarchy.define_factory_configuration()
        with_singleton_features = True
        with_anaphoricity_features = False
        quantize = True
        args_1 = tuple()
        kwargs_1 = {"pair_hierarchy_factory_configuration": pair_hierarchy_factory_configuration, 
                    "mention_hierarchy_factory_configuration": mention_hierarchy_factory_configuration, 
                    "with_singleton_features": with_singleton_features,
                    "with_anaphoricity_features": with_anaphoricity_features,
                    "quantize": quantize, 
                    }
        sample_features_vectorizer_factory_configuration =\
         ExtendedMentionPairSampleFeaturesVectorizer.define_factory_configuration(*args_1, **kwargs_1)
        
        C = 1.5
        fit_intercept = False
        max_iter = 47
        tol = 2e-2
        loss = "squared_hinge"
        average = True
        random_state = 9
        class_weight = "balanced"
        shuffle = False
        classifier_factory_configuration = Mapping({"name": "PassiveAggressiveSKLEARNBinaryClassifier", 
                                                    "config": Mapping({"C": C,
                                                                       "fit_intercept": fit_intercept,
                                                                       "max_iter": max_iter,
                                                                       "tol": tol,
                                                                       "loss": loss,
                                                                       "average": average,
                                                                       "random_state": random_state,
                                                                       "class_weight": class_weight,
                                                                       "shuffle": shuffle,
                                                                       }),
                                                    })
        
        generator_pair_filter_factory_configuration = RecallPairFilter.define_factory_configuration(max_mention_distance=7.9)
        
        args = tuple()
        kwargs = {"sample_features_vectorizer_factory_configuration": sample_features_vectorizer_factory_configuration,
                  "classifier_factory_configuration": classifier_factory_configuration,
                  "generator_pair_filter_factory_configuration": generator_pair_filter_factory_configuration,
                  }
        expected_result = Mapping({"generator": Mapping({"name": "MentionPairSampleGenerator",
                                                         "config": Mapping({"pair_filter": generator_pair_filter_factory_configuration,
                                                                            }),
                                                         }),
                                   "decoder": Mapping({"name": "OracleAggressiveMergeCoreferenceDecoder",
                                                       "config": Mapping(),
                                                       }),
                                   "model_interface": Mapping({"name": "SKLEARNInterface",
                                                               "config": Mapping({"sample_features_vectorizer": sample_features_vectorizer_factory_configuration,
                                                                                  "classifier": Mapping({"name": "PassiveAggressiveSKLEARNBinaryClassifier",
                                                                                                         "config": Mapping({"C": C,
                                                                                                                            "fit_intercept": fit_intercept,
                                                                                                                            "max_iter": max_iter,
                                                                                                                            "tol": tol,
                                                                                                                            "loss": loss,
                                                                                                                            "average": average,
                                                                                                                            "random_state": random_state,
                                                                                                                            "class_weight": class_weight,
                                                                                                                            "shuffle": shuffle,
                                                                                                                            }),
                                                                                                         }),
                                                                                  }),
                                                               }),
                                   })
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip   
    def test_resolve(self):
        # Test parameters
        data = get_oracle_pair_score_resolver_test_resolve_data()
        
        # Test
        for ref_document, resolver, kwargs, expected_result in data:
            actual_result = resolver.resolve(ref_document, **kwargs)
            self.assertEqual(expected_result, actual_result)




def compare_float_values_dict(test_case, expected_result, actual_result):
    test_case.assertEqual(type(expected_result), type(actual_result))
    test_case.assertEqual(set(expected_result), set(actual_result))
    keys = tuple(sorted(expected_result))
    expected_values = numpy.array(tuple(expected_result[k] for k in keys))
    actual_values = numpy.array(tuple(expected_result[k] for k in keys))
    test_case.assertTrue(are_array_equal(expected_values, actual_values))

def get_test_document_data():
    from cortex.io.pivot_reader import PivotReader
    
    folder_path = os.path.join(os.path.dirname(__file__), "data", "pairs")
    document_folder_path = os.path.join(folder_path, "pair_resolver_test_data", "ref")
    
    strict = True # We want to synchronize head tokens.
    test_document = PivotReader.parse(document_folder_path, strict=strict)
    
    return test_document

@Memoized
def get_memoized_test_document_data():
    return get_test_document_data()


penalty = "l2"
dual = False
tol = 1e-4
C = 1.0
fit_intercept = True
intercept_scaling = 1
class_weight = None
random_state = None
solver = "liblinear"
max_iter = 100
multi_class = "ovr"
DEFAULT_LOGISTIC_REGRESSION_SKLEARN_BINARY_CLASSIFIER_CONFIG = Mapping([("penalty", penalty), 
                                                                        ("dual", dual), 
                                                                        ("tol", tol), 
                                                                        ("C", C), 
                                                                        ("fit_intercept", fit_intercept), 
                                                                        ("intercept_scaling", intercept_scaling), 
                                                                        ("class_weight", class_weight), 
                                                                        ("random_state", random_state), 
                                                                        ("solver", solver), 
                                                                        ("max_iter", max_iter), 
                                                                        ("multi_class", multi_class),
                                                                        ])

def get_soon_resolver_test___init___data():
    attribute_name_expected_result_pairs = []
    
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                    "with_singleton_features": False, 
                                                                                                    "with_anaphoricity_features": False, 
                                                                                                    "quantize": True, 
                                                                                                    "strict": True,
                                                                                                    "useless_field": "useless_value"})
                                                                                 }),
                                             "classifier": Mapping({"name": "LogisticRegressionSKLEARNBinaryClassifier", 
                                                                    "config": DEFAULT_LOGISTIC_REGRESSION_SKLEARN_BINARY_CLASSIFIER_CONFIG,
                                                                    }), 
                                             })
    model_interface = create_model_interface_from_factory_configuration(Mapping((("name", "SKLEARNInterface"), ("config", model_interface_configuration))))
    attribute_name_expected_result_pairs.append(("_model_interface",model_interface))
    
    generator_configuration = Mapping([("pair_filter", Mapping([("name", "PairFiltersCombination"), 
                                                                           ("config", Mapping([("filters", [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                                              "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                                              }),
                                                                                                                               ]),
                                                                                                    ("witness_field6", "witness_value6"),
                                                                                                    ])
                                                                            ),
                                                                            ])
                                              ),
                                             ("witness_field7", "witness_value7"),
                                             ])
    generator = SoonGenerator.from_configuration(generator_configuration)
    attribute_name_expected_result_pairs.append(("_generator",generator))
    
    decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    decoder = ClosestFirstCoreferenceDecoder.from_configuration(decoder_configuration)
    attribute_name_expected_result_pairs.append(("_decoder",decoder))
    
    configuration = Mapping({"witness_field9": "witness_value9"})
    
    
    expected_model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                                      "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                                               "with_singleton_features": False, 
                                                                                                                               "with_anaphoricity_features": False, 
                                                                                                                               "quantize": True,
                                                                                                                               "strict": True, 
                                                                                                                               "useless_field": "useless_value"})
                                                                                                      }),
                                                      "classifier": Mapping({"name": "LogisticRegressionSKLEARNBinaryClassifier", 
                                                                             "config": DEFAULT_LOGISTIC_REGRESSION_SKLEARN_BINARY_CLASSIFIER_CONFIG,
                                                                        }), 
                                                      })
    expected_model_interface_configuration = Mapping({"name": "SKLEARNInterface", "config": expected_model_interface_configuration})
    expected_generator_configuration = Mapping([("witness_field7", "witness_value7"),
                                                      ("pair_filter", Mapping({"name": "PairFiltersCombination", 
                                                                                     "config": Mapping({"filters": [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                                                       "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                                                       }),
                                                                                                                                        ],
                                                                                                              "witness_field6": "witness_value6",
                                                                                                              }),
                                                                                     })
                                                       ),
                                                      ])
    expected_generator_configuration = Mapping({"name": "SoonGenerator", "config": expected_generator_configuration})
    expected_decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    expected_decoder_configuration = Mapping({"name": "ClosestFirstCoreferenceDecoder", "config": expected_decoder_configuration})
    expected_configuration = Mapping({"model_interface": expected_model_interface_configuration,
                                            "generator": expected_generator_configuration,
                                            "decoder": expected_decoder_configuration,
                                            "witness_field9": "witness_value9",
                                            })
    
    attribute_name_expected_result_pairs.append(("configuration", expected_configuration))
    
    args = (model_interface, generator, decoder)
    kwargs = {"configuration": configuration}
    
    return (args, kwargs), attribute_name_expected_result_pairs

def get_soon_resolver_test__inner_eq_data():
    data = []
    
    (args, kwargs), _ = get_soon_resolver_test___init___data() #attribute_name_expected_result_pairs
    
    resolver1 = SoonResolver(*args, **kwargs)
    resolver2 = SoonResolver(*args, **kwargs)
    expected_result = (True, None)
    data.append((resolver1, resolver2, expected_result))
    
    decoder = ClosestFirstCoreferenceDecoder.from_configuration(Mapping({"witness_field42": "witness_value42s"}))
    args = args[:-1] + (decoder,)
    resolver3 = SoonResolver(*args, **kwargs)
    expected_result = (False, "_decoder")
    data.append((resolver1, resolver3, expected_result))
    
    return data

def get_soon_resolver_test_from_configuration_data():
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                             "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                                      "with_singleton_features": False, 
                                                                                                                      "with_anaphoricity_features": False, 
                                                                                                                      "quantize": True, 
                                                                                                                      "strict": True,
                                                                                                                      "useless_field": "useless_value"})
                                                                                             }),
                                             "classifier": Mapping({"name": "LogisticRegressionSKLEARNBinaryClassifier", 
                                                                    "config": DEFAULT_LOGISTIC_REGRESSION_SKLEARN_BINARY_CLASSIFIER_CONFIG,
                                                                    }), 
                                             "witness_field0": "witness_value0",
                                             })
    model_interface = create_model_interface_from_factory_configuration(Mapping([("name", "SKLEARNInterface"), ("config", model_interface_configuration),]))
    
    generator_configuration = Mapping([("pair_filter", Mapping([("name", "PairFiltersCombination"), 
                                                                            ("config", Mapping((("filters", [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                                                "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                                                }),
                                                                                                                                 ]),
                                                                                                      ("witness_field6", "witness_value6"),
                                                                                                      ))),
                                                                            ])
                                              ),
                                             ("witness_field7", "witness_value7"),
                                             ])
    generator = SoonGenerator.from_configuration(generator_configuration)
    
    decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    decoder = ClosestFirstCoreferenceDecoder.from_configuration(decoder_configuration)
    
    configuration = Mapping({"witness_field9": "witness_value9"})
    
    
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                             "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                                      "with_singleton_features": False, 
                                                                                                                      "with_anaphoricity_features": False, 
                                                                                                                      "quantize": True, 
                                                                                                                      "strict": True,
                                                                                                                      "useless_field": "useless_value"})
                                                                                             }),
                                             "classifier": Mapping({"name": "LogisticRegressionSKLEARNBinaryClassifier", 
                                                                    "config": DEFAULT_LOGISTIC_REGRESSION_SKLEARN_BINARY_CLASSIFIER_CONFIG,
                                                                    }), 
                                             "witness_field0": "witness_value0",
                                             })
    model_interface_configuration = Mapping({"name": "SKLEARNInterface", "config": model_interface_configuration})
    generator_configuration = Mapping([("witness_field7", "witness_value7"),
                                             ("pair_filter", Mapping({"name": "PairFiltersCombination", 
                                                                            "config": Mapping({"filters": [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                                              "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                                              }),
                                                                                                                               ],
                                                                                                     "witness_field6": "witness_value6",
                                                                                                     }),
                                                                            })
                                              ),
                                             ])
    generator_configuration = Mapping({"name": "SoonGenerator", "config": generator_configuration})
    decoder_configuration = Mapping({"witness_field15": "witness_value15"})
    decoder_configuration = Mapping({"name": "ClosestFirstCoreferenceDecoder", "config": decoder_configuration})
    
    args = (model_interface, generator, decoder)
    kwargs = {"configuration": configuration}
    expected_result = SoonResolver(*args, **kwargs)
    
    configuration = Mapping({"model_interface": model_interface_configuration,
                                   "generator": generator_configuration,
                                   "decoder": decoder_configuration,
                                   "witness_field9": "witness_value9",
                                   })
    
    return configuration, expected_result

def get_soon_resolver_test_train_data():
    test_document = get_memoized_test_document_data()
    
    with_singleton_features = False
    with_anaphoricity_features = False
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                             "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy"}), 
                                                                                                                      "with_singleton_features": with_singleton_features, 
                                                                                                                      "with_anaphoricity_features": with_anaphoricity_features, 
                                                                                                                      "quantize": True, 
                                                                                                                      "strict": True,
                                                                                                                      "useless_field": "useless_value"})
                                                                                             }),
                                             "classifier": Mapping({"name": "LogisticRegressionSKLEARNBinaryClassifier", 
                                                                    "config": DEFAULT_LOGISTIC_REGRESSION_SKLEARN_BINARY_CLASSIFIER_CONFIG,
                                                                    }), 
                                             })
    model_interface_configuration = Mapping({"name": "SKLEARNInterface", "config": model_interface_configuration})
    expected_model_interface = create_model_interface_from_factory_configuration(model_interface_configuration)
    model_folder_path = os.path.join(os.path.dirname(__file__), "data", "pairs", "train_test_sklearn_model_interface")
    expected_model_interface.load_model(model_folder_path)
    
    generator_configuration = Mapping([("binarize", True),
                                       ("quantize", True),
                                       ("pair_hierarchy", Mapping({"name": "PairGramTypeHierarchy"})),
                                       ("mention_filter", Mapping({"name": "NonReferentialMentionFilter", 
                                                                   "config": Mapping({"referential_probability_threshold": 0.0}), #0.3
                                                                   })),
                                       ("pair_filter", Mapping({"name": "acceptall"})),
                                       ])
    generator_configuration = Mapping({"name": "SoonGenerator", "config": generator_configuration})
    decoder_configuration = Mapping({"name": "ClosestFirstCoreferenceDecoder"})
    
    configuration = Mapping({"model_interface": model_interface_configuration,
                             "generator": generator_configuration,
                             "decoder": decoder_configuration,
                             "witness_field9": "witness_value9",
                             })
    resolver = SoonResolver.from_configuration(configuration)
    
    documents_iterable = (d for d in (test_document,))
    
    kwargs = {}
    
    return resolver, documents_iterable, kwargs, expected_model_interface

def get_soon_resolver_test__predict_coreference_scores_data():
    data = []
    
    test_document = get_memoized_test_document_data()
    
    with_singleton_features = False
    with_anaphoricity_features = False
    model_interface_configuration_ = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                             "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy"}), 
                                                                                                                      "with_singleton_features": with_singleton_features, 
                                                                                                                      "with_anaphoricity_features": with_anaphoricity_features, 
                                                                                                                      "quantize": True, 
                                                                                                                      "strict": True,
                                                                                                                      "useless_field": "useless_value"})
                                                                                             }),
                                             "classifier": Mapping({"name": "LogisticRegressionSKLEARNBinaryClassifier", 
                                                                    "config": DEFAULT_LOGISTIC_REGRESSION_SKLEARN_BINARY_CLASSIFIER_CONFIG,
                                                                    }), 
                                             })
    model_interface_configuration = Mapping({"name": "SKLEARNInterface", "config": model_interface_configuration_})
    model_interface_folder_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "pairs", "train_test_sklearn_model_interface")
    generator_configuration = Mapping((("pair_filter", Mapping({"name": "acceptall"})),))
    generator_configuration = Mapping({"name": "SoonGenerator", "config": generator_configuration})
    decoder_configuration = Mapping({"name": "ClosestFirstCoreferenceDecoder"})
    configuration = Mapping({"model_interface": model_interface_configuration,
                             "generator": generator_configuration,
                             "decoder": decoder_configuration,
                             "witness_field9": "witness_value9",
                             })
    resolver = SoonResolver.from_configuration(configuration)
    resolver.load_model(model_interface_folder_path)
    
    pair_classification_samples_iterable = resolver._generator.generate_prediction_set(test_document)
    
    kwargs = {}
    
    # With SKLEARN
    expected_result = {((68, 75), (114, 120)): 0.06528476305040043, 
                        ((1, 9), (46, 47)): 0.4522209964467172, 
                        ((1, 9), (114, 120)): 0.03480735619266995, 
                        ((83, 84), (114, 120)): 0.02910828415106781, 
                        ((1, 9), (83, 84)): 0.5247371718566036, 
                        ((14, 42), (46, 47)): 0.47800111927793343, 
                        ((46, 47), (68, 75)): 0.03842960020172004, 
                        ((14, 42), (68, 75)): 0.35417361239864265, 
                        ((1, 9), (14, 42)): 0.5897265133696545, 
                        ((1, 9), (68, 75)): 0.36250933084757997, 
                        ((68, 75), (83, 84)): 0.12932126826854506, 
                        ((14, 42), (27, 42)): 0.10973969486615676, 
                        ((46, 47), (83, 84)): 0.9551621853910138, 
                        ((14, 42), (83, 84)): 0.5504967840977787, 
                        ((27, 42), (83, 84)): 0.5112784498119287, 
                        ((27, 42), (114, 120)): 0.03658362322782682, 
                        ((14, 42), (114, 120)): 0.030432570594668973, 
                        ((46, 47), (114, 120)): 0.026729224665914098, 
                        ((27, 42), (68, 75)): 0.9074327424351927, 
                        ((1, 9), (27, 42)): 0.25497832375088814, 
                        ((27, 42), (46, 47)): 0.43890456172358183,
                        }
    
    data.append((pair_classification_samples_iterable, resolver, kwargs, expected_result))
    
    return data

def get_soon_resolver_test__pipeline_resolve_data():
    data = []
    
    test_document = get_memoized_test_document_data()
    
    entity_heads = None
    singletons = None
    with_singleton_features = False
    with_anaphoricity_features = False
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                             "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy"}), 
                                                                                                                      "with_singleton_features": with_singleton_features, 
                                                                                                                      "with_anaphoricity_features": with_anaphoricity_features, 
                                                                                                                      "quantize": True, 
                                                                                                                      "strict": True,
                                                                                                                      "useless_field": "useless_value"})
                                                                                             }),
                                             "classifier": Mapping({"name": "LogisticRegressionSKLEARNBinaryClassifier", 
                                                                    "config": DEFAULT_LOGISTIC_REGRESSION_SKLEARN_BINARY_CLASSIFIER_CONFIG,
                                                                    }), 
                                             })
    model_interface_configuration = Mapping({"name": "SKLEARNInterface", "config": model_interface_configuration})
    model_interface_folder_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "pairs", "train_test_sklearn_model_interface")
    generator_configuration = Mapping([("pair_filter", Mapping({"name": "acceptall"})),])
    generator_configuration = Mapping({"name": "SoonGenerator", "config": generator_configuration})
    decoder_configuration = Mapping({"name": "ClosestFirstCoreferenceDecoder"})
    
    configuration = Mapping({"model_interface": model_interface_configuration,
                             "generator": generator_configuration,
                             "decoder": decoder_configuration,
                             "witness_field9": "witness_value9",
                            })
    resolver = SoonResolver.from_configuration(configuration)
    resolver._model_interface.load_model(model_interface_folder_path)
    
    kwargs = {"entity_head_extents": entity_heads, "singleton_extents": singletons}
    
    pos_coref_pairs = (((1,9), (14,42)),
                       ((27,42), (68,75)),
                       ((46,47), (83,84)),
                       )
    #excluded_pairs = ((46,47), (83,84))
    excluded_pairs = tuple()
    expected_result = CoreferencePartition(mention_extents=tuple(m.extent for m in test_document.mentions if m.extent not in excluded_pairs))
    for left_extent, right_extent in pos_coref_pairs:
        left_mention = test_document.extent2mention[left_extent]
        right_mention = test_document.extent2mention[right_extent]
        expected_result.join(left_mention.extent, right_mention.extent)
    
    data.append((test_document, resolver, kwargs, expected_result))
    
    return data

def get_soon_resolver_test_resolve_data():
    data0 = get_soon_resolver_test__pipeline_resolve_data()
    data = []
    for test_document, resolver, kwargs, expected_result in data0:
        kwargs = {}
        data.append((test_document, resolver, kwargs, expected_result))
    return data

def get_test_soon_resolver_save_data():
    model_folder_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "pairs", "train_test_sklearn_model_interface")
    with_singleton_features = False
    with_anaphoricity_features = False
    model_interface_configuration = Mapping([("sample_features_vectorizer", Mapping([("name", "MentionPairSampleFeaturesVectorizer"),
                                                                                    ("config", Mapping([("pair_hierarchy", Mapping([("name", "PairGramTypeHierarchy"),
                                                                                                                                    ("config", Mapping())
                                                                                                                                    ])
                                                                                                         ), 
                                                                                                        ("with_singleton_features", with_singleton_features), 
                                                                                                        ("with_anaphoricity_features", with_anaphoricity_features), 
                                                                                                        ("quantize", True), 
                                                                                                        ("strict", True),
                                                                                                        ])
                                                                                     ),
                                                                                    ])),
                                             ("classifier", Mapping([("name", "LogisticRegressionSKLEARNBinaryClassifier"), 
                                                                    ("config", DEFAULT_LOGISTIC_REGRESSION_SKLEARN_BINARY_CLASSIFIER_CONFIG),
                                                                    ])), 
                                             ])
    model_interface_configuration = Mapping([("name", "SKLEARNInterface"), 
                                             ("config", model_interface_configuration),
                                             ])
    generator_configuration = Mapping([("pair_filter", Mapping([("name", "acceptall"), ("config", Mapping())])),])
    generator_configuration = Mapping([("name", "SoonGenerator"), 
                                       ("config", generator_configuration),
                                       ])
    decoder_configuration = Mapping([("name", "ClosestFirstCoreferenceDecoder"), ("config", Mapping())])
    
    configuration = Mapping([("model_interface", model_interface_configuration),
                             ("generator", generator_configuration),
                             ("decoder", decoder_configuration),
                             ("witness_field9", "witness_value9"),
                             ])
    resolver = SoonResolver.from_configuration(configuration)
    resolver._model_interface.load_model(model_folder_path)
    
    expected_config_file_content =\
    """name : 'SoonResolver'
config :
{
    model_interface :
    {
        name : 'SKLEARNInterface'
        config :
        {
            sample_features_vectorizer :
            {
                name : 'MentionPairSampleFeaturesVectorizer'
                config :
                {
                    pair_hierarchy :
                    {
                        name : 'PairGramTypeHierarchy'
                        config : { }
                    }
                    with_singleton_features : False
                    with_anaphoricity_features : False
                    quantize : True
                    strict : True
                }
            }
            classifier :
            {
                name : 'LogisticRegressionSKLEARNBinaryClassifier'
                config :
                {
                    penalty : 'l2'
                    dual : False
                    tol : 0.0001
                    C : 1.0
                    fit_intercept : True
                    intercept_scaling : 1
                    class_weight : None
                    random_state : None
                    solver : 'liblinear'
                    max_iter : 100
                    multi_class : 'ovr'
                }
            }
        }
    }
    generator :
    {
        name : 'SoonGenerator'
        config :
        {
            pair_filter :
            {
                name : 'AcceptAllPairFilter'
                config : { }
            }
        }
    }
    decoder :
    {
        name : 'ClosestFirstCoreferenceDecoder'
        config : { }
    }
    witness_field9 : 'witness_value9'
}
"""
    _sklearn_interface = create_model_interface_from_factory_configuration(model_interface_configuration)
    _sklearn_interface.load_model(model_folder_path)
    expected_sklearn_classifier = _sklearn_interface._classifier
    test_sklearn_classifier = create_model_interface_from_factory_configuration(model_interface_configuration)._classifier
    with open(os.path.join(model_folder_path, "features_names.txt"), "r", encoding=ENCODING) as f:
        expected_feature_names_file_content = f.read()
    
    #archive_file_name = "test_soon_resolver_load"
    #archive_folder_path = os.path.join(os.path.dirname(__file__), "data", "select_", "test_load_resolver")
    #resolver.save(archive_folder_path, archive_file_name)
    
    return resolver, test_sklearn_classifier, (expected_config_file_content, (expected_sklearn_classifier, expected_feature_names_file_content))



def get_oracle_pair_resolver_test___init___data():
    attribute_name_expected_result_pairs = []
    
    generator_configuration = Mapping([("witness_field7", "witness_value7"),
                                             ("pair_filter", Mapping([("name", "PairFiltersCombination"), 
                                                                            ("config", Mapping([("filters", [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                                                "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                                                }),
                                                                                                                                 ]),
                                                                                                      ("witness_field6", "witness_value6"),
                                                                                                      ])
                                                                             ),
                                                                            ])
                                              ),
                                             ])
    generator = MentionPairSampleGenerator.from_configuration(generator_configuration)
    attribute_name_expected_result_pairs.append(("_generator", generator))
    
    configuration = Mapping({"witness_field9": "witness_value9"})
    
    with_singleton_features = False
    with_anaphoricity_features = True
    expected_model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                                      "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping({"witness_field2": "witness_value2"})}), 
                                                                                                                               "with_singleton_features": with_singleton_features, 
                                                                                                                               "with_anaphoricity_features": with_anaphoricity_features, 
                                                                                                                               "quantize": True, 
                                                                                                                               "strict": False,
                                                                                                                               "useless_field": "useless_value"})
                                                                                                      }),
                                                            "witness_field0": "witness_value0",
                                                            })
    expected_model_interface_configuration = Mapping({"name": "SKLEARNInterface", "config": expected_model_interface_configuration})
    expected_generator_configuration = Mapping((("witness_field7", "witness_value7"),
                                                      ("pair_filter", Mapping({"name": "PairFiltersCombination", 
                                                                                     "config": Mapping({"filters": [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                                                       "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                                                       }),
                                                                                                                                        ],
                                                                                                              "witness_field6": "witness_value6",
                                                                                                              }),
                                                                                     })),
                                                      ),
                                                     )
    expected_generator_configuration = Mapping({"name": "MentionPairSampleGenerator", 
                                                      "config": expected_generator_configuration})
    expected_configuration = Mapping({"generator": expected_generator_configuration,
                                            "witness_field9": "witness_value9",
                                            })
    
    attribute_name_expected_result_pairs.append(("configuration", expected_configuration))
    
    args = (generator, )
    kwargs = {"configuration": configuration}
    
    return (args, kwargs), attribute_name_expected_result_pairs

def get_oracle_pair_resolver_test__inner_eq_data():
    data = []
    
    (args, kwargs), _ = get_oracle_pair_resolver_test___init___data() #attribute_name_expected_result_pairs
    
    resolver1 = OraclePairResolver(*args, **kwargs)
    resolver2 = OraclePairResolver(*args, **kwargs)
    expected_result = (True, None)
    data.append((resolver1, resolver2, expected_result))
    
    generator_configuration = Mapping([("witness_field42", "witness_value42"),
                                             ("pair_filter", Mapping({"name": "PairFiltersCombination", 
                                                                            "config": Mapping({"filters": [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                                              "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                                              }),
                                                                                                                               ],
                                                                                                     "witness_field6": "witness_value6",
                                                                                                     }),
                                                                            })),
                                             ])
    generator = MentionPairSampleGenerator.from_configuration(generator_configuration)
    args = (generator,)
    resolver3 = OraclePairResolver(*args, **kwargs)
    expected_result = (False, "_generator")
    data.append((resolver1, resolver3, expected_result))
    
    return data

def get_oracle_pair_resolver_test_from_configuration_data():
    generator_configuration = Mapping([("witness_field7", "witness_value7"),
                                             ("pair_filter", Mapping({"name": "PairFiltersCombination", 
                                                                            "config": Mapping({"filters": [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                                              "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                                              }),
                                                                                                                               ],
                                                                                                     "witness_field6": "witness_value6",
                                                                                                     }),
                                                                            })),
                                             ])
    generator_configuration = Mapping({"name": "MentionPairSampleGenerator", "config": generator_configuration})
    generator = create_generator_from_factory_configuration(generator_configuration)
    
    configuration = Mapping({"witness_field9": "witness_value9"})
    expected_result = OraclePairResolver(generator, configuration=configuration)
    
    configuration = Mapping({"generator": generator_configuration,
                               "witness_field9": "witness_value9",
                               })
    
    return configuration, expected_result

def get_oracle_pair_resolver_test_resolve_data():
    data = []
    
    expected_generator_configuration = Mapping(OrderedDict(()))
    expected_generator_configuration = Mapping({"name": "MentionPairSampleGenerator", "config": expected_generator_configuration})
    configuration = Mapping({"generator": expected_generator_configuration,
                                   "witness_field9": "witness_value9",
                                   })
    resolver = OraclePairResolver.from_configuration(configuration)
    
    #sys_mentions = None
    kwargs = {}#{"sys_mentions": sys_mentions}
    
    # Defining the 'reference' document
    ref_document = get_test_document_data()
    #pos_coref_pairs = (((1,9), (14,42)),
    #                   ((27,42), (68,75)),
    #                   )
    #excluded_pairs = ((46,47), (83,84))
    pos_coref_pairs = (((1,9), (14,42)),
                       ((1,9), (46,47)),
                       ((1,9), (83,84)),
                       ((27,42), (68,75)),
                       )
    excluded_pairs = tuple()
    expected_result = CoreferencePartition(mention_extents=tuple(m.extent for m in ref_document.mentions if m.extent not in excluded_pairs))
    for left_extent, right_extent in pos_coref_pairs:
        left_mention = ref_document.extent2mention[left_extent]
        right_mention = ref_document.extent2mention[right_extent]
        expected_result.join(left_mention.extent, right_mention.extent)
    
    # Defining the 'system' document
    sys_document = get_test_document_data()
    
    data.append(((sys_document, ref_document), resolver, kwargs, expected_result))
    
    return data



def get_oracle_pair_score_resolver_test___init___data():
    attribute_name_expected_result_pairs = []
    
    with_singleton_features = False
    with_anaphoricity_features = False
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                             "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping({"witness_field2": "witness_value2"})}), 
                                                                                                                      "with_singleton_features": with_singleton_features, 
                                                                                                                      "with_anaphoricity_features": with_anaphoricity_features, 
                                                                                                                      "quantize": True, 
                                                                                                                      "strict": True,
                                                                                                                      "useless_field": "useless_value"})
                                                                                            }),
                                             "classifier": Mapping({"name": "LogisticRegressionSKLEARNBinaryClassifier", 
                                                                    "config": DEFAULT_LOGISTIC_REGRESSION_SKLEARN_BINARY_CLASSIFIER_CONFIG,
                                                                    }), 
                                             "witness_field0": "witness_value0",
                                             })
    model_interface = create_model_interface_from_factory_configuration(Mapping((("name", "sklearn"), ("config", model_interface_configuration))))
    attribute_name_expected_result_pairs.append(("_model_interface",model_interface))
    
    generator_configuration = Mapping([("pair_filter", Mapping([("name", "PairFiltersCombination"), 
                                                                ("config", Mapping([("filters", [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                                    "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                                    }),
                                                                                                                     ]),
                                                                                          ("witness_field6", "witness_value6"),
                                                                                          ])
                                                                 ),
                                                                ])
                                      ),
                                      ("witness_field7", "witness_value7"),
                                      ])
    generator = MentionPairSampleGenerator.from_configuration(generator_configuration)
    attribute_name_expected_result_pairs.append(("_generator",generator))
    
    decoder_configuration = Mapping()
    decoder = OracleAggressiveMergeCoreferenceDecoder.from_configuration(decoder_configuration)
    attribute_name_expected_result_pairs.append(("_decoder",decoder))
    
    configuration = Mapping({"witness_field9": "witness_value9"})
    
    
    expected_model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                                      "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping({"witness_field2": "witness_value2"})}), 
                                                                                                                               "with_singleton_features": with_singleton_features, 
                                                                                                                               "with_anaphoricity_features": with_anaphoricity_features, 
                                                                                                                               "quantize": True, 
                                                                                                                               "strict": True,
                                                                                                                               "useless_field": "useless_value"})
                                                                                                      }),
                                                      "classifier": Mapping({"name": "LogisticRegressionSKLEARNBinaryClassifier", 
                                                                    "config": DEFAULT_LOGISTIC_REGRESSION_SKLEARN_BINARY_CLASSIFIER_CONFIG,
                                                                    }), 
                                                      "witness_field0": "witness_value0",
                                                      })
    expected_model_interface_configuration = Mapping({"name": "SKLEARNInterface", "config": expected_model_interface_configuration})
    expected_generator_configuration = Mapping([("witness_field7", "witness_value7"),
                                                  ("pair_filter", Mapping({"name": "PairFiltersCombination", 
                                                                                 "config": Mapping({"filters": [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                                                   "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                                                   }),
                                                                                                                                    ],
                                                                                                          "witness_field6": "witness_value6",
                                                                                                          }),
                                                                                 })
                                                   ),
                                                  ])
    expected_generator_configuration = Mapping({"name": "MentionPairSampleGenerator", "config": expected_generator_configuration})
    expected_decoder_configuration = Mapping({"name": "OracleAggressiveMergeCoreferenceDecoder", "config": Mapping()})
    expected_configuration = Mapping({"model_interface": expected_model_interface_configuration,
                                      "generator": expected_generator_configuration,
                                      "decoder": expected_decoder_configuration, 
                                      "witness_field9": "witness_value9",
                                      })
    
    attribute_name_expected_result_pairs.append(("configuration", expected_configuration))
    
    args = (model_interface, generator, decoder)
    kwargs = {"configuration": configuration}
    
    return (args, kwargs), attribute_name_expected_result_pairs

def get_oracle_pair_score_resolver_test__inner_eq_data():
    data = []
    
    (args, kwargs), _ = get_oracle_pair_score_resolver_test___init___data() #attribute_name_expected_result_pairs
    
    resolver1 = OraclePairScoreResolver(*args, **kwargs)
    resolver2 = OraclePairScoreResolver(*args, **kwargs)
    expected_result = (True, None)
    data.append((resolver1, resolver2, expected_result))
    
    current_configuration = kwargs["configuration"]
    new_configuration = Mapping(object.__getattribute__(current_configuration, "data"))
    new_configuration.witness_field9 = "None"
    kwargs["configuration"] = new_configuration
    resolver3 = OraclePairScoreResolver(*args, **kwargs)
    expected_result = (False, "configuration")
    data.append((resolver1, resolver3, expected_result))
    
    return data

def get_oracle_pair_score_resolver_test_from_configuration_data():
    with_singleton_features = False
    with_anaphoricity_features = False
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                              "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping({"witness_field2": "witness_value2"})}), 
                                                                                                                       "with_singleton_features": with_singleton_features, 
                                                                                                                       "with_anaphoricity_features": with_anaphoricity_features, 
                                                                                                                       "quantize": True, 
                                                                                                                       "strict": True,
                                                                                                                       "useless_field": "useless_value"})
                                                                                             }),
                                             "classifier": Mapping({"name": "LogisticRegressionSKLEARNBinaryClassifier", 
                                                                    "config": DEFAULT_LOGISTIC_REGRESSION_SKLEARN_BINARY_CLASSIFIER_CONFIG,
                                                                    }), 
                                             "witness_field0": "witness_value0",
                                             })
    model_interface_configuration = Mapping({"name": "SKLEARNInterface", "config": model_interface_configuration})
    model_interface = create_model_interface_from_factory_configuration(model_interface_configuration)
    
    generator_configuration = Mapping([("witness_field7", "witness_value7"),
                                         ("pair_filter", Mapping({"name": "PairFiltersCombination", 
                                                                        "config": Mapping({"filters": [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                                                          "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                                                          }),
                                                                                                                           ],
                                                                                                 "witness_field6": "witness_value6",
                                                                                             }),
                                                                        })
                                          ),
                                         ])
    generator_configuration = Mapping({"name": "MentionPairSampleGenerator", "config": generator_configuration})
    generator = create_generator_from_factory_configuration(generator_configuration)
    
    decoder_configuration = Mapping()
    decoder_configuration = Mapping({"name": "OracleAggressiveMergeCoreferenceDecoder", "config": decoder_configuration})
    decoder = create_decoder_from_factory_configuration(decoder_configuration)
    
    configuration = Mapping({"witness_field9": "witness_value9"})
    expected_result = OraclePairScoreResolver(model_interface, generator, decoder, configuration=configuration)
    
    configuration = Mapping({"model_interface": model_interface_configuration,
                             "generator": generator_configuration,
                             "decoder": decoder_configuration, 
                             "witness_field9": "witness_value9",
                             })
    
    return configuration, expected_result

def get_oracle_pair_score_resolver_test_resolve_data():
    data = []
    
    #entity_heads = None
    with_singleton_features = False
    with_anaphoricity_features = False
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                             "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping({"witness_field2": "witness_value2"})}), 
                                                                                                                       "with_singleton_features": with_singleton_features, 
                                                                                                                       "with_anaphoricity_features": with_anaphoricity_features, 
                                                                                                                       "quantize": True, 
                                                                                                                       "strict": True,
                                                                                                                       "useless_field": "useless_value"})
                                                                                             }),
                                             "classifier": Mapping({"name": "LogisticRegressionSKLEARNBinaryClassifier", 
                                                                    "config": DEFAULT_LOGISTIC_REGRESSION_SKLEARN_BINARY_CLASSIFIER_CONFIG,
                                                                    }), 
                                             })
    model_interface_configuration = Mapping({"name": "SKLEARNInterface", "config": model_interface_configuration})
    model_interface_folder_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "pairs", "train_test_sklearn_model_interface")
    generator_configuration = Mapping((("pair_hierarchy", Mapping({"name": "PairGramTypeHierarchy"})),))
    generator_configuration = Mapping({"name": "MentionPairSampleGenerator", "config": generator_configuration})
    decoder_configuration = Mapping()
    decoder_configuration = Mapping({"name": "OracleAggressiveMergeCoreferenceDecoder", "config": decoder_configuration})
    configuration = Mapping({"model_interface": model_interface_configuration,
                             "generator": generator_configuration,
                             "decoder": decoder_configuration, 
                             "witness_field9": "witness_value9",
                            })
    resolver = OraclePairScoreResolver.from_configuration(configuration)
    resolver._model_interface.load_model(model_interface_folder_path)
    
    test_document = get_memoized_test_document_data()
    kwargs = {}#{"entity_head_extents": entity_heads}
    pos_coref_pairs = (((27,42), (68,75)),
                       ((1,9), (14,42)),
                       ((46,47), (83,84)),
                       ((1,9), (46,47)),
                       )
    expected_result = CoreferencePartition(mention_extents=tuple(m.extent for m in test_document.mentions))
    for left_extent, right_extent in pos_coref_pairs:
        left_mention = test_document.extent2mention[left_extent]
        right_mention = test_document.extent2mention[right_extent]
        expected_result.join(left_mention.extent, right_mention.extent)
    
    data.append((test_document, resolver, kwargs, expected_result))
    
    return data


'''
model_folder_path = os.path.join(os.path.dirname(__file__), "data", "pairs", "train_test_sklearn_model_interface")
resolver._model_interface.save_model(model_folder_path)
'''


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()