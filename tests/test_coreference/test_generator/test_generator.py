# -*- coding: utf-8 -*-

import unittest

from cortex.coreference.generator import (create_generator_from_factory_configuration, MentionPairSampleGenerator, SoonGenerator, 
                                    NgCardieGenerator, ExtendedMentionPairSampleGenerator,
                                    )
from cortex.tools import Mapping

class Test(unittest.TestCase):

    def test_create_generator_from_factory_configuration(self):
        # Test parameters
        data = get_test_create_generator_from_factory_configuration_data()
        
        # Test
        for configuration, expected_result in data:
            actual_result = create_generator_from_factory_configuration(configuration)
            self.assertTrue(*expected_result._inner_eq(actual_result))

def get_test_create_generator_from_factory_configuration_data():
    data = []
    
    # MentionPairSampleGenerator
    configuration = Mapping([("witness_field7", "witness_value7"),
                           ("pair_filter", Mapping({"name": "PairFiltersCombination", 
                                                  "config": Mapping({"filters": [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                        "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                            }),
                                                                                             ],
                                                                   "witness_field6": "witness_value6",
                                                                   }),
                                                  })
                            ),
                           ])
    expected_result = MentionPairSampleGenerator.from_configuration(configuration)
    configuration = Mapping({"name": "mentionpair", "config": configuration})
    data.append((configuration, expected_result))
    
    # SoonGenerator
    configuration = Mapping([("pair_filter", Mapping({"name": "PairFiltersCombination", 
                                                      "config": Mapping({"filters": [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                            "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                            }),
                                                                                                 ],
                                                                       "witness_field6": "witness_value6",
                                                                       }),
                                                      })
                              ),
                           ("witness_field7", "witness_value7"),
                           ])
    expected_result = SoonGenerator.from_configuration(configuration)
    configuration = Mapping({"name": "soon", "config": configuration})
    data.append((configuration, expected_result))
    
    # NgCardieGenerator
    configuration = Mapping([("pair_filter", Mapping({"name": "PairFiltersCombination", 
                                                      "config": Mapping({"filters": [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                            "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                            }),
                                                                                                   ],
                                                                         "witness_field6": "witness_value6",
                                                                         }),
                                                      })
                              ),
                           ("witness_field7", "witness_value7"),
                           ])
    expected_result = NgCardieGenerator.from_configuration(configuration)
    configuration = Mapping({"name": "ngcardie", "config": configuration})
    data.append((configuration, expected_result))
    
    # ExtendedMentionPairSampleGenerator
    configuration = Mapping([("witness_field7", "witness_value7"),
                           ("pair_filter", Mapping({"name": "PairFiltersCombination", 
                                                  "config": Mapping({"filters": [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                        "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                            }),
                                                                                             ],
                                                                   "witness_field6": "witness_value6",
                                                                   }),
                                                  })
                            ),
                           ])
    expected_result = ExtendedMentionPairSampleGenerator.from_configuration(configuration)
    configuration = Mapping({"name": "extendedmentionpair", "config": configuration})
    data.append((configuration, expected_result))
    
    return data


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()