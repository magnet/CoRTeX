# -*- coding: utf-8 -*-

import unittest
import itertools

from cortex.api.markable import NULL_MENTION
from cortex.utils.memoize import Memoized
from cortex.tools import Mapping
from cortex.coreference.generator.pairs import (MentionPairSampleGenerator, SoonGenerator, 
                                                NgCardieGenerator, ExtendedMentionPairSampleGenerator, )
from cortex.learning.samples import POS_CLASS, NEG_CLASS
from cortex.learning.filter.pairs import AcceptAllPairFilter, PairFiltersCombination, RecallPairFilter


#@unittest.skip
class TestMentionPairSampleGenerator(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        (args, kwargs), attribute_name_expected_result_pairs = get_mention_pair_generator_test___init___data()
        
        # Test
        mention_pair_generator = MentionPairSampleGenerator(*args, **kwargs)
        for attribute_name, expected_result in attribute_name_expected_result_pairs:
            self.assertEqual(expected_result, getattr(mention_pair_generator, attribute_name), attribute_name)
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        data = get_mention_pair_generator_test__inner_eq_data()
        
        # Test
        for generator1, generator2, expected_result in data:
            self.assertEqual(expected_result, generator1._inner_eq(generator2))
    
    #@unittest.skip
    def test_from_configuration(self):
        # Test parameters
        configuration, expected_result = get_mention_pair_generator_test_from_configuration_data()
        
        # Test
        actual_result = MentionPairSampleGenerator.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = MentionPairSampleGenerator
        generator_pair_filter_factory_configuration = RecallPairFilter.define_factory_configuration(max_mention_distance=7.9)
        args = ()
        kwargs = {"pair_filter_factory_configuration": generator_pair_filter_factory_configuration}
        expected_result = Mapping({"pair_filter": generator_pair_filter_factory_configuration,})
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #@unittest.skip
    def test__generate_sample_set(self):
        # Test parameters
        data = get_mention_pair_generator_test__generate_sample_set_data()
        
        # Test
        for test_document, mention_pair_generator, kwargs, expected_result in data:
            mention_pair_samples_iterable = mention_pair_generator._generate_sample_set(test_document, **kwargs)
            mention_pair_samples = tuple(mention_pair_samples_iterable)
            actual_result = tuple((s.left.extent, s.right.extent, s.label) for s in mention_pair_samples)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__generate_training_set(self):
        # Test parameters
        data = get_mention_pair_generator_test__generate_training_set_data()
        
        # Test
        for test_document, mention_pair_generator, expected_result in data:
            mention_pair_samples_iterable = mention_pair_generator._generate_training_set(test_document)
            mention_pair_samples = tuple(mention_pair_samples_iterable)
            actual_result = tuple((s.left.extent, s.right.extent, s.label) for s in mention_pair_samples)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_generate_training_set(self):
        # Test parameters
        test_documents, mention_pair_generator, expected_result = get_mention_pair_generator_test_generate_training_set_data()
        
        # Test
        mention_pair_samples_iterable = mention_pair_generator.generate_training_set(test_documents)
        mention_pair_samples = tuple(mention_pair_samples_iterable)
        actual_result = tuple((s.left.extent, s.right.extent, s.label) for s in mention_pair_samples)
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_generate_prediction_set(self):
        # Test parameters
        data = get_mention_pair_generator_test_generate_prediction_set_data()
        
        # Test
        for test_document, mention_pair_generator, expected_result in data:
            mention_pair_samples_iterable = mention_pair_generator.generate_prediction_set(test_document)
            mention_pair_samples = tuple(mention_pair_samples_iterable)
            actual_result = tuple((s.left.extent, s.right.extent, s.label) for s in mention_pair_samples)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_generate_sample_from_mention_pair(self):
        # Test parameters
        data = get_mention_pair_generator_test_generate_sample_from_mention_pair_data()
        
        # Test
        for generator, mention_pair, kwargs, expected_result in data:
            mention_pair_sample = generator.generate_sample_from_mention_pair(*mention_pair, **kwargs)
            actual_result = mention_pair_sample
            if expected_result is not None:
                actual_result = (mention_pair_sample.left.extent, mention_pair_sample.right.extent, mention_pair_sample.label)
            self.assertEqual(expected_result, actual_result)
    


#@unittest.skip
class TestSoonGenerator(unittest.TestCase):
    
    #@unittest.skip
    def test__generate_sample_set(self):
        # Test parameters
        data = get_soon_generator_test__generate_sample_set_data()
        
        # Test
        for test_document, mention_pair_generator, kwargs, expected_result in data:
            mention_pair_samples_iterable = mention_pair_generator._generate_sample_set(test_document, **kwargs)
            mention_pair_samples = tuple(mention_pair_samples_iterable)
            actual_result = tuple((s.left.extent, s.right.extent, s.label) for s in mention_pair_samples)
            self.assertEqual(expected_result, actual_result)



#@unittest.skip
class TestNgCardieGenerator(unittest.TestCase):
    
    #@unittest.skip
    def test__generate_sample_set(self):
        # Test parameters
        data = get_ng_cardie_generator_test__generate_sample_set_data()
        
        # Test
        for test_document, mention_pair_generator, kwargs, expected_result in data:
            mention_pair_samples_iterable = mention_pair_generator._generate_sample_set(test_document, **kwargs)
            mention_pair_samples = tuple(mention_pair_samples_iterable)
            actual_result = tuple((s.left.extent, s.right.extent, s.label) for s in mention_pair_samples)
            self.assertEqual(expected_result, actual_result)



#@unittest.skip
class TestExtendedMentionPairSampleGenerator(unittest.TestCase):
    
    #@unittest.skip
    def test__generate_sample_set(self):
        # Test parameters
        data = get_structured_mention_pair_generator_test__generate_sample_set_data()
        
        # Test
        for test_document, mention_pair_generator, kwargs, expected_result in data:
            mention_pair_samples_iterable = mention_pair_generator._generate_sample_set(test_document, **kwargs)
            mention_pair_samples = tuple(mention_pair_samples_iterable)
            self.assertTrue(len(mention_pair_samples) >= 1)
            actual_result = tuple((s.left.extent, s.right.extent, s.label) for s in mention_pair_samples)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_generate_sample_from_mention_pair(self):
        # Test parameters
        data = get_structured_mention_pair_generator_test_generate_sample_from_mention_pair_data()
        
        # Test
        for generator, mention_pair, kwargs, expected_result in data:
            mention_pair_sample = generator.generate_sample_from_mention_pair(*mention_pair, **kwargs)
            actual_result = (mention_pair_sample.left.extent, mention_pair_sample.right.extent, mention_pair_sample.label)
            self.assertEqual(expected_result, actual_result)



@Memoized
def get_test_document_data():
    import os
    from cortex.io.pivot_reader import PivotReader
    
    root_folder_path, file_name = os.path.split(__file__)
    file_name, _ = os.path.splitext(file_name)
    folder_path = os.path.join(root_folder_path, "{}_data".format(file_name))
    document_folder_path = os.path.join(folder_path, "mention_pair_generator_test_data", "ref")
    
    strict = True # We want to synchronize head tokens.
    test_document = PivotReader.parse(document_folder_path, strict=strict)
    
    return test_document


def get_mention_pair_generator_test___init___data():
    attribute_name_expected_result_pairs = []
    
    pair_filters = (AcceptAllPairFilter.from_configuration(Mapping({"witness_field5": "witness_value5"})),
                    )
    pair_filter = PairFiltersCombination(pair_filters, configuration=Mapping({"witness_field6": "witness_value6"}))
    attribute_name_expected_result_pairs.append(("pair_filter",pair_filter))
    
    configuration = Mapping({"witness_field7": "witness_value7"})
    
    expected_configuration = Mapping([("witness_field7", "witness_value7"),
                                    ("pair_filter", Mapping({"name": "PairFiltersCombination", 
                                                           "config": Mapping({"filters": [Mapping({"name": "AcceptAllPairFilter", 
                                                                                                     "config": Mapping({"witness_field5": "witness_value5"})
                                                                                                     }),
                                                                                            ],
                                                                              "witness_field6": "witness_value6",
                                                                              }),
                                                             })
                                     ),
                                  ],)
    attribute_name_expected_result_pairs.append(("configuration", expected_configuration))
    
    args = tuple()
    kwargs = {"pair_filter": pair_filter, "configuration": configuration}
    
    return (args, kwargs), attribute_name_expected_result_pairs

def get_mention_pair_generator_test__inner_eq_data():
    data = []
    
    (args, kwargs), _ = get_mention_pair_generator_test___init___data() #attribute_name_expected_result_pairs
    
    generator1 = MentionPairSampleGenerator(*args, **kwargs)
    generator2 = MentionPairSampleGenerator(*args, **kwargs)
    expected_result = (True, None)
    data.append((generator1, generator2, expected_result))
    
    kwargs["pair_filter"] = None
    generator3 = MentionPairSampleGenerator(*args, **kwargs)
    expected_result = (False, "pair_filter")
    data.append((generator1, generator3, expected_result))
    
    return data

def get_mention_pair_generator_test_from_configuration_data():
    pair_filter = PairFiltersCombination((AcceptAllPairFilter.from_configuration(Mapping({"witness_field5": "witness_value5"})),), 
                                         configuration=Mapping({"witness_field6": "witness_value6"}))
    configuration = Mapping({"witness_field7": "witness_value7"})
    args = tuple()
    kwargs = {"pair_filter": pair_filter, "configuration": configuration}
    
    configuration = Mapping([("witness_field7", "witness_value7"),
                           ("pair_filter", Mapping({"name": "PairFiltersCombination", 
                                                  "config": Mapping({"filters": [Mapping({"name": "AcceptAllPairFilter", 
                                                                                          "config": Mapping({"witness_field5": "witness_value5"})
                                                                                         }),
                                                                               ],
                                                                     "witness_field6": "witness_value6",
                                                                     }),
                                                    })
                            ),
                           ])
    expected_result = MentionPairSampleGenerator(*args, **kwargs)
    
    return configuration, expected_result    

def get_mention_pair_generator_test__generate_sample_set_data():
    test_document = get_test_document_data()
    
    data = []
    
    # train = True
    #mentions = [(1,9), (14,42), (27,42), (46,47), (68,75), (114,120)] # (83,84)
    config_dict = {"pair_filter": Mapping({"name": "acceptall"}),}
    configuration = Mapping(config_dict)
    mention_pair_generator = MentionPairSampleGenerator.from_configuration(configuration)
    kwargs = {"train": True, "entity_head_extents": None, "singleton_extents": None} 
    expected_result = (((1,9), (14,42), POS_CLASS),
                       ((14,42), (27,42), NEG_CLASS),
                       ((1,9), (27,42), NEG_CLASS),
                       ((27,42), (46,47), NEG_CLASS),
                       ((14,42), (46,47), POS_CLASS),
                       ((1,9), (46,47), POS_CLASS),
                       ((46,47), (68,75), NEG_CLASS),
                       ((27,42), (68,75), POS_CLASS),
                       ((14,42), (68,75), NEG_CLASS),
                       ((1,9), (68,75), NEG_CLASS),
                       
                       ((68,75), (83,84), NEG_CLASS),
                       ((46,47), (83,84), POS_CLASS),
                       ((27,42), (83,84), NEG_CLASS),
                       ((14,42), (83,84), POS_CLASS),
                       ((1,9), (83,84), POS_CLASS),
                       ((83,84), (114,120), NEG_CLASS),
                       
                       ((68,75), (114,120), NEG_CLASS),
                       ((46,47), (114,120), NEG_CLASS),
                       ((27,42), (114,120), NEG_CLASS),
                       ((14,42), (114,120), NEG_CLASS),
                       ((1,9), (114,120), NEG_CLASS),
                       )
    data.append((test_document, mention_pair_generator, kwargs, expected_result))
    
    # train = False
    #mentions = [(1,9), (14,42), (27,42), (46,47), (68,75), (114,120)] # (83,84)
    config_dict = {"pair_filter": Mapping({"name": "acceptall"}),}
    configuration = Mapping(config_dict)
    mention_pair_generator = MentionPairSampleGenerator.from_configuration(configuration)
    kwargs = {"train": False, "entity_head_extents": None, "singleton_extents": None} 
    expected_result = (((1,9), (14,42), None),
                       ((14,42), (27,42), None),
                       ((1,9), (27,42), None),
                       ((27,42), (46,47), None),
                       ((14,42), (46,47), None),
                       ((1,9), (46,47), None),
                       ((46,47), (68,75), None),
                       ((27,42), (68,75), None),
                       ((14,42), (68,75), None),
                       ((1,9), (68,75), None),
                       
                       ((68,75), (83,84), None),
                       ((46,47), (83,84), None),
                       ((27,42), (83,84), None),
                       ((14,42), (83,84), None),
                       ((1,9), (83,84), None),
                       ((83,84), (114,120), None),
                       
                       ((68,75), (114,120), None),
                       ((46,47), (114,120), None),
                       ((27,42), (114,120), None),
                       ((14,42), (114,120), None),
                       ((1,9), (114,120), None),
                       )
    data.append((test_document, mention_pair_generator, kwargs, expected_result))
    
    return data

def get_mention_pair_generator_test__generate_training_set_data():
    data0 = get_mention_pair_generator_test__generate_sample_set_data()
    data0 = data0[0:1]
    
    data = []
    for test_document, mention_pair_generator, _, expected_result in data0: #kwargs
        data.append((test_document, mention_pair_generator, expected_result))
    
    return data

def get_mention_pair_generator_test_generate_training_set_data():
    data = get_mention_pair_generator_test__generate_training_set_data()
    
    test_documents = tuple(d[0] for d in data) * 2
    mention_pair_generator = data[0][1]
    expected_result = tuple(itertools.chain(*tuple(d[2] for d in data))) * 2
    
    return test_documents, mention_pair_generator, expected_result


def get_mention_pair_generator_test_generate_prediction_set_data():
    data0 = get_mention_pair_generator_test__generate_sample_set_data()
    data0 = data0[1:]
    
    data = []
    for test_document, mention_pair_generator, _, expected_result in data0: #kwargs
        data.append((test_document, mention_pair_generator, expected_result))
    
    return data


def get_mention_pair_generator_test_generate_sample_from_mention_pair_data():
    test_document = get_test_document_data()
    
    data = []
    
    # train = True
    #mentions = [(1,9), (14,42), (27,42), (46,47), (68,75), (114,120)] # (83,84)
    config_dict = {"pair_filter": Mapping({"name": "acceptall"}),}
    configuration = Mapping(config_dict)
    mention_pair_generator = MentionPairSampleGenerator.from_configuration(configuration)
    kwargs = {"train": True, "entity_head_extents": None, "singleton_extents": None}
    mention_pair = tuple(test_document.extent2mention[extent] for extent in ((1,9),(14,42)))
    expected_result = ((1,9), (14,42), POS_CLASS)
    data.append((mention_pair_generator, mention_pair, kwargs, expected_result))
    mention_pair = tuple(test_document.extent2mention[extent] for extent in ((46,47),(68,75)))
    expected_result = ((46,47), (68,75), NEG_CLASS)
    data.append((mention_pair_generator, mention_pair, kwargs, expected_result))
    mention_pair = (NULL_MENTION, test_document.extent2mention[(114,120)])
    expected_result = None#(NULL_MENTION.extent, (68,75), POS_CLASS)
    #data.append((mention_pair_generator, mention_pair, kwargs, expected_result))
    mention_pair = (NULL_MENTION, test_document.extent2mention[(83,84)])
    expected_result = None#(NULL_MENTION.extent, (83,84), NEG_CLASS)
    #data.append((mention_pair_generator, mention_pair, kwargs, expected_result))
    
    # train = False
    #mentions = [(1,9), (14,42), (27,42), (46,47), (68,75), (114,120)] # (83,84)
    config_dict = {"pair_filter": Mapping({"name": "acceptall"}),}
    configuration = Mapping(config_dict)
    mention_pair_generator = MentionPairSampleGenerator.from_configuration(configuration)
    kwargs = {"train": False, "entity_head_extents": None, "singleton_extents": None}
    mention_pair = tuple(test_document.extent2mention[extent] for extent in ((1,9),(14,42)))
    expected_result = ((1,9), (14,42), None)
    data.append((mention_pair_generator, mention_pair, kwargs, expected_result))
    mention_pair = tuple(test_document.extent2mention[extent] for extent in ((46,47),(68,75)))
    expected_result = ((46,47), (68,75), None)
    data.append((mention_pair_generator, mention_pair, kwargs, expected_result))
    mention_pair = (NULL_MENTION, test_document.extent2mention[(114,120)])
    expected_result = None#(NULL_MENTION.extent, (68,75), None)
    #data.append((mention_pair_generator, mention_pair, kwargs, expected_result))
    mention_pair = (NULL_MENTION, test_document.extent2mention[(83,84)])
    expected_result = None#(NULL_MENTION.extent, (83,84), None)
    #data.append((mention_pair_generator, mention_pair, kwargs, expected_result))
    
    return data
    


def get_soon_generator_test__generate_sample_set_data():
    test_document = get_test_document_data()
    
    data = []
    
    # train = True
    #mentions = [(1,9), (14,42), (27,42), (46,47), (68,75), (114,120)] # (83,84)
    config_dict = {"pair_filter": Mapping({"name": "acceptall"}),}
    configuration = Mapping(config_dict)
    mention_pair_generator = SoonGenerator.from_configuration(configuration)
    kwargs = {"train": True, "entity_head_extents": None, "singleton_extents": None} 
    expected_result = (((1,9), (14,42), POS_CLASS),
                       ((14,42), (27,42), NEG_CLASS),
                       ((1,9), (27,42), NEG_CLASS),
                       ((27,42), (46,47), NEG_CLASS),
                       ((14,42), (46,47), POS_CLASS),
                       #((1,9), (46,47), POS_CLASS), #Removal is due to the functioning of the SoonGenerator compared to the MentionPairSampleGenerator
                       ((46,47), (68,75), NEG_CLASS),
                       ((27,42), (68,75), POS_CLASS),
                       #((14,42), (68,75), NEG_CLASS), #Removal is due to the functioning of the SoonGenerator compared to the MentionPairSampleGenerator
                       #((1,9), (68,75), NEG_CLASS), #Removal is due to the functioning of the SoonGenerator compared to the MentionPairSampleGenerator
                       
                       ((68,75), (83,84), NEG_CLASS),
                       ((46,47), (83,84), POS_CLASS),
                       #((27,42), (83,84), NEG_CLASS), #Removal is due to the functioning of the SoonGenerator compared to the MentionPairSampleGenerator
                       #((14,42), (83,84), POS_CLASS), #Removal is due to the functioning of the SoonGenerator compared to the MentionPairSampleGenerator
                       #((1,9), (83,84), POS_CLASS), #Removal is due to the functioning of the SoonGenerator compared to the MentionPairSampleGenerator
                       ((83,84), (114,120), NEG_CLASS),
                       
                       ((68,75), (114,120), NEG_CLASS),
                       ((46,47), (114,120), NEG_CLASS),
                       ((27,42), (114,120), NEG_CLASS),
                       ((14,42), (114,120), NEG_CLASS),
                       ((1,9), (114,120), NEG_CLASS),
                       )
    data.append((test_document, mention_pair_generator, kwargs, expected_result))
    
    # train = False
    #mentions = [(1,9), (14,42), (27,42), (46,47), (68,75), (114,120)] # (83,84)
    config_dict = {"pair_filter": Mapping({"name": "acceptall"}),}
    configuration = Mapping(config_dict)
    mention_pair_generator = SoonGenerator.from_configuration(configuration)
    kwargs = {"train": False, "entity_head_extents": None, "singleton_extents": None} 
    expected_result = (((1,9), (14,42), None),
                       ((14,42), (27,42), None),
                       ((1,9), (27,42), None),
                       ((27,42), (46,47), None),
                       ((14,42), (46,47), None),
                       ((1,9), (46,47), None), # Not removed when label is unknown
                       ((46,47), (68,75), None),
                       ((27,42), (68,75), None),
                       ((14,42), (68,75), None), # Not removed when label is unknown
                       ((1,9), (68,75), None), # Not removed when label is unknown
                       
                       ((68,75), (83,84), None),
                       ((46,47), (83,84), None),
                       ((27,42), (83,84), None), # Not removed when label is unknown
                       ((14,42), (83,84), None), # Not removed when label is unknown
                       ((1,9), (83,84), None), # Not removed when label is unknown
                       ((83,84), (114,120), None),
                       
                       ((68,75), (114,120), None),
                       ((46,47), (114,120), None),
                       ((27,42), (114,120), None),
                       ((14,42), (114,120), None),
                       ((1,9), (114,120), None),
                       )
    data.append((test_document, mention_pair_generator, kwargs, expected_result))
    
    return data



def get_ng_cardie_generator_test__generate_sample_set_data():
    test_document = get_test_document_data()
    
    data = []
    
    # (46,47) and (82,83) are pronouns
    # train = True
    #mentions = [(1,9), (14,42), (27,42), (46,47), (68,75), (114,120)] # (83,84)
    config_dict = {"pair_filter": Mapping({"name": "acceptall"}),}
    configuration = Mapping(config_dict)
    mention_pair_generator = NgCardieGenerator.from_configuration(configuration)
    kwargs = {"train": True, "entity_head_extents": None, "singleton_extents": None} 
    expected_result = (((1,9), (14,42), POS_CLASS),
                       ((14,42), (27,42), NEG_CLASS),
                       ((1,9), (27,42), NEG_CLASS),
                       ((27,42), (46,47), NEG_CLASS),
                       ((14,42), (46,47), POS_CLASS),
                       #((1,9), (46,47), POS_CLASS), # Removal is due to the functioning of the SoonGenerator compared to the MentionPairSampleGenerator
                       #((46,47), (68,75), NEG_CLASS), # Removal is due to the functioning of the NgCardieGenerator compared to the SoonGenerator
                       ((27,42), (68,75), POS_CLASS),
                       #((14,42), (68,75), NEG_CLASS), # Removal is due to the functioning of the SoonGenerator compared to the MentionPairSampleGenerator
                       #((1,9), (68,75), NEG_CLASS), # Removal is due to the functioning of the SoonGenerator compared to the MentionPairSampleGenerator
                       
                       ((68,75), (83,84), NEG_CLASS),
                       ((46,47), (83,84), POS_CLASS),
                       #((27,42), (83,84), NEG_CLASS), # Removal is due to the functioning of the SoonGenerator compared to the MentionPairSampleGenerator
                       #((14,42), (83,84), POS_CLASS), # Removal is due to the functioning of the SoonGenerator compared to the MentionPairSampleGenerator
                       #((1,9), (83,84), POS_CLASS), # Removal is due to the functioning of the SoonGenerator compared to the MentionPairSampleGenerator
                       #((83,84), (114,120), NEG_CLASS), # Removal is due to the functioning of the NgCardieGenerator compared to the SoonGenerator
                       
                       ((68,75), (114,120), NEG_CLASS),
                       #((46,47), (114,120), NEG_CLASS), # Removal is due to the functioning of the NgCardieGenerator compared to the SoonGenerator
                       ((27,42), (114,120), NEG_CLASS),
                       ((14,42), (114,120), NEG_CLASS),
                       ((1,9), (114,120), NEG_CLASS),
                       )
    data.append((test_document, mention_pair_generator, kwargs, expected_result))
    
    # train = True
    #mentions = [(1,9), (14,42), (27,42), (46,47), (68,75), (114,120)] # (83,84)
    config_dict = {"pair_filter": Mapping({"name": "acceptall"}),}
    configuration = Mapping(config_dict)
    mention_pair_generator = NgCardieGenerator.from_configuration(configuration)
    kwargs = {"train": False, "entity_head_extents": None, "singleton_extents": None} 
    expected_result = (((1,9), (14,42), None),
                       ((14,42), (27,42), None),
                       ((1,9), (27,42), None),
                       ((27,42), (46,47), None),
                       ((14,42), (46,47), None),
                       ((1,9), (46,47), None), # # Not removed when label is unknown
                       #((46,47), (68,75), None), # Removal is due to the functioning of the NgCardieGenerator compared to the SoonGenerator
                       ((27,42), (68,75), None),
                       ((14,42), (68,75), None), # # Not removed when label is unknown
                       ((1,9), (68,75), None), # # Not removed when label is unknown
                       
                       ((68,75), (83,84), None),
                       ((46,47), (83,84), None),
                       ((27,42), (83,84), None), # # Not removed when label is unknown
                       ((14,42), (83,84), None), # # Not removed when label is unknown
                       ((1,9), (83,84), None), # # Not removed when label is unknown
                       #((83,84), (114,120), None), # Removal is due to the functioning of the NgCardieGenerator compared to the SoonGenerator
                       
                       ((68,75), (114,120), None),
                       #((46,47), (114,120), None), # Removal is due to the functioning of the NgCardieGenerator compared to the SoonGenerator
                       ((27,42), (114,120), None),
                       ((14,42), (114,120), None),
                       ((1,9), (114,120), None),
                       )
    data.append((test_document, mention_pair_generator, kwargs, expected_result))
    
    return data



def get_structured_mention_pair_generator_test__generate_sample_set_data():
    test_document = get_test_document_data()
    nm_extent = NULL_MENTION.extent
    
    data = []
    
    # train = True 
    #mentions = [(1,9), (14,42), (27,42), (46,47), (68,75), (114,120)] # (83,84)
    config_dict = {"pair_filter": Mapping({"name": "acceptall"}),}
    configuration = Mapping(config_dict)
    mention_pair_generator = ExtendedMentionPairSampleGenerator.from_configuration(configuration)
    kwargs = {"train": True, "entity_head_extents": None, "singleton_extents": None} 
    expected_result = ((nm_extent, (1,9), POS_CLASS), 
                       ((1,9), (14,42), POS_CLASS),
                       (nm_extent, (14,42), NEG_CLASS), 
                       ((14,42), (27,42), NEG_CLASS),
                       ((1,9), (27,42), NEG_CLASS),
                       (nm_extent, (27,42), POS_CLASS), 
                       ((27,42), (46,47), NEG_CLASS),
                       ((14,42), (46,47), POS_CLASS),
                       ((1,9), (46,47), POS_CLASS),
                       (nm_extent, (46,47), NEG_CLASS), 
                       ((46,47), (68,75), NEG_CLASS),
                       ((27,42), (68,75), POS_CLASS),
                       ((14,42), (68,75), NEG_CLASS),
                       ((1,9), (68,75), NEG_CLASS),
                       (nm_extent, (68,75), NEG_CLASS), 
                       
                       ((68,75), (83,84), NEG_CLASS),
                       ((46,47), (83,84), POS_CLASS),
                       ((27,42), (83,84), NEG_CLASS),
                       ((14,42), (83,84), POS_CLASS),
                       ((1,9), (83,84), POS_CLASS),
                       (nm_extent, (83,84), NEG_CLASS), 
                       ((83,84), (114,120), NEG_CLASS),
                       
                       ((68,75), (114,120), NEG_CLASS),
                       ((46,47), (114,120), NEG_CLASS),
                       ((27,42), (114,120), NEG_CLASS),
                       ((14,42), (114,120), NEG_CLASS),
                       ((1,9), (114,120), NEG_CLASS),
                       (nm_extent, (114,120), POS_CLASS), 
                       )
    data.append((test_document, mention_pair_generator, kwargs, expected_result))
    
    # train = False
    #mentions = [(1,9), (14,42), (27,42), (46,47), (68,75), (114,120)] # (83,84)
    config_dict = {"pair_filter": Mapping({"name": "acceptall"}),}
    configuration = Mapping(config_dict)
    mention_pair_generator = ExtendedMentionPairSampleGenerator.from_configuration(configuration)
    kwargs = {"train": False, "entity_head_extents": None, "singleton_extents": None} 
    expected_result = ((nm_extent, (1,9), None), 
                       ((1,9), (14,42), None),
                       (nm_extent, (14,42), None), 
                       ((14,42), (27,42), None),
                       ((1,9), (27,42), None),
                       (nm_extent, (27,42), None), 
                       ((27,42), (46,47), None),
                       ((14,42), (46,47), None),
                       ((1,9), (46,47), None),
                       (nm_extent, (46,47), None), 
                       ((46,47), (68,75), None),
                       ((27,42), (68,75), None),
                       ((14,42), (68,75), None),
                       ((1,9), (68,75), None),
                       (nm_extent, (68,75), None), 
                       
                       ((68,75), (83,84), None),
                       ((46,47), (83,84), None),
                       ((27,42), (83,84), None),
                       ((14,42), (83,84), None),
                       ((1,9), (83,84), None),
                       (nm_extent, (83,84), None), 
                       ((83,84), (114,120), None),
                       
                       ((68,75), (114,120), None),
                       ((46,47), (114,120), None),
                       ((27,42), (114,120), None),
                       ((14,42), (114,120), None),
                       ((1,9), (114,120), None),
                       (nm_extent, (114,120), None), 
                       )
    data.append((test_document, mention_pair_generator, kwargs, expected_result))
    
    return data


def get_structured_mention_pair_generator_test_generate_sample_from_mention_pair_data():
    test_document = get_test_document_data()
    
    data = []
    
    # train = True
    #mentions = [(1,9), (14,42), (27,42), (46,47), (68,75), (114,120)] # (83,84)
    config_dict = {"pair_filter": Mapping({"name": "acceptall"}),}
    configuration = Mapping(config_dict)
    mention_pair_generator = ExtendedMentionPairSampleGenerator.from_configuration(configuration)
    kwargs = {"train": True, "entity_head_extents": None, "singleton_extents": None}
    mention_pair = tuple(test_document.extent2mention[extent] for extent in ((1,9),(14,42)))
    expected_result = ((1,9), (14,42), POS_CLASS)
    data.append((mention_pair_generator, mention_pair, kwargs, expected_result))
    mention_pair = tuple(test_document.extent2mention[extent] for extent in ((46,47),(68,75)))
    expected_result = ((46,47), (68,75), NEG_CLASS)
    data.append((mention_pair_generator, mention_pair, kwargs, expected_result))
    mention_pair = (NULL_MENTION, test_document.extent2mention[(114,120)])
    expected_result = (NULL_MENTION.extent, (114,120), POS_CLASS)
    data.append((mention_pair_generator, mention_pair, kwargs, expected_result))
    mention_pair = (NULL_MENTION, test_document.extent2mention[(83,84)])
    expected_result = (NULL_MENTION.extent, (83,84), NEG_CLASS)
    data.append((mention_pair_generator, mention_pair, kwargs, expected_result))
    
    # train = False
    #mentions = [(1,9), (14,42), (27,42), (46,47), (68,75), (114,120)] # (83,84)
    config_dict = {"pair_filter": Mapping({"name": "acceptall"}),}
    configuration = Mapping(config_dict)
    mention_pair_generator = ExtendedMentionPairSampleGenerator.from_configuration(configuration)
    kwargs = {"train": False, "entity_head_extents": None, "singleton_extents": None}
    mention_pair = tuple(test_document.extent2mention[extent] for extent in ((1,9),(14,42)))
    expected_result = ((1,9), (14,42), None)
    data.append((mention_pair_generator, mention_pair, kwargs, expected_result))
    mention_pair = tuple(test_document.extent2mention[extent] for extent in ((46,47),(68,75)))
    expected_result = ((46,47), (68,75), None)
    data.append((mention_pair_generator, mention_pair, kwargs, expected_result))
    mention_pair = (NULL_MENTION, test_document.extent2mention[(68,75)])
    expected_result = (NULL_MENTION.extent, (68,75), None)
    data.append((mention_pair_generator, mention_pair, kwargs, expected_result))
    mention_pair = (NULL_MENTION, test_document.extent2mention[(83,84)])
    expected_result = (NULL_MENTION.extent, (83,84), None)
    data.append((mention_pair_generator, mention_pair, kwargs, expected_result))
    
    return data


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()