# -*- coding: utf-8 -*-

import unittest

from cortex.api.markable import NULL_MENTION
from cortex.utils.memoize import Memoized
from cortex.api.coreference_partition import CoreferencePartition
from cortex.coreference.decoder.pairs import (ClosestFirstCoreferenceDecoder, AggressiveMergeCoreferenceDecoder, 
                                          OracleAggressiveMergeCoreferenceDecoder, BestFirstCoreferenceDecoder, 
                                          BestNextCoreferenceDecoder, BestLinkCoreferenceDecoder, 
                                          MixedFirstCoreferenceDecoder, NBestFirstCoreferenceDecoder, 
                                          NBestLinkCoreferenceDecoder, PermutationCoreferenceDecoder, 
                                          HACCoreferenceDecoder, ExtendedBestFirstCoreferenceDecoder,
                                          ConstrainedBestFirstCoreferenceDecoder, 
                                          ConstrainedExtendedBestFirstCoreferenceDecoder, 
                                          )
from cortex.tools import Mapping

#decoder.decode
class TestClosestFirstCoreferenceDecoder(unittest.TestCase):
    
    #decoder.decode
    def test___init__(self):
        # Test parameters
        configuration = Mapping({"useless_field": "useless_value"})
        decoder = ClosestFirstCoreferenceDecoder(configuration=configuration)
        
        # Test
        self.assertEqual(configuration, decoder.configuration)
    
    #decoder.decode
    def test__inner_eq(self):
        # Test parameters
        decoder1 = ClosestFirstCoreferenceDecoder(configuration=Mapping({"witness_field": "witness_value"}))
        decoder2 = ClosestFirstCoreferenceDecoder(configuration=Mapping({"witness_field": "witness_value"}))
        decoder3 = ClosestFirstCoreferenceDecoder(configuration=Mapping({"witness_field2": "witness_value2"}))
        
        # Test
        self.assertTrue(*decoder1._inner_eq(decoder2))
        self.assertEqual((False,"configuration"), decoder1._inner_eq(decoder3))
    
    #decoder.decode
    def test_from_configuration(self):
        # Test parameters
        configuration = Mapping({"useless_field": "useless_value"})
        expected_result = ClosestFirstCoreferenceDecoder(configuration=configuration)
        
        # Test
        actual_result = ClosestFirstCoreferenceDecoder.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #decoder.decode
    def test_decode(self):
        # Test parameters
        data = get_closest_first_decoder_decode_test_data()
        
        # Test
        for decoder, (document, coreference_scores, kwargs), expected_result in data:
            actual_result = decoder.decode(document, coreference_scores, **kwargs)
            self.assertEqual(expected_result, actual_result)



#decoder.decode
class TestAggressiveMergeCoreferenceDecoder(unittest.TestCase):
    
    #decoder.decode
    def test_decode(self):
        # Test parameters
        data = get_aggressive_merge_decoder_decode_test_data()
        
        # Test
        for decoder, (document, coreference_scores, kwargs), expected_result in data:
            actual_result = decoder.decode(document, coreference_scores, **kwargs)
            self.assertEqual(expected_result, actual_result)



#decoder.decode
class TestOracleAgressiveMergeCoreferenceDecoder(unittest.TestCase):
    
    #decoder.decode
    def test_decode(self):
        # Test parameters
        data = get_oracle_aggressive_merge_decoder_decode_test_data()
        
        # Test
        for decoder, (document, coreference_scores, ref_coreference_partition, kwargs), expected_result in data:
            actual_result = decoder.decode(document, coreference_scores, ref_coreference_partition, **kwargs)
            self.assertEqual(expected_result, actual_result)



#decoder.decode
class TestBestFirstCoreferenceDecoder(unittest.TestCase):
    
    #decoder.decode
    def test_decode(self):
        # Test parameters
        data = get_best_first_decoder_decode_test_data()
        
        # Test
        for decoder, (document, coreference_scores, kwargs), expected_result in data:
            actual_result = decoder.decode(document, coreference_scores, **kwargs)
            self.assertEqual(expected_result, actual_result)


#decoder.decode
class TestExtendedBestFirstCoreferenceDecoder(unittest.TestCase):
    
    #decoder.decode
    def test_decode(self):
        # Test parameters
        data = get_extended_best_first_decoder_decode_test_data()
        
        # Test
        for decoder, (document, coreference_scores, kwargs), expected_result in data:
            actual_result = decoder.decode(document, coreference_scores, **kwargs)
            self.assertEqual(expected_result, actual_result)



#decoder.decode
class TestBestNextCoreferenceDecoder(unittest.TestCase):
    
    #decoder.decode
    def test_decode(self):
        # Test parameters
        data = get_best_next_decoder_decode_test_data()
        
        # Test
        for decoder, (document, coreference_scores, kwargs), expected_result in data:
            actual_result = decoder.decode(document, coreference_scores, **kwargs)
            self.assertEqual(expected_result, actual_result)



#decoder.decode
class TestBestLinkCoreferenceDecoder(unittest.TestCase):
    
    #decoder.decode
    def test_decode(self):
        # Test parameters
        data = get_best_link_decoder_decode_test_data()
        
        # Test
        for decoder, (document, coreference_scores, kwargs), expected_result in data:
            actual_result = decoder.decode(document, coreference_scores, **kwargs)
            self.assertEqual(expected_result, actual_result)



#decoder.decode
class TestMixedFirstCoreferenceDecoder(unittest.TestCase):
    
    #decoder.decode
    def test_decode(self):
        # Test parameters
        data = get_mixed_first_decoder_decode_test_data()
        
        # Test
        for decoder, (document, coreference_scores, kwargs), expected_result in data:
            actual_result = decoder.decode(document, coreference_scores, **kwargs)
            self.assertEqual(expected_result, actual_result)



#decoder.decode
class TestNBestFirstCoreferenceDecoder(unittest.TestCase):
    
    #decoder.decode
    def test___init__(self):
        # Test parameters
        configuration = Mapping({"n_best": 2, "useless_field": "useless_value"})
        n_best = 2
        decoder = NBestFirstCoreferenceDecoder(n_best, configuration=Mapping({"useless_field": "useless_value"}))
        
        # Test
        self.assertEqual(configuration, decoder.configuration)
    
    #decoder.decode
    def test__inner_eq(self):
        # Test parameters
        decoder1 = NBestFirstCoreferenceDecoder(2, configuration=Mapping({"witness_field": "witness_value"}))
        decoder2 = NBestFirstCoreferenceDecoder(2, configuration=Mapping({"witness_field": "witness_value"}))
        decoder3 = NBestFirstCoreferenceDecoder(3, configuration=Mapping({"witness_field2": "witness_value2"}))
        
        # Test
        self.assertTrue(*decoder1._inner_eq(decoder2))
        self.assertEqual((False,"n_best"), decoder1._inner_eq(decoder3))
    
    #decoder.decode
    def test_from_configuration(self):
        # Test parameters
        configuration = Mapping({"n_best": 2, "useless_field": "useless_value"})
        n_best = 2
        expected_result = NBestFirstCoreferenceDecoder(n_best, configuration=Mapping({"useless_field": "useless_value"}))
        
        # Test
        actual_result = NBestFirstCoreferenceDecoder.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #decoder.decode
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = NBestFirstCoreferenceDecoder
        n_best = 5
        args = (n_best, )
        kwargs = {}
        expected_result = Mapping({"n_best": n_best,})
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #decoder.decode
    def test_decode(self):
        # Test parameters
        data = get_nbest_first_decoder_decode_test_data()
        
        # Test
        for decoder, (document, coreference_scores, kwargs), expected_result in data:
            actual_result = decoder.decode(document, coreference_scores, **kwargs)
            self.assertEqual(expected_result, actual_result)



#decoder.decode
class TestNBestLinkCoreferenceDecoder(unittest.TestCase):
    
    #decoder.decode
    def test___init__(self):
        # Test parameters
        configuration = Mapping({"n_best": 2, "useless_field": "useless_value"})
        n_best = 2
        decoder = NBestLinkCoreferenceDecoder(n_best, configuration=Mapping({"useless_field": "useless_value"}))
        
        # Test
        self.assertEqual(configuration, decoder.configuration)
    
    #decoder.decode
    def test__inner_eq(self):
        # Test parameters
        decoder1 = NBestLinkCoreferenceDecoder(2, configuration=Mapping({"witness_field": "witness_value"}))
        decoder2 = NBestLinkCoreferenceDecoder(2, configuration=Mapping({"witness_field": "witness_value"}))
        decoder3 = NBestLinkCoreferenceDecoder(3, configuration=Mapping({"witness_field2": "witness_value2"}))
        
        # Test
        self.assertTrue(*decoder1._inner_eq(decoder2))
        self.assertEqual((False,"n_best"), decoder1._inner_eq(decoder3))
    
    #decoder.decode
    def test_from_configuration(self):
        # Test parameters
        configuration = Mapping({"n_best": 2, "useless_field": "useless_value"})
        n_best = 2
        expected_result = NBestLinkCoreferenceDecoder(n_best, configuration=Mapping({"useless_field": "useless_value"}))
        
        # Test
        actual_result = NBestLinkCoreferenceDecoder.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #decoder.decode
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = NBestLinkCoreferenceDecoder
        n_best = 5
        args = (n_best, )
        kwargs = {}
        expected_result = Mapping({"n_best": n_best,})
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #decoder.decode
    def test_decode(self):
        # Test parameters
        data = get_nbest_link_decoder_decode_test_data()
        
        # Test
        for decoder, (document, coreference_scores, kwargs), expected_result in data:
            actual_result = decoder.decode(document, coreference_scores, **kwargs)
            self.assertEqual(expected_result, actual_result)



#decoder.decode
class TestPermutationCoreferenceDecoder(unittest.TestCase):
    
    #decoder.decode
    def test_decode(self):
        # Test parameters
        data = get_permutation_decoder_decode_test_data()
        
        # Test
        for decoder, (document, coreference_scores, kwargs), expected_result in data:
            actual_result = decoder.decode(document, coreference_scores, **kwargs)
            self.assertEqual(expected_result, actual_result)



#decoder.decode
class TestHACCoreferenceDecoder(unittest.TestCase):
    
    #decoder.decode
    def test___init__(self):
        # Test parameters
        configuration = Mapping({"clustering_type": "group_average", "useless_field": "useless_value"})
        clustering_type = "group_average"
        decoder = HACCoreferenceDecoder(clustering_type, configuration=Mapping({"useless_field": "useless_value"}))
        
        # Test
        self.assertEqual(configuration, decoder.configuration)
    
    #decoder.decode
    def test__inner_eq(self):
        # Test parameters
        decoder1 = HACCoreferenceDecoder("group_average", configuration=Mapping({"witness_field": "witness_value"}))
        decoder2 = HACCoreferenceDecoder("group_average", configuration=Mapping({"witness_field": "witness_value"}))
        decoder3 = HACCoreferenceDecoder("single_link", configuration=Mapping({"witness_field2": "witness_value2"}))
        
        # Test
        self.assertTrue(*decoder1._inner_eq(decoder2))
        self.assertEqual((False,"clustering_type"), decoder1._inner_eq(decoder3))
    
    #decoder.decode
    def test_from_configuration(self):
        # Test parameters
        configuration = Mapping({"clustering_type": "group_average", "useless_field": "useless_value"})
        clustering_type = "group_average"
        expected_result = HACCoreferenceDecoder(clustering_type, configuration=Mapping({"useless_field": "useless_value"}))
        
        # Test
        actual_result = HACCoreferenceDecoder.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #decoder.decode
    def test_define_configuration(self):
        # Test parameter
        TEST_CLASS = HACCoreferenceDecoder
        clustering_type = "single_link"
        args = (clustering_type, )
        kwargs = {}
        expected_result = Mapping({"clustering_type": clustering_type,})
        
        # Test
        actual_result = TEST_CLASS.define_configuration(*args, **kwargs)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
    
    #decoder.decode
    def test_decode(self):
        # Test parameters
        data = get_hac_decoder_decode_test_data()
        
        # Test
        for decoder, (document, coreference_scores, kwargs), expected_result in data:
            actual_result = decoder.decode(document, coreference_scores, **kwargs)
            self.assertEqual(expected_result, actual_result)


#decoder.decode
class TestConstrainedBestFirstCoreferenceDecoder(unittest.TestCase):
    
    #decoder.decode
    def test_decode(self):
        # Test parameters
        data = get_constrained_best_first_decoder_decode_test_data()
        
        # Test
        for decoder, (document, coreference_scores, kwargs), expected_result in data:
            actual_result = decoder.decode(document, coreference_scores, **kwargs)
            self.assertEqual(expected_result, actual_result)


#decoder.decode
class TestConstrainedExtendedBestFirstCoreferenceDecoder(unittest.TestCase):
    
    #decoder.decode
    def test_decode(self):
        # Test parameters
        data = get_constrained_extended_best_first_decoder_decode_test_data()
        
        # Test
        for decoder, (document, coreference_scores, kwargs), expected_result in data:
            actual_result = decoder.decode(document, coreference_scores, **kwargs)
            print(expected_result)
            print()
            print(actual_result)
            self.assertEqual(expected_result, actual_result)



@Memoized
def get_test_document_data():
    import os
    from cortex.io.pivot_reader import PivotReader
    
    root_folder_path, file_name = os.path.split(__file__)
    file_name, _ = os.path.splitext(file_name)
    folder_path = os.path.join(root_folder_path, "{}_data".format(file_name))
    document_folder_path = os.path.join(folder_path, "decoder_test_data", "ref")
    
    strict = True # We want to synchronize head tokens.
    test_document = PivotReader.parse(document_folder_path, strict=strict)
    
    return test_document


def get_closest_first_decoder_decode_test_data():
    data = []
    
    test_document = get_test_document_data()
    
    # mention_extents = ((1,9), (14,42), (46,47), (83,84),)
    # scores_are_proba = True
    scores_are_proba = True
    coreference_scores = {((1,9), (14,42)): 0.65, 
                          ((14,42), (46,47)): 0.1, 
                          ((1,9), (46,47)): 0.29, 
                          ((46,47), (83,84)): 0.46,
                          ((14,42), (83,84)): 0.89, 
                          ((1,9), (83,84)): 0.78,
                          }
    extents_entities = (((1,9), (14,42), (83,84)), 
                        ((27,42),),
                        ((46,47),),
                        ((68,75),),
                        ((114, 120),),
                        )
    expected_result = CoreferencePartition(entities=extents_entities)
    
    configuration = Mapping()
    decoder = ClosestFirstCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"scores_are_proba": scores_are_proba}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    
    # scores_are_proba = False
    scores_are_proba = False
    coreference_scores = {((1,9), (14,42)): 0.65, 
                          ((14,42), (46,47)): -0.1, 
                          ((1,9), (46,47)): -0.29, 
                          ((46,47), (83,84)): -0.46,
                          ((14,42), (83,84)): 0.89, 
                          ((1,9), (83,84)): 0.78,
                          }
    extents_entities = (((1,9), (14,42), (83,84)), 
                        ((27,42),),
                        ((46,47),),
                        ((68,75),),
                        ((114, 120),),
                        )
    expected_result = CoreferencePartition(entities=extents_entities)
    
    configuration = Mapping()
    decoder = ClosestFirstCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"scores_are_proba": scores_are_proba}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    return data



def get_aggressive_merge_decoder_decode_test_data():
    data = []
    
    test_document = get_test_document_data()
    
    # mention_extents = ((1,9), (14,42), (27,42), (46,47), (68,75), (83,84), (114,120))
    # scores_are_proba = True
    scores_are_proba = True
    coreference_scores = {((1,9), (14,42)): 0.6, ((1,9), (27,42)): 0.4, ((1,9), (46,47)): 0.2, ((1,9), (68,75)): 0.45, ((1,9), (83,84)): 0.19, ((1,9), (114,120)): 0.3,
                          ((14,42), (27,42)): 0.07, ((14,42), (46,47)): 0.1, ((14,42), (68,75)): 0.065, ((14,42), (83,84)): 0.07, ((14,42), (114,120)): 0.05,
                          ((27,42), (46,47)): 0.45, ((27,42), (68,75)): 0.89, ((27,42), (83,84)): 0.35, ((27,42), (114,120)): 0.069,
                          ((46,47), (68,75)): 0.01, ((46,47), (83,84)): 0.072, ((46,47), (114,120)): 0.96,
                          ((68,75), (83,84)): 0.19, ((68,75), (114,120)): 0.081,
                          ((83,84), (114,120)): 0.99,
                          }
    extents_entities = (((1,9), (14,42),),
                        ((27,42), (68,75),),
                        ((46,47), (83,84),(114,120),), 
                        )
    expected_result = CoreferencePartition(entities=extents_entities)
    
    configuration = Mapping()
    decoder = AggressiveMergeCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"scores_are_proba": scores_are_proba}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    
    # scores_are_proba = False
    scores_are_proba = False
    coreference_scores = {((1,9), (14,42)): 0.6, ((1,9), (27,42)): -0.4, ((1,9), (46,47)): -0.2, ((1,9), (68,75)): -0.45, ((1,9), (83,84)): -0.19, ((1,9), (114,120)): -0.3,
                          ((14,42), (27,42)): -0.07, ((14,42), (46,47)): -0.1, ((14,42), (68,75)): -0.065, ((14,42), (83,84)): -0.07, ((14,42), (114,120)): -0.05,
                          ((27,42), (46,47)): -0.45, ((27,42), (68,75)): 0.89, ((27,42), (83,84)): -0.35, ((27,42), (114,120)): -0.069,
                          ((46,47), (68,75)): -0.01, ((46,47), (83,84)): -0.072, ((46,47), (114,120)): 0.96,
                          ((68,75), (83,84)): -0.19, ((68,75), (114,120)): -0.081,
                          ((83,84), (114,120)): 0.99,
                          }
    extents_entities = (((1,9), (14,42),),
                        ((27,42), (68,75),),
                        ((46,47), (83,84),(114,120),), 
                        )
    
    configuration = Mapping()
    decoder = AggressiveMergeCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"scores_are_proba": scores_are_proba}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    return data



def get_oracle_aggressive_merge_decoder_decode_test_data():
    data = []
    
    test_document = get_test_document_data()
    
    # mention_extents = ((1,9), (14,42), (27,42), (46,47), (68,75), (83,84), (114,120))
    # scores_are_proba = True
    scores_are_proba = True
    coreference_scores = {((1,9), (14,42)): 0.6, ((1,9), (27,42)): 0.4, ((1,9), (46,47)): 0.2, ((1,9), (68,75)): 0.55, ((1,9), (83,84)): 0.19, ((1,9), (114,120)): 0.3,
                          ((14,42), (27,42)): 0.7, ((14,42), (46,47)): 0.1, ((14,42), (68,75)): 0.65, ((14,42), (83,84)): 0.07, ((14,42), (114,120)): 0.05,
                          ((27,42), (46,47)): 0.45, ((27,42), (68,75)): 0.89, ((27,42), (83,84)): 0.35, ((27,42), (114,120)): 0.69,
                          ((46,47), (68,75)): 0.0, ((46,47), (83,84)): 0.72, ((46,47), (114,120)): 0.96,
                          ((68,75), (83,84)): 0.19, ((68,75), (114,120)): 0.81,
                          ((83,84), (114,120)): 0.99,
                          }
    extents_entities = (((1,9), (14,42),),
                        ((27,42), (68,75),),
                        ((114,120),),
                        ((46,47), (83,84),), 
                        )
    expected_result = CoreferencePartition(entities=extents_entities)
    
    configuration = Mapping()
    decoder = OracleAggressiveMergeCoreferenceDecoder.from_configuration(configuration)
    ref_coreference_partition = test_document.coreference_partition
    kwargs = {"scores_are_proba": scores_are_proba}
    
    data.append((decoder, (test_document, coreference_scores, ref_coreference_partition, kwargs), expected_result))
    
    
    # scores_are_proba = False
    scores_are_proba = False
    coreference_scores = {((1,9), (14,42)): 0.6, ((1,9), (27,42)): -0.4, ((1,9), (46,47)): -0.2, ((1,9), (68,75)): 0.55, ((1,9), (83,84)): -0.19, ((1,9), (114,120)): -0.3,
                          ((14,42), (27,42)): 0.7, ((14,42), (46,47)): -0.1, ((14,42), (68,75)): 0.65, ((14,42), (83,84)): -0.07, ((14,42), (114,120)): -0.05,
                          ((27,42), (46,47)): -0.45, ((27,42), (68,75)): 0.89, ((27,42), (83,84)): -0.35, ((27,42), (114,120)): 0.69,
                          ((46,47), (68,75)): -0.01, ((46,47), (83,84)): 0.72, ((46,47), (114,120)): 0.96,
                          ((68,75), (83,84)): -0.19, ((68,75), (114,120)): 0.81,
                          ((83,84), (114,120)): 0.99,
                          }
    extents_entities = (((1,9), (14,42),),
                        ((27,42), (68,75),),
                        ((114,120),),
                        ((46,47), (83,84),), 
                        )
    
    configuration = Mapping()
    decoder = OracleAggressiveMergeCoreferenceDecoder.from_configuration(configuration)
    ref_coreference_partition = test_document.coreference_partition
    kwargs = {"scores_are_proba": scores_are_proba}
    
    data.append((decoder, (test_document, coreference_scores, ref_coreference_partition, kwargs), expected_result))
    
    return data
    


def get_best_first_decoder_decode_test_data():
    data = []
    
    test_document = get_test_document_data()
    
    # mention_extents = ((1,9), (14,42), (27,42), (46,47), (68,75), (83,84), (114,120))
    # (46,47) and (83,84) are pronouns
    # scores_are_proba = True
    scores_are_proba = True
    coreference_scores = {((1,9), (14,42)): 0.6, ((1,9), (27,42)): 0.4, ((1,9), (46,47)): 0.2, ((1,9), (68,75)): 0.45, ((1,9), (83,84)): 0.19, ((1,9), (114,120)): 0.3,
                          ((14,42), (27,42)): 0.07, ((14,42), (46,47)): 0.1, ((14,42), (68,75)): 0.065, ((14,42), (83,84)): 0.07, ((14,42), (114,120)): 0.05,
                          ((27,42), (46,47)): 0.45, ((27,42), (68,75)): 0.89, ((27,42), (83,84)): 0.35, ((27,42), (114,120)): 0.069,
                          ((46,47), (68,75)): 0.01, ((46,47), (83,84)): 0.072, ((46,47), (114,120)): 0.96,
                          ((68,75), (83,84)): 0.19, ((68,75), (114,120)): 0.081,
                          ((83,84), (114,120)): 0.99,
                          }
    extents_entities = (((1,9), (14,42),),
                        ((27,42),(68,75),),
                        ((46,47),),
                        ((83,84),),
                        ((114,120),),
                        )
    expected_result = CoreferencePartition(entities=extents_entities)
    
    configuration = Mapping()
    decoder = BestFirstCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"scores_are_proba": scores_are_proba}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    # scores_are_proba = False
    scores_are_proba = False
    coreference_scores = {((1,9), (14,42)): 0.6, ((1,9), (27,42)): -0.4, ((1,9), (46,47)): -0.2, ((1,9), (68,75)): -0.45, ((1,9), (83,84)): -0.19, ((1,9), (114,120)): -0.3,
                          ((14,42), (27,42)): -0.07, ((14,42), (46,47)): -0.1, ((14,42), (68,75)): -0.065, ((14,42), (83,84)): -0.07, ((14,42), (114,120)): -0.05,
                          ((27,42), (46,47)): -0.45, ((27,42), (68,75)): 0.89, ((27,42), (83,84)): -0.35, ((27,42), (114,120)): -0.069,
                          ((46,47), (68,75)): -0.01, ((46,47), (83,84)): -0.072, ((46,47), (114,120)): 0.96,
                          ((68,75), (83,84)): -0.19, ((68,75), (114,120)): -0.081,
                          ((83,84), (114,120)): 0.99,
                          }
    extents_entities = (((1,9), (14,42),),
                        ((27,42),(68,75),),
                        ((46,47),),
                        ((83,84),),
                        ((114,120),),
                        )
    
    configuration = Mapping()
    decoder = BestFirstCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"scores_are_proba": scores_are_proba}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    return data



def get_best_next_decoder_decode_test_data():
    data = []
    
    test_document = get_test_document_data()
    
    # mention_extents = ((1,9), (14,42), (27,42), (46,47), (68,75), (83,84), (114,120))
    # (46,47) and (83,84) are pronouns
    # scores_are_proba = True
    scores_are_proba = True
    coreference_scores = {((1,9), (14,42)): 0.6, ((1,9), (27,42)): 0.4, ((1,9), (46,47)): 0.52, ((1,9), (68,75)): 0.45, ((1,9), (83,84)): 0.19, ((1,9), (114,120)): 0.3,
                          ((14,42), (27,42)): 0.07, ((14,42), (46,47)): 0.1, ((14,42), (68,75)): 0.065, ((14,42), (83,84)): 0.07, ((14,42), (114,120)): 0.05,
                          ((27,42), (46,47)): 0.45, ((27,42), (68,75)): 0.89, ((27,42), (83,84)): 0.35, ((27,42), (114,120)): 0.069,
                          ((46,47), (68,75)): 0.01, ((46,47), (83,84)): 0.072, ((46,47), (114,120)): 0.96,
                          ((68,75), (83,84)): 0.19, ((68,75), (114,120)): 0.081,
                          ((83,84), (114,120)): 0.99,
                          }
    extents_entities = (((1,9), (14,42),),
                        ((27,42),(68,75),),
                        ((46,47),),
                        ((83,84),),
                        ((114,120),),
                        )
    expected_result = CoreferencePartition(entities=extents_entities)
    
    configuration = Mapping()
    decoder = BestNextCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"scores_are_proba": scores_are_proba}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    # scores_are_proba = False
    scores_are_proba = False
    coreference_scores = {((1,9), (14,42)): 0.6, ((1,9), (27,42)): -0.4, ((1,9), (46,47)): 0.52, ((1,9), (68,75)): -0.45, ((1,9), (83,84)): -0.19, ((1,9), (114,120)): -0.3,
                          ((14,42), (27,42)): -0.07, ((14,42), (46,47)): -0.1, ((14,42), (68,75)): -0.065, ((14,42), (83,84)): -0.07, ((14,42), (114,120)): -0.05,
                          ((27,42), (46,47)): -0.45, ((27,42), (68,75)): 0.89, ((27,42), (83,84)): -0.35, ((27,42), (114,120)): -0.069,
                          ((46,47), (68,75)): -0.01, ((46,47), (83,84)): -0.072, ((46,47), (114,120)): 0.96,
                          ((68,75), (83,84)): -0.19, ((68,75), (114,120)): -0.081,
                          ((83,84), (114,120)): 0.99,
                          }
    extents_entities = (((1,9), (14,42),),
                        ((27,42),(68,75),),
                        ((46,47),),
                        ((83,84),),
                        ((114,120),),
                        )
    
    configuration = Mapping()
    decoder = BestNextCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"scores_are_proba": scores_are_proba}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    return data


def get_best_link_decoder_decode_test_data():
    data = []
    
    test_document = get_test_document_data()
    
    # mention_extents = ((1,9), (14,42), (27,42), (46,47), (68,75), (83,84), (114,120))
    # (46,47) and (83,84) are pronouns
    # scores_are_proba = True
    scores_are_proba = True
    coreference_scores = {((1,9), (14,42)): 0.6, ((1,9), (27,42)): 0.4, ((1,9), (46,47)): 0.52, ((1,9), (68,75)): 0.45, ((1,9), (83,84)): 0.19, ((1,9), (114,120)): 0.3,
                          ((14,42), (27,42)): 0.07, ((14,42), (46,47)): 0.1, ((14,42), (68,75)): 0.065, ((14,42), (83,84)): 0.07, ((14,42), (114,120)): 0.05,
                          ((27,42), (46,47)): 0.45, ((27,42), (68,75)): 0.89, ((27,42), (83,84)): 0.35, ((27,42), (114,120)): 0.069,
                          ((46,47), (68,75)): 0.01, ((46,47), (83,84)): 0.072, ((46,47), (114,120)): 0.96,
                          ((68,75), (83,84)): 0.19, ((68,75), (114,120)): 0.081,
                          ((83,84), (114,120)): 0.99,
                          }
    extents_entities = (((1,9), (14,42),),
                        ((27,42),(68,75),),
                        ((46,47),(83,84),(114,120),),
                        )
    expected_result = CoreferencePartition(entities=extents_entities)
    
    configuration = Mapping()
    decoder = BestLinkCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"scores_are_proba": scores_are_proba}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    # scores_are_proba = False
    scores_are_proba = False
    coreference_scores = {((1,9), (14,42)): 0.6, ((1,9), (27,42)): -0.4, ((1,9), (46,47)): 0.52, ((1,9), (68,75)): -0.45, ((1,9), (83,84)): -0.19, ((1,9), (114,120)): -0.3,
                          ((14,42), (27,42)): -0.07, ((14,42), (46,47)): -0.1, ((14,42), (68,75)): -0.065, ((14,42), (83,84)): -0.07, ((14,42), (114,120)): -0.05,
                          ((27,42), (46,47)): -0.45, ((27,42), (68,75)): 0.89, ((27,42), (83,84)): -0.35, ((27,42), (114,120)): -0.069,
                          ((46,47), (68,75)): -0.01, ((46,47), (83,84)): -0.072, ((46,47), (114,120)): 0.96,
                          ((68,75), (83,84)): -0.19, ((68,75), (114,120)): -0.081,
                          ((83,84), (114,120)): 0.99,
                          }
    extents_entities = (((1,9), (14,42),),
                        ((27,42),(68,75),),
                        ((46,47),(83,84),(114,120),),
                        )
    
    configuration = Mapping()
    decoder = BestLinkCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"scores_are_proba": scores_are_proba}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    return data



def get_mixed_first_decoder_decode_test_data():
    data = []
    
    test_document = get_test_document_data()
    
    # mention_extents = ((1,9), (14,42), (27,42), (46,47), (68,75), (83,84), (114,120))
    # (46,47) and (83,84) are pronouns
    # scores_are_proba = True
    scores_are_proba = True
    coreference_scores = {((1,9), (14,42)): 0.6, ((1,9), (27,42)): 0.4, ((1,9), (46,47)): 0.52, ((1,9), (68,75)): 0.45, ((1,9), (83,84)): 0.19, ((1,9), (114,120)): 0.3,
                          ((14,42), (27,42)): 0.07, ((14,42), (46,47)): 0.1, ((14,42), (68,75)): 0.065, ((14,42), (83,84)): 0.07, ((14,42), (114,120)): 0.05,
                          ((27,42), (46,47)): 0.45, ((27,42), (68,75)): 0.89, ((27,42), (83,84)): 0.35, ((27,42), (114,120)): 0.069,
                          ((46,47), (68,75)): 0.01, ((46,47), (83,84)): 0.072, ((46,47), (114,120)): 0.96,
                          ((68,75), (83,84)): 0.19, ((68,75), (114,120)): 0.081,
                          ((83,84), (114,120)): 0.99,
                          }
    extents_entities = (((1,9),(14,42),(46,47),),
                        ((27,42),(68,75),),
                        ((83,84),),
                        ((114,120),),
                        )
    expected_result = CoreferencePartition(entities=extents_entities)
    
    configuration = Mapping()
    decoder = MixedFirstCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"scores_are_proba": scores_are_proba}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    # scores_are_proba = False
    scores_are_proba = False
    coreference_scores = {((1,9), (14,42)): 0.6, ((1,9), (27,42)): -0.4, ((1,9), (46,47)): 0.52, ((1,9), (68,75)): -0.45, ((1,9), (83,84)): -0.19, ((1,9), (114,120)): -0.3,
                          ((14,42), (27,42)): -0.07, ((14,42), (46,47)): -0.1, ((14,42), (68,75)): -0.065, ((14,42), (83,84)): -0.07, ((14,42), (114,120)): -0.05,
                          ((27,42), (46,47)): -0.45, ((27,42), (68,75)): 0.89, ((27,42), (83,84)): -0.35, ((27,42), (114,120)): -0.069,
                          ((46,47), (68,75)): -0.01, ((46,47), (83,84)): -0.072, ((46,47), (114,120)): 0.96,
                          ((68,75), (83,84)): -0.19, ((68,75), (114,120)): -0.081,
                          ((83,84), (114,120)): 0.99,
                          }
    extents_entities = (((1,9),(14,42),(46,47),),
                        ((27,42),(68,75),),
                        ((83,84),),
                        ((114,120),),
                        )
    
    configuration = Mapping()
    decoder = MixedFirstCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"scores_are_proba": scores_are_proba}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    return data



def get_nbest_first_decoder_decode_test_data():
    data = []
    
    test_document = get_test_document_data()
    
    # mention_extents = ((1,9), (14,42), (27,42), (46,47), (68,75), (83,84), (114,120))
    # (46,47) and (83,84) are pronouns
    # scores_are_proba = True
    scores_are_proba = True
    coreference_scores = {((1,9), (14,42)): 0.6, ((1,9), (27,42)): 0.4, ((1,9), (46,47)): 0.52, ((1,9), (68,75)): 0.45, ((1,9), (83,84)): 0.19, ((1,9), (114,120)): 0.3,
                          ((14,42), (27,42)): 0.07, ((14,42), (46,47)): 0.1, ((14,42), (68,75)): 0.065, ((14,42), (83,84)): 0.07, ((14,42), (114,120)): 0.05,
                          ((27,42), (46,47)): 0.45, ((27,42), (68,75)): 0.89, ((27,42), (83,84)): 0.35, ((27,42), (114,120)): 0.069,
                          ((46,47), (68,75)): 0.01, ((46,47), (83,84)): 0.072, ((46,47), (114,120)): 0.96,
                          ((68,75), (83,84)): 0.19, ((68,75), (114,120)): 0.081,
                          ((83,84), (114,120)): 0.99,
                          }
    extents_entities = (((1,9),(14,42),(46,47),),
                        ((27,42),(68,75),),
                        ((83,84),),
                        ((114,120),),
                        )
    expected_result = CoreferencePartition(entities=extents_entities)
    
    configuration = Mapping({"n_best": 2})
    decoder = NBestFirstCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"scores_are_proba": scores_are_proba}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    # scores_are_proba = False
    scores_are_proba = False
    coreference_scores = {((1,9), (14,42)): 0.6, ((1,9), (27,42)): -0.4, ((1,9), (46,47)): 0.52, ((1,9), (68,75)): -0.45, ((1,9), (83,84)): -0.19, ((1,9), (114,120)): -0.3,
                          ((14,42), (27,42)): -0.07, ((14,42), (46,47)): -0.1, ((14,42), (68,75)): -0.065, ((14,42), (83,84)): -0.07, ((14,42), (114,120)): -0.05,
                          ((27,42), (46,47)): -0.45, ((27,42), (68,75)): 0.89, ((27,42), (83,84)): -0.35, ((27,42), (114,120)): -0.069,
                          ((46,47), (68,75)): -0.01, ((46,47), (83,84)): -0.072, ((46,47), (114,120)): 0.96,
                          ((68,75), (83,84)): -0.19, ((68,75), (114,120)): -0.081,
                          ((83,84), (114,120)): 0.99,
                          }
    extents_entities = (((1,9),(14,42),(46,47),),
                        ((27,42),(68,75),),
                        ((83,84),),
                        ((114,120),),
                        )
    
    configuration = Mapping({"n_best": 2})
    decoder = NBestFirstCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"scores_are_proba": scores_are_proba}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    return data



def get_nbest_link_decoder_decode_test_data():
    data = []
    
    test_document = get_test_document_data()
    
    # mention_extents = ((1,9), (14,42), (27,42), (46,47), (68,75), (83,84), (114,120))
    # (46,47) and (83,84) are pronouns
    # scores_are_proba = True
    scores_are_proba = True
    coreference_scores = {((1,9), (14,42)): 0.6, ((1,9), (27,42)): 0.4, ((1,9), (46,47)): 0.52, ((1,9), (68,75)): 0.45, ((1,9), (83,84)): 0.19, ((1,9), (114,120)): 0.3,
                          ((14,42), (27,42)): 0.07, ((14,42), (46,47)): 0.1, ((14,42), (68,75)): 0.065, ((14,42), (83,84)): 0.07, ((14,42), (114,120)): 0.05,
                          ((27,42), (46,47)): 0.45, ((27,42), (68,75)): 0.89, ((27,42), (83,84)): 0.35, ((27,42), (114,120)): 0.069,
                          ((46,47), (68,75)): 0.01, ((46,47), (83,84)): 0.072, ((46,47), (114,120)): 0.96,
                          ((68,75), (83,84)): 0.19, ((68,75), (114,120)): 0.081,
                          ((83,84), (114,120)): 0.99,
                          }
    extents_entities = (((1,9),(14,42),(46,47),(83,84),(114,120),),
                        ((27,42),(68,75),),
                        )
    expected_result = CoreferencePartition(entities=extents_entities)
    
    configuration = Mapping({"n_best": 2})
    decoder = NBestLinkCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"scores_are_proba": scores_are_proba}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    # scores_are_proba = False
    scores_are_proba = False
    coreference_scores = {((1,9), (14,42)): 0.6, ((1,9), (27,42)): -0.4, ((1,9), (46,47)): 0.52, ((1,9), (68,75)): -0.45, ((1,9), (83,84)): -0.19, ((1,9), (114,120)): -0.3,
                          ((14,42), (27,42)): -0.07, ((14,42), (46,47)): -0.1, ((14,42), (68,75)): -0.065, ((14,42), (83,84)): -0.07, ((14,42), (114,120)): -0.05,
                          ((27,42), (46,47)): -0.45, ((27,42), (68,75)): 0.89, ((27,42), (83,84)): -0.35, ((27,42), (114,120)): -0.069,
                          ((46,47), (68,75)): -0.01, ((46,47), (83,84)): -0.072, ((46,47), (114,120)): 0.96,
                          ((68,75), (83,84)): -0.19, ((68,75), (114,120)): -0.081,
                          ((83,84), (114,120)): 0.99,
                          }
    extents_entities = (((1,9),(14,42),(46,47),(83,84),(114,120),),
                        ((27,42),(68,75),),
                        )
    
    configuration = Mapping({"n_best": 2})
    decoder = NBestLinkCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"scores_are_proba": scores_are_proba}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    return data


def get_permutation_decoder_decode_test_data():
    data = []
    
    test_document = get_test_document_data()
    
    # mention_extents = ((1,9), (14,42), (27,42), (46,47), (68,75), (83,84), (114,120))
    # (46,47) and (83,84) are pronouns
    # scores_are_proba = True
    scores_are_proba = True
    coreference_scores = {((1,9), (14,42)): 0.6, ((1,9), (27,42)): 0.4, ((1,9), (46,47)): 0.52, ((1,9), (68,75)): 0.45, ((1,9), (83,84)): 0.19, ((1,9), (114,120)): 0.3,
                          ((14,42), (27,42)): 0.07, ((14,42), (46,47)): 0.1, ((14,42), (68,75)): 0.065, ((14,42), (83,84)): 0.07, ((14,42), (114,120)): 0.05,
                          ((27,42), (46,47)): 0.45, ((27,42), (68,75)): 0.89, ((27,42), (83,84)): 0.35, ((27,42), (114,120)): 0.069,
                          ((46,47), (68,75)): 0.01, ((46,47), (83,84)): 0.072, ((46,47), (114,120)): 0.96,
                          ((68,75), (83,84)): 0.19, ((68,75), (114,120)): 0.081,
                          ((83,84), (114,120)): 0.99,
                          }
    extents_entities = (((1,9),(14,42),),
                        ((27,42),(68,75),),
                        ((46,47),),
                        ((83,84),(114,120),),
                        )
    expected_result = CoreferencePartition(entities=extents_entities)
    
    configuration = Mapping({"n_best": 2})
    decoder = PermutationCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"scores_are_proba": scores_are_proba}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    # scores_are_proba = False
    scores_are_proba = False
    coreference_scores = {((1,9), (14,42)): 0.6, ((1,9), (27,42)): -0.4, ((1,9), (46,47)): 0.52, ((1,9), (68,75)): -0.45, ((1,9), (83,84)): -0.19, ((1,9), (114,120)): -0.3,
                          ((14,42), (27,42)): -0.07, ((14,42), (46,47)): -0.1, ((14,42), (68,75)): -0.065, ((14,42), (83,84)): -0.07, ((14,42), (114,120)): -0.05,
                          ((27,42), (46,47)): -0.45, ((27,42), (68,75)): 0.89, ((27,42), (83,84)): -0.35, ((27,42), (114,120)): -0.069,
                          ((46,47), (68,75)): -0.01, ((46,47), (83,84)): -0.072, ((46,47), (114,120)): 0.96,
                          ((68,75), (83,84)): -0.19, ((68,75), (114,120)): -0.081,
                          ((83,84), (114,120)): 0.99,
                          }
    extents_entities = (((1,9),(14,42),),
                        ((27,42),(68,75),),
                        ((46,47),),
                        ((83,84),(114,120),),
                        )
    
    configuration = Mapping({"n_best": 2})
    decoder = PermutationCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"scores_are_proba": scores_are_proba}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    return data




def get_hac_decoder_decode_test_data():
    data = []
    
    test_document = get_test_document_data()
    
    # mention_extents = ((1,9), (14,42), (27,42), (46,47), (68,75), (83,84), (114,120))
    # clustering type = "group_average"
    coreference_scores = {((1,9), (14,42)): 0.6, ((1,9), (27,42)): 0.4, ((1,9), (46,47)): 0.2, ((1,9), (68,75)): 0.45, ((1,9), (83,84)): 0.19, ((1,9), (114,120)): 0.3,
                          ((14,42), (27,42)): 0.07, ((14,42), (46,47)): 0.1, ((14,42), (68,75)): 0.065, ((14,42), (83,84)): 0.07, ((14,42), (114,120)): 0.05,
                          ((27,42), (46,47)): 0.45, ((27,42), (68,75)): 0.89, ((27,42), (83,84)): 0.35, ((27,42), (114,120)): 0.069,
                          ((46,47), (68,75)): 0.01, ((46,47), (83,84)): 0.072, ((46,47), (114,120)): 0.96,
                          ((68,75), (83,84)): 0.19, ((68,75), (114,120)): 0.081,
                          ((83,84), (114,120)): 0.99,
                          }
    extents_entities = (((1,9), (14,42),),
                        ((27,42),(68,75),),
                        ((46,47),(83,84),(114,120),),
                        )
    expected_result = CoreferencePartition(entities=extents_entities)
    
    configuration = Mapping({"clustering_type": "group_average"})
    decoder = HACCoreferenceDecoder.from_configuration(configuration)
    scores_are_proba = True
    kwargs = {"scores_are_proba": scores_are_proba}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    # clustering type = "single_link"
    coreference_scores = {((1,9), (14,42)): 0.6, ((1,9), (27,42)): 0.4, ((1,9), (46,47)): 0.2, ((1,9), (68,75)): 0.45, ((1,9), (83,84)): 0.19, ((1,9), (114,120)): 0.3,
                          ((14,42), (27,42)): 0.07, ((14,42), (46,47)): 0.1, ((14,42), (68,75)): 0.065, ((14,42), (83,84)): 0.07, ((14,42), (114,120)): 0.05,
                          ((27,42), (46,47)): 0.45, ((27,42), (68,75)): 0.89, ((27,42), (83,84)): 0.35, ((27,42), (114,120)): 0.069,
                          ((46,47), (68,75)): 0.01, ((46,47), (83,84)): 0.072, ((46,47), (114,120)): 0.96,
                          ((68,75), (83,84)): 0.19, ((68,75), (114,120)): 0.081,
                          ((83,84), (114,120)): 0.99,
                          }
    extents_entities = (((1,9), (14,42),),
                        ((27,42),(68,75),),
                        ((46,47),(83,84),(114,120),),
                        )
    expected_result = CoreferencePartition(entities=extents_entities)
    
    configuration = Mapping({"clustering_type": "single_link"})
    decoder = HACCoreferenceDecoder.from_configuration(configuration)
    scores_are_proba = True
    kwargs = {"scores_are_proba": scores_are_proba}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    # clustering type = "complete_link"
    coreference_scores = {((1,9), (14,42)): 0.6, ((1,9), (27,42)): 0.4, ((1,9), (46,47)): 0.2, ((1,9), (68,75)): 0.45, ((1,9), (83,84)): 0.19, ((1,9), (114,120)): 0.3,
                          ((14,42), (27,42)): 0.07, ((14,42), (46,47)): 0.1, ((14,42), (68,75)): 0.065, ((14,42), (83,84)): 0.07, ((14,42), (114,120)): 0.05,
                          ((27,42), (46,47)): 0.45, ((27,42), (68,75)): 0.89, ((27,42), (83,84)): 0.35, ((27,42), (114,120)): 0.069,
                          ((46,47), (68,75)): 0.01, ((46,47), (83,84)): 0.072, ((46,47), (114,120)): 0.96,
                          ((68,75), (83,84)): 0.19, ((68,75), (114,120)): 0.081,
                          ((83,84), (114,120)): 0.99,
                          }
    extents_entities = (((1,9), (14,42),),
                        ((27,42),(68,75),),
                        ((46,47),),
                        ((83,84),(114,120),)
                        )
    expected_result = CoreferencePartition(entities=extents_entities)
    
    configuration = Mapping({"clustering_type": "complete_link"})
    decoder = HACCoreferenceDecoder.from_configuration(configuration)
    scores_are_proba = True
    kwargs = {"scores_are_proba": scores_are_proba}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    return data


def get_extended_best_first_decoder_decode_test_data():
    data = []
    nm_extent = NULL_MENTION.extent
    
    test_document = get_test_document_data()
    
    # mention_extents = ((1,9), (14,42), (27,42), (46,47), (68,75), (83,84), (114,120))
    # (46,47) and (83,84) are pronouns
    # scores_are_proba = True
    scores_are_proba = True
    coreference_scores = {(nm_extent, (14,42)): 0.7, (nm_extent, (27,42)): 0.45, (nm_extent, (46,47)): 0.42, (nm_extent, (68,75)): 0.55, (nm_extent, (83,84)): 0.4, (nm_extent, (114,120)): 0.8, 
                          ((1,9), (14,42)): 0.6, ((1,9), (27,42)): 0.4, ((1,9), (46,47)): 0.2, ((1,9), (68,75)): 0.45, ((1,9), (83,84)): 0.19, ((1,9), (114,120)): 0.3,
                          ((14,42), (27,42)): 0.07, ((14,42), (46,47)): 0.1, ((14,42), (68,75)): 0.065, ((14,42), (83,84)): 0.07, ((14,42), (114,120)): 0.05,
                          ((27,42), (46,47)): 0.45, ((27,42), (68,75)): 0.89, ((27,42), (83,84)): 0.35, ((27,42), (114,120)): 0.069,
                          ((46,47), (68,75)): 0.01, ((46,47), (83,84)): 0.072, ((46,47), (114,120)): 0.96,
                          ((68,75), (83,84)): 0.19, ((68,75), (114,120)): 0.081,
                          ((83,84), (114,120)): 0.99,
                          }
    extents_entities = (((1,9),),
                        ((14,42),),
                        ((27,42),(68,75),),
                        ((46,47),),
                        ((83,84),),
                        ((114,120),),
                        )
    expected_result = CoreferencePartition(entities=extents_entities)
    
    configuration = Mapping()
    decoder = ExtendedBestFirstCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"scores_are_proba": scores_are_proba}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    # scores_are_proba = False
    scores_are_proba = False
    coreference_scores = {(nm_extent, (14,42)): 0.7, (nm_extent, (27,42)): -0.45, (nm_extent, (46,47)): -0.42, (nm_extent, (68,75)): 0.55, (nm_extent, (83,84)): -0.4, (nm_extent, (114,120)): 0.8, 
                          ((1,9), (14,42)): 0.6, ((1,9), (27,42)): -0.4, ((1,9), (46,47)): -0.2, ((1,9), (68,75)): -0.45, ((1,9), (83,84)): -0.19, ((1,9), (114,120)): -0.3,
                          ((14,42), (27,42)): -0.07, ((14,42), (46,47)): -0.1, ((14,42), (68,75)): -0.065, ((14,42), (83,84)): -0.07, ((14,42), (114,120)): -0.05,
                          ((27,42), (46,47)): -0.45, ((27,42), (68,75)): 0.89, ((27,42), (83,84)): -0.35, ((27,42), (114,120)): -0.069,
                          ((46,47), (68,75)): -0.01, ((46,47), (83,84)): -0.072, ((46,47), (114,120)): 0.96,
                          ((68,75), (83,84)): -0.19, ((68,75), (114,120)): -0.081,
                          ((83,84), (114,120)): 0.99,
                          }
    extents_entities = (((1,9),),
                        ((14,42),),
                        ((27,42),(68,75),),
                        ((46,47),),
                        ((83,84),),
                        ((114,120),),
                        )
    
    configuration = Mapping()
    decoder = ExtendedBestFirstCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"scores_are_proba": scores_are_proba}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    return data



def get_constrained_best_first_decoder_decode_test_data():
    data = []
    
    test_document = get_test_document_data()
    
    # mention_extents = ((1,9), (14,42), (27,42), (46,47), (68,75), (83,84), (114,120))
    # (46,47) and (83,84) are pronouns
    # scores_are_proba = True
    scores_are_proba = True
    coreference_scores = {((1,9), (14,42)): 0.6, ((1,9), (27,42)): 0.4, ((1,9), (46,47)): 0.2, ((1,9), (68,75)): 0.45, ((1,9), (83,84)): 0.19, ((1,9), (114,120)): 0.3,
                          ((14,42), (27,42)): 0.07, ((14,42), (46,47)): 0.1, ((14,42), (68,75)): 0.065, ((14,42), (83,84)): 0.07, ((14,42), (114,120)): 0.05,
                          ((27,42), (46,47)): 0.45, ((27,42), (68,75)): 0.89, ((27,42), (83,84)): 0.35, ((27,42), (114,120)): 0.069,
                          ((46,47), (68,75)): 0.01, ((46,47), (83,84)): 0.072, ((46,47), (114,120)): 0.96,
                          ((68,75), (83,84)): 0.19, ((68,75), (114,120)): 0.081,
                          ((83,84), (114,120)): 0.99,
                          }
    positive_edges = [((46,47), (83,84)), ((83,84), (114,120)), ]
    negative_edges = [((27,42),(68,75)), ]
    extents_entities = (((1,9), (14,42),),
                        ((27,42),),
                        ((68,75),),
                        ((46,47),(83,84),(114,120),),
                        )
    expected_result = CoreferencePartition(entities=extents_entities)
    
    configuration = Mapping()
    decoder = ConstrainedBestFirstCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"scores_are_proba": scores_are_proba, "positive_edges": positive_edges, "negative_edges": negative_edges}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    # scores_are_proba = False
    scores_are_proba = False
    coreference_scores = {((1,9), (14,42)): 0.6, ((1,9), (27,42)): -0.4, ((1,9), (46,47)): -0.2, ((1,9), (68,75)): -0.45, ((1,9), (83,84)): -0.19, ((1,9), (114,120)): -0.3,
                          ((14,42), (27,42)): -0.07, ((14,42), (46,47)): -0.1, ((14,42), (68,75)): -0.065, ((14,42), (83,84)): -0.07, ((14,42), (114,120)): -0.05,
                          ((27,42), (46,47)): -0.45, ((27,42), (68,75)): 0.89, ((27,42), (83,84)): -0.35, ((27,42), (114,120)): -0.069,
                          ((46,47), (68,75)): -0.01, ((46,47), (83,84)): -0.072, ((46,47), (114,120)): 0.96,
                          ((68,75), (83,84)): -0.19, ((68,75), (114,120)): -0.081,
                          ((83,84), (114,120)): 0.99,
                          }
    positive_edges = [((46,47), (83,84)), ((83,84), (114,120)), ]
    negative_edges = [((27,42),(68,75)), ]
    extents_entities = (((1,9), (14,42),),
                        ((27,42),),
                        ((68,75),),
                        ((46,47),(83,84),(114,120),),
                        )
    
    configuration = Mapping()
    decoder = ConstrainedBestFirstCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"scores_are_proba": scores_are_proba, "positive_edges": positive_edges, "negative_edges": negative_edges}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    return data



def get_constrained_extended_best_first_decoder_decode_test_data():
    data = []
    
    test_document = get_test_document_data()
    ne_extent = NULL_MENTION.extent
    
    # mention_extents = ((1,9), (14,42), (27,42), (46,47), (68,75), (83,84), (114,120))
    # (46,47) and (83,84) are pronouns
    # scores_are_proba = True
    scores_are_proba = True
    coreference_scores = {(ne_extent, (1,9)): 0.7, (ne_extent, (14,42)): 0.3, (ne_extent, (27,42)): 0.6, 
                          (ne_extent, (46,47)): 0.55, (ne_extent, (68,75)): 0.78, (ne_extent, (83,84)): 0.51, (ne_extent, (114,120)): 0.0001, 
                          ((1,9), (14,42)): 0.6, ((1,9), (27,42)): 0.4, ((1,9), (46,47)): 0.2, ((1,9), (68,75)): 0.45, ((1,9), (83,84)): 0.19, ((1,9), (114,120)): 0.3,
                          ((14,42), (27,42)): 0.77, ((14,42), (46,47)): 0.1, ((14,42), (68,75)): 0.065, ((14,42), (83,84)): 0.07, ((14,42), (114,120)): 0.05,
                          ((27,42), (46,47)): 0.45, ((27,42), (68,75)): 0.89, ((27,42), (83,84)): 0.35, ((27,42), (114,120)): 0.069,
                          ((46,47), (68,75)): 0.01, ((46,47), (83,84)): 0.072, ((46,47), (114,120)): 0.06,
                          ((68,75), (83,84)): 0.19, ((68,75), (114,120)): 0.081,
                          ((83,84), (114,120)): 0.09,
                          }
    positive_edges = [((46,47), (83,84)), ((83,84), (114,120)), ]
    negative_edges = [((27,42),(68,75)), ]
    extents_entities = (((1,9), (14,42), (27,42), ),
                        ((68,75),),
                        ((46,47),(83,84),(114,120),),
                        )
    expected_result = CoreferencePartition(entities=extents_entities)
    
    configuration = Mapping()
    decoder = ConstrainedExtendedBestFirstCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"scores_are_proba": scores_are_proba, "positive_edges": positive_edges, "negative_edges": negative_edges}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    # scores_are_proba = False
    scores_are_proba = False
    coreference_scores = {(ne_extent, (1,9)): 0.7, (ne_extent, (14,42)): -0.3, (ne_extent, (27,42)): 0.6, 
                          (ne_extent, (46,47)): 0.55, (ne_extent, (68,75)): 0.78, (ne_extent, (83,84)): 0.51, (ne_extent, (114,120)): -0.0001, 
                          ((1,9), (14,42)): 0.6, ((1,9), (27,42)): -0.4, ((1,9), (46,47)): -0.2, ((1,9), (68,75)): -0.45, ((1,9), (83,84)): -0.19, ((1,9), (114,120)): -0.3,
                          ((14,42), (27,42)): 0.77, ((14,42), (46,47)): -0.1, ((14,42), (68,75)): -0.065, ((14,42), (83,84)): -0.07, ((14,42), (114,120)): -0.05,
                          ((27,42), (46,47)): -0.45, ((27,42), (68,75)): 0.89, ((27,42), (83,84)): -0.35, ((27,42), (114,120)): -0.069,
                          ((46,47), (68,75)): -0.01, ((46,47), (83,84)): -0.072, ((46,47), (114,120)): -0.06,
                          ((68,75), (83,84)): -0.19, ((68,75), (114,120)): -0.081,
                          ((83,84), (114,120)): -0.09,
                          }
    positive_edges = [((46,47), (83,84)), ((83,84), (114,120)), ]
    negative_edges = [((27,42),(68,75)), ]
    extents_entities = (((1,9), (14,42), (27,42),),
                        ((68,75),),
                        ((46,47),(83,84),(114,120),),
                        )
    
    configuration = Mapping()
    decoder = ConstrainedExtendedBestFirstCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"scores_are_proba": scores_are_proba, "positive_edges": positive_edges, "negative_edges": negative_edges}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    return data




if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()