# -*- coding: utf-8 -*-

import unittest

from cortex.utils.memoize import Memoized
from cortex.api.coreference_partition import CoreferencePartition
from cortex.coreference.decoder.mst import (MSTPrimCoreferenceDecoder, 
                                            MSTKruskalCoreferenceDecoder, 
                                            HybridBestFirstMSTCoreferenceDecoder,)
from cortex.tools import Mapping
from cortex.api.markable import NULL_MENTION

#@unittest.skip
class TestMSTPrimCoreferenceDecoder(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        configuration = Mapping({"useless_field": "useless_value"})
        decoder = MSTPrimCoreferenceDecoder(configuration=configuration)
        
        # Test
        self.assertEqual(configuration, decoder.configuration)
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        decoder1 = MSTPrimCoreferenceDecoder(configuration=Mapping({"witness_field": "witness_value"}))
        decoder2 = MSTPrimCoreferenceDecoder(configuration=Mapping({"witness_field": "witness_value"}))
        decoder3 = MSTPrimCoreferenceDecoder(configuration=Mapping({"witness_field2": "witness_value2"}))
        
        # Test
        self.assertTrue(*decoder1._inner_eq(decoder2))
        self.assertEqual((False,"configuration"), decoder1._inner_eq(decoder3))
    
    #@unittest.skip
    def test_from_configuration(self):
        # Test parameters
        configuration = Mapping({"useless_field": "useless_value"})
        expected_result = MSTPrimCoreferenceDecoder(configuration=configuration)
        
        # Test
        actual_result = MSTPrimCoreferenceDecoder.from_configuration(configuration)
        self.assertTrue(*expected_result._inner_eq(actual_result))
    
    #@unittest.skip
    def test_decode(self):
        # Test parameters
        data = get_mst_prim_decoder_decode_test_data()
        
        # Test
        for decoder, (document, coreference_scores, kwargs), expected_result in data:
            actual_result = decoder.decode(document, coreference_scores, **kwargs)
            self.assertEqual(expected_result, actual_result)



#@unittest.skip
class TestMSTKruskalCoreferenceDecoder(unittest.TestCase):
    
    #@unittest.skip
    def test_decode(self):
        # Test parameters
        data = get_mst_kruskal_decoder_decode_test_data()
        
        # Test
        for decoder, (document, coreference_scores, kwargs), expected_result in data:
            actual_result = decoder.decode(document, coreference_scores, **kwargs)
            self.assertEqual(expected_result, actual_result)



#@unittest.skip
class TestHybridBestFirstMSTCoreferenceDecoder(unittest.TestCase):
    
    #@unittest.skip
    def test_decode(self):
        # Test parameters
        data = get_hybrid_best_first_mst_decoder_decode_test_data()
        
        # Test
        for decoder, (document, coreference_scores, kwargs), expected_result in data:
            actual_result = decoder.decode(document, coreference_scores, **kwargs)
            self.assertEqual(expected_result, actual_result)




@Memoized
def get_test_document_data():
    import os
    from cortex.io.pivot_reader import PivotReader
    
    root_folder_path, file_name = os.path.split(__file__)
    file_name, _ = os.path.splitext(file_name)
    folder_path = os.path.join(root_folder_path, "{}_data".format(file_name))
    document_folder_path = os.path.join(folder_path, "decoder_test_data", "ref")
    
    strict = True # We want to synchronize head tokens.
    test_document = PivotReader.parse(document_folder_path, strict=strict)
    
    return test_document


def get_mst_prim_decoder_decode_test_data():
    data = []
    nm_extent = NULL_MENTION.extent
    
    test_document = get_test_document_data()
    
    # mention_extents = ((1,9), (14,42), (46,47), (83,84),)
    # scores_are_proba = True
    scores_are_proba = True
    coreference_scores = {(nm_extent, (1,9)): 0.68, 
                          ((1,9), (14,42)): 0.65, 
                          (nm_extent, (14,42)): 0.48, 
                          ((14,42), (46,47)): 0.1, 
                          ((1,9), (46,47)): 0.29, 
                          (nm_extent, (46,47)): 0.38, 
                          ((46,47), (83,84)): 0.36,
                          ((14,42), (83,84)): 0.89, 
                          ((1,9), (83,84)): 0.78,
                          (nm_extent, (83,84)): 0.58, 
                          }
    extents_entities = (((1,9), (14,42), (83,84)), 
                        ((27,42),),
                        ((46,47),),
                        ((68,75),),
                        ((114, 120),),
                        )
    expected_result = CoreferencePartition(entities=extents_entities)
    
    configuration = Mapping()
    decoder = MSTPrimCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"scores_are_proba": scores_are_proba}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    
    # scores_are_proba = False
    _score_threshold = 0.5
    scores_are_proba = False
    coreference_scores = dict((k,-v) if v <= _score_threshold else (k,v) for k,v in coreference_scores.items())
    extents_entities = (((1,9), (14,42), (46,47), (83,84)), 
                        ((27,42),),
                        ((68,75),),
                        ((114, 120),),
                        )
    expected_result = CoreferencePartition(entities=extents_entities)
    
    configuration = Mapping()
    decoder = MSTPrimCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"scores_are_proba": scores_are_proba}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    return data



def get_mst_kruskal_decoder_decode_test_data():
    data = []
    nm_extent = NULL_MENTION.extent
    
    test_document = get_test_document_data()
    
    # mention_extents = ((1,9), (14,42), (27,42), (46,47), (68,75), (83,84), (114,120))
    # Without positives and negative edges inputs
    positive_edges = None
    negative_edges = None
    coreference_scores = {(nm_extent, (1,9)): 0.68, 
                          ((1,9), (14,42)): 0.65, 
                          (nm_extent, (14,42)): 0.48, 
                          ((14,42), (46,47)): 0.1, 
                          ((1,9), (46,47)): 0.29, 
                          (nm_extent, (46,47)): 0.38, 
                          ((46,47), (83,84)): 0.36,
                          ((14,42), (83,84)): 0.89, 
                          ((1,9), (83,84)): 0.78,
                          (nm_extent, (83,84)): 0.58, 
                          }
    extents_entities = (((1,9), (14,42), (83,84)), 
                        ((27,42),),
                        ((46,47),),
                        ((68,75),),
                        ((114, 120),),
                        )
    expected_result = CoreferencePartition(entities=extents_entities)
    
    configuration = Mapping()
    decoder = MSTKruskalCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"positive_edges": positive_edges, "negative_edges": negative_edges}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    
    # With positives and negative edges inputs
    positive_edges = (((1,9), (46,47)),)
    negative_edges = (((1,9), (83,84)),)
    coreference_scores = {(nm_extent, (1,9)): 0.68, 
                          ((1,9), (14,42)): 0.65, 
                          (nm_extent, (14,42)): 0.48, 
                          ((14,42), (46,47)): 0.1, 
                          ((1,9), (46,47)): 0.29, 
                          (nm_extent, (46,47)): 0.38, 
                          ((46,47), (83,84)): 0.36,
                          ((14,42), (83,84)): 0.09, 
                          ((1,9), (83,84)): 0.78,
                          (nm_extent, (83,84)): 0.58, 
                          }
    extents_entities = (((1,9), (14,42), (46,47)), 
                        ((27,42),),
                        ((68,75),),
                        ((83,84),),
                        ((114, 120),),
                        )
    expected_result = CoreferencePartition(entities=extents_entities)
    
    configuration = Mapping()
    decoder = MSTKruskalCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"positive_edges": positive_edges, "negative_edges": negative_edges}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    return data



def get_hybrid_best_first_mst_decoder_decode_test_data():
    data = []
    nm_extent = NULL_MENTION.extent
    
    test_document = get_test_document_data()
    
    # mention_extents = ((1,9), (14,42), (27,42), (46,47), (68,75), (83,84), (114,120))
    # (46,47) and (83,84) are pronouns
    # scores_are_proba = True
    positive_edges = None
    negative_edges = None
    coreference_scores = {(nm_extent, (1,9)): 0.68, 
                          ((1,9), (14,42)): 0.65, 
                          (nm_extent, (14,42)): 0.48, 
                          ((14,42), (46,47)): 0.1,
                          ((1,9), (46,47)): 0.29,
                          (nm_extent, (46,47)): 0.38, 
                          ((46,47), (83,84)): 0.36, #
                          ((14,42), (83,84)): 0.89, #
                          ((1,9), (83,84)): 0.78,
                          (nm_extent, (83,84)): 0.58, #
                          }
    extents_entities = (((1,9), (14,42), (46,47), (83,84)), 
                        ((27,42),),
                        ((68,75),),
                        ((114, 120),),
                        )
    expected_result = CoreferencePartition(entities=extents_entities)
    
    configuration = Mapping()
    decoder = HybridBestFirstMSTCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"positive_edges": positive_edges, "negative_edges": negative_edges}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    
    # scores_are_proba = False
    positive_edges = None
    negative_edges = None
    coreference_scores = {(nm_extent, (1,9)): 0.68, 
                          ((1,9), (14,42)): 0.65, 
                          (nm_extent, (14,42)): -0.48, 
                          ((14,42), (46,47)): -0.1,
                          ((1,9), (46,47)): -0.29,
                          (nm_extent, (46,47)): -0.38, 
                          ((46,47), (83,84)): -0.36, #
                          ((14,42), (83,84)): 0.89, #
                          ((1,9), (83,84)): 0.78,
                          (nm_extent, (83,84)): 0.58, #
                          }
    extents_entities = (((1,9), (14,42), (83,84)), 
                        ((27,42),),
                        ((46,47),), 
                        ((68,75),),
                        ((114, 120),),
                        )
    expected_result = CoreferencePartition(entities=extents_entities)
    
    configuration = Mapping()
    decoder = HybridBestFirstMSTCoreferenceDecoder.from_configuration(configuration)
    kwargs = {"positive_edges": positive_edges, "negative_edges": negative_edges}
    
    data.append((decoder, (test_document, coreference_scores, kwargs), expected_result))
    
    return data



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()