# -*- coding: utf-8 -*-

import unittest

from cortex.coreference.decoder import (create_decoder_from_factory_configuration, ClosestFirstCoreferenceDecoder, AggressiveMergeCoreferenceDecoder, 
                                        OracleAggressiveMergeCoreferenceDecoder, BestFirstCoreferenceDecoder, BestNextCoreferenceDecoder, 
                                        BestLinkCoreferenceDecoder, MixedFirstCoreferenceDecoder, NBestFirstCoreferenceDecoder, 
                                        NBestLinkCoreferenceDecoder, PermutationCoreferenceDecoder, HACCoreferenceDecoder, 
                                        ExtendedBestFirstCoreferenceDecoder, 
                                        ConstrainedBestFirstCoreferenceDecoder, 
                                        ConstrainedExtendedBestFirstCoreferenceDecoder, 
                                        )
from cortex.coreference.decoder.mst import (MSTPrimCoreferenceDecoder, MSTKruskalCoreferenceDecoder, 
                                            HybridBestFirstMSTCoreferenceDecoder)
from cortex.tools import Mapping

class Test(unittest.TestCase):

    def test_create_decoder_from_factory_configuration(self):
        # Test parameters
        data = []
        
        configuration = Mapping({"name": "closestfirst", "config": Mapping({"witness_field": "witness_value"})})
        expected_result = ClosestFirstCoreferenceDecoder.from_configuration(Mapping({"witness_field": "witness_value"}))
        data.append((configuration, expected_result))
        
        configuration = Mapping({"name": "aggressivemerge", "config": Mapping({"witness_field": "witness_value"})})
        expected_result = AggressiveMergeCoreferenceDecoder.from_configuration(Mapping({"witness_field": "witness_value"}))
        data.append((configuration, expected_result))
        
        configuration = Mapping({"name": "oracleaggressivemerge", "config": Mapping({"witness_field": "witness_value"})})
        expected_result = OracleAggressiveMergeCoreferenceDecoder.from_configuration(Mapping({"witness_field": "witness_value"}))
        data.append((configuration, expected_result))
        
        configuration = Mapping({"name": "bestfirst", "config": Mapping({"witness_field": "witness_value"})})
        expected_result = BestFirstCoreferenceDecoder.from_configuration(Mapping({"witness_field": "witness_value"}))
        data.append((configuration, expected_result))
        
        configuration = Mapping({"name": "bestnext", "config": Mapping({"witness_field": "witness_value"})})
        expected_result = BestNextCoreferenceDecoder.from_configuration(Mapping({"witness_field": "witness_value"}))
        data.append((configuration, expected_result))
        
        configuration = Mapping({"name": "bestlink", "config": Mapping({"witness_field": "witness_value"})})
        expected_result = BestLinkCoreferenceDecoder.from_configuration(Mapping({"witness_field": "witness_value"}))
        data.append((configuration, expected_result))
        
        configuration = Mapping({"name": "mixedfirst", "config": Mapping({"witness_field": "witness_value"})})
        expected_result = MixedFirstCoreferenceDecoder.from_configuration(Mapping({"witness_field": "witness_value"}))
        data.append((configuration, expected_result))
        
        n_best = 3
        configuration = Mapping({"name": "nbestfirst", "config": Mapping({"n_best": n_best, "witness_field": "witness_value"})})
        expected_result = NBestFirstCoreferenceDecoder.from_configuration(Mapping({"n_best": n_best, "witness_field": "witness_value"}))
        data.append((configuration, expected_result))
        
        n_best = 2
        configuration = Mapping({"name": "nbestlink", "config": Mapping({"n_best": n_best, "witness_field": "witness_value"})})
        expected_result = NBestLinkCoreferenceDecoder.from_configuration(Mapping({"n_best": n_best, "witness_field": "witness_value"}))
        data.append((configuration, expected_result))
        
        configuration = Mapping({"name": "permutation", "config": Mapping({"witness_field": "witness_value"})})
        expected_result = PermutationCoreferenceDecoder.from_configuration(Mapping({"witness_field": "witness_value"}))
        data.append((configuration, expected_result))
        
        configuration = Mapping({"name": "hac", "config": Mapping({"clustering_type": "single_link", "witness_field": "witness_value"})})
        expected_result = HACCoreferenceDecoder.from_configuration(Mapping({"clustering_type": "single_link", "witness_field": "witness_value"}))
        data.append((configuration, expected_result))
        
        configuration = Mapping({"name": "extendedbestfirst", "config": Mapping({"witness_field": "witness_value"})})
        expected_result = ExtendedBestFirstCoreferenceDecoder.from_configuration(Mapping({"witness_field": "witness_value"}))
        data.append((configuration, expected_result))
        
        configuration = Mapping({"name": "constrainedbestfirst", "config": Mapping({"witness_field": "witness_value"})})
        expected_result = ConstrainedBestFirstCoreferenceDecoder.from_configuration(Mapping({"witness_field": "witness_value"}))
        data.append((configuration, expected_result))
        
        configuration = Mapping({"name": "constrainedextendedbestfirst", "config": Mapping({"witness_field": "witness_value"})})
        expected_result = ConstrainedExtendedBestFirstCoreferenceDecoder.from_configuration(Mapping({"witness_field": "witness_value"}))
        data.append((configuration, expected_result))
        
        configuration = Mapping({"name": "mstprim", "config": Mapping({"witness_field": "witness_value"})})
        expected_result = MSTPrimCoreferenceDecoder.from_configuration(Mapping({"witness_field": "witness_value"}))
        data.append((configuration, expected_result))
        
        configuration = Mapping({"name": "mstkruskal", "config": Mapping({"witness_field": "witness_value"})})
        expected_result = MSTKruskalCoreferenceDecoder.from_configuration(Mapping({"witness_field": "witness_value"}))
        data.append((configuration, expected_result))
        
        configuration = Mapping({"name": "hybridbestfirstmst", "config": Mapping({"witness_field": "witness_value"})})
        expected_result = HybridBestFirstMSTCoreferenceDecoder.from_configuration(Mapping({"witness_field": "witness_value"}))
        data.append((configuration, expected_result))
                            
        # Test
        for configuration, expected_result in data:
            actual_result = create_decoder_from_factory_configuration(configuration)
            self.assertTrue(*expected_result._inner_eq(actual_result))


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()