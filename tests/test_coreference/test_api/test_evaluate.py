# -*- coding: utf-8 -*-

import unittest
import os
import logging
import sys

from cortex.utils.memoize import Memoized
from cortex.api.corpus import Corpus
from cortex.api.document_buffer import deepcopy_document
from cortex.coreference.resolver import load_resolver
from cortex.coreference.evaluation.conll2012_scorer import format_evaluation_result
from cortex.coreference.api.evaluate import evaluate_resolver


#@unittest.skip  
class Test(unittest.TestCase):
    
    #@unittest.skip  
    def test_evaluate_resolver(self):
        # Test parameters
        args, kwargs = get_test_evaluate_resolver_data()
        
        FORMAT = '%(asctime)-15s :: %(levelname)-8s :: %(message)s'
        handler = logging.StreamHandler(stream=sys.stdout)
        logging_lvl = getattr(logging, "DEBUG")
        logging.basicConfig(format=FORMAT, level=logging_lvl, handlers=[handler])
        
        # Test
        metric_overall_results, metric_name2results_per_document = evaluate_resolver(*args, **kwargs)
        avg_report, document_reports = format_evaluation_result(metric_overall_results, 
                                                                 metric_name2results_per_document, 
                                                                 detailed=True)
        print(avg_report)
        print()
        for document_report in document_reports:
            print(document_report)
        #raise Exception


def get_test_document_data():
    from cortex.io.pivot_reader import PivotReader
    
    folder_path = os.path.join(os.path.dirname(__file__), "data")
    document_folder_path = os.path.join(folder_path, "ref")
    
    strict = True # We want to assess the synchronization of head tokens.
    test_document = PivotReader.parse(document_folder_path, strict=strict)
    
    return test_document

@Memoized
def get_memoized_test_document_data():
    return get_test_document_data()


def get_test_evaluate_resolver_data():
    archive_file_name = "test_soon_resolver_load.zip"
    archive_file_path = os.path.join(os.path.dirname(__file__), "data", "test_evaluate", archive_file_name)
    resolver = load_resolver(archive_file_path)
    
    test_document = get_memoized_test_document_data()
    test_document2 = deepcopy_document(test_document)
    test_document2.coreference_partition = None
    test_document3 = deepcopy_document(test_document)
    test_document3.ident += "_2"
    documents = [test_document, 
                 test_document2, 
                 test_document3,
                 ]
    test_corpus = Corpus(documents)
    
    args = (resolver, test_corpus)
    kwargs = {"metric": "all", "skip_singletons": False, "retrieve_individual_document_data": True,
              "document_preprocessing_identifier_cache_parameter": "test", 
              "detect_mentions": True,
              #"detect_mentions": False,
              }
    
    return args, kwargs


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()