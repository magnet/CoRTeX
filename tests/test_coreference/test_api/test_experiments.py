# -*- coding: utf-8 -*-

import unittest
import os
import logging
import sys

from cortex.coreference.api.experiments import (carry_out_efficient_online_interface_based_resolver_comparison_experiment, 
                                                carry_out_efficient_online_interface_based_resolver_comparison_single_experiment, 
                                                carry_out_resolver_grid_search, 
                                                create_kfold_corpora_files_folder,
                                                parse_kfold_corpora,
                                                )
from cortex.tools import Mapping
from cortex.utils import get_parameter_grids
from collections import OrderedDict
from cortex.utils.io import TemporaryDirectory


#@unittest.skip  
class Test(unittest.TestCase):
    
    #@unittest.skip
    def test_create_kfold_corpora_files_folder(self):
        # Test parameters
        test_data_folder_path = os.path.join(os.path.dirname(__file__), "data")
        corpus_file_path = os.path.join(test_data_folder_path, "corpora", "cross_validation.txt")
        
        n_splits = 4
        shuffle = True
        random_state = 42
        expected_data = []
        ## CV1
        expected_train_file_name = "CV1_train.txt"
        expected_train_file_content = """{"format_": "pivot"}
cnn_0001__part__0
cnn_0001__part__2
cnn_0001__part__3"""
        expected_test_file_name = "CV1_test.txt"
        expected_test_file_content = """{"format_": "pivot"}
cnn_0001__part__1"""
        expected_data.append(((expected_train_file_name, expected_train_file_content), (expected_test_file_name, expected_test_file_content)))
        ## CV2
        expected_train_file_name = "CV2_train.txt"
        expected_train_file_content = """{"format_": "pivot"}
cnn_0001__part__0
cnn_0001__part__1
cnn_0001__part__2"""
        expected_test_file_name = "CV2_test.txt"
        expected_test_file_content = """{"format_": "pivot"}
cnn_0001__part__3"""
        expected_data.append(((expected_train_file_name, expected_train_file_content), (expected_test_file_name, expected_test_file_content)))
        ## CV3
        expected_train_file_name = "CV3_train.txt"
        expected_train_file_content = """{"format_": "pivot"}
cnn_0001__part__1
cnn_0001__part__2
cnn_0001__part__3"""
        expected_test_file_name = "CV3_test.txt"
        expected_test_file_content = """{"format_": "pivot"}
cnn_0001__part__0"""
        expected_data.append(((expected_train_file_name, expected_train_file_content), (expected_test_file_name, expected_test_file_content)))
        ## CV4
        expected_train_file_name = "CV4_train.txt"
        expected_train_file_content = """{"format_": "pivot"}
cnn_0001__part__0
cnn_0001__part__1
cnn_0001__part__3"""
        expected_test_file_name = "CV4_test.txt"
        expected_test_file_content = """{"format_": "pivot"}
cnn_0001__part__2"""
        expected_data.append(((expected_train_file_name, expected_train_file_content), (expected_test_file_name, expected_test_file_content)))
        
        # Test
        with TemporaryDirectory(delete_upon_exit=True) as temp_dir:
            output_folder_path = temp_dir.temporary_folder_path
            self.assertEqual(os.listdir(output_folder_path), [])
            create_kfold_corpora_files_folder(corpus_file_path, output_folder_path, n_splits=n_splits, 
                                              shuffle=shuffle, random_state=random_state)
            self.assertEqual(len(output_folder_path), 4*4)
            for expected_file_name_file_content_pairs in expected_data:
                for expected_file_name, expected_file_content in expected_file_name_file_content_pairs:
                    expected_file_path = os.path.join(output_folder_path, expected_file_name)
                    self.assertTrue(os.path.isfile(expected_file_path))
                    with open(expected_file_path, "r", encoding="utf-8") as f:
                        actual_file_content = f.read()
                    self.assertEqual(expected_file_content, actual_file_content)
            
    #@unittest.skip
    def test_parse_kfold_corpora(self):
        # Test parameters
        test_data_folder_path = os.path.join(os.path.dirname(__file__), "data")
        documents_root_folder_path = os.path.join(test_data_folder_path, "corpora", "documents")
        cv_corpora_folder_path = os.path.join(test_data_folder_path, "cv_corpora")
        
        expected_cv_corpora_configurations = []
        cv_corpora_configuration = Mapping({"train": Mapping({"corpus_file_path": os.path.join(cv_corpora_folder_path, "CV1_train.txt"), 
                                                              "documents_root_folder_path": documents_root_folder_path,
                                                              }), 
                                            "test": Mapping({"corpus_file_path": os.path.join(cv_corpora_folder_path, "CV1_test.txt"), 
                                                             "documents_root_folder_path": documents_root_folder_path,
                                                             })
                                            })
        expected_cv_corpora_configurations.append(cv_corpora_configuration)
        cv_corpora_configuration = Mapping({"train": Mapping({"corpus_file_path": os.path.join(cv_corpora_folder_path, "CV2_train.txt"), 
                                                              "documents_root_folder_path": documents_root_folder_path,
                                                              }), 
                                            "test": Mapping({"corpus_file_path": os.path.join(cv_corpora_folder_path, "CV2_test.txt"), 
                                                             "documents_root_folder_path": documents_root_folder_path,
                                                             })
                                            })
        expected_cv_corpora_configurations.append(cv_corpora_configuration)
        cv_corpora_configuration = Mapping({"train": Mapping({"corpus_file_path": os.path.join(cv_corpora_folder_path, "CV3_train.txt"), 
                                                              "documents_root_folder_path": documents_root_folder_path,
                                                              }), 
                                            "test": Mapping({"corpus_file_path": os.path.join(cv_corpora_folder_path, "CV3_test.txt"), 
                                                             "documents_root_folder_path": documents_root_folder_path,
                                                             })
                                            })
        expected_cv_corpora_configurations.append(cv_corpora_configuration)
        cv_corpora_configuration = Mapping({"train": Mapping({"corpus_file_path": os.path.join(cv_corpora_folder_path, "CV4_train.txt"), 
                                                              "documents_root_folder_path": documents_root_folder_path,
                                                              }), 
                                            "test": Mapping({"corpus_file_path": os.path.join(cv_corpora_folder_path, "CV4_test.txt"), 
                                                             "documents_root_folder_path": documents_root_folder_path,
                                                             })
                                            })
        expected_cv_corpora_configurations.append(cv_corpora_configuration)
        expected_cv_corpora_configurations = tuple(expected_cv_corpora_configurations)
        
        # Test
        actual_cv_corpora_configuration = parse_kfold_corpora(cv_corpora_folder_path, documents_root_folder_path)
        self.assertEqual(expected_cv_corpora_configurations, actual_cv_corpora_configuration)
    
    #@unittest.skip
    def test_carry_out_efficient_online_interface_based_resolver_comparison_single_experiment(self):
        FORMAT = '%(asctime)-15s :: %(levelname)-8s :: %(message)s'
        handler = logging.StreamHandler(stream=sys.stdout)
        logging_lvl = getattr(logging, "INFO")
        logging.basicConfig(format=FORMAT, level=logging_lvl, handlers=[handler])
        
        # Change the current working directory value so that relative path-based applications work
        current_working_folder_path = os.getcwd()
        os.chdir(os.path.dirname(__file__))
        
        # Test parameter
        test_data_folder_path = os.path.join("data")
        document_preprocessing_version = "test_document_preprocessing_version"
        train_corpus_file_path = os.path.join(test_data_folder_path, "corpora", "train.txt")
        test_corpus_file_path = os.path.join(test_data_folder_path, "corpora", "test.txt")
        document_root_folder_path = os.path.join(test_data_folder_path, "corpora", "documents")
        evaluation_configuration = Mapping({"skip_singletons": False, "detect_mentions": False, 
                                            "retrieve_individual_document_data": True})
        corpora_configuration = Mapping({"train": {"corpus_file_path": train_corpus_file_path, "documents_root_folder_path": document_root_folder_path}, 
                                         "test": {"corpus_file_path": test_corpus_file_path, "documents_root_folder_path": document_root_folder_path}
                                        })
        ## Resolver configuration
        model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "ExtendedMentionPairSampleFeaturesVectorizer",
                                                                                        "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                           "mention_hierarchy": Mapping({"name": "mentiongramtype", "config": Mapping()}), 
                                                                                                           "with_singleton_features": False, 
                                                                                                           "with_anaphoricity_features": False, 
                                                                                                           "quantize": True,
                                                                                                           })
                                                                                        }), 
                                                 "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                        "config": Mapping({"iteration_nb": 2, 
                                                                                           "avg": True, 
                                                                                           "warm_start": False, 
                                                                                           "alpha": 1.52,
                                                                                           }),
                                                                        })
                                                })
        generator_configuration = Mapping({"pair_filter": Mapping({"name": "PairFiltersCombination", 
                                                                   "config": Mapping({"filters": [Mapping({"name": "AcceptAllPairFilter"}),]}),
                                                                   }),
                                           })
        decoder_configuration = Mapping()
        root_loss = 1.2
        update_mode = "ML"
        configuration = Mapping({"model_interface": Mapping({"name": "OnlineInterface", "config": model_interface_configuration}),
                                  "generator": Mapping({"name": "ExtendedMentionPairSampleGenerator", "config": generator_configuration}),
                                  "decoder": Mapping({"name": "MSTPrimCoreferenceDecoder", "config": decoder_configuration}),
                                  "root_loss": root_loss, 
                                  "update_mode": update_mode, 
                                 })
        resolver_configuration = Mapping({"name": "extendedmst", "config": configuration})
        
        iterations_nb = [5, 10, 15, 20]
        
        # Test
        iterations_nb2evaluation_result =\
         carry_out_efficient_online_interface_based_resolver_comparison_single_experiment(resolver_configuration, 
                                                                                   iterations_nb, 
                                                                                   corpora_configuration, 
                                                                                   evaluation_configuration, 
                                                                                   document_preprocessing_version)
        evaluation_result_to_conll_score = lambda evaluation_result: evaluation_result[0]["conll"]["f1"]
        iterations_nb2conll_score = OrderedDict((iterations_nb, evaluation_result_to_conll_score(evaluation_result))\
                                                for iterations_nb, evaluation_result in iterations_nb2evaluation_result.items()
                                                )
        import pprint
        pprint.pprint(iterations_nb2conll_score)
        #pprint.pprint(iterations_nb2evaluation_result)
        #raise Exception()
        
        # Change back the current working directory value
        os.chdir(current_working_folder_path)
    
    #@unittest.skip  
    def test_carry_out_efficient_online_interface_based_resolver_comparison_experiment(self):
        FORMAT = '%(asctime)-15s :: %(levelname)-8s :: %(message)s'
        handler = logging.StreamHandler(stream=sys.stdout)
        logging_lvl = getattr(logging, "INFO")
        logging.basicConfig(format=FORMAT, level=logging_lvl, handlers=[handler])
        
        # Change the current working directory value so that relative path-based applications work
        current_working_folder_path = os.getcwd()
        os.chdir(os.path.dirname(__file__))
        
        # Test parameter
        test_data_folder_path = os.path.join("data")
        document_preprocessing_version = "test_document_preprocessing_version"
        train_corpus_file_path = os.path.join(test_data_folder_path, "corpora", "train.txt")
        test_corpus_file_path = os.path.join(test_data_folder_path, "corpora", "test.txt")
        document_root_folder_path = os.path.join(test_data_folder_path, "corpora", "documents")
        evaluation_configuration = Mapping({"skip_singletons": False, "detect_mentions": False, 
                                            "retrieve_individual_document_data": True})
        corpora_configuration = Mapping({"train": {"corpus_file_path": train_corpus_file_path, "documents_root_folder_path": document_root_folder_path}, 
                                         "test": {"corpus_file_path": test_corpus_file_path, "documents_root_folder_path": document_root_folder_path}
                                        })
        ## Resolver configuration
        resolver_factory_configuration_iteration_nbs_pairs = []
        ### MST
        model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "ExtendedMentionPairSampleFeaturesVectorizer",
                                                                                        "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                           "mention_hierarchy": Mapping({"name": "mentiongramtype", "config": Mapping()}), 
                                                                                                           "with_singleton_features": False, 
                                                                                                           "with_anaphoricity_features": False, 
                                                                                                           "quantize": True,
                                                                                                           })
                                                                                        }), 
                                                 "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                        "config": Mapping({"iteration_nb": 2, 
                                                                                           "avg": True, 
                                                                                           "warm_start": False, 
                                                                                           "alpha": 1.52,
                                                                                           }),
                                                                        })
                                                })
        generator_configuration = Mapping({"pair_filter": Mapping({"name": "PairFiltersCombination", 
                                                                   "config": Mapping({"filters": [Mapping({"name": "AcceptAllPairFilter"}),]}),
                                                                   }),
                                           })
        decoder_configuration = Mapping()
        root_loss = 1.2
        update_mode = "ML"
        configuration = Mapping({"model_interface": Mapping({"name": "OnlineInterface", "config": model_interface_configuration}),
                                  "generator": Mapping({"name": "ExtendedMentionPairSampleGenerator", "config": generator_configuration}),
                                  "decoder": Mapping({"name": "MSTPrimCoreferenceDecoder", "config": decoder_configuration}),
                                  "root_loss": root_loss, 
                                  "update_mode": update_mode, 
                                 })
        resolver_configuration = Mapping({"name": "extendedmst", "config": configuration})
        iterations_nb = [5, 10, 15, 20]
        
        resolver_factory_configuration_iteration_nbs_pairs.append((resolver_configuration, iterations_nb))
        
        ### HybridBestFirstMST
        root_loss = 1.5
        update_mode = "ML"
        
        iteration_nb = 42
        C = 14.0675
        avg = True
        warm_start = True
        configuration = Mapping({"generator": Mapping({"name": "MentionPairSampleGenerator",
                                                         "config": Mapping({"pair_filter": Mapping({"name": "acceptall", "config": Mapping()}),
                                                                            }),
                                                         }),
                                   "decoder": Mapping({"name": "HybridBestFirstMSTCoreferenceDecoder",
                                                       "config": Mapping(),
                                                       }),
                                   "model_interface": Mapping({"name": "OnlineInterface",
                                                               "config": Mapping({"sample_features_vectorizer": Mapping({"name": "ExtendedMentionPairSampleFeaturesVectorizer",
                                                                                                                        "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                                                           "mention_hierarchy": Mapping({"name": "mentiongramtype", "config": Mapping()}), 
                                                                                                                                           "with_singleton_features": False, 
                                                                                                                                           "with_anaphoricity_features": False, 
                                                                                                                                           "quantize": True,
                                                                                                                                           })
                                                                                                                        }), 
                                                                                  "classifier": Mapping({"name": "PAOnlineBinaryClassifier",
                                                                                                         "config": Mapping({"iteration_nb": iteration_nb,
                                                                                                                            "C": C,
                                                                                                                            "avg": avg,
                                                                                                                            "warm_start": warm_start, 
                                                                                                                            }),
                                                                                                         }),
                                                                                  }),
                                                               }),
                                   "root_loss": root_loss,
                                   "update_mode": update_mode,
                                   })
        resolver_configuration = Mapping({"name": "hybridbestfirstmst", "config": configuration})
        iterations_nb = [10, 15, 20, 25]
        
        resolver_factory_configuration_iteration_nbs_pairs.append((resolver_configuration, iterations_nb))
        
        # Test
        iteration_nb2evaluation_result_maps =\
         carry_out_efficient_online_interface_based_resolver_comparison_experiment(resolver_factory_configuration_iteration_nbs_pairs, 
                                                                                   corpora_configuration, 
                                                                                   evaluation_configuration, 
                                                                                   document_preprocessing_version)
        evaluation_result_to_conll_score = lambda evaluation_result: evaluation_result[0]["conll"]["f1"]
        iteration_nb2conll_score_maps = [OrderedDict((iteration_nb, evaluation_result_to_conll_score(evaluation_result))\
                                                    for iteration_nb, evaluation_result in iteration_nb2evaluation_result.items()
                                                    ) for iteration_nb2evaluation_result in iteration_nb2evaluation_result_maps]
        import pprint
        pprint.pprint(iteration_nb2conll_score_maps)
        #pprint.pprint(iteration_nb2evaluation_result_maps)
        #raise Exception()
        
        # Change back the current working directory value
        os.chdir(current_working_folder_path)
    
    #@unittest.skip  
    def test_carry_out_resolver_grid_search(self):
        FORMAT = '%(asctime)-15s :: %(levelname)-8s :: %(message)s'
        handler = logging.StreamHandler(stream=sys.stdout)
        #logging_lvl = getattr(logging, "INFO")
        logging_lvl = getattr(logging, "DEBUG")
        logging.basicConfig(format=FORMAT, level=logging_lvl, handlers=[handler])
        
        # Change the current working directory value so that relative path-based applications work
        current_working_folder_path = os.getcwd()
        os.chdir(os.path.dirname(__file__))
        
        # Test parameters
        resolver_name = "mst"
        parameter_name2parameter_possible_values =\
            {"classifier_factory_configuration": get_parameter_grids({"name": ["perceptrononlinebinary"], 
                                                                      "config": get_parameter_grids({"iteration_nb": [5,10]})
                                                                      }),
             "root_loss": [1.0, 5.0],
             "update_mode": ["ML", "PB"],
            }
        
        test_data_folder_path = os.path.join("data")
        train_corpus_file_path = os.path.join(test_data_folder_path, "corpora", "train.txt")
        test_corpus_file_path = os.path.join(test_data_folder_path, "corpora", "test.txt")
        document_root_folder_path = os.path.join(test_data_folder_path, "corpora", "documents")
        corpora_configuration = Mapping({"train": {"corpus_file_path": train_corpus_file_path, "documents_root_folder_path": document_root_folder_path}, 
                                         "test": {"corpus_file_path": test_corpus_file_path, "documents_root_folder_path": document_root_folder_path}
                                        })
        
        evaluation_configuration = Mapping({"skip_singletons": False, "retrieve_individual_document_data": True, 
                                            "detect_mentions": False})
        document_preprocessing_version = "test_document_preprocessing_version"
        evaluation_result_scorer = None
        
        args = (resolver_name, parameter_name2parameter_possible_values, corpora_configuration, 
                evaluation_configuration, document_preprocessing_version,)
        kwargs = {"evaluation_result_scorer": evaluation_result_scorer}
        
        # Test
        configuration_dict_evaluation_score_pairs = carry_out_resolver_grid_search(*args, **kwargs)
        
        import pprint
        pprint.pprint(configuration_dict_evaluation_score_pairs)
        #raise Exception()
        
        # Change back the current working directory value
        os.chdir(current_working_folder_path)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()