# -*- coding: utf-8 -*-

import unittest
import os
from collections import defaultdict
from scipy import sparse
import numpy
from copy import deepcopy

from cortex.api.document import Document
from cortex.utils.memoize import Memoized
from cortex.api.corpus import Corpus
from cortex.api.document_buffer import deepcopy_document
from cortex.coreference.resolver import load_resolver
from cortex.coreference.api.vectorization_cache import LocalVectorizationCache
from cortex.utils import are_array_equal
from cortex.utils.io import TemporaryDirectory
from cortex.tools import Mapping


#@unittest.skip
class TestLocalVectorizationCache(unittest.TestCase):
    
    def _compare_document_cache(self, expected, actual):
        expected_keys = set(expected.keys())
        actual_keys = set(actual.keys())
        if not expected_keys == actual_keys:
            return False, "document_cache_keys: {} v. {}".format(expected_keys, actual_keys)
        for key in expected_keys:
            expected_value = expected[key]
            actual_value = actual[key]
            self.assertEqual(type(expected_value), type(actual_value))
            if not isinstance(actual_value, sparse.csr_matrix):
                return False, "document_cache value: key = '{}', wrong type '{}', expected type = '{}'".format(key, type(actual_value), sparse.csr_matrix)
            if not are_array_equal(expected_value.todense(), actual_value.todense()):
                return False, "document_cache value: key = '{}', wrong value '{}', expected value = '{}'".format(key, expected_value.todense(), actual_value.todense())
        return True, None
    def _compare_cache(self, expected, actual):
        expected_keys = set(expected.keys())
        actual_keys = set(actual.keys())
        if not expected_keys == actual_keys:
            return False, "cache_keys: {} v. {}".format(expected_keys, actual_keys)
        for key in expected_keys:
            expected_value = expected[key]
            actual_value = actual[key]
            test, message = self._compare_document_cache(expected_value, actual_value)
            if not test:
                return False, "cache value: key = '{}': values are not equal, for the '{}' reason".format(key, message)
        return True, None
    
    #@unittest.skip
    def test(self):
        # Test parameters
        language = "en"
        document_preprocessing_version = "ver1" 
        language_parameter_version = "0.1.0"
        sample_type = "mention_pair"
        vectorizer_configuration = Mapping({"name": "mentionpair",
                                            "config": Mapping(),
                                            })
        documents = [Document("ident1", "raw_text1"), Document("ident2", "raw_text2")]
        vec1 = sparse.csr_matrix([[0,0,3.6,0,-42.0]], dtype=numpy.float)
        vec2 = sparse.csr_matrix([[2.8,0,3.6,0,-42.0,-6.0]], dtype=numpy.float)
        vec3 = sparse.csr_matrix([[2.8,0,3.6]], dtype=numpy.float)
        cache_doc1 = {"hash1": vec1, "hash3": vec3}
        cache_doc2 = {"hash2": vec2}
        document_hash2cache = defaultdict(dict)
        document_hash2cache["glop1"] = cache_doc1
        document_hash2cache["glop2"] = cache_doc2
        expected_document_hash2cache = deepcopy(document_hash2cache)
        
        # Test
        with TemporaryDirectory() as temp_dir:
            temp_folder_path = temp_dir.temporary_folder_path
            cortex_cache_folder_path = temp_folder_path
            
            # Test1: persistence using pickle
            persist_as_svmlight_like = False
            sparse_format = None
            ## First, save it
            with LocalVectorizationCache(cortex_cache_folder_path, language, document_preprocessing_version, 
                                        language_parameter_version, sample_type, vectorizer_configuration, documents, 
                                        persist_as_svmlight_like=persist_as_svmlight_like, sparse_format=sparse_format) as local_vectorization_cache:
                local_vectorization_cache.document_hash2cache.update(document_hash2cache)
            
            ## Second load it back
            with LocalVectorizationCache(cortex_cache_folder_path, language, document_preprocessing_version, 
                                        language_parameter_version, sample_type, vectorizer_configuration, documents, 
                                        persist_as_svmlight_like=persist_as_svmlight_like, sparse_format=sparse_format) as local_vectorization_cache:
                actual_document_hash2cache = local_vectorization_cache.document_hash2cache
                self._compare_cache(expected_document_hash2cache, actual_document_hash2cache)
            
            # Test2: persistence using persist_as_svmlight_like-like format
            persist_as_svmlight_like = True
            sparse_format = True
            ## First, save it
            with LocalVectorizationCache(cortex_cache_folder_path, language, document_preprocessing_version, 
                                        language_parameter_version, sample_type, vectorizer_configuration, documents, 
                                        persist_as_svmlight_like=persist_as_svmlight_like, sparse_format=sparse_format) as local_vectorization_cache:
                local_vectorization_cache.document_hash2cache.update(document_hash2cache)
            
            ## Second load it back
            with LocalVectorizationCache(cortex_cache_folder_path, language, document_preprocessing_version, 
                                        language_parameter_version, sample_type, vectorizer_configuration, documents, 
                                        persist_as_svmlight_like=persist_as_svmlight_like, sparse_format=sparse_format) as local_vectorization_cache:
                actual_document_hash2cache = local_vectorization_cache.document_hash2cache
                self._compare_cache(expected_document_hash2cache, actual_document_hash2cache)




def get_test_document_data():
    from cortex.io.pivot_reader import PivotReader
    
    folder_path = os.path.join(os.path.dirname(__file__), "data")
    document_folder_path = os.path.join(folder_path, "ref")
    
    strict = True # We want to assess the synchronization of head tokens.
    test_document = PivotReader.parse(document_folder_path, strict=strict)
    
    return test_document

@Memoized
def get_memoized_test_document_data():
    return get_test_document_data()


def get_test_evaluate_resolver_data():
    archive_file_name = "test_soon_resolver_load.zip"
    archive_file_path = os.path.join(os.path.dirname(__file__), "test_resolver", "data", "select_", "test_load_resolver", archive_file_name)
    resolver = load_resolver(archive_file_path)
    
    test_document = get_memoized_test_document_data()
    test_document2 = deepcopy_document(test_document)
    test_document2.coreference_partition = None
    test_document3 = deepcopy_document(test_document)
    documents = [test_document, 
                 test_document2, 
                 test_document3,
                 ]
    test_corpus = Corpus(documents)
    
    args = (resolver, test_corpus)
    kwargs = {"metric": "all", "skip_singletons": False, "retrieve_individual_document_data": True,
              #"document_preprocessing_identifier_cache_parameter": "test", 
              }
    
    return args, kwargs

'''
model_folder_path = os.path.join(os.path.dirname(__file__), "data", "pairs", "train_test_sklearn_model_interface")
resolver.model_interface.save_model(model_folder_path)
'''


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()