# -*- coding: utf-8 -*-

import unittest
import os
import logging
import sys
import shutil

from cortex.coreference.api.resolver_comparison_experiment import (ResolverComparisonExperiment, 
                                                                   ExistingResolverComparisonExperimentError,)
from cortex.utils.io import TemporaryDirectory
from cortex.tools import Mapping, deepcopy_mapping


#@unittest.skip
class TestResolverComparisonExperiment(unittest.TestCase):
    
    #@unittest.skip
    def test_separate_factory_configuration(self):
        # Test parameters
        ## resolver_ident2factory_configuration data
        resolver_ident2factory_configuration = create_resolver_ident2factory_configuration_data()
        
        extendedmst_factory_configuration_without_iteration_nb = deepcopy_mapping(resolver_ident2factory_configuration["extendedmst_iteration_nb=5"])
        del extendedmst_factory_configuration_without_iteration_nb["config"]["model_interface"]["config"]["classifier"]["config"]["iteration_nb"]
        
        mst_factory_configuration_without_iteration_nb = deepcopy_mapping(resolver_ident2factory_configuration["mst_iteration_nb=25"])
        del mst_factory_configuration_without_iteration_nb["config"]["model_interface"]["config"]["classifier"]["config"]["iteration_nb"]
        
        expected_factory_configuration_and_resolver_ident2iteration_nb_pairs = [(extendedmst_factory_configuration_without_iteration_nb, 
                                                                                 {"extendedmst_iteration_nb={}".format(iteration_nb): iteration_nb for iteration_nb in [5,10,15]}),
                                                                                (mst_factory_configuration_without_iteration_nb, 
                                                                                 {"mst_iteration_nb={}".format(iteration_nb): iteration_nb for iteration_nb in [25, 30, 35]}),
                                                                                ]
        expected_remaining_resolver_ident2factory_configuration = {"soon": resolver_ident2factory_configuration["soon"],
                                                                   "hybridbestfirstmst": resolver_ident2factory_configuration["hybridbestfirstmst"],
                                                                   }
        
        def _compare_factory_configuration_and_resolver_ident2iteration_nb_pairs(input1, input2):
            dict1 = {t[0]["name"]: t for t in input1}
            dict2 = {t[0]["name"]: t for t in input2}
            self.assertEqual(dict1, dict2)
        
        # Test
        (actual_factory_configuration_and_resolver_ident2iteration_nb_pairs, 
         actual_remaining_resolver_ident2factory_configuration) =\
          ResolverComparisonExperiment._separate_factory_configuration(resolver_ident2factory_configuration)
        self.assertEqual(expected_remaining_resolver_ident2factory_configuration, 
                         actual_remaining_resolver_ident2factory_configuration)
        _compare_factory_configuration_and_resolver_ident2iteration_nb_pairs(expected_factory_configuration_and_resolver_ident2iteration_nb_pairs, 
                                                                             actual_factory_configuration_and_resolver_ident2iteration_nb_pairs)
    
    #@unittest.skip
    def test_initialize_identical_experiment(self):
        FORMAT = '%(asctime)-15s :: %(levelname)-8s :: %(message)s'
        handler = logging.StreamHandler(stream=sys.stdout)
        logging_lvl = getattr(logging, "DEBUG")
        logging.basicConfig(format=FORMAT, level=logging_lvl, handlers=[handler])
        
        # Change the current working directory value so that relative path-based applications work
        current_working_folder_path = os.getcwd()
        os.chdir(os.path.dirname(__file__))
        
        test_data_folder_path = os.path.join("data")
        
        # Test parameter
        (resolver_ident2resolver_factory_configuration, _, document_preprocessing_version, verbose, 
         evaluation_configuration, corpora_configuration) = get_test_base_data(test_data_folder_path) #ident
        
        # Check that pre-existing is not wiped clean when initializing with an identical experiment 
        # (useful for being crash-resistant, without having to modify the script that is responsible for creating an running the experiment)
        ## Test parameters
        begun_expe_root_folder_path = os.path.join(test_data_folder_path, "test_expe")
        begun_expe_ident = "test_expe_begun"
        new_evaluation_configuration = Mapping(evaluation_configuration)
        new_evaluation_configuration.skip_singletons = not evaluation_configuration.skip_singletons
        
        ## Test
        with TemporaryDirectory(delete_upon_exit=True) as temp_dir:
            src_begun_expe_folder_path = os.path.join(begun_expe_root_folder_path, begun_expe_ident)
            dest_begun_expe_folder_path = os.path.join(temp_dir.temporary_folder_path, "test_expe_begun")
            shutil.copytree(src_begun_expe_folder_path, dest_begun_expe_folder_path)
            resolver_comparison_experiment = ResolverComparisonExperiment(begun_expe_ident, document_preprocessing_version, 
                                                                          experiments_folder_path=temp_dir.temporary_folder_path, 
                                                                          verbose=verbose)
            ### When reset = False
            #### Check that test folder does not exist yet, then create it
            test_folder_name = "test_folder_"
            test_folder_path = os.path.join(resolver_comparison_experiment.experiment_folder_path, test_folder_name)
            self.assertFalse(os.path.exists(test_folder_path))
            os.mkdir(test_folder_path)
            self.assertTrue(os.path.exists(test_folder_path))
            #### Initialize anew
            resolver_comparison_experiment.initialize_experiment(resolver_ident2resolver_factory_configuration, 
                                                                 evaluation_configuration, 
                                                                 corpora_configuration, reset=False)
            #### Check that folder is still there
            self.assertTrue(os.path.exists(test_folder_path))
            os.rmdir(test_folder_path)
            
            ### When reset = False with a different experiment
            with self.assertRaises(ExistingResolverComparisonExperimentError):
                resolver_comparison_experiment.initialize_experiment(resolver_ident2resolver_factory_configuration, 
                                                                     new_evaluation_configuration, 
                                                                     corpora_configuration, reset=False)
            
            ### When reset = True with the same experiment
            #### Check that test folder does not exist, and then create it
            self.assertFalse(os.path.exists(test_folder_path))
            os.mkdir(test_folder_path)
            self.assertTrue(os.path.exists(test_folder_path))
            #### Initialize anew
            resolver_comparison_experiment.initialize_experiment(resolver_ident2resolver_factory_configuration, 
                                                                 evaluation_configuration, 
                                                                 corpora_configuration, reset=True)
            #### Check that test folder still exists
            self.assertTrue(os.path.exists(test_folder_path))
            os.rmdir(test_folder_path)
            
            ### When reset = True with a different experiment
            #### Check that test folder does not exist yet, then create it
            self.assertFalse(os.path.exists(test_folder_path))
            os.mkdir(test_folder_path)
            self.assertTrue(os.path.exists(test_folder_path))
            #### Initialize anew
            resolver_comparison_experiment.initialize_experiment(resolver_ident2resolver_factory_configuration, 
                                                                 new_evaluation_configuration, 
                                                                 corpora_configuration, reset=True)
            #### Check that folder does not exist anymore, but experiment folder still does
            self.assertFalse(os.path.exists(test_folder_path))
            self.assertTrue(os.path.exists(resolver_comparison_experiment.experiment_folder_path))
        
        # Set the current working directory back to its original value
        os.chdir(current_working_folder_path)
    
    #@unittest.skip
    def test_initialize_and_run(self):
        FORMAT = '%(asctime)-15s :: %(levelname)-8s :: %(message)s'
        handler = logging.StreamHandler(stream=sys.stdout)
        #logging_lvl = getattr(logging, "DEBUG")
        logging_lvl = getattr(logging, "INFO")
        logging.basicConfig(format=FORMAT, level=logging_lvl, handlers=[handler])
        
        # Change the current working directory value so that relative path-based applications work
        current_working_folder_path = os.getcwd()
        os.chdir(os.path.dirname(__file__))
        
        test_data_folder_path = os.path.join("data")
        
        # Test parameter
        (resolver_ident2resolver_factory_configuration, ident, document_preprocessing_version, verbose, 
         evaluation_configuration, corpora_configuration) = get_test_base_data(test_data_folder_path)
        soon_resolver_ident = "soon"
        extendedmst_resolver_ident = "extendedmst"
        extendedmst_configuration = resolver_ident2resolver_factory_configuration[extendedmst_resolver_ident]
        pure_extendedmst_configuration = deepcopy_mapping(extendedmst_configuration)
        
        with TemporaryDirectory(delete_upon_exit=True) as root_temp_dir:
            # Test creation and run of experiment, and prepare the test to check that the loading of a pre-existing experiment is correctly implemented
            ## Test parameters
            expected_resolver_factory_configuration_file_names = sorted(["extendedmst.txt", "mccarthylehnert.txt", "soon.txt"])
            expected_trained_resolver_file_names = sorted(["extendedmst.zip", "mccarthylehnert.zip", "soon.zip"])
            expected_evaluation_result_file_names = sorted(["extendedmst_evaluation_result.pkl", 
                                                             "mccarthylehnert_evaluation_result.pkl", 
                                                             "soon_evaluation_result.pkl"])
            expected_best_resolver_ident = "soon"
            
            ## Test
            with TemporaryDirectory(temporary_root_folder_path=root_temp_dir.temporary_folder_path, 
                                    delete_upon_exit=False) as temp_dir:
                experiments_folder_path = temp_dir.temporary_folder_path
                
                resolver_comparison_experiment = ResolverComparisonExperiment(ident, document_preprocessing_version, 
                                                                              experiments_folder_path=experiments_folder_path, 
                                                                              verbose=verbose)
                temp_experiment_folder_path = resolver_comparison_experiment.experiment_folder_path
                resolver_comparison_experiment.initialize_experiment(resolver_ident2resolver_factory_configuration, 
                                                                     evaluation_configuration, corpora_configuration)
                
                
                _ = resolver_comparison_experiment.run(do_print=True) #formatted_results
                experiment_folder_path_ = resolver_comparison_experiment.experiment_folder_path
                actual_resolver_factory_configuration_file_names = sorted(os.listdir(os.path.join(experiment_folder_path_, resolver_comparison_experiment._RESOLVER_CONFIGURATION_FOLDER_NAME)))
                actual_trained_resolver_file_names = sorted(os.listdir(os.path.join(experiment_folder_path_, resolver_comparison_experiment._TRAINED_RESOLVER_FOLDER_NAME)))
                actual_evaluation_result_file_names = sorted(os.listdir(os.path.join(experiment_folder_path_, resolver_comparison_experiment._EVALUATION_RESULTS_FOLDER_NAME)))
                self.assertEqual(expected_resolver_factory_configuration_file_names, actual_resolver_factory_configuration_file_names)
                self.assertEqual(expected_trained_resolver_file_names, actual_trained_resolver_file_names)
                self.assertEqual(expected_evaluation_result_file_names, actual_evaluation_result_file_names)
                #raise Exception()

            # Loading of pre-existing experiment
            ## Test parameters
            ### Modify the experiment folder that was just carried out
            ### Remove the mst resolver and its result, remove the resuts for the mccarthy resolver
            os.remove(os.path.join(temp_experiment_folder_path, ResolverComparisonExperiment._EVALUATION_RESULTS_FOLDER_NAME, "extendedmst_evaluation_result.pkl"))
            os.remove(os.path.join(temp_experiment_folder_path, ResolverComparisonExperiment._EVALUATION_RESULTS_FOLDER_NAME, "mccarthylehnert_evaluation_result.pkl"))
            os.remove(os.path.join(temp_experiment_folder_path, ResolverComparisonExperiment._TRAINED_RESOLVER_FOLDER_NAME, "extendedmst.zip"))
            begun_expe_ident = ident
            experiments_folder_path = os.path.dirname(temp_experiment_folder_path)
            
            ## Test
            resolver_comparison_experiment = ResolverComparisonExperiment(begun_expe_ident, document_preprocessing_version, 
                                                                          experiments_folder_path=experiments_folder_path, 
                                                                          verbose=verbose)
            resolver_comparison_experiment.initialize_experiment_from_persisted_state()
            self.assertEqual(sorted(resolver_comparison_experiment.resolver_ident2evaluation_result.keys()), 
                             [soon_resolver_ident])
            self.assertEqual(resolver_comparison_experiment.remaining_resolver_ident_resolver_factory_configuration_pairs, 
                             [(extendedmst_resolver_ident, pure_extendedmst_configuration)])
            formatted_results = resolver_comparison_experiment.run(do_print=False)
            actual_best_resolver_ident, _, _ = resolver_comparison_experiment.get_current_best_resolver() #resolver, evaluation_result
            self.assertEqual(expected_best_resolver_ident, actual_best_resolver_ident)
            print(formatted_results)
            #raise Exception()
            
        # Set the current working directory back to its original value
        os.chdir(current_working_folder_path)
    
    #@unittest.skip
    def test_efficient_processing_of_configurations_where_only_iteration_nb_of_online_interface_varies(self):
        FORMAT = '%(asctime)-15s :: %(levelname)-8s :: %(message)s'
        handler = logging.StreamHandler(stream=sys.stdout)
        #logging_lvl = getattr(logging, "DEBUG")
        logging_lvl = getattr(logging, "INFO")
        logging.basicConfig(format=FORMAT, level=logging_lvl, handlers=[handler])
        
        # Change the current working directory value so that relative path-based applications work
        current_working_folder_path = os.getcwd()
        os.chdir(os.path.dirname(__file__))
        
        # Test parameter
        test_data_folder_path = os.path.join("data")
        ident = "test_expe"
        document_preprocessing_version = "test_document_preprocessing_version"
        verbose = True
        train_corpus_file_path = os.path.join(test_data_folder_path, "corpora", "train.txt")
        test_corpus_file_path = os.path.join(test_data_folder_path, "corpora", "test.txt")
        document_root_folder_path = os.path.join(test_data_folder_path, "corpora", "documents")
        evaluation_configuration = Mapping({"skip_singletons": False, "detect_mentions": True, 
                                            "retrieve_individual_document_data": True})
        corpora_configuration = Mapping({"train": {"corpus_file_path": train_corpus_file_path, "documents_root_folder_path": document_root_folder_path}, 
                                         "test": {"corpus_file_path": test_corpus_file_path, "documents_root_folder_path": document_root_folder_path}
                                        })
        
        # resolver configuration data
        resolver_ident2resolver_factory_configuration = create_resolver_ident2factory_configuration_data()
        
        ## Test
        with TemporaryDirectory(delete_upon_exit=True) as temp_dir:
            experiments_folder_path = temp_dir.temporary_folder_path
            
            resolver_comparison_experiment = ResolverComparisonExperiment(ident, document_preprocessing_version, 
                                                                          experiments_folder_path=experiments_folder_path, 
                                                                          verbose=verbose)
            
            resolver_comparison_experiment.initialize_experiment(resolver_ident2resolver_factory_configuration, 
                                                                 evaluation_configuration, corpora_configuration)
            
            
            resolver_comparison_experiment.train_remaining_resolvers()
            #formatted_results = resolver_comparison_experiment.run(do_print=False)
            #print(formatted_results)
        
        #raise Exception
        
        # Set the current working directory back to its original value
        os.chdir(current_working_folder_path)
        


def get_test_base_data(test_data_folder_path):
    ident = "test_expe"
    document_preprocessing_version = "test_document_preprocessing_version"
    verbose = True
    train_corpus_file_path = os.path.join(test_data_folder_path, "corpora", "train.txt")
    test_corpus_file_path = os.path.join(test_data_folder_path, "corpora", "test.txt")
    document_root_folder_path = os.path.join(test_data_folder_path, "corpora", "documents")
    evaluation_configuration = Mapping({"skip_singletons": False, "detect_mentions": True, 
                                        "retrieve_individual_document_data": True})
    corpora_configuration = Mapping({"train": {"corpus_file_path": train_corpus_file_path, "documents_root_folder_path": document_root_folder_path}, 
                                     "test": {"corpus_file_path": test_corpus_file_path, "documents_root_folder_path": document_root_folder_path}
                                    })
        
    # resolver configuration data
    resolver_ident2resolver_factory_configuration = {}
    ## Soon
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                    "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                       "with_singleton_features": False, 
                                                                                                       "with_anaphoricity_features": False, 
                                                                                                       "quantize": True,})
                                                                                    }),
                                             "classifier": Mapping({"name": "logisticregressionsklearnbinary", 
                                                                    "config": Mapping({"penalty": "l2",
                                                                                       "dual": True,
                                                                                       "tol": 1e-3,
                                                                                       "C": 1.5,
                                                                                       "fit_intercept": True,
                                                                                       "intercept_scaling": 1,
                                                                                       "class_weight": "balanced",
                                                                                       "random_state": 84,
                                                                                       "solver": "liblinear",
                                                                                       "max_iter": 42,
                                                                                       "multi_class": "ovr",
                                                                                       })
                                                                    }),
                                             })
    model_interface_configuration = Mapping({"name": "SKLEARNInterface", "config": model_interface_configuration})
    generator_configuration = Mapping({"pair_filter": Mapping({"name": "PairFiltersCombination", 
                                                               "config": Mapping({"filters": [Mapping({"name": "AcceptAllPairFilter"})]})})
                                       })
    generator_configuration = Mapping({"name": "SoonGenerator", "config": generator_configuration})
    decoder_configuration = Mapping({"name": "ClosestFirstCoreferenceDecoder"})
    
    configuration = Mapping({"model_interface": model_interface_configuration,
                             "generator": generator_configuration,
                             "decoder": decoder_configuration,
                             })
    soon_resolver_factory_configuration = Mapping({"name": "soon", "config": configuration})
    soon_resolver_ident = "soon"
    resolver_ident2resolver_factory_configuration[soon_resolver_ident] = soon_resolver_factory_configuration
    ## McCarthyLehnertResolver
    with_singleton_features = False
    with_anaphoricity_features = False
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                 "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                      "with_singleton_features": with_singleton_features, 
                                                                                                      "with_anaphoricity_features": with_anaphoricity_features, 
                                                                                                      "quantize": True,})
                                                                                 }),
                                             "classifier": Mapping({"name": "logisticregressionsklearnbinary", 
                                                                   "config": Mapping({"penalty": "l2",
                                                                                    "dual": True,
                                                                                    "tol": 1e-3,
                                                                                    "C": 1.5,
                                                                                    "fit_intercept": True,
                                                                                    "intercept_scaling": 1,
                                                                                    "class_weight": "balanced",
                                                                                    "random_state": 84,
                                                                                    "solver": "liblinear",
                                                                                    "max_iter": 42,
                                                                                    "multi_class": "ovr",
                                                                                    })
                                                                    }),
                                           })
    model_interface_configuration = Mapping({"name": "SKLEARNInterface", "config": model_interface_configuration})
    generator_configuration = Mapping([("pair_filter", Mapping({"name": "acceptall"})),])
    generator_configuration = Mapping({"name": "MentionPairSampleGenerator", "config": generator_configuration})
    decoder_configuration = Mapping({"name": "AggressiveMergeCoreferenceDecoder"})
    
    configuration = Mapping({"model_interface": model_interface_configuration,
                               "generator": generator_configuration,
                               "decoder": decoder_configuration,
                               })
    mccarthylehnert_resolver_ident = "mccarthylehnert"
    mccarthylehnert_configuration = Mapping({"name": "mccarthylehnert", "config": configuration})
    resolver_ident2resolver_factory_configuration[mccarthylehnert_resolver_ident] = mccarthylehnert_configuration
    ## ExtendedMST
    model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "ExtendedMentionPairSampleFeaturesVectorizer",
                                                                                    "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                       "mention_hierarchy": Mapping({"name": "mentiongramtype", "config": Mapping()}), 
                                                                                                       "with_singleton_features": False, 
                                                                                                       "with_anaphoricity_features": False, 
                                                                                                       "quantize": True,
                                                                                                       })
                                                                                    }), 
                                             "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                    "config": Mapping({"iteration_nb": 2, 
                                                                                       "avg": True, 
                                                                                       "warm_start": False, 
                                                                                       "alpha": 1.52,
                                                                                       }),
                                                                    })
                                            })
    generator_configuration = Mapping({"pair_filter": Mapping({"name": "PairFiltersCombination", 
                                                               "config": Mapping({"filters": [Mapping({"name": "AcceptAllPairFilter"}),]}),
                                                               }),
                                       })
    decoder_configuration = Mapping()
    root_loss = 1.2
    update_mode = "ML"
    configuration = Mapping({"model_interface": Mapping({"name": "OnlineInterface", "config": model_interface_configuration}),
                              "generator": Mapping({"name": "ExtendedMentionPairSampleGenerator", "config": generator_configuration}),
                              "decoder": Mapping({"name": "MSTPrimCoreferenceDecoder", "config": decoder_configuration}),
                              "root_loss": root_loss, 
                              "update_mode": update_mode, 
                             })
    extendedmst_configuration = Mapping({"name": "extendedmst", "config": configuration})
    
    extendedmst_resolver_ident = "extendedmst"
    resolver_ident2resolver_factory_configuration[extendedmst_resolver_ident] = extendedmst_configuration
    
    return (resolver_ident2resolver_factory_configuration, ident, document_preprocessing_version, verbose, 
            evaluation_configuration, corpora_configuration)
    



def create_resolver_ident2factory_configuration_data():
    resolver_ident2factory_configuration = {}
    ### ExtendedMST
    for iteration_nb in [5, 10, 15]:
        resolver_ident = "extendedmst_iteration_nb={}".format(iteration_nb)
        
        model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "ExtendedMentionPairSampleFeaturesVectorizer",
                                                                                        "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                           "mention_hierarchy": Mapping({"name": "mentiongramtype", "config": Mapping()}), 
                                                                                                           "with_singleton_features": False, 
                                                                                                           "with_anaphoricity_features": False, 
                                                                                                           "quantize": True,
                                                                                                           })
                                                                                        }), 
                                                 "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                        "config": Mapping({"iteration_nb": iteration_nb, 
                                                                                           "avg": True, 
                                                                                           "warm_start": False, 
                                                                                           "alpha": 1.52,
                                                                                           }),
                                                                        })
                                                })
        generator_configuration = Mapping({"pair_filter": Mapping({"name": "PairFiltersCombination", 
                                                                   "config": Mapping({"filters": [Mapping({"name": "AcceptAllPairFilter"}),]}),
                                                                   }),
                                           })
        decoder_configuration = Mapping()
        root_loss = 1.2
        update_mode = "ML"
        configuration = Mapping({"model_interface": Mapping({"name": "OnlineInterface", "config": model_interface_configuration}),
                                  "generator": Mapping({"name": "ExtendedMentionPairSampleGenerator", "config": generator_configuration}),
                                  "decoder": Mapping({"name": "MSTPrimCoreferenceDecoder", "config": decoder_configuration}),
                                  "root_loss": root_loss, 
                                  "update_mode": update_mode, 
                                 })
        factory_configuration = Mapping({"name": "extendedmst", "config": configuration})
        
        resolver_ident2factory_configuration[resolver_ident] = factory_configuration
    
    ### MST
    for iteration_nb in [25, 30, 35]:
        resolver_ident = "mst_iteration_nb={}".format(iteration_nb)
        
        model_interface_configuration = Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                        "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}),
                                                                                                           "with_singleton_features": False, 
                                                                                                           "with_anaphoricity_features": False, 
                                                                                                           "quantize": True,
                                                                                                           })
                                                                                        }), 
                                                 "classifier": Mapping({"name": "PerceptronOnlineBinaryClassifier",
                                                                        "config": Mapping({"iteration_nb": iteration_nb, 
                                                                                           "avg": True, 
                                                                                           "warm_start": False, 
                                                                                           "alpha": 1.52,
                                                                                           }),
                                                                        })
                                                })
        generator_configuration = Mapping({"pair_filter": Mapping({"name": "PairFiltersCombination", 
                                                                   "config": Mapping({"filters": [Mapping({"name": "AcceptAllPairFilter"}),]}),
                                                                   }),
                                           })
        decoder_configuration = Mapping()
        root_loss = 1.2
        update_mode = "ML"
        configuration = Mapping({"model_interface": Mapping({"name": "OnlineInterface", "config": model_interface_configuration}),
                                  "generator": Mapping({"name": "MentionPairSampleGenerator", "config": generator_configuration}),
                                  "decoder": Mapping({"name": "MSTPrimCoreferenceDecoder", "config": decoder_configuration}),
                                  "root_loss": root_loss, 
                                  "update_mode": update_mode, 
                                 })
        factory_configuration = Mapping({"name": "mst", "config": configuration})
        
        resolver_ident2factory_configuration[resolver_ident] = factory_configuration
    
    ### HybridBestFirstMST
    resolver_ident = "hybridbestfirstmst"
    configuration = Mapping({"generator": Mapping({"name": "MentionPairSampleGenerator",
                                                     "config": Mapping({"pair_filter": Mapping({"name": "combination",
                                                                                                "config": Mapping({"filters": [Mapping({"name": "AcceptAllPairFilter"}),]}),
                                                                                                }),
                                                                        }),
                                                     }),
                               "decoder": Mapping({"name": "HybridBestFirstMSTCoreferenceDecoder",
                                                   "config": Mapping(),
                                                   }),
                               "model_interface": Mapping({"name": "OnlineInterface",
                                                           "config": Mapping({"sample_features_vectorizer": Mapping({"name": "ExtendedMentionPairSampleFeaturesVectorizer",
                                                                                                                    "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}), 
                                                                                                                                       "mention_hierarchy": Mapping({"name": "mentiongramtype", "config": Mapping()}), 
                                                                                                                                       "with_singleton_features": False, 
                                                                                                                                       "with_anaphoricity_features": False, 
                                                                                                                                       "quantize": True,
                                                                                                                                       })
                                                                                        }), 
                                                                              "classifier": Mapping({"name": "PAOnlineBinaryClassifier",
                                                                                                     "config": Mapping({"iteration_nb": 5,
                                                                                                                        "C": 2.6,
                                                                                                                        "avg": True,
                                                                                                                        "warm_start": False, 
                                                                                                                        }),
                                                                                                     }),
                                                                              }),
                                                           }),
                               "root_loss": root_loss,
                               "update_mode": update_mode,
                               })
    
    factory_configuration = Mapping({"name": "hybridbestfirstmst", "config": configuration})
        
    resolver_ident2factory_configuration[resolver_ident] = factory_configuration
    
    ### Soon
    resolver_ident = "soon"
    configuration = Mapping({"generator": Mapping({"name": "SoonGenerator",
                                                     "config": Mapping({"pair_filter": Mapping({"name": "combination",
                                                                                                "config": Mapping({"filters": [Mapping({"name": "AcceptAllPairFilter"}),]}),
                                                                                                }),
                                                                        }),
                                                     }),
                               "decoder": Mapping({"name": "ClosestFirstCoreferenceDecoder",
                                                   "config": Mapping(),
                                                   }),
                               "model_interface": Mapping({"name": "SKLEARNInterface",
                                                           "config": Mapping({"sample_features_vectorizer": Mapping({"name": "MentionPairSampleFeaturesVectorizer",
                                                                                                                    "config": Mapping({"pair_hierarchy": Mapping({"name": "PairGramTypeHierarchy", "config": Mapping()}),
                                                                                                                                       "with_singleton_features": False, 
                                                                                                                                       "with_anaphoricity_features": False, 
                                                                                                                                       "quantize": True,
                                                                                                                                       })
                                                                                                                    }), 
                                                                              "classifier": Mapping({"name": "PassiveAggressiveSKLEARNBinaryClassifier",
                                                                                                     "config": Mapping({"C": 1.5,
                                                                                                                        "fit_intercept": True,
                                                                                                                        "max_iter": 100,
                                                                                                                        "tol": 1e-3,
                                                                                                                        "loss": "hinge",
                                                                                                                        "average": True,
                                                                                                                        "random_state": 42,
                                                                                                                        "class_weight": "balanced",
                                                                                                                        "shuffle": True,
                                                                                                                        }),
                                                                                                     }),
                                                                              }),
                                                           }),
                               })
    
    factory_configuration = Mapping({"name": "soon", "config": configuration})
        
    resolver_ident2factory_configuration[resolver_ident] = factory_configuration
    
    return resolver_ident2factory_configuration
    


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()