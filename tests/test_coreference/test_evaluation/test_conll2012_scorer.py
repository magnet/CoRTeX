# -*- coding: utf-8 -*-

import unittest
import numpy
from collections import defaultdict

from cortex.api.document import Document
from cortex.api.document_buffer import DocumentBuffer, deepcopy_document
from cortex.api.coreference_partition import CoreferencePartition
from cortex.coreference.evaluation.conll2012_scorer import (CONLL2012Scorer, format_evaluation_result, 
                                                            format_resolvers_evaluation_results,
                                                            )


#@unittest.skip
class TestCONLL2012Scorer(unittest.TestCase):
    
    #@unittest.skip
    def test_evaluate(self):
        # Test parameters
        data = get_test_evaluate_data()
        conll2012_scorer = CONLL2012Scorer()
        
        # Test
        for (args, kwargs), (expected_metric_overall_results, expected_document_ident_and_results_per_metric_name_pairs) in data:
            ## Prepare a copy of the document so as to check that the process did not change their content (notably their respective coreference partition value)
            document_ref_partition_predicted_partition_triplets, _ = args #metric
            input_documents = tuple(document for document, _, _ in document_ref_partition_predicted_partition_triplets) #sys_coreference_partition
            copied_documents = tuple(deepcopy_document(document, copy_document_ident=None, strict=False) for document in input_documents)
            
            ## Pre-process check
            self.assertEqual(copied_documents, input_documents)
            ## Test
            actual_metric_overall_results, actual_document_ident_and_results_per_metric_name_pairs = conll2012_scorer.evaluate(*args, **kwargs)
            self.assertEqual(expected_metric_overall_results, actual_metric_overall_results)
            self.assertEqual(expected_document_ident_and_results_per_metric_name_pairs, actual_document_ident_and_results_per_metric_name_pairs)
            ## Post-process check
            self.assertEqual(copied_documents, input_documents)
    
    #@unittest.skip
    def test_evaluate_(self):
        # Test parameters
        test_document = get_memoized_test_document_data()
        reference_documents = [test_document,
                               deepcopy_document(test_document),
                               ]
        conll2012_scorer = CONLL2012Scorer()
        
        # Test
        document_ref_partition_predicted_partition_triplets = []
        for ref_document in reference_documents:
            ref_partition = ref_document.coreference_partition
            predicted_partition = ref_partition
            document_ref_partition_predicted_partition_triplets.append((ref_document, ref_partition, predicted_partition))
        metric_overall_results, document_ident_and_results_per_metric_name_pairs =\
         conll2012_scorer.evaluate(document_ref_partition_predicted_partition_triplets, 
                                   metric="all", skip_singletons=True, retrieve_individual_document_data=True)
        avg_report, document_reports = format_evaluation_result(metric_overall_results, 
                                                                 document_ident_and_results_per_metric_name_pairs,
                                                                 detailed=True)
        print("Beginning average report:")
        print(avg_report)
        print("End average report.")
        print()
        print("Beginning individual document reports:")
        print("\n".join(document_reports))
        print()
        print()
        #raise Exception()
    
    #@unittest.skip
    def test_format_overall_results(self):
        # Test parameters
        data = get_test_evaluate_data()
        
        # Test
        for (args, kwargs), (expected_metric_overall_results, expected_document_ident_and_results_per_metric_name_pairs) in data:
            _, metric_name = args
            if metric_name == "all":
                avg_report, document_reports = format_evaluation_result(expected_metric_overall_results, 
                                                                         expected_document_ident_and_results_per_metric_name_pairs,
                                                                         detailed=True)
                print((args, kwargs))
                print("Beginning average report:")
                print(avg_report)
                print("End average report.")
                print()
                print("Beginning individual document reports:")
                if kwargs["retrieve_individual_document_data"]:
                    print("\n".join(document_reports))
                    print()
                    print()
                print("End individual document reports.")
                print()
                print()
        #raise Exception()
    
    
    #@unittest.skip
    def test_format_resolvers_evaluation_results(self):
        # Test parameters
        ### all
        #### muc
        muc_overall_results = {"overall_mention_identification_recall_num": 6.,
                                        "overall_mention_identification_recall_denom": 12.,
                                        "overall_mention_identification_recall": 50.,
                                        "overall_mention_identification_precision_num": 6.,
                                        "overall_mention_identification_precision_denom": 7.,
                                        "overall_mention_identification_precision": 85.71,
                                        "overall_mention_identification_f1": 63.15,
                                        "recall_num": 3.,
                                        "recall_denom": 8.,
                                        "recall": 37.5,
                                        "precision_num": 3.,
                                        "precision_denom": 4.,
                                        "precision": 75.,
                                        "f1": 50.,
                                        }
        
        #### bcub
        bcub_overall_results = {"overall_mention_identification_recall_num": 6.,
                                         "overall_mention_identification_recall_denom": 12.,
                                         "overall_mention_identification_recall": 50.,
                                         "overall_mention_identification_precision_num": 6.,
                                         "overall_mention_identification_precision_denom": 7.,
                                         "overall_mention_identification_precision": 85.71,
                                         "overall_mention_identification_f1": 63.15,
                                         "recall_num": 3.5,
                                         "recall_denom": 12.,
                                         "recall": 29.16,
                                         "precision_num": 5.5,
                                         "precision_denom": 7.,
                                         "precision": 78.57,
                                         "f1": 42.54,
                                         }
        
        #### ceafm
        ceafm_overall_results = {"overall_mention_identification_recall_num": 6.,
                                          "overall_mention_identification_recall_denom": 12.,
                                          "overall_mention_identification_recall": 50.,
                                          "overall_mention_identification_precision_num": 6.,
                                          "overall_mention_identification_precision_denom": 7.,
                                          "overall_mention_identification_precision": 85.71,
                                          "overall_mention_identification_f1": 63.15,
                                          "recall_num": 5.,
                                          "recall_denom": 12.,
                                          "recall": 41.66,
                                          "precision_num": 5.,
                                          "precision_denom": 7.,
                                          "precision": 71.42,
                                          "f1": 52.63,
                                          }
        
        ### ceafe
        ceafe_overall_results = {"overall_mention_identification_recall_num": 6.,
                                          "overall_mention_identification_recall_denom": 12.,
                                          "overall_mention_identification_recall": 50.,
                                          "overall_mention_identification_precision_num": 6.,
                                          "overall_mention_identification_precision_denom": 7.,
                                          "overall_mention_identification_precision": 85.71,
                                          "overall_mention_identification_f1": 63.15,
                                          "recall_num": 1.52380952380952,
                                          "recall_denom": 4.,
                                          "recall": 38.09,
                                          "precision_num": 1.52380952380952,
                                          "precision_denom": 3.,
                                          "precision": 50.79,
                                          "f1": 43.53,
                                          }
        
        #### blanc
        blanc_overall_results = {"overall_mention_identification_recall_num": 6., 
                                          "overall_mention_identification_recall_denom": 12.,
                                          "overall_mention_identification_recall": 50.,  
                                          "overall_mention_identification_precision_num": 6., 
                                          "overall_mention_identification_precision_denom": 7., 
                                          "overall_mention_identification_precision": 85.71, 
                                          "overall_mention_identification_f1": 63.15,
                                       
                                          "coreference_recall_num": 4., 
                                          "coreference_recall_denom": 14.,
                                          "coreference_recall": 28.57,  
                                          "coreference_precision_num": 4., 
                                          "coreference_precision_denom": 5., 
                                          "coreference_precision": 80., 
                                          "coreference_f1": 42.1,
                                          
                                          "non_coreference_recall_num": 0., 
                                          "non_coreference_recall_denom": 16.,
                                          "non_coreference_recall": 0.,  
                                          "non_coreference_precision_num": 0., 
                                          "non_coreference_precision_denom": 6., 
                                          "non_coreference_precision": 0., 
                                          "non_coreference_f1": 0.,
                                       
                                          "blanc_recall_num": 0.142857142857143, 
                                          "blanc_recall_denom": 1.,
                                          "blanc_recall": 14.28,  
                                          "blanc_precision_num": 0.4, 
                                          "blanc_precision_denom": 1., 
                                          "blanc_precision": 40., 
                                          "blanc_f1": 21.05,
                                          }
        
        ### conll
        conll_overall_results = {"f1": (42.54 + 50 + 52.63) / 3,
                                          }
        
        metric_overall_results = {"muc": muc_overall_results, 
                                   "bcub": bcub_overall_results, 
                                   "ceafm": ceafm_overall_results, 
                                   "ceafe": ceafe_overall_results, 
                                   "blanc": blanc_overall_results,
                                   "conll": conll_overall_results,
                                   }
        metric_overall_results2 = dict((key, dict(value)) for key, value in metric_overall_results.items())
        for key, value in metric_overall_results2.items():
            key = "f1" if key != "blanc" else "coreference_f1"
            value[key] = value[key] / 2
    
        resolver_name2evaluation_result = {"resolver_0": (metric_overall_results2, None), 
                                           "resolver_1": (metric_overall_results, None),
                                           }
        
        data = []
        # sort_conll = False
        sort_conll = False
        args = (resolver_name2evaluation_result,)
        kwargs = {"sort_conll": sort_conll}
        data.append((args, kwargs))
        # sort_conll = True
        sort_conll = True
        args = (resolver_name2evaluation_result,)
        kwargs = {"sort_conll": sort_conll}
        data.append((args, kwargs))
        
        # Test
        for args, kwargs in data:
            report = format_resolvers_evaluation_results(*args, **kwargs)
            print(report)
        #raise Exception()




import os
from cortex.utils.memoize import Memoized
def get_test_document_data():
    from cortex.io.pivot_reader import PivotReader
    
    folder_path = os.path.join(os.path.dirname(__file__), "data")
    document_folder_path = os.path.join(folder_path, "ref")
    
    strict = True # We want to assess the synchronization of head tokens.
    test_document = PivotReader.parse(document_folder_path, strict=strict)
    
    return test_document

@Memoized
def get_memoized_test_document_data():
    return get_test_document_data()
    
def get_test_evaluate_data():
    data = []
    
    # With singleton removed
    ## No document
    ### ceafe
    skip_singletons = True
    retrieve_individual_document_data = False
    metric = "ceafe"
    document_ref_partition_predicted_partition_triplets = create_test_documents()
    args = (document_ref_partition_predicted_partition_triplets, metric)
    kwargs = {"skip_singletons": skip_singletons, 
              "retrieve_individual_document_data": retrieve_individual_document_data}
    
    expected_ceafe_overall_results = {"overall_mention_identification_recall_num": 6.,
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,
                                      "overall_mention_identification_precision_num": 6.,
                                      "overall_mention_identification_precision_denom": 7.,
                                      "overall_mention_identification_precision": 85.71,
                                      "overall_mention_identification_f1": 63.15,
                                      "recall_num": 1.52380952380952,
                                      "recall_denom": 4.,
                                      "recall": 38.09,
                                      "precision_num": 1.52380952380952,
                                      "precision_denom": 3.,
                                      "precision": 50.79,
                                      "f1": 43.53,
                                      }
    expected_metric_overall_results = {"ceafe": expected_ceafe_overall_results}
    expected_document_ident_and_results_per_metric_name_pairs = []
    
    data.append(((args, kwargs), (expected_metric_overall_results, expected_document_ident_and_results_per_metric_name_pairs)))
    
    ### blanc
    skip_singletons = True
    retrieve_individual_document_data = False
    metric = "blanc"
    document_ref_partition_predicted_partition_triplets = create_test_documents()
    args = (document_ref_partition_predicted_partition_triplets, metric)
    kwargs = {"skip_singletons": skip_singletons, 
              "retrieve_individual_document_data": retrieve_individual_document_data}
    
    expected_blanc_overall_results = {"overall_mention_identification_recall_num": 6., 
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,  
                                      "overall_mention_identification_precision_num": 6., 
                                      "overall_mention_identification_precision_denom": 7., 
                                      "overall_mention_identification_precision": 85.71, 
                                      "overall_mention_identification_f1": 63.15,
                                      
                                      "coreference_recall_num": 4., 
                                      "coreference_recall_denom": 14.,
                                      "coreference_recall": 28.57,  
                                      "coreference_precision_num": 4., 
                                      "coreference_precision_denom": 5., 
                                      "coreference_precision": 80., 
                                      "coreference_f1": 42.1,
                                      
                                      "non_coreference_recall_num": 0., 
                                      "non_coreference_recall_denom": 16.,
                                      "non_coreference_recall": 0.,  
                                      "non_coreference_precision_num": 0., 
                                      "non_coreference_precision_denom": 6., 
                                      "non_coreference_precision": 0., 
                                      "non_coreference_f1": 0.,
                                      
                                      "blanc_recall_num": 0.142857142857143, 
                                      "blanc_recall_denom": 1.,
                                      "blanc_recall": 14.28,  
                                      "blanc_precision_num": 0.4, 
                                      "blanc_precision_denom": 1., 
                                      "blanc_precision": 40., 
                                      "blanc_f1": 21.05,
                                      }
    expected_metric_overall_results = {"blanc": expected_blanc_overall_results}
    expected_document_ident_and_results_per_metric_name_pairs = []
    
    data.append(((args, kwargs), (expected_metric_overall_results, expected_document_ident_and_results_per_metric_name_pairs)))
    
    ### all
    skip_singletons = True
    retrieve_individual_document_data = False
    metric = "all"
    document_ref_partition_predicted_partition_triplets = create_test_documents()
    args = (document_ref_partition_predicted_partition_triplets, metric)
    kwargs = {"skip_singletons": skip_singletons, 
              "retrieve_individual_document_data": retrieve_individual_document_data}
    
    
    #### muc
    expected_muc_overall_results = {"overall_mention_identification_recall_num": 6.,
                                    "overall_mention_identification_recall_denom": 12.,
                                    "overall_mention_identification_recall": 50.,
                                    "overall_mention_identification_precision_num": 6.,
                                    "overall_mention_identification_precision_denom": 7.,
                                    "overall_mention_identification_precision": 85.71,
                                    "overall_mention_identification_f1": 63.15,
                                    "recall_num": 3.,
                                    "recall_denom": 8.,
                                    "recall": 37.5,
                                    "precision_num": 3.,
                                    "precision_denom": 4.,
                                    "precision": 75.,
                                    "f1": 50.,
                                    }
    
    #### bcub
    expected_bcub_overall_results = {"overall_mention_identification_recall_num": 6.,
                                     "overall_mention_identification_recall_denom": 12.,
                                     "overall_mention_identification_recall": 50.,
                                     "overall_mention_identification_precision_num": 6.,
                                     "overall_mention_identification_precision_denom": 7.,
                                     "overall_mention_identification_precision": 85.71,
                                     "overall_mention_identification_f1": 63.15,
                                     "recall_num": 3.5,
                                     "recall_denom": 12.,
                                     "recall": 29.16,
                                     "precision_num": 5.5,
                                     "precision_denom": 7.,
                                     "precision": 78.57,
                                     "f1": 42.54,
                                     }
    
    #### ceafm
    expected_ceafm_overall_results = {"overall_mention_identification_recall_num": 6.,
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,
                                      "overall_mention_identification_precision_num": 6.,
                                      "overall_mention_identification_precision_denom": 7.,
                                      "overall_mention_identification_precision": 85.71,
                                      "overall_mention_identification_f1": 63.15,
                                      "recall_num": 5.,
                                      "recall_denom": 12.,
                                      "recall": 41.66,
                                      "precision_num": 5.,
                                      "precision_denom": 7.,
                                      "precision": 71.42,
                                      "f1": 52.63,
                                      }
    
    ### ceafe
    expected_ceafe_overall_results = {"overall_mention_identification_recall_num": 6.,
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,
                                      "overall_mention_identification_precision_num": 6.,
                                      "overall_mention_identification_precision_denom": 7.,
                                      "overall_mention_identification_precision": 85.71,
                                      "overall_mention_identification_f1": 63.15,
                                      "recall_num": 1.52380952380952,
                                      "recall_denom": 4.,
                                      "recall": 38.09,
                                      "precision_num": 1.52380952380952,
                                      "precision_denom": 3.,
                                      "precision": 50.79,
                                      "f1": 43.53,
                                      }
    
    #### blanc
    expected_blanc_overall_results = {"overall_mention_identification_recall_num": 6., 
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,  
                                      "overall_mention_identification_precision_num": 6., 
                                      "overall_mention_identification_precision_denom": 7., 
                                      "overall_mention_identification_precision": 85.71, 
                                      "overall_mention_identification_f1": 63.15,
                                   
                                      "coreference_recall_num": 4., 
                                      "coreference_recall_denom": 14.,
                                      "coreference_recall": 28.57,  
                                      "coreference_precision_num": 4., 
                                      "coreference_precision_denom": 5., 
                                      "coreference_precision": 80., 
                                      "coreference_f1": 42.1,
                                      
                                      "non_coreference_recall_num": 0., 
                                      "non_coreference_recall_denom": 16.,
                                      "non_coreference_recall": 0.,  
                                      "non_coreference_precision_num": 0., 
                                      "non_coreference_precision_denom": 6., 
                                      "non_coreference_precision": 0., 
                                      "non_coreference_f1": 0.,
                                   
                                      "blanc_recall_num": 0.142857142857143, 
                                      "blanc_recall_denom": 1.,
                                      "blanc_recall": 14.28,  
                                      "blanc_precision_num": 0.4, 
                                      "blanc_precision_denom": 1., 
                                      "blanc_precision": 40., 
                                      "blanc_f1": 21.05,
                                      }
    
    ### conll
    expected_conll_overall_results = {"f1": (42.54 + 50 + 52.63) / 3,
                                      }
    
    expected_metric_overall_results = {"muc": expected_muc_overall_results, 
                                       "bcub": expected_bcub_overall_results, 
                                       "ceafm": expected_ceafm_overall_results, 
                                       "ceafe": expected_ceafe_overall_results, 
                                       "blanc": expected_blanc_overall_results,
                                       "conll": expected_conll_overall_results,
                                       }
    expected_document_ident_and_results_per_metric_name_pairs = []
    
    data.append(((args, kwargs), (expected_metric_overall_results, expected_document_ident_and_results_per_metric_name_pairs)))
    
    ## All documents
    ### ceafe
    skip_singletons = True
    retrieve_individual_document_data = True
    metric = "ceafe"
    document_ref_partition_predicted_partition_triplets = create_test_documents()
    args = (document_ref_partition_predicted_partition_triplets, metric)
    kwargs = {"skip_singletons": skip_singletons, 
              "retrieve_individual_document_data": retrieve_individual_document_data}
    
    expected_ceafe_overall_results = {"overall_mention_identification_recall_num": 6.,
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,
                                      "overall_mention_identification_precision_num": 6.,
                                      "overall_mention_identification_precision_denom": 7.,
                                      "overall_mention_identification_precision": 85.71,
                                      "overall_mention_identification_f1": 63.15,
                                      "recall_num": 1.52380952380952,
                                      "recall_denom": 4.,
                                      "recall": 38.09,
                                      "precision_num": 1.52380952380952,
                                      "precision_denom": 3.,
                                      "precision": 50.79,
                                      "f1": 43.53,
                                      }
    expected_metric_overall_results = {"ceafe": expected_ceafe_overall_results}
    expected_document_ident_and_results_per_metric_name_pairs = [("doc1", {"ceafe": {"total_key_mentions_nb": 6,
                                                                     "total_response_mentions_nb": 5, 
                                                                     "strictly_correct_identified_mentions_nb": 4,  
                                                                     "coreference_recall_num": 0.857142857142857, 
                                                                     "coreference_recall_denom": 2.,
                                                                     "coreference_recall": 42.85,  
                                                                     "coreference_precision_num": 0.857142857142857, 
                                                                     "coreference_precision_denom": 2., 
                                                                     "coreference_precision": 42.85, 
                                                                     "coreference_f1": 42.85,
                                                                     } 
                                                           }),
                                                 ("doc2", {"ceafe": {"total_key_mentions_nb": 6,
                                                                     "total_response_mentions_nb": 2, 
                                                                     "strictly_correct_identified_mentions_nb": 2,  
                                                                     "coreference_recall_num": 0.666666666666667, 
                                                                     "coreference_recall_denom": 2.,
                                                                     "coreference_recall": 33.33,  
                                                                     "coreference_precision_num": 0.666666666666667, 
                                                                     "coreference_precision_denom": 1., 
                                                                     "coreference_precision": 66.66, 
                                                                     "coreference_f1": 44.44,
                                                                    } 
                                                             }),
                                                 ]
    
    data.append(((args, kwargs), (expected_metric_overall_results, expected_document_ident_and_results_per_metric_name_pairs)))
    
    ### blanc
    skip_singletons = True
    retrieve_individual_document_data = True
    metric = "blanc"
    document_ref_partition_predicted_partition_triplets = create_test_documents()
    args = (document_ref_partition_predicted_partition_triplets, metric)
    kwargs = {"skip_singletons": skip_singletons, 
              "retrieve_individual_document_data": retrieve_individual_document_data}
    
    expected_blanc_overall_results = {"overall_mention_identification_recall_num": 6., 
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,  
                                      "overall_mention_identification_precision_num": 6., 
                                      "overall_mention_identification_precision_denom": 7., 
                                      "overall_mention_identification_precision": 85.71, 
                                      "overall_mention_identification_f1": 63.15,
                                      
                                      "coreference_recall_num": 4., 
                                      "coreference_recall_denom": 14.,
                                      "coreference_recall": 28.57,  
                                      "coreference_precision_num": 4., 
                                      "coreference_precision_denom": 5., 
                                      "coreference_precision": 80., 
                                      "coreference_f1": 42.1,
                                      
                                      "non_coreference_recall_num": 0., 
                                      "non_coreference_recall_denom": 16.,
                                      "non_coreference_recall": 0.,  
                                      "non_coreference_precision_num": 0., 
                                      "non_coreference_precision_denom": 6., 
                                      "non_coreference_precision": 0., 
                                      "non_coreference_f1": 0.,
                                      
                                      "blanc_recall_num": 0.142857142857143, 
                                      "blanc_recall_denom": 1.,
                                      "blanc_recall": 14.28,  
                                      "blanc_precision_num": 0.4, 
                                      "blanc_precision_denom": 1., 
                                      "blanc_precision": 40., 
                                      "blanc_f1": 21.05,
                                      }
    expected_metric_overall_results = {"blanc": expected_blanc_overall_results}
    expected_document_ident_and_results_per_metric_name_pairs = [("doc1", {"blanc": {"total_key_mentions_nb": 6, 
                                                                      "total_response_mentions_nb": 5, 
                                                                      "strictly_correct_identified_mentions_nb": 4
                                                                    }
                                                           }),
                                                 ("doc2", {"blanc": {"total_key_mentions_nb": 6, 
                                                                      "total_response_mentions_nb": 2, 
                                                                      "strictly_correct_identified_mentions_nb": 2
                                                                    }
                                                             }),
                                                 ]
    
    data.append(((args, kwargs), (expected_metric_overall_results, expected_document_ident_and_results_per_metric_name_pairs)))
    
    ### all
    skip_singletons = True
    retrieve_individual_document_data = True
    metric = "all"
    document_ref_partition_predicted_partition_triplets = create_test_documents()
    args = (document_ref_partition_predicted_partition_triplets, metric)
    kwargs = {"skip_singletons": skip_singletons, 
              "retrieve_individual_document_data": retrieve_individual_document_data}
    
    
    #### muc
    expected_muc_overall_results = {"overall_mention_identification_recall_num": 6.,
                                    "overall_mention_identification_recall_denom": 12.,
                                    "overall_mention_identification_recall": 50.,
                                    "overall_mention_identification_precision_num": 6.,
                                    "overall_mention_identification_precision_denom": 7.,
                                    "overall_mention_identification_precision": 85.71,
                                    "overall_mention_identification_f1": 63.15,
                                    "recall_num": 3.,
                                    "recall_denom": 8.,
                                    "recall": 37.5,
                                    "precision_num": 3.,
                                    "precision_denom": 4.,
                                    "precision": 75.,
                                    "f1": 50.,
                                    }
    
    #### bcub
    expected_bcub_overall_results = {"overall_mention_identification_recall_num": 6.,
                                     "overall_mention_identification_recall_denom": 12.,
                                     "overall_mention_identification_recall": 50.,
                                     "overall_mention_identification_precision_num": 6.,
                                     "overall_mention_identification_precision_denom": 7.,
                                     "overall_mention_identification_precision": 85.71,
                                     "overall_mention_identification_f1": 63.15,
                                     "recall_num": 3.5,
                                     "recall_denom": 12.,
                                     "recall": 29.16,
                                     "precision_num": 5.5,
                                     "precision_denom": 7.,
                                     "precision": 78.57,
                                     "f1": 42.54,
                                     }
    
    #### ceafm
    expected_ceafm_overall_results = {"overall_mention_identification_recall_num": 6.,
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,
                                      "overall_mention_identification_precision_num": 6.,
                                      "overall_mention_identification_precision_denom": 7.,
                                      "overall_mention_identification_precision": 85.71,
                                      "overall_mention_identification_f1": 63.15,
                                      "recall_num": 5.,
                                      "recall_denom": 12.,
                                      "recall": 41.66,
                                      "precision_num": 5.,
                                      "precision_denom": 7.,
                                      "precision": 71.42,
                                      "f1": 52.63,
                                      }
    
    ### ceafe
    expected_ceafe_overall_results = {"overall_mention_identification_recall_num": 6.,
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,
                                      "overall_mention_identification_precision_num": 6.,
                                      "overall_mention_identification_precision_denom": 7.,
                                      "overall_mention_identification_precision": 85.71,
                                      "overall_mention_identification_f1": 63.15,
                                      "recall_num": 1.52380952380952,
                                      "recall_denom": 4.,
                                      "recall": 38.09,
                                      "precision_num": 1.52380952380952,
                                      "precision_denom": 3.,
                                      "precision": 50.79,
                                      "f1": 43.53,
                                      }
    
    #### blanc
    expected_blanc_overall_results = {"overall_mention_identification_recall_num": 6., 
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,  
                                      "overall_mention_identification_precision_num": 6., 
                                      "overall_mention_identification_precision_denom": 7., 
                                      "overall_mention_identification_precision": 85.71, 
                                      "overall_mention_identification_f1": 63.15,
                                   
                                      "coreference_recall_num": 4., 
                                      "coreference_recall_denom": 14.,
                                      "coreference_recall": 28.57,  
                                      "coreference_precision_num": 4., 
                                      "coreference_precision_denom": 5., 
                                      "coreference_precision": 80., 
                                      "coreference_f1": 42.1,
                                      
                                      "non_coreference_recall_num": 0., 
                                      "non_coreference_recall_denom": 16.,
                                      "non_coreference_recall": 0.,  
                                      "non_coreference_precision_num": 0., 
                                      "non_coreference_precision_denom": 6., 
                                      "non_coreference_precision": 0., 
                                      "non_coreference_f1": 0.,
                                   
                                      "blanc_recall_num": 0.142857142857143, 
                                      "blanc_recall_denom": 1.,
                                      "blanc_recall": 14.28,  
                                      "blanc_precision_num": 0.4, 
                                      "blanc_precision_denom": 1., 
                                      "blanc_precision": 40., 
                                      "blanc_f1": 21.05,
                                      }
    
    #### conll
    expected_conll_overall_results = {"f1": (42.54 + 50 + 52.63) / 3,}
    
    expected_metric_overall_results = {"muc": expected_muc_overall_results, 
                                       "bcub": expected_bcub_overall_results, 
                                       "ceafm": expected_ceafm_overall_results, 
                                       "ceafe": expected_ceafe_overall_results, 
                                       "blanc": expected_blanc_overall_results,
                                       "conll": expected_conll_overall_results,
                                       }
    expected_document_ident_and_results_per_metric_name_pairs = [("doc1", {"muc": {"total_key_mentions_nb": 6,
                                                                                 "total_response_mentions_nb": 5, 
                                                                                 "strictly_correct_identified_mentions_nb": 4,  
                                                                                 "coreference_recall_num": 2., 
                                                                                 "coreference_recall_denom": 4.,
                                                                                 "coreference_recall": 50.,  
                                                                                 "coreference_precision_num": 2., 
                                                                                 "coreference_precision_denom": 3., 
                                                                                 "coreference_precision": 66.66, 
                                                                                 "coreference_f1": 57.14,
                                                                                  }, 
                                                          "bcub": {"total_key_mentions_nb": 6,
                                                                     "total_response_mentions_nb": 5, 
                                                                     "strictly_correct_identified_mentions_nb": 4,  
                                                                     "coreference_recall_num": 2.5, 
                                                                     "coreference_recall_denom": 6.,
                                                                     "coreference_recall": 41.66,  
                                                                     "coreference_precision_num": 3.5, 
                                                                     "coreference_precision_denom": 5., 
                                                                     "coreference_precision": 70., 
                                                                     "coreference_f1": 52.23,
                                                                      }, 
                                                          "ceafm": {"total_key_mentions_nb": 6,
                                                                     "total_response_mentions_nb": 5, 
                                                                     "strictly_correct_identified_mentions_nb": 4,  
                                                                     "coreference_recall_num": 3., 
                                                                     "coreference_recall_denom": 6.,
                                                                     "coreference_recall": 50.,  
                                                                     "coreference_precision_num": 3., 
                                                                     "coreference_precision_denom": 5., 
                                                                     "coreference_precision": 60., 
                                                                     "coreference_f1": 54.54,
                                                                      }, 
                                                          "ceafe": {"total_key_mentions_nb": 6,
                                                                     "total_response_mentions_nb": 5, 
                                                                     "strictly_correct_identified_mentions_nb": 4,  
                                                                     "coreference_recall_num": 0.857142857142857, 
                                                                     "coreference_recall_denom": 2.,
                                                                     "coreference_recall": 42.85,  
                                                                     "coreference_precision_num": 0.857142857142857, 
                                                                     "coreference_precision_denom": 2., 
                                                                     "coreference_precision": 42.85, 
                                                                     "coreference_f1": 42.85,
                                                                     },
                                                          "blanc": {"total_key_mentions_nb": 6, 
                                                                      "total_response_mentions_nb": 5, 
                                                                      "strictly_correct_identified_mentions_nb": 4
                                                                    },
                                                            "conll": defaultdict((lambda: numpy.NaN), {"coreference_f1": (57.14 + 52.23 + 54.54)/3}),
                                                           }),
                                                 ("doc2", {"muc": {"total_key_mentions_nb": 6,
                                                                 "total_response_mentions_nb": 2, 
                                                                 "strictly_correct_identified_mentions_nb": 2,  
                                                                 "coreference_recall_num": 1., 
                                                                 "coreference_recall_denom": 4.,
                                                                 "coreference_recall": 25.,  
                                                                 "coreference_precision_num": 1., 
                                                                 "coreference_precision_denom": 1., 
                                                                 "coreference_precision": 100., 
                                                                 "coreference_f1": 40.,
                                                                  }, 
                                                          "bcub": {"total_key_mentions_nb": 6,
                                                                     "total_response_mentions_nb": 2, 
                                                                     "strictly_correct_identified_mentions_nb": 2,  
                                                                     "coreference_recall_num": 1., 
                                                                     "coreference_recall_denom": 6.,
                                                                     "coreference_recall": 16.66,  
                                                                     "coreference_precision_num": 2., 
                                                                     "coreference_precision_denom": 2., 
                                                                     "coreference_precision": 100., 
                                                                     "coreference_f1": 28.57,
                                                                      }, 
                                                          "ceafm": {"total_key_mentions_nb": 6,
                                                                     "total_response_mentions_nb": 2, 
                                                                     "strictly_correct_identified_mentions_nb": 2,  
                                                                     "coreference_recall_num": 2., 
                                                                     "coreference_recall_denom": 6.,
                                                                     "coreference_recall": 33.33,  
                                                                     "coreference_precision_num": 2., 
                                                                     "coreference_precision_denom": 2., 
                                                                     "coreference_precision": 100., 
                                                                     "coreference_f1": 50.,
                                                                      }, 
                                                          "ceafe": {"total_key_mentions_nb": 6,
                                                                     "total_response_mentions_nb": 2, 
                                                                     "strictly_correct_identified_mentions_nb": 2,  
                                                                     "coreference_recall_num": 0.666666666666667, 
                                                                     "coreference_recall_denom": 2.,
                                                                     "coreference_recall": 33.33,  
                                                                     "coreference_precision_num": 0.666666666666667, 
                                                                     "coreference_precision_denom": 1., 
                                                                     "coreference_precision": 66.66, 
                                                                     "coreference_f1": 44.44,
                                                                    },
                                                          "blanc": {"total_key_mentions_nb": 6, 
                                                                      "total_response_mentions_nb": 2, 
                                                                      "strictly_correct_identified_mentions_nb": 2
                                                                    },
                                                           "conll": defaultdict((lambda: numpy.NaN), {"coreference_f1": (40. + 28.57 + 50.)/3}),
                                                             }),
                                                 ]
    
    data.append(((args, kwargs), (expected_metric_overall_results, expected_document_ident_and_results_per_metric_name_pairs)))
    
    return data


def create_document(mention_extents, coreference_partition):
    document_ident = "test_document"
    raw_text = "\nThis text is a sample for a unit test task . It must be adapted to the task , and it must be easily readable for a human as well ." 
    test_document = Document(document_ident, raw_text)
    
    document_buffer = DocumentBuffer(test_document)
    token_extents = ((1, 4), (6, 9), (11, 12), (14, 14), (16, 21), (23, 25), (27, 27), 
                     (29, 32), (34, 37), (39, 42), (44, 44), 
                     (46, 47), (49, 52), (54, 55), (57, 63), (65, 66), (68, 70), (72, 75), 
                     (77, 77), (79, 81), (83, 84), (86, 89), (91, 92), (94, 99), (101, 108), 
                     (110, 112), (114, 114), (116, 120), (122, 123), (125, 128), (130, 130)
                    )
    document_buffer.add_tokens({extent: {"ident": "{},{}".format(*extent)}\
                                           for extent in token_extents
                                 }
                                )
    sentence_extents = ((1,44), (46,130))
    document_buffer.add_sentences({extent: {"ident": str(i)}\
                                           for i, extent in enumerate(sentence_extents)
                                     }
                                    )
    document_buffer.add_mentions({extent: {"ident": "{},{}".format(*extent)}\
                                           for extent in mention_extents
                                 }
                                )
    document_buffer.set_coreference_partition(coreference_partition)
    
    test_document = document_buffer.flush()
    
    return test_document

def _fill_coreference_partition(corefence_partition, coreference_extent_entities):
    for extent_entity in coreference_extent_entities:
        head_mention_extent = extent_entity[0]
        extents = extent_entity[1:]
        for extent in extents:
            corefence_partition.join(head_mention_extent, extent)


def create_test_documents():
    # Create reference / ground truth document
    mention_extents = ((1,9), (14,42), (27,42), (46,47), (68,75), (83,84), (114,120))
    reference_partition = CoreferencePartition(mention_extents=mention_extents)
    coreference_extent_entities = (((1,9), (14,42), (46,47), (83,84)),
                                  ((27,42), (68,75)),
                                  ((114,120), )
                                  )
    _fill_coreference_partition(reference_partition, coreference_extent_entities)
    
    ref_document1 = create_document(mention_extents, reference_partition)
    ref_document1.ident = "doc1"
    reference_partition1 = ref_document1.coreference_partition
    ref_document1.coreference_partition = None # To check that we indeed do not use directly the value of the document's coreference partition attribute
    
    mention_extents = ((1,9), (14,42), (27,42), (46,47), (68,75), (83,84), (114,120))
    reference_partition = CoreferencePartition(mention_extents=mention_extents)
    coreference_extent_entities = (((1,9), (14,42), (46,47), (83,84)),
                                  ((27,42), (68,75)),
                                  ((114,120), )
                                  )
    _fill_coreference_partition(reference_partition, coreference_extent_entities)
    
    ref_document2 = create_document(mention_extents, reference_partition)
    ref_document2.ident = "doc2"
    reference_partition2 = ref_document2.coreference_partition
    ref_document2.coreference_partition = None # To check that we indeed do not use directly the value of the document's coreference partition attribute

    # Create two sys versions of the document
    ## First data set
    #original_mention_extents = ((1,9), (14,42), (27,42), (46,47), (68,75), (83,84), (114,120))
    #removed_extents = set(((68,75), ))
    #false_extent = (54,75)
    mention_extents = ((1,9), (14,42), (27,42), (46,47), (54,75), (83,84), (114,120))
    system_partition1 = CoreferencePartition(mention_extents=mention_extents)
    coreference_extent_entities = (((1,9), (14,42), (46,47), ),
                                  ((27,42), ),
                                  ((114,120), ),
                                  ((54,75), (83,84)),
                                  )
    _fill_coreference_partition(system_partition1, coreference_extent_entities)
    
    ## Second data set
    #original_mention_extents = ((1,9), (14,42), (27,42), (46,47), (68,75), (83,84), (114,120))
    #removed_extents = set(((14,42), (68,75), ))
    mention_extents = ((1,9), (27,42), (46,47), (83,84), (114,120))
    system_partition2 = CoreferencePartition(mention_extents=mention_extents)
    coreference_extent_entities = (((1,9), (46,47), ),
                                  ((27,42), ),
                                  ((114,120), ),
                                  ((83,84), ),
                                  )
    _fill_coreference_partition(system_partition2, coreference_extent_entities)
    
    document_ref_partition_predicted_partition_triplets = ((ref_document1, reference_partition1, system_partition1),
                                                              (ref_document2, reference_partition2, system_partition2),
                                                              )
    
    return document_ref_partition_predicted_partition_triplets
    
    



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()