# -*- coding: utf-8 -*-

import unittest
import itertools

from cortex.api.document import Document
from cortex.api.markable import Mention, ExtentComparisonKey
from cortex.api.entity import Entity
from cortex.api.coreference_partition import CoreferencePartition
from cortex.coreference.evaluation.scorer import (MacroAverageScore, MicroAverageScore, DetectionScorer, 
                                              B3Scorer, MucScorer, CEAFScorer, CEAFEntityScorer, 
                                              BLANCScorer, AllScorer, compute_f1_score)
from cortex.utils.memoize import Memoized



class TestDetectionScorer(unittest.TestCase):
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        scorer1 = DetectionScorer()
        scorer2 = DetectionScorer()
        scorer3 = DetectionScorer()
        scorer3._scores = {1: 2}
        
        # Test
        self.assertTrue(*scorer1._inner_eq(scorer2))
        self.assertEqual((False,"_scores"), scorer1._inner_eq(scorer3))
    
    #@unittest.skip
    def test__compute_scores(self):
        # Test parameters
        data = get_detection_scorer_test__compute_score_data()
        
        # Test
        for (key, response), expected_result in data:
            scorer = DetectionScorer()
            actual_result = scorer._compute_scores(key, response)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_update(self):
        # Test parameters
        data, expected_result = get_detection_scorer_test_update_data()
        
        # Test
        scorer = DetectionScorer()
        for doc_id, key, response in data:
            scorer.update(doc_id, key, response)
        for attribute_name, expected_value in expected_result.items():
            actual_value = getattr(scorer, attribute_name)
            self.assertEqual(expected_value, actual_value)



class TestMucScorer(unittest.TestCase):
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        scorer1 = MucScorer()
        scorer2 = MucScorer()
        scorer3 = MucScorer()
        scorer3._scores = {1: 2}
        
        # Test
        self.assertTrue(*scorer1._inner_eq(scorer2))
        self.assertEqual((False, "_scores"), scorer1._inner_eq(scorer3))
    
    #@unittest.skip
    def test__villain_compare(self):
        # Test parameters
        data = get_mucscorer_test__vilain_compare_data()
        
        # Test
        for (ref_partition, sys_partition), (expected_result1, expected_result2) in data:
            scorer = MucScorer()
            actual_result1 = scorer._vilain_compare(ref_partition, sys_partition)
            self.assertEqual(expected_result1, actual_result1)
            
            actual_result2 = scorer._vilain_compare(sys_partition, ref_partition)
            self.assertEqual(expected_result2, actual_result2)
            
            expected_total_mentions_nb = actual_result1[0]
            actual_total_mentions_nb = actual_result2[0]
            self.assertEqual(expected_total_mentions_nb, actual_total_mentions_nb)
    
    #@unittest.skip
    def test__compute_scores(self):
        # Test parameters
        data = get_mucscorer_test__compute_scores_data()
        
        # Test
        for (ref_partition, sys_partition), expected_result in data:
            scorer = MucScorer()
            actual_result = scorer._compute_scores(ref_partition, sys_partition)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_update(self):
        # Test parameters
        data, expected_result = get_mucscorer_test_update_data()
        
        # Test
        scorer = MucScorer()
        for doc_id, key, response in data:
            scorer.update(doc_id, key, response)
        for attribute_name, expected_value in expected_result.items():
            actual_value = getattr(scorer, attribute_name)
            self.assertEqual(expected_value, actual_value)
        
    
    #@unittest.skip
    def test_micro_average_scores(self):
        # Test
        scorer, expected_result = get_mucscorer_test_micro_average_scores_data()
        
        # Test
        actual_result = scorer.compute_micro_average_scores()
        self.assertEqual(expected_result, actual_result)



class TestB3Scorer(unittest.TestCase):
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        scorer1 = B3Scorer()
        scorer2 = B3Scorer()
        scorer3 = B3Scorer()
        scorer3._mention_count += 1
        
        # Test
        self.assertTrue(*scorer1._inner_eq(scorer2))
        self.assertEqual((False,"_mention_count"), scorer1._inner_eq(scorer3))
    
    #@unittest.skip
    def test__compute_score(self):
        # Test parameters
        data = get_b3scorer_test__compute_score_data()
        
        # Test
        for (key, response), expected_result in data:
            scorer = B3Scorer()
            actual_result = scorer._compute_scores(key, response)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_update(self):
        # Test parameters
        data, expected_result = get_b3scorer_test_update_data()
        
        # Test
        scorer = B3Scorer()
        for doc_id, key, response in data:
            scorer.update(doc_id, key, response)
        for attribute_name, expected_value in expected_result.items():
            actual_value = getattr(scorer, attribute_name)
            self.assertEqual(expected_value, actual_value)
    
    #@unittest.skip
    def test_micro_average_scores(self):
        # Test
        scorer, expected_result = get_b3scorer_test_micro_average_scores_data()
        
        # Test
        actual_result = scorer.compute_micro_average_scores()
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_macro_average_scores(self):
        # Test parameters
        scorer, expected_result = get_b3scorer_test_macro_average_scores_data()
        
        # Test
        actual_result = scorer.compute_macro_average_scores()
        self.assertEqual(expected_result, actual_result)



class TestCEAFScorer(unittest.TestCase):
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        scorer1 = CEAFScorer()
        scorer2 = CEAFScorer()
        scorer3 = CEAFScorer()
        scorer3._accum_scores["test_field"] = "test_value"
        
        # Test
        self.assertTrue(*scorer1._inner_eq(scorer2))
        self.assertEqual((False,"_accum_scores"), scorer1._inner_eq(scorer3))
    
    #@unittest.skip
    def test__compute_score(self):
        # Test parameters
        data = get_ceaf_scorer_test__compute_scores_data()
        
        # Test
        for (key, response), expected_result in data:
            scorer = CEAFScorer()
            actual_result = scorer._compute_scores(key, response)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_update(self):
        # Test parameters
        data, expected_result = get_ceaf_scorer_test_update_data()
        
        # Test
        scorer = CEAFScorer()
        for doc_id, key, response in data:
            scorer.update(doc_id, key, response)
        for attribute_name, expected_value in expected_result.items():
            actual_value = getattr(scorer, attribute_name)
            self.assertEqual(expected_value, actual_value)
    
    #@unittest.skip
    def test_micro_average_scores(self):
        # Test
        scorer, expected_result = get_ceaf_scorer_test_micro_average_scores_data()
        
        # Test
        actual_result = scorer.compute_micro_average_scores()
        self.assertEqual(expected_result, actual_result)



class TestCEAFEntityScorer(unittest.TestCase):
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        scorer1 = CEAFEntityScorer()
        scorer2 = CEAFEntityScorer()
        scorer3 = CEAFEntityScorer()
        scorer3._scores.append(("test_field", "test_value"))
        
        # Test
        self.assertTrue(*scorer1._inner_eq(scorer2))
        self.assertEqual((False,"_scores"), scorer1._inner_eq(scorer3))
    
    #@unittest.skip
    def test__compute_score(self):
        # Test parameters
        data = get_ceaf_entity_scorer_test__compute_scores_data()
        
        # Test
        for (key, response), expected_result in data:
            scorer = CEAFEntityScorer()
            actual_result = scorer._compute_scores(key, response)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_update(self):
        # Test parameters
        data, expected_result = get_ceaf_entity_scorer_test_update_data()
        
        # Test
        scorer = CEAFEntityScorer()
        for doc_id, key, response in data:
            scorer.update(doc_id, key, response)
        for attribute_name, expected_value in expected_result.items():
            actual_value = getattr(scorer, attribute_name)
            self.assertEqual(expected_value, actual_value)
    
    #@unittest.skip
    def test_micro_average_scores(self):
        # Test
        scorer, expected_result = get_ceaf_entity_scorer_test_micro_average_scores_data()
        
        # Test
        actual_result = scorer.compute_micro_average_scores()
        self.assertEqual(expected_result, actual_result)



class TestBLANCScorer(unittest.TestCase):
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        scorer1 = BLANCScorer()
        scorer2 = BLANCScorer()
        scorer3 = BLANCScorer()
        scorer3._mention_count += 1
        
        # Test
        self.assertTrue(*scorer1._inner_eq(scorer2))
        self.assertEqual((False,"_mention_count"), scorer1._inner_eq(scorer3))
    
    #@unittest.skip
    def test__get_coreferring_mention_extent_pairs_sets(self):
        # Test parameters
        data, transform_result_to_test_for_equality_fct = get_blanc_scorer_test__get_coreferring_mention_extent_pairs_sets_data()
        
        # Test
        for entities, expected_result in data:
            scorer = BLANCScorer()
            actual_result = scorer._get_coreferring_mention_extent_pairs_sets(entities)
            expected_result = transform_result_to_test_for_equality_fct(expected_result)
            actual_result = transform_result_to_test_for_equality_fct(actual_result)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__get_non_coreferring_mention_extent_pairs_sets(self):
        # Test parameters
        data, transform_result_to_test_for_equality_fct = get_blanc_scorer_test__get_non_coreferring_mention_extent_pairs_sets_data()
        
        # Test
        for entities, expected_result in data:
            scorer = BLANCScorer()
            actual_result = scorer._get_non_coreferring_mention_extent_pairs_sets(entities)
            expected_result = transform_result_to_test_for_equality_fct(expected_result)
            actual_result = transform_result_to_test_for_equality_fct(actual_result)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__compute_score(self):
        # Test parameters
        data = get_blanc_scorer_test__compute_score_data()
        
        # Test
        for (key, response), expected_result in data:
            scorer = BLANCScorer()
            actual_result = scorer._compute_scores(key, response)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_update(self):
        # Test parameters
        data, expected_result = get_blanc_scorer_test_update_data()
        
        # Test
        scorer = BLANCScorer()
        for doc_id, key, response in data:
            scorer.update(doc_id, key, response)
        for attribute_name, expected_value in expected_result.items():
            actual_value = getattr(scorer, attribute_name)
            self.assertEqual(expected_value, actual_value)
    
    #@unittest.skip
    def test_micro_average_scores(self):
        # Test
        scorer, expected_result = get_blanc_scorer_test_micro_average_scores_data()
        
        # Test
        actual_result = scorer.compute_micro_average_scores()
        self.assertEqual(expected_result, actual_result)



class TestAllScorer(unittest.TestCase):
    
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        scorer1 = AllScorer()
        scorer2 = AllScorer()
        scorer3 = AllScorer()
        scorer3.blanc._mention_count = 1
        
        # Test
        self.assertTrue(*scorer1._inner_eq(scorer2))
        self.assertEqual((False, "blanc"), scorer1._inner_eq(scorer3))
    
    #@unittest.skip
    def test_print_overall_scores_light(self):
        # Test parameters
        data, _ = get_blanc_scorer_test_update_data()
        avg_scores_type = "micro"
        all_scorer = AllScorer()
        for doc_id, ref_partition, sys_partition in data:
            all_scorer.update(doc_id, ref_partition, sys_partition)
        all_scorer.print_average_scores(avg_scores_type=avg_scores_type)
        #raise Exception()




@Memoized
def get_test_document_data():
    document_ident = "test_document"
    raw_text = "\nThis text is a sample for a unit test task . It must be adapted to the task , and it must be easily readable for a human as well ." 
    test_document = Document(document_ident, raw_text)
    
    extents = ((1,9), (14,42), (27,42), (46,47), (68,75), (83,84), (114,120))
    mentions = tuple(Mention("{},{}".format(*extent), extent, test_document) for extent in extents)
    test_document.mentions = mentions
    
    return test_document

def _fill_coreference_partition(corefence_partition, coreference_extent_entities):
    for extent_entity in coreference_extent_entities:
        head_mention_extent = extent_entity[0]
        extents = extent_entity[1:]
        for extent in extents:
            corefence_partition.join(head_mention_extent, extent)

@Memoized
def get_ref_sys_coreference_partition_datasets():
    test_document = get_test_document_data()
    ## Reference data set
    reference_partition = CoreferencePartition(mention_extents=(m.extent for m in test_document.mentions))
    coreference_extent_entities = (((1,9), (14,42), (46,47), (83,84)),
                                  ((27,42), (68,75)),
                                  ((114,120), )
                                  )
    
    _fill_coreference_partition(reference_partition, coreference_extent_entities)
    
    ## First data set
    removed_extents = set(((68,75), ))
    false_extent = (54,75)
    response_document1 = Document("test_response_document1", test_document.raw_text)
    response_mentions1 = tuple(sorted(Mention("{},{}".format(*extent), extent, response_document1) for extent in itertools.chain((false_extent,), (m.extent for m in test_document.mentions if m.extent not in removed_extents))))
    response_document1.mentions = response_mentions1
    system_partition1 = CoreferencePartition(mention_extents=(m.extent for m in response_document1.mentions))
    coreference_extent_entities = (((1,9), (14,42), (46,47), ),
                                  ((27,42), ),
                                  ((114,120), ),
                                  ((54,75), (83,84)),
                                  )
    _fill_coreference_partition(system_partition1, coreference_extent_entities)
    
    ## Second data set
    removed_extents = set(((14,42), (68,75), ))
    response_document2 = Document("test_response_document2", test_document.raw_text)
    response_mentions2 = tuple(sorted(Mention("{},{}".format(*extent), extent, response_document2) for extent in (m.extent for m in test_document.mentions if m.extent not in removed_extents)))
    response_document2.mentions = response_mentions2
    system_partition2 = CoreferencePartition(mention_extents=(m.extent for m in response_document2.mentions))
    coreference_extent_entities = (((1,9), (46,47), ),
                                  ((27,42), ),
                                  ((114,120), ),
                                  ((83,84), ),
                                  )
    _fill_coreference_partition(system_partition2, coreference_extent_entities)
    
    return reference_partition, system_partition1, system_partition2


def get_detection_scorer_test__compute_score_data():
    data = []
    
    reference_partition, system_partition1, system_partition2 = get_ref_sys_coreference_partition_datasets()
    
    ## First data set
    ref_mentions_nb = 7
    sys_mentions_nb = 7
    common_mentions_nb = 6
    recall = common_mentions_nb / ref_mentions_nb
    precision = common_mentions_nb / sys_mentions_nb
    f1 = compute_f1_score(recall, precision)
    expected_result = DetectionScorer._DetectionScore(recall, precision, f1, common_mentions_nb, ref_mentions_nb, sys_mentions_nb)
    data.append(((reference_partition, system_partition1), expected_result))
    
    ## Second data set
    ref_mentions_nb = 7
    sys_mentions_nb = 5
    common_mentions_nb = 5
    recall = common_mentions_nb / ref_mentions_nb
    precision = common_mentions_nb / sys_mentions_nb
    f1 = compute_f1_score(recall, precision)
    expected_result = DetectionScorer._DetectionScore(recall, precision, f1, common_mentions_nb, ref_mentions_nb, sys_mentions_nb)
    data.append(((reference_partition, system_partition2), expected_result))
    
    return data

def get_detection_scorer_test_update_data():
    data = []
    for i, ((key, response), _) in enumerate(get_detection_scorer_test__compute_score_data()):
        data.append(("test_document_{}".format(i), key, response))
    
    ref_mentions_nb1 = 7
    sys_mentions_nb1 = 7
    common_mentions_nb1 = 6
    recall1 = common_mentions_nb1 / ref_mentions_nb1
    precision1 = common_mentions_nb1 / sys_mentions_nb1
    f11 = compute_f1_score(recall1, precision1)
    score1 = DetectionScorer._DetectionScore(recall1, precision1, f11, common_mentions_nb1, ref_mentions_nb1, sys_mentions_nb1)
    
    ref_mentions_nb2 = 7
    sys_mentions_nb2 = 5
    common_mentions_nb2 = 5
    recall2 = common_mentions_nb2 / ref_mentions_nb2
    precision2 = common_mentions_nb2 / sys_mentions_nb2
    f12 = compute_f1_score(recall2, precision2)
    score2 = DetectionScorer._DetectionScore(recall2, precision2, f12, common_mentions_nb2, ref_mentions_nb2, sys_mentions_nb2)
    
    expected_result = {"_accum_scores": {"common_mentions_nb": common_mentions_nb1+common_mentions_nb2, 
                                         "ref_mentions_nb": ref_mentions_nb1+ref_mentions_nb2, 
                                         "sys_mentions_nb": sys_mentions_nb1+sys_mentions_nb2}, 
                       "_scores": [("test_document_0", score1),
                                   ("test_document_1", score2),
                                   ], 
                       }
    
    return data, expected_result



def get_mucscorer_test__vilain_compare_data():
    data = []
    
    reference_partition, system_partition1, system_partition2 = get_ref_sys_coreference_partition_datasets()
    
    ## First data set
    common_mentions_nb = (3-1 + 0 + 0 + 1-1) + (0 + 1-1 + 0 + 0) + (0 + 0 + 1-1 + 0) # 2
    ref_mentions_nb = 4-1 + 2-1 + 1-1 # 4
    expected_result1 = (common_mentions_nb, ref_mentions_nb)
    common_mentions_nb2 = (3-1 + 0 + 0) + (0 + 1-1 + 0 + 0) + (0 + 0 + 1-1) + (1-1 + 0 + 0) # 2
    sys_mentions_nb = 3-1 + 1-1 + 1-1 + 2-1 # 3
    expected_result2 = (common_mentions_nb2, sys_mentions_nb)
    expected_results = (expected_result1, expected_result2)
    data.append(((reference_partition, system_partition1), expected_results))
    
    ## Second data set
    common_mentions_nb = (2-1 + 0 + 0 + 1-1) + (0 + 1-1 + 0 + 0) + (0 + 0 + 1-1 + 0) # 1
    ref_mentions_nb = 4-1 + 2-1 + 1-1 # 4
    expected_result1 = (common_mentions_nb, ref_mentions_nb)
    common_mentions_nb2 = (2-1 + 0 + 0) + (0 + 1-1 + 0) + (0 + 0 + 1-1) + (1-1 + 0 + 0) # 1
    sys_mentions_nb = 2-1 + 1-1 + 1-1 + 1-1 # 1
    expected_result2 = (common_mentions_nb2, sys_mentions_nb)
    expected_results = (expected_result1, expected_result2)
    data.append(((reference_partition, system_partition2), expected_results))
    
    return data

def get_mucscorer_test__compute_scores_data():
    data = []
    _data = get_mucscorer_test__vilain_compare_data()
    
    # First data set
    (ref_partition, sys_partition), _ = _data[0]
    correct_mentions_nb = 2
    ref_mentions_nb = 4
    sys_mentions_nb = 3
    recall = 2 / 4
    precision = 2 / 3
    f1 = compute_f1_score(recall, precision)
    expected_result = MucScorer._MucScore(recall, precision, f1, correct_mentions_nb, ref_mentions_nb, sys_mentions_nb)
    data.append(((ref_partition, sys_partition), expected_result))
    
    # Second data test
    (ref_partition, sys_partition), _ = _data[1]
    correct_mentions_nb = 1
    ref_mentions_nb = 4
    sys_mentions_nb = 1
    recall = 1 / 4
    precision = 1 / 1
    f1 = compute_f1_score(recall, precision)
    expected_result = MucScorer._MucScore(recall, precision, f1, correct_mentions_nb, ref_mentions_nb, sys_mentions_nb)
    data.append(((ref_partition, sys_partition), expected_result))
    
    return data

def get_mucscorer_test_update_data():
    data = []
    for i, ((key, response), _) in enumerate(get_mucscorer_test__compute_scores_data()):
        data.append(("test_document_{}".format(i), key, response))
    
    correct_mentions_nb1 = 2
    ref_mentions_nb1 = 4
    sys_mentions_nb1 = 3
    rec1 = 2 / 4
    prec1 = 2 / 3
    f11 = compute_f1_score(rec1, prec1)
    score1 = MucScorer._MucScore(rec1, prec1, f11, correct_mentions_nb1, ref_mentions_nb1, sys_mentions_nb1)
    correct_mentions_nb2 = 1
    ref_mentions_nb2 = 4
    sys_mentions_nb2 = 1
    rec2 = 1 / 4
    prec2 = 1 / 1
    f12 = compute_f1_score(rec2, prec2)
    score2 = MucScorer._MucScore(rec2, prec2, f12, correct_mentions_nb2, ref_mentions_nb2, sys_mentions_nb2)
    expected_result = {"_accum_scores": {"c": correct_mentions_nb1+correct_mentions_nb2, 
                                         "s": sys_mentions_nb1+sys_mentions_nb2, 
                                         "t": ref_mentions_nb1+ref_mentions_nb2}, 
                       "_scores": [("test_document_0", score1),
                                   ("test_document_1", score2),
                                   ], 
                       }
    
    return data, expected_result

def get_mucscorer_test_micro_average_scores_data():
    data = []
    for i, ((key, response), _) in enumerate(get_mucscorer_test__compute_scores_data()):
        data.append(("test_document_{}".format(i), key, response))
    
    scorer = MucScorer()
    for doc_id, key, response in data:
        scorer.update(doc_id, key, response)
    
    correct_mentions_nb1 = 2
    ref_mentions_nb1 = 4
    sys_mentions_nb1 = 3
    correct_mentions_nb2 = 1
    ref_mentions_nb2 = 4
    sys_mentions_nb2 = 1
    recall = (correct_mentions_nb1+correct_mentions_nb2) / (ref_mentions_nb1+ref_mentions_nb2)
    precision = (correct_mentions_nb1+correct_mentions_nb2) / (sys_mentions_nb1+sys_mentions_nb2)
    f1 = compute_f1_score(recall, precision)
    r_num = correct_mentions_nb1+correct_mentions_nb2
    r_den = ref_mentions_nb1+ref_mentions_nb2
    p_num = correct_mentions_nb1+correct_mentions_nb2
    p_den = sys_mentions_nb1+sys_mentions_nb2
    expected_result = MicroAverageScore(recall, precision, f1, r_num, r_den, p_num, p_den)
    
    return scorer, expected_result




def get_b3scorer_test__compute_score_data():
    data = []
    
    reference_partition, system_partition1, system_partition2 = get_ref_sys_coreference_partition_datasets()
    
    ## First data set
    recall = sum((3/4, 3/4, 1/2, 3/4, 0.0, 1/4, 1/1)) / 7
    precision = sum((3/3, 3/3, 1/1, 3/3, 0/2, 1/2, 1/1)) / 7
    f1 = compute_f1_score(recall, precision)
    ref_mention_nb = 7
    expected_result = B3Scorer._B3Score(recall, precision, f1, ref_mention_nb)
    data.append(((reference_partition, system_partition1),expected_result))
    
    ## Second data set
    recall = sum((2/4, 1/2, 2/4, 1/4, 1/1)) / 7
    precision = sum((2/2, 1/1, 2/2, 1/1, 1/1)) / 7
    f1 = compute_f1_score(recall, precision)
    ref_mention_nb = 7
    expected_result = B3Scorer._B3Score(recall, precision, f1, ref_mention_nb)
    data.append(((reference_partition, system_partition2), expected_result))
    
    return data

def get_b3scorer_test_update_data():
    data = []
    for i, ((key, response), _) in enumerate(get_b3scorer_test__compute_score_data()):
        data.append(("test_document_{}".format(i), key, response))
    
    mentions_ct1 = 7
    rec1 = sum((3/4, 3/4, 1/2, 3/4, 0.0, 1/4, 1/1)) / mentions_ct1
    pre1 = sum((3/3, 3/3, 1/1, 3/3, 0/2, 1/2, 1/1)) / mentions_ct1
    f11 = compute_f1_score(rec1, pre1)
    score1 = B3Scorer._B3Score(rec1, pre1, f11, mentions_ct1)
    mentions_ct2 = 7
    rec2 = sum((2/4, 1/2, 2/4, 1/4, 1/1)) / mentions_ct2
    pre2 = sum((2/2, 1/1, 2/2, 1/1, 1/1)) / mentions_ct2
    f12 = compute_f1_score(rec2, pre2)
    score2 = B3Scorer._B3Score(rec2, pre2, f12, mentions_ct2)
    expected_result = {"_accum_scores": {"r": mentions_ct1*rec1+mentions_ct2*rec2, 
                                         "p": mentions_ct1*pre1+mentions_ct2*pre2, 
                                         "f": mentions_ct1*f11+mentions_ct2*f12}, 
                       "_scores": [("test_document_0", score1), 
                                   ("test_document_1", score2),
                                   ], 
                       "_mention_count": 14}
    
    return data, expected_result

def get_b3scorer_test_micro_average_scores_data():
    data = []
    for i, ((key, response), _) in enumerate(get_b3scorer_test__compute_score_data()):
        data.append(("test_document_{}".format(i), key, response))
    
    scorer = B3Scorer()
    for doc_id, key, response in data:
        scorer.update(doc_id, key, response)
    
    rec1 = sum((3/4, 3/4, 1/2, 3/4, 0.0, 1/4, 1/1)) / 7 
    pre1 = sum((3/3, 3/3, 1/1, 3/3, 0/2, 1/2, 1/1)) / 7
    rec2 = sum((2/4, 1/2, 2/4, 1/4, 1/1)) / 7
    pre2 = sum((2/2, 1/1, 2/2, 1/1, 1/1)) / 7
    rec = (7*rec1+7*rec2) / 14
    pre = (7*pre1+7*pre2) / 14
    f1 = compute_f1_score(rec, pre)
    r_num = 7*rec1+7*rec2
    r_den = 14
    p_num = 7*pre1+7*pre2
    p_den = 14
    expected_result = MicroAverageScore(rec, pre, f1, r_num, r_den, p_num, p_den)
    
    return scorer, expected_result

def get_b3scorer_test_macro_average_scores_data():
    data = []
    for i, ((key, response), _) in enumerate(get_b3scorer_test__compute_score_data()):
        data.append(("test_document_{}".format(i), key, response))
    
    scorer = B3Scorer()
    for doc_id, key, response in data:
        scorer.update(doc_id, key, response)
    
    rec1 = sum((3/4, 3/4, 1/2, 3/4, 0.0, 1/4, 1/1)) / 7 
    pre1 = sum((3/3, 3/3, 1/1, 3/3, 0/2, 1/2, 1/1)) / 7
    rec2 = sum((2/4, 1/2, 2/4, 1/4, 1/1)) / 7
    pre2 = sum((2/2, 1/1, 2/2, 1/1, 1/1)) / 7
    rec = (rec1 + rec2) / 2
    pre = (pre1 + pre2) / 2
    f1 = compute_f1_score(rec, pre)
    expected_result = MacroAverageScore(rec, pre, f1)
    
    return scorer, expected_result
    


def get_ceaf_scorer_test__compute_scores_data():
    data = []
    
    reference_partition, system_partition1, system_partition2 = get_ref_sys_coreference_partition_datasets()
    
    ## First data set
    best_map_sim = 1+3+1
    ref_self_sim = 7
    sys_self_sim = 7
    recall = best_map_sim / ref_self_sim
    precision = best_map_sim / sys_self_sim
    f1 = compute_f1_score(recall, precision)
    expected_result = CEAFScorer._CEAFScore(recall, precision, f1, best_map_sim, ref_self_sim, sys_self_sim)
    data.append(((reference_partition, system_partition1), expected_result))
    
    
    ## Second data set
    best_map_sim = 1+2+1
    ref_self_sim = 7
    sys_self_sim = 5
    recall = best_map_sim / ref_self_sim
    precision = best_map_sim / sys_self_sim
    f1 = compute_f1_score(recall, precision)
    expected_result = CEAFScorer._CEAFScore(recall, precision, f1, best_map_sim, ref_self_sim, sys_self_sim)
    data.append(((reference_partition, system_partition2), expected_result))
    
    return data

def get_ceaf_scorer_test_update_data():
    data = []
    for i, ((key, response), _) in enumerate(get_ceaf_scorer_test__compute_scores_data()):
        data.append(("test_document_{}".format(i), key, response))
    
    best_map_sim1 = 5
    ref_self_sim1 = 7
    sys_self_sim1 = 7
    recall1 = best_map_sim1 / ref_self_sim1
    precision1 = best_map_sim1 / sys_self_sim1
    f11 = compute_f1_score(recall1, precision1)
    score1 = CEAFScorer._CEAFScore(recall1, precision1, f11, best_map_sim1, ref_self_sim1, sys_self_sim1)
    best_map_sim2 = 4
    ref_self_sim2 = 7
    sys_self_sim2 = 5
    recall2 = best_map_sim2 / ref_self_sim2
    precision2 = best_map_sim2 / sys_self_sim2
    f12 = compute_f1_score(recall2, precision2)
    score2 = CEAFScorer._CEAFScore(recall2, precision2, f12, best_map_sim2, ref_self_sim2, sys_self_sim2)
    expected_result = {"_accum_scores": {"c": best_map_sim1+best_map_sim2, 
                                         "t": ref_self_sim1+ref_self_sim2, 
                                         "s": sys_self_sim1+sys_self_sim2}, 
                       "_scores": [("test_document_0", score1),
                                   ("test_document_1", score2),
                                   ], 
                       }
    
    return data, expected_result

def get_ceaf_scorer_test_micro_average_scores_data():
    data = []
    for i, ((key, response), _) in enumerate(get_ceaf_scorer_test__compute_scores_data()):
        data.append(("test_document_{}".format(i), key, response))
    
    scorer = CEAFScorer()
    for doc_id, key, response in data:
        scorer.update(doc_id, key, response)
    
    best_map_sim1 = 5
    ref_self_sim1 = 7
    sys_self_sim1 = 7
    best_map_sim2 = 4
    ref_self_sim2 = 7
    sys_self_sim2 = 5
    recall = (best_map_sim1+best_map_sim2) / (ref_self_sim1+ref_self_sim2)
    precision = (best_map_sim1+best_map_sim2) / (sys_self_sim1+sys_self_sim2)
    f1 = compute_f1_score(recall, precision)
    r_num = best_map_sim1+best_map_sim2
    r_den = ref_self_sim1+ref_self_sim2
    p_num = best_map_sim1+best_map_sim2
    p_den = sys_self_sim1+sys_self_sim2
    expected_result = MicroAverageScore(recall, precision, f1, r_num, r_den, p_num, p_den)
    
    return scorer, expected_result



def get_ceaf_entity_scorer_test__compute_scores_data():
    data = []
    
    reference_partition, system_partition1, system_partition2 = get_ref_sys_coreference_partition_datasets()
    
    ## First data set
    best_map_sim = (2*1)/(2+1) + (2*3)/(4+3) + (2*1)/(1+1) 
    ref_self_sim = 3
    sys_self_sim = 4
    recall = best_map_sim / ref_self_sim
    precision = best_map_sim / sys_self_sim
    f1 = compute_f1_score(recall, precision)
    expected_result = CEAFScorer._CEAFScore(recall, precision, f1, best_map_sim, ref_self_sim, sys_self_sim)
    data.append(((reference_partition, system_partition1), expected_result))
    
    
    ## Second data set
    best_map_sim = (2*1)/(2+1) + (2*2)/(4+2) + (2*1)/(1+1)
    ref_self_sim = 3
    sys_self_sim = 4
    recall = best_map_sim / ref_self_sim
    precision = best_map_sim / sys_self_sim
    f1 = compute_f1_score(recall, precision)
    expected_result = CEAFScorer._CEAFScore(recall, precision, f1, best_map_sim, ref_self_sim, sys_self_sim)
    data.append(((reference_partition, system_partition2), expected_result))
    
    return data

def get_ceaf_entity_scorer_test_update_data():
    data = []
    for i, ((key, response), _) in enumerate(get_ceaf_scorer_test__compute_scores_data()):
        data.append(("test_document_{}".format(i), key, response))
    
    best_map_sim1 = (2*1)/(2+1) + (2*3)/(4+3) + (2*1)/(1+1)
    ref_self_sim1 = 3
    sys_self_sim1 = 4
    recall1 = best_map_sim1 / ref_self_sim1
    precision1 = best_map_sim1 / sys_self_sim1
    f11 = compute_f1_score(recall1, precision1)
    score1 = CEAFScorer._CEAFScore(recall1, precision1, f11, best_map_sim1, ref_self_sim1, sys_self_sim1)
    best_map_sim2 = (2*1)/(2+1) + (2*2)/(4+2) + (2*1)/(1+1)
    ref_self_sim2 = 3
    sys_self_sim2 = 4
    recall2 = best_map_sim2 / ref_self_sim2
    precision2 = best_map_sim2 / sys_self_sim2
    f12 = compute_f1_score(recall2, precision2)
    score2 = CEAFScorer._CEAFScore(recall2, precision2, f12, best_map_sim2, ref_self_sim2, sys_self_sim2)
    expected_result = {"_accum_scores": {"c": best_map_sim1+best_map_sim2, 
                                         "t": ref_self_sim1+ref_self_sim2, 
                                         "s": sys_self_sim1+sys_self_sim2}, 
                       "_scores": [("test_document_0", score1),
                                   ("test_document_1", score2),
                                   ], 
                       }
    
    return data, expected_result

def get_ceaf_entity_scorer_test_micro_average_scores_data():
    data = []
    for i, ((key, response), _) in enumerate(get_ceaf_entity_scorer_test__compute_scores_data()):
        data.append(("test_document_{}".format(i), key, response))
    
    scorer = CEAFEntityScorer()
    for doc_id, key, response in data:
        scorer.update(doc_id, key, response)
    
    best_map_sim1 = (2*1)/(2+1) + (2*3)/(4+3) + (2*1)/(1+1)
    ref_self_sim1 = 3
    sys_self_sim1 = 4
    best_map_sim2 = (2*1)/(2+1) + (2*2)/(4+2) + (2*1)/(1+1)
    ref_self_sim2 = 3
    sys_self_sim2 = 4
    recall = (best_map_sim1+best_map_sim2) / (ref_self_sim1+ref_self_sim2)
    precision = (best_map_sim1+best_map_sim2) / (sys_self_sim1+sys_self_sim2)
    f1 = compute_f1_score(recall, precision)
    r_num = best_map_sim1+best_map_sim2
    r_den = ref_self_sim1+ref_self_sim2
    p_num = best_map_sim1+best_map_sim2
    p_den = sys_self_sim1+sys_self_sim2
    expected_result = MicroAverageScore(recall, precision, f1, r_num, r_den, p_num, p_den)
    
    return scorer, expected_result



def get_blanc_scorer_test__get_coreferring_mention_extent_pairs_sets_data():
    data = []
    
    # Reference partition
    entities = [Entity(((1,9), (14,42), (46,47), (83,84))),
                Entity(((27,42), (68,75))),
                Entity(((114,120),))]
    expected_result = [frozenset(((1,9), (14,42))), frozenset(((1,9), (46,47))), frozenset(((1,9), (83,84))), frozenset(((14,42), (46,47))), frozenset(((14,42), (83,84))), frozenset(((46,47), (83,84))), 
                       frozenset(((27,42), (68,75))), 
                       ]
    data.append((entities, expected_result))
    
    # First predicted partition
    entities = [Entity(((1,9), (14,42), (46,47))),
                Entity(((27,42),)),
                Entity(((114,120),)),
                Entity(((54,75), (83,84)))]
    expected_result = [frozenset(((1,9), (14,42))), frozenset(((1,9), (46,47))), frozenset(((14,42), (46,47))), 
                       frozenset(((54,75), (83,84))), 
                       ]
    data.append((entities, expected_result))
    
    # Second predicted partition
    entities = [Entity(((1,9), (46,47))),
                Entity(((27,42),)),
                Entity(((114,120),)),
                Entity(((83,84),))]
    expected_result = [frozenset(((1,9), (46,47))),  
                       ]
    data.append((entities, expected_result))
    
    def _transform_result_to_test_for_equality(result):
        return {tuple(sorted(frozenset_, key=ExtentComparisonKey)): frozenset_ for frozenset_ in result}
        
    return data, _transform_result_to_test_for_equality

def get_blanc_scorer_test__get_non_coreferring_mention_extent_pairs_sets_data():
    data = []
    
    # Reference partition
    entities = [Entity(((1,9), (14,42), (46,47), (83,84))),
                Entity(((27,42), (68,75))),
                Entity(((114,120),))]
    expected_result = [frozenset(((1,9), (27,42))), frozenset(((1,9), (68,75))), frozenset(((1,9), (114,120))), 
                       frozenset(((14,42), (27,42))), frozenset(((14,42), (68,75))), frozenset(((14,42), (114,120))), 
                       frozenset(((46,47), (27,42))), frozenset(((46,47), (68,75))), frozenset(((46,47), (114,120))), 
                       frozenset(((83,84), (27,42))), frozenset(((83,84), (68,75))), frozenset(((83,84), (114,120))), 
                       frozenset(((27,42), (114,120))), 
                       frozenset(((68,75), (114,120))), 
                       ]
    data.append((entities, expected_result))
    
    # First predicted partition
    entities = [Entity(((1,9), (14,42), (46,47))),
                Entity(((27,42),)),
                Entity(((114,120),)),
                Entity(((54,75), (83,84)))]
    expected_result = [frozenset(((1,9), (27,42))), frozenset(((1,9), (114,120))), frozenset(((1,9), (54,75))), frozenset(((1,9), (83,84))), 
                       frozenset(((14,42), (27,42))), frozenset(((14,42), (114,120))), frozenset(((14,42), (54,75))), frozenset(((14,42), (83,84))), 
                       frozenset(((46,47), (27,42))), frozenset(((46,47), (114,120))), frozenset(((46,47), (54,75))), frozenset(((46,47), (83,84))), 
                       frozenset(((27,42), (114,120))), frozenset(((27,42), (54,75))), frozenset(((27,42), (83,84))), 
                       frozenset(((114,120), (54,75))), frozenset(((114,120), (83,84))), 
                       ]
    data.append((entities, expected_result))
    
    # Second predicted partition
    entities = [Entity(((1,9), (46,47))),
                Entity(((27,42),)),
                Entity(((114,120),)),
                Entity(((83,84),))]
    expected_result = [frozenset(((1,9), (27,42))), frozenset(((1,9), (114,120))), frozenset(((1,9), (83,84))), 
                       frozenset(((46,47), (27,42))), frozenset(((46,47), (114,120))), frozenset(((46,47), (83,84))), 
                       frozenset(((27,42), (114,120))), frozenset(((27,42), (83,84))), 
                       frozenset(((114,120), (83,84))), 
                       ]
    data.append((entities, expected_result))
    
    def _transform_result_to_test_for_equality(result):
        return {tuple(sorted(frozenset_, key=ExtentComparisonKey)): frozenset_ for frozenset_ in result}
        
    return data, _transform_result_to_test_for_equality

def get_blanc_scorer_test__compute_score_data():
    data = []
    
    reference_partition, system_partition1, system_partition2 = get_ref_sys_coreference_partition_datasets()
    
    ## First data set
    rc = 3 # 1+1+1 = 3
    wc = 1 # 1 = 1
    rn = 9 # 1+1+1+1+1+1+1+1+1 = 9
    wn = 8 # 1+1+1+1+1+1+1+1 = 8
    Pc = 0 if rc == 0 else rc*1.0/(rc+wc)
    Pn = 0 if rn == 0 else rn*1.0/(rn+wn)
    precision = (Pc+Pn) / 2
    Rc = 0 if rc == 0 else rc*1.0/(rc+wn)
    Rn = 0 if rn == 0 else rn*1.0/(rn+wc)
    recall  = (Rc+Rn)/2
    Fc = compute_f1_score(Pc, Rc)
    Fn = compute_f1_score(Pn, Rn)
    f1 = (Fc+Fn) / 2
    ref_mention_ct = 7
    expected_result = BLANCScorer._BLANCScore(recall, precision, f1, ref_mention_ct)
    data.append(((reference_partition, system_partition1), expected_result))
    
    ## Second data set
    rc = 1 # 1 = 1
    wc = 0 # 0
    rn = 7 # 1+1+1+1+1+1+1 = 7
    wn = 2 # 1+1 = 2
    Pc = 0 if rc == 0 else rc*1.0/(rc+wc)
    Pn = 0 if rn == 0 else rn*1.0/(rn+wn)
    precision = (Pc+Pn) / 2
    Rc = 0 if rc == 0 else rc*1.0/(rc+wn)
    Rn = 0 if rn == 0 else rn*1.0/(rn+wc)
    recall  = (Rc+Rn)/2
    Fc = compute_f1_score(Pc, Rc)
    Fn = compute_f1_score(Pn, Rn)
    f1 = (Fc+Fn) / 2
    ref_mention_ct = 7
    expected_result = BLANCScorer._BLANCScore(recall, precision, f1, ref_mention_ct)
    data.append(((reference_partition, system_partition2), expected_result))
    
    return data

def get_blanc_scorer_test_update_data():
    data = []
    for i, ((key, response), _) in enumerate(get_blanc_scorer_test__compute_score_data()):
        data.append(("test_document_{}".format(i), key, response))
    
    rc = 3 # 1+1+1 = 3
    wc = 1 # 1 = 1
    rn = 9 # 1+1+1+1+1+1+1+1+1 = 9
    wn = 8 # 1+1+1+1+1+1+1+1 = 8
    Pc = 0 if rc == 0 else rc*1.0/(rc+wc)
    Pn = 0 if rn == 0 else rn*1.0/(rn+wn)
    precision1 = (Pc+Pn) / 2
    Rc = 0 if rc == 0 else rc*1.0/(rc+wn)
    Rn = 0 if rn == 0 else rn*1.0/(rn+wc)
    recall1  = (Rc+Rn)/2
    Fc = compute_f1_score(Pc, Rc)
    Fn = compute_f1_score(Pn, Rn)
    f11 = (Fc+Fn) / 2
    ref_mention_ct1 = 7
    score1 = BLANCScorer._BLANCScore(recall1, precision1, f11, ref_mention_ct1)
    
    rc = 1 # 1 = 1
    wc = 0 # 0
    rn = 7 # 1+1+1+1+1+1+1 = 7
    wn = 2 # 1+1 = 2
    Pc = 0 if rc == 0 else rc*1.0/(rc+wc)
    Pn = 0 if rn == 0 else rn*1.0/(rn+wn)
    precision2 = (Pc+Pn) / 2
    Rc = 0 if rc == 0 else rc*1.0/(rc+wn)
    Rn = 0 if rn == 0 else rn*1.0/(rn+wc)
    recall2  = (Rc+Rn)/2
    Fc = compute_f1_score(Pc, Rc)
    Fn = compute_f1_score(Pn, Rn)
    f12 = (Fc+Fn) / 2
    ref_mention_ct2 = 7
    score2 = BLANCScorer._BLANCScore(recall2, precision2, f12, ref_mention_ct2)
    
    expected_result = {"_accum_scores": {"r": ref_mention_ct1*recall1+ref_mention_ct2*recall2, 
                                         "p": ref_mention_ct1*precision1+ref_mention_ct2*precision2, 
                                         "f": ref_mention_ct1*f11+ref_mention_ct2*f12}, 
                       "_scores": [("test_document_0", score1), 
                                   ("test_document_1", score2),
                                   ], 
                       "_mention_count": 14}
    
    return data, expected_result

def get_blanc_scorer_test_micro_average_scores_data():
    data = []
    for i, ((key, response), _) in enumerate(get_blanc_scorer_test__compute_score_data()):
        data.append(("test_document_{}".format(i), key, response))
    
    scorer = BLANCScorer()
    for doc_id, key, response in data:
        scorer.update(doc_id, key, response)
    
    rc = 3 # 1+1+1 = 3
    wc = 1 # 1 = 1
    rn = 9 # 1+1+1+1+1+1+1+1+1 = 9
    wn = 8 # 1+1+1+1+1+1+1+1 = 8
    Pc = 0 if rc == 0 else rc*1.0/(rc+wc)
    Pn = 0 if rn == 0 else rn*1.0/(rn+wn)
    precision1 = (Pc+Pn) / 2
    Rc = 0 if rc == 0 else rc*1.0/(rc+wn)
    Rn = 0 if rn == 0 else rn*1.0/(rn+wc)
    recall1  = (Rc+Rn)/2
    ref_mention_ct1 = 7
    
    rc = 1 # 1 = 1
    wc = 0 # 0
    rn = 7 # 1+1+1+1+1+1+1 = 7
    wn = 2 # 1+1 = 2
    Pc = 0 if rc == 0 else rc*1.0/(rc+wc)
    Pn = 0 if rn == 0 else rn*1.0/(rn+wn)
    precision2 = (Pc+Pn) / 2
    Rc = 0 if rc == 0 else rc*1.0/(rc+wn)
    Rn = 0 if rn == 0 else rn*1.0/(rn+wc)
    recall2  = (Rc+Rn)/2
    ref_mention_ct2 = 7
    r_den = p_den = ref_mention_ct1 + ref_mention_ct2
    r_num = ref_mention_ct1*recall1+ref_mention_ct2*recall2
    p_num = ref_mention_ct1*precision1+ref_mention_ct2*precision2
    recall = r_num / r_den
    precision = p_num / p_den
    f1 = compute_f1_score(recall, precision)
    
    expected_result = MicroAverageScore(recall, precision, f1, r_num, r_den, p_num, p_den)
    
    return scorer, expected_result




if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()