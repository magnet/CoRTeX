# -*- coding: utf-8 -*-

import unittest
import itertools

from cortex.tools import Mapping
from cortex.api.document import Document
from cortex.api.markable import Mention
from cortex.api.coreference_partition import CoreferencePartition
from cortex.coreference.evaluation.matcher import Matcher
from cortex.utils.memoize import Memoized


class TestMatcher(unittest.TestCase):
    
    #@unittest.skip
    def test__update_new_partition_with_previous_partition_info(self):
        # Test parameters
        (new_partition, previous_partition), _, expected_result = get_matcher_test__update_new_partition_with_previous_partition_info_data() #configuration
        matcher = Matcher()
        
        # Test
        self.assertNotEqual(expected_result, new_partition)
        matcher._update_new_partition_with_previous_partition_info(new_partition, previous_partition)
        self.assertEqual(expected_result, new_partition)
        

    #@unittest.skip
    def test_match(self):
        # Test parameters
        data = get_matcher_test_match_data()
        
        # Test
        for (ref_partition, sys_partition), _, (expected_new_ref_partition, expected_new_sys_partition) in data: #configuration
            matcher = Matcher()
            actual_new_ref_partition, actual_new_sys_partition = matcher.match(ref_partition, sys_partition)
            self.assertEqual(expected_new_ref_partition, actual_new_ref_partition)
            self.assertEqual(expected_new_sys_partition, actual_new_sys_partition)




@Memoized
def get_test_document_data():
    document_ident = "test_document"
    raw_text = "\nThis text is a sample for a unit test task . It must be adapted to the task , and it must be easily readable for a human as well ." 
    test_document = Document(document_ident, raw_text)
    
    extents = ((1,9), (14,42), (27,42), (46,47), (68,75), (83,84), (114,120))
    mentions = tuple(Mention("{},{}".format(*extent), extent, test_document) for extent in extents)
    test_document.mentions = mentions
    
    return test_document

def _fill_coreference_partition(corefence_partition, coreference_extent_entities):
    for extent_entity in coreference_extent_entities:
        head_mention_extent = extent_entity[0]
        extents = extent_entity[1:]
        for extent in extents:
            corefence_partition.join(head_mention_extent, extent)

@Memoized
def get_ref_sys_coreference_partition_datasets():
    test_document = get_test_document_data()
    ## Reference data set
    reference_partition = CoreferencePartition(mention_extents=(m.extent for m in test_document.mentions))
    coreference_extent_entities = (((1,9), (14,42), (46,47), (83,84)),
                                  ((27,42), (68,75)),
                                  ((114,120), )
                                  )
    
    _fill_coreference_partition(reference_partition, coreference_extent_entities)
    
    ## First data set
    removed_extents = set(((68,75), ))
    false_extent = (54,75)
    response_document1 = Document("test_response_document1", test_document.raw_text)
    response_mentions1 = tuple(sorted(Mention("{},{}".format(*extent), extent, response_document1) for extent in itertools.chain((false_extent,), (m.extent for m in test_document.mentions if m.extent not in removed_extents))))
    response_document1.mentions = response_mentions1
    system_partition1 = CoreferencePartition(mention_extents=(m.extent for m in response_document1.mentions))
    coreference_extent_entities = (((1,9), (14,42), (46,47), ),
                                  ((27,42), ),
                                  ((114,120), ),
                                  ((54,75), (83,84)),
                                  )
    _fill_coreference_partition(system_partition1, coreference_extent_entities)
    
    ## Second data set
    removed_extents = set(((14,42), (68,75), ))
    response_document2 = Document("test_response_document2", test_document.raw_text)
    response_mentions2 = tuple(sorted(Mention("{},{}".format(*extent), extent, response_document2) for extent in (m.extent for m in test_document.mentions if m.extent not in removed_extents)))
    response_document2.mentions = response_mentions2
    system_partition2 = CoreferencePartition(mention_extents=(m.extent for m in response_document2.mentions))
    coreference_extent_entities = (((1,9), (46,47), ),
                                  ((27,42), ),
                                  ((114,120), ),
                                  ((83,84), ),
                                  )
    _fill_coreference_partition(system_partition2, coreference_extent_entities)
    
    return reference_partition, system_partition1, system_partition2




def get_matcher_test__update_new_partition_with_previous_partition_info_data():
    previous_coreference_partition, _, _ = get_ref_sys_coreference_partition_datasets()
    
    configuration = Mapping({"remove_singletons": False})
    new_coreference_partition = CoreferencePartition(mention_extents=previous_coreference_partition.get_universe())
    expected_result = CoreferencePartition(entities=tuple(previous_coreference_partition))
    
    return (new_coreference_partition, previous_coreference_partition), configuration, expected_result

'''
coreference_extent_entities = (((1,9), (14,42), (46,47), (83,84)),
                              ((27,42), (68,75)),
                              ((114,120), )
                              )

coreference_extent_entities = (((1,9), (14,42), (46,47), ),
                              ((27,42), ),
                              ((114,120), ),
                              ((54,75), (83,84)),
                              )

coreference_extent_entities = (((1,9), (46,47), ),
                              ((27,42), ),
                              ((114,120), ),
                              ((83,84), ),
                              )
'''
def get_matcher_test_match_data():
    data = []
    
    reference_partition, system_partition1, system_partition2 = get_ref_sys_coreference_partition_datasets()
    
    # First data set
    ## First data set with singletons
    configuration = Mapping({"remove_singletons": False})
    new_ref_coreference_extent_entities = (((1,9), (14,42), (46,47), (83,84)),
                                           ((27,42), (68,75)),
                                           ((114,120), )
                                          )
    new_sys_ref_coreference_extent_entities = (((1,9), (14,42), (46,47), ),
                                               ((27,42), ),
                                               ((114,120), ),
                                               ((54,75), (83,84)),
                                              )
    new_universe = set(itertools.chain(itertools.chain(*new_ref_coreference_extent_entities), itertools.chain(*new_sys_ref_coreference_extent_entities)))
    new_ref_partition = CoreferencePartition(mention_extents=new_universe)
    _fill_coreference_partition(new_ref_partition, new_ref_coreference_extent_entities)
    new_sys_partition = CoreferencePartition(mention_extents=new_universe)
    _fill_coreference_partition(new_sys_partition, new_sys_ref_coreference_extent_entities)
    expected_result = (new_ref_partition, new_sys_partition)
    data.append(((reference_partition, system_partition1), configuration, expected_result))
    
    ## First data set without singletons
    configuration = Mapping({"remove_singletons": True})
    new_ref_coreference_extent_entities = (((1,9), (14,42), (46,47), (83,84)),
                                           ((27,42), (68,75)),
                                           ((114,120), )
                                          )
    new_sys_ref_coreference_extent_entities = (((1,9), (14,42), (46,47), ),
                                               ((54,75), (83,84)),
                                              )
    new_universe = set(itertools.chain(itertools.chain(*new_ref_coreference_extent_entities), itertools.chain(*new_sys_ref_coreference_extent_entities)))
    new_ref_partition = CoreferencePartition(mention_extents=new_universe)
    _fill_coreference_partition(new_ref_partition, new_ref_coreference_extent_entities)
    new_sys_partition = CoreferencePartition(mention_extents=new_universe)
    _fill_coreference_partition(new_sys_partition, new_sys_ref_coreference_extent_entities)
    expected_result = (new_ref_partition, new_sys_partition)
    data.append(((reference_partition, system_partition1), configuration, expected_result))
    
    # Second data set
    ## Second data set with singletons
    configuration = Mapping({"remove_singletons": False})
    new_ref_coreference_extent_entities = (((1,9), (14,42), (46,47), (83,84)),
                                           ((27,42), (68,75)),
                                           ((114,120), )
                                          )
    new_sys_ref_coreference_extent_entities = (((1,9), (46,47), ),
                                               ((27,42), ),
                                               ((114,120), ),
                                               ((83,84), ),
                                              )
    new_universe = set(itertools.chain(itertools.chain(*new_ref_coreference_extent_entities), itertools.chain(*new_sys_ref_coreference_extent_entities)))
    new_ref_partition = CoreferencePartition(mention_extents=new_universe)
    _fill_coreference_partition(new_ref_partition, new_ref_coreference_extent_entities)
    new_sys_partition = CoreferencePartition(mention_extents=new_universe)
    _fill_coreference_partition(new_sys_partition, new_sys_ref_coreference_extent_entities)
    expected_result = (new_ref_partition, new_sys_partition)
    data.append(((reference_partition, system_partition2), configuration, expected_result))
    
    ## Second data set without singletons
    configuration = Mapping({"remove_singletons": True})
    new_ref_coreference_extent_entities = (((1,9), (14,42), (46,47), (83,84)),
                                           ((27,42), (68,75)),
                                           ((114,120), )
                                          )
    new_sys_ref_coreference_extent_entities = (((1,9), (46,47), ),
                                              )
    new_universe = set(itertools.chain(itertools.chain(*new_ref_coreference_extent_entities), itertools.chain(*new_sys_ref_coreference_extent_entities)))
    new_ref_partition = CoreferencePartition(mention_extents=new_universe)
    _fill_coreference_partition(new_ref_partition, new_ref_coreference_extent_entities)
    new_sys_partition = CoreferencePartition(mention_extents=new_universe)
    _fill_coreference_partition(new_sys_partition, new_sys_ref_coreference_extent_entities)
    expected_result = (new_ref_partition, new_sys_partition)
    data.append(((reference_partition, system_partition2), configuration, expected_result))
    
    return data



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()