# -*- coding: utf-8 -*-

import unittest

from cortex.languages.french.mention_characterizer import FrenchMentionCharacterizer

class TestFrenchMentionCharacterizer(unittest.TestCase):

    #@unittest.skip
    def test_enrich_documents_mentions(self):
        # Test data
        test_document = get_test_data() 
        english_mention_characterizer = FrenchMentionCharacterizer()
        documents = (test_document,)

        # Test
        english_mention_characterizer.enrich_documents_mentions(documents)


def get_test_data():
    import os
    from cortex.io.conll2012_reader import CONLL2012DocumentReader
    folder_path = os.path.join(os.path.dirname(__file__), "data")
    file_name = "test.conll2012"
    file_path = os.path.join(folder_path, file_name)
    document = CONLL2012DocumentReader.parse(file_path)
    return document


"""
print(m1)
print(m2)
result2, message2 = m1._inner_eq(m2)
if message2:
    attr1 = getattr(m1, message2)
    attr2 = getattr(m2, message2)
    print(attr1)
    print(attr2)
    print(m1.named_entity)
self.assertTrue(result2, message2)
"""


'''
pprint(expected_mentions)
pprint(actual_mentions)
l1 = expected_mentions
l2 = actual_mentions
i = 0
m1 = l1[i]
m2 = l2[i]

attribute_name = "gender"
attr1 = getattr(m1, attribute_name)
attr2 = getattr(m2, attribute_name)
print(attr1)
print(attr2)
#result3, message3 = attr1._inner_eq(attr2)
#self.assertTrue(result3, message3)
result2, message2 = m1._inner_eq(m2)
self.assertTrue(result2, message2)
'''



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()