# -*- coding: utf-8 -*-

import unittest

from cortex.io import CONLL2012DocumentReader, CONLLUDocumentReader, check_and_merge_conllu_document_into_conll2012_document
from cortex.languages.french.mention_detector import FrenchMentionDetector
from cortex.api.document_buffer import deepcopy_document, DocumentBuffer


class TestFrenchMentionDetector(unittest.TestCase):
    
    #@unittest.skip
    def test__detect_mentions(self):
        # Test data
        #(args, kwargs), (offset, expected_mention_token_based_definitions) = get_test__detect_mentions_data()
        (args, kwargs), (offset, expected_mention_token_based_definitions) = get_test__detect_mentions_data2()
        document = args[0]
        mention_characterizer = FrenchMentionDetector()
        
        # Test
        actual_mention_head_extent2mention_extent = mention_characterizer._detect_mentions(*args, **kwargs)
        actual_mention_token_based_definitions = _convert_mention_head_extent2mention_extent_to_used_defintion(actual_mention_head_extent2mention_extent, 
                                                                                                               document, offset=offset)
        expected_set = set(expected_mention_token_based_definitions)
        actual_set = set(actual_mention_token_based_definitions)
        self.assertEqual(expected_set, actual_set)


"""
Must be able to specify the mention extent and their head extent from collection of tokens, 
represented as index of tokens in their respective sentence => from the sentence's index
Ex: (sentence_index, 
     (mention_beginning_token_sentence_index, mention_ending_token_sentence_index), 
     (mention_head_beginning_token_sentence_index, mention_head_ending_token_sentence_index)
    )
Needs to have a function to transform the output of the _detected_mentions method into a collection 
of the corresponding definition protocol defined above, so as to be able to easily compare the two
"""
def _convert_mention_head_extent2mention_extent_to_used_defintion(mention_head_extent2mention_extent, document, offset=False):
    """
    :param offset: boolean, whether or not to begin counting sentences and tokens from 1 (TRue) or 0 
    (False) in order to define sentence indices and token indices
    """
    offset = int(offset)
    new_document = deepcopy_document(document)
    document_buffer = DocumentBuffer(new_document)
    mentions_data = {mention_extent: {"ident": str(mention_extent), "head_extent": mention_head_extent}\
                     for mention_head_extent, mention_extent in mention_head_extent2mention_extent.items()
                     }
    
    document_buffer.add_mentions(mentions_data)
    document_buffer.flush(strict=True)
    
    for sentence in new_document.sentences:
        for i, token in enumerate(sentence.tokens, offset):
            token.sentence_index = i
    
    mention_token_based_definitions = []
    for mention in new_document.mentions:
        mention_beginning_token_sentence_index = mention.tokens[0].sentence_index
        mention_ending_token_sentence_index = mention.tokens[-1].sentence_index
        mention_head_beginning_token_sentence_index = mention.head_tokens[0].sentence_index
        mention_head_ending_token_sentence_index = mention.head_tokens[-1].sentence_index
        sentence_index = new_document.extent2sentence_index[mention.sentence.extent] + offset
        mention_token_based_definition = (sentence_index, 
                                          (mention_beginning_token_sentence_index, mention_ending_token_sentence_index), 
                                          (mention_head_beginning_token_sentence_index, mention_head_ending_token_sentence_index)
                                          )            
        mention_token_based_definitions.append(mention_token_based_definition)
    
    return mention_token_based_definitions
    

def get_test__detect_mentions_data():
    conll2012_formatted_test_document = """text.conllu    0    1    La    DET    (ROOT(SENT(NP*    le    *    *    *    -    -
text.conllu    0    2    souris    NC    *)    souris    *    *    *    -    -
text.conllu    0    3    a    V    (VN*    avoir    *    *    *    -    -
text.conllu    0    4    mangé    VPP    *)    manger    *    *    *    -    -
text.conllu    0    5    .    PUNC    *))    .    *    *    *    -    -

text.conllu    0    1    Maintenant    ADV    (ROOT(SENT*    maintenant    *    *    *    -    -
text.conllu    0    2    elle    CLS    (VN*    elle    *    *    *    -    -
text.conllu    0    3    dort    V    *)    dormir    *    *    *    -    -
text.conllu    0    4    .    PUNC    *))    .    *    *    *    -    -
""" 
    lines = conll2012_formatted_test_document.split("\n")
    line_iterator = iter(lines)
    
    test_document = CONLL2012DocumentReader.parse(line_iterator)
    offset = True
    expected_mention_token_based_definitions = [(1, (1,2), (2,2)),
                                                (2, (2,2), (2,2)),
                                                ]
    
    
    args = (test_document,)
    kwargs = {"detect_verbs": True}
    
    return (args, kwargs), (offset, expected_mention_token_based_definitions)


def get_test__detect_mentions_data2():
    test_document, offset = get_test_document()
    
    expected_mention_token_based_definitions = [(1, (1,1), (1,1)), # NP
                                                
                                                (2, (1,9), (1,1)), # NP
                                                
                                                (3, (1,1), (1,1)), # NP
                                                
                                                (4, (1,5), (1,1)), # NP
                                                (4, (9, 9), (9, 9)), # NP
                                                
                                                (5, (1,2), (1,1)), # NP
                                                
                                                (6, (1,21), (1,1)), # NP
                                                (6, (1, 2), (1, 2)), # Named entity
                                                (6, (5, 8), (7, 7)), # NP
                                                (6, (10, 13), (10, 10)), # NP
                                                (6, (16, 19), (18, 18)), # NP
                                                (6, (21, 21), (21, 21)), # Clitic
                                                (6, (24,28), (25,25)), # NP
                                                (6, (27, 27), (27, 27)), # NP
                                                
                                                (8, (1,1), (1,1)), # NP
                                                
                                                (9, (1,1), (1,1)), # Clitic
                                                (9, (5,5), (5,5)), # NP
                                                (9, (10,10), (10,10)), # Clitic
                                                (9, (11,11), (11,11)), # Clitic
                                                (9, (13,17), (14,14)), # NP
                                                (9, (16,17), (17,17)), # NP
                                                
                                                (10, (1,1), (1,1)), # Clitic
                                                (10, (3,14), (4,4)), # NP
                                                (10, (6,6), (6,6)), # NP
                                                (10, (9,14), (9,9)), # NP
                                                (10, (11,14), (11,11)), # NP
                                                (10, (13,14), (14,14)), # NP
                                                (10, (20,21), (21,21)), # NP
                                                (10, (22,22), (22,22)), # Clitic
                                                (10, (24,28), (25,25)), # NP
                                                (10, (27,28), (27,27)), # NP
                                                
                                                (11, (2,9), (3,3)), # NP
                                                (11, (2,2), (2,2)), # Expand poss pron
                                                (11, (5,9), (6,6)), # NP
                                                (11, (8,9), (9,9)), # NP
                                                (11, (11,11), (11,11)), # Clitic
                                                (11, (13,15), (15,15)), # NP
                                                
                                                (12, (1,1), (1,1)), # Clitic
                                                (12, (5,21), (6,6)), # NP
                                                (12, (8,11), (8,8)), # NP
                                                (12, (12,21), (13,13)), # NP
                                                (12, (15,15), (15,15)), # NP
                                                (12, (18,21), (19,19)), # NP
                                                (12, (21,21), (21,21)), # NP
                                                
                                                (13, (1,1), (1,1)), # Clitic
                                                (13, (4, 7), (4, 7)), # Named entity
                                                (13, (4,22), (5,5)), # NP
                                                (13, (7,7), (7,7)), # NP
                                                (13, (9,22), (9,9)), # NP
                                                (13, (10,10), (10,10)), # NP
                                                (13, (11,11), (11,11)), # Clitic
                                                (13, (13,22), (14,14)), # NP
                                                (13, (16,22), (17,17)), # NP
                                                (13, (19,22), (20,20)), # NP
                                                (13, (22,22), (22,22)), # NP
                                                
                                                (14, (1,1), (1,1)), # Clitic
                                                (14, (6,6), (6,6)), # NP
                                                (14, (9,14), (10,10)), # NP
                                                (14, (12,14), (12,12)), # NP
                                                (14, (14,14), (14,14)), # NP
                                                ]
    
    
    args = (test_document,)
    kwargs = {"detect_verbs": True}
    
    return (args, kwargs), (offset, expected_mention_token_based_definitions)


def get_test_document():
    conll2012_formatted_test_document = """007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    1    vikidia    NC    (ROOT(SENT(NP*)))    vikidia    *    *    *    -    -

007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    1    007ef79e    DET    (ROOT(NP(MWN*    007ef79e    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    2    -    PUNC    *    -    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    3    40cc    DET    (MWN*    40cc    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    4    -    PUNC    *    -    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    5    431e    DET    (MWN*    431e    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    6    -    PUNC    *    -    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    7    a12a    DET    *    a12a    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    8    -    PUNC    *    -    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    9    b7c50c9ebae8    DET    *)))))    b7c50c9ebae8    *    *    *    -    -

007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    1    ADDED    NPP    (ROOT(SENT(NP*)))    ADDED    *    *    *    -    -

007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    1    2016    DET    (ROOT(SENT(NP(MWN*    2016    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    2    -    PUNC    *    -    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    3    06    DET    *    06    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    4    -    PUNC    *    -    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    5    15T14    DET    *))    15T14    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    6    :    PUNC    *    :    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    7    32    DET    *    32    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    8    :    PUNC    *    :    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    9    39Z    DET    (NP*)))    39Z    *    *    *    -    -

007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    1    Édouard    NPP    (ROOT(SENT(NP*    Édouard    *    *    *    (Person    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    2    Piette    NPP    *)))    Piette    *    *    *    Person)    -

007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    1    Édouard    NPP    (ROOT(SENT(NP*    Édouard    *    *    *    (Person    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    2    Piette    NPP    *    Piette    *    *    *    Person)    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    3    ,    PUNC    *    ,    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    4    né    VPP    (VPpart*    naître    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    5    le    DET    (NP*    le    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    6    11    ADJ    *    11    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    7    mars    NC    *    mars    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    8    1827    N    *)    1827    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    9    à    P    (PP*    à    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    10    Aubigny    N    (NP(MWN*    Aubigny    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    11    -les    N    *    -les    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    12    -    PUNC    *    -    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    13    Pothées    N    *)))    Pothées    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    14    et    CC    (COORD*    et    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    15    mort    VPP    *    mourir    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    16    le    DET    (NP*    le    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    17    5    ADJ    *    5    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    18    juin    NC    *    juin    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    19    1906    N    *)    1906    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    20    à    P    (PP*    à    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    21    Rumigny    NPP    (NP*)))))    Rumigny    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    22    ,    PUNC    *    ,    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    23    était    V    (VN*)    être    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    24    un    DET    (NP*    un    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    25    archéologue    NC    *    archéologue    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    26    et    CC    (COORD*    et    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    27    préhistorien    NC    (NP*))    préhistorien    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    28    français    ADJ    (AP*))    français    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    29    .    PUNC    *))    .    *    *    *    -    -

007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    1    Préhistoire    ADJ    (ROOT(AP*))    préhistoire    *    *    *    -    -

007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    1    Biographie    NPP    (ROOT(SENT(NP*)))    biographie    *    *    *    -    -

007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    1    Il    CLS    (ROOT(SENT(VN*    il    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    2    voulait    V    *)    vouloir    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    3    d'abord    V    (VPinf*    d'abord    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    4    être    VINF    (VN*)    être    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    5    avocat    NC    (NP*))    avocat    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    6    ,    PUNC    *    ,    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    7    mais    CC    (COORD*    mais    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    8    vers    P    (Ssub*    vers    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    9    1848    DET    *    1848    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    10    il    CLS    (Sint(VN*    il    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    11    se    CLR    *    se    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    12    découvre    V    *)    découvrir    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    13    un    DET    (NP*    un    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    14    intérêt    NC    *    intérêt    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    15    pour    P    (PP*    pour    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    16    la    DET    (NP*    le    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    17    géologie    NC    *))))))    géologie    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    18    .    PUNC    *))    .    *    *    *    -    -

007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    1    Il    CLS    (ROOT(SENT(VN*    il    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    2    étudie    V    *)    étudier    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    3    la    DET    (NP*    le    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    4    formation    NC    *    formation    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    5    du    P    (PP*    devoir    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    6    calcaire    NC    (NP*)    calcaire    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    7    et    CC    (COORD*    et    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    8    des    P    (PP*    un    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    9    fossiles    NC    (NP*    fossile    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    10    du    P    (PP*    du    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    11    nord-est    NC    (NP*    nord-est    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    12    de    P    (PP*    de    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    13    la    DET    (NP*    le    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    14    France    NPP    *)))))))))    France    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    15    ,    PUNC    *    ,    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    16    à    P    (PP(MWP*    à    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    17    la    DET    *    le    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    18    suite    N    *    suite    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    19    de    P    *)    de    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    20    ces    DET    (NP*    ce    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    21    recherches    NC    *))    recherche    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    22    il    CLS    (Sint(VN*    il    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    23    fera    V    *)    faire    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    24    la    DET    (NP*    le    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    25    connaissance    NC    *    connaissance    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    26    d'    P    (PP*    de    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    27    Édouard    NPP    (NP*    Édouard    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    28    Lartet    NPP    *))))    Lartet    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    29    .    PUNC    *))    .    *    *    *    -    -

007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    1    Durant    P    (ROOT(SENT(PP*    durant    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    2    ses    DET    (NP*    son    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    3    fouilles    NC    *    fouille    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    4    dans    P    (PP*    dans    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    5    le    DET    (NP*    le    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    6    sud-ouest    NC    *    sud-ouest    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    7    de    P    (PP*    de    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    8    la    DET    (NP*    le    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    9    France    NPP    *))))))    France    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    10    ,    PUNC    *    ,    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    11    il    CLS    (VN*    il    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    12    découvre    V    *)    découvrir    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    13    de    DET    (NP*    un    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    14    nombreux    ADJ    *    nombreux    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    15    objets    NC    *)    objet    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    16    .    PUNC    *))    .    *    *    *    -    -

007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    1    Il    CLS    (ROOT(SENT(VN*    il    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    2    a    V    *    avoir    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    3    découvert    VPP    *)    découvrir    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    4    dans    P    (PP*    dans    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    5    la    DET    (NP*    le    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    6    grotte    NC    *    grotte    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    7    du    P    (PP*    du    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    8    Mas    N    (NP(MWN*    mas    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    9    -    PUNC    *    -    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    10    d'    P    *    de    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    11    Azil    NPP    *)))    Azil    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    12    une    DET    (NP*    un    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    13    statuette    NC    *    statuette    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    14    de    P    (PP*    de    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    15    femme    NC    (NP*))    femme    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    16    sculptée    VPP    (VPpart*    sculpter    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    17    dans    P    (PP*    dans    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    18    une    DET    (NP*    un    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    19    dent    NC    *    dent    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    20    de    P    (PP*    de    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    21    cheval    NC    (NP*))))))))    cheval    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    22    .    PUNC    *))    .    *    *    *    -    -

007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    1    Il    CLS    (ROOT(SENT(VN*    il    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    2    fouilla    V    *)    fouiller    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    3    également    ADV    *    également    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    4    la    DET    (NP*    le    *    *    *    (Place    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    5    grotte    NC    *    grotte    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    6    du    P    (PP*    du    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    7    Pape    NC    (NP*))    Pape    *    *    *    Place)    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    8    à    P    (PP*    à    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    9    Brassempouy    NPP    (NP*    Brassempouy    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    10    où    PROREL    (Srel(NP*)    où    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    11    il    CLS    (VN*    il    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    12    découvrit    V    *)    découvrir    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    13    des    DET    (NP*    un    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    14    fragments    NC    *    fragment    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    15    de    P    (PP*    de    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    16    sept    DET    (NP*    sept    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    17    statuettes    NC    *    statuette    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    18    dont    PROREL    (Srel(PP*)    dont    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    19    la    DET    (NP*    le    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    20    Dame    N    *    Dame    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    21    de    P    (PP*    de    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    22    Brassempouy    NPP    (NP*)))))))))))    Brassempouy    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    23    .    PUNC    *))    .    *    *    *    -    -

007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    1    Il    CLS    (ROOT(SENT(VN*    il    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    2    fut    V    *)    être    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    3    également    ADV    *    également    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    4    l'un    ADJ    (AP*)    l'un    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    5    des    P    (PP*    un    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    6    premiers    ADJ    (NP*))    premier    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    7    à    P    (VPinf*    à    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    8    reconnaître    VINF    (VN*)    reconnaître    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    9    l'    DET    (NP*    le    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    10    authenticité    NC    *    authenticité    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    11    des    P    (PP*    un    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    12    peintures    NC    (NP*    peinture    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    13    d'    P    (PP*    de    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    14    Altamira    NPP    (NP*))))))    Altamira    *    *    *    -    -
007ef79e-40cc-431e-a12a-b7c50c9ebae8.ann.extended.conllu    0    15    .    PUNC    *))    .    *    *    *    -    -
"""
    
    conllu_formatted_test_document = """1    vikidia    vikidia    PROPN    _    _    0    root    _    _

1    007ef79e    007ef79e    PROPN    _    _    0    root    _    _
2    -    -    PUNCT    _    _    1    flat:name    _    _
3    40cc    40cc    PROPN    _    _    1    flat:name    _    _
4    -    -    PUNCT    _    _    1    flat:name    _    _
5    431e    431e    PROPN    _    _    1    flat:name    _    _
6    -    -    PUNCT    _    _    1    flat:name    _    _
7    a12a    a12a    PROPN    _    _    1    flat:name    _    _
8    -    -    PUNCT    _    _    1    punct    _    _
9    b7c50c9ebae8    b7c50c9ebae8    PROPN    _    _    1    appos    _    _

1    ADDED    ADDED    PROPN    _    _    0    root    _    _

1    2016    2016    NUM    _    _    3    nummod    _    _
2    -    -    PUNCT    _    _    3    punct    _    _
3    06    06    PROPN    _    _    0    root    _    _
4    -    -    PUNCT    _    _    5    punct    _    _
5    15T14    15T14    PROPN    _    _    3    appos    _    _
6    :    :    PUNCT    _    _    3    punct    _    _
7    32    32    NUM    _    _    3    appos    _    _
8    :    :    PUNCT    _    _    7    punct    _    _
9    39Z    39Z    PROPN    _    _    3    appos    _    _

1    Édouard    Édouard    PROPN    _    _    0    root    B:coref1    B:T10001:Person
2    Piette    Piette    PROPN    _    _    1    flat:name    I:coref1    I:T10001:Person

1    Édouard    Édouard    PROPN    _    _    25    nsubj    B:coref1    B:T10002:Person
2    Piette    Piette    PROPN    _    _    1    flat:name    I:coref1    I:T10002:Person
3    ,    ,    PUNCT    _    _    4    punct    _    _
4    né    naître    VERB    _    Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part    1    acl    _    _
5    le    le    DET    _    Definite=Def|Gender=Masc|Number=Sing|PronType=Art    7    det    _    _
6    11    11    NUM    _    _    7    nummod    _    _
7    mars    mars    NOUN    _    Gender=Masc|Number=Sing    4    obl    _    _
8    1827    1827    NUM    _    _    7    nmod    _    _
9    à    à    ADP    _    _    10    case    _    _
10    Aubigny    Aubigny    PROPN    _    _    4    obl    _    _
11    -les    -les    PROPN    _    _    10    flat:name    _    _
12    -    -    PUNCT    _    _    10    flat:name    _    _
13    Pothées    Pothées    PROPN    _    _    10    flat:name    _    _
14    et    et    CCONJ    _    _    15    cc    _    _
15    mort    mourir    VERB    _    Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part    4    conj    _    _
16    le    le    DET    _    Definite=Def|Gender=Masc|Number=Sing|PronType=Art    18    det    _    _
17    5    5    NUM    _    _    18    nummod    _    _
18    juin    juin    NOUN    _    Gender=Masc|Number=Sing    15    obl    _    _
19    1906    1906    NUM    _    _    18    nmod    _    _
20    à    à    ADP    _    _    21    case    _    _
21    Rumigny    Rumigny    PROPN    _    _    15    obl    _    _
22    ,    ,    PUNCT    _    _    25    punct    _    _
23    était    être    AUX    _    Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin    25    cop    _    _
24    un    un    DET    _    Definite=Ind|Gender=Masc|Number=Sing|PronType=Art    25    det    _    _
25    archéologue    archéologue    NOUN    _    Gender=Masc|Number=Sing    0    root    _    _
26    et    et    CCONJ    _    _    27    cc    _    _
27    préhistorien    préhistorien    NOUN    _    Gender=Masc|Number=Sing    25    conj    _    _
28    français    français    ADJ    _    Gender=Masc|Number=Sing    25    amod    _    _
29    .    .    PUNCT    _    _    25    punct    _    _

1    Préhistoire    préhistoire    NOUN    _    Gender=Fem|Number=Sing    0    root    _    _

1    Biographie    biographie    NOUN    _    Gender=Fem|Number=Sing    0    root    _    _

1    Il    il    PRON    _    Gender=Masc|Number=Sing|Person=3|PronType=Prs    5    nsubj    B:coref1    B:T8:Personal
2    voulait    vouloir    AUX    _    Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin    5    aux    _    _
3    d'abord    d'abord    PROPN    _    _    5    nsubj    _    _
4    être    être    AUX    _    VerbForm=Inf    5    cop    _    _
5    avocat    avocat    NOUN    _    Gender=Masc|Number=Sing    0    root    _    _
6    ,    ,    PUNCT    _    _    5    punct    _    _
7    mais    mais    CCONJ    _    _    9    cc    _    _
8    vers    vers    ADP    _    _    9    case    _    _
9    1848    1848    NUM    _    _    5    conj    _    _
10    il    il    PRON    _    Gender=Masc|Number=Sing|Person=3|PronType=Prs    12    nsubj    B:coref1    B:T9:Personal
11    se    se    PRON    _    Person=3|PronType=Prs    12    expl    _    _
12    découvre    découvrir    VERB    _    Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin    5    acl:relcl    _    _
13    un    un    DET    _    Definite=Ind|Gender=Masc|Number=Sing|PronType=Art    14    det    _    _
14    intérêt    intérêt    NOUN    _    Gender=Masc|Number=Sing    12    obj    _    _
15    pour    pour    ADP    _    _    17    case    _    _
16    la    le    DET    _    Definite=Def|Gender=Fem|Number=Sing|PronType=Art    17    det    _    _
17    géologie    géologie    NOUN    _    Gender=Fem|Number=Sing    12    obl    _    _
18    .    .    PUNCT    _    _    5    punct    _    _

1    Il    il    PRON    _    Gender=Masc|Number=Sing|Person=3|PronType=Prs    2    nsubj    B:coref1    B:T10:Personal
2    étudie    étudier    VERB    _    Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin    0    root    _    _
3    la    le    DET    _    Definite=Def|Gender=Fem|Number=Sing|PronType=Art    4    det    _    _
4    formation    formation    NOUN    _    Gender=Fem|Number=Sing    6    nsubj    _    _
5    du    devoir    AUX    _    Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin    6    cop    _    _
6    calcaire    calcaire    ADJ    _    Gender=Fem|Number=Sing    2    ccomp    _    _
7    et    et    CCONJ    _    _    9    cc    _    _
8    des    un    DET    _    Definite=Ind|Gender=Masc|Number=Plur|PronType=Art    9    det    _    _
9    fossiles    fossile    NOUN    _    Gender=Masc|Number=Plur    6    conj    _    _
10    du    du    DET    _    Gender=Masc|Number=Sing    11    det    _    _
11    nord-est    nord-est    NOUN    _    Gender=Masc|Number=Sing    23    nsubj    _    _
12    de    de    ADP    _    _    14    case    _    _
13    la    le    DET    _    Definite=Def|Gender=Fem|Number=Sing|PronType=Art    14    det    _    _
14    France    France    PROPN    _    _    23    obl    _    _
15    ,    ,    PUNCT    _    _    23    punct    _    _
16    à    à    ADP    _    _    18    case    _    _
17    la    le    DET    _    Definite=Def|Gender=Fem|Number=Sing|PronType=Art    18    det    _    _
18    suite    suite    NOUN    _    Gender=Fem|Number=Sing    23    obl    _    _
19    de    de    ADP    _    _    21    case    _    _
20    ces    ce    DET    _    Gender=Fem|Number=Plur|PronType=Dem    21    det    _    _
21    recherches    recherche    NOUN    _    Gender=Fem|Number=Plur    18    nmod    _    _
22    il    il    PRON    _    Gender=Masc|Number=Sing|Person=3|PronType=Prs    23    nsubj    B:coref1    B:T11:Personal
23    fera    faire    VERB    _    Mood=Ind|Number=Sing|Person=3|Tense=Fut|VerbForm=Fin    6    conj    _    _
24    la    le    DET    _    Definite=Def|Gender=Fem|Number=Sing|PronType=Art    25    det    _    _
25    connaissance    connaissance    NOUN    _    Gender=Fem|Number=Sing    23    obj    _    _
26    d'    de    ADP    _    _    27    case    _    _
27    Édouard    Édouard    PROPN    _    _    25    nmod    _    _
28    Lartet    Lartet    PROPN    _    _    27    flat:name    _    _
29    .    .    PUNCT    _    _    2    punct    _    _

1    Durant    durant    ADP    _    _    3    case    _    _
2    ses    son    DET    _    Gender=Fem|Number=Plur|PronType=Prs|Poss=Yes    3    nmod:poss    B:coref1    B:T7:Possessive
3    fouilles    fouille    NOUN    _    Gender=Fem|Number=Plur    12    obl    _    _
4    dans    dans    ADP    _    _    6    case    _    _
5    le    le    DET    _    Definite=Def|Gender=Masc|Number=Sing|PronType=Art    6    det    _    _
6    sud-ouest    sud-ouest    NOUN    _    Gender=Masc|Number=Sing    3    nmod    _    _
7    de    de    ADP    _    _    9    case    _    _
8    la    le    DET    _    Definite=Def|Gender=Fem|Number=Sing|PronType=Art    9    det    _    _
9    France    France    PROPN    _    _    6    nmod    _    _
10    ,    ,    PUNCT    _    _    12    punct    _    _
11    il    il    PRON    _    Gender=Masc|Number=Sing|Person=3|PronType=Prs    12    nsubj    B:coref1    B:T19:Personal
12    découvre    découvrir    VERB    _    Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin    0    root    _    _
13    de    un    DET    _    Definite=Ind|Gender=Masc|Number=Plur|PronType=Art    15    det    _    _
14    nombreux    nombreux    ADJ    _    Gender=Masc|Number=Plur    15    amod    _    _
15    objets    objet    NOUN    _    Gender=Masc|Number=Plur    12    obj    _    _
16    .    .    PUNCT    _    _    12    punct    _    _

1    Il    il    PRON    _    Gender=Masc|Number=Sing|Person=3|PronType=Prs    3    nsubj    B:coref1    B:T21:Personal
2    a    avoir    AUX    _    Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin    3    aux    _    _
3    découvert    découvrir    VERB    _    Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part    0    root    _    _
4    dans    dans    ADP    _    _    6    case    _    _
5    la    le    DET    _    Definite=Def|Gender=Fem|Number=Sing|PronType=Art    6    det    _    _
6    grotte    grotte    NOUN    _    Gender=Fem|Number=Sing    3    obl    _    _
7    du    du    DET    _    Gender=Masc|Number=Sing    8    det    _    _
8    Mas    mas    NOUN    _    Gender=Masc|Number=Sing    3    obj    _    _
9    -    -    PUNCT    _    _    8    punct    _    _
10    d'    de    ADP    _    _    11    case    _    _
11    Azil    Azil    PROPN    _    _    8    nmod    _    _
12    une    un    DET    _    Definite=Ind|Gender=Fem|Number=Sing|PronType=Art    13    det    _    _
13    statuette    statuette    NOUN    _    Gender=Fem|Number=Sing    8    appos    _    _
14    de    de    ADP    _    _    15    case    _    _
15    femme    femme    NOUN    _    Gender=Fem|Number=Sing    13    nmod    _    _
16    sculptée    sculpter    VERB    _    Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part    13    acl    _    _
17    dans    dans    ADP    _    _    19    case    _    _
18    une    un    DET    _    Definite=Ind|Gender=Fem|Number=Sing|PronType=Art    19    det    _    _
19    dent    dent    NOUN    _    Gender=Fem|Number=Sing    16    obl    _    _
20    de    de    ADP    _    _    21    case    _    _
21    cheval    cheval    NOUN    _    Gender=Masc|Number=Sing    19    nmod    _    _
22    .    .    PUNCT    _    _    3    punct    _    _

1    Il    il    PRON    _    Gender=Masc|Number=Sing|Person=3|PronType=Prs    2    nsubj    B:coref1    B:T22:Personal
2    fouilla    fouiller    VERB    _    Number=Sing|Person=3|VerbForm=Fin|Mood=Ind    0    root    _    _
3    également    également    ADV    _    _    2    advmod    _    _
4    la    le    DET    _    Definite=Def|Gender=Fem|Number=Sing|PronType=Art    5    det    B:coref2    B:T3:Place
5    grotte    grotte    NOUN    _    Gender=Fem|Number=Sing    2    obj    I:coref2    I:T3:Place
6    du    du    PROPN    _    _    5    appos    I:coref2    I:T3:Place
7    Pape    Pape    PROPN    _    _    6    flat:name    I:coref2    I:T3:Place
8    à    à    ADP    _    _    9    case    _    _
9    Brassempouy    Brassempouy    PROPN    _    _    5    nmod    _    _
10    où    où    PRON    _    PronType=Rel    12    obl    B:coref2    B:T6:Locative
11    il    il    PRON    _    Gender=Masc|Number=Sing|Person=3|PronType=Prs    12    nsubj    B:coref1    B:T23:Personal
12    découvrit    découvrir    VERB    _    Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin    9    acl:relcl    _    _
13    des    un    DET    _    Definite=Ind|Gender=Masc|Number=Plur|PronType=Art    14    det    _    _
14    fragments    fragment    NOUN    _    Gender=Masc|Number=Plur    12    obj    _    _
15    de    de    ADP    _    _    17    case    _    _
16    sept    sept    NUM    _    _    17    nummod    _    _
17    statuettes    statuette    NOUN    _    Gender=Fem|Number=Plur    14    nmod    _    _
18    dont    dont    PRON    _    PronType=Rel    20    nmod    _    _
19    la    le    DET    _    Definite=Def|Gender=Fem|Number=Sing|PronType=Art    20    det    _    _
20    Dame    Dame    PROPN    _    _    14    appos    _    _
21    de    de    ADP    _    _    22    case    _    _
22    Brassempouy    Brassempouy    PROPN    _    _    20    nmod    _    _
23    .    .    PUNCT    _    _    2    punct    _    _

1    Il    il    PRON    _    Gender=Masc|Number=Sing|Person=3|PronType=Prs    4    nsubj    B:coref1    B:T24:Personal
2    fut    être    AUX    _    Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin    4    cop    _    _
3    également    également    ADV    _    _    4    advmod    _    _
4    l'un    l'un    PROPN    _    _    0    root    _    _
5    des    un    DET    _    Definite=Ind|Gender=Masc|Number=Plur|PronType=Art    6    det    _    _
6    premiers    premier    PRON    _    Gender=Masc|Number=Plur    4    appos    _    _
7    à    à    ADP    _    _    8    mark    _    _
8    reconnaître    reconnaître    VERB    _    VerbForm=Inf    6    acl    _    _
9    l'    le    DET    _    Definite=Def|Gender=Fem|Number=Sing|PronType=Art    10    det    _    _
10    authenticité    authenticité    NOUN    _    Gender=Fem|Number=Sing    8    obj    _    _
11    des    un    DET    _    Definite=Ind|Gender=Fem|Number=Plur|PronType=Art    12    det    _    _
12    peintures    peinture    NOUN    _    Gender=Fem|Number=Plur    10    appos    _    _
13    d'    de    ADP    _    _    14    case    _    _
14    Altamira    Altamira    PROPN    _    _    12    nmod    _    _
15    .    .    PUNCT    _    _    4    punct    _    _
"""
    lines = conll2012_formatted_test_document.split("\n")
    line_iterator = iter(lines)
    conll2012_test_document = CONLL2012DocumentReader.parse(line_iterator)
    offset = True
    
    lines = conllu_formatted_test_document.split("\n")
    line_iterator = iter(lines)
    conllu_test_document = CONLLUDocumentReader.parse(line_iterator)
    
    test_document = check_and_merge_conllu_document_into_conll2012_document(conll2012_test_document, 
                                                                            conllu_test_document)
    
    return test_document, offset
    



"""
actual_diff_expected:

actual_diff_actual:
(6, (1, 2), (1, 2)) => Makes sense, but do not know why the algo got it; need to check the algo, good: add it to 'expected' data => Named entity

(13, (4, 7), (4, 7)) => Makes sense, but do not know why the algo got it (tought it would be replaced by bigger mention with the same head); need to check the algo, good: add it to 'expected' data
=> Named entity, has not been already replaced because the head extent defined for a named entity candidate mention is the whole mention extent

(6, (1, 2), (1, 2))
{'head_extent': (104, 117), 'extent': (104, 117)}

(13, (4, 7), (4, 7))
{'head_extent': (727, 743), 'extent': (727, 743)}
"""



"""
print(m1)
print(m2)
result2, message2 = m1._inner_eq(m2)
if message2:
    attr1 = getattr(m1, message2)
    attr2 = getattr(m2, message2)
    print(attr1)
    print(attr2)
    print(m1.named_entity)
self.assertTrue(result2, message2)
"""


'''
print(expected_document.ident)
print(document.ident)
l1 = expected_document.mentions
l2 = document.mentions
from pprint import pprint
pprint(l1)
pprint(l2)
i = 0
m1 = l1[i]
m2 = l2[i]
print(m1)
print(m2)
attribute_name = "head_extent"
attr1 = getattr(m1, attribute_name)
attr2 = getattr(m2, attribute_name)
print(attr1)
print(attr2)
#result3, message3 = attr1._inner_eq(attr2)
#self.assertTrue(result3, message3)
result2, message2 = m1._inner_eq(m2)
self.assertTrue(result2, message2)
'''



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()