# -*- coding: utf-8 -*-

import unittest

from cortex.api.constituency_tree import ConstituencyTree
from cortex.languages.french.constituency_tree_node_head_finder import DybroJohansenHeadFinder



class TestDybroJohansenHeadFinder(unittest.TestCase):
    
    @unittest.skip
    def test__get_head_leaf(self):
        # Test parameter
        data = []
        
        tree_line = "(NP (DET les) (ADJ deux) (NC guerres) (AP (ADJ mondiales)))"
        constituency_tree = ConstituencyTree.parse(tree_line)
        data.append((constituency_tree.root, constituency_tree.leaves[2]))
        
        tree_line = "(MWADV (P à) (N 1945))"
        constituency_tree = ConstituencyTree.parse(tree_line)
        data.append((constituency_tree.root, constituency_tree.leaves[1]))
        
        collins_head_finder = DybroJohansenHeadFinder()
        
        # Test
        for test_node, expected_result in data:
            actual_result = collins_head_finder._get_head_leaf(test_node)
            self.assertIs(expected_result, actual_result)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()