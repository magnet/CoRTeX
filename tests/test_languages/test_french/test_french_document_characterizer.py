# -*- coding: utf-8 -*-

import unittest
import pytest
from collections import OrderedDict

from cortex.api.document import Document
from cortex.api.document_buffer import DocumentBuffer
from cortex.languages.french.document_characterizer import FrenchDocumentCharacterizer


class TestFrenchDocumentCharacterizer(unittest.TestCase):
    
    #@unittest.skip
    @pytest.mark.need_stanford_core_nlp
    def test_characterize_documents(self):
        # Test parameters
        import logging
        import sys
        logging.basicConfig(level=logging.DEBUG, format="%(asctime)s :: %(levelname)s :: %(message)s", handlers=[logging.StreamHandler(sys.stdout)])
        
        data = get_test_characterize_documents_data()
        english_document_characterizer = FrenchDocumentCharacterizer()
        
        # Test
        for (args, kwargs), expected_documents in data:
            actual_documents = english_document_characterizer.characterize_documents(*args, **kwargs)
            self.assertEqual(len(expected_documents), len(actual_documents))
            for expected_document, actual_document in zip(expected_documents, actual_documents):
                # Ident
                self.assertEqual(expected_document.ident, actual_document.ident)
                # Info
                self.assertEqual(expected_document.info, actual_document.info)
                # Raw text
                self.assertEqual(expected_document.raw_text, actual_document.raw_text)
                # Named Entities
                self.assertEqual(expected_document.named_entities, actual_document.named_entities)
                # Tokens
                self.assertEqual(expected_document.tokens, actual_document.tokens)
                # Mention
                self.assertEqual(expected_document.mentions, actual_document.mentions)
                # Sentence
                self.assertEqual(expected_document.sentences, actual_document.sentences)
                # Coreference partition
                self.assertEqual(expected_document.coreference_partition, actual_document.coreference_partition)



def get_test_characterize_documents_data():
    raw_text = """
Il fait beau , et les oiseaux volent .
Ceci ne concerne pas Antoine .
Jeanne s' est assurée qu' il travaille vraiment , ce qui était douteux ."""
    # Create the input document
    test_document_tokens_data = {(1, 2): {'raw_text': 'Il', 'ident': '1-1'},
                                (4, 7): {'raw_text': 'fait', 'ident': '1-2'},
                                (9, 12): {'raw_text': 'beau', 'ident': '1-3'},
                                (14, 14): {'raw_text': ',', 'ident': '1-4'},
                                (16, 17): {'raw_text': 'et', 'ident': '1-5'},
                                (19, 21): {'raw_text': 'les', 'ident': '1-6'},
                                (23, 29): {'raw_text': 'oiseaux', 'ident': '1-7'},
                                (31, 36): {'raw_text': 'volent', 'ident': '1-8'},
                                (38, 38): {'raw_text': '.', 'ident': '1-9'},
                                (40, 43): {'raw_text': 'Ceci', 'ident': '2-1'},
                                (45, 46): {'raw_text': 'ne', 'ident': '2-2'},
                                (48, 55): {'raw_text': 'concerne', 'ident': '2-3'},
                                (57, 59): {'raw_text': 'pas', 'ident': '2-4'},
                                (61, 67): {'raw_text': 'Antoine', 'ident': '2-5'},
                                (69, 69): {'raw_text': '.', 'ident': '2-6'},
                                (71, 76): {'raw_text': 'Jeanne', 'ident': '3-1'},
                                (78, 79): {'raw_text': "s'", 'ident': '3-2'},
                                (81, 83): {'raw_text': 'est', 'ident': '3-3'},
                                (85, 91): {'raw_text': 'assurée', 'ident': '3-4'},
                                (93, 95): {'ident': '3-5', 'raw_text': "qu'"},
                                (97, 98): {'ident': '3-6', 'raw_text': 'il'},
                                (100, 108): {'ident': '3-7', 'raw_text': 'travaille'},
                                (110, 117): {'ident': '3-8', 'raw_text': 'vraiment'},
                                (119, 119): {'ident': '3-9', 'raw_text': ','},
                                (121, 122): {'ident': '3-10', 'raw_text': 'ce'},
                                (124, 126): {'ident': '3-11', 'raw_text': 'qui'},
                                (128, 132): {'ident': '3-12', 'raw_text': 'était'},
                                (134, 140): {'ident': '3-13', 'raw_text': 'douteux'},
                                (142, 142): {'ident': '3-14', 'raw_text': '.'},
                                 }
    test_document_sentences_data = {(1, 38): {'raw_text': 'Il fait beau , et les oiseaux volent .', 'ident': '1'},
                                    (40, 69): {'raw_text': 'Ceci ne concerne pas Antoine .', 'ident': '2'},
                                    (71, 142): {'raw_text': "Jeanne s' est assurée qu' il travaille vraiment , ce qui était douteux .", 'ident': '3'},
                                    }
    document = Document("test_doc", raw_text)
    document_buffer = DocumentBuffer(document)
    document_buffer.add_tokens(test_document_tokens_data)
    document_buffer.add_sentences(test_document_sentences_data)
    document = document_buffer.flush()
    
    PARAMETERS_AND_EXPECTED_RESULTS = {("FrenchTagger", "FrenchFactored"): [(("CLS", "V", "ADJ", "PUNC", "CC", "DET", "NC", "V", "PUNC"), 
                                                                  "(ROOT (SENT (VN (CLS Il) (MWV (V fait) (ADJ beau))) (PUNC ,) (COORD (CC et) (Sint (NP (DET les) (NC oiseaux)) (VN (V volent)))) (PUNC .)))",
                                                                  ),
                                                                  (("PRO", "ADV", "V", "ADV", "NPP", "PUNC", ), 
                                                                  "(ROOT (SENT (NP (PRO Ceci)) (VN (ADV ne) (V concerne)) (ADV pas) (NP (NPP Antoine)) (PUNC .)))",
                                                                  ),
                                                                  (("N", "CLR", "V", "VPP", "CS", "CLS", "V", "ADV", "PUNC", "PRO", "PROREL", "V", "ADJ", "PUNC"), 
                                                                  "(ROOT (SENT (NP (MWN (N Jeanne))) (VN (CLR s') (V est) (VPP assurée)) (Ssub (CS qu') (VN (CLS il) (V travaille)) (ADV vraiment) (PUNC ,) (NP (PRO ce) (Srel (NP (PROREL qui)) (VN (V était)) (AP (ADJ douteux))))) (PUNC .)))",
                                                                  ),
                                                                  ], 
                                       
                                       ("FrenchTagger", "SR"): [(("CLS", "V", "ADJ", "PUNC", "CC", "DET", "NC", "V", "PUNC"), 
                                                                  "(ROOT (SENT (VN (CLS Il) (V fait)) (AP (ADJ beau)) (PUNC ,) (COORD (CC et) (Sint (NP (DET les) (NC oiseaux)) (VN (V volent)))) (PUNC .)))",
                                                                  ),
                                                                  (("PRO", "ADV", "V", "ADV", "NPP", "PUNC", ), 
                                                                  "(ROOT (SENT (NP (PRO Ceci)) (VN (ADV ne) (V concerne)) (ADV pas) (NP (NPP Antoine)) (PUNC .)))",
                                                                  ),
                                                                  (("N", "CLR", "V", "VPP", "CS", "CLS", "V", "ADV", "PUNC", "PRO", "PROREL", "V", "ADJ", "PUNC"), 
                                                                  "(ROOT (SENT (NP (N Jeanne)) (VN (CLR s') (V est) (VPP assurée)) (Ssub (CS qu') (VN (CLS il) (V travaille)) (ADV vraiment)) (PUNC ,) (NP (PRO ce) (Srel (NP (PROREL qui)) (VN (V était)) (AP (ADJ douteux)))) (PUNC .)))",
                                                                  ),
                                                                  ], 
                                       
                                       ("FrenchTagger", "SRBeam"): [(("CLS", "V", "ADJ", "PUNC", "CC", "DET", "NC", "V", "PUNC"), 
                                                                  "(ROOT (SENT (VN (CLS Il) (MWV (V fait) (ADJ beau))) (PUNC ,) (COORD (CC et) (Sint (NP (DET les) (NC oiseaux)) (VN (V volent)))) (PUNC .)))",
                                                                  ),
                                                                  (("PRO", "ADV", "V", "ADV", "NPP", "PUNC", ), 
                                                                  "(ROOT (SENT (NP (PRO Ceci)) (VN (ADV ne) (V concerne)) (ADV pas) (NP (NPP Antoine)) (PUNC .)))",
                                                                  ),
                                                                  (("N", "CLR", "V", "VPP", "CS", "CLS", "V", "ADV", "PUNC", "PRO", "PROREL", "V", "ADJ", "PUNC"), 
                                                                  "(ROOT (SENT (NP (N Jeanne)) (VN (CLR s') (V est) (VPP assurée)) (Ssub (CS qu') (VN (CLS il) (V travaille)) (ADV vraiment) (PUNC ,) (NP (PRO ce) (Srel (NP (PROREL qui)) (VN (V était)) (AP (ADJ douteux))))) (PUNC .)))",
                                                                  ),
                                                                  ], 
                                       
                                       ("FrenchUDTagger", "FrenchFactored"): [(("PRON", "VERB", "ADJ", "PUNCT", "CONJ", "DET", "NOUN", "VERB", "PUNCT"), 
                                                                  "(ROOT (SENT (NP (PRON Il) (VERB fait) (AP (ADJ beau))) (VN (PUNCT ,)) (PP (CONJ et) (NP (DET les) (NOUN oiseaux) (AP (VERB volent)))) (PUNCT .)))",
                                                                  ),
                                                                  (("PRON", "PART", "VERB", "ADV", "PROPN", "PUNCT"), 
                                                                  "(ROOT (SENT (NP (PRON Ceci) (PART ne)) (VN (VERB concerne)) (ADV pas) (AP (PROPN Antoine)) (PUNCT .)))",
                                                                  ),
                                                                  (("PROPN", "PRON", "AUX", "VERB", "SCONJ", "PRON", "VERB", "ADV", "PUNCT", "PRON", "PRON", "AUX", "ADJ", "PUNCT"), 
                                                                  "(ROOT (SENT (NP (PROPN Jeanne) (PRON s') (PP (AUX est) (NP (VERB assurée) (SCONJ qu')))) (VN (PRON il) (VERB travaille)) (ADV vraiment) (VPinf (PUNCT ,) (VN (PRON ce)) (NP (PRON qui) (AUX était) (AP (ADJ douteux)))) (PUNCT .)))",
                                                                  ),
                                                                  ], 
                                       
                                       ("FrenchUDTagger", "SR"): [(("PRON", "VERB", "ADJ", "PUNCT", "CONJ", "DET", "NOUN", "VERB", "PUNCT"), 
                                                                  "(ROOT (SENT (NP (PRON Il) (VERB fait) (AP (ADJ beau)) (NP (PUNCT ,) (MWADV (CONJ et) (DET les) (NOUN oiseaux) (VERB volent)))) (PUNCT .)))",
                                                                  ),
                                                                  (("PRON", "PART", "VERB", "ADV", "PROPN", "PUNCT"), 
                                                                  "(ROOT (SENT (PRON Ceci) (PART ne) (VERB concerne) (ADV pas) (PROPN Antoine) (PUNCT .)))",
                                                                  ),
                                                                  (("PROPN", "PRON", "AUX", "VERB", "SCONJ", "PRON", "VERB", "ADV", "PUNCT", "PRON", "PRON", "AUX", "ADJ", "PUNCT"), 
                                                                  "(ROOT (SENT (PROPN Jeanne) (PRON s') (AUX est) (VERB assurée) (SCONJ qu') (PRON il) (VERB travaille) (ADV vraiment) (PUNCT ,) (MWPRO (PRON ce) (AP (MWA (PRON qui) (MWI (AUX était) (AP (ADJ douteux)))))) (PUNCT .)))",
                                                                  ),
                                                                  ], 
                                       
                                       ("FrenchUDTagger", "SRBeam"): [(("PRON", "VERB", "ADJ", "PUNCT", "CONJ", "DET", "NOUN", "VERB", "PUNCT"), 
                                                                  "(ROOT (SENT (VN (PRON Il) (MWV (VERB fait) (ADJ beau))) (PUNCT ,) (CONJ et) (NP (DET les) (NOUN oiseaux) (VERB volent)) (PUNCT .)))",
                                                                  ),
                                                                  (("PRON", "PART", "VERB", "ADV", "PROPN", "PUNCT"), 
                                                                  "(ROOT (SENT (NP (PRON Ceci)) (VN (PART ne) (VERB concerne) (ADV pas) (PROPN Antoine)) (PUNCT .)))",
                                                                  ),
                                                                  (("PROPN", "PRON", "AUX", "VERB", "SCONJ", "PRON", "VERB", "ADV", "PUNCT", "PRON", "PRON", "AUX", "ADJ", "PUNCT"), 
                                                                  "(ROOT (SENT (PROPN Jeanne) (PRON s') (AUX est) (VERB assurée) (NP (SCONJ qu')) (PRON il) (VERB travaille) (ADV vraiment) (PUNCT ,) (PRON ce) (SENT (NP (PRON qui) (NP (AUX était) (AP (ADJ douteux))))) (PUNCT .)))",
                                                                  ),
                                                                  ], 
                                       }
    
    
    
    
    
    data = []
    
    pos_model_parse_model_pairs = [("FrenchTagger", "FrenchFactored"),
                                   ("FrenchTagger", "SR"),
                                   ("FrenchTagger", "SRBeam"),
                                   #("FrenchUDTagger", "FrenchFactored"),
                                   #("FrenchUDTagger", "SR"),
                                   #("FrenchUDTagger", "SRBeam"),
                                   ]
    
    # Create the output document
    for pos_model, parse_model in pos_model_parse_model_pairs:
        
        EXPECTED_RESULTS = PARAMETERS_AND_EXPECTED_RESULTS[(pos_model, parse_model)]
        
        named_entities_data = {}
        tokens_data = {}
        sentences_data = {}
        
        ## First sentence
        pos_tags, constituency_tree_string = EXPECTED_RESULTS[0]
        sentences_data[(1, 38)] = {'raw_text': 'Il fait beau , et les oiseaux volent .', 'ident': '1',
                                   "constituency_tree_string": constituency_tree_string, 
                                   "predicate_arguments": None, "speaker": None}
        
        tokens_data[(1, 2)] = {'raw_text': 'Il', 'ident': '1-1', "POS_tag": pos_tags[0]}
        tokens_data[(4, 7)] = {'raw_text': 'fait', 'ident': '1-2', "POS_tag": pos_tags[1]}
        tokens_data[(9, 12)] = {'raw_text': 'beau', 'ident': '1-3', "POS_tag": pos_tags[2]}
        tokens_data[(14, 14)] = {'raw_text': ',', 'ident': '1-4', "POS_tag": pos_tags[3]}
        tokens_data[(16, 17)] = {'raw_text': 'et', 'ident': '1-5', "POS_tag": pos_tags[4]}
        tokens_data[(19, 21)] = {'raw_text': 'les', 'ident': '1-6', "POS_tag": pos_tags[5]}
        tokens_data[(23, 29)] = {'raw_text': 'oiseaux', 'ident': '1-7', "POS_tag": pos_tags[6]}
        tokens_data[(31, 36)] = {'raw_text': 'volent', 'ident': '1-8', "POS_tag": pos_tags[7]}
        tokens_data[(38, 38)] = {'raw_text': '.', 'ident': '1-9', "POS_tag": pos_tags[8]}
        
        ## Second sentence
        pos_tags, constituency_tree_string = EXPECTED_RESULTS[1]
        sentences_data[(40, 69)] = {"ident": "2", 
                                   "raw_text": "Ceci ne concerne pas Antoine .", 
                                   "constituency_tree_string": constituency_tree_string, 
                                   "predicate_arguments": None, "speaker": None}
        
        tokens_data[(40, 43)] = {'raw_text': 'Ceci', 'ident': '2-1', "POS_tag": pos_tags[0]}
        tokens_data[(45, 46)] = {'raw_text': 'ne', 'ident': '2-2', "POS_tag": pos_tags[1]}
        tokens_data[(48, 55)] = {'raw_text': 'concerne', 'ident': '2-3', "POS_tag": pos_tags[2]}
        tokens_data[(57, 59)] = {'raw_text': 'pas', 'ident': '2-4', "POS_tag": pos_tags[3]}
        tokens_data[(61, 67)] = {'raw_text': 'Antoine', 'ident': '2-5', "POS_tag": pos_tags[4]}
        tokens_data[(69, 69)] = {'raw_text': '.', 'ident': '2-6', "POS_tag": pos_tags[5]}
        
        ## Third sentence
        pos_tags, constituency_tree_string = EXPECTED_RESULTS[2]
        sentences_data[(71, 142)] = {"ident": "3", 
                                   "raw_text": "Jeanne s' est assurée qu' il travaille vraiment , ce qui était douteux .", 
                                   "constituency_tree_string": constituency_tree_string, 
                                   "predicate_arguments": None, "speaker": None}
        
        tokens_data[(71, 76)] = {'raw_text': 'Jeanne', 'ident': '3-1', "POS_tag": pos_tags[0]}
        tokens_data[(78, 79)] = {'raw_text': "s'", 'ident': '3-2', "POS_tag": pos_tags[1]}
        tokens_data[(81, 83)] = {'raw_text': 'est', 'ident': '3-3', "POS_tag": pos_tags[2]}
        tokens_data[(85, 91)] = {'raw_text': 'assurée', 'ident': '3-4', "POS_tag": pos_tags[3]}
        tokens_data[(93, 95)] = {'ident': '3-5', 'raw_text': "qu'", "POS_tag": pos_tags[4]}
        tokens_data[(97, 98)] = {'ident': '3-6', 'raw_text': 'il', "POS_tag": pos_tags[5]}
        tokens_data[(100, 108)] = {'ident': '3-7', 'raw_text': 'travaille', "POS_tag": pos_tags[6]}
        tokens_data[(110, 117)] = {'ident': '3-8', 'raw_text': 'vraiment', "POS_tag": pos_tags[7]}
        tokens_data[(119, 119)] = {'ident': '3-9', 'raw_text': ',', "POS_tag": pos_tags[8]}
        tokens_data[(121, 122)] = {'ident': '3-10', 'raw_text': 'ce', "POS_tag": pos_tags[9]}
        tokens_data[(124, 126)] = {'ident': '3-11', 'raw_text': 'qui', "POS_tag": pos_tags[10]}
        tokens_data[(128, 132)] = {'ident': '3-12', 'raw_text': 'était', "POS_tag": pos_tags[11]}
        tokens_data[(134, 140)] = {'ident': '3-13', 'raw_text': 'douteux', "POS_tag": pos_tags[12]}
        tokens_data[(142, 142)] = {'ident': '3-14', 'raw_text': '.', "POS_tag": pos_tags[13]}
        
        expected_document = Document("characterized_test_doc", raw_text, info=OrderedDict({"processes": " , French document characterization"}))
        document_buffer.initialize_anew(expected_document)
        document_buffer.add_tokens(tokens_data)
        document_buffer.add_named_entities(named_entities_data)
        document_buffer.add_sentences(sentences_data)
        expected_document = document_buffer.flush()
        
        args = ((document,),)
        kwargs = {"pos_model": pos_model,
                  "parse_model": parse_model,
                  "use_existing_tokenization": True, 
                  "use_existing_sentence_split": True,
                  }
        expected_documents = (expected_document,)
        
        data.append(((args, kwargs), expected_documents))
    
    return data



"""
from pprint import pprint
            l1 = expected_document.sentences
            l2 = actual_document.sentences
            pprint(l1)
            pprint(l2)
            i = 1
            m1 = l1[i]
            m2 = l2[i]
            #attribute_name = "constituency_tree"
            #attr1 = getattr(m1, attribute_name).string_representation
            #attr2 = getattr(m2, attribute_name).string_representation
            #print(attr1)
            #print(attr2)
            #result3, message3 = attr1._inner_eq(attr2)
            #self.assertTrue(result3, message3)
            result2, message2 = m1._inner_eq(m2)
            self.assertTrue(result2, message2)
"""
    


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()