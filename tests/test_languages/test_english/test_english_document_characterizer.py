# -*- coding: utf-8 -*-

import unittest
import pytest
from collections import OrderedDict

from cortex.api.document import Document
from cortex.api.document_buffer import DocumentBuffer
from cortex.api.markable import NamedEntity, Token, Mention, Sentence
from cortex.api.entity import Entity
from cortex.api.coreference_partition import CoreferencePartition
from cortex.api.constituency_tree import ConstituencyTree
from cortex.languages.english.document_characterizer import EnglishDocumentCharacterizer
from cortex.parameters.mention_data_tags import (UNKNOWN_VALUE_TAG, 
                                                  EXPANDED_PRONOUN_GRAM_TYPE_TAG, NAME_GRAM_TYPE_TAG, 
                                                  NOMINAL_GRAM_TYPE_TAG, VERB_GRAM_TYPE_TAG, 
                                                  EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS, 
                                                  NAME_GRAM_TYPE_SUBTYPE_TAGS, 
                                                  NOMINAL_GRAM_TYPE_SUBTYPE_TAGS, 
                                                  SINGULAR_NUMBER_TAG, PLURAL_NUMBER_TAG, 
                                                  MALE_GENDER_TAG, FEMALE_GENDER_TAG, 
                                                  NEUTRAL_GENDER_TAG, FIRST_PERSON_TAG, 
                                                  SECOND_PERSON_TAG, THIRD_PERSON_TAG)

from cortex.languages.english.parameters.ontonotes_v5_named_entities_data_tags import (PERSON_NAMED_ENTITY_TYPE_TAGS, 
                                                                                         ORGANIZATION_NAMED_ENTITY_TYPE_TAGS, 
                                                                                         LOCATION_NAMED_ENTITY_TYPE_TAGS,
                                                                                         ARTIFACT_NAMED_ENTITY_TYPE_TAGS)

PERSON_NAMED_ENTITY_TYPE_TAG = PERSON_NAMED_ENTITY_TYPE_TAGS[0]
ORGANIZATION_NAMED_ENTITY_TYPE_TAG = ORGANIZATION_NAMED_ENTITY_TYPE_TAGS[0] 
LOCATION_NAMED_ENTITY_TYPE_TAG = LOCATION_NAMED_ENTITY_TYPE_TAGS[0]
ARTIFACT_NAMED_ENTITY_TYPE_TAG = ARTIFACT_NAMED_ENTITY_TYPE_TAGS[0]

#@unittest.skip
class TestEnglishDocumentCharacterizer(unittest.TestCase):
    
    #@unittest.skip
    def test__compute_sentence_sep(self):
        # Test parameters
        data = []
        raw_text1 = "\nIn June of 2004 , the Shanxi Yongji Peasants Association was formally "\
                "established ; it now has 3,800 members . Accordingly , this is the first peasant "\
                "organization to be legally registered and designated a peasant association since "\
                "the founding of the PRC ."
        raw_text2 = "In June of 2004 , the Shanxi Yongji Peasants Association was formally "\
                    "established ; it now has 3,800 members . Accordingly , this is the first peasant "\
                    "organization to be legally registered and designated a peasant association since "\
                    "the founding of the PRC ."
        raw_text4 = "In June of 2004 , the Shanxi Yongji Peasants Association was formally "\
                    "established ; it now has 3,800 members .\nAccordingly , this is the first peasant "\
                    "organization to be legally registered and designated a peasant association since "\
                    "the founding of the PRC ."
        
        for i, (raw_text, expected_sentence_sep) in enumerate(((raw_text1, " "), (raw_text2, " "), (raw_text4, "\n"))):
            ident = "test_n°{}".format(i)
            document = Document(ident, raw_text)
            data.append((document, expected_sentence_sep))
        
        # Tests
        english_document_characterizer = EnglishDocumentCharacterizer()
        for document, expected_result in data:
            actual_result = english_document_characterizer._compute_sentence_sep(document)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__reassemble_mentions_and_entities_data(self):
        # Test parameters
        (original_document, characterized_document_buffer), expected_document = get_test__reassemble_mentions_and_entities_data()
        english_document_characterizer = EnglishDocumentCharacterizer()
        
        # Test
        actual_document = english_document_characterizer._reassemble_mentions_and_entities_data(original_document, characterized_document_buffer)
        self.assertTrue(*expected_document._inner_eq(actual_document))
    
    #@unittest.skip
    @pytest.mark.need_stanford_core_nlp
    def test_characterize_documents(self):
        # Test parameters
        data = get_data()
        test_documents, expected_documents = tuple(zip(*data))
        english_document_characterizer = EnglishDocumentCharacterizer()
        
        # Test
        actual_documents = english_document_characterizer.characterize_documents(test_documents)
        self.assertEqual(len(expected_documents), len(actual_documents))
        for expected_document, actual_document in zip(expected_documents, actual_documents):
            # Ident
            self.assertEqual(expected_document.ident, actual_document.ident)
            # Info
            self.assertEqual(expected_document.info, actual_document.info)
            # Raw text
            self.assertEqual(expected_document.raw_text, actual_document.raw_text)
            # Named Entities
            self.assertEqual(expected_document.named_entities, actual_document.named_entities)
            # Tokens
            self.assertEqual(expected_document.tokens, actual_document.tokens)
            # Mention
            self.assertEqual(expected_document.mentions, actual_document.mentions)
            # Sentence
            self.assertEqual(expected_document.sentences, actual_document.sentences)
            # Coreference partition
            self.assertEqual(expected_document.coreference_partition, actual_document.coreference_partition)
    
    #@unittest.skip
    @pytest.mark.need_stanford_core_nlp
    def test_characterize_document_with_pre_existing_tokenization(self):
        # Test parameters
        english_document_characterizer = EnglishDocumentCharacterizer()
        def _fct(document):
            raw_text = document.raw_text
            c = 1
            collec = []
            for i, s in enumerate(raw_text[1:].split(" ")):
                extent = (c,c+len(s)-1)
                ident = str(i)
                t = Token(ident, extent, document)
                collec.append(t)
                c += len(s) + 1
            return collec
        original_raw_text = "\nIf pictures are taken without permission , %pw that is to say , it will at all times be pursued by legal action ."
        test_document = Document(ident="1", raw_text=original_raw_text)
        test_document.tokens = _fct(test_document)
        
        # Test
        ## If no tokenization enforced
        actual_document1 = english_document_characterizer.characterize_document(test_document, 
                                                                                use_existing_tokenization=False, 
                                                                                strict=False)
        expected_raw_text1 = "\nIf pictures are taken without permission , % pw that is to say , it will at all times be pursued by legal action ."
        self.assertEqual(expected_raw_text1, actual_document1.raw_text)
        self.assertEqual(25, len(actual_document1.tokens))
        
        ## If own tokenization enforced
        actual_document1 = english_document_characterizer.characterize_document(test_document, 
                                                                                use_existing_tokenization=True, 
                                                                                strict=False)
        expected_raw_text1 = "\nIf pictures are taken without permission , %pw that is to say , it will at all times be pursued by legal action ."
        self.assertEqual(expected_raw_text1, actual_document1.raw_text)
        self.assertEqual(24, len(actual_document1.tokens))
    
    #@unittest.skip
    @pytest.mark.need_stanford_core_nlp
    def test_characterize_document_with_pre_existing_sentence_tokenization(self):
        # Test parameters
        english_document_characterizer = EnglishDocumentCharacterizer()
        (document, kwargs), expected_sentences_tokens = get_test_characterize_document_with_pre_existing_sentence_tokenization_data()
        
        # Test
        characterized_document = english_document_characterizer.characterize_documents((document,), **kwargs)[0]
        actual_sentences_tokens = tuple(tuple(token.raw_text for token in sentence.tokens) for sentence in characterized_document.sentences)
        self.maxDiff = None
        self.assertEqual(len(expected_sentences_tokens), len(actual_sentences_tokens))
        for expected_sentence_tokens, actual_sentence_tokens in zip(expected_sentences_tokens, actual_sentences_tokens):
            self.assertEqual(expected_sentence_tokens, actual_sentence_tokens)
        




def get_test__reassemble_mentions_and_entities_data():
    raw_text1 = "\nIn June of 2004 , the Shanxi Yongji Peasants Association was formally "\
                "established ; it now has 3,800 members . Accordingly , this is the first peasant "\
                "organization to be legally registered and designated a peasant association since "\
                "the founding of the PRC ."
    raw_text2 = "In June of 2004 , the Shanxi Yongji Peasants Association was formally "\
                "established ; it now has 3,800 members . Accordingly , this is the first peasant "\
                "organization to be legally registered and designated a peasant association since "\
                "the founding of the PRC ."
    
    (named_entities_data, tokens_data, sentences_data), expected_sentences_data, test_mentions_data1, _, expected_mentions_data1, _ = _define_sentences_data() #test_mentions_data2, expected_mentions_data2
    
    # Build the documents
    ## Document 1: info about mentions position and their coreference entity is known
    original_document = Document("test_document1", raw_text2)
    info = OrderedDict()
    info["processes"] = " , English document characterization"
    expected_document = Document("expected_document1", raw_text1, info=info)
    _assemble_document_data(original_document, tuple(), test_mentions_data1)
    _assemble_document_data(expected_document, expected_sentences_data, expected_mentions_data1)
    
    ## Build the DocumentBuffer
    document_buffer = DocumentBuffer(Document("characterized_test_document1", raw_text1, info=info))
    document_buffer.add_named_entities(named_entities_data)
    document_buffer.add_tokens(tokens_data)
    document_buffer.add_sentences(sentences_data)
    
    return (original_document, document_buffer), expected_document




def get_test_characterize_document_with_pre_existing_tokenization_data():
    raw_text = "\nStanford University is located in California . It is a great university , founded in 1891 ."
    
    # Create the input document
    test_document_tokens_data = {(1,8): {"ident": "1-1", "raw_text": "Stanford"},
                                 (10,19): {"ident": "1-2", "raw_text": "University"},
                                 (21,22): {"ident": "1-3", "raw_text": "is"},
                                 (24,30): {"ident": "1-4", "raw_text": "located"},
                                 (32,33): {"ident": "1-5", "raw_text": "in"},
                                 (35,44): {"ident": "1-6", "raw_text": "California"},
                                 (46,46): {"ident": "1-7", "raw_text": "."},
                                 (48,49): {"ident": "2-1", "raw_text": "It"},
                                 (51,52): {"ident": "2-2", "raw_text": "is"},
                                 (54,54): {"ident": "2-3", "raw_text": "a"},
                                 (56,60): {"ident": "2-4", "raw_text": "great"},
                                 (62,71): {"ident": "2-5", "raw_text": "university"},
                                 (73,73): {"ident": "2-6", "raw_text": ","},
                                 (75,81): {"ident": "2-7", "raw_text": "founded"},
                                 (83,84): {"ident": "2-8", "raw_text": "in"},
                                 (86,89): {"ident": "2-9", "raw_text": "1891"},
                                 (91,91): {"ident": "2-10", "raw_text": "."},
                                 }
    document = Document("test_doc", raw_text)
    document_buffer = DocumentBuffer(document)
    document_buffer.add_tokens(test_document_tokens_data)
    document = document_buffer.flush()
    
    # Create the output document
    named_entities_data = {}
    tokens_data = {}
    sentences_data = {}
    
    ## First sentence
    sentences_data[(1,46)] = {"ident": "1", "extent": (1,46), 
                               "raw_text": "Stanford University is located in California .", 
                               "constituency_tree_string": "(ROOT (S (NP (NNP Stanford) (NNP University)) (VP (VBZ is) (ADJP (JJ located) (PP (IN in) (NP (NNP California))))) (. .)))", 
                               "predicate_arguments": None, "speaker": None}
    named_entities_data[(1,8)] = {"ident": "1,8", "extent": (1,8), "raw_text": "Stanford", "type": "ORGANIZATION", "subtype": None}
    named_entities_data[(10,19)] = {"ident": "10,19", "extent": (10,19), "raw_text": "University", "type": "ORGANIZATION", "subtype": None}
    named_entities_data[(35,44)] = {"ident": "35,44", "extent": (35,44), "raw_text": "California", "type": "LOCATION", "subtype": None}
    
    tokens_data[(1,8)] = {"ident": "1-1", "extent": (1,8), "raw_text": "Stanford", "lemma": "Stanford", "POS_tag": "NNP"}
    tokens_data[(10,19)] = {"ident": "1-2", "extent": (10,19), "raw_text": "University", "lemma": "University", "POS_tag": "NNP"}
    tokens_data[(21,22)] = {"ident": "1-3", "extent": (21,22), "raw_text": "is", "lemma": "be", "POS_tag": "VBZ"}
    tokens_data[(24,30)] = {"ident": "1-4", "extent": (24,30), "raw_text": "located", "lemma": "located", "POS_tag": "JJ"}
    tokens_data[(32,33)] = {"ident": "1-5", "extent": (32,33), "raw_text": "in", "lemma": "in", "POS_tag": "IN"}
    tokens_data[(35,44)] = {"ident": "1-6", "extent": (35,44), "raw_text": "California", "lemma": "California", "POS_tag": "NNP"}
    tokens_data[(46,46)] = {"ident": "1-7", "extent": (46,46), "raw_text": ".", "lemma": ".", "POS_tag": "."}
    
    ## Second sentence
    sentences_data[(48,91)] = {"ident": "2", "extent": (48,91), 
                               "raw_text": "It is a great university , founded in 1891 .", 
                               "constituency_tree_string": "(ROOT (S (NP (PRP It)) (VP (VBZ is) (NP (NP (DT a) (JJ great) (NN university)) (, ,) (VP (VBN founded) (PP (IN in) (NP (CD 1891)))))) (. .)))", 
                               "predicate_arguments": None, "speaker": None}
    named_entities_data[(86,89)] = {"ident": "86,89", "extent": (86,89), "raw_text": "1891", "type": "DATE", "subtype": "1891"}
    
    tokens_data[(48,49)] = {"ident": "2-1", "extent": (48,49), "raw_text": "It", "lemma": "it", "POS_tag": "PRP"}
    tokens_data[(51,52)] = {"ident": "2-2", "extent": (51,52), "raw_text": "is", "lemma": "be", "POS_tag": "VBZ"}
    tokens_data[(54,54)] = {"ident": "2-3", "extent": (54,54), "raw_text": "a", "lemma": "a", "POS_tag": "DT"}
    tokens_data[(56,60)] = {"ident": "2-4", "extent": (56,60), "raw_text": "great", "lemma": "great", "POS_tag": "JJ"}
    tokens_data[(62,71)] = {"ident": "2-5", "extent": (62,71), "raw_text": "university", "lemma": "university", "POS_tag": "NN"}
    tokens_data[(73,73)] = {"ident": "2-6", "extent": (73,73), "raw_text": ",", "lemma": ",", "POS_tag": ","}
    tokens_data[(75,81)] = {"ident": "2-7", "extent": (75,81), "raw_text": "founded", "lemma": "found", "POS_tag": "VBN"}
    tokens_data[(83,84)] = {"ident": "2-8", "extent": (83,84), "raw_text": "in", "lemma": "in", "POS_tag": "IN"}
    tokens_data[(86,89)] = {"ident": "2-9", "extent": (86,89), "raw_text": "1891", "lemma": "1891", "POS_tag": "CD"}
    tokens_data[(91,91)] = {"ident": "2-10", "extent": (91,91), "raw_text": ".", "lemma": ".", "POS_tag": "."}
    
    expected_document = Document("document_n0.txt", raw_text, info=OrderedDict({"original_corpus_format": "stanford_core_nlp_xml"}))
    document_buffer.initialize_anew(expected_document)
    document_buffer.add_tokens(tokens_data)
    document_buffer.add_named_entities(named_entities_data)
    document_buffer.add_sentences(sentences_data)
    expected_document = document_buffer.flush()
    
    args = (document,)
    kwargs = {"use_existing_tokenization": True}
    
    return (args, kwargs), expected_document
                

def get_test_characterize_document_with_pre_existing_sentence_tokenization_data():
    raw_text = """
WW II Landmarks on the Great Earth of China : Eternal Memories of Taihang Mountain
Standing tall on Taihang Mountain is the Monument to the Hundred Regiments Offensive ."""
    
    # Create the input document
    test_document_tokens_data = {(1, 2): {'ident': '1-1', 'raw_text': 'WW'},
                                (4, 5): {'ident': '1-2', 'raw_text': 'II'},
                                (7, 15): {'ident': '1-3', 'raw_text': 'Landmarks'},
                                (17, 18): {'ident': '1-4', 'raw_text': 'on'},
                                (20, 22): {'ident': '1-5', 'raw_text': 'the'},
                                (24, 28): {'ident': '1-6', 'raw_text': 'Great'},
                                (30, 34): {'ident': '1-7', 'raw_text': 'Earth'},
                                (36, 37): {'ident': '1-8', 'raw_text': 'of'},
                                (39, 43): {'ident': '1-9', 'raw_text': 'China'},
                                (45, 45): {'ident': '1-10', 'raw_text': ':'},
                                (47, 53): {'ident': '1-11', 'raw_text': 'Eternal'},
                                (55, 62): {'ident': '1-12', 'raw_text': 'Memories'},
                                (64, 65): {'ident': '1-13', 'raw_text': 'of'},
                                (67, 73): {'ident': '1-14', 'raw_text': 'Taihang'},
                                (75, 82): {'ident': '1-15', 'raw_text': 'Mountain'},
                                (84, 91): {'ident': '2-1', 'raw_text': 'Standing'},
                                (93, 96): {'ident': '2-2', 'raw_text': 'tall'},
                                (98, 99): {'ident': '2-3', 'raw_text': 'on'},
                                (101, 107): {'ident': '2-4', 'raw_text': 'Taihang'},
                                (109, 116): {'ident': '2-5', 'raw_text': 'Mountain'},
                                (118, 119): {'ident': '2-6', 'raw_text': 'is'},
                                (121, 123): {'ident': '2-7', 'raw_text': 'the'},
                                (125, 132): {'ident': '2-8', 'raw_text': 'Monument'},
                                (134, 135): {'ident': '2-9', 'raw_text': 'to'},
                                (137, 139): {'ident': '2-10', 'raw_text': 'the'},
                                (141, 147): {'ident': '2-11', 'raw_text': 'Hundred'},
                                (149, 157): {'ident': '2-12', 'raw_text': 'Regiments'},
                                (159, 167): {'ident': '2-13', 'raw_text': 'Offensive'},
                                (169, 169): {'ident': '2-14', 'raw_text': '.'},
                                 }
    test_document_sentences_data = {(1, 82): {'raw_text': 'WW II Landmarks on the Great Earth of China : Eternal Memories of Taihang Mountain', 'ident': '1'},
                                    (84, 169): {'raw_text': 'Standing tall on Taihang Mountain is the Monument to the Hundred Regiments Offensive .', 'ident': '2'},
                                    }
    document = Document("test_doc", raw_text)
    document_buffer = DocumentBuffer(document)
    document_buffer.add_tokens(test_document_tokens_data)
    document_buffer.add_sentences(test_document_sentences_data)
    document = document_buffer.flush()
    
    # Create expected data
    expected_sentence_tokens = []
    expected_sentence_tokens.append(("WW", "II", "Landmarks", "on", "the", "Great", "Earth", "of", "China", ":", "Eternal", "Memories", "of", "Taihang", "Mountain")
                                    )
    expected_sentence_tokens.append(("Standing", "tall", "on", "Taihang", "Mountain", "is", "the", "Monument", "to", "the", "Hundred", "Regiments", "Offensive", ".")
                                    )
    
    kwargs = {"use_existing_tokenization": True, "use_existing_sentence_split": True}
    
    return (document, kwargs), expected_sentence_tokens



def get_data():
    data = []
    
    # Document with mention and coreference data
    # Build document data
    raw_text1 = "\nIn June of 2004 , the Shanxi Yongji Peasants Association was formally "\
                "established ; it now has 3,800 members . Accordingly , this is the first peasant "\
                "organization to be legally registered and designated a peasant association since "\
                "the founding of the PRC ."
    raw_text2 = "In June of 2004 , the Shanxi Yongji Peasants Association was formally "\
                "established ; it now has 3,800 members . Accordingly , this is the first peasant "\
                "organization to be legally registered and designated a peasant association since "\
                "the founding of the PRC ."
    raw_text3 = "In June of 2004, the Shanxi Yongji Peasants Association was formally established; "\
                "it now has 3,800 members. Accordingly, this is the first peasant organization to "\
                "be legally registered and designated a peasant association since the founding of "\
                "the PRC."
    raw_text4 = "In June of 2004 , the Shanxi Yongji Peasants Association was formally "\
                "established ; it now has 3,800 members .\nAccordingly , this is the first peasant "\
                "organization to be legally registered and designated a peasant association since "\
                "the founding of the PRC ."
    raw_text5 = "\nIn June of 2004 , the Shanxi Yongji Peasants Association was formally "\
                "established ; it now has 3,800 members .\nAccordingly , this is the first peasant "\
                "organization to be legally registered and designated a peasant association since "\
                "the founding of the PRC ."
    
    
    _, expected_sentences_data, test_mentions_data1, test_mentions_data2, expected_mentions_data1, expected_mentions_data2 = _define_sentences_data() #(named_entities_data, tokens_data, sentences_data)
    
    # Build the documents
    ## Document 1: info about mentions position and their coreference entity is known
    test_document = Document("test_document1", raw_text2)
    info = OrderedDict()
    info["processes"] = " , English document characterization"
    expected_document = Document("characterized_test_document1", raw_text1, info=info)
    _assemble_document_data(test_document, tuple(), test_mentions_data1)
    _assemble_document_data(expected_document, expected_sentences_data, expected_mentions_data1)
    data.append((test_document, expected_document))
    
    ## Document 2: info about mentions position is known, but info about their coreference entity is not
    test_document = Document("test_document1", raw_text2)
    info = OrderedDict()
    info["processes"] = " , English document characterization"
    expected_document = Document("characterized_test_document1", raw_text1, info=info)
    _assemble_document_data(test_document, tuple(), test_mentions_data2)
    _assemble_document_data(expected_document, expected_sentences_data, expected_mentions_data2)
    #data.append((test_document, expected_document))
    
    ## Document 3: no info about mentions, pure raw text
    test_document = Document("test_document3", raw_text3)
    info = OrderedDict()
    info["processes"] = " , English document characterization"
    expected_document = Document("characterized_test_document3", raw_text1, info=info)
    _assemble_document_data(test_document, tuple(), tuple())
    _assemble_document_data(expected_document, expected_sentences_data, tuple())
    #data.append((test_document, expected_document))
    
    ## Document 4: info about mentions position is known, original raw_text has its sentences separated by a '\n' character
    test_document = Document("test_document1", raw_text4)
    info = OrderedDict()
    info["processes"] = " , English document characterization"
    expected_document = Document("characterized_test_document1", raw_text5, info=info)
    _assemble_document_data(test_document, tuple(), test_mentions_data2)
    _assemble_document_data(expected_document, expected_sentences_data, expected_mentions_data2)
    #data.append((test_document, expected_document))
    
    return data


def _define_sentences_data():
    test_mentions_data1 = []
    test_mentions_data2 = []
    expected_mentions_data1 = []
    expected_mentions_data2 = []
    expected_sentences_data = []
    
    named_entities_data = {}
    tokens_data = {}
    sentences_data = {}
    
    ## First sentence
    expected_sentence_data = ["1", (1,110), "In June of 2004 , the Shanxi Yongji Peasants Association was formally established ; it now has 3,800 members .", 
                             "(ROOT (S (S (PP (IN In) (NP (NP (NNP June)) (PP (IN of) (NP (CD 2004))))) (, ,) (NP (DT the) (NNP Shanxi) (NNP Yongji) (NNPS Peasants) (NNP Association)) (VP (VBD was) (VP (ADVP (RB formally)) (VBN established)))) (: ;) (S (NP (PRP it)) (ADVP (RB now)) (VP (VBZ has) (NP (CD 3,800) (NNS members)))) (. .)))",
                             [("4,15", (4,15), "June of 2004", "DATE", None),
                              ("23,56", (23,56), "Shanxi Yongji Peasants Association", "ORGANIZATION", None),
                              ("88,90", (88,90), "now", "DATE", None),
                              ("96,100", (96,100), "3,800", "NUMBER", None)
                             ]
                            ]
    sentences_data[(1,110)] = {"ident": "1", "extent": (1,110), 
                               "raw_text": "In June of 2004 , the Shanxi Yongji Peasants Association was formally established ; it now has 3,800 members .", 
                               "constituency_tree_string": "(ROOT (S (S (PP (IN In) (NP (NP (NNP June)) (PP (IN of) (NP (CD 2004))))) (, ,) (NP (DT the) (NNP Shanxi) (NNP Yongji) (NNPS Peasants) (NNP Association)) (VP (VBD was) (VP (ADVP (RB formally)) (VBN established)))) (: ;) (S (NP (PRP it)) (ADVP (RB now)) (VP (VBZ has) (NP (CD 3,800) (NNS members)))) (. .)))", 
                               "predicate_arguments": None, "speaker": None}
    named_entities_data[(4,15)] = {"ident": "4,15", "extent": (4,15), "raw_text": "June of 2004", "type": "DATE", "subtype": None}
    named_entities_data[(23,56)] = {"ident": "23,56", "extent": (23,56), "raw_text": "Shanxi Yongji Peasants Association", "type": "ORGANIZATION", "subtype": None}
    named_entities_data[(88,90)] = {"ident": "88,90", "extent": (88,90), "raw_text": "now", "type": "DATE", "subtype": None}
    named_entities_data[(96,100)] = {"ident": "96,100", "extent": (96,100), "raw_text": "3,800", "type": "NUMBER", "subtype": None}
    sentence_tokens_data = [("1-1", (1,2), "In", "in", "IN"), 
                            ("1-2", (4,7), "June", "June", "NNP"), 
                            ("1-3", (9,10), "of", "of", "IN"), 
                            ("1-4", (12,15), "2004", "2004", "CD"),
                            ("1-5", (17,17), ",", ",", ","),
                            ("1-6", (19,21), "the", "the", "DT"),
                            ("1-7", (23,28), "Shanxi", "Shanxi", "NNP"),
                            ("1-8", (30,35), "Yongji", "Yongji", "NNP"),
                            ("1-9", (37,44), "Peasants", "Peasants", "NNPS"),
                            ("1-10", (46,56), "Association", "Association", "NNP"), 
                            ("1-11", (58,60), "was", "be", "VBD"),
                            ("1-12", (62,69), "formally", "formally", "RB"),
                            ("1-13", (71,81), "established", "establish", "VBN"),
                            ("1-14", (83,83), ";", ";", ":"),
                            ("1-15", (85,86), "it", "it", "PRP"),
                            ("1-16", (88,90), "now", "now", "RB"),
                            ("1-17", (92,94), "has", "have", "VBZ"),
                            ("1-18", (96,100), "3,800", "3,800", "CD"),
                            ("1-19", (102,108), "members", "member", "NNS"),
                            ("1-20", (110,110), ".", ".", "."),
                            ]
    tokens_data[(1,2)] = {"ident": "1-1", "extent": (1,2), "raw_text": "In", "lemma": "in", "POS_tag": "IN"}
    tokens_data[(4,7)] = {"ident": "1-2", "extent": (4,7), "raw_text": "June", "lemma": "June", "POS_tag": "NNP"}
    tokens_data[(9,10)] = {"ident": "1-3", "extent": (9,10), "raw_text": "of", "lemma": "of", "POS_tag": "IN"}
    tokens_data[(12,15)] = {"ident": "1-4", "extent": (12,15), "raw_text": "2004", "lemma": "2004", "POS_tag": "CD"}
    tokens_data[(17,17)] = {"ident": "1-5", "extent": (17,17), "raw_text": ",", "lemma": ",", "POS_tag": ","}
    tokens_data[(19,21)] = {"ident": "1-6", "extent": (19,21), "raw_text": "the", "lemma": "the", "POS_tag": "DT"}
    tokens_data[(23,28)] = {"ident": "1-7", "extent": (23,28), "raw_text": "Shanxi", "lemma": "Shanxi", "POS_tag": "NNP"}
    tokens_data[(30,35)] = {"ident": "1-8", "extent": (30,35), "raw_text": "Yongji", "lemma": "Yongji", "POS_tag": "NNP"}
    tokens_data[(37,44)] = {"ident": "1-9", "extent": (37,44), "raw_text": "Peasants", "lemma": "Peasants", "POS_tag": "NNPS"}
    tokens_data[(46,56)] = {"ident": "1-10", "extent": (46,56), "raw_text": "Association", "lemma": "Association", "POS_tag": "NNP"}
    tokens_data[(58,60)] = {"ident": "1-11", "extent": (58,60), "raw_text": "was", "lemma": "be", "POS_tag": "VBD"}
    tokens_data[(62,69)] = {"ident": "1-12", "extent": (62,69), "raw_text": "formally", "lemma": "formally", "POS_tag": "RB"}
    tokens_data[(71,81)] = {"ident": "1-13", "extent": (71,81), "raw_text": "established", "lemma": "establish", "POS_tag": "VBN"}
    tokens_data[(83,83)] = {"ident": "1-14", "extent": (83,83), "raw_text": ";", "lemma": ";", "POS_tag": ":"}
    tokens_data[(85,86)] = {"ident": "1-15", "extent": (85,86), "raw_text": "it", "lemma": "it", "POS_tag": "PRP"}
    tokens_data[(88,90)] = {"ident": "1-16", "extent": (88,90), "raw_text": "now", "lemma": "now", "POS_tag": "RB"}
    tokens_data[(92,94)] = {"ident": "1-17", "extent": (92,94), "raw_text": "has", "lemma": "have", "POS_tag": "VBZ"}
    tokens_data[(96,100)] = {"ident": "1-18", "extent": (96,100), "raw_text": "3,800", "lemma": "3,800", "POS_tag": "CD"}
    tokens_data[(102,108)] = {"ident": "1-19", "extent": (102,108), "raw_text": "members", "lemma": "member", "POS_tag": "NNS"}
    tokens_data[(110,110)] = {"ident": "1-20", "extent": (110,110), "raw_text": ".", "lemma": ".", "POS_tag": "."}
    
    test_sentence_mentions_data1 = [((3,14), (3,6), 0, "June of 2004", NAME_GRAM_TYPE_TAG, NAME_GRAM_TYPE_SUBTYPE_TAGS["LONG_NAME"], SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, [], []), 
                                  ((18,55), (45,55), 1, "the Shanxi Yongji Peasants Association", NAME_GRAM_TYPE_TAG, NAME_GRAM_TYPE_SUBTYPE_TAGS["LONG_NAME"], SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, [], []), 
                                  ((84,85), (84,85), 1, "it", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, 0.991, [], [])]
    test_sentence_mentions_data2 = [((3,14), None, None, "June of 2004", None, None, None, None, None, None, None), 
                                  ((18,55), None, None, "the Shanxi Yongji Peasants Association", None, None, None, None, None, None, None), 
                                  ((84,85), None, None, "it", None, None, None, None, None, None, None)]
    expected_sentence_mentions_data1 = [((4,15), (4,7), 0, "June of 2004", NAME_GRAM_TYPE_TAG, NAME_GRAM_TYPE_SUBTYPE_TAGS["LONG_NAME"], SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, [], []), 
                                      ((19,56), (46,56), 1, "the Shanxi Yongji Peasants Association", NAME_GRAM_TYPE_TAG, NAME_GRAM_TYPE_SUBTYPE_TAGS["LONG_NAME"], SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, [], []), 
                                      ((85,86), (85,86), 1, "it", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, 0.991, [], [])]
    expected_sentence_mentions_data2 = [((4,15), None, None, "June of 2004", None, None, None, None, None, None, None), 
                                      ((19,56), None, None, "the Shanxi Yongji Peasants Association", None, None, None, None, None, None, None), 
                                      ((85,86), None, None, "it", None, None, None, None, None, None, None)]
    
    test_mentions_data1.extend(test_sentence_mentions_data1)
    test_mentions_data2.extend(test_sentence_mentions_data2)
    expected_sentence_data.append(sentence_tokens_data)
    expected_mentions_data1.extend(expected_sentence_mentions_data1)
    expected_mentions_data2.extend(expected_sentence_mentions_data2)
    expected_sentences_data.append(expected_sentence_data)
    
    ## Second sentence
    expected_sentence_data = ["2", (112,257), "Accordingly , this is the first peasant organization to be legally registered and designated a peasant association since the founding of the PRC .", 
                               "(ROOT (S (ADVP (RB Accordingly)) (, ,) (NP (DT this)) (VP (VBZ is) (NP (DT the) (JJ first) (NN peasant) (NN organization) (S (VP (TO to) (VP (VB be) (VP (ADVP (RB legally)) (VBN registered) (CC and) (VBN designated) (NP (NP (DT a) (NN peasant) (NN association)) (PP (IN since) (NP (NP (DT the) (NN founding)) (PP (IN of) (NP (DT the) (NNP PRC)))))))))))) (. .)))", 
                              [("138,142", (138,142), "first", "ORDINAL", None), 
                               ("253,255", (253,255), "PRC", "LOCATION", None),
                              ]
                            ]
    sentences_data[(112,257)] = {"ident": "2", "extent": (112,257),
                                 "raw_text": "Accordingly , this is the first peasant organization to be legally registered and designated a peasant association since the founding of the PRC .",
                                 "constituency_tree_string": "(ROOT (S (ADVP (RB Accordingly)) (, ,) (NP (DT this)) (VP (VBZ is) (NP (DT the) (JJ first) (NN peasant) (NN organization) (S (VP (TO to) (VP (VB be) (VP (ADVP (RB legally)) (VBN registered) (CC and) (VBN designated) (NP (NP (DT a) (NN peasant) (NN association)) (PP (IN since) (NP (NP (DT the) (NN founding)) (PP (IN of) (NP (DT the) (NNP PRC)))))))))))) (. .)))", 
                                 "predicate_arguments": None, "speaker": None}
    named_entities_data[(138,142)] = {"ident": "138,142", "extent": (138,142), "raw_text": "first", "type": "ORDINAL", "subtype": None}
    named_entities_data[(253,255)] = {"ident": "", "extent": (253,255), "raw_text": "PRC", "type": "LOCATION", "subtype": None}
    sentence_tokens_data = [("2-1", (112,122), "Accordingly", "accordingly", "RB"),
                            ("2-2", (124,124), ",", ",", ","),
                            ("2-3", (126,129), "this", "this", "DT"),
                            ("2-4", (131,132), "is", "be", "VBZ"),
                            ("2-5", (134,136), "the", "the", "DT"),
                            ("2-6", (138,142), "first", "first", "JJ"),
                            ("2-7", (144,150), "peasant", "peasant", "NN"),
                            ("2-8", (152,163), "organization", "organization", "NN"),
                            ("2-9", (165,166), "to", "to", "TO"),
                            ("2-10", (168,169), "be", "be", "VB"),
                            ("2-11", (171,177), "legally", "legally", "RB"),
                            ("2-12", (179,188), "registered", "register", "VBN"),
                            ("2-13", (190,192), "and", "and", "CC"),
                            ("2-14", (194,203), "designated", "designate", "VBN"),
                            ("2-15", (205,205), "a", "a", "DT"),
                            ("2-16", (207,213), "peasant", "peasant", "NN"),
                            ("2-17", (215,225), "association", "association", "NN"),
                            ("2-18", (227,231), "since", "since", "IN"),
                            ("2-19", (233,235), "the", "the", "DT"),
                            ("2-20", (237,244), "founding", "founding", "NN"),
                            ("2-21", (246,247), "of", "of", "IN"),
                            ("2-22", (249,251), "the", "the", "DT"),
                            ("2-23", (253,255), "PRC", "PRC", "NNP"),
                            ("2-24", (257,257), ".", ".", "."),
                            ]
    tokens_data[(112,122)] = {"ident": "2-1", "extent": (112,122), "raw_text": "Accordingly", "lemma": "accordingly", "POS_tag": "RB"}
    tokens_data[(124,124)] = {"ident": "2-2", "extent": (124,124), "raw_text": ",", "lemma": ",", "POS_tag": ","}
    tokens_data[(126,129)] = {"ident": "2-3", "extent": (126,129), "raw_text": "this", "lemma": "this", "POS_tag": "DT"}
    tokens_data[(131,132)] = {"ident": "2-4", "extent": (131,132), "raw_text": "is", "lemma": "be", "POS_tag": "VBZ"}
    tokens_data[(134,136)] = {"ident": "2-5", "extent": (134,136), "raw_text": "the", "lemma": "the", "POS_tag": "DT"}
    tokens_data[(138,142)] = {"ident": "2-6", "extent": (138,142), "raw_text": "first", "lemma": "first", "POS_tag": "JJ"}
    tokens_data[(144,150)] = {"ident": "2-7", "extent": (144,150), "raw_text": "peasant", "lemma": "peasant", "POS_tag": "NN"}
    tokens_data[(152,163)] = {"ident": "2-8", "extent": (152,163), "raw_text": "organization", "lemma": "organization", "POS_tag": "NN"}
    tokens_data[(165,166)] = {"ident": "2-9", "extent": (165,166), "raw_text": "to", "lemma": "to", "POS_tag": "TO"}
    tokens_data[(168,169)] = {"ident": "2-10", "extent": (168,169), "raw_text": "be", "lemma": "be", "POS_tag": "VB"}
    tokens_data[(171,177)] = {"ident": "2-11", "extent": (171,177), "raw_text": "legally", "lemma": "legally", "POS_tag": "RB"}
    tokens_data[(179,188)] = {"ident": "2-12", "extent": (179,188), "raw_text": "registered", "lemma": "register", "POS_tag": "VBN"}
    tokens_data[(190,192)] = {"ident": "2-13", "extent": (190,192), "raw_text": "and", "lemma": "and", "POS_tag": "CC"}
    tokens_data[(194,203)] = {"ident": "2-14", "extent": (194,203), "raw_text": "designated", "lemma": "designate", "POS_tag": "VBN"}
    tokens_data[(205,205)] = {"ident": "2-15", "extent": (205,205), "raw_text": "a", "lemma": "a", "POS_tag": "DT"}
    tokens_data[(207,213)] = {"ident": "2-16", "extent": (207,213), "raw_text": "peasant", "lemma": "peasant", "POS_tag": "NN"}
    tokens_data[(215,225)] = {"ident": "2-17", "extent": (215,225), "raw_text": "association", "lemma": "association", "POS_tag": "NN"}
    tokens_data[(227,231)] = {"ident": "2-18", "extent": (227,231), "raw_text": "since", "lemma": "since", "POS_tag": "IN"}
    tokens_data[(233,235)] = {"ident": "2-19", "extent": (233,235), "raw_text": "the", "lemma": "the", "POS_tag": "DT"}
    tokens_data[(237,244)] = {"ident": "2-20", "extent": (237,244), "raw_text": "founding", "lemma": "founding", "POS_tag": "NN"}
    tokens_data[(246,247)] = {"ident": "2-21", "extent": (246,247), "raw_text": "of", "lemma": "of", "POS_tag": "IN"}
    tokens_data[(249,251)] = {"ident": "2-22", "extent": (249, 251), "raw_text": "the", "lemma": "the", "POS_tag": "DT"}
    tokens_data[(253,255)] = {"ident": "2-23", "extent": (253,255), "raw_text": "PRC", "lemma": "PRC", "POS_tag": "NNP"}
    tokens_data[(257,257)] = {"ident": "2-24", "extent": (257,257), "raw_text": ".", "lemma": ".", "POS_tag": "."}
    
    test_sentence_mentions_data1 = [((125,128), (125,128), 1, "this", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["DEM_PRO"], SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, [], []), 
                                    ((248,254), (252,254), 2, "the PRC", NAME_GRAM_TYPE_TAG, NAME_GRAM_TYPE_SUBTYPE_TAGS["LONG_NAME"], SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, [], [])]
    test_sentence_mentions_data2 = [((125,128), None, None, "this", None, None, None, None,  None, None, None), 
                                    ((248,254), None, None, "the PRC", None, None, None, None, None, None, None)] # Warning: this mention has been added to the test test_document, compared to the original test_document where it does not appear.
    expected_sentence_mentions_data1 = [((126,129), (126,129), 1, "this", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["DEM_PRO"], SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, [], []), 
                                       ((249,255), (253,255), 2, "the PRC", NAME_GRAM_TYPE_TAG, NAME_GRAM_TYPE_SUBTYPE_TAGS["LONG_NAME"], SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, [], [])] # Warning: this mention has been added to the test test_document, compared to the original test_document where it does not appear.
    expected_sentence_mentions_data2 = [((126,129), None, None, "this", None, None, None, None,  None, None, None), 
                                        ((249,255), None, None, "the PRC", None, None, None, None, None, None, None)] # Warning: this mention has been added to the test test_document, compared to the original test_document where it does not appear.
    
    test_mentions_data1.extend(test_sentence_mentions_data1)
    test_mentions_data2.extend(test_sentence_mentions_data2)
    expected_sentence_data.append(sentence_tokens_data)
    expected_mentions_data1.extend(expected_sentence_mentions_data1)
    expected_mentions_data2.extend(expected_sentence_mentions_data2)
    expected_sentences_data.append(expected_sentence_data)
    
    return (named_entities_data, tokens_data, sentences_data), expected_sentences_data, test_mentions_data1, test_mentions_data2, expected_mentions_data1, expected_mentions_data2


def _assemble_document_data(document, sentences_data, mentions_data):
    if sentences_data:
        sentences = []
        full_named_entities = OrderedDict()
        for sentence_data in sentences_data:
            (ident, extent, raw_text, constituency_tree_string, named_entities_data, 
             tokens_data) = sentence_data
            sentence = Sentence(ident, extent, document, raw_text=raw_text)
            
            # Tokens
            tokens = OrderedDict()
            for token_data in tokens_data:
                ident, extent, raw_text, lemma, POS_tag = token_data
                token = Token(ident, extent,document, raw_text=raw_text)
                token.lemma = lemma
                token.POS_tag = POS_tag
                tokens[extent] = token
            tokens_list = list(tokens.values())
            
            # Named entities
            named_entities = OrderedDict()
            for named_entity_data in named_entities_data:
                ident, extent, raw_text, type_, subtype = named_entity_data
                named_entity = NamedEntity(ident, extent, document, type_, raw_text=raw_text)
                named_entity.subtype = subtype
                named_entities[extent] = named_entity
            full_named_entities.update(named_entities)
            
            # ConstituencyTree
            sentence.constituency_tree = ConstituencyTree.parse(constituency_tree_string)
            
            # Synchronization named_entities <-> tokens
            for extent, token in tokens.items():
                token.named_entity = named_entities.get(extent)
            
            # Synchronization tokens <-> constituency tree leaves
            assert len(sentence.constituency_tree.leaves) == len(tokens), "Arg."
            for i, node in enumerate(sentence.constituency_tree.leaves):
                token = tokens_list[i]
                node.token = token
                token.constituency_tree_node = node
            
            # Set sentence's data
            sentence.tokens = tokens_list
            
            # Finally
            sentences.append(sentence)
        
        ## Finally, set the test_document's data
        document.tokens = list(token for sentence in sentences for token in sentence.tokens)
        document.sentences = sentences
        document.named_entities = list(full_named_entities.values())
    
    # Mentions
    expected_entities_data = {}
    if mentions_data:
        mentions = OrderedDict()
        for mention_data in mentions_data:
            (extent, head_extent, entity_id, raw_text, gram_type, gram_subtype, number, gender, 
             referential_probability, wn_synonyms, wn_hypernyms) = mention_data
            ident = "{e[0]},{e[1]}".format(e=extent, he=head_extent)
            mention = Mention(ident, extent, document, raw_text=raw_text)
            mention.head_extent = head_extent
            mention.gram_type, mention.gram_subtype = gram_type, gram_subtype
            mention.number, mention.gender = number, gender
            mention.referential_probability = referential_probability
            mention.wn_synonyms = wn_synonyms
            mention.wn_hypernyms = wn_hypernyms
            if entity_id is not None:
                if entity_id not in expected_entities_data:
                    entity = Entity(ident=entity_id)
                    expected_entities_data[entity_id] = entity
                expected_entities_data[entity_id].add(mention.extent)
            mentions[extent] =  mention
        mentions_list = list(mentions.values())
        
        ## Finally, set the test_document's data
        document.mentions = mentions_list
    
    if sentences_data and mentions_data:
        # Synchronize mentions' data with sentences' if possible and asked for
        for sentence in document.sentences:
            ss, se = sentence.extent
            tokens = OrderedDict((t.extent, t) for t in sentence.tokens)
            mentions_list = [m for m in mentions.values() if ss <= m.start and m.end <= se]
            
            ## Synchronization tokens <-> mentions
            for mention in mentions_list:
                ms, me = mention.extent
                mention.tokens = list(t for (ts, te), t in tokens.items() if ms <= ts and te <= me)
                if mention.head_extent:
                    mhs, mhe = mention.head_extent
                    mention.head_tokens = list(t for (ts, te), t in tokens.items() if mhs <= ts and te <= mhe)
            
            ## Synchronization named_entities <-> mentions
            for mention in mentions_list:
                extent = mention.extent
                mention.named_entity = full_named_entities.get(extent)
            
            sentence.mentions = mentions_list
    
    # Create the entities only when we are sure that the mention's data will not change annymore
    if expected_entities_data:
        expected_entities = {}
        for entity_ident, a_set in expected_entities_data.items():
            if entity_ident not in expected_entities:
                entity = Entity(ident=entity_ident)
                expected_entities[entity_ident] = entity
            for extent in a_set:
                expected_entities[entity_ident].add(extent)
        document.coreference_partition = CoreferencePartition(entities=expected_entities.values())


"""
from pprint import pprint
            l1 = expected_document.sentences
            l2 = actual_document.sentences
            pprint(l1)
            pprint(l2)
            i = 1
            m1 = l1[i]
            m2 = l2[i]
            #attribute_name = "constituency_tree"
            #attr1 = getattr(m1, attribute_name).string_representation
            #attr2 = getattr(m2, attribute_name).string_representation
            #print(attr1)
            #print(attr2)
            #result3, message3 = attr1._inner_eq(attr2)
            #self.assertTrue(result3, message3)
            result2, message2 = m1._inner_eq(m2)
            self.assertTrue(result2, message2)
"""
    


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()