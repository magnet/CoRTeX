# -*- coding: utf-8 -*-

import unittest
import pytest
from collections import OrderedDict

from cortex.api.document import Document
from cortex.api.coreference_partition import CoreferencePartition
from cortex.api.markable import Token, NamedEntity, Mention, Sentence
from cortex.api.constituency_tree import ConstituencyTree
from cortex.api.entity import Entity

from cortex.languages.english.mention_characterizer import (EnglishMentionCharacterizer, 
                                                              REGEXES_STRING, 
                                                              LAST_TOKEN_NAMED_ENTITY_STRING)
from cortex.parameters.mention_data_tags import (UNKNOWN_VALUE_TAG, 
                                                  EXPANDED_PRONOUN_GRAM_TYPE_TAG, NAME_GRAM_TYPE_TAG, 
                                                  NOMINAL_GRAM_TYPE_TAG, VERB_GRAM_TYPE_TAG, 
                                                  EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS, 
                                                  NAME_GRAM_TYPE_SUBTYPE_TAGS, 
                                                  NOMINAL_GRAM_TYPE_SUBTYPE_TAGS, 
                                                  SINGULAR_NUMBER_TAG, PLURAL_NUMBER_TAG, 
                                                  MALE_GENDER_TAG, FEMALE_GENDER_TAG, 
                                                  NEUTRAL_GENDER_TAG, FIRST_PERSON_TAG, 
                                                  SECOND_PERSON_TAG, THIRD_PERSON_TAG)

from cortex.languages.english.parameters.ontonotes_v5_named_entities_data_tags import (PERSON_NAMED_ENTITY_TYPE_TAGS, 
                                                                                         ORGANIZATION_NAMED_ENTITY_TYPE_TAGS, 
                                                                                         LOCATION_NAMED_ENTITY_TYPE_TAGS,
                                                                                         ARTIFACT_NAMED_ENTITY_TYPE_TAGS)

PERSON_NAMED_ENTITY_TYPE_TAG = PERSON_NAMED_ENTITY_TYPE_TAGS[0]
ORGANIZATION_NAMED_ENTITY_TYPE_TAG = ORGANIZATION_NAMED_ENTITY_TYPE_TAGS[0] 
LOCATION_NAMED_ENTITY_TYPE_TAG = LOCATION_NAMED_ENTITY_TYPE_TAGS[0]
ARTIFACT_NAMED_ENTITY_TYPE_TAG = ARTIFACT_NAMED_ENTITY_TYPE_TAGS[0]

class TestEnglishMentionCharacterizer(unittest.TestCase):

    def setUp(self):
        self.english_mention_characterizer = EnglishMentionCharacterizer()
    
    #@unittest.skip
    def test__enrich_head_extent(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        data = tuple((m1, m2) for m1, m2 in zip(test_document.mentions, expected_document.mentions))
        english_mention_characterizer = EnglishMentionCharacterizer()
        
        # Test
        for m1, m2 in data:
            self.assertIs(m1.head_extent, None)
            expected_result = m2.head_extent
            english_mention_characterizer._enrich_head_extent(m1)
            actual_result = m1.head_extent
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__enrich_head_tokens(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        data = tuple((m1, m2) for m1, m2 in zip(test_document.mentions, expected_document.mentions))
        english_mention_characterizer = EnglishMentionCharacterizer()
        
        # Test
        for m1, m2 in data:
            self.assertIs(m1.head_tokens, None)
            expected_result = m2.head_tokens
            english_mention_characterizer._enrich_head_extent(m1)
            english_mention_characterizer._enrich_head_tokens(m1)
            actual_result = m1.head_tokens
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__mention_is_expanded_pronoun(self):
        # Test parameter
        (_, test_document), _ = get_test_data() #mention_extent_to_proba, expected_document
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [((85,86), True), ((126,129), True), ((4,15), False), ((19,56), False), ((249,255), False)]
        data = tuple((extent_to_actual_mentions_map[extent], expected_result)\
                      for extent, expected_result in test_extents)
        
        # Test
        for m, expected_result in data:
            actual_result = english_mention_characterizer._mention_is_expanded_pronoun(m)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__find_mention_gram_type(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [(85,86), (126,129), (249,255), (4,15), (19,56)] # FIXME: not all kinds of type are tested
        data = tuple((extent_to_actual_mentions_map[extent], 
                      extent_to_expected_mentions_map[extent].gram_type)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            english_mention_characterizer._enrich_head_extent(m)
            english_mention_characterizer._enrich_head_tokens(m)
            actual_result = english_mention_characterizer._find_mention_gram_type(m)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__enrich_gram_type(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [(85,86), (126,129), (249,255), (4,15), (19,56)] # FIXME: not all kinds of type are tested
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].gram_type)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            english_mention_characterizer._enrich_head_extent(m)
            english_mention_characterizer._enrich_head_tokens(m)
            english_mention_characterizer._enrich_gram_type(m)
            actual_result = m.gram_type
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__find_EXPANDED_PRONOUN_gram_subtype(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [(85,86), (126,129)] # FIXME: not all kinds of subtype are tested
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].gram_subtype)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            actual_result = english_mention_characterizer._find_EXPANDED_PRONOUN_gram_subtype(m)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__find_NAME_gram_subtype(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [(4,15), (19,56), (249,255)] # FIXME: not all kinds of subtype are tested
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].gram_subtype)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            actual_result = english_mention_characterizer._find_NAME_gram_subtype(m)
            self.assertEqual(expected_result, actual_result)
    '''
    @unittest.skip
    def test__find_NOMINAL_gram_subtype(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [] # FIXME: no test data yet
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].gram_subtype)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            actual_result = english_mention_characterizer._find_NOMINAL_gram_subtype(m)
            self.assertEqual(expected_result, actual_result)
    
    @unittest.skip
    def test__find_VERB_gram_subtype(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [] # FIXME: no test data yet
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].gram_subtype)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            actual_result = english_mention_characterizer._find_VERB_gram_subtype(m)
            self.assertEqual(expected_result, actual_result)
    '''
    #@unittest.skip
    def test_hypernyms_from_str(self):
        # Test parameter
        data = (("cat", set(['feline.n.01', 'carnivore.n.01', 'placental.n.01', 'mammal.n.01', 
                               'vertebrate.n.01', 'chordate.n.01', 'animal.n.01', 'organism.n.01', 
                               'living_thing.n.01', 'whole.n.02', 'object.n.01', 'physical_entity.n.01', 
                               'entity.n.01'])),
                )
        english_mention_characterizer = EnglishMentionCharacterizer()
        
        # Test
        for string, expected_result in data:
            hypernym_synsets = english_mention_characterizer._hypernyms_from_str(string)
            actual_result = set(s.name() for s in hypernym_synsets)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__find_possible_NAME_named_entity_and_gender(self):
        # Test parameter
        english_mention_characterizer = EnglishMentionCharacterizer()
        data = ((("Julia JONES", "Julia"), ((FEMALE_GENDER_TAG, "gazetteers"), (PERSON_NAMED_ENTITY_TYPE_TAG, UNKNOWN_VALUE_TAG, "gazetteers"))), 
                (("Mr ANDERSON", "Mr"), ((MALE_GENDER_TAG, "regexes"), (PERSON_NAMED_ENTITY_TYPE_TAG, UNKNOWN_VALUE_TAG, "regexes"))), 
                (("Ms JOHANSDÖTIR", "Ms"), ((FEMALE_GENDER_TAG, "regexes"), (PERSON_NAMED_ENTITY_TYPE_TAG, UNKNOWN_VALUE_TAG, "regexes"))), 
                (("the great day", "the"), (None, None))
                )
        
        # Test
        for (mention_raw_text, first_head_token_raw_text), expected_result in data:
            actual_result = english_mention_characterizer._find_possible_NAME_named_entity_and_gender(mention_raw_text, first_head_token_raw_text)
            self.assertEqual(expected_result, actual_result)
            
    #@unittest.skip
    def test__find_EXPANDED_PRONOUN_named_entity(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [(85, 86), (126,129)] # FIXME: not all test data possibilities
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].named_entity)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            actual_result = english_mention_characterizer._find_EXPANDED_PRONOUN_named_entity(m)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__find_NAME_named_entity(self):
        # Test parameter
        english_mention_characterizer = EnglishMentionCharacterizer()
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [(19,56)] # Do not use (4,15) and (249,255), because here they come with their own, from their original corpus
        # FIXME: far from testing all possibilities (enumeration, for instance...
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].named_entity)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            english_mention_characterizer._enrich_head_extent(m)
            english_mention_characterizer._enrich_head_tokens(m)
            actual_result = english_mention_characterizer._find_NAME_named_entity(m)
            self.assertEqual(expected_result, actual_result)
    '''
    @unittest.skip
    def test__find_NOMINAL_named_entity(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [] # FIXME: no test data yet
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].named_entity)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            actual_result = english_mention_characterizer._find_NOMINAL_named_entity(m)
            self.assertEqual(expected_result, actual_result)
    
    @unittest.skip
    def test__find_VERB_named_entity(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [] # FIXME: no test data yet
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].named_entity)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            actual_result = english_mention_characterizer._find_VERB_named_entity(m)
            self.assertEqual(expected_result, actual_result)
    '''
    #@unittest.skip
    def test__find_EXPANDED_PRONOUN_gender(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [(85,86), (126,129)] # FIXME: not all genders are tested
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].gender)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            english_mention_characterizer._enrich_head_extent(m)
            actual_result = english_mention_characterizer._find_EXPANDED_PRONOUN_gender(m)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__find_NAME_gender(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [(4,15), (19,56), (249,255)] # FIXME: not all genders are tested
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].gender)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            english_mention_characterizer._enrich_head_extent(m)
            english_mention_characterizer._enrich_head_tokens(m)
            if m.named_entity is None:
                m.named_entity = english_mention_characterizer._find_NAME_named_entity(m)
            actual_result = english_mention_characterizer._find_NAME_gender(m) # FIXME: 'June of 2004' is attributed 'fem' instead of 'neut' because 'June' can be used as a feminine name
            self.assertEqual(expected_result, actual_result)
    '''
    @unittest.skip
    def test__find_NOMINAL_gender(self):
        # Test parameter
        d(_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [] # FIXME: no test data yet
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].gender)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            english_mention_characterizer._enrich_head_extent(m)
            actual_result = english_mention_characterizer._find_NOMINAL_gender(m)
            self.assertEqual(expected_result, actual_result)
    
    @unittest.skip
    def test__find_VERB_gender(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [] # FIXME: no test data yet
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].gender)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            english_mention_characterizer._enrich_head_extent(m)
            actual_result = english_mention_characterizer._find_VERB_gender(m)
            self.assertEqual(expected_result, actual_result)
    '''
    #@unittest.skip
    def test__find_EXPANDED_PRONOUN_number(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [(85,86), (126,129)] # FIXME: not all numbers are tested
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].number)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            actual_result = english_mention_characterizer._find_EXPANDED_PRONOUN_number(m)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__find_NAME_number(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [(4,15), (19,56), (249,255)] # FIXME: not all numbers are tested
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].number)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            english_mention_characterizer._enrich_head_extent(m)
            english_mention_characterizer._enrich_head_tokens(m)
            actual_result = english_mention_characterizer._find_NAME_number(m)
            self.assertEqual(expected_result, actual_result)
    '''
    @unittest.skip
    def test__find_NOMINAL_number(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [] # FIXME: no test data yet
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].number)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            english_mention_characterizer._enrich_head_extent(m)
            english_mention_characterizer._enrich_head_tokens(m)
            actual_result = english_mention_characterizer._find_NOMINAL_number(m)
            self.assertEqual(expected_result, actual_result)
    
    @unittest.skip
    def test__find_VERB_number(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [] # FIXME: no test data yet
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].number)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            english_mention_characterizer._enrich_head_extent(m)
            english_mention_characterizer._enrich_head_tokens(m)
            actual_result = english_mention_characterizer._find_VERB_number(m)
            self.assertEqual(expected_result, actual_result)
    '''
    #@unittest.skip
    def test_wn_str(self):
        # Test parameter
        data = (("this is\na\ttest", "this_is_a_test"), )
        english_mention_characterizer = EnglishMentionCharacterizer()
        
        # Test
        for string, expected_result in data:
            actual_result = english_mention_characterizer._wn_str(string)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__find_EXPANDED_PRONOUN_wn_synonyms(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [(85,86), (126,129)]
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].wn_synonyms)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            actual_result = tuple(english_mention_characterizer._find_EXPANDED_PRONOUN_wn_synonyms(m))
            self.assertEqual(expected_result, actual_result)
        
    
    #@unittest.skip
    def test__find_NAME_wn_synonyms(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [(4,15), (19,56), (249,255)]
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].wn_synonyms)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            actual_result = tuple(english_mention_characterizer._find_NAME_wn_synonyms(m))
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__find_NOMINAL_wn_synonyms(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data2() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [(16,23)]
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].wn_synonyms)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            actual_result = tuple(english_mention_characterizer._find_NOMINAL_wn_synonyms(m))
            self.assertEqual(expected_result, actual_result)
    '''
    @unittest.skip
    def test__find_VERB_wn_synonyms(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [] # FIXME: no test data yet
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].wn_synonyms)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            actual_result = tuple(english_mention_characterizer._find_VERB_wn_synonyms(m))
            self.assertEqual(expected_result, actual_result)
    '''
    #@unittest.skip
    def test__find_EXPANDED_PRONOUN_wn_hypernyms(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [(85,86), (126,129)]
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].wn_hypernyms)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            actual_result = tuple(english_mention_characterizer._find_EXPANDED_PRONOUN_wn_hypernyms(m))
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__find_NAME_wn_hypernyms(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [(4,15), (19,56), (249,255)]
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].wn_hypernyms)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            actual_result = tuple(english_mention_characterizer._find_NAME_wn_hypernyms(m))
            self.assertEqual(expected_result, actual_result)
    '''
    @unittest.skip
    def test__find_NOMINAL_wn_hypernyms(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [] # FIXME: no test data yet
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].wn_hypernyms)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            actual_result = tuple(english_mention_characterizer._find_NOMINAL_wn_hypernyms(m))
            self.assertEqual(expected_result, actual_result)
    
    @unittest.skip
    def test__find_VERB_wn_hypernyms(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [] # FIXME: no test data yet
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].wn_hypernyms)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            actual_result = tuple(english_mention_characterizer._find_VERB_wn_hypernyms(m))
            self.assertEqual(expected_result, actual_result)
    '''
    #@unittest.skip
    def test_antonyms_from_str(self):
        # Test parameter
        data = (("cold", set([])), ("tall", set([])), ("cat", set([])))
        english_mention_characterizer = EnglishMentionCharacterizer()
        
        # Test
        for string, expected_result in data:
            antonym_synsets_it = english_mention_characterizer._antonyms_from_str(string)
            actual_result = set(s.name() for s in antonym_synsets_it)
            self.assertEqual(expected_result, actual_result)
    '''
    #@unittest.skip
    def test__find_EXPANDED_PRONOUN_wn_antonyms(self):
        # Test parameter
        (_, test_document), _ = get_test_data() #mention_extent_to_proba, expected_document
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [(85,86), (126,129)]
        data = tuple((extent_to_actual_mentions_map[extent], ()) for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            actual_result = tuple(english_mention_characterizer._find_EXPANDED_PRONOUN_wn_antonyms(m))
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__find_NAME_wn_antonyms(self):
        # Test parameter
        (_, test_document), _ = get_test_data() #mention_extent_to_proba, expected_document
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [(4,15), (19,56), (249,255)]
        data = tuple((extent_to_actual_mentions_map[extent], () ) for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            actual_result = tuple(english_mention_characterizer._find_NAME_wn_antonyms(m))
            self.assertEqual(expected_result, actual_result)
    
    @unittest.skip
    def test__find_NOMINAL_wn_antonyms(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [] # FIXME: no test data yet
        data = tuple((extent_to_actual_mentions_map[extent], ()) for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            actual_result = english_mention_characterizer._find_NOMINAL_wn_antonyms(m)
            self.assertEqual(expected_result, actual_result)
    
    @unittest.skip
    def test__find_VERB_wn_antonyms(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [] # FIXME: no test data yet
        data = tuple((extent_to_actual_mentions_map[extent], ()) for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            actual_result = english_mention_characterizer._find_VERB_wn_antonyms(m)
            self.assertEqual(expected_result, actual_result)
    '''
    #@unittest.skip
    def test__enrich_attribute(self):
        # Test parameter
        attribute_names = ["gram_subtype"] # , "named_entity", "gender", "number", "wn_synonyms", "wn_hypernyms", "wn_antonyms"
        english_mention_characterizer = EnglishMentionCharacterizer()
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [(85,86), (126,129), (4,15), (19,56), (249,255)] # FIXME: not every kind of gram_type / subtype data
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent])\
                      for extent in test_extents)
        
        # Test 
        for name in attribute_names:
            for m1, m2 in data:
                english_mention_characterizer._enrich_head_extent(m1)
                english_mention_characterizer._enrich_head_tokens(m1)
                english_mention_characterizer._enrich_gram_type(m1)
                english_mention_characterizer._enrich_attribute(m1, name)
                expected_value = getattr(m2, name)
                actual_value = getattr(m1, name)
                self.assertEqual(expected_value, actual_value)
    
    #@unittest.skip
    def test__enrich_gram_subtype(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [(85,86), (126,129), (4,15), (19,56), (249,255)] # FIXME: not all kinds of gram_type are tested
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].gram_subtype)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            english_mention_characterizer._enrich_head_extent(m)
            english_mention_characterizer._enrich_head_tokens(m)
            english_mention_characterizer._enrich_gram_type(m)
            english_mention_characterizer._enrich_gram_subtype(m)
            actual_result = m.gram_subtype
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__enrich_named_entity(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [(249,255)]#[(85,86), (126,129), (4,15), (19,56), (249,255)] # FIXME: not all kinds of gram_type / subtype are tested
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].named_entity)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            english_mention_characterizer._enrich_head_extent(m)
            english_mention_characterizer._enrich_head_tokens(m)
            english_mention_characterizer._enrich_gram_type(m)
            english_mention_characterizer._enrich_named_entity(m)
            actual_result = m.named_entity
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__enrich_gender(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [(85,86), (126,129), (4,15), (19,56), (249,255)] # FIXME: not all kinds of gram_type / subtype are tested
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].gender)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            english_mention_characterizer._enrich_head_extent(m)
            english_mention_characterizer._enrich_head_tokens(m)
            english_mention_characterizer._enrich_gram_type(m)
            english_mention_characterizer._enrich_named_entity(m)
            english_mention_characterizer._enrich_gender(m) # FIXME: problem because of 'June'
            actual_result = m.gender
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__enrich_number(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [(85,86), (126,129), (4,15), (19,56), (249,255)] # FIXME: not all kinds of gram_type / subtype are tested
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].number)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            english_mention_characterizer._enrich_head_extent(m)
            english_mention_characterizer._enrich_head_tokens(m)
            english_mention_characterizer._enrich_gram_type(m)
            english_mention_characterizer._enrich_number(m)
            actual_result = m.number
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__enrich_wn_synonyms(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [(85,86), (126,129), (4,15), (19,56), (249,255)] # FIXME: not all kinds of gram_type / subtype are tested
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].wn_synonyms)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            english_mention_characterizer._enrich_head_extent(m)
            english_mention_characterizer._enrich_head_tokens(m)
            english_mention_characterizer._enrich_gram_type(m)
            english_mention_characterizer._enrich_wn_synonyms(m)
            actual_result = m.wn_synonyms
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__enrich_wn_hypernyms(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [(85,86), (126,129), (4,15), (19,56), (249,255)] # FIXME: not all kinds of gram_type / subtype are tested
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].wn_hypernyms)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            english_mention_characterizer._enrich_head_extent(m)
            english_mention_characterizer._enrich_head_tokens(m)
            english_mention_characterizer._enrich_gram_type(m)
            english_mention_characterizer._enrich_wn_hypernyms(m)
            actual_result = m.wn_hypernyms
            self.assertEqual(expected_result, actual_result)
    '''
    @unittest.skip
    def test__enrich_wn_antonyms(self):
        # Test parameter
        (_, test_document), expected_document = get_test_data() #mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [(85,86), (126,129), (4,15), (19,56), (249,255)] # FIXME: not all kinds of gram_type / subtype are tested
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent].wn_antonyms)\
                      for extent in test_extents)
        
        # Test
        for m, expected_result in data:
            english_mention_characterizer._enrich_head_extent(m)
            english_mention_characterizer._enrich_head_tokens(m)
            english_mention_characterizer._enrich_gram_type(m)
            english_mention_characterizer._enrich_wn_antonyms(m)
            actual_result = m.wn_antonyms
            self.assertEqual(expected_result, actual_result)
    '''
    #@unittest.skip
    def test__enrich_mention(self):
        # Test parameter
        (mention_extent_to_proba, test_document), expected_document = get_test_data()
        english_mention_characterizer = EnglishMentionCharacterizer()
        extent_to_expected_mentions_map = expected_document.extent2mention
        extent_to_actual_mentions_map = test_document.extent2mention
        test_extents = [(85,86), (126,129), (4,15), (19,56), (249,255)] # FIXME: not all kinds of gram_type / subtype are tested
        data = tuple((extent_to_actual_mentions_map[extent], extent_to_expected_mentions_map[extent])\
                      for extent in test_extents)
        
        # Test
        for m1, m2 in data:
            english_mention_characterizer._enrich_mention(m1, mention_extent_to_proba=mention_extent_to_proba)
            self.assertEqual(m2, m1)
    
    #@unittest.skip
    def test__enrich_document_mentions(self):
        # Test data
        (mention_extent_to_proba, test_document), expected_document = get_test_data()
        english_mention_characterizer = EnglishMentionCharacterizer()
        english_mention_characterizer._enrich_document_mentions(test_document, mention_extent_to_proba=mention_extent_to_proba)
        
        # Test equality mention to mention
        self.assertEqual(expected_document.mentions, test_document.mentions)
        
        # Test equality of document wrt named entities
        self.assertEqual(expected_document.named_entities, test_document.named_entities)
        
        # Test equality of document wrt coreference partition
        self.assertEqual(expected_document.coreference_partition, test_document.coreference_partition)
    
    #@unittest.skip
    @pytest.mark.need_nada
    def test_enrich_documents_mentions(self):
        # Test data
        (_, test_document), expected_document = get_test_data() # mention_extent_to_proba
        english_mention_characterizer = EnglishMentionCharacterizer(raise_exception_if_referential_proba_not_computable=True)
        documents = (test_document, test_document)
        expected_documents = (expected_document, expected_document)
        
        # Test
        english_mention_characterizer.enrich_documents_mentions(documents)
        for expected_document, document in zip(expected_documents, documents):
            # Test equality mention to mention
            self.assertEqual(expected_document.mentions, document.mentions)
            
            # Test equality of document wrt named entities
            self.assertEqual(expected_document.named_entities, document.named_entities)
            
            # Test equality of document wrt coreference partition
            self.assertEqual(expected_document.coreference_partition, test_document.coreference_partition)



def get_test_data():
    # Build document data
    raw_text = "\nIn June of 2004 , the Shanxi Yongji Peasants Association was formally "\
                        "established ; it now has 3,800 members .\nAccordingly , this is the "\
                        "first peasant organization to be legally registered and designated a "\
                        "peasant association since the founding of the PRC ."
    test_document = Document("test_document", raw_text)
    
    expected_document = Document("expected_document", raw_text)
    
    
    test_sentences_data = []
    expected_sentences_data = []
    mention_extent_to_proba = {}
    
    ## First sentence
    test_sentence_data = ["1", (1,110), "In June of 2004 , the Shanxi Yongji Peasants Association was formally established ; it now has 3,800 members .", 
                     "(TOP (S (S (PP (IN In) (NP (NP (NNP June)) (PP (IN of) (NP (CD 2004))))) (, ,) (NP (DT the) (NNP Shanxi) (NNP Yongji) (NNPS Peasants) (NNP Association)) (VP (VBD was) (ADVP (RB formally)) (VP (VBN established)))) (: ;) (S (NP (PRP it)) (ADVP (RB now)) (VP (VBZ has) (NP (CD 3,800) (NNS members)))) (. .)))",
                     "speaker#1", 
                     (((58, 60, "V"),),
                      ((1, 15, "ARGM-TMP"), (19, 56, "ARG1"), (62, 69, "ARGM-MNR"), (71, 81, "V")),
                      ((85, 86, "ARG0"), (88, 90, "ARGM-TMP"), (92, 94, "V"), (96, 108, "ARG1"))
                     ),
                     [("4,15", (4,15), "June of 2004", "DATE", UNKNOWN_VALUE_TAG, None),
                      ("96,100", (96,100), "3,800", "CARDINAL", UNKNOWN_VALUE_TAG, None)
                     ]
                    ]
    expected_sentence_data = ["1", (1,110), "In June of 2004 , the Shanxi Yongji Peasants Association was formally established ; it now has 3,800 members .", 
                             "(TOP (S (S (PP (IN In) (NP (NP (NNP June)) (PP (IN of) (NP (CD 2004))))) (, ,) (NP (DT the) (NNP Shanxi) (NNP Yongji) (NNPS Peasants) (NNP Association)) (VP (VBD was) (ADVP (RB formally)) (VP (VBN established)))) (: ;) (S (NP (PRP it)) (ADVP (RB now)) (VP (VBZ has) (NP (CD 3,800) (NNS members)))) (. .)))",
                             "speaker#1", 
                             (((58, 60, "V"),),
                              ((1, 15, "ARGM-TMP"), (19, 56, "ARG1"), (62, 69, "ARGM-MNR"), (71, 81, "V")),
                              ((85, 86, "ARG0"), (88, 90, "ARGM-TMP"), (92, 94, "V"), (96, 108, "ARG1"))
                             ),
                             [("4,15", (4,15), "June of 2004", "DATE", UNKNOWN_VALUE_TAG, None),
                              ("19,56", (19,56), "the Shanxi Yongji Peasants Association", ORGANIZATION_NAMED_ENTITY_TYPE_TAG, UNKNOWN_VALUE_TAG, REGEXES_STRING),
                              ("96,100", (96,100), "3,800", "CARDINAL", UNKNOWN_VALUE_TAG, None)
                             ]
                            ]
    sentence_tokens_data = [("1-1", (1,2), "In", "In", "IN"), 
                   ("1-2", (4,7), "June", "June", "NNP"), 
                   ("1-3", (9,10), "of", "of", "IN"), 
                   ("1-4", (12,15), "2004", "2004", "CD"),
                   ("1-5", (17,17), ",", ",", ","),
                   ("1-6", (19,21), "the", "the", "DT"),
                   ("1-7", (23,28), "Shanxi", "Shanxi", "NNP"),
                   ("1-8", (30,35), "Yongji", "Yongji", "NNP"),
                   ("1-9", (37,44), "Peasants", "Peasants", "NNPS"),
                   ("1-10", (46,56), "Association", "Association", "NNP"), 
                   ("1-11", (58,60), "was", "be", "VBD"),
                   ("1-12", (62,69), "formally", "formally", "RB"),
                   ("1-13", (71,81), "established", "establish", "VBN"),
                   ("1-14", (83,83), ";", ";", ":"),
                   ("1-15", (85,86), "it", "it", "PRP"),
                   ("1-16", (88,90), "now", "now", "RB"),
                   ("1-17", (92,94), "has", "have", "VBZ"),
                   ("1-18", (96,100), "3,800", "3,800", "CD"),
                   ("1-19", (102,108), "members", "members", "NNS"),
                   ("1-20", (110,110), ".", ".", ".")]
    
    test_sentence_mentions_data = [((4,15), None, 0, "June of 2004", None, None, None, None, None, None, None), 
                              ((19,56), None, 1, "the Shanxi Yongji Peasants Association", None, None, None, None, None, None, None), 
                              ((85,86), None, 1, "it", None, None, None, None, None, None, None)]
    mention_extent_to_proba[(85,86)] = 1 - float("0.991")
    expected_mentions_data = [((4,15), (4,7), 0, "June of 2004", NAME_GRAM_TYPE_TAG, NAME_GRAM_TYPE_SUBTYPE_TAGS["LONG_NAME"], SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, [], []), 
                              ((19,56), (46,56), 1, "the Shanxi Yongji Peasants Association", NAME_GRAM_TYPE_TAG, NAME_GRAM_TYPE_SUBTYPE_TAGS["LONG_NAME"], SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, [], []), 
                              ((85,86), (85,86), 1, "it", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, mention_extent_to_proba[(85,86)], [], [])]
    
    test_sentence_data.append(sentence_tokens_data)
    expected_sentence_data.append(sentence_tokens_data)
    test_sentence_data.append(test_sentence_mentions_data)
    expected_sentence_data.append(expected_mentions_data)
    
    test_sentences_data.append(test_sentence_data)
    expected_sentences_data.append(expected_sentence_data)
    
    
    ## Second sentence
    test_sentence_data = ["2", (112,257), "Accordingly , this is the first peasant organization to be legally registered and designated a peasant association since the founding of the PRC .", 
                       "(TOP (S (ADVP (RB Accordingly)) (, ,) (NP (DT this)) (VP (VBZ is) (NP (NP (DT the) (JJ first) (NN peasant) (NN organization)) (SBAR (S (VP (TO to) (VP (VB be) (ADVP (RB legally)) (VP (VP (VBN registered)) (CC and) (VP (VBN designated) (S (NP (DT a) (NN peasant) (NN association)))) (PP (IN since) (NP (NP (DT the) (NN founding)) (PP (IN of) (NP (DT the) (NNP PRC)))))))))))) (. .)))", 
                      "speaker#1", 
                      (((112, 122, "ARGM-DIS"), (126, 129, "ARG1"), (131, 132, "V"), (134, 255, "ARG2")),
                       ((134, 163, "ARG1"), (171, 177, "ARGM-MNR"), (179, 188, "V"), (227, 255, "ARGM-TMP")) ,
                       ((134, 163, "ARG1"), (171, 177, "ARGM-MNR"), (194, 203, "V"), (205, 225, "ARG2"), (227, 255, "ARGM-TMP")), 
                       ((168, 169, "V"),)           
                      ),
                      [("138,142", (138,142), "first", "ORDINAL", UNKNOWN_VALUE_TAG, None), 
                       ("253,255", (253,255), "PRC", "GPE", UNKNOWN_VALUE_TAG, None)
                      ]
                    ]
    expected_sentence_data = ["2", (112,257), "Accordingly , this is the first peasant organization to be legally registered and designated a peasant association since the founding of the PRC .", 
                           "(TOP (S (ADVP (RB Accordingly)) (, ,) (NP (DT this)) (VP (VBZ is) (NP (NP (DT the) (JJ first) (NN peasant) (NN organization)) (SBAR (S (VP (TO to) (VP (VB be) (ADVP (RB legally)) (VP (VP (VBN registered)) (CC and) (VP (VBN designated) (S (NP (DT a) (NN peasant) (NN association)))) (PP (IN since) (NP (NP (DT the) (NN founding)) (PP (IN of) (NP (DT the) (NNP PRC)))))))))))) (. .)))", 
                          "speaker#1", 
                          (((112, 122, "ARGM-DIS"), (126, 129, "ARG1"), (131, 132, "V"), (134, 255, "ARG2")),
                           ((134, 163, "ARG1"), (171, 177, "ARGM-MNR"), (179, 188, "V"), (227, 255, "ARGM-TMP")) ,
                           ((134, 163, "ARG1"), (171, 177, "ARGM-MNR"), (194, 203, "V"), (205, 225, "ARG2"), (227, 255, "ARGM-TMP")), 
                           ((168, 169, "V"),)           
                          ),
                          [("138,142", (138,142), "first", "ORDINAL", UNKNOWN_VALUE_TAG, None), 
                           ("253,255", (253,255), "PRC", "GPE", UNKNOWN_VALUE_TAG, None),
                           ("249,255", (249,255), "the PRC", "GPE", UNKNOWN_VALUE_TAG, LAST_TOKEN_NAMED_ENTITY_STRING)
                          ]
                        ]
    
    sentence_tokens_data = [("2-1", (112,122), "Accordingly", "Accordingly", "RB"),
                   ("2-2", (124,124), ",", ",", ","),
                   ("2-3", (126,129), "this", "this", "DT"),
                   ("2-4", (131,132), "is", "be", "VBZ"),
                   ("2-5", (134,136), "the", "the", "DT"),
                   ("2-6", (138,142), "first", "first", "JJ"),
                   ("2-7", (144,150), "peasant", "peasant", "NN"),
                   ("2-8", (152,163), "organization", "organization", "NN"),
                   ("2-9", (165,166), "to", "to", "TO"),
                   ("2-10", (168,169), "be", "be", "VB"),
                   ("2-11", (171,177), "legally", "legally", "RB"),
                   ("2-12", (179,188), "registered", "register", "VBN"),
                   ("2-13", (190,192), "and", "and", "CC"),
                   ("2-14", (194,203), "designated", "designate", "VBN"),
                   ("2-15", (205,205), "a", "a", "DT"),
                   ("2-16", (207,213), "peasant", "peasant", "NN"),
                   ("2-17", (215,225), "association", "association", "NN"),
                   ("2-18", (227,231), "since", "since", "IN"),
                   ("2-19", (233,235), "the", "the", "DT"),
                   ("2-20", (237,244), "founding", "found", "NN"),
                   ("2-21", (246,247), "of", "of", "IN"),
                   ("2-22", (249, 251), "the", "the", "DT"),
                   ("2-23", (253,255), "PRC", "PRC", "NNP"),
                   ("2-24", (257,257), ".", ".", ".")]
    
    test_sentence_mentions_data = [((126,129), None, 1, "this", None, None, None, None,  None, None, None), 
                              ((249,255), None, 2, "the PRC", None, None, None, None, None, None, None)] # Warning: this mention has been added to the test test_document, compared to the original test_document where it does not appear.
    expected_mentions_data = [((126,129), (126,129), 1, "this", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["DEM_PRO"], SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, [], []), 
                              ((249,255), (253,255), 2, "the PRC", NAME_GRAM_TYPE_TAG, NAME_GRAM_TYPE_SUBTYPE_TAGS["LONG_NAME"], SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, [], [])] # Warning: this mention has been added to the test test_document, compared to the original test_document where it does not appear.
    
    test_sentence_data.append(sentence_tokens_data)
    expected_sentence_data.append(sentence_tokens_data)
    
    test_sentence_data.append(test_sentence_mentions_data)
    expected_sentence_data.append(expected_mentions_data)
    
    test_sentences_data.append(test_sentence_data)
    expected_sentences_data.append(expected_sentence_data)
    
    
    #outside_origin_named_entity_extents = set(l[1] for test_sentence_data in test_sentences_data for l in test_sentence_data[4])
    #outside_origin_named_entity_extents = set([(4,15), (96,100), (138,142), (249,255), (253,255)])
    
    ## Build the test document
    _assemble_document_data(test_document, test_sentences_data, find_and_synchronize_heads=False)
    
    ## Build the expected document
    _assemble_document_data(expected_document, expected_sentences_data, find_and_synchronize_heads=True)
    
    return (mention_extent_to_proba, test_document), expected_document


def get_test_data2():
    # Build document data
    raw_text = "\nIt can be said citizens were a very important factor ."
    test_document = Document("test_document", raw_text)
    
    expected_document = Document("expected_document", raw_text)
    
    
    test_sentences_data = []
    expected_sentences_data = []
    mention_extent_to_proba = {}
    
    ## First sentence
    test_sentence_data = ["1", (1,54), "It can be said citizens were a very important factor .", 
                          "(TOP (S (NP (PRP it)) (VP (MD can) (VP (VB be) (VP (VBN said) (SBAR (S (NP (NNS citizens)) (VP (VBD were) (NP (NP (DT a) (ADJP (RB very) (JJ important)) (NN factor))))))))) (. .)))",
                          "speaker#1", 
                          tuple(),
                          tuple(),
                          ]
    expected_sentence_data = ["1", (1,54), "It can be said citizens were a very important factor .", 
                              "(TOP (S (NP (PRP it)) (VP (MD can) (VP (VB be) (VP (VBN said) (SBAR (S (NP (NNS citizens)) (VP (VBD were) (NP (NP (DT a) (ADJP (RB very) (JJ important)) (NN factor))))))))) (. .)))", 
                              "speaker#1", 
                              tuple(),
                              tuple(),
                             ]
    sentence_tokens_data = [("1-1", (1,2), "It", "PRP", "it"), 
                            ("1-2", (4,6), "can", "MD", "can"), 
                            ("1-3", (8,9), "be", "VB", "be"), 
                            ("1-4", (11,14), "said", "VBN", "say"), 
                            ("1-5", (16,23), "citizens", "NNS", "citizen"), 
                            ("1-6", (25,28), "were", "VBD", "be"), 
                            ("1-7", (30,30), "a", "DT", "a"), 
                            ("1-8", (32,35), "very", "RB", "very"), 
                            ("1-9", (37,45), "important", "JJ", "important"), 
                            ("1-10", (47,52), "factor", "NN", "factor"), 
                            ("1-11", (54,54), ".", ".", "."), 
                           ]
    test_sentence_mentions_data = [((16,23), None, 0, "citizens", None, None, None, None, None, None, None), 
                                   ]
    expected_mentions_data = [((16,23), (16,23), 0, "citizens", NOMINAL_GRAM_TYPE_TAG, NOMINAL_GRAM_TYPE_SUBTYPE_TAGS["SHORT_UNDET_NP"], PLURAL_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, ["citizen.n.01",], ["national.n.01", "person.n.01", "causal_agent.n.01", "organism.n.01", "physical_entity.n.01", "living_thing.n.01", "entity.n.01", "whole.n.02", "object.n.01"]), 
                              ]
    
    test_sentence_data.append(sentence_tokens_data)
    expected_sentence_data.append(sentence_tokens_data)
    test_sentence_data.append(test_sentence_mentions_data)
    expected_sentence_data.append(expected_mentions_data)
    
    test_sentences_data.append(test_sentence_data)
    expected_sentences_data.append(expected_sentence_data)
    
    #outside_origin_named_entity_extents = set(l[1] for test_sentence_data in test_sentences_data for l in test_sentence_data[4])
    #outside_origin_named_entity_extents = set([(4,15), (96,100), (138,142), (249,255), (253,255)])
    
    ## Build the test document
    _assemble_document_data(test_document, test_sentences_data, find_and_synchronize_heads=False)
    
    ## Build the expected document
    _assemble_document_data(expected_document, expected_sentences_data, find_and_synchronize_heads=True)
    
    return (mention_extent_to_proba, test_document), expected_document


def _assemble_document_data(document, sentences_data, find_and_synchronize_heads=False):
    expected_entities = {}
    sentences = []
    named_entities_list = []
    for sentence_data in sentences_data:
        (ident, extent, raw_text, constituency_tree_string, speaker, predicate_arguments, 
         named_entities_data, tokens_data, mentions_data) = sentence_data
        sentence = Sentence(ident, extent, document, raw_text=raw_text)
        
        # Mentions
        mentions = OrderedDict()
        for mention_data in mentions_data:
            (extent, head_extent, entity_id, raw_text, gram_type, gram_subtype, number, gender, 
             referential_probability, wn_synonyms, wn_hypernyms) = mention_data
            ident = "{e[0]},{e[1]}".format(e=extent, he=head_extent)
            mention = Mention(ident, extent, document, raw_text=raw_text)
            mention.head_extent = head_extent
            mention.gram_type, mention.gram_subtype = gram_type, gram_subtype
            mention.number, mention.gender = number, gender
            mention.referential_probability = referential_probability
            mention.wn_synonyms = wn_synonyms
            mention.wn_hypernyms = wn_hypernyms
            if entity_id is not None:
                if entity_id not in expected_entities:
                    entity = Entity(ident=entity_id)
                    expected_entities[entity_id] = entity
                expected_entities[entity_id].add(mention.extent)
            mentions[extent] =  mention
        mentions_list = list(mentions.values())
        
        # Tokens
        tokens = OrderedDict()
        for token_data in tokens_data:
            ident, extent, raw_text, lemma, POS_tag = token_data
            token = Token(ident, extent,document, raw_text=raw_text)
            token.lemma = lemma
            token.POS_tag = POS_tag
            tokens[extent] = token
        tokens_list = list(tokens.values())
        
        # Named entities
        named_entities = OrderedDict()
        for named_entity_data in named_entities_data:
            ident, extent, raw_text, type_, subtype, origin = named_entity_data
            named_entity = NamedEntity(ident, extent, document, type_, raw_text=raw_text, 
                                       subtype=subtype, origin=origin)
            named_entities[extent] = named_entity
        named_entities_list.extend(list(named_entities.values()))
        
        # ConstituencyTree
        sentence.constituency_tree = ConstituencyTree.parse(constituency_tree_string)
        
        # Speaker
        sentence.speaker = speaker
        
        # Predicate arguments
        sentence.predicate_arguments = predicate_arguments
        
        # Synchronization tokens <-> mentions
        for (ms, me), mention in mentions.items():
            mention.tokens = list(t for (ts, te), t in tokens.items() if ms <= ts and te <= me)
            if find_and_synchronize_heads:
                mhs, mhe = mention.head_extent
                mention.head_tokens = list(t for (ts, te), t in tokens.items() if mhs <= ts and te <= mhe)
        
        # Synchronization named_entities <-> mentions
        for extent, mention in mentions.items():
            if extent in named_entities:
                mention.named_entity = named_entities.get(extent)
        
        # Synchronization named_entities <-> tokens
        for extent, token in tokens.items():
            token.named_entity = named_entities.get(extent)
        
        # Synchronization tokens <-> constituency tree leaves
        assert len(sentence.constituency_tree.leaves) == len(tokens), "Arg."
        for i, node in enumerate(sentence.constituency_tree.leaves):
            token = tokens_list[i]
            node.token = token
            token.constituency_tree_node = node
        
        # Set sentence's data
        sentence.mentions = mentions_list
        sentence.tokens = tokens_list
        
        # Finally
        sentences.append(sentence)
    
    ## Finally, set the test_document's data
    document.tokens = list(token for sentence in sentences for token in sentence.tokens)
    document.mentions = list(mention for sentence in sentences for mention in sentence.mentions)
    document.sentences = sentences
    document.named_entities = sorted(named_entities_list)
    document.coreference_partition = CoreferencePartition(entities=expected_entities.values())


"""
print(m1)
print(m2)
result2, message2 = m1._inner_eq(m2)
if message2:
    attr1 = getattr(m1, message2)
    attr2 = getattr(m2, message2)
    print(attr1)
    print(attr2)
    print(m1.named_entity)
self.assertTrue(result2, message2)
"""


'''
pprint(expected_mentions)
pprint(actual_mentions)
l1 = expected_mentions
l2 = actual_mentions
i = 0
m1 = l1[i]
m2 = l2[i]

attribute_name = "gender"
attr1 = getattr(m1, attribute_name)
attr2 = getattr(m2, attribute_name)
print(attr1)
print(attr2)
#result3, message3 = attr1._inner_eq(attr2)
#self.assertTrue(result3, message3)
result2, message2 = m1._inner_eq(m2)
self.assertTrue(result2, message2)
'''



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()