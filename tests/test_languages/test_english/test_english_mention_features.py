# -*- coding: utf-8 -*-

import unittest
from collections import OrderedDict

from cortex.api.document import Document
from cortex.api.markable import Token, NamedEntity, Mention, Sentence
from cortex.api.constituency_tree import ConstituencyTree
from cortex.languages.english.mention_features import EnglishMentionFeatures
from cortex.parameters.mention_data_tags import (UNKNOWN_VALUE_TAG, 
                                                  EXPANDED_PRONOUN_GRAM_TYPE_TAG, NAME_GRAM_TYPE_TAG, 
                                                  NOMINAL_GRAM_TYPE_TAG, VERB_GRAM_TYPE_TAG, 
                                                  NAME_GRAM_TYPE_SUBTYPE_TAGS, 
                                                  EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS, 
                                                  NOMINAL_GRAM_TYPE_SUBTYPE_TAGS, 
                                                  SINGULAR_NUMBER_TAG, PLURAL_NUMBER_TAG, 
                                                  MALE_GENDER_TAG, FEMALE_GENDER_TAG, 
                                                  NEUTRAL_GENDER_TAG, FIRST_PERSON_TAG, 
                                                  SECOND_PERSON_TAG, THIRD_PERSON_TAG)

from cortex.languages.english.parameters.ontonotes_v5_named_entities_data_tags import (PERSON_NAMED_ENTITY_TYPE_TAGS, 
                                                                                         ORGANIZATION_NAMED_ENTITY_TYPE_TAGS, 
                                                                                         LOCATION_NAMED_ENTITY_TYPE_TAGS,
                                                                                         ARTIFACT_NAMED_ENTITY_TYPE_TAGS)

PERSON_NAMED_ENTITY_TYPE_TAG = PERSON_NAMED_ENTITY_TYPE_TAGS[0]
ORGANIZATION_NAMED_ENTITY_TYPE_TAG = ORGANIZATION_NAMED_ENTITY_TYPE_TAGS[0] 
LOCATION_NAMED_ENTITY_TYPE_TAG = LOCATION_NAMED_ENTITY_TYPE_TAGS[0]
ARTIFACT_NAMED_ENTITY_TYPE_TAG = ARTIFACT_NAMED_ENTITY_TYPE_TAGS[0]

doc_ident = "test" # Nathan ends at 80; "for him" ends at 164; "hers" begins at 208
raw_text = "\nI am great , but you are even better ! Wow , she can do that thing which Nathan "\
            "could not . What thing ? Well , a high jump kick . It seems doing it is out for him "\
            ", at least for now . 'Charlotte's skill , hers, truly , is really up to the task .' "\
            ", they said . But , see the thing , this , for yourself , a different task could be given ."
document = Document(doc_ident, raw_text)

english_mention_features = EnglishMentionFeatures()


class TestEnglishMentionFeatures(unittest.TestCase):
    
    def setUp(self):
        self.mention_features = EnglishMentionFeatures()
    
    def test_find_mention_head_extent(self):
        # Test parameters
        test_fixture = get_test_mention_head_extent_data()
        
        # Test
        for mention, expected_head_extent in test_fixture:
            actual_head_extent = self.mention_features.find_mention_head_extent(mention)
            self.assertEqual(expected_head_extent, actual_head_extent)
    
    def test_is_expanded_pronoun(self):
        # Test parameter
        mention_features = self.mention_features
        document = get_data()
        data = [(document.mentions[0], False), # name
                (document.mentions[7], True), # pronoun
                (document.mentions[10], False), # nominal
                (document.mentions[11], False)# verb
                ]
        
        # Test
        for mention, expected_result in data:
            actual_result = mention_features.is_expanded_pronoun(mention)
            self.assertEqual(expected_result, actual_result)
    
    def test_is_name(self):
        # Test parameter
        mention_features = self.mention_features
        document = get_data()
        data = [(document.mentions[0], True), # name
                (document.mentions[7], False), # pronoun
                (document.mentions[10], False), # nominal
                (document.mentions[11], False)# verb
                ]
        
        # Test
        for mention, expected_result in data:
            actual_result = mention_features.is_name(mention)
            self.assertEqual(expected_result, actual_result)
    
    def test_is_nominal(self):
        # Test parameter
        mention_features = self.mention_features
        document = get_data()
        data = [(document.mentions[0], False), # name
                (document.mentions[7], False), # pronoun
                (document.mentions[10], True), # nominal
                (document.mentions[11], False)# verb
                ]
        
        # Test
        for mention, expected_result in data:
            actual_result = mention_features.is_nominal(mention)
            self.assertEqual(expected_result, actual_result)
    
    def test_is_verb(self):
        # Test parameter
        mention_features = self.mention_features
        document = get_data()
        data = [(document.mentions[0], False), # name
                (document.mentions[7], False), # pronoun
                (document.mentions[10], False), # nominal
                (document.mentions[11], True)# verb
                ]
        
        # Test
        for mention, expected_result in data:
            actual_result = mention_features.is_verb(mention)
            self.assertEqual(expected_result, actual_result)
    
    def test_is_indefinite(self):
        # Test parameter
        mention_features = self.mention_features
        document = get_data()
        data = [(document.mentions[7], False), (document.mentions[9], True), (document.mentions[10], False)]
        
        # Test
        for mention, expected_result in data:
            actual_result = mention_features.is_indefinite(mention)
            self.assertEqual(expected_result, actual_result)
    
    def test_get_dominating_node(self):
        # Test parameter
        mention_features = self.mention_features
        document = get_data()
        data = [(document.mentions[0], document.mentions[0].tokens[0].constituency_tree_node.parent), 
                (document.mentions[4], document.mentions[0].tokens[0].constituency_tree_node.parent.parent), 
                (document.mentions[7], document.mentions[7].tokens[0].constituency_tree_node.parent)]
        
        # Test
        for mention, expected_result in data:
            actual_result = mention_features.get_dominating_node(mention)
            self.assertIs(expected_result, actual_result)
        
    def test_get_dominating_NP_ancestor_node(self):
        # Test parameter
        mention_features = self.mention_features
        document = get_data()
        data = [(document.mentions[0], document.mentions[0].tokens[0].constituency_tree_node.parent.parent), 
                (document.mentions[4], document.mentions[0].tokens[0].constituency_tree_node.parent.parent), 
                (document.mentions[7], document.mentions[7].tokens[0].constituency_tree_node.parent.parent.parent.parent.parent)]
        
        # Test
        for mention, expected_result in data:
            actual_result = mention_features.get_dominating_NP_ancestor_node(mention)
            self.assertIs(expected_result, actual_result)
    
    def test_get_enumeration(self):
        # Test parameter
        mention_features = self.mention_features
        document = get_data()
        data = [(document.mentions[4], [(document.mentions[4].tokens[i],) for i in (0,2,4,6)]), 
                (document.mentions[5], None)]
        
        # Test
        for mention, expected_result in data:
            actual_result = mention_features.get_enumeration(mention)
            self.assertEqual(expected_result, actual_result)
    
    def test_is_enumeration(self):
        # Test parameter
        mention_features = self.mention_features
        document = get_data()
        data = [(document.mentions[4], True), (document.mentions[5], False)]
        
        # Test
        for mention, expected_result in data:
            actual_result = mention_features.is_enumeration(mention)
            self.assertEqual(expected_result, actual_result)
        


        
def get_data():
    raw_text = "\nJohn , Jessie , Jack and Julia should drive this car up to the garage which is located on the 5th avenue in a town ."
    document = Document("test", raw_text)
    
    sentences_data = []
    tree_line = "(TOP (S (S (NP (NNP John) (, ,) (NNP Jessie) (, ,) (NNP Jack) (CC and) (NNP Julia)) "\
                "(VP (VX (MD should))) (VP (VX (VB drive)) (NP (DP (DT this)) (NX (NX (NN car)) (PP (PP (IN up)) "\
                "(PP (PX (TO to))) (NP (DP (DT the)) (NX (NX (NN garage)) (S-REL (NP-REL (WDT which)) "\
                "(VP (VX (VBZ is)) (VP (VP (VBN located)) (PP (PX (IN on)) (NP (DP (DT the)) (NX (NX (ADJP (JJ 5th)) "\
                "(NX (NN avenue))) (PP (PX (IN in)) (NP (DP (DT a)) (NX (NN town))))))))))))))))) (. .)))"
    sentence_data = ["1", (1,116), "John , Jessie , Jack and Julia should drive this car up to the garage which is located on the 5th avenue in a town .", tree_line, None, (), []]
    tokens_data = [("1-1", (1,4), "John", "John", "NNP"), 
                   ("1-2", (6,6), ",", ",", ","), 
                   ("1-3", (8,13), "Jessie", "Jessie", "NNP"), 
                   ("1-4", (15,15), ",", ",", ","),
                   ("1-5", (17,20), "Jack", "Jack", "NNP"),
                   ("1-6", (22,24), "and", "and", "CC"),
                   ("1-7", (26,30), "Julia", "Julia", "NNP"),
                   ("1-8", (32,37), "should", "shall", "MD"),
                   ("1-9", (39,43), "drive", "drive", "VB"),
                   ("1-10", (45,48), "this", "this", "DT"), 
                   ("1-11", (50,52), "car", "car", "NN"),
                   ("1-12", (54,55), "up", "up", "IN"),
                   ("1-13", (57,58), "to", "to", "TO"),
                   ("1-14", (60,62), "the", "the", "DT"),
                   ("1-15", (64,69), "garage", "garage", "NN"),
                   ("1-16", (71,75), "which", "which", "WDT"),
                   ("1-17", (77,78), "is", "be", "VBZ"),
                   ("1-18", (80,86), "located", "locate", "VBN"),
                   ("1-19", (88,89), "on", "on", "IN"),
                   ("1-20", (91,93), "the", "the", "DT"),
                   ("1-21", (95,97), "5th", "5th", "JJ"),
                   ("1-22", (99,104), "avenue", "avenue", "NN"),
                   ("1-23", (106,107), "in", "in", "IN"),
                   ("1-24", (109,109), "a", "a", "DT"),
                   ("1-25", (111,114), "town", "town", "NN"),
                   ("1-26", (116,116), ".", ".", ".")]
    mentions_data = [((1,4), (1,4), None, "John", NAME_GRAM_TYPE_TAG, NAME_GRAM_TYPE_SUBTYPE_TAGS["SHORT_NAME"], SINGULAR_NUMBER_TAG, MALE_GENDER_TAG, PERSON_NAMED_ENTITY_TYPE_TAG, UNKNOWN_VALUE_TAG, None),
                     ((8,13), (8,13), None, "Jessie", NAME_GRAM_TYPE_TAG, NAME_GRAM_TYPE_SUBTYPE_TAGS["SHORT_NAME"], SINGULAR_NUMBER_TAG, FEMALE_GENDER_TAG, PERSON_NAMED_ENTITY_TYPE_TAG, UNKNOWN_VALUE_TAG, None),
                     ((17,20), (17,20), None, "Jack", NAME_GRAM_TYPE_TAG, NAME_GRAM_TYPE_SUBTYPE_TAGS["SHORT_NAME"], SINGULAR_NUMBER_TAG, MALE_GENDER_TAG, PERSON_NAMED_ENTITY_TYPE_TAG, UNKNOWN_VALUE_TAG, None),
                     ((26,30), (26,30), None, "Julia", NAME_GRAM_TYPE_TAG, NAME_GRAM_TYPE_SUBTYPE_TAGS["SHORT_NAME"], SINGULAR_NUMBER_TAG, FEMALE_GENDER_TAG, PERSON_NAMED_ENTITY_TYPE_TAG, UNKNOWN_VALUE_TAG, None),
                     ((1,30), None, None, "John , Jessie , Jack and Julia", NOMINAL_GRAM_TYPE_TAG, None, PLURAL_NUMBER_TAG, NEUTRAL_GENDER_TAG, ORGANIZATION_NAMED_ENTITY_TYPE_TAG, UNKNOWN_VALUE_TAG, None), # FIXME: ask correct gram subtype to Pascal; and which head??? Is this because of such cases that there can be morethan one head?
                     ((45,52), (50,52), None, "this car", NOMINAL_GRAM_TYPE_TAG, None, SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, None, None), # FIXME: ask correct gram subtype to Pascal
                     ((60,69), (64,69), None, "the garage", NOMINAL_GRAM_TYPE_TAG, None, SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, None, None), # FIXME: ask correct gram subtype to Pascal
                     ((71,75), (71,75), None, "which", EXPANDED_PRONOUN_GRAM_TYPE_TAG, None, SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, None, None), # FIXME: it appears we do need an 'unknown' tag, right?
                     ((91,104), (99,104), None, "the 5th avenue", NOMINAL_GRAM_TYPE_TAG, None, SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, None, None), # FIXME: check with Pascal
                     ((109,114), (111, 114), None, "a town", NOMINAL_GRAM_TYPE_TAG, None, SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, None, None),
                     ((91,114), (99,104), None, "the 5th avenue in a town", NOMINAL_GRAM_TYPE_TAG, None, SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, None, None),
                     ((39,114), (39,43), None, "drive this car up to the garage which is located on the 5th avenue in a town", VERB_GRAM_TYPE_TAG, None, None, None, None, None, None)
                     ]
    sentence_data.append(tokens_data)
    sentence_data.append(mentions_data)
    sentences_data.append(sentence_data)

    sentences = []
    named_entities_list = []
    for sentence_data in sentences_data:
        (ident, extent, raw_text, constituency_tree_string, speaker, predicate_arguments, 
         named_entities_data, tokens_data, mentions_data) = sentence_data
        sentence = Sentence(ident, extent, document, raw_text=raw_text)
        
        # Mentions
        mentions = OrderedDict()
        for mention_data in mentions_data:
            (extent, head_extent, _, raw_text, gram_type, gram_subtype, number, gender, #entity_id
             ne_type, ne_subtype, referential_probability) = mention_data
            ident = "{e[0]},{e[1]}".format(e=extent)
            mention = Mention(ident, extent, document, raw_text=raw_text)
            mention.head_extent = head_extent
            mention.gram_type, mention.gram_subtype = gram_type, gram_subtype
            mention.number, mention.gender = number, gender
            if ne_type is not None:
                named_entity = NamedEntity(str(extent), extent, document, ne_type, raw_text=raw_text)
                named_entity.subtype = ne_subtype
                mention.named_entity = named_entity
            mention.referential_probability = referential_probability
            mentions[extent] =  mention
        mentions_list = list(mentions.values())
        
        # Tokens
        tokens = OrderedDict()
        for token_data in tokens_data:
            ident, extent, raw_text, lemma, POS_tag = token_data
            token = Token(ident, extent, document, raw_text=raw_text)
            token.lemma = lemma
            token.POS_tag = POS_tag
            tokens[extent] = token
        tokens_list = list(tokens.values())
        
        # Named entities
        named_entities = OrderedDict()
        for named_entity_data in named_entities_data:
            ident, extent, raw_text, type_, subtype = named_entity_data
            named_entity = NamedEntity(ident, extent, document, type_, raw_text=raw_text)
            named_entity.subtype = subtype
            named_entities[extent] = named_entity
        named_entities_list.extend(list(named_entities.values()))
        
        # ConstituencyTree
        sentence.constituency_tree = ConstituencyTree.parse(constituency_tree_string)
        
        # Speaker
        sentence.speaker = speaker
        
        # Predicate arguments
        sentence.predicate_arguments = predicate_arguments
        
        # Synchronization tokens <-> mentions
        for (ms, me), mention in mentions.items():
            mention.tokens = list(t for (ts, te), t in tokens.items() if ms <= ts and te <= me)
            #mention.synchronize_head_tokens()
        
        # Synchronization named_entities <-> mentions
        for extent, mention in mentions.items():
            mention.named_entity = named_entities.get(extent)
        
        # Synchronization tokens <-> constituency tree leaves
        assert len(sentence.constituency_tree.leaves) == len(tokens), "Arg."
        for i, node in enumerate(sentence.constituency_tree.leaves):
            token = tokens_list[i]
            node.token = token
            token.constituency_tree_node = node
        
        # Set sentence's data
        sentence.mentions = mentions_list
        sentence.tokens = tokens_list
        #sentence.named_entities = named_entities_list
        
        # Finally
        sentences.append(sentence)
    
    ## Finally, set the second_document's data
    document.tokens = list(token for sentence in sentences for token in sentence.tokens)
    document.mentions = list(mention for sentence in sentences for mention in sentence.mentions)
    document.sentences = sentences
    document.named_entities = named_entities_list
    
    return document



def get_test_mention_head_extent_data():
    # Expected result
    doc_ident = "test"
    raw_text = "\nIn June of 2004 , the Shanxi Yongji Peasants Association was formally "\
                "established ; it now has 3,800 members .\nAccordingly , this is the "\
                "first peasant organization to be legally registered and designated a "\
                "peasant association since the founding of the PRC ."
    document = Document(doc_ident, raw_text)
    
    ## First expected sentence
    sentences_data = []
    sentence_data = ["1", (1,110), "In June of 2004 , the Shanxi Yongji Peasants Association was formally established ; it now has 3,800 members .", 
                     "(TOP (S (S (PP (IN In) (NP (NP (NNP June)) (PP (IN of) (NP (CD 2004))))) (, ,) "\
                     "(NP (DT the) (NNP Shanxi) (NNP Yongji) (NNPS Peasants) (NNP Association)) "\
                     "(VP (VBD was) (ADVP (RB formally)) (VP (VBN established)))) (: ;) (S (NP "\
                     "(PRP it)) (ADVP (RB now)) (VP (VBZ has) (NP (CD 3,800) (NNS members)))) (. .)))"
                    ]
    sentence_tokens_data = [("1-1", (1,2), "In", "In", "IN"), 
                           ("1-2", (4,7), "June", "June", "NNP"), 
                           ("1-3", (9,10), "of", "of", "IN"), 
                           ("1-4", (12,15), "2004", "2004", "CD"),
                           ("1-5", (17,17), ",", ",", ","),
                           ("1-6", (19,21), "the", "the", "DT"),
                           ("1-7", (23,28), "Shanxi", "Shanxi", "NNP"),
                           ("1-8", (30,35), "Yongji", "Yongji", "NNP"),
                           ("1-9", (37,44), "Peasants", "Peasants", "NNPS"),
                           ("1-10", (46,56), "Association", "Association", "NNP"), 
                           ("1-11", (58,60), "was", "be", "VBD"),
                           ("1-12", (62,69), "formally", "formally", "RB"),
                           ("1-13", (71,81), "established", "establish", "VBN"),
                           ("1-14", (83,83), ";", ";", ":"),
                           ("1-15", (85,86), "it", "it", "PRP"),
                           ("1-16", (88,90), "now", "now", "RB"),
                           ("1-17", (92,94), "has", "have", "VBZ"),
                           ("1-18", (96,100), "3,800", "3,800", "CD"),
                           ("1-19", (102,108), "members", "members", "NNS"),
                           ("1-20", (110,110), ".", ".", ".")]
    sentence_mentions_data = [((19,56), (46,56), "the Shanxi Yongji Peasants Association", None, None, None, None, None, None, None), 
                              ((85,86), (85,86), "it", None, None, None, None, None, None, None)]
    sentence_data.append(sentence_tokens_data)
    sentence_data.append(sentence_mentions_data)
    sentences_data.append(sentence_data)
    
    ## Second expected sentence
    sentence_data = ["2", (112,257), "Accordingly , this is the first peasant organization to be legally registered and designated a peasant association since the founding of the PRC .", 
                       "(TOP (S (ADVP (RB Accordingly)) (, ,) (NP (DT this)) (VP (VBZ is) (NP (NP ("\
                       "DT the) (JJ first) (NN peasant) (NN organization)) (SBAR (S (VP (TO to) "\
                       "(VP (VB be) (ADVP (RB legally)) (VP (VP (VBN registered)) (CC and) (VP "\
                       "(VBN designated) (S (NP (DT a) (NN peasant) (NN association)))) (PP "\
                       "(IN since) (NP (NP (DT the) (NN founding)) (PP (IN of) (NP (DT the) "\
                       "(NNP PRC)))))))))))) (. .)))"
                    ]
    sentence_tokens_data = [("2-1", (112,122), "Accordingly", "Accordingly", "RB"),
                           ("2-2", (124,124), ",", ",", ","),
                           ("2-3", (126,129), "this", "this", "DT"),
                           ("2-4", (131,132), "is", "be", "VBZ"),
                           ("2-5", (134,136), "the", "the", "DT"),
                           ("2-6", (138,142), "first", "first", "JJ"),
                           ("2-7", (144,150), "peasant", "peasant", "NN"),
                           ("2-8", (152,163), "organization", "organization", "NN"),
                           ("2-9", (165,166), "to", "to", "TO"),
                           ("2-10", (168,169), "be", "be", "VB"),
                           ("2-11", (171,177), "legally", "legally", "RB"),
                           ("2-12", (179,188), "registered", "register", "VBN"),
                           ("2-13", (190,192), "and", "and", "CC"),
                           ("2-14", (194,203), "designated", "designate", "VBN"),
                           ("2-15", (205,205), "a", "a", "DT"),
                           ("2-16", (207,213), "peasant", "peasant", "NN"),
                           ("2-17", (215,225), "association", "association", "NN"),
                           ("2-18", (227,231), "since", "since", "IN"),
                           ("2-19", (233,235), "the", "the", "DT"),
                           ("2-20", (237,244), "founding", "found", "NN"),
                           ("2-21", (246,247), "of", "of", "IN"),
                           ("2-22", (249, 251), "the", "the", "DT"),
                           ("2-23", (253,255), "PRC", "PRC", "NNP"),
                           ("2-24", (257,257), ".", ".", ".")]
    sentence_mentions_data = [((126,129), (126,129), "this", None, None, None, None, None, None, None), 
                              ((249,255), (253,255), "the PRC", None, None, None, None, None, None, None)]
    sentence_data.append(sentence_tokens_data)
    sentence_data.append(sentence_mentions_data)
    sentences_data.append(sentence_data)
    
    ## Set the sentences' data
    sentences = []
    for sentence_data in sentences_data:
        (ident, extent, raw_text, constituency_tree_string, tokens_data, mentions_data) = sentence_data
        sentence = Sentence(ident, extent, document, raw_text=raw_text)
        
        # Mentions
        mentions = OrderedDict()
        for mention_data in mentions_data:
            (extent, expected_head_extent, raw_text, gram_type, gram_subtype, number, gender, 
             ne_type, ne_subtype, referential_probability) = mention_data
            ident = "{e[0]},{e[1]}".format(e=extent)
            mention = Mention(ident, extent, document, raw_text=raw_text)
            mention.head_extent = None #head_extent
            mention.gram_type, mention.gram_subtype = gram_type, gram_subtype
            mention.number, mention.gender = number, gender
            if ne_type is not None:
                named_entity = NamedEntity(str(extent), extent, document, ne_type, raw_text=raw_text)
                named_entity.subtype = ne_subtype
                mention.named_entity = named_entity
            mention.referential_probability = referential_probability
            mentions[extent] =  mention
        mentions_list = list(mentions.values())
        
        # Tokens
        tokens = OrderedDict()
        for token_data in tokens_data:
            ident, extent, raw_text, lemma, POS_tag = token_data
            token = Token(ident, extent, document, raw_text=raw_text)
            token.lemma = lemma
            token.POS_tag = POS_tag
            tokens[extent] = token
        tokens_list = list(tokens.values())
        
        # ConstituencyTree
        sentence.constituency_tree = ConstituencyTree.parse(constituency_tree_string)
        
        # Synchronization tokens <-> mentions
        for (ms, me), mention in mentions.items():
            mention.tokens = list(t for (ts, te), t in tokens.items() if ms <= ts and te <= me)
        
        # Synchronization tokens <-> constituency tree leaves
        assert len(sentence.constituency_tree.leaves) == len(tokens), "Arg."
        for i, node in enumerate(sentence.constituency_tree.leaves):
            token = tokens_list[i]
            node.token = token
            token.constituency_tree_node = node
        
        # Set sentence's data
        sentence.mentions = mentions_list
        sentence.tokens = tokens_list
        
        # Finally
        sentences.append(sentence)
    
    # Define the expected result
    test_fixture = []
    for i, sentence_data in enumerate(sentences_data):
        mentions_data = sentence_data[-1]
        for mention, mention_data in zip(sentences[i].mentions, mentions_data):
            expected_head_extent = mention_data[1]
            test_fixture.append((mention, expected_head_extent))
    
    return test_fixture


    
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()