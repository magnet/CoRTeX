# -*- coding: utf-8 -*-

import unittest
from cortex.api.document import Document
from cortex.api.markable import Token
from cortex.languages.english.token_features import EnglishTokenFeatures


doc_ident = "test" # Nathan ends at 80; "for him" ends at 164; "hers" begins at 208
raw_text = "\nI am great , but you are even better ! Wow , she can do that thing which Nathan "\
            "could not . What thing ? Well , a high jump kick . It seems doing it is out for him "\
            ", at least for now . 'Charlotte's skill , hers, truly , is really up to the task .' "\
            ", they said . But , see the thing , this , for yourself: a different task could be given ."
document = Document(doc_ident, raw_text)

english_token_features = EnglishTokenFeatures()


class TestEnglishTokenFeatures(unittest.TestCase):
    
    def test_is_pers_pro(self):
        # Test parameters
        method_name = "is_pers_pro"
        data = ((Token.from_dict({"ident": "1,1", "extent": (1,1), "document":document, "raw_text": "I", "POS_tag": "PRP"}), True), 
                (Token.from_dict({"ident": "3,4", "extent": (3,4), "document": document, "raw_text": "am", "POS_tag": "VBP"}), False)
               )
        method_to_test = getattr(english_token_features, method_name)
        
        # Test
        for token, expected_result in data:
            actual_result = method_to_test(token)
            self.assertEqual(expected_result, actual_result)
    
    def test_is_expand_poss_pro(self):
        # Test parameters
        method_name = "is_expand_poss_pro"
        data = ((Token.from_dict({"ident": "207,210", "extent": (207,210), "document":document, "raw_text": "hers", "POS_tag": "PRP$"}), True), 
                (Token.from_dict({"ident": "3,4", "extent": (3,4), "document": document, "raw_text": "am", "POS_tag": "VBZ"}), False)
               )
        method_to_test = getattr(english_token_features, method_name)
        
        # Test
        for token, expected_result in data:
            actual_result = method_to_test(token)
            self.assertEqual(expected_result, actual_result)
    
    def test_is_def_det(self):
        # Test parameters
        method_name = "is_def_det"
        data = ((Token.from_dict({"ident": "273,275", "extent": (273,275), "document":document, "raw_text": "the", "POS_tag": "DT"}), True), 
                (Token.from_dict({"ident": "307,307", "extent": (306,306), "document": document, "raw_text": "a", "POS_tag": "DT"}), False)
               )
        method_to_test = getattr(english_token_features, method_name)
        
        # Test
        for token, expected_result in data:
            actual_result = method_to_test(token)
            self.assertEqual(expected_result, actual_result)
    
    def test_is_indef_det(self):
        # Test parameters
        method_name = "is_indef_det"
        data = ((Token.from_dict({"ident": "273,275", "extent": (273,275), "document":document, "raw_text": "the", "POS_tag": "DT"}), False), 
                (Token.from_dict({"ident": "307,307", "extent": (306,306), "document": document, "raw_text": "a", "POS_tag": "DT"}), True)
               )
        method_to_test = getattr(english_token_features, method_name)
        
        # Test
        for token, expected_result in data:
            actual_result = method_to_test(token)
            self.assertEqual(expected_result, actual_result)
    
    def test_is_dem(self):
        # Test parameters
        method_name = "is_dem"
        data = ((Token.from_dict({"ident": "285,288", "extent": (285,288), "document":document, "raw_text": "this", "POS_tag": "PRP"}), True), 
                (Token.from_dict({"ident": "18,20", "extent": (18,20), "document": document, "raw_text": "you", "POS_tag": "PRP"}), False)
               )
        method_to_test = getattr(english_token_features, method_name)
        
        # Test
        for token, expected_result in data:
            actual_result = method_to_test(token)
            self.assertEqual(expected_result, actual_result)
    
    def test_common_noun(self):
        # Test parameters
        method_name = "is_common_noun"
        data = ((Token.from_dict({"ident": "62,66", "extent": (62,66), "document":document, "raw_text": "thing", "POS_tag": "NN"}), True), 
                (Token.from_dict({"ident": "74,79", "extent": (74,79), "document": document, "raw_text": "Nathan", "POS_tag": "NNP"}), False),
                (Token.from_dict({"ident": "256,259", "extent": (256,259), "document": document, "raw_text": "said", "POS_tag": "WBD"}), False)
               )
        method_to_test = getattr(english_token_features, method_name)
        
        # Test
        for token, expected_result in data:
            actual_result = method_to_test(token)
            self.assertEqual(expected_result, actual_result)
    
    def test_is_name(self):
        # Test parameters
        method_name = "is_name"
        data = ((Token.from_dict({"ident": "62,66", "extent": (62,66), "document":document, "raw_text": "thing", "POS_tag": "NN"}), False), 
                (Token.from_dict({"ident": "74,79", "extent": (74,79), "document": document, "raw_text": "Nathan", "POS_tag": "NNP"}), True),
                (Token.from_dict({"ident": "256,259", "extent": (256,259), "document": document, "raw_text": "said", "POS_tag": "WBD"}), False)
               )
        method_to_test = getattr(english_token_features, method_name)
        
        # Test
        for token, expected_result in data:
            actual_result = method_to_test(token)
            self.assertEqual(expected_result, actual_result)
    
    def test_is_noun(self):
        # Test parameters
        method_name = "is_noun"
        data = ((Token.from_dict({"ident": "62,66", "extent": (62,66), "document":document, "raw_text": "thing", "POS_tag": "NN"}), True), 
                (Token.from_dict({"ident": "74,79", "extent": (74,79), "document": document, "raw_text": "Nathan", "POS_tag": "NNP"}), True),
                (Token.from_dict({"ident": "256,259", "extent": (256,259), "document": document, "raw_text": "said", "POS_tag": "WBD"}), False)
               )
        method_to_test = getattr(english_token_features, method_name)
        
        # Test
        for token, expected_result in data:
            actual_result = method_to_test(token)
            self.assertEqual(expected_result, actual_result)
    
    def test_is_verb(self):
        # Test parameters
        method_name = "is_verb"
        data = ((Token.from_dict({"ident": "62,66", "extent": (62,66), "document":document, "raw_text": "thing", "POS_tag": "NN"}), False), 
                (Token.from_dict({"ident": "74,79", "extent": (74,79), "document": document, "raw_text": "Nathan", "POS_tag": "NNP"}), False),
                (Token.from_dict({"ident": "256,259", "extent": (256,259), "document": document, "raw_text": "said", "POS_tag": "VBD"}), True)
               )
        method_to_test = getattr(english_token_features, method_name)
        
        # Test
        for token, expected_result in data:
            actual_result = method_to_test(token)
            self.assertEqual(expected_result, actual_result)
    
    def test_is_start_quote(self):
        # Test parameters
        method_name = "is_start_quote"
        data = ((Token.from_dict({"ident": "186,186", "extent": (186,186), "document":document, "raw_text": "'", "POS_tag": "``"}), True), 
                (Token.from_dict({"ident": "247,247", "extent": (247,247), "document": document, "raw_text": "'", "POS_tag": "''"}), False),
                (Token.from_dict({"ident": "256,259", "extent": (256,259), "document": document, "raw_text": "said", "POS_tag": "WBD"}), False)
               )
        method_to_test = getattr(english_token_features, method_name)
        
        # Test
        for token, expected_result in data:
            actual_result = method_to_test(token)
            print(token)
            self.assertEqual(expected_result, actual_result)
        
    def test_is_end_quote(self):
        # Test parameters
        method_name = "is_end_quote"
        data = ((Token.from_dict({"ident": "186,186", "extent": (186,186), "document":document, "raw_text": "'", "POS_tag": "``"}), False), 
                (Token.from_dict({"ident": "247,247", "extent": (247,247), "document": document, "raw_text": "'", "POS_tag": "''"}), True),
                (Token.from_dict({"ident": "256,259", "extent": (256,259), "document": document, "raw_text": "said", "POS_tag": "WBD"}), False)
               )
        method_to_test = getattr(english_token_features, method_name)
        
        # Test
        for token, expected_result in data:
            actual_result = method_to_test(token)
            print(token)
            self.assertEqual(expected_result, actual_result)
    
    
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()