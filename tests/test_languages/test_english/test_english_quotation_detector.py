# -*- coding: utf-8 -*-

import unittest
from collections import OrderedDict

from cortex.api.document import Document
from cortex.api.markable import NamedEntity, Token, Mention, Sentence, Quotation
from cortex.api.entity import Entity
from cortex.api.coreference_partition import CoreferencePartition
from cortex.api.constituency_tree import ConstituencyTree
from cortex.languages.english.quotation_detector import EnglishQuotationDetector
from cortex.parameters.mention_data_tags import (UNKNOWN_VALUE_TAG, 
                                                  EXPANDED_PRONOUN_GRAM_TYPE_TAG, NAME_GRAM_TYPE_TAG, 
                                                  NOMINAL_GRAM_TYPE_TAG, VERB_GRAM_TYPE_TAG, 
                                                  EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS, 
                                                  NAME_GRAM_TYPE_SUBTYPE_TAGS, 
                                                  NOMINAL_GRAM_TYPE_SUBTYPE_TAGS, 
                                                  SINGULAR_NUMBER_TAG, PLURAL_NUMBER_TAG, 
                                                  MALE_GENDER_TAG, FEMALE_GENDER_TAG, 
                                                  NEUTRAL_GENDER_TAG, FIRST_PERSON_TAG, 
                                                  SECOND_PERSON_TAG, THIRD_PERSON_TAG)

from cortex.languages.english.parameters.ontonotes_v5_named_entities_data_tags import (PERSON_NAMED_ENTITY_TYPE_TAGS, 
                                                                                         ORGANIZATION_NAMED_ENTITY_TYPE_TAGS, 
                                                                                         LOCATION_NAMED_ENTITY_TYPE_TAGS,
                                                                                         ARTIFACT_NAMED_ENTITY_TYPE_TAGS)

PERSON_NAMED_ENTITY_TYPE_TAG = PERSON_NAMED_ENTITY_TYPE_TAGS[0]
ORGANIZATION_NAMED_ENTITY_TYPE_TAG = ORGANIZATION_NAMED_ENTITY_TYPE_TAGS[0] 
LOCATION_NAMED_ENTITY_TYPE_TAG = LOCATION_NAMED_ENTITY_TYPE_TAGS[0]
ARTIFACT_NAMED_ENTITY_TYPE_TAG = ARTIFACT_NAMED_ENTITY_TYPE_TAGS[0]


class TestEnglishDocumentCharacterizer(unittest.TestCase):
    
    #@unittest.skip
    def test_find_quotations(self):
        # Test parameters
        document, expected_document = get_test_find_quotations_data()
        english_quotation_detector = EnglishQuotationDetector()
        
        # Test
        actual_document = document
        expected__inner_eq_result = (False, "_quotations")
        actual__inner_eq_result = expected_document._inner_eq(actual_document)
        self.assertEqual(expected__inner_eq_result, actual__inner_eq_result)
        english_quotation_detector.detect_quotations(document)
        self.assertTrue(*expected_document._inner_eq(actual_document))

def _assemble_document_data(document, sentences_data, synchronize=True, 
                             find_and_synchronize_heads=False, flush_synchronize=False):
    expected_entities = {}
    sentences = []
    named_entities_list = []
    all_quotations = OrderedDict()
    for sentence_data in sentences_data:
        (ident, extent, raw_text, constituency_tree_string, speaker, predicate_arguments, 
         named_entities_data, tokens_data, mentions_data, quotations_data) = sentence_data
        sentence = Sentence(ident, extent, document, raw_text=raw_text)
        
        # Mentions
        mentions = OrderedDict()
        for mention_data in mentions_data:
            (extent, head_extent, entity_id, raw_text, gram_type, gram_subtype, number, gender, 
             referential_probability, wn_synonyms, wn_hypernyms) = mention_data
            ident = "{e[0]},{e[1]}".format(e=extent, he=head_extent)
            mention = Mention(ident, extent, document, raw_text=raw_text)
            mention.head_extent = head_extent
            mention.gram_type, mention.gram_subtype = gram_type, gram_subtype
            mention.number, mention.gender = number, gender
            mention.referential_probability = referential_probability
            mention.wn_synonyms = wn_synonyms
            mention.wn_hypernyms = wn_hypernyms
            if entity_id is not None:
                if entity_id not in expected_entities:
                    entity = Entity(ident=entity_id)
                    expected_entities[entity_id] = entity
                expected_entities[entity_id].add(mention.extent)
            mentions[extent] = mention
        mentions_list = list(mentions.values())
        
        # Tokens
        tokens = OrderedDict()
        for token_data in tokens_data:
            ident, extent, raw_text, lemma, POS_tag = token_data
            token = Token(ident, extent, document, raw_text=raw_text)
            token.lemma = lemma
            token.POS_tag = POS_tag
            tokens[extent] = token
        tokens_list = list(tokens.values())
        
        # Named entities
        named_entities = OrderedDict()
        for named_entity_data in named_entities_data:
            ident, extent, raw_text, type_, subtype, origin = named_entity_data
            named_entity = NamedEntity(ident, extent, document, type_, raw_text=raw_text, 
                                       subtype=subtype, origin=origin)
            named_entities[extent] = named_entity
        named_entities_list.extend(list(named_entities.values()))
        
        # Quotations
        quotations_data_ = OrderedDict()
        for quotation_data in quotations_data:
            extent, raw_text, speaker_mention_extent, speaker_verb_token_extent, interlocutor_mention_extent, mentions_extents = quotation_data
            ident = "{e[0]},{e[1]}".format(e=extent)
            quotation = Quotation(ident, extent, document, raw_text=raw_text, 
                                  speaker_mention_extent=speaker_mention_extent, 
                                  speaker_verb_token_extent=speaker_verb_token_extent, 
                                  interlocutor_mention_extent=interlocutor_mention_extent)
            quotations_data_[extent] = (quotation, mentions_extents)
        all_quotations.update((extent, q) for extent, (q, _) in quotations_data_.items())
        
        # ConstituencyTree
        sentence.constituency_tree = ConstituencyTree.parse(constituency_tree_string)
        
        # Speaker
        sentence.speaker = speaker
        
        # Predicate arguments
        sentence.predicate_arguments = predicate_arguments
        
        # Synchronize
        if synchronize:
            # Synchronization tokens <-> mentions
            for (ms, me), mention in mentions.items():
                mention.tokens = list(t for (ts, te), t in tokens.items() if ms <= ts and te <= me)
                if find_and_synchronize_heads:
                    mhs, mhe = mention.head_extent
                    mention.head_tokens = list(t for (ts, te), t in tokens.items() if mhs <= ts and te <= mhe)
            
            # Synchronization named_entities <-> mentions
            for extent, mention in mentions.items():
                if extent in named_entities:
                    mention.named_entity = named_entities.get(extent)
            
            # Synchronization named_entities <-> tokens
            for extent, token in tokens.items():
                token.named_entity = named_entities.get(extent)
            
            # Synchronization tokens <-> constituency tree leaves and sentence
            assert len(sentence.constituency_tree.leaves) == len(tokens), "Arg."
            for i, node in enumerate(sentence.constituency_tree.leaves):
                token = tokens_list[i]
                node.token = token
                token.constituency_tree_node = node
                token.sentence = sentence
            
            # Synchronization quotations
            for quotation, mentions_extents in quotations_data_.values():
                quotation_mentions = []
                if mentions_extents is not None:
                    for mention_extent in mentions_extents:
                        mention = mentions[mention_extent]
                        quotation_mentions.append(mention)
                    quotation.mentions = quotation_mentions
                if speaker_mention_extent is not None and speaker_mention_extent in mentions:
                    quotation.speaker_mention = mentions[speaker_mention_extent]
                if speaker_verb_token_extent is not None:
                    quotation.speaker_verb = tokens[speaker_verb_token_extent]
                if interlocutor_mention_extent is not None and interlocutor_mention_extent in mentions:
                    quotation.interlocutor_mention = mentions[interlocutor_mention_extent]
        
        elif flush_synchronize:
            # Synchronization tokens <-> constituency tree leaves
            assert len(sentence.constituency_tree.leaves) == len(tokens), "Arg."
            for i, node in enumerate(sentence.constituency_tree.leaves):
                token = tokens_list[i]
                node.token = token
                token.constituency_tree_node = node
        
        # Set sentence's data
        sentence.mentions = tuple(mentions_list)
        sentence.tokens = tuple(tokens_list)
        
        # Finally
        sentences.append(sentence)
    
    # Add coreference partition data
    coreference_partition = None
    if expected_entities:
        coreference_partition = CoreferencePartition(entities=expected_entities.values())
    
    ## Finally, set the test_document's data
    document.tokens = tuple(token for sentence in sentences for token in sentence.tokens)
    document.mentions = tuple(mention for sentence in sentences for mention in sentence.mentions)
    document.sentences = tuple(sentences)
    document.named_entities = tuple(sorted(named_entities_list))
    document.quotations = tuple(sorted(all_quotations.values()))
    document.coreference_partition = coreference_partition

def create_base_test_data():
    # Define document data
    raw_text = "\nI say to you '' This is good '' . You say to him '' That is bad '' . He says to me '' This is bad '' ."
    
    sentences_data = []
    expected_sentences_data = []
    
    ## First sentence
    sentence_data = ["1", (1,33), "I say to you '' This is good '' .", 
                     "(ROOT (S (NP (PRP I)) (VP (VBP say) (PP (TO to) (NP (PRP you) (`` `''))) (NP (NP (DT This)) (SBAR (S (VP (VBZ is) (ADJP (JJ good)))))) ('' '')) (. .)))",
                     "speaker#1", 
                     (((1, 1, "ARG0"), (3, 5, "V"), (10, 12, "ARG2"), (14, 31, "ARG1")),
                     )
                    ]
    expected_sentence_data = list(sentence_data)
    
    sentence_tokens_data = [("1-1", (1,1), "I", "I", "PRP"), 
                            ("1-2", (3,5), "say", "say", "VBP"), 
                            ("1-3", (7,8), "to", "to", "TO"), 
                            ("1-4", (10,12), "you", "you", "PRP"), 
                            ("1-5", (14,15), "''", "``", "``"), 
                            ("1-6", (17,20), "This", "this", "DT"), 
                            ("1-7", (22,23), "is", "be", "VBZ"), 
                            ("1-8", (25,28), "good", "good", "JJ"), 
                            ("1-9", (30,31), "''", "''", "''"), 
                            ("1-10", (33,33), ".", ".", "."), 
                            ]
    expected_sentence_tokens_data = list(sentence_tokens_data)
    
    named_entities_data = [("1,1", (1,1), "I", "PERS", UNKNOWN_VALUE_TAG, "test"),
                           ("10,12", (10,12), "you", "PERS", UNKNOWN_VALUE_TAG, "test"),
                           ]
    expected_named_entities_data = [("1,1", (1,1), "I", "PERS", UNKNOWN_VALUE_TAG, "test"),
                                    ("10,12", (10,12), "you", "PERS", UNKNOWN_VALUE_TAG, "test"),
                                    ]
    
    mentions_data = [((1,1), (1,1), 1, "I", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], SINGULAR_NUMBER_TAG, UNKNOWN_VALUE_TAG, None, [], []), 
                     ((10,12), (10,12), 2, "you", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], SINGULAR_NUMBER_TAG, UNKNOWN_VALUE_TAG, None, [], []), 
                     ((17,20), (17,20), 3, "This", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["DEM_PRO"], SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, [], []), 
                     ]
    expected_mentions_data = [((1,1), (1,1), 1, "I", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], SINGULAR_NUMBER_TAG, UNKNOWN_VALUE_TAG, None, [], []), 
                              ((10,12), (10,12), 2, "you", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], SINGULAR_NUMBER_TAG, UNKNOWN_VALUE_TAG, None, [], []), 
                              ((17,20), (17,20), 3, "This", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["DEM_PRO"], SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, [], []), 
                             ]
    
    quotations_data = []
    expected_quotations_data = [((14,31), "'' This is good ''", (1,1), (3,5), (10,12), ((17,20,),)),
                                ]
    
    sentence_data.append(named_entities_data)
    sentence_data.append(sentence_tokens_data)
    sentence_data.append(mentions_data)
    sentence_data.append(quotations_data)
    
    expected_sentence_data.append(expected_named_entities_data)
    expected_sentence_data.append(expected_sentence_tokens_data)
    expected_sentence_data.append(expected_mentions_data)
    expected_sentence_data.append(expected_quotations_data)
    
    sentences_data.append(sentence_data)
    expected_sentences_data.append(expected_sentence_data)
    
    ## Second sentence
    sentence_data = ["2", (35,68), "You say to him '' That is bad '' .", 
                     "(ROOT (S (NP (PRP You)) (VP (VBP say) (PP (TO to) (NP (PRP him) (`` ''))) (NP (NP (DT That)) (SBAR (S (VP (VBZ is) (ADJP (JJ bad)))))) ('' '')) (. .)))", 
                     "speaker#1", 
                     (((35, 37, "ARG0"), (39, 41, "V"), (46, 48, "ARG2"), (50, 66, "ARG1"), ),
                     )
                    ]
    expected_sentence_data = list(sentence_data)
    
    sentence_tokens_data = [("2-1", (35,37), "You", "you", "PRP"),
                            ("2-2", (39,41), "say", "say", "VBP"),
                            ("2-3", (43,44), "to", "to", "TO"),
                            ("2-4", (46,48), "him", "him", "PRP"),
                            ("2-5", (50,51), "''", "''", "``"),
                            ("2-6", (53,56), "That", "that", "DT"),
                            ("2-7", (58,59), "is", "be", "VBZ"),
                            ("2-8", (61,63), "bad", "bad", "JJ"),
                            ("2-9", (65,66), "''", "''", "''"),
                            ("2-10", (68,68), ".", ".", "."),
                           ]
    expected_sentence_tokens_data = list(sentence_tokens_data)
    
    named_entities_data = [("35,37", (35,37), "You", "PERS", UNKNOWN_VALUE_TAG, "test"),
                           ("46,48", (46,48), "him", "PERS", UNKNOWN_VALUE_TAG, "test"),
                          ]
    expected_named_entities_data = [("35,37", (35,37), "You", "PERS", UNKNOWN_VALUE_TAG, "test"),
                                    ("46,48", (46,48), "him", "PERS", UNKNOWN_VALUE_TAG, "test"),
                                    ]
    
    mentions_data = [((35,37), (35,37), 2, "You", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], SINGULAR_NUMBER_TAG, UNKNOWN_VALUE_TAG, None, [], []), 
                     ((46,48), (46,48), 4, "him", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], SINGULAR_NUMBER_TAG, MALE_GENDER_TAG, None, [], []), 
                     ((53,56), (53,56), 5, "That", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["DEM_PRO"], SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, [], []), 
                     ]
    expected_mentions_data = [((35,37), (35,37), 2, "You", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], SINGULAR_NUMBER_TAG, UNKNOWN_VALUE_TAG, None, [], []), 
                              ((46,48), (46,48), 4, "him", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], SINGULAR_NUMBER_TAG, MALE_GENDER_TAG, None, [], []), 
                              ((53,56), (53,56), 5, "That", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["DEM_PRO"], SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, [], []), 
                             ]
    
    quotations_data = []
    expected_quotations_data = [((50,66), "'' That is bad ''", (35,37), (39,41), (46,48), ((53,56),)),
                                ]
    
    sentence_data.append(named_entities_data)
    sentence_data.append(sentence_tokens_data)
    sentence_data.append(mentions_data)
    sentence_data.append(quotations_data)
    
    expected_sentence_data.append(expected_named_entities_data)
    expected_sentence_data.append(expected_sentence_tokens_data)
    expected_sentence_data.append(expected_mentions_data)
    expected_sentence_data.append(expected_quotations_data)
    
    sentences_data.append(sentence_data)
    expected_sentences_data.append(expected_sentence_data)
    
    
    ## Third sentence
    sentence_data = ["3", (70,102), "He says to me '' This is bad '' .", 
                     "(ROOT (S (NP (PRP He)) (VP (VBZ says) (PP (TO to) (NP (NP (PRP me)) (`` '') (SBAR (S (NP (DT This)) (VP (VBZ is) (ADJP (JJ bad) ('' '')))))))) (. .)))", 
                     "speaker#1", 
                     (((70, 71, "ARG0"), (73, 76, "V"), (84, 100, "ARG1"), ), #((70, 71, "ARG0"), (73, 76, "V"), (81, 82, "ARG2"), (87, 97, "ARG1"), ),
                     )
                    ]
    expected_sentence_data = list(sentence_data)
    
    sentence_tokens_data = [("3-1", (70,71), "He", "he", "PRP"),
                            ("3-2", (73,76), "says", "say", "VBZ"),
                            ("3-3", (78,79), "to", "to", "TO"),
                            ("3-4", (81,82), "me", "me", "PRP"),
                            ("3-5", (84,85), "''", "''", "``"),
                            ("3-6", (87,90), "This", "this", "DT"),
                            ("3-7", (92,93), "is", "be", "VBZ"),
                            ("3-8", (95,97), "bad", "bad", "JJ"),
                            ("3-9", (99,100), "''", "''", "''"),
                            ("3-10", (102,102), ".", ".", "."),
                           ]
    expected_sentence_tokens_data = list(sentence_tokens_data)
    
    named_entities_data = [("70,71", (70,71), "He", "PERS", UNKNOWN_VALUE_TAG, "test"),
                           ("81,82", (81,82), "me", "PERS", UNKNOWN_VALUE_TAG, "test"),
                          ]
    expected_named_entities_data = [("70,71", (70,71), "He", "PERS", UNKNOWN_VALUE_TAG, "test"),
                                    ("81,82", (81,82), "me", "PERS", UNKNOWN_VALUE_TAG, "test"),
                                    ]
    
    mentions_data = [((70,71), (70,71), 4, "He", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], SINGULAR_NUMBER_TAG, MALE_GENDER_TAG, None, [], []), 
                     ((81,82), (81,82), 1, "me", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], SINGULAR_NUMBER_TAG, UNKNOWN_VALUE_TAG, None, [], []), 
                     ((87,90), (87,90), 3, "This", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["DEM_PRO"], SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, [], []), 
                     ]
    expected_mentions_data = [((70,71), (70,71), 4, "He", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], SINGULAR_NUMBER_TAG, MALE_GENDER_TAG, None, [], []), 
                              ((81,82), (81,82), 1, "me", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], SINGULAR_NUMBER_TAG, UNKNOWN_VALUE_TAG, None, [], []), 
                              ((87,90), (87,90), 3, "This", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["DEM_PRO"], SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, [], []), 
                             ]
    
    quotations_data = []
    expected_quotations_data = [((84,100), "'' This is bad ''", (70,71), (73,76), None, ((87,90),)),
                                ]
    
    sentence_data.append(named_entities_data)
    sentence_data.append(sentence_tokens_data)
    sentence_data.append(mentions_data)
    sentence_data.append(quotations_data)
    
    expected_sentence_data.append(expected_named_entities_data)
    expected_sentence_data.append(expected_sentence_tokens_data)
    expected_sentence_data.append(expected_mentions_data)
    expected_sentence_data.append(expected_quotations_data)
    
    sentences_data.append(sentence_data)
    expected_sentences_data.append(expected_sentence_data)
    
    return raw_text, sentences_data, expected_sentences_data


def get_test_find_quotations_data():
    # Fetch document bas data
    raw_text, sentences_data, expected_sentences_data = create_base_test_data()
    
    # Create documents
    synchronize = True
    find_and_synchronize_heads = True
    test_document = Document("test_document", raw_text, info={"document_type": "test"})
    _assemble_document_data(test_document, sentences_data, synchronize=synchronize, 
                            find_and_synchronize_heads=find_and_synchronize_heads)
    
    expected_document = Document("expected_document", raw_text, info={"document_type": "test"})
    _assemble_document_data(expected_document, expected_sentences_data, synchronize=synchronize, 
                            find_and_synchronize_heads=find_and_synchronize_heads)
    
    return test_document, expected_document


"""
from pprint import pprint
            l1 = expected_document.sentences
            l2 = actual_document.sentences
            pprint(l1)
            pprint(l2)
            i = 1
            m1 = l1[i]
            m2 = l2[i]
            #attribute_name = "constituency_tree"
            #attr1 = getattr(m1, attribute_name).string_representation
            #attr2 = getattr(m2, attribute_name).string_representation
            #print(attr1)
            #print(attr2)
            #result3, message3 = attr1._inner_eq(attr2)
            #self.assertTrue(result3, message3)
            result2, message2 = m1._inner_eq(m2)
            self.assertTrue(result2, message2)
"""
    


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()