# -*- coding: utf-8 -*-

import unittest

from cortex.api.constituency_tree import ConstituencyTree
from cortex.languages.english.constituency_tree_node_head_finder import CollinsHeadFinder
from cortex.languages.english.constituency_tree_features import (EnglishConstituencyTreeNodeFeatures, 
                                                                   EnglishConstituencyTreeFeatures)

"""
Test data created with the online parser demo located here: http://www.nactem.ac.uk/enju/demo.html
John, Jessie, Jack and Julia should drive this car up to the garage which is located on the 5th avenue.
"""



##@unittest.skip
class TestEnglishConstituencyTreeNodeFeatures(unittest.TestCase):
    
    def setUp(self):
        self.constituency_tree_node_features = EnglishConstituencyTreeNodeFeatures()
    
    #@unittest.skip
    def test_is_NP(self):
        # Test parameter
        # "John , Jessie , Jack and Julia should drive this car up to the garage which is located on the 5th avenue ."
        tree_line = "(TOP (S (S (NP (NX-COOD (NX-COOD (NX-COOD (NX (NNP John)) (COOD (CONJP (, ,) "\
                    "(NX (NNP Jessie))))) (COOD (CONJP (, ,)) (NX (NNP Jack)))) (COOD (CONJP (CC and)) "\
                    "(NX (NNP Julia))))) (VP (VX (MD should))) (VP (VX (VB drive)) (NP (DP (DT this)) "\
                    "(NX (NX (NN car)) (PP (PP (IN up)) (PP (PX (TO to))) (NP (DP (DT the)) (NX (NX "\
                    "(NN garage)) (S-REL (NP-REL (WDT which)) (VP (VX (VBZ is)) (VP (VP (VBN located)) "\
                    "(PP (PX (IN on)) (NP (DP (DT the)) (NX (ADJP (JJ 5th)) (NX (NX (NN avenue)))))))))))))))) (. .)))"
        constituency_tree = ConstituencyTree.parse(tree_line)
        constituency_tree_node_features = self.constituency_tree_node_features
        data = [(constituency_tree.leaves[0].parent.parent.parent.parent.parent.parent, True), 
                (constituency_tree.leaves[0].parent.parent.parent.parent.parent, False)]
        # Test
        for node, expected_result in data:
            actual_result = constituency_tree_node_features.is_NP(node)
            self.assertEqual(expected_result, actual_result)    
    
    #@unittest.skip
    def test_get_dominating_NP_ancestor(self):
        # Test parameter
        tree_line = "(TOP (S (S (NP (NX-COOD (NX-COOD (NX-COOD (NX (NNP John)) (COOD (CONJP (, ,) "\
                    "(NX (NNP Jessie))))) (COOD (CONJP (, ,)) (NX (NNP Jack)))) (COOD (CONJP (CC and)) "\
                    "(NX (NNP Julia))))) (VP (VX (MD should))) (VP (VX (VB drive)) (NP (DP (DT this)) "\
                    "(NX (NX (NN car)) (PP (PP (IN up)) (PP (PX (TO to))) (NP (DP (DT the)) (NX (NX "\
                    "(NN garage)) (S-REL (NP-REL (WDT which)) (VP (VX (VBZ is)) (VP (VP (VBN located)) "\
                    "(PP (PX (IN on)) (NP (DP (DT the)) (NX (ADJP (JJ 5th)) (NX (NX (NN avenue)))))))))))))))) (. .)))"
        constituency_tree = ConstituencyTree.parse(tree_line)
        constituency_tree_node_features = self.constituency_tree_node_features
        node = constituency_tree.leaves[0]
        expected_result_node = constituency_tree.leaves[0].parent.parent.parent.parent.parent.parent
        data = [(constituency_tree.leaves[0], expected_result_node), 
                (expected_result_node, expected_result_node),
                (expected_result_node.parent, None)]
        # Test
        for node, expected_result in data:
            actual_result = constituency_tree_node_features.get_dominating_NP_ancestor(node)
            self.assertIs(expected_result, actual_result)
    
    #@unittest.skip
    def test_is_maximal_NP(self):
        # Test parameter
        tree_line = "(TOP (S (S (NP (NX-COOD (NX-COOD (NX-COOD (NX (NNP John)) (COOD (CONJP (, ,) "\
                    "(NX (NNP Jessie))))) (COOD (CONJP (, ,)) (NX (NNP Jack)))) (COOD (CONJP (CC and)) "\
                    "(NX (NNP Julia))))) (VP (VX (MD should))) (VP (VX (VB drive)) (NP (DP (DT this)) "\
                    "(NX (NX (NN car)) (PP (PP (IN up)) (PP (PX (TO to))) (NP (DP (DT the)) (NX (NX "\
                    "(NN garage)) (S-REL (NP-REL (WDT which)) (VP (VX (VBZ is)) (VP (VP (VBN located)) "\
                    "(PP (PX (IN on)) (NP (DP (DT the)) (NX (ADJP (JJ 5th)) (NX (NX (NN avenue)))))))))))))))) (. .)))"
        constituency_tree = ConstituencyTree.parse(tree_line)
        constituency_tree_node_features = self.constituency_tree_node_features
        data = [(constituency_tree.leaves[0].parent.parent.parent.parent.parent.parent, True), 
                (constituency_tree.leaves[0].parent.parent.parent.parent.parent, False),
                (constituency_tree.leaves[13].parent.parent.parent, False)]
        # Test
        for node, expected_result in data:
            actual_result = constituency_tree_node_features.is_maximal_NP(node)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_get_NP_enum(self):
        # Test parameter
        tree_line = "(TOP (S (S (NP (NNP John) (, ,) (NNP Jessie) (, ,) (NNP Jack) (CC and) (NNP Julia)) "\
                    "(VP (VX (MD should))) (VP (VX (VB drive)) (NP (DP (DT this)) (NX (NX (NN car)) "\
                    "(PP (PP (IN up)) (PP (PX (TO to))) (NP (DP (DT the)) (NX (NX (NN garage)) "\
                    "(S-REL (NP-REL (WDT which)) (VP (VX (VBZ is)) (VP (VP (VBN located)) (PP (PX "\
                    "(IN on)) (NP (DP (DT the)) (NX (ADJP (JJ 5th)) (NX (NX (NN avenue)))))))))))))))) (. .)))"
        tree_line2 = "(TOP (S (S (NP (NX-COOD (NX-COOD (NX-COOD (NX (NNP John)) (COOD (CONJP (, ,) "\
                    "(NX (NNP Jessie))))) (COOD (CONJP (, ,)) (NX (NNP Jack)))) (COOD (CONJP (CC and)) "\
                    "(NX (NNP Julia))))) (VP (VX (MD should))) (VP (VX (VB drive)) (NP (DP (DT this)) "\
                    "(NX (NX (NN car)) (PP (PP (IN up)) (PP (PX (TO to))) (NP (DP (DT the)) (NX (NX "\
                    "(NN garage)) (S-REL (NP-REL (WDT which)) (VP (VX (VBZ is)) (VP (VP (VBN located)) "\
                    "(PP (PX (IN on)) (NP (DP (DT the)) (NX (ADJP (JJ 5th)) (NX (NX (NN avenue)))))))))))))))) (. .)))"
        tree_line3 = "(TOP (S (NP (NNP Death) (CC and) (NNP Hades)) (VP (VBD gave) (PRT (RP up)) "\
                     "(NP (DT the) (JJ dead)) (SBAR (WHNP (WP who)) (S (VP (VBD were) (PP (IN in) "\
                     "(NP (PRP them))))))) (. .)))"
        tree_line4 = "(TOP (S (NP (NP (NNP Death)) (CC and) (NP (NNP Hades))) (VP (VBD gave) (PRT (RP up)) "\
                     "(NP (NP (DT the) (NN dead)) (SBAR (WHNP (WP who)) (S (VP (VBD were) (PP (IN in) "\
                     "(NP (PRP them))))))))(. .)))"
        tree_line5 = "(TOP (S (NP (NP (NP (DP (DT A)) (NX (NN car))) (PN (PN (, ,)) (NP (NX (NNP John)))))"\
                     " (PN (PN (, ,)) (NP-COOD (NP (DP (NP (NX (NNP Jane))) (DX (POS 's))) (NX (NN bike)))"\
                     " (COOD (CONJP (CC and)) (NP (DP (DT a)) (NX (ADJP (JJ huge)) (NX (NN train))))))))"\
                     " (VP (VX (VBD were)) (VP (VP (VBG going)) (PP (PX (IN in)) (NP (DP (DT the)) "\
                     "(NX (ADJP (JJ same)) (NX (NN direction))))))) (. .)))"
        tree_line6 = "(TOP (S (NP (PRP It)) (VP (VBZ 's) (NP (NP (DT the) (NN lack)) (PP (IN of) "\
                     "(NP (NP (JJ true) (NN culture)) (CC and) (NP (NP (NN connection)) (PP (IN to) "\
                     "(NP (NN anything))))))) (SBAR (WHNP (WDT that)) (S (ADVP (RB really)) "\
                     "(VP (VBZ fucks) (NP (NNS things)) (PRT (RP up)))))) (. .)))"
        constituency_tree = ConstituencyTree.parse(tree_line)
        constituency_tree2 = ConstituencyTree.parse(tree_line2)
        constituency_tree3 = ConstituencyTree.parse(tree_line3)
        constituency_tree4 = ConstituencyTree.parse(tree_line4)
        constituency_tree5 = ConstituencyTree.parse(tree_line5)
        #l5 = constituency_tree5.leaves
        constituency_tree6 = ConstituencyTree.parse(tree_line6)
        constituency_tree_node_features = self.constituency_tree_node_features
        data = [(constituency_tree.root.children[0].children[0].children[0], tuple((constituency_tree.leaves[i],) for i in (0,2,4,6))),
                (constituency_tree.root.children[0].children[0].children[2], None), # None
                (constituency_tree2.root.children[0].children[0].children[0], None), # tuple((constituency_tree.leaves[i],) for i in (0,2,4,6))
                (constituency_tree3.root.children[0].children[0], tuple((constituency_tree3.leaves[i],) for i in (0,2))),
                (constituency_tree4.root.children[0].children[0], tuple((constituency_tree4.leaves[i],) for i in (0,2))),
                (constituency_tree5.root.children[0].children[0], None), # tuple([(l5[0],l5[1]), (l5[3],), (l5[5],l5[6],l5[7]), (l5[9],l5[10],l5[11])])
                (constituency_tree6.root.children[0].children[1].children[1], None)
                ]
        
        # Test
        for node, expected_result in data:
            actual_result = constituency_tree_node_features.get_NP_enum(node)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_is_NP_enum(self):
        # Test parameter
        tree_line = "(TOP (S (S (NP (NNP John) (, ,) (NNP Jessie) (, ,) (NNP Jack) (CC and) (NNP Julia)) "\
                    "(VP (VX (MD should))) (VP (VX (VB drive)) (NP (DP (DT this)) (NX (NX (NN car)) "\
                    "(PP (PP (IN up)) (PP (PX (TO to))) (NP (DP (DT the)) (NX (NX (NN garage)) "\
                    "(S-REL (NP-REL (WDT which)) (VP (VX (VBZ is)) (VP (VP (VBN located)) (PP (PX "\
                    "(IN on)) (NP (DP (DT the)) (NX (ADJP (JJ 5th)) (NX (NX (NN avenue)))))))))))))))) (. .)))"
        tree_line2 = "(TOP (S (S (NP (NX-COOD (NX-COOD (NX-COOD (NX (NNP John)) (COOD (CONJP (, ,) "\
                    "(NX (NNP Jessie))))) (COOD (CONJP (, ,)) (NX (NNP Jack)))) (COOD (CONJP (CC and)) "\
                    "(NX (NNP Julia))))) (VP (VX (MD should))) (VP (VX (VB drive)) (NP (DP (DT this)) "\
                    "(NX (NX (NN car)) (PP (PP (IN up)) (PP (PX (TO to))) (NP (DP (DT the)) (NX (NX "\
                    "(NN garage)) (S-REL (NP-REL (WDT which)) (VP (VX (VBZ is)) (VP (VP (VBN located)) "\
                    "(PP (PX (IN on)) (NP (DP (DT the)) (NX (ADJP (JJ 5th)) (NX (NX (NN avenue)))))))))))))))) (. .)))"
        tree_line3 = "(TOP (S (NP (NNP Death) (CC and) (NNP Hades)) (VP (VBD gave) (PRT (RP up)) "\
                     "(NP (DT the) (JJ dead)) (SBAR (WHNP (WP who)) (S (VP (VBD were) (PP (IN in) "\
                     "(NP (PRP them))))))) (. .)))"
        tree_line4 = "(TOP (S (NP (NP (NNP Death)) (CC and) (NP (NNP Hades))) (VP (VBD gave) (PRT (RP up)) "\
                     "(NP (NP (DT the) (NN dead)) (SBAR (WHNP (WP who)) (S (VP (VBD were) (PP (IN in) "\
                     "(NP (PRP them))))))))(. .)))"
        tree_line6 = "(TOP (S (NP (PRP It)) (VP (VBZ 's) (NP (NP (DT the) (NN lack)) (PP (IN of) "\
                     "(NP (NP (JJ true) (NN culture)) (CC and) (NP (NP (NN connection)) (PP (IN to) "\
                     "(NP (NN anything))))))) (SBAR (WHNP (WDT that)) (S (ADVP (RB really)) "\
                     "(VP (VBZ fucks) (NP (NNS things)) (PRT (RP up)))))) (. .)))"
        constituency_tree = ConstituencyTree.parse(tree_line)
        constituency_tree2 = ConstituencyTree.parse(tree_line2)
        constituency_tree3 = ConstituencyTree.parse(tree_line3)
        constituency_tree4 = ConstituencyTree.parse(tree_line4)
        constituency_tree6 = ConstituencyTree.parse(tree_line6)
        constituency_tree_node_features = self.constituency_tree_node_features
        data = [(constituency_tree.root.children[0].children[0].children[0], True),
                (constituency_tree.root.children[0].children[0].children[2], False),# False
                (constituency_tree2.root.children[0].children[0].children[0], False),# True
                (constituency_tree3.root.children[0].children[0], True),
                (constituency_tree4.root.children[0].children[0], True),
                (constituency_tree6.root.children[0].children[1].children[1], False)
                ]
        # Test
        for node, expected_result in data:
            actual_result = constituency_tree_node_features.is_NP_enum(node)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_get_tags_leaves_boundaries(self):
        def _assert_fct(expected_result, actual_result):
            for i in range(max(len(expected_result), len(actual_result))):
                exp_lb, exp_hlb = expected_result[i]
                act_lb, act_hlb = actual_result[i]
                exp_lbl, exp_lbr = exp_lb
                exp_hlbl, exp_hlbr = exp_hlb
                act_lbl, act_lbr = act_lb
                act_hlbl, act_hlbr = act_hlb
                self.assertIs(exp_lbl, act_lbl)
                self.assertIs(exp_lbr, act_lbr)
                self.assertIs(exp_hlbl, act_hlbl)
                self.assertIs(exp_hlbr, act_hlbr)
        
        # With find_head = True
        ## Test parameter
        tree_line = "(TOP (S (NP (PRP$ Its) (NN president)) (VP (VBZ is) (NP (NNP Zheng) (NNP Bing))) (. .)))"
        constituency_tree = ConstituencyTree.parse(tree_line)
        constituency_tree_node_features = self.constituency_tree_node_features
        leaves = constituency_tree.leaves
        
        ## Expected result
        input_node = leaves[0].parent.parent
        tags = set(["NP", "NN"])
        find_head = True
        node_head_finder = CollinsHeadFinder()
        head_search_heuristic = "leaf"
        expected_result = []
        ### Input node
        head = node_head_finder.get_head(input_node, head_search_heuristic=head_search_heuristic)
        expected_result.append((input_node.get_leaves_boundaries(), head.get_leaves_boundaries()))
        ### Second child
        head = node_head_finder.get_head(input_node.children[1], head_search_heuristic=head_search_heuristic)
        expected_result.append((input_node.children[1].get_leaves_boundaries(), head.get_leaves_boundaries()))
        ### First child
        #head = node_head_finder.get_head(input_node.children[0], head_search_heuristic=head_search_heuristic)
        #expected_result.append((input_node.children[0].get_leaves_boundaries, head.get_leaves_boundaries()))
        ### Final child 2
        #head = node_head_finder.get_head(leaves[1], head_search_heuristic=head_search_heuristic)
        #expected_result.append((leaves[1].get_leaves_boundaries, head.get_leaves_boundaries()))
        ### Final child 1
        #head = node_head_finder.get_head(leaves[0], head_search_heuristic=head_search_heuristic)
        #expected_result.append((leaves[0].get_leaves_boundaries, head.get_leaves_boundaries()))
        
        ## Actual result
        actual_result = constituency_tree_node_features.get_tags_leaves_boundaries(input_node, tags, 
                                                                                  find_head=find_head, 
                                                                                  head_search_heuristic=head_search_heuristic)
        
        ## Test
        _assert_fct(expected_result, actual_result)
        
        
        # With find_head = False
        ## Expected result
        input_node = leaves[0].parent.parent
        tags = set(["NP", "NN"])
        find_head = False
        expected_result = []
        ### Input node
        expected_result.append((input_node.get_leaves_boundaries(), input_node.get_leaves_boundaries()))
        ### Second child
        expected_result.append((input_node.children[1].get_leaves_boundaries(), input_node.children[1].get_leaves_boundaries()))
        ### First child
        #expected_result.append((input_node.children[0].get_leaves_boundaries, input_node.children[0].get_leaves_boundaries()))
        ### Final child 2
        #expected_result.append((leaves[1].get_leaves_boundaries, leaves[0].get_leaves_boundaries()))
        ### Final child 1
        #expected_result.append((leaves[0].get_leaves_boundaries, leaves[0].get_leaves_boundaries()))
        
        ## Actual result
        actual_result = constituency_tree_node_features.get_tags_leaves_boundaries(input_node, tags, 
                                                                                  find_head=find_head, 
                                                                                  head_search_heuristic=head_search_heuristic)
        
        ## Test
        _assert_fct(expected_result, actual_result)



class TestEnglishConstituencyTreeFeatures(unittest.TestCase):
    
    def setUp(self):
        self.constituency_tree_features = EnglishConstituencyTreeFeatures()
    
    #@unittest.skip
    def test_get_tags_leaves_boundaries(self):
        def _assert_fct(expected_result, actual_result):
            for i in range(max(len(expected_result), len(actual_result))):
                exp_lb, exp_hlb = expected_result[i]
                act_lb, act_hlb = actual_result[i]
                exp_lbl, exp_lbr = exp_lb
                exp_hlbl, exp_hlbr = exp_hlb
                act_lbl, act_lbr = act_lb
                act_hlbl, act_hlbr = act_hlb
                self.assertIs(exp_lbl, act_lbl)
                self.assertIs(exp_lbr, act_lbr)
                self.assertIs(exp_hlbl, act_hlbl)
                self.assertIs(exp_hlbr, act_hlbr)
        
        # With find_head = True
        ## Test parameter
        tree_line = "(TOP (S (NP (PRP$ Its) (NN president)) (VP (VBZ is) (NP (NNP Zheng) (NNP Bing))) (. .)))"
        constituency_tree = ConstituencyTree.parse(tree_line)
        constituency_tree_features = self.constituency_tree_features
        leaves = constituency_tree.leaves
        
        ## Expected result
        tags = set(["NP", "NN"])
        find_head = True
        node_head_finder = CollinsHeadFinder()
        head_search_heuristic = "leaf"
        expected_result = []
        ### COD's NP
        node = leaves[-2].parent.parent
        head = node_head_finder.get_head(node, head_search_heuristic=head_search_heuristic)
        expected_result.append((node.get_leaves_boundaries(), head.get_leaves_boundaries()))
        ### Subject's NP
        node = leaves[0].parent.parent
        head = node_head_finder.get_head(node, head_search_heuristic=head_search_heuristic)
        expected_result.append((node.get_leaves_boundaries(), head.get_leaves_boundaries()))
        ### President's NP
        node = leaves[0].parent.parent.children[1]
        head = node_head_finder.get_head(node, head_search_heuristic=head_search_heuristic)
        expected_result.append((node.get_leaves_boundaries(), head.get_leaves_boundaries()))
        
        ## Actual result
        actual_result = constituency_tree_features.get_tags_leaves_boundaries(constituency_tree, tags, 
                                                                             find_head=find_head, 
                                                                             head_search_heuristic=head_search_heuristic)
        
        ## Test
        _assert_fct(expected_result, actual_result)
        
        # With find_head = False
        ## Expected result
        tags = set(["NP", "NN"])
        find_head = False
        expected_result = []
        ### COD's NP
        node = leaves[-2].parent.parent
        expected_result.append((node.get_leaves_boundaries(), node.get_leaves_boundaries()))
        ### Subject's NP
        node = leaves[0].parent.parent
        expected_result.append((node.get_leaves_boundaries(), node.get_leaves_boundaries()))
        ### President's NP
        node = leaves[0].parent.parent.children[1]
        expected_result.append((node.get_leaves_boundaries(), node.get_leaves_boundaries()))
        
        ## Actual result
        actual_result = constituency_tree_features.get_tags_leaves_boundaries(constituency_tree, tags, 
                                                                             find_head=find_head, 
                                                                             head_search_heuristic=head_search_heuristic)
        
        ## Test
        _assert_fct(expected_result, actual_result)
    


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()