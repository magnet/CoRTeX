# -*- coding: utf-8 -*-

import unittest
from collections import OrderedDict

from cortex.api.document import Document
from cortex.api.markable import Token, NamedEntity, Mention, Sentence
from cortex.api.constituency_tree import ConstituencyTree
from cortex.languages.english.constituency_tree_node_head_finder import CollinsHeadFinder



class TestCollinsHeadFinder(unittest.TestCase):
    
    @unittest.skip
    def test__get_head_leaf(self):
        # Test parameter
        tree_line = "(NP (DT the) (JJR older) (JJ elderly))"
        constituency_tree = ConstituencyTree.parse(tree_line)
        data = ((constituency_tree.root, constituency_tree.leaves[2]),
                )
        
        collins_head_finder = CollinsHeadFinder()
        
        # Test
        for test_node, expected_result in data:
            actual_result = collins_head_finder._get_head_leaf(test_node)
            self.assertIs(expected_result, actual_result)



def get_test_data():
    # Expected result
    doc_ident = "test"
    raw_text = "\nIn June of 2004 , the Shanxi Yongji Peasants Association was formally "\
                "established ; it now has 3,800 members .\nAccordingly , this is the "\
                "first peasant organization to be legally registered and designated a "\
                "peasant association since the founding of the PRC ."
    document = Document(doc_ident, raw_text)
    
    ## First expected sentence
    sentences_data = []
    sentence_data = ["1", (1,110), "In June of 2004 , the Shanxi Yongji Peasants Association was formally established ; it now has 3,800 members .", 
                     "(TOP (S (S (PP (IN In) (NP (NP (NNP June)) (PP (IN of) (NP (CD 2004))))) (, ,) "\
                     "(NP (DT the) (NNP Shanxi) (NNP Yongji) (NNPS Peasants) (NNP Association)) "\
                     "(VP (VBD was) (ADVP (RB formally)) (VP (VBN established)))) (: ;) (S (NP "\
                     "(PRP it)) (ADVP (RB now)) (VP (VBZ has) (NP (CD 3,800) (NNS members)))) (. .)))"
                    ]
    sentence_tokens_data = [("1-1", (1,2), "In", "In", "IN"), 
                           ("1-2", (4,7), "June", "June", "NNP"), 
                           ("1-3", (9,10), "of", "of", "IN"), 
                           ("1-4", (12,15), "2004", "2004", "CD"),
                           ("1-5", (17,17), ",", ",", ","),
                           ("1-6", (19,21), "the", "the", "DT"),
                           ("1-7", (23,28), "Shanxi", "Shanxi", "NNP"),
                           ("1-8", (30,35), "Yongji", "Yongji", "NNP"),
                           ("1-9", (37,44), "Peasants", "Peasants", "NNPS"),
                           ("1-10", (46,56), "Association", "Association", "NNP"), 
                           ("1-11", (58,60), "was", "be", "VBD"),
                           ("1-12", (62,69), "formally", "formally", "RB"),
                           ("1-13", (71,81), "established", "establish", "VBN"),
                           ("1-14", (83,83), ";", ";", ":"),
                           ("1-15", (85,86), "it", "it", "PRP"),
                           ("1-16", (88,90), "now", "now", "RB"),
                           ("1-17", (92,94), "has", "have", "VBZ"),
                           ("1-18", (96,100), "3,800", "3,800", "CD"),
                           ("1-19", (102,108), "members", "members", "NNS"),
                           ("1-20", (110,110), ".", ".", ".")]
    sentence_mentions_data = [((19,56), (46,56), "the Shanxi Yongji Peasants Association", None, None, None, None, None, None, None), 
                              ((85,86), (85,86), "it", None, None, None, None, None, None, None)]
    sentence_data.append(sentence_tokens_data)
    sentence_data.append(sentence_mentions_data)
    sentences_data.append(sentence_data)
    
    ## Second expected sentence
    sentence_data = ["2", (112,257), "Accordingly , this is the first peasant organization to be legally registered and designated a peasant association since the founding of the PRC .", 
                       "(TOP (S (ADVP (RB Accordingly)) (, ,) (NP (DT this)) (VP (VBZ is) (NP (NP ("\
                       "DT the) (JJ first) (NN peasant) (NN organization)) (SBAR (S (VP (TO to) "\
                       "(VP (VB be) (ADVP (RB legally)) (VP (VP (VBN registered)) (CC and) (VP "\
                       "(VBN designated) (S (NP (DT a) (NN peasant) (NN association)))) (PP "\
                       "(IN since) (NP (NP (DT the) (NN founding)) (PP (IN of) (NP (DT the) "\
                       "(NNP PRC)))))))))))) (. .)))"
                    ]
    sentence_tokens_data = [("2-1", (112,122), "Accordingly", "Accordingly", "RB"),
                           ("2-2", (124,124), ",", ",", ","),
                           ("2-3", (126,129), "this", "this", "DT"),
                           ("2-4", (131,132), "is", "be", "VBZ"),
                           ("2-5", (134,136), "the", "the", "DT"),
                           ("2-6", (138,142), "first", "first", "JJ"),
                           ("2-7", (144,150), "peasant", "peasant", "NN"),
                           ("2-8", (152,163), "organization", "organization", "NN"),
                           ("2-9", (165,166), "to", "to", "TO"),
                           ("2-10", (168,169), "be", "be", "VB"),
                           ("2-11", (171,177), "legally", "legally", "RB"),
                           ("2-12", (179,188), "registered", "register", "VBN"),
                           ("2-13", (190,192), "and", "and", "CC"),
                           ("2-14", (194,203), "designated", "designate", "VBN"),
                           ("2-15", (205,205), "a", "a", "DT"),
                           ("2-16", (207,213), "peasant", "peasant", "NN"),
                           ("2-17", (215,225), "association", "association", "NN"),
                           ("2-18", (227,231), "since", "since", "IN"),
                           ("2-19", (233,235), "the", "the", "DT"),
                           ("2-20", (237,244), "founding", "found", "NN"),
                           ("2-21", (246,247), "of", "of", "IN"),
                           ("2-22", (249, 251), "the", "the", "DT"),
                           ("2-23", (253,255), "PRC", "PRC", "NNP"),
                           ("2-24", (257,257), ".", ".", ".")]
    sentence_mentions_data = [((126,129), (126,129), "this", None, None, None, None, None, None, None), 
                              ((249,255), (253,255), "the PRC", None, None, None, None, None, None, None)]
    sentence_data.append(sentence_tokens_data)
    sentence_data.append(sentence_mentions_data)
    sentences_data.append(sentence_data)
    
    ## Set the sentences' data
    sentences = []
    for sentence_data in sentences_data:
        (ident, extent, raw_text, constituency_tree_string, tokens_data, mentions_data) = sentence_data
        sentence = Sentence(ident, extent, document, raw_text=raw_text)
        
        # Mentions
        mentions = OrderedDict()
        for mention_data in mentions_data:
            (extent, expected_head_extent, raw_text, gram_type, gram_subtype, number, gender, 
             ne_type, ne_subtype, referential_probability) = mention_data
            ident = "{e[0]},{e[1]}".format(e=extent)
            mention = Mention(ident, extent, document, raw_text=raw_text)
            mention.head_extent = None #head_extent
            mention.gram_type, mention.gram_subtype = gram_type, gram_subtype
            mention.number, mention.gender = number, gender
            if ne_type is not None:
                named_entity = NamedEntity(str(extent), extent, document, ne_type, raw_text=raw_text)
                named_entity.subtype = ne_subtype
                mention.named_entity = named_entity
            mention.referential_probability = referential_probability
            mentions[extent] =  mention
        mentions_list = list(mentions.values())
        
        # Tokens
        tokens = OrderedDict()
        for token_data in tokens_data:
            ident, extent, raw_text, lemma, POS_tag = token_data
            token = Token(ident, extent, document, raw_text=raw_text)
            token.lemma = lemma
            token.POS_tag = POS_tag
            tokens[extent] = token
        tokens_list = list(tokens.values())
        
        # ConstituencyTree
        sentence.constituency_tree = ConstituencyTree.parse(constituency_tree_string)
        
        # Synchronization tokens <-> mentions
        for (ms, me), mention in mentions.items():
            mention.tokens = list(t for (ts, te), t in tokens.items() if ms <= ts and te <= me)
        
        # Synchronization tokens <-> constituency tree leaves
        assert len(sentence.constituency_tree.leaves) == len(tokens), "Arg."
        for i, node in enumerate(sentence.constituency_tree.leaves):
            token = tokens_list[i]
            node.token = token
            token.constituency_tree_node = node
        
        # Set sentence's data
        sentence.mentions = mentions_list
        sentence.tokens = tokens_list
        
        # Finally
        sentences.append(sentence)
    
    # Define the expected result
    test_fixture = []
    for i, sentence_data in enumerate(sentences_data):
        mentions_data = sentence_data[-1]
        for mention, mention_data in zip(sentences[i].mentions, mentions_data):
            expected_head_extent = mention_data[1]
            test_fixture.append((mention, expected_head_extent))
    
    return test_fixture


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()