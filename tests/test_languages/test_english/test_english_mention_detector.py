# -*- coding: utf-8 -*-

import unittest
from collections import OrderedDict


from cortex.api.document import Document
from cortex.api.markable import Token, NamedEntity, Mention, Sentence
from cortex.api.constituency_tree import ConstituencyTree
from cortex.api.entity import Entity
from cortex.api.coreference_partition import CoreferencePartition

from cortex.languages.common.mention_detector import evaluate_mention_detection
from cortex.languages.english.mention_detector import EnglishMentionDetector
from cortex.parameters.mention_data_tags import (UNKNOWN_VALUE_TAG, 
                                                  EXPANDED_PRONOUN_GRAM_TYPE_TAG, NAME_GRAM_TYPE_TAG, 
                                                  NOMINAL_GRAM_TYPE_TAG, VERB_GRAM_TYPE_TAG, 
                                                  EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS, 
                                                  NAME_GRAM_TYPE_SUBTYPE_TAGS, 
                                                  NOMINAL_GRAM_TYPE_SUBTYPE_TAGS, 
                                                  SINGULAR_NUMBER_TAG, PLURAL_NUMBER_TAG, 
                                                  MALE_GENDER_TAG, FEMALE_GENDER_TAG, 
                                                  NEUTRAL_GENDER_TAG, FIRST_PERSON_TAG, 
                                                  SECOND_PERSON_TAG, THIRD_PERSON_TAG)

from cortex.languages.english.parameters.ontonotes_v5_named_entities_data_tags import (PERSON_NAMED_ENTITY_TYPE_TAGS, 
                                                                                         ORGANIZATION_NAMED_ENTITY_TYPE_TAGS, 
                                                                                         LOCATION_NAMED_ENTITY_TYPE_TAGS,
                                                                                         ARTIFACT_NAMED_ENTITY_TYPE_TAGS)
from cortex.api.document_buffer import deepcopy_document

PERSON_NAMED_ENTITY_TYPE_TAG = PERSON_NAMED_ENTITY_TYPE_TAGS[0]
ORGANIZATION_NAMED_ENTITY_TYPE_TAG = ORGANIZATION_NAMED_ENTITY_TYPE_TAGS[0] 
LOCATION_NAMED_ENTITY_TYPE_TAG = LOCATION_NAMED_ENTITY_TYPE_TAGS[0]
ARTIFACT_NAMED_ENTITY_TYPE_TAG = ARTIFACT_NAMED_ENTITY_TYPE_TAGS[0]


def _create_mention_collection(mention_extents, document):
    mentions = tuple(Mention(str(i), extent, document) for i, extent in enumerate(mention_extents))
    document.mentions = mentions

#@unittest.skip
class Test(unittest.TestCase):
    
    #@unittest.skip
    def test_evaluate_mention_detection(self):
        # Test parameters
        # mention extent: (4,15), (19,56), (85,86), (126,129), (249,255), (134,163), (205,225)
        raw_text = "\nIn June of 2004 , the Shanxi Yongji Peasants Association was formally "\
                    "established ; it now has 3,800 members . Accordingly , this is the first peasant "\
                    "organization to be legally registered and designated a peasant association since "\
                    "the founding of the PRC ."
        sys_document = Document("sys_test", raw_text)
        ref_document = Document("ref_test", raw_text)
        sys_mention_extents = ((19,56), (249,255), (134,163), (205,225))
        ref_mention_extents = ((4,15), (19,56), (85,86), (126,129), (134,163), (205,225))
        
        _create_mention_collection(sys_mention_extents, sys_document)
        _create_mention_collection(ref_mention_extents, ref_document)
        
        ref_mentions_nb = 6
        sys_mentions_nb = 4
        common_mentions_nb = 3
        recall = 0.5
        precision = 0.75
        f1 = 2*0.5*0.75 / (0.5+0.75)
        expected_result = {"recall": recall, "precision": precision, "f1": f1, 
                          "ref_mentions_nb": ref_mentions_nb, "sys_mentions_nb": sys_mentions_nb, 
                          "common_mentions_nb": common_mentions_nb}
        
        # Test
        actual_result = evaluate_mention_detection(sys_document, ref_document)
        self.assertEqual(expected_result, actual_result)


class TestEnglishMentionDetector(unittest.TestCase):
    
    #@unittest.skip
    def test_oracle_detect_mentions(self):
        # Test parameter
        document, expected_document = get_test_oracle_detect_mentions_data()
        english_mention_characterizer = EnglishMentionDetector()
        
        # Test
        actual_document = english_mention_characterizer.oracle_detect_mentions(document)
        # Test equality mention to mention
        self.assertEqual(False, actual_document is document)
        self.assertTrue(expected_document._inner_eq(actual_document))
    
    #@unittest.skip
    def test__detect_mentions(self):
        # Test data
        data = get_test__detect_mentions_data()
        english_mention_characterizer = EnglishMentionDetector()
        
        # Test
        for args, kwargs, expected_result in data:
            actual_result = english_mention_characterizer._detect_mentions(*args, **kwargs)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_detect_mentions(self):
        # Test data
        data = get_test_detect_mentions_data()
        english_mention_characterizer = EnglishMentionDetector()
        
        # Test
        for args, kwargs, (expected_document, expected_is_same_instance) in data:
            output_document = english_mention_characterizer.detect_mentions(*args, **kwargs)
            input_document = args[0]
            
            # Test equality mention to mention
            self.assertEqual(expected_document.mentions, output_document.mentions)
            # Test equality of coreference_partition
            self.assertEqual(expected_document.coreference_partition, output_document.coreference_partition)
            
            # Test identity of produced output
            actual_is_same_instance = output_document is input_document
            self.assertEqual(expected_is_same_instance, actual_is_same_instance)

def get_test_oracle_detect_mentions_data():
    data0 = get_test_data()
    document = data0[0][-1]
    expected_document = deepcopy_document(document)
    expected_document.coreference_partition = None
    return document, expected_document
    

def get_test__detect_mentions_data():
    data = []
    data0 = get_test_data()
    for document, kwargs, expected_mention_head_extent2mention_extent_detect_verbs, _ in data0: #, detect_verbs_expected_document
        args = (document,)
        expected_result = expected_mention_head_extent2mention_extent_detect_verbs
        data.append((args, kwargs, expected_result))
    return data
        
def get_test_detect_mentions_data():
    data = []
    for inplace in (False, True):
        data0 = get_test_data()
        document, kwargs, _, expected_document = data0[0] #expected_mention_head_extent2mention_extent_detect_verbs
        args = (document,)
        kwargs["inplace"] = inplace
        expected_is_same_instance = inplace
        expected_results = (expected_document, expected_is_same_instance)
        data.append((args, kwargs, expected_results))
    return data

def get_test_data():
    # Build document data
    raw_text = "\nIn June of 2004 , the Shanxi Yongji Peasants Association was formally "\
                        "established ; it now has 3,800 members .\nAccordingly , this is the "\
                        "first peasant organization to be legally registered and designated a "\
                        "peasant association since the founding of the PRC ."
    test_document = Document("test_document", raw_text)
    not_detect_verbs_expected_document = Document("not_detect_verbs_expected_document", raw_text)
    detect_verbs_expected_document = Document("detect_verbs_expected_document", raw_text)
    
    
    test_sentences_data = []
    expected_sentences_data_not_detect_verbs = []
    expected_sentences_data_detect_verbs = []
    expected_mention_head_extent2mention_extent_not_detect_verbs = {}
    expected_mention_head_extent2mention_extent_detect_verbs = {}
    
    ## First sentence
    test_sentence_data = ["1", (1,110), "In June of 2004 , the Shanxi Yongji Peasants Association was formally established ; it now has 3,800 members .", 
                     "(TOP (S (S (PP (IN In) (NP (NP (NNP June)) (PP (IN of) (NP (CD 2004))))) (, ,) (NP (DT the) (NNP Shanxi) (NNP Yongji) (NNPS Peasants) (NNP Association)) (VP (VBD was) (ADVP (RB formally)) (VP (VBN established)))) (: ;) (S (NP (PRP it)) (ADVP (RB now)) (VP (VBZ has) (NP (CD 3,800) (NNS members)))) (. .)))",
                     "speaker#1", 
                     (((58, 60, "V"),),
                      ((1, 15, "ARGM-TMP"), (19, 56, "ARG1"), (62, 69, "ARGM-MNR"), (71, 81, "V")),
                      ((85, 86, "ARG0"), (88, 90, "ARGM-TMP"), (92, 94, "V"), (96, 108, "ARG1"))
                     ),
                     []
                    ]
    expected_sentence_data_not_detect_verbs = ["1", (1,110), "In June of 2004 , the Shanxi Yongji Peasants Association was formally established ; it now has 3,800 members .", 
                             "(TOP (S (S (PP (IN In) (NP (NP (NNP June)) (PP (IN of) (NP (CD 2004))))) (, ,) (NP (DT the) (NNP Shanxi) (NNP Yongji) (NNPS Peasants) (NNP Association)) (VP (VBD was) (ADVP (RB formally)) (VP (VBN established)))) (: ;) (S (NP (PRP it)) (ADVP (RB now)) (VP (VBZ has) (NP (CD 3,800) (NNS members)))) (. .)))",
                             "speaker#1", 
                             (((58, 60, "V"),),
                              ((1, 15, "ARGM-TMP"), (19, 56, "ARG1"), (62, 69, "ARGM-MNR"), (71, 81, "V")),
                              ((85, 86, "ARG0"), (88, 90, "ARGM-TMP"), (92, 94, "V"), (96, 108, "ARG1"))
                             ),
                             []
                            ]
    expected_sentence_data_detect_verbs = list(expected_sentence_data_not_detect_verbs)
    
    sentence_tokens_data = [("1-1", (1,2), "In", "In", "IN"), 
                           ("1-2", (4,7), "June", "June", "NNP"), 
                           ("1-3", (9,10), "of", "of", "IN"), 
                           ("1-4", (12,15), "2004", "2004", "CD"),
                           ("1-5", (17,17), ",", ",", ","),
                           ("1-6", (19,21), "the", "the", "DT"),
                           ("1-7", (23,28), "Shanxi", "Shanxi", "NNP"),
                           ("1-8", (30,35), "Yongji", "Yongji", "NNP"),
                           ("1-9", (37,44), "Peasants", "Peasants", "NNPS"),
                           ("1-10", (46,56), "Association", "Association", "NNP"), 
                           ("1-11", (58,60), "was", "be", "VBD"),
                           ("1-12", (62,69), "formally", "formally", "RB"),
                           ("1-13", (71,81), "established", "establish", "VBN"),
                           ("1-14", (83,83), ";", ";", ":"),
                           ("1-15", (85,86), "it", "it", "PRP"),
                           ("1-16", (88,90), "now", "now", "RB"),
                           ("1-17", (92,94), "has", "have", "VBZ"),
                           ("1-18", (96,100), "3,800", "3,800", "CD"),
                           ("1-19", (102,108), "members", "members", "NNS"),
                           ("1-20", (110,110), ".", ".", ".")]
    
    test_sentence_mentions_data = [((4,15), None, 0, "June of 2004"),
                                  ((19,56), None, 1, "the Shanxi Yongji Peasants Association"), 
                                  ((85,86), None, 1, "it")
                                  ]
    expected_mentions_data_not_detect_verbs = [((4,15), None, None, "June of 2004"),
                                               #((4,7), None, None, "June"),
                                               ((12,15), None, None, "2004"),
                                               ((19,56), None, None, "the Shanxi Yongji Peasants Association"), 
                                               ((85,86), None, None, "it"),
                                               ((96,108), None, None, "3,800 members")
                                              ]
    expected_mention_head_extent2mention_extent_not_detect_verbs[(4,7)] = (4,15) # June of 2004
    #expected_mention_head_extent2mention_extent_not_detect_verbs[(4,7)] = (4,7) # June
    expected_mention_head_extent2mention_extent_not_detect_verbs[(12,15)] = (12,15) # 2004
    expected_mention_head_extent2mention_extent_not_detect_verbs[(46,56)] = (19,56) # the Shanxi Yongji Peasants Association
    expected_mention_head_extent2mention_extent_not_detect_verbs[(85,86)] = (85,86) # it
    expected_mention_head_extent2mention_extent_not_detect_verbs[(102,108)] = (96,108) # 3,800 members
    expected_mentions_data_detect_verbs = [((71,81), None, None, "established")]
    expected_mentions_data_detect_verbs.extend(expected_mentions_data_not_detect_verbs)
    expected_mention_head_extent2mention_extent_detect_verbs[(71,81)] = (71,81)
    
    test_sentence_data.append(sentence_tokens_data)
    expected_sentence_data_not_detect_verbs.append(sentence_tokens_data)
    expected_sentence_data_detect_verbs.append(sentence_tokens_data)
    
    test_sentence_data.append(test_sentence_mentions_data)
    expected_sentence_data_not_detect_verbs.append(expected_mentions_data_not_detect_verbs)
    expected_sentence_data_detect_verbs.append(expected_mentions_data_detect_verbs)
    
    test_sentences_data.append(test_sentence_data)
    expected_sentences_data_not_detect_verbs.append(expected_sentence_data_not_detect_verbs)
    expected_sentences_data_detect_verbs.append(expected_sentence_data_detect_verbs)
    
    
    ## Second sentence
    test_sentence_data = ["2", (112,257), "Accordingly , this is the first peasant organization to be legally registered and designated a peasant association since the founding of the PRC .", 
                       "(TOP (S (ADVP (RB Accordingly)) (, ,) (NP (DT this)) (VP (VBZ is) (NP (NP (DT the) (JJ first) (NN peasant) (NN organization)) (SBAR (S (VP (TO to) (VP (VB be) (ADVP (RB legally)) (VP (VP (VBN registered)) (CC and) (VP (VBN designated) (S (NP (DT a) (NN peasant) (NN association)))) (PP (IN since) (NP (NP (DT the) (NN founding)) (PP (IN of) (NP (DT the) (NNP PRC)))))))))))) (. .)))", 
                      "speaker#1", 
                      (((112, 122, "ARGM-DIS"), (126, 129, "ARG1"), (131, 132, "V"), (134, 255, "ARG2")),
                       ((134, 163, "ARG1"), (171, 177, "ARGM-MNR"), (179, 188, "V"), (227, 255, "ARGM-TMP")) ,
                       ((134, 163, "ARG1"), (171, 177, "ARGM-MNR"), (194, 203, "V"), (205, 225, "ARG2"), (227, 255, "ARGM-TMP")), 
                       ((168, 169, "V"),)           
                      ),
                      []
                    ]
    expected_sentence_data_not_detect_verbs = ["2", (112,257), "Accordingly , this is the first peasant organization to be legally registered and designated a peasant association since the founding of the PRC .", 
                           "(TOP (S (ADVP (RB Accordingly)) (, ,) (NP (DT this)) (VP (VBZ is) (NP (NP (DT the) (JJ first) (NN peasant) (NN organization)) (SBAR (S (VP (TO to) (VP (VB be) (ADVP (RB legally)) (VP (VP (VBN registered)) (CC and) (VP (VBN designated) (S (NP (DT a) (NN peasant) (NN association)))) (PP (IN since) (NP (NP (DT the) (NN founding)) (PP (IN of) (NP (DT the) (NNP PRC)))))))))))) (. .)))", 
                          "speaker#1", 
                          (((112, 122, "ARGM-DIS"), (126, 129, "ARG1"), (131, 132, "V"), (134, 255, "ARG2")),
                           ((134, 163, "ARG1"), (171, 177, "ARGM-MNR"), (179, 188, "V"), (227, 255, "ARGM-TMP")) ,
                           ((134, 163, "ARG1"), (171, 177, "ARGM-MNR"), (194, 203, "V"), (205, 225, "ARG2"), (227, 255, "ARGM-TMP")), 
                           ((168, 169, "V"),)           
                          ),
                          []
                        ]
    expected_sentence_data_detect_verbs = list(expected_sentence_data_not_detect_verbs)
    
    sentence_tokens_data = [("2-1", (112,122), "Accordingly", "Accordingly", "RB"),
                   ("2-2", (124,124), ",", ",", ","),
                   ("2-3", (126,129), "this", "this", "DT"),
                   ("2-4", (131,132), "is", "be", "VBZ"),
                   ("2-5", (134,136), "the", "the", "DT"),
                   ("2-6", (138,142), "first", "first", "JJ"),
                   ("2-7", (144,150), "peasant", "peasant", "NN"),
                   ("2-8", (152,163), "organization", "organization", "NN"),
                   ("2-9", (165,166), "to", "to", "TO"),
                   ("2-10", (168,169), "be", "be", "VB"),
                   ("2-11", (171,177), "legally", "legally", "RB"),
                   ("2-12", (179,188), "registered", "register", "VBN"),
                   ("2-13", (190,192), "and", "and", "CC"),
                   ("2-14", (194,203), "designated", "designate", "VBN"),
                   ("2-15", (205,205), "a", "a", "DT"),
                   ("2-16", (207,213), "peasant", "peasant", "NN"),
                   ("2-17", (215,225), "association", "association", "NN"),
                   ("2-18", (227,231), "since", "since", "IN"),
                   ("2-19", (233,235), "the", "the", "DT"),
                   ("2-20", (237,244), "founding", "found", "NN"),
                   ("2-21", (246,247), "of", "of", "IN"),
                   ("2-22", (249, 251), "the", "the", "DT"),
                   ("2-23", (253,255), "PRC", "PRC", "NNP"),
                   ("2-24", (257,257), ".", ".", ".")]
    
    test_sentence_mentions_data = [((126,129), None, 1, "this"),
                                   ((249,255), None, 2, "the PRC")
                                  ]
    expected_mentions_data_not_detect_verbs = [((126,129), None, None, "this"), 
                                               ((134,255), None, None, "the first peasant organization to be legally registered and designated a peasant association since the founding of the PRC"),
                                               #((134,163), None, None, "the first peasant organization"),
                                               ((205,225), None, None, "a peasant association"),
                                               ((233,255), None, None, "the founding of the PRC"),
                                               #((233,244), None, None, "the founding"),
                                               ((249,255), None, None, "the PRC")
                                              ]
    expected_mention_head_extent2mention_extent_not_detect_verbs[(126,129)] = (126,129) # this
    expected_mention_head_extent2mention_extent_not_detect_verbs[(152,163)] = (134,255) # the first peasant organization to be legally registered and designated a peasant association since the founding of the PRC
    #expected_mention_head_extent2mention_extent_not_detect_verbs[(152,163)] = (134,163) # the first peasant organization
    expected_mention_head_extent2mention_extent_not_detect_verbs[(215,225)] = (205,225) # a peasant association
    expected_mention_head_extent2mention_extent_not_detect_verbs[(237,244)] = (233,255) # the founding of the PRC
    #expected_mention_head_extent2mention_extent_not_detect_verbs[(237,244)] = (233,244) # the founding
    expected_mention_head_extent2mention_extent_not_detect_verbs[(253,255)] = (249,255) # the PRC
    expected_mentions_data_detect_verbs = []
    expected_mentions_data_detect_verbs.extend(expected_mentions_data_not_detect_verbs)
    
    test_sentence_data.append(sentence_tokens_data)
    expected_sentence_data_not_detect_verbs.append(sentence_tokens_data)
    expected_sentence_data_detect_verbs.append(sentence_tokens_data)
    
    test_sentence_data.append(test_sentence_mentions_data)
    expected_sentence_data_not_detect_verbs.append(expected_mentions_data_not_detect_verbs)
    expected_sentence_data_detect_verbs.append(expected_mentions_data_detect_verbs)
    
    test_sentences_data.append(test_sentence_data)
    expected_sentences_data_not_detect_verbs.append(expected_sentence_data_not_detect_verbs)
    expected_sentences_data_detect_verbs.append(expected_sentence_data_detect_verbs)
    
    expected_mention_head_extent2mention_extent_detect_verbs.update(expected_mention_head_extent2mention_extent_not_detect_verbs)
    
    ## Build the documents
    ### The test document
    _assemble_document_data(test_document, test_sentences_data, synchronize_heads=False)
    ### The 'not_detect_verbs' expected document
    _assemble_document_data(not_detect_verbs_expected_document, expected_sentences_data_not_detect_verbs, synchronize_heads=False)
    ### The 'detect_verbs' expected document
    _assemble_document_data(detect_verbs_expected_document, expected_sentences_data_detect_verbs, synchronize_heads=False)
    
    data = (#(test_document, {"detect_verbs": False}, expected_mention_head_extent2mention_extent_not_detect_verbs, not_detect_verbs_expected_document),
            (test_document, {"detect_verbs": True}, expected_mention_head_extent2mention_extent_detect_verbs, detect_verbs_expected_document),
            )
    
    return data



def _assemble_document_data(document, sentences_data, synchronize_heads=False):
    expected_entities = {}
    sentences = []
    named_entities_list = []
    for sentence_data in sentences_data:
        (ident, extent, raw_text, constituency_tree_string, speaker, predicate_arguments, 
         named_entities_data, tokens_data, mentions_data) = sentence_data
        sentence = Sentence(ident, extent, document, raw_text=raw_text)
        
        # Mentions
        mentions = OrderedDict()
        for mention_data in mentions_data:
            (extent, head_extent, entity_id, raw_text) = mention_data
            ident = "{e[0]},{e[1]}".format(e=extent)
            mention = Mention(ident, extent, document, raw_text=raw_text)
            mention.head_extent = head_extent
            if entity_id is not None:
                if entity_id not in expected_entities:
                    entity = Entity(ident=entity_id)
                    expected_entities[entity_id] = entity
                expected_entities[entity_id].add(mention.extent)
            mentions[extent] =  mention
        mentions_list = list(sorted(mentions.values()))
        
        # Tokens
        tokens = OrderedDict()
        for token_data in tokens_data:
            ident, extent, raw_text, lemma, POS_tag = token_data
            token = Token(ident, extent,document, raw_text=raw_text)
            token.lemma = lemma
            token.POS_tag = POS_tag
            tokens[extent] = token
        tokens_list = list(tokens.values())
        
        # Named entities
        named_entities = OrderedDict()
        for named_entity_data in named_entities_data:
            ident, extent, raw_text, type_, subtype = named_entity_data
            named_entity = NamedEntity(ident, extent, document, type_, raw_text=raw_text)
            named_entity.subtype = subtype
            named_entities[extent] = named_entity
        named_entities_list.extend(list(named_entities.values()))
        
        # ConstituencyTree
        sentence.constituency_tree = ConstituencyTree.parse(constituency_tree_string)
        
        # Speaker
        sentence.speaker = speaker
        
        # Predicate arguments
        sentence.predicate_arguments = predicate_arguments
        
        # Synchronization tokens <-> mentions
        for (ms, me), mention in mentions.items():
            mention.tokens = list(t for (ts, te), t in tokens.items() if ms <= ts and te <= me)
            if synchronize_heads:
                mhs, mhe = mention.head_extent
                mention.head_tokens = list(t for (ts, te), t in tokens.items() if mhs <= ts and te <= mhe)
        # Synchronization named_entities <-> mentions
        for extent, mention in mentions.items():
            if extent in named_entities:
                mention.named_entity = named_entities.get(extent)
        
        # Synchronization named_entities <-> tokens
        for extent, token in tokens.items():
            token.named_entity = named_entities.get(extent)
        
        # Synchronization tokens <-> constituency tree leaves
        assert len(sentence.constituency_tree.leaves) == len(tokens), "Arg."
        for i, node in enumerate(sentence.constituency_tree.leaves):
            token = tokens_list[i]
            node.token = token
            token.constituency_tree_node = node
        
        # Set sentence's data
        sentence.mentions = mentions_list
        sentence.tokens = tokens_list
        
        # Finally
        sentences.append(sentence)
    
    ## Finally, set the test_document's data
    document.tokens = list(token for sentence in sentences for token in sentence.tokens)
    document.mentions = list(mention for sentence in sentences for mention in sentence.mentions)
    document.sentences = sentences
    document.named_entities = sorted(named_entities_list)
    if expected_entities:
        document.coreference_partition = CoreferencePartition(entities=expected_entities.values())


"""
print(m1)
print(m2)
result2, message2 = m1._inner_eq(m2)
if message2:
    attr1 = getattr(m1, message2)
    attr2 = getattr(m2, message2)
    print(attr1)
    print(attr2)
    print(m1.named_entity)
self.assertTrue(result2, message2)
"""


'''
print(expected_document.ident)
print(document.ident)
l1 = expected_document.mentions
l2 = document.mentions
from pprint import pprint
pprint(l1)
pprint(l2)
i = 0
m1 = l1[i]
m2 = l2[i]
print(m1)
print(m2)
attribute_name = "head_extent"
attr1 = getattr(m1, attribute_name)
attr2 = getattr(m2, attribute_name)
print(attr1)
print(attr2)
#result3, message3 = attr1._inner_eq(attr2)
#self.assertTrue(result3, message3)
result2, message2 = m1._inner_eq(m2)
self.assertTrue(result2, message2)
'''



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()