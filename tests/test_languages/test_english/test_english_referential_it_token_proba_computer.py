# -*- coding: utf-8 -*-

import unittest
import pytest
from collections import OrderedDict

from cortex.api.document import Document
from cortex.api.markable import NamedEntity, Token, Mention, Sentence
from cortex.api.entity import Entity
from cortex.api.coreference_partition import CoreferencePartition
from cortex.api.constituency_tree import ConstituencyTree
from cortex.languages.english.referential_it_token_proba_computer import ReferentialEnglishItTokenProbaComputer

from cortex.languages.english.parameters.ontonotes_v5_named_entities_data_tags import (PERSON_NAMED_ENTITY_TYPE_TAGS, 
                                                                                         ORGANIZATION_NAMED_ENTITY_TYPE_TAGS, 
                                                                                         LOCATION_NAMED_ENTITY_TYPE_TAGS,
                                                                                         ARTIFACT_NAMED_ENTITY_TYPE_TAGS)

PERSON_NAMED_ENTITY_TYPE_TAG = PERSON_NAMED_ENTITY_TYPE_TAGS[0]
ORGANIZATION_NAMED_ENTITY_TYPE_TAG = ORGANIZATION_NAMED_ENTITY_TYPE_TAGS[0] 
LOCATION_NAMED_ENTITY_TYPE_TAG = LOCATION_NAMED_ENTITY_TYPE_TAGS[0]
ARTIFACT_NAMED_ENTITY_TYPE_TAG = ARTIFACT_NAMED_ENTITY_TYPE_TAGS[0]

@pytest.mark.need_nada
class TestReferentialEnglishItTokenProbaComputer(unittest.TestCase):
    
    #@unittest.skip
    def test_compute_referential_proba(self):
        # Test parameters
        data = get_data()
        referential_it_token_proba_computer = ReferentialEnglishItTokenProbaComputer()
        documents, expected_result = tuple(zip(*data))
        
        # Test
        actual_result = referential_it_token_proba_computer.compute_referential_proba(documents)
        self.assertEqual(expected_result, actual_result)



def get_data():
    data = []
    
    # Document 1
    # Build document data
    raw_text1 = "\nIn June of 2004 , the Shanxi Yongji Peasants Association was formally "\
                "established ; it now has 3,800 members . Accordingly , this is the first peasant "\
                "organization to be legally registered and designated a peasant association since "\
                "the founding of the PRC ."
    expected_mentions_data1 = []
    expected_sentences_data = []
    
    ## First sentence
    expected_sentence_data = ["1", (1,110), "In June of 2004 , the Shanxi Yongji Peasants Association was formally established ; it now has 3,800 members .", 
                             "(ROOT (S (S (PP (IN In) (NP (NP (NNP June)) (PP (IN of) (NP (CD 2004))))) (, ,) (NP (DT the) (NNP Shanxi) (NNP Yongji) (NNPS Peasants) (NNP Association)) (VP (VBD was) (VP (ADVP (RB formally)) (VBN established)))) (: ;) (S (NP (PRP it)) (ADVP (RB now)) (VP (VBZ has) (NP (CD 3,800) (NNS members)))) (. .)))",
                             tuple()
                            ]
    sentence_tokens_data = [("1-1", (1,2), "In", "In", "IN"), 
                           ("1-2", (4,7), "June", "June", "NNP"), 
                           ("1-3", (9,10), "of", "of", "IN"), 
                           ("1-4", (12,15), "2004", "2004", "CD"),
                           ("1-5", (17,17), ",", ",", ","),
                           ("1-6", (19,21), "the", "the", "DT"),
                           ("1-7", (23,28), "Shanxi", "Shanxi", "NNP"),
                           ("1-8", (30,35), "Yongji", "Yongji", "NNP"),
                           ("1-9", (37,44), "Peasants", "Peasants", "NNPS"),
                           ("1-10", (46,56), "Association", "Association", "NNP"), 
                           ("1-11", (58,60), "was", "be", "VBD"),
                           ("1-12", (62,69), "formally", "formally", "RB"),
                           ("1-13", (71,81), "established", "establish", "VBN"),
                           ("1-14", (83,83), ";", ";", ":"),
                           ("1-15", (85,86), "it", "it", "PRP"),
                           ("1-16", (88,90), "now", "now", "RB"),
                           ("1-17", (92,94), "has", "have", "VBZ"),
                           ("1-18", (96,100), "3,800", "3,800", "CD"),
                           ("1-19", (102,108), "members", "members", "NNS"),
                           ("1-20", (110,110), ".", ".", ".")]
    
    expected_sentence_mentions_data1 = tuple()
    
    expected_sentence_data.append(sentence_tokens_data)
    expected_mentions_data1.extend(expected_sentence_mentions_data1)
    expected_sentences_data.append(expected_sentence_data)
    
    
    ## Second sentence
    expected_sentence_data = ["2", (112,257), "Accordingly , this is the first peasant organization to be legally registered and designated a peasant association since the founding of the PRC .", 
                               "(ROOT (S (ADVP (RB Accordingly)) (, ,) (NP (DT this)) (VP (VBZ is) (NP (DT the) (JJ first) (NN peasant) (NN organization) (S (VP (TO to) (VP (VB be) (VP (ADVP (RB legally)) (VBN registered) (CC and) (VBN designated) (NP (NP (DT a) (NN peasant) (NN association)) (PP (IN since) (NP (NP (DT the) (NN founding)) (PP (IN of) (NP (DT the) (NNP PRC)))))))))))) (. .)))", 
                              tuple()
                            ]
    
    sentence_tokens_data = [("2-1", (112,122), "Accordingly", "Accordingly", "RB"),
                           ("2-2", (124,124), ",", ",", ","),
                           ("2-3", (126,129), "this", "this", "DT"),
                           ("2-4", (131,132), "is", "be", "VBZ"),
                           ("2-5", (134,136), "the", "the", "DT"),
                           ("2-6", (138,142), "first", "first", "JJ"),
                           ("2-7", (144,150), "peasant", "peasant", "NN"),
                           ("2-8", (152,163), "organization", "organization", "NN"),
                           ("2-9", (165,166), "to", "to", "TO"),
                           ("2-10", (168,169), "be", "be", "VB"),
                           ("2-11", (171,177), "legally", "legally", "RB"),
                           ("2-12", (179,188), "registered", "register", "VBN"),
                           ("2-13", (190,192), "and", "and", "CC"),
                           ("2-14", (194,203), "designated", "designate", "VBN"),
                           ("2-15", (205,205), "a", "a", "DT"),
                           ("2-16", (207,213), "peasant", "peasant", "NN"),
                           ("2-17", (215,225), "association", "association", "NN"),
                           ("2-18", (227,231), "since", "since", "IN"),
                           ("2-19", (233,235), "the", "the", "DT"),
                           ("2-20", (237,244), "founding", "found", "NN"),
                           ("2-21", (246,247), "of", "of", "IN"),
                           ("2-22", (249, 251), "the", "the", "DT"),
                           ("2-23", (253,255), "PRC", "PRC", "NNP"),
                           ("2-24", (257,257), ".", ".", ".")]
    
    expected_sentence_mentions_data1 = tuple()
    expected_sentence_data.append(sentence_tokens_data)
    expected_mentions_data1.extend(expected_sentence_mentions_data1)
    expected_sentences_data.append(expected_sentence_data)
    
    # Build the document
    test_document = Document("expected_document1", raw_text1)
    _assemble_document_data(test_document, expected_sentences_data, expected_mentions_data1)
    
    expected_result = {(85,86): 1 - float("0.991")}
    
    data.append((test_document, expected_result))
    
    
    # Document 2
    # Build document data
    raw_text1 = "\nIt rains ."
    expected_mentions_data1 = []
    expected_sentences_data = []
    
    ## First sentence
    expected_sentence_data = ["1", (1,10), "It rains .", 
                             "(ROOT (S (NP (PRP it)) (VP (VBZ rains)) (. .)))",
                             tuple()
                            ]
    sentence_tokens_data = [("1-1", (1,2), "It", "it", "PRP"),
                            ("1-2", (4,8), "rains", "rain", "VBZ"),
                            ("1-3", (10,10), ".", ".", ".")]
    
    expected_sentence_mentions_data1 = tuple()
    
    expected_sentence_data.append(sentence_tokens_data)
    expected_mentions_data1.extend(expected_sentence_mentions_data1)
    expected_sentences_data.append(expected_sentence_data)
    
    # Build the document
    test_document = Document("expected_document1", raw_text1)
    _assemble_document_data(test_document, expected_sentences_data, expected_mentions_data1)
    
    expected_result = {(1,2): 1 - float("0.895")}
    
    data.append((test_document, expected_result))
    
    return data


def _assemble_document_data(document, sentences_data, mentions_data):
    if sentences_data:
        sentences = []
        full_named_entities = OrderedDict()
        for sentence_data in sentences_data:
            (ident, extent, raw_text, constituency_tree_string, named_entities_data, 
             tokens_data) = sentence_data
            sentence = Sentence(ident, extent, document, raw_text=raw_text)
            
            # Tokens
            tokens = OrderedDict()
            for token_data in tokens_data:
                ident, extent, raw_text, lemma, POS_tag = token_data
                token = Token(ident, extent,document, raw_text=raw_text)
                token.lemma = lemma
                token.POS_tag = POS_tag
                tokens[extent] = token
            tokens_list = list(tokens.values())
            
            # Named entities
            named_entities = OrderedDict()
            for named_entity_data in named_entities_data:
                ident, extent, raw_text, type_, subtype = named_entity_data
                named_entity = NamedEntity(ident, extent, document, type_, raw_text=raw_text)
                named_entity.subtype = subtype
                named_entities[extent] = named_entity
            full_named_entities.update(named_entities)
            
            # ConstituencyTree
            sentence.constituency_tree = ConstituencyTree.parse(constituency_tree_string)
            
            # Synchronization named_entities <-> tokens
            for extent, token in tokens.items():
                token.named_entity = named_entities.get(extent)
            
            # Synchronization tokens <-> constituency tree leaves
            assert len(sentence.constituency_tree.leaves) == len(tokens), "Arg."
            for i, node in enumerate(sentence.constituency_tree.leaves):
                token = tokens_list[i]
                node.token = token
                token.constituency_tree_node = node
            
            # Set sentence's data
            sentence.tokens = tokens_list
            
            # Finally
            sentences.append(sentence)
        
        ## Finally, set the test_document's data
        document.tokens = list(token for sentence in sentences for token in sentence.tokens)
        document.sentences = sentences
        document.named_entities = list(full_named_entities.values())
    
    # Mentions
    expected_entities_data = {}
    if mentions_data:
        mentions = OrderedDict()
        for mention_data in mentions_data:
            (extent, head_extent, entity_id, raw_text, gram_type, gram_subtype, number, gender, 
             referential_probability, wn_synonyms, wn_hypernyms) = mention_data
            ident = "{e[0]},{e[1]}".format(e=extent, he=head_extent)
            mention = Mention(ident, extent, document, raw_text=raw_text)
            mention.head_extent = head_extent
            mention.gram_type, mention.gram_subtype = gram_type, gram_subtype
            mention.number, mention.gender = number, gender
            mention.referential_probability = referential_probability
            mention.wn_synonyms = wn_synonyms
            mention.wn_hypernyms = wn_hypernyms
            if entity_id is not None:
                if entity_id not in expected_entities_data:
                    entity = Entity(ident=entity_id)
                    expected_entities_data[entity_id] = entity
                expected_entities_data[entity_id].add(mention.ident)
            mentions[extent] =  mention
        mentions_list = list(mentions.values())
        
        ## Finally, set the test_document's data
        document.mentions = mentions_list
    
    if sentences_data and mentions_data:
        # Synchronize mentions' data with sentences' if possible and asked for
        for sentence in document.sentences:
            ss, se = sentence.extent
            tokens = OrderedDict((t.extent, t) for t in sentence.tokens)
            mentions_list = [m for m in mentions.values() if ss <= m.start and m.end <= se]
            
            ## Synchronization tokens <-> mentions
            for mention in mentions_list:
                ms, me = mention.extent
                mention.tokens = list(t for (ts, te), t in tokens.items() if ms <= ts and te <= me)
                if mention.head_extent:
                    mhs, mhe = mention.head_extent
                    mention.head_tokens = list(t for (ts, te), t in tokens.items() if mhs <= ts and te <= mhe)
            
            ## Synchronization named_entities <-> mentions
            for mention in mentions_list:
                extent = mention.extent
                mention.named_entity = full_named_entities.get(extent)
            
            sentence.mentions = mentions_list
    
    # Create the entities only when we are sure that the mention's data will not change annymore
    if expected_entities_data:
        expected_entities = {}
        for entity_ident, a_set in expected_entities_data.items():
            if entity_ident not in expected_entities:
                entity = Entity(ident=entity_ident)
                expected_entities[entity_ident] = entity
            for ident in a_set:
                expected_entities[entity_ident].add(document.ident2mention[ident])
        document.coreference_partition = CoreferencePartition(entities=expected_entities.values())


"""
from pprint import pprint
            l1 = expected_document.sentences
            l2 = actual_document.sentences
            pprint(l1)
            pprint(l2)
            i = 1
            m1 = l1[i]
            m2 = l2[i]
            #attribute_name = "constituency_tree"
            #attr1 = getattr(m1, attribute_name).string_representation
            #attr2 = getattr(m2, attribute_name).string_representation
            #print(attr1)
            #print(attr2)
            #result3, message3 = attr1._inner_eq(attr2)
            #self.assertTrue(result3, message3)
            result2, message2 = m1._inner_eq(m2)
            self.assertTrue(result2, message2)
"""
    


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()