# -*- coding: utf-8 -*-

import unittest
import os
import tempfile

from cortex.api.document import Document
from cortex.api.markable import Mention
from cortex.api.entity import Entity
from cortex.api.coreference_partition import CoreferencePartition
from cortex.parameters import ENCODING


class TestCoreferencePartition(unittest.TestCase):
    
    def test___init__(self):
        # Test parameters
        universe_subpart = ((3,4), (5,6), (7,8), (11,12), (13,14))
        pseudo_entities = (((3,4), (9,10), (13,14)),
                           ((1,2), (11,12)),
                           )
        expected_universe = set(((1,2), (3,4), (5,6), (7,8), (9,10), (11,12), (13,14)))
        expected_entity1 = Entity(mention_extents=((3,4), (9,10), (13,14)))
        expected_entity2 = Entity(mention_extents=((1,2), (11,12)))
        expected_entities = {(1,2): expected_entity2,
                             (3,4): expected_entity1,
                             (5,6): Entity(mention_extents=((5,6),)), 
                             (7,8): Entity(mention_extents=((7,8),)),
                             (9,10): expected_entity1, 
                             (11,12): expected_entity2, 
                             (13,14): expected_entity1,
                             }
        
        # Test
        coreference_partition = CoreferencePartition(entities=pseudo_entities, mention_extents=universe_subpart)
        actual_entities = coreference_partition._mention_extent_to_entity
        actual_universe = set(coreference_partition._mention_extent_to_entity.keys())
        self.assertEqual(expected_universe, actual_universe)
        self.assertEqual(expected_entities, actual_entities)
    
    def test_build_and_consistency(self):
        # Intializing with a list of mentions
        document = Document("test_document", "This is a drill ! No way ! Yes way ! I don't believe you .")
        extents = ((1, 4), (11, 15), (38,38), (54,56))
        mentions = tuple(Mention("extent[0],extent[1]".format(extent=extent), extent, document) for extent in extents)
        set_head_extents(mentions, extents)
        coreference_partition = CoreferencePartition(mention_extents=(m.extent for m in mentions))
        mention1 = mentions[0]
        mention2 = mentions[1]
        
        coreference_partition.join(mention1.extent, mention2.extent)
        
        expected_length = 3
        actual_length = len(coreference_partition)
        self.assertEqual(expected_length, actual_length)
        
        expected_entity1 = Entity(m.extent for m in (mention1, mention2,))
        self.assertEqual(expected_entity1, coreference_partition.get_entity(mention1.extent))
        self.assertEqual(expected_entity1, coreference_partition.get_entity(mention2.extent))
        
        # Intializing with a list of entities
        document = Document("test_document", "This is a drill ! No way ! Yes way ! I don't believe you .")
        extents = [(1, 4), (11, 15), (38,38), (54,56)]
        mentions = {"mention{}".format(i+1): Mention("extent[0],extent[1]".format(extent=extent), extent, document) \
                        for i, extent in enumerate(extents)}
        set_head_extents(sorted(mentions.values()), extents)
        entity1 = Entity(m.extent for m in (mentions["mention1"], mentions["mention2"]))
        entity2 = Entity(m.extent for m in (mentions["mention3"],))
        entity3 = Entity(m.extent for m in (mentions["mention4"],))
        coreference_partition = CoreferencePartition(entities=[entity1, entity2, entity3])
        
        expected_length = 3
        actual_length = len(coreference_partition)
        self.assertEqual(expected_length, actual_length)
        
        expected_entity1 = Entity(m.extent for m in (mention1, mention2))
        self.assertEqual(expected_entity1, coreference_partition.get_entity(mention1.extent))
        self.assertEqual(expected_entity1, coreference_partition.get_entity(mention2.extent))
    
    def test_equal(self):
        # Test parameters
        document = Document("test_document", "This is a drill ! No way ! Yes way ! I don't believe you .")
        extents = ((1, 4), (11, 15), (38,38), (54,56))
        mentions1 = tuple(Mention("extent[0],extent[1]".format(extent=extent), extent, document) for extent in extents)
        set_head_extents(mentions1, extents)
        coreference_partition1 = CoreferencePartition(mention_extents=(m.extent for m in mentions1))
        mentions2 = tuple(Mention("extent[0],extent[1]".format(extent=extent), extent, document) for extent in extents)
        set_head_extents(mentions2, extents)
        coreference_partition2 = CoreferencePartition(mention_extents=(m.extent for m in mentions2))
        
        # Test
        self.assertEqual(coreference_partition1, coreference_partition2)
    
    def test_remove_elements(self):
        # Test parameters
        entities = (Entity(mention_extents=((19,56), (85,86), (126,129))), 
                    Entity(mention_extents=((249,255),)),
                    )
        coreference_partition = CoreferencePartition(entities=entities)
        mention_extent = (85,86)
        expected_coreference_partition = CoreferencePartition(entities=(((19,56), (126,129)),
                                                                        ((249,255),),
                                                                        )
                                                              )
        
        # Test
        self.assertNotEqual(expected_coreference_partition, coreference_partition)
        coreference_partition.remove_element(mention_extent)
        self.assertEqual(expected_coreference_partition, coreference_partition)
    
    def test_remove_entity(self):
        # Test parameters
        entities = (Entity(mention_extents=((19,56), (85,86), (126,129))), 
                    Entity(mention_extents=((249,255),)),
                    )
        coreference_partition = CoreferencePartition(entities=entities)
        mention_extent = (85,86)
        expected_coreference_partition = CoreferencePartition(entities=(((249,255),),))
        
        # Test
        self.assertNotEqual(expected_coreference_partition, coreference_partition)
        coreference_partition.remove_entity(mention_extent)
        self.assertEqual(expected_coreference_partition, coreference_partition)
    
    def test_load(self):
        # Test parameters
        file_name = "test_load.txt"
        test_data_folder_path = os.path.join(os.path.dirname(__file__), "data", "coreference_partition")
        file_path = os.path.join(test_data_folder_path, file_name)
        entities = (Entity(mention_extents=((19,56), (85,86), (126,129))), 
                    Entity(mention_extents=((249,255),)),
                    )
        
        # Expected result
        expected_result = CoreferencePartition(entities=entities)
        
        # Actual result
        actual_result = CoreferencePartition.load(file_path)
        
        # Test
        self.assertEqual(expected_result, actual_result)
        
    def test_save(self):
        # Test parameter
        expected_result_file_name = "test_save.txt"
        test_data_folder_path = os.path.join(os.path.dirname(__file__), "data", "coreference_partition")
        expected_result_file_path = os.path.join(test_data_folder_path, expected_result_file_name)
        entities = (Entity(mention_extents=((19,56), (85,86), (126,129))), 
                    Entity(mention_extents=((249,255),)),
                    )
        coreference_partition = CoreferencePartition(entities=entities)
        
        def get_result(file_path):
            with open(file_path, "r", encoding=ENCODING) as f:
                result = f.read().strip()
            return result
        
        # Expected result
        expected_result = get_result(expected_result_file_path)
        
        # Actual result
        actual_result_file_path = tempfile.mktemp()
        try:
            coreference_partition.save(actual_result_file_path)
            actual_result = get_result(actual_result_file_path)
        except Exception:
            if os.path.isfile(actual_result_file_path):
                os.remove(actual_result_file_path)
            raise
        else:
            if os.path.isfile(actual_result_file_path):
                os.remove(actual_result_file_path)
        
        # Test
        self.assertEqual(expected_result, actual_result)
    
    def test_copy(self):
        # Test parameters
        pseudo_entities = (((1,2), (3,4)),
                           ((5,6), (7,8), (8,9)),
                           ((10,11),),
                           )
        coreference_partition = CoreferencePartition(entities=pseudo_entities)
        entities_original_partition = tuple(coreference_partition)
        
        # Test
        copy_coreference_partition = coreference_partition.copy()
        entities_copy_partition = tuple(copy_coreference_partition)
        self.assertEqual(coreference_partition, copy_coreference_partition)
        self.assertFalse(copy_coreference_partition is coreference_partition)
        self.assertEqual(len(entities_original_partition), len(entities_copy_partition))
        remaining_original_entities = list(entities_original_partition)
        remaining_copy_entities = list(entities_copy_partition)
        for pseudo_entity in pseudo_entities:
            head_extent = pseudo_entity[0]
            # Fetch the original entity that contains the head_extent
            ind = None
            for i, entity in enumerate(remaining_original_entities):
                if head_extent in entity:
                    ind = i
                    break
            original_entity = remaining_original_entities.pop(ind)
            ind = None
            for i, entity in enumerate(remaining_copy_entities):
                if head_extent in entity:
                    ind = i
                    break
            copy_entity = remaining_copy_entities.pop(ind)
            self.assertEqual(original_entity, copy_entity)
            self.assertFalse(copy_entity is original_entity)


def set_head_extents(mentions, head_extents):
    for mention, head_extent in zip(mentions, head_extents):
        mention.head_extent = head_extent

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()