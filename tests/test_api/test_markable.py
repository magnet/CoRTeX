# -*- coding: utf-8 -*-

import unittest
import os
import xml.etree.ElementTree as ET
from collections import OrderedDict
import tempfile

from cortex.parameters import ENCODING
from cortex.utils.io import TemporaryDirectory
from cortex.api.document import Document
from cortex.api.markable import Markable, Token, Mention, Sentence, NamedEntity, Quotation
from cortex.api.constituency_tree import ConstituencyTree


def get_document():
    ident = "test_doc"
    raw_text = "\nThe nice cake that you made on the 4th of July is good ."
    document = Document(ident, raw_text)
    return document

def get_document2():
    ident = "test_doc"
    raw_text = "\nI say to you that the nice cake that you made on the 4th of July is good ."
    document = Document(ident, raw_text)
    return document

def set_elements(elt1, elt2, attributes_values):
    for attribute_name, val1, val2 in attributes_values:
        setattr(elt1, attribute_name, val1)
        setattr(elt2, attribute_name, val2)

def retrieve_result_from_using_to_xml_element_method(elt):
    with TemporaryDirectory() as temp_dir:
        file_path = tempfile.mktemp(dir=temp_dir.temporary_folder_path)
        tree = ET.ElementTree(elt)
        tree.write(file_path, encoding=ENCODING)
        with open(file_path, "r", encoding=ENCODING) as f:
            actual_result = f.read()
    return actual_result

def write_and_parse_back_an_xml_element(input_markable_elt):
    with TemporaryDirectory() as temp_dir:
        # Write
        file_path = tempfile.mktemp(dir=temp_dir.temporary_folder_path)
        tree = ET.ElementTree(input_markable_elt)
        tree.write(file_path, encoding=ENCODING)
        # Parse
        xml_tree = ET.parse(file_path)
        parsed_markable_elt = xml_tree.getroot()
    return parsed_markable_elt

test_write_folder_path = os.path.join(os.path.dirname(__file__), "data", "markable")
#@unittest.skip
class TestMarkable(unittest.TestCase):
    
    #@unittest.skip
    def test_equal(self):
        # Test parameters
        document = get_document()
        raw_text = "cake"
        element1 = Markable("element1", (10,13), document, raw_text=raw_text)
        element2 = Markable("element2", (10,13), document, raw_text=raw_text)
        # Test
        self.assertEqual(element1, element2)
    
    #@unittest.skip
    def test_inequalities(self):
        # Test parameters
        document = get_document()
        element1 = Markable("element1", (10,13), document, raw_text="cake")
        element2 = Markable("element2", (15,18), document, raw_text="that")
        element3 = Markable("element3", (10,18), document, raw_text="cake that")
        element4 = Markable("element4", (12,17), document, raw_text="ke tha")
        # Test
        self.assertTrue(element1 <= element2)
        
        self.assertTrue(element1 < element3)
        self.assertTrue(element3 > element1)
        self.assertTrue(element1 <= element3)
        self.assertTrue(element3 >= element1)
        self.assertFalse(element3 < element1)
        self.assertFalse(element1 > element3)
        self.assertFalse(element3 <= element1)
        self.assertFalse(element1 >= element3)
        
        self.assertFalse(element2 < element3)
        self.assertFalse(element3 > element2)
        self.assertFalse(element2 <= element3)
        self.assertFalse(element3 >= element2)
        self.assertTrue(element3 < element2)
        self.assertTrue(element2 > element3)
        self.assertTrue(element3 <= element2)
        self.assertTrue(element2 >= element3)
        
        self.assertTrue(element3 <= element4)
        self.assertTrue(element4 >= element3)
        self.assertTrue(element3 < element4)
        self.assertTrue(element4 > element3)
        self.assertFalse(element3 > element4)
        self.assertFalse(element4 < element3)
        self.assertFalse(element3 >= element4)
        self.assertFalse(element4 <= element3)
        
    #@unittest.skip
    def test_includes(self):
        # Test parameters
        document = get_document()
        element1 = Markable("element1", (10,13), document, raw_text="cake")
        element3 = Markable("element3", (10,18), document, raw_text="cake that")
        element4 = Markable("element4", (12,17), document, raw_text="ke tha")
        element5 = Markable("element5", (1,3), document, raw_text="The")
        data = ((element3, element4, True),
                (element4, element3, False),
                (element3, element1, True),
                (element3, element5, False),
                (element5, element3, False),
                )
        # Test
        for elt1, elt2, expected_result in data:
            actual_result = elt1.includes(elt2)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_finishes(self):
        # Test parameters
        document = get_document()
        element2 = Markable("element2", (15,18), document, raw_text="that")
        element3 = Markable("element3", (10,18), document, raw_text="cake that")
        element4 = Markable("element4", (12,17), document, raw_text="ke tha")
        element5 = Markable("element5", (1,3), document, raw_text="The")
        data = ((element3, element4, False),
                (element4, element3, False),
                (element3, element2, False),
                (element2, element3, True),
                (element3, element5, False),
                (element5, element3, False),
                )
        # Test
        for elt1, elt2, expected_result in data:
            actual_result = elt1.finishes(elt2)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_starts(self):
        # Test parameters
        document = get_document()
        element1 = Markable("element1", (10,13), document, raw_text="cake")
        element3 = Markable("element3", (10,18), document, raw_text="cake that")
        element4 = Markable("element4", (12,17), document, raw_text="ke tha")
        element5 = Markable("element5", (1,3), document, raw_text="The")
        data = ((element3, element4, False),
                (element4, element3, False),
                (element3, element1, False),
                (element1, element3, True),
                (element3, element5, False),
                (element5, element3, False),
                )
        # Test
        for elt1, elt2, expected_result in data:
            actual_result = elt1.starts(elt2)
            self.assertEqual(expected_result, actual_result)

#@unittest.skip
class TestNamedEntity(unittest.TestCase):
    
    #@unittest.skip
    def test_equal(self):
        # Test parameters
        document = get_document()
        raw_text = "the 4th of July"
        type_ = "DATE"
        element1 = NamedEntity("element1", (32,46), document, type_, raw_text=raw_text)
        element2 = NamedEntity("element2", (32,46), document, type_, raw_text=raw_text)
        attributes_values = [(n,v,v) for n,v in [("subtype", None)]]
        set_elements(element1, element2, attributes_values)
        
        # Test
        self.assertTrue(element1 == element2)
        element2.type = "ORG"
        self.assertFalse(element1 == element2)
    
    #@unittest.skip
    def test_to_dict(self):
        # Test parameters
        document = get_document()
        raw_text = "the 4th of July"
        type_ = "DATE"
        origin = "test"
        element = NamedEntity("element1", (32,46), document, type_, raw_text=raw_text, origin=origin)
        expected_result = {"ident": "element1", "extent": (32,46), "raw_text": raw_text, 
                           "type": type_, "origin": origin}
        
        # Test
        actual_result = element.to_dict()
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_from_dict(self):
        # Test parameters
        document = get_document()
        ident = "test_named_entity"
        extent = (32,46)
        raw_text = "the 4th of July"
        type_ = "DATE"
        subtype = "DAY"
        origin = "original_corpus"
        dict_ = {"ident": ident, "extent": extent, "document": document, "raw_text": raw_text, 
                 "type": type_, "subtype": subtype, "origin": origin}
        
        # Expected value
        expected_element = NamedEntity(ident, extent, document, type_=type_, raw_text=raw_text, 
                                       subtype=subtype, origin=origin)
        
        # Actual value
        actual_element = NamedEntity.from_dict(dict_)
        
        # Test
        self.assertEqual(expected_element, actual_element)
    
    #@unittest.skip
    def test_to_xml_element(self):
        # Test parameters
        markable_name = "named_entity"
        document = get_document()
        ident = "test_{}".format(markable_name)
        extent = (32,46)
        raw_text = "the 4th of July"
        type_ = "DATE"
        subtype = "DAY"
        origin = "original_corpus"
        dict_ = {"ident": ident, "extent": extent, "document": document, "raw_text": raw_text, 
                 "type": type_, "subtype": subtype, "origin": origin}
        named_entity = NamedEntity.from_dict(dict_)
        
        # Expected result
        expected_result = '<named_entity id="test_named_entity"><extent end="46" start="32" /><text>the 4th of July</text><type>DATE</type><subtype>DAY</subtype><origin>original_corpus</origin></named_entity>'
        
        # Actual result
        elt = named_entity.to_xml_element()
        actual_result = retrieve_result_from_using_to_xml_element_method(elt)
        
        # Test
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_from_xml_element_to_dict(self):
        # Test parameters
        class_ = NamedEntity
        document = get_document()
        ident = "test_named_entity"
        extent = (32,46)
        raw_text = "the 4th of July"
        type_ = "DATE"
        subtype = "DAY"
        origin = "original_corpus"
        dict_ = {"ident": ident, "extent": extent, "document": document, "raw_text": raw_text, 
                 "type": type_, "subtype": subtype, "origin": origin}
        item = class_.from_dict(dict_)
        xml_element = item.to_xml_element()
        
        # Expected value
        expected_result = dict(dict_)
        del(expected_result["document"])
        
        # Actual value
        actual_result = class_.from_xml_element_to_dict(xml_element)
        
        # Test
        self.assertEqual(expected_result, actual_result)


#@unittest.skip
class TestToken(unittest.TestCase):
    
    #@unittest.skip
    def test_equal(self):
        # Test parameters
        document = get_document()
        raw_text = "made"
        element1 = Token("element1", (24,27), document, raw_text=raw_text, UD_features={"Tense": "Past"})
        element2 = Token("element2", (24,27), document, raw_text=raw_text, UD_features={"Tense": "Past"})
        attributes_values = [("lemma", "make", "make"), ("POS_tag", "VBZ", "VBZ")]
        set_elements(element1, element2, attributes_values)
        named_entity1 = NamedEntity("24,27", (24,27), document, "vb")
        named_entity1.subtype = "glop"
        named_entity2 = NamedEntity("24,27", (24,27), document, "vb")
        named_entity2.subtype = "glop"
        element1.named_entity = named_entity1
        element2.named_entity = named_entity2
        
        # Test
        self.assertTrue(element1 == element2)
        #element2.POS_tag = None
        element2.UD_features = None
        self.assertFalse(element1 == element2)
    
    #@unittest.skip
    def test_to_dict(self):
        # Test parameters
        document = get_document()
        raw_text = "made"
        POS_tag = "VBZ"
        lemma = "make"
        UD_features = {"Tense": "Past", "Mood": "Ind"}
        element = Token("element1", (24,27), document, raw_text=raw_text, POS_tag=POS_tag, 
                        lemma=lemma, UD_features=UD_features)
        expected_result = {"ident": "element1", "extent": (24,27), "raw_text": raw_text, 
                           "POS_tag": POS_tag, "lemma": lemma, "UD_features": dict(UD_features)}
        
        # Test
        actual_result = element.to_dict()
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_from_dict(self):
        # Test parameters
        document = get_document()
        ident = "test_token"
        extent = (24,27)
        raw_text = "made"
        POS_tag = "VBZ"
        lemma = "make"
        UD_features = {"Tense": "Past", "Mood": "Ind"}
        named_entity = NamedEntity("24,27", (24,27), document, "vb")
        named_entity.subtype = "glop"
        dict_ = {"ident": ident, "extent": extent, "document": document, "raw_text": raw_text, 
                 "POS_tag": POS_tag, "lemma": lemma, "UD_features": UD_features, "named_entity": named_entity}
        
        # Expected value
        expected_element = Token(ident, extent, document, raw_text=raw_text, POS_tag=POS_tag, 
                                 lemma=lemma, UD_features=UD_features)
        
        # Actual value
        actual_element = Token.from_dict(dict_)
        
        # Test
        self.assertEqual(expected_element, actual_element)
    
    #@unittest.skip
    def test_to_xml_element(self):
        # Test parameters
        document = get_document()
        markable_name = "token"
        ident = "test_{}".format(markable_name)
        raw_text = "made"
        token = Token(ident, (24,27), document, raw_text=raw_text)
        token.POS_tag = "VBZ"
        token.lemma = "make"
        token.UD_features = OrderedDict((("Tense", "Past"), ("Mood", "Ind")))
        named_entity = NamedEntity("24,27", (24,27), document, "vb")
        named_entity.subtype = "glop"
        token.named_entity = named_entity
        
        # Expected result
        expected_result = '<token id="test_token"><extent end="27" start="24" /><text>made</text><POS_tag>VBZ</POS_tag><lemma>make</lemma><UD_features><Tense>Past</Tense><Mood>Ind</Mood></UD_features></token>'
        
        # Actual result
        elt = token.to_xml_element()
        actual_result = retrieve_result_from_using_to_xml_element_method(elt)
        
        # Test
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_from_xml_element_to_dict(self):
        # Test parameters
        document = get_document()
        ident = "test_token"
        extent = (24,27)
        raw_text = "made"
        POS_tag = "VBZ"
        lemma = "make"
        UD_features = OrderedDict((("Tense", "Past"), ("Mood", "Ind")))
        named_entity = NamedEntity("24,27", (24,27), document, "vb")
        named_entity.subtype = "glop"
        dict_ = {"ident": ident, "extent": extent, "document": document, "raw_text": raw_text, 
                 "POS_tag": POS_tag, "lemma": lemma, "UD_features": UD_features, 
                 "named_entity": named_entity}
        item = Token.from_dict(dict_)
        xml_element = item.to_xml_element()
        
        # Expected value
        expected_result = dict(dict_)
        del(expected_result["document"])
        del(expected_result["named_entity"])
        
        # Actual value
        actual_result = Token.from_xml_element_to_dict(xml_element)
        
        # Test
        self.assertEqual(expected_result, actual_result)



#@unittest.skip
class TestMention(unittest.TestCase):
    
    #@unittest.skip
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.maxDiff = None
    
    #@unittest.skip
    def test_equal(self):
        # Test parameters
        document = get_document()
        raw_text = "The nice cake"
        element1 = Mention("element1", (1,13), document, raw_text=raw_text)
        element2 = Mention("element2", (1,13), document, raw_text=raw_text)
        tokens = [Token("1", (1,3), document, "The"), 
                  Token("2", (5,8), document, "nice"), 
                  Token("3", (10,13), document, "cake")]
        head_tokens = tokens[2:3]
        attributes_values = [(n,v,v) for n,v in \
                             [("head_extent", (10,13)), ("gram_type", "NNP"), ("gram_subtype", None), ("gender", "neut"), 
                              ("number", "sg"), ("tokens", tokens), ("head_tokens", head_tokens)]
                             ]
        set_elements(element1, element2, attributes_values)
        
        # Test
        self.assertTrue(element1 == element2)
        element2._head_tokens = None
        self.assertFalse(element1 == element2)
    
    #@unittest.skip
    def test_inequalities(self):
        # Test parameters
        document = get_document()
        "The nice cake that you made on the 4th of July"
        element1 = Mention.from_dict({"ident": "element1", "extent": (1,13), "document": document, 
                                      "raw_text": "The nice cake", "head_extent": (10,13)})
        element2 = Mention.from_dict({"ident": "element2", "extent": (32,46), "document": document, 
                                      "raw_text": "the 4th of July", "head_extent": (36,38)})
        element2_5 = Mention.from_dict({"ident": "element2_5", "extent": (32,46), "document": document, 
                                      "raw_text": "the 4th of July", "head_extent": None})
        element3 = Mention.from_dict({"ident": "element3", "extent": (1,46), "document": document, 
                                      "raw_text": "The nice cake that you made on the 4th of July", 
                                      "head_extent": (1,13)})
        
        # Test
        self.assertTrue(element1 < element2)
        self.assertTrue(element2 > element1)
        self.assertTrue(element1 <= element2)
        self.assertTrue(element2 >= element1)
        self.assertFalse(element2 < element1)
        self.assertFalse(element1 > element2)
        self.assertFalse(element2 <= element1)
        self.assertFalse(element1 >= element2)
        
        self.assertTrue(element1 < element2_5)
        self.assertTrue(element2_5 > element1)
        self.assertTrue(element1 <= element2_5)
        self.assertTrue(element2_5 >= element1)
        self.assertFalse(element2_5 < element1)
        self.assertFalse(element1 > element2_5)
        self.assertFalse(element2_5 <= element1)
        self.assertFalse(element1 >= element2_5)
        
        self.assertFalse(element2 < element3)
        self.assertFalse(element3 > element2)
        self.assertFalse(element2 <= element3)
        self.assertFalse(element3 >= element2)
        self.assertTrue(element3 < element2)
        self.assertTrue(element2 > element3)
        self.assertTrue(element3 <= element2)
        self.assertTrue(element2 >= element3)
        
        self.assertFalse(element2_5 < element3)
        self.assertFalse(element3 > element2_5)
        self.assertFalse(element2_5 <= element3)
        self.assertFalse(element3 >= element2_5)
        self.assertTrue(element3 < element2_5)
        self.assertTrue(element2_5 > element3)
        self.assertTrue(element3 <= element2_5)
        self.assertTrue(element2_5 >= element3)
    
    #@unittest.skip
    def test_to_dict(self):
        # Test parameters
        document = get_document()
        raw_text = "The nice cake"
        gram_type = "NOMINAL"
        gram_subtype = None
        gender = "neut"
        number = "sg"
        wn_synonyms = tuple()
        wn_hypernyms = tuple()
        referential_probability = 1.0
        element = Mention("element1", (1,13), document, raw_text=raw_text, gram_type=gram_type, 
                          gram_subtype=gram_subtype, gender=gender, number=number, 
                          wn_synonyms=wn_synonyms, wn_hypernyms=wn_hypernyms, 
                          referential_probability=referential_probability)
        expected_result = {"ident": "element1", "extent": (1,13), "raw_text": raw_text, 
                           "gram_type": gram_type, "gender": gender, "number": number, 
                           "wn_synonyms": wn_synonyms, "wn_hypernyms": wn_hypernyms,
                           "referential_probability": referential_probability}
        
        # Test
        actual_result = element.to_dict()
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_from_dict(self):
        # Test parameters
        document = get_document()
        ident = "test_mention"
        extent = (1,13)
        raw_text = "The nice cake"
        head_extent = (10,13)
        gram_type = "NNP"
        gram_subtype = "NNP-N"
        gender = "neut"
        number = "sg"
        
        dict_ = {"ident": ident, "extent": extent, "document": document, "raw_text": raw_text, 
                 "head_extent": head_extent, "gram_type": gram_type, "gram_subtype": gram_subtype, 
                 "gender": gender, "number": number}
        
        # Expected value
        expected_element = Mention(ident, extent, document, raw_text=raw_text)
        expected_element.head_extent = head_extent
        expected_element.gram_type = gram_type
        expected_element.gram_subtype = gram_subtype
        expected_element.gender = gender
        expected_element.number = number
        
        # Actual value
        actual_element = Mention.from_dict(dict_)
        
        # Test
        self.assertEqual(expected_element, actual_element)
    
    #@unittest.skip
    def test_update(self):
        # Test parameters
        document = get_document()
        ident = "test_mention"
        extent = (1,13)
        raw_text = "The nice cake"
        head_extent1 = (10,13)
        gram_type1 = "NNP"
        gram_subtype1 = "NNP-N"
        gender1 = "neut"
        number1 = "sg"
        wn_synonyms1 = []
        wn_hypernnyms1 = []
        referential_probability1 = 0.56
        head_extent2 = (1,13)
        gram_type2 = "NNP2"
        gram_subtype2 = "NNP-N2"
        gender2 = "neut2"
        number2 = "sg2"
        wn_synonyms2 = [1]
        wn_hypernnyms2 = [2]
        referential_probability2 = 0.38
        
        dict_ = {"ident": ident, "extent": extent, "document": document, "raw_text": raw_text, 
                 "head_extent": head_extent1, "gram_type": gram_type1, "gram_subtype": gram_subtype1, 
                 "gender": gender1, "number": number1, "wn_synonyms": wn_synonyms1, 
                 "wn_hypernnyms": wn_hypernnyms1, "referential_probability": referential_probability1}
        
        data = {"head_extent": head_extent2, "gram_type": gram_type2, "gram_subtype": gram_subtype2, 
                 "gender": gender2, "number": number2, "wn_synonyms": wn_synonyms2, 
                 "wn_hypernnyms": wn_hypernnyms2, "referential_probability": referential_probability2}
        
        # Expected value
        expected_element_before = Mention(ident, extent, document, raw_text=raw_text)
        expected_element_before.head_extent = head_extent1
        expected_element_before.gram_type = gram_type1
        expected_element_before.gram_subtype = gram_subtype1
        expected_element_before.gender = gender1
        expected_element_before.number = number1
        expected_element_before.wn_synonyms = wn_synonyms1
        expected_element_before.wn_hypernnyms = wn_hypernnyms1
        expected_element_before.referential_probability = referential_probability1
        
        expected_element_after = Mention(ident, extent, document, raw_text=raw_text)
        expected_element_after.head_extent = head_extent2
        expected_element_after.gram_type = gram_type2
        expected_element_after.gram_subtype = gram_subtype2
        expected_element_after.gender = gender2
        expected_element_after.number = number2
        expected_element_after.wn_synonyms = wn_synonyms2
        expected_element_after.wn_hypernnyms = wn_hypernnyms2
        expected_element_after.referential_probability = referential_probability2
        
        # Actual value
        actual_element = Mention.from_dict(dict_)
        
        # Test
        self.assertNotEqual(expected_element_before, expected_element_after)
        self.assertEqual(expected_element_before, actual_element)
        actual_element.update(data)
        self.assertEqual(expected_element_after, actual_element)
    
    #@unittest.skip
    def test_to_xml_element(self):
        # Test parameters
        data = []
        
        ## Mention1
        markable_name = "mention"
        document = get_document()
        ident = "test_{}".format(markable_name)
        extent = (1,13)
        raw_text = "The nice cake"
        head_extent = (10,13)
        gram_type = "NNP"
        gram_subtype = "NNP-N"
        gender = "neut"
        number = "sg"
        wn_synonyms = ["memory.n.01", "memory.n.02"]
        wn_hypernyms = ["representation.n.01", "content.n.05"]
        dict_ = {"ident": ident, "extent": extent, "document": document, "raw_text": raw_text, 
                 "head_extent": head_extent, "gram_type": gram_type, "gram_subtype": gram_subtype, 
                 "gender": gender, "number": number, "wn_synonyms": wn_synonyms, 
                 "wn_hypernyms": wn_hypernyms}
        mention = Mention.from_dict(dict_)
        
        args = tuple()
        kwargs = {"with_named_entity_info": False}
        
        expected_strings_dict = OrderedDict([("extent", '<extent end="13" start="1" />'), 
                                             ("text", '<text>The nice cake</text>'), 
                                             ("head_extent", '<head_extent end="13" start="10" />'), 
                                             ("gram_type", '<gram_type>NNP</gram_type>'), 
                                             ("gram_subtype", '<gram_subtype>NNP-N</gram_subtype>'),
                                             ("gender", '<gender>neut</gender>'), 
                                             ("number", '<number>sg</number>'), 
                                             ("wn_synonyms", '<wn_synonyms><wn_synonym>memory.n.01</wn_synonym><wn_synonym>memory.n.02</wn_synonym></wn_synonyms>'), 
                                             ("wn_hypernyms", '<wn_hypernyms><wn_hypernym>representation.n.01</wn_hypernym><wn_hypernym>content.n.05</wn_hypernym></wn_hypernyms>')
                                             ]) 
        expected_result = '<mention id="test_mention">{extent}{text}{head_extent}{gram_type}{gram_subtype}{gender}{number}{wn_synonyms}{wn_hypernyms}</mention>'.format(**expected_strings_dict)
        data.append((mention, args, kwargs, expected_result))
        
        ## Mention2
        wn_synonyms = []
        wn_hypernyms = []
        dict_ = {"ident": ident, "extent": extent, "document": document, "raw_text": raw_text, 
                 "head_extent": head_extent, "gram_type": gram_type, "gram_subtype": gram_subtype, 
                 "gender": gender, "number": number, "wn_synonyms": wn_synonyms, 
                 "wn_hypernyms": wn_hypernyms}
        mention = Mention.from_dict(dict_)
        
        args = tuple()
        kwargs = {"with_named_entity_info": False}
        
        expected_strings_dict = OrderedDict([("extent", '<extent end="13" start="1" />'), 
                                             ("text", '<text>The nice cake</text>'), 
                                             ("head_extent", '<head_extent end="13" start="10" />'), 
                                             ("gram_type", '<gram_type>NNP</gram_type>'), 
                                             ("gram_subtype", '<gram_subtype>NNP-N</gram_subtype>'),
                                             ("gender", '<gender>neut</gender>'), 
                                             ("number", '<number>sg</number>'), 
                                             ("wn_synonyms", '<wn_synonyms />'), 
                                             ("wn_hypernyms", '<wn_hypernyms />')
                                             ]) 
        expected_result = '<mention id="test_mention">{extent}{text}{head_extent}{gram_type}{gram_subtype}{gender}{number}{wn_synonyms}{wn_hypernyms}</mention>'.format(**expected_strings_dict)
        data.append((mention, args, kwargs, expected_result))
        
        ## Mention3
        wn_synonyms = None
        wn_hypernyms = None
        dict_ = {"ident": ident, "extent": extent, "document": document, "raw_text": raw_text, 
                 "head_extent": head_extent, "gram_type": gram_type, "gram_subtype": gram_subtype, 
                 "gender": gender, "number": number, "wn_synonyms": wn_synonyms, 
                 "wn_hypernyms": wn_hypernyms}
        mention = Mention.from_dict(dict_)
        
        args = tuple()
        kwargs = {"with_named_entity_info": False}
        
        expected_strings_dict = OrderedDict([("extent", '<extent end="13" start="1" />'), 
                                             ("text", '<text>The nice cake</text>'), 
                                             ("head_extent", '<head_extent end="13" start="10" />'), 
                                             ("gram_type", '<gram_type>NNP</gram_type>'), 
                                             ("gram_subtype", '<gram_subtype>NNP-N</gram_subtype>'),
                                             ("gender", '<gender>neut</gender>'), 
                                             ("number", '<number>sg</number>'), 
                                             ]) 
        expected_result = '<mention id="test_mention">{extent}{text}{head_extent}{gram_type}{gram_subtype}{gender}{number}</mention>'.format(**expected_strings_dict)
        data.append((mention, args, kwargs, expected_result))
        
        # Test
        for mention, args, kwargs, expected_result in data:
            elt = mention.to_xml_element(*args, **kwargs)
            actual_result = retrieve_result_from_using_to_xml_element_method(elt)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_from_xml_element_to_dict(self):
        # Test parameters
        data = []
        
        ## Elt1
        class_ = Mention
        document = get_document()
        ident = "test_mention"
        extent = (1,13)
        raw_text = "The nice cake"
        head_extent = (10,13)
        gram_type = "NNP"
        gram_subtype = "NNP-N"
        gender = "neut"
        number = "sg"
        dict_ = {"ident": ident, "extent": extent, "document": document, "raw_text": raw_text, 
                 "head_extent": head_extent, "gram_type": gram_type, "gram_subtype": gram_subtype, 
                 "gender": gender, "number": number}
        item = class_.from_dict(dict_)
        xml_element = item.to_xml_element()
        
        expected_result = dict(dict_)
        del(expected_result["document"])
        data.append((class_, xml_element, expected_result))
        
        ## Elt2
        dict_ = {"ident": ident, "extent": extent, "document": document, "raw_text": raw_text, 
                 "head_extent": head_extent, "gram_type": gram_type, "gram_subtype": gram_subtype, 
                 "gender": gender, "number": number, "wn_synonyms": ("pie.n.01",), 
                 "wn_hypernyms": ("food.n.01", "organic.n.01")}
        item = class_.from_dict(dict_)
        xml_element = item.to_xml_element()
        
        expected_result = dict(dict_)
        del(expected_result["document"])
        data.append((class_, xml_element, expected_result))
        
        ## Elt3
        dict_ = {"ident": ident, "extent": extent, "document": document, "raw_text": raw_text, 
                 "head_extent": head_extent, "gram_type": gram_type, "gram_subtype": gram_subtype, 
                 "gender": gender, "number": number, "wn_synonyms": tuple(), 
                 "wn_hypernyms": tuple()}
        item = class_.from_dict(dict_)
        xml_element = item.to_xml_element()
        
        expected_result = dict(dict_)
        del(expected_result["document"])
        data.append((class_, xml_element, expected_result))
        
        # Test
        for class_, xml_element, expected_result in data:
            actual_result = class_.from_xml_element_to_dict(xml_element)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_to_xml_element_and_back_again(self):
        # Test parameters
        data = []
        
        ## Mention1
        markable_name = "mention"
        document = get_document()
        ident = "test_{}".format(markable_name)
        extent = (1,13)
        raw_text = "The nice cake"
        head_extent = (10,13)
        gram_type = "NNP"
        gram_subtype = "NNP-N"
        gender = "neut"
        number = "sg"
        wn_synonyms = ["memory.n.01", "memory.n.02"]
        wn_hypernyms = ["representation.n.01", "content.n.05"]
        dict_ = {"ident": ident, "extent": extent, "document": document, "raw_text": raw_text, 
                 "head_extent": head_extent, "gram_type": gram_type, "gram_subtype": gram_subtype, 
                 "gender": gender, "number": number, "wn_synonyms": wn_synonyms, 
                 "wn_hypernyms": wn_hypernyms}
        mention = Mention.from_dict(dict_)
        
        args = tuple()
        kwargs = {"with_named_entity_info": False}
        
        expected_result = mention
        data.append((mention, args, kwargs, expected_result))
        
        ## Mention2
        wn_synonyms = []
        wn_hypernyms = []
        dict_ = {"ident": ident, "extent": extent, "document": document, "raw_text": raw_text, 
                 "head_extent": head_extent, "gram_type": gram_type, "gram_subtype": gram_subtype, 
                 "gender": gender, "number": number, "wn_synonyms": wn_synonyms, 
                 "wn_hypernyms": wn_hypernyms}
        mention = Mention.from_dict(dict_)
        
        args = tuple()
        kwargs = {"with_named_entity_info": False}
        
        expected_result = mention
        data.append((mention, args, kwargs, expected_result))
        
        ## Mention3
        wn_synonyms = None
        wn_hypernyms = None
        dict_ = {"ident": ident, "extent": extent, "document": document, "raw_text": raw_text, 
                 "head_extent": head_extent, "gram_type": gram_type, "gram_subtype": gram_subtype, 
                 "gender": gender, "number": number, "wn_synonyms": wn_synonyms, 
                 "wn_hypernyms": wn_hypernyms}
        mention = Mention.from_dict(dict_)
        
        args = tuple()
        kwargs = {"with_named_entity_info": False}
        
        expected_result = mention
        data.append((mention, args, kwargs, expected_result))
        
        # Test
        for mention, args, kwargs, expected_result in data:
            elt = mention.to_xml_element(*args, **kwargs)
            parsed_elt = write_and_parse_back_an_xml_element(elt)
            d = Mention.from_xml_element_to_dict(parsed_elt)
            d["document"] = document
            actual_result = Mention.from_dict(d)
            self.assertEqual(expected_result, actual_result)
    
    

#@unittest.skip
class TestSentence(unittest.TestCase):
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.maxDiff = None
    
    #@unittest.skip
    def test_equal(self):
        # Test parameters
        document = get_document()
        raw_text = "The nice cake that you made on the 4th of July is good ."
        element1 = Sentence("element1", (1,56), document, raw_text=raw_text)
        element2 = Sentence("element1", (1,56), document, raw_text=raw_text)
        # Tokens
        tokens1 = [Token("1", (1,3), document, "The"), 
                  Token("2", (5,8), document, "nice"), 
                  Token("3", (10,13), document, "cake"),
                  Token("4", (15,18), document, "that"),
                  Token("5", (20,22), document, "you"),
                  Token("6", (24,27), document, "made"),
                  Token("7", (29,30), document, "on"),
                  Token("8", (32,34), document, "the"),
                  Token("9", (36,38), document, "4th"),
                  Token("10", (40,41), document, "of"),
                  Token("11", (43,46), document, "July"),
                  Token("12", (48,49), document, "is"),
                  Token("13", (51,54), document, "good"),
                  Token("14", (56,56), document, ".")]
        tokens2 = [Token("1", (1,3), document, "The"), 
                  Token("2", (5,8), document, "nice"), 
                  Token("3", (10,13), document, "cake"),
                  Token("4", (15,18), document, "that"),
                  Token("5", (20,22), document, "you"),
                  Token("6", (24,27), document, "made"),
                  Token("7", (29,30), document, "on"),
                  Token("8", (32,34), document, "the"),
                  Token("9", (36,38), document, "4th"),
                  Token("10", (40,41), document, "of"),
                  Token("11", (43,46), document, "July"),
                  Token("12", (48,49), document, "is"),
                  Token("13", (51,54), document, "good"),
                  Token("14", (56,56), document, ".")]
        # Mentions
        mention11 = Mention("11", (1,13), document, raw_text="The nice cake")
        mention21 = Mention("21", (1,13), document, raw_text="The nice cake")
        attributes_values = [(n,v,v) for n,v in \
                             [("head_extent", (10,13)), ("gram_type", "NNP"), ("gram_subtype", None), ("gender", "neut"), 
                              ("number", "sg"), ("tokens", tokens1[:3]), ("head_tokens", tokens1[2:3])]
                             ]
        set_elements(mention11, mention21, attributes_values)
        mention12 = Mention("12", (32,46), document, raw_text="the 4th of July")
        mention22 = Mention("22", (32,46), document, raw_text="the 4th of July")
        attributes_values = [(n,v,v) for n,v in \
                             [("head_extent", (36,38)), ("gram_type", "NNP"), ("gram_subtype", None), ("gender", "neut"), 
                              ("number", "sg"), ("tokens", tokens2[7:11]), ("head_tokens", tokens2[8:9])]
                             ]
        set_elements(mention12, mention22, attributes_values)
        mentions1 = [mention11, mention12]
        mentions2 = [mention21, mention22]
        # Named entities
        raw_text = "the 4th of July"
        type_ = "DATE"
        named_entity1 = NamedEntity("element1", (32,46), document, type_, raw_text=raw_text)
        named_entity2 = NamedEntity("element2", (32,46), document, type_, raw_text=raw_text)
        attributes_values = [(n,v,v) for n,v in [("subtype", None)]]
        set_elements(named_entity1, named_entity2, attributes_values)
        mention12.named_entity = named_entity1
        mention22.named_entity = named_entity2
        named_entities1 = [named_entity1]
        named_entities2 = [named_entity2]
        # Predicate argument
        pred_args1 = ((("112","122","ARGM-DIS"), ("126", "129", "ARG1"), ("131", "132", "V")), (("168", "169", "V"),)) # /!\ Does not have any relation to the real sentence
        pred_args2 = ((("112","122","ARGM-DIS"), ("126", "129", "ARG1"), ("131", "132", "V")), (("168", "169", "V"),))
        # Speaker
        speaker1 = "speaker #1"
        speaker2 = "speaker #1"
        # Constituency tree
        constituency_tree_string = "(TOP (S (NP (NP (DT The) (JJ nice) (NN cake)) (SBAR (WHNP (IN that)) (S (NP (PRP you))) (VP (VBD made)) (PP (IN on) (NP (DT the) (CD 4th) (NP (IN of) (NNP July)))))) (VP (VBD is)) (VP (JJ good)) (. .)))"
        constituency_tree1 = ConstituencyTree.parse(constituency_tree_string)
        constituency_tree2 = ConstituencyTree.parse(constituency_tree_string)
        
        # Set data
        attributes_values = [("_tokens", tokens1, tokens2), 
                             ("_named_entities", named_entities1, named_entities2), 
                             ("_mentions", mentions1, mentions2),
                             ("constituency_tree", constituency_tree1, constituency_tree2),
                             ("predicate_arguments", pred_args1, pred_args2),
                             ("speaker", speaker1, speaker2)]
        set_elements(element1, element2, attributes_values)
        
        # Test
        self.assertTrue(element1 == element2)
    
    #@unittest.skip
    def test_to_dict(self):
        # Test parameters
        document = get_document()
        raw_text = "The nice cake that you made on the 4th of July is good ."
        constituency_tree_string = "(TOP (S (NP (NP (DT The) (JJ nice) (NN cake)) (SBAR (WHNP (IN that)) (S (NP (PRP you))) (VP (VBD made)) (PP (IN on) (NP (DT the) (CD 4th) (NP (IN of) (NNP July)))))) (VP (VBD is)) (VP (JJ good)) (. .)))"
        predicate_arguments = (((112,122,"ARGM-DIS"), (126, 129, "ARG1"), (131, 132, "V")), ((168, 169, "V"),))
        speaker = "speaker #1"
        element = Sentence("element1", (1,56), document, raw_text=raw_text, 
                           constituency_tree_string=constituency_tree_string, 
                           predicate_arguments=predicate_arguments, speaker=speaker)
        expected_result = {"ident": "element1", "extent": (1,56), "raw_text": raw_text,
                           "constituency_tree_string": constituency_tree_string, 
                           "predicate_arguments": predicate_arguments, "speaker": speaker}
        
        # Test
        actual_result = element.to_dict()
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_from_dict(self):
        # Test parameters
        document = get_document()
        ident = "test_sentence"
        extent = (1,56)
        raw_text = "The nice cake that you made on the 4th of July is good ."
        ## Tokens
        tokens = [Token("1", (1,3), document, "The"), 
                  Token("2", (5,8), document, "nice"), 
                  Token("3", (10,13), document, "cake"),
                  Token("4", (15,18), document, "that"),
                  Token("5", (20,22), document, "you"),
                  Token("6", (24,27), document, "made"),
                  Token("7", (29,30), document, "on"),
                  Token("8", (32,34), document, "the"),
                  Token("9", (36,38), document, "4th"),
                  Token("10", (40,41), document, "of"),
                  Token("11", (43,46), document, "July"),
                  Token("12", (48,49), document, "is"),
                  Token("13", (51,54), document, "good"),
                  Token("14", (56,56), document, ".")]
        ## Mentions
        mention1_dict = {"ident": "1", "extent": (1,13), "document": document, 
                         "raw_text":"The nice cake", "head_extent": (10,13), "gram_type": "NNP", 
                         "gram_subtype": None, "gender": "neut", "number": "sg", 
                         "tokens": tokens[:3], "head_tokens": tokens[2:3]}
        mention2_dict = {"ident": "2", "extent": (32,46), "document": document, "raw_text": "the 4th of July", 
                         "head_extent": (36,38), "gram_type": "NNP", "gram_subtype": None, "gender": "neut", 
                         "number": "sg", "tokens": tokens[7:11], "head_tokens": tokens[8:9]}
        mention1 = Mention.from_dict(mention1_dict)
        mention2 = Mention.from_dict(mention2_dict)
        mentions = [mention1, mention2]
        ## Named entities
        named_entity_dict = {"ident": "element1", "extent": (32,46), "document": document, 
                             "type": "DATE", "raw_text": "the 4th of July"}
        named_entity = NamedEntity.from_dict(named_entity_dict)
        mention1.named_entity = named_entity
        named_entities = [named_entity]
        ## Predicate argument
        predicate_arguments = ((("112","122","ARGM-DIS"), ("126", "129", "ARG1"), ("131", "132", "V")), (("168", "169", "V"),)) # /!\ Does not have any relation to the real sentence
        ## Speaker
        speaker = "speaker #1"
        ## Constituency tree
        constituency_tree_string = "(TOP (S (NP (NP (DT The) (JJ nice) (NN cake)) (SBAR (WHNP (IN that)) (S (NP (PRP you))) (VP (VBD made)) (PP (IN on) (NP (DT the) (CD 4th) (NP (IN of) (NNP July)))))) (VP (VBD is)) (VP (JJ good)) (. .)))"
        constituency_tree = ConstituencyTree.parse(constituency_tree_string)
        
        dict_ = {"ident": ident, "extent": extent, "document": document, "raw_text": raw_text, 
                 "tokens": tokens, "named_entities": named_entities, "mentions": mentions, 
                 "predicate_arguments": predicate_arguments, "speaker": speaker, 
                 "constituency_tree": constituency_tree
                 }
        
        # Expected value
        expected_element = Sentence(ident, extent, document, raw_text=raw_text, 
                                    constituency_tree_string=constituency_tree_string, 
                                    predicate_arguments=predicate_arguments, speaker=speaker)
        
        # Actual value
        actual_element = Sentence.from_dict(dict_)
        
        # Test
        self.assertTrue(*expected_element._inner_eq(actual_element))
        
        # With constituency tree data input as a string
        dict_ = {"ident": ident, "extent": extent, "document": document, "raw_text": raw_text, 
                 "tokens": tokens, "named_entities": named_entities, "mentions": mentions, 
                 "predicate_arguments": predicate_arguments, "speaker": speaker, 
                 "constituency_tree_string": constituency_tree_string}
        actual_element = Sentence.from_dict(dict_)
        self.assertEqual(expected_element, actual_element)

    #@unittest.skip
    def test_to_xml_element(self):
        # Test parameters
        markable_name = "sentence"
        document = get_document()
        ident = "test_{}".format(markable_name)
        extent = (1,56)
        raw_text = "The nice cake that you made on the 4th of July is good ."
        ## Tokens
        tokens = [Token("1", (1,3), document, "The"), 
                  Token("2", (5,8), document, "nice"), 
                  Token("3", (10,13), document, "cake"),
                  Token("4", (15,18), document, "that"),
                  Token("5", (20,22), document, "you"),
                  Token("6", (24,27), document, "made"),
                  Token("7", (29,30), document, "on"),
                  Token("8", (32,34), document, "the"),
                  Token("9", (36,38), document, "4th"),
                  Token("10", (40,41), document, "of"),
                  Token("11", (43,46), document, "July"),
                  Token("12", (48,49), document, "is"),
                  Token("13", (51,54), document, "good"),
                  Token("14", (56,56), document, ".")]
        ## Mentions
        mention1_dict = {"ident": "1", "extent": (1,13), "document": document, 
                         "raw_text":"The nice cake", "head_extent": (10,13), "gram_type": "NNP", 
                         "gram_subtype": None, "gender": "neut", "number": "sg", 
                         "tokens": tokens[:3], "head_tokens": tokens[2:3]}
        mention2_dict = {"ident": "2", "extent": (32,46), "document": document, "raw_text": "the 4th of July", 
                         "head_extent": (36,38), "gram_type": "NNP", "gram_subtype": None, "gender": "neut", 
                         "number": "sg", "tokens": tokens[7:11], "head_tokens": tokens[8:9]}
        mention1 = Mention.from_dict(mention1_dict)
        mention2 = Mention.from_dict(mention2_dict)
        mentions = [mention1, mention2]
        ## Named entities
        named_entity_dict = {"ident": "element1", "extent": (32,46), "document": document, 
                             "type": "DATE", "raw_text": "the 4th of July"}
        named_entity = NamedEntity.from_dict(named_entity_dict)
        mention1.named_entity = named_entity
        named_entities = [named_entity]
        ## Predicate argument
        predicate_arguments = ((("112","122","ARGM-DIS"), ("126", "129", "ARG1"), ("131", "132", "V")), (("168", "169", "V"),)) # /!\ Does not have any relation to the real sentence
        ## Speaker
        speaker = "speaker #1"
        ## Constituency tree
        constituency_tree_string = "(TOP (S (NP (NP (DT The) (JJ nice) (NN cake)) (SBAR (WHNP (IN that)) (S (NP (PRP you))) (VP (VBD made)) (PP (IN on) (NP (DT the) (CD 4th) (NP (IN of) (NNP July)))))) (VP (VBD is)) (VP (JJ good)) (. .)))"
        constituency_tree = ConstituencyTree.parse(constituency_tree_string)
        
        dict_ = {"ident": ident, "extent": extent, "document": document, "raw_text": raw_text, 
                 "tokens": tokens, "named_entities": named_entities, "mentions": mentions, 
                 "predicate_arguments": predicate_arguments, "speaker": speaker, 
                 "constituency_tree": constituency_tree}
        sentence = Sentence.from_dict(dict_)
        
        # Expected result
        pred_args1_string = '<pred_args><pred_arg end="122" start="112" type="ARGM-DIS" /><pred_arg end="129" start="126" type="ARG1" /><pred_arg end="132" start="131" type="V" /></pred_args>'
        pred_args2_string = '<pred_args><pred_arg end="169" start="168" type="V" /></pred_args>'
        expected_predicate_arguments_string = '<predicate_arguments>{}{}</predicate_arguments>'.format(pred_args1_string, pred_args2_string)
        expected_strings_dict = OrderedDict([("extent", '<extent end="56" start="1" />'), 
                                             ("text", '<text>The nice cake that you made on the 4th of July is good .</text>'),
                                             ("parse", '<parse>{}</parse>'.format(constituency_tree_string)), 
                                             ("predicate_arguments", expected_predicate_arguments_string), 
                                             ("speaker", '<speaker>speaker #1</speaker>')
                                             ]) 
        expected_result = '<sentence id="test_sentence">{extent}{text}{parse}{predicate_arguments}{speaker}</sentence>'.format(**expected_strings_dict)
        
        # Actual result
        elt = sentence.to_xml_element()
        actual_result = retrieve_result_from_using_to_xml_element_method(elt)
        
        # Test
        self.assertEqual(expected_result, actual_result)
    
    def test_from_xml_element_to_dict(self):
        # Test parameters
        class_ = Sentence
        document = get_document()
        ident = "test_sentence"
        extent = (1,56)
        raw_text = "The nice cake that you made on the 4th of July is good ."
        ## Tokens
        tokens = [Token("1", (1,3), document, "The"), 
                  Token("2", (5,8), document, "nice"), 
                  Token("3", (10,13), document, "cake"),
                  Token("4", (15,18), document, "that"),
                  Token("5", (20,22), document, "you"),
                  Token("6", (24,27), document, "made"),
                  Token("7", (29,30), document, "on"),
                  Token("8", (32,34), document, "the"),
                  Token("9", (36,38), document, "4th"),
                  Token("10", (40,41), document, "of"),
                  Token("11", (43,46), document, "July"),
                  Token("12", (48,49), document, "is"),
                  Token("13", (51,54), document, "good"),
                  Token("14", (56,56), document, ".")]
        ## Mentions
        mention1_dict = {"ident": "1", "extent": (1,13), "document": document, 
                         "raw_text":"The nice cake", "head_extent": (10,13), "gram_type": "NNP", 
                         "gram_subtype": None, "gender": "neut", "number": "sg", 
                         "tokens": tokens[:3], "head_tokens": tokens[2:3]}
        mention2_dict = {"ident": "2", "extent": (32,46), "document": document, "raw_text": "the 4th of July", 
                         "head_extent": (36,38), "gram_type": "NNP", "gram_subtype": None, "gender": "neut", 
                         "number": "sg", "tokens": tokens[7:11], "head_tokens": tokens[8:9]}
        mention1 = Mention.from_dict(mention1_dict)
        mention2 = Mention.from_dict(mention2_dict)
        mentions = [mention1, mention2]
        ## Named entities
        named_entity_dict = {"ident": "element1", "extent": (32,46), "document": document, 
                             "type": "DATE", "raw_text": "the 4th of July"}
        named_entity = NamedEntity.from_dict(named_entity_dict)
        mention1.named_entity = named_entity
        named_entities = [named_entity]
        ## Predicate argument
        predicate_arguments = [[("112","122","ARGM-DIS"), ("126", "129", "ARG1"), ("131", "132", "V")], [("168", "169", "V")]] # /!\ Does not have any relation to the real sentence
        ## Speaker
        speaker = "speaker #1"
        ## Constituency tree
        constituency_tree_string = "(TOP (S (NP (NP (DT The) (JJ nice) (NN cake)) (SBAR (WHNP (IN that)) (S (NP (PRP you))) (VP (VBD made)) (PP (IN on) (NP (DT the) (CD 4th) (NP (IN of) (NNP July)))))) (VP (VBD is)) (VP (JJ good)) (. .)))"
        constituency_tree = ConstituencyTree.parse(constituency_tree_string)
        
        dict_ = {"ident": ident, "extent": extent, "document": document, "raw_text": raw_text, 
                 "tokens": tokens, "named_entities": named_entities, "mentions": mentions, 
                 "predicate_arguments": predicate_arguments, "speaker": speaker, 
                 "constituency_tree": constituency_tree}
        item = class_.from_dict(dict_)
        xml_element = item.to_xml_element()
        
        # Expected value
        expected_result = dict(dict_)
        del(expected_result["document"])
        del(expected_result["tokens"])
        del(expected_result["mentions"])
        del(expected_result["named_entities"])
        
        # Actual value
        actual_result = class_.from_xml_element_to_dict(xml_element)
        
        # Test
        self.assertEqual(expected_result, actual_result)

##@unittest.skip
class TestQuotation(unittest.TestCase):
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.maxDiff = None
    
    #@unittest.skip
    def test_equal(self):
        # Test parameters
        document = get_document2()
        raw_text = "the nice cake that you made on the 4th of July is good"
        speaker_mention_extent = (1,1)
        speaker_verb_token_extent = (3,5)
        interlocutor_mention_extent = (10,12)
        
        element1 = Quotation("element1", (19,72), document, raw_text=raw_text, 
                             speaker_mention_extent=speaker_mention_extent, 
                             speaker_verb_token_extent=speaker_verb_token_extent, 
                             interlocutor_mention_extent=interlocutor_mention_extent)
        element2 = Quotation("element2", (19,72), document, raw_text=raw_text, 
                             speaker_mention_extent=speaker_mention_extent, 
                             speaker_verb_token_extent=speaker_verb_token_extent, 
                             interlocutor_mention_extent=interlocutor_mention_extent)
        
        speaker_mention = Mention("speaker_mention", (1,1), document, "I")
        speaker_verb = Token("speaker_verb", (3,5), document, "say")
        interlocutor_mention = Mention("interlocutor_mention", (10,12), document, "you")
        attributes_values = [(n,v,v) for n,v in \
                             [("speaker_mention", speaker_mention),
                              ("speaker_verb", speaker_verb),
                              ("interlocutor_mention", interlocutor_mention)
                             ]
                             ]
        set_elements(element1, element2, attributes_values)
        
        # Test
        self.assertTrue(element1 == element2)
        element2.interlocutor_mention = None
        expected_result = (False, "interlocutor_mention")
        actual_result = element1._inner_eq(element2)
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_to_dict(self):
        # Test parameters
        document = get_document2()
        raw_text = "the nice cake that you made on the 4th of July is good"
        speaker_mention_extent = (1,1)
        speaker_verb_token_extent = (3,5)
        interlocutor_mention_extent = (10,12)
        element = Quotation("element1", (19,72), document, raw_text=raw_text, 
                             speaker_mention_extent=speaker_mention_extent, 
                             speaker_verb_token_extent=speaker_verb_token_extent, 
                             interlocutor_mention_extent=interlocutor_mention_extent)
        expected_result = {"ident": "element1", "extent": (19,72), "raw_text": raw_text, 
                           "speaker_mention_extent": speaker_mention_extent, 
                           "speaker_verb_token_extent": speaker_verb_token_extent, 
                           "interlocutor_mention_extent": interlocutor_mention_extent}
        
        # Test
        actual_result = element.to_dict()
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_from_dict(self):
        # Test parameters
        document = get_document2()
        ident = "test_quotation"
        extent = (19,72)
        raw_text = "the nice cake that you made on the 4th of July is good"
        speaker_mention_extent = (1,1)
        speaker_verb_token_extent = (3,5)
        interlocutor_mention_extent = (10,12)
        dict_ = {"ident": ident, "extent": extent, "document": document, "raw_text": raw_text, 
                 "speaker_mention_extent": speaker_mention_extent, 
                 "speaker_verb_token_extent": speaker_verb_token_extent, 
                 "interlocutor_mention_extent": interlocutor_mention_extent}
        expected_element = Quotation(ident, extent, document, raw_text=raw_text, 
                             speaker_mention_extent=speaker_mention_extent, 
                             speaker_verb_token_extent=speaker_verb_token_extent, 
                             interlocutor_mention_extent=interlocutor_mention_extent)
        
        # Test
        actual_element = Quotation.from_dict(dict_)
        self.assertEqual(expected_element, actual_element)
    
    #@unittest.skip
    def test_update(self):
        # Test parameters
        document = get_document2()
        
        ident = "test_quotation"
        extent = (19,72)
        raw_text = "the nice cake that you made on the 4th of July is good"
        speaker_mention_extent1 = (1,1)
        speaker_verb_token_extent1 = (3,5)
        interlocutor_mention_extent1 = (10,12)
        speaker_mention_extent2 = (1,2)
        speaker_verb_token_extent2 = (4,5)
        interlocutor_mention_extent2 = (11,12)
        
        dict_ = {"ident": ident, "extent": extent, "document": document, "raw_text": raw_text, 
                 "speaker_mention_extent": speaker_mention_extent1, 
                 "speaker_verb_token_extent": speaker_verb_token_extent1, 
                 "interlocutor_mention_extent": interlocutor_mention_extent1}
        
        data = {"speaker_mention_extent": speaker_mention_extent2, 
                 "speaker_verb_token_extent": speaker_verb_token_extent2, 
                 "interlocutor_mention_extent": interlocutor_mention_extent2}
        
        # Expected value
        expected_element_before = Quotation(ident, extent, document, raw_text=raw_text, 
                                            speaker_mention_extent=speaker_mention_extent1, 
                                            speaker_verb_token_extent=speaker_verb_token_extent1, 
                                            interlocutor_mention_extent=interlocutor_mention_extent1)
        
        expected_element_after = Quotation(ident, extent, document, raw_text=raw_text, 
                                            speaker_mention_extent=speaker_mention_extent2, 
                                            speaker_verb_token_extent=speaker_verb_token_extent2, 
                                            interlocutor_mention_extent=interlocutor_mention_extent2)
        
        # Actual value
        actual_element = Quotation.from_dict(dict_)
        
        # Test
        self.assertNotEqual(expected_element_before, expected_element_after)
        self.assertEqual(expected_element_before, actual_element)
        actual_element.update(data)
        self.assertEqual(expected_element_after, actual_element)
    
    #@unittest.skip
    def test_to_xml_element(self):
        # Test parameters
        data = []
        
        ## Quotation1
        markable_name = "quotation"
        document = get_document2()
        ident = "test_{}".format(markable_name)
        extent = (19,72)
        raw_text = "the nice cake that you made on the 4th of July is good"
        speaker_mention_extent = (1,1)
        speaker_verb_token_extent = (3,5)
        interlocutor_mention_extent = (10,12)
        dict_ = {"ident": ident, "extent": extent, "document": document, "raw_text": raw_text, 
                 "speaker_mention_extent": speaker_mention_extent, 
                 "speaker_verb_token_extent": speaker_verb_token_extent, 
                 "interlocutor_mention_extent": interlocutor_mention_extent}
        quotation = Quotation.from_dict(dict_)
        
        args = tuple()
        kwargs = {}
        
        expected_strings_dict = OrderedDict([("extent", '<extent end="72" start="19" />'), 
                                             ("text", '<text>the nice cake that you made on the 4th of July is good</text>'), 
                                             ("speaker_mention_extent", "<speaker_mention_extent end=\"1\" start=\"1\" />"), 
                                             ("speaker_verb_token_extent", "<speaker_verb_token_extent end=\"5\" start=\"3\" />"), 
                                             ("interlocutor_mention_extent", "<interlocutor_mention_extent end=\"12\" start=\"10\" />"),
                                            ]) 
        expected_result = '<quotation id="test_quotation">{extent}{text}{speaker_mention_extent}{speaker_verb_token_extent}{interlocutor_mention_extent}</quotation>'.format(**expected_strings_dict)
        data.append((quotation, args, kwargs, expected_result))
        
        # Test
        for quotation, args, kwargs, expected_result in data:
            elt = quotation.to_xml_element(*args, **kwargs)
            actual_result = retrieve_result_from_using_to_xml_element_method(elt)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_from_xml_element_to_dict(self):
        # Test parameters
        class_ = Quotation
        document = get_document2()
        ident = "test_mention"
        extent = (19,72)
        raw_text = "the nice cake that you made on the 4th of July is good"
        speaker_mention_extent = (1,1)
        speaker_verb_token_extent = (3,5)
        interlocutor_mention_extent = (10,12)
        dict_ = {"ident": ident, "extent": extent, "document": document, "raw_text": raw_text, 
                 "speaker_mention_extent": speaker_mention_extent, 
                 "speaker_verb_token_extent": speaker_verb_token_extent, 
                 "interlocutor_mention_extent": interlocutor_mention_extent}
        item = class_.from_dict(dict_)
        xml_element = item.to_xml_element()
        
        expected_result = dict(dict_)
        del(expected_result["document"])
        
        # Test
        actual_result = class_.from_xml_element_to_dict(xml_element)
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_to_xml_element_and_back_again(self):
        # Test parameters
        data = []
        
        ## Quotation1
        markable_name = "quotation"
        document = get_document2()
        ident = "test_{}".format(markable_name)
        extent = (19,72)
        raw_text = "the nice cake that you made on the 4th of July is good"
        speaker_mention_extent = (1,1)
        speaker_verb_token_extent = (3,5)
        interlocutor_mention_extent = (10,12)
        dict_ = {"ident": ident, "extent": extent, "document": document, "raw_text": raw_text, 
                 "speaker_mention_extent": speaker_mention_extent, 
                 "speaker_verb_token_extent": speaker_verb_token_extent, 
                 "interlocutor_mention_extent": interlocutor_mention_extent}
        quotation = Quotation.from_dict(dict_)
        
        args = tuple()
        kwargs = {}
        
        expected_result = quotation
        data.append((quotation, args, kwargs, expected_result))
        
        # Test
        for quotation, args, kwargs, expected_result in data:
            elt = quotation.to_xml_element(*args, **kwargs)
            parsed_elt = write_and_parse_back_an_xml_element(elt)
            d = Quotation.from_xml_element_to_dict(parsed_elt)
            d["document"] = document
            actual_result = Quotation.from_dict(d)
            self.assertEqual(expected_result, actual_result)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()