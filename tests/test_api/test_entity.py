# -*- coding: utf-8 -*-

import unittest

from cortex.api.document import Document
from cortex.api.markable import Mention
from cortex.api.entity import Entity


class TestEntity(unittest.TestCase):

    def test_equal(self):
        # Test parameters
        document = Document("test_document", "\nThis is a drill ! No way ! Yes way ! I don't believe you .")
        mention11 = Mention.from_dict({"ident": "1,4", "extent": (1, 4), "document": document, 
                                       "raw_text": "This", "head_extent": (1, 4)})
        mention12 = Mention.from_dict({"ident": "11,15", "extent": (11, 15), "document": document, 
                                       "raw_text": "drill", "head_extent": (11, 15)})
        
        mention21 = Mention.from_dict({"ident": "1,4", "extent": (1, 4), "document": document, 
                                       "raw_text": "This", "head_extent": (1, 4)})
        mention22 = Mention.from_dict({"ident": "11,15", "extent": (11, 15), "document": document, 
                                       "raw_text": "drill", "head_extent": (11, 15)})
        
        entity1 = Entity(m.extent for m in (mention11, mention12))
        entity2 = Entity(m.extent for m in (mention21, mention22))
        
        # Test
        self.assertEqual(entity1, entity2)
    
    def test_inequalities(self):
        # Test parameters
        document = Document("test_document", "\nThis is a drill ! It is pointy ! You are mad .")
        mention11 = Mention.from_dict({"ident": "1,4", "extent": (1, 4), "document": document, 
                                       "raw_text": "This", "head_extent": (1, 4)})
        mention12 = Mention.from_dict({"ident": "11,15", "extent": (11, 15), "document": document, 
                                       "raw_text": "drill", "head_extent": (11, 15)})
        mention13 = Mention.from_dict({"ident": "19,20", "extent": (19, 20), "document": document, 
                                       "raw_text": "It", "head_extent": (19, 20)})
        
        mention21 = Mention.from_dict({"ident": "34,36", "extent": (34, 36), "document": document, 
                                       "raw_text": "You", "head_extent": (34, 36)})
        
        mention31 = Mention.from_dict({"ident": "19,20", "extent": (19, 20), "document": document, 
                                       "raw_text": "It", "head_extent": None})
        
        entity1 = Entity(m.extent for m in (mention11, mention12, mention13))
        entity2 = Entity(m.extent for m in (mention21,))
        entity3 = Entity(m.extent for m in (mention31,))
        
        # Test
        self.assertTrue(entity1 < entity2)
        self.assertTrue(entity2 > entity1)
        self.assertFalse(entity1 > entity2)
        self.assertFalse(entity2 < entity1)
        
        self.assertTrue(entity1 < entity3)
        self.assertTrue(entity3 > entity1)
        self.assertFalse(entity1 > entity3)
        self.assertFalse(entity3 < entity1)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()