# -*- coding: utf-8 -*-

import unittest
import os
import logging

from cortex.io.corpus_io import parse_corpus


class TestCorpus(unittest.TestCase):

    def test_from_parsing_data(self):
        # Test parameters
        logging.basicConfig(level=logging.DEBUG)
        verbose = False#True
        root_data_path = os.path.join(os.path.dirname(__file__), "data", "corpus")
        
        corpus_folder_path = root_data_path
        corpus_file_path = os.path.join(corpus_folder_path, "conll_test_corpus.txt")
        name = "test_multi_conll"
        corpus = parse_corpus(corpus_file_path, root_path=root_data_path, name=name, verbose=verbose)
        
        print("Corpus:")
        print(corpus)
        print()
        
        print("Corpus' summary:")
        detailed = True #False #True # False
        corpus.print_summary(detailed=detailed)
        
        #raise Exception()


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()