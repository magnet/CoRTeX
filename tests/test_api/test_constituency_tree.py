# -*- coding: utf-8 -*-

import unittest

from cortex.parameters import LRB_cortex_symbol, RRB_cortex_symbol, ELLIPSIS_cortex_symbol
from cortex.api.constituency_tree import (ConstituencyTree, ConstituencyTreeNode, CircularDependencyError, 
                                          ConstituencyTreeNodeCONLL2012StringRepresentationBuilder,)
from cortex.api.document import Document
from cortex.api.markable import Token

#@unittest.skip
class TestConstituencyTreeNode(unittest.TestCase):
    
    def test_equal(self):
        # Test parameters
        tree_line = "(TOP (S (NP (PRP$ Its) (NN president)) (VP (VBZ is) (NP (NNP Zheng) (NNP Bing))) (. .)))"
        tree1 = ConstituencyTree.parse(tree_line)
        node1 = tree1.root
        tree2 = ConstituencyTree.parse(tree_line)
        node2 = tree2.root
        # Test
        self.assertEqual(node1, node2)
    
    def test_is_leaf(self):
        # Test parameters
        tree_line = "(TOP (S (NP (PRP$ Its) (NN president)) (VP (VBZ is) (NP (NNP Zheng) (NNP Bing))) (. .)))"
        tree = ConstituencyTree.parse(tree_line)
        node = tree.root
        while node.children:
            node = node.children[0]
        
        # Test
        self.assertTrue(node.is_leaf())
        self.assertFalse(node.parent.is_leaf())
    
    def test_get_leaves(self):
        # Test parameter
        parent_node = ConstituencyTreeNode("parent")
        child1_node = ConstituencyTreeNode("child1")
        child2_node = ConstituencyTreeNode("child2")
        child3_node = ConstituencyTreeNode("child3")
        child4_node = ConstituencyTreeNode("child4")
        child5_node = ConstituencyTreeNode("child5")
        child6_node = ConstituencyTreeNode("child6")
        parent_node.children = [child1_node, child2_node]
        child1_node.parent = parent_node
        child2_node.parent = parent_node
        child1_node.children = [child3_node, child4_node]
        child3_node.parent = child1_node
        child4_node.parent = child1_node
        child2_node.children = [child5_node, child6_node]
        child5_node.parent = child2_node
        child6_node.parent = child2_node
        
        # Expected result
        expected_result = (child3_node, child4_node, child5_node, child6_node)
        # Actual result
        actual_result = tuple(parent_node.get_leaves())
        # Test
        g = (expected_result[i] is actual_result[i] for i in range(max(len(expected_result), len(actual_result))))
        self.assertTrue(all(g))
    
    def test_get_string_representation(self):
        # Test parameters
        tree_line = "(TOP (S (NP (PRP$ Its) (NN president)) (VP (VBZ is) (NP (NNP Zheng) (NNP Bing))) (. .)))"
        tree = ConstituencyTree.parse(tree_line)
        node = tree.root
        # Expected result
        expected_result = "(TOP (S (NP (PRP$ Its) (NN president)) (VP (VBZ is) (NP (NNP Zheng) (NNP Bing))) (. .)))"
        # Actual_result
        actual_result = node.get_string_representation()
        # Test
        self.assertEqual(expected_result, actual_result)
    
    def test_get_leftmost_leaf(self):
        # Test parameter
        parent_node = ConstituencyTreeNode("parent")
        child1_node = ConstituencyTreeNode("child1")
        child2_node = ConstituencyTreeNode("child2")
        child3_node = ConstituencyTreeNode("child3")
        child4_node = ConstituencyTreeNode("child4")
        parent_node.children = [child1_node, child2_node]
        child1_node.children = [child3_node, child4_node]
        # Expected result
        expected_result = child3_node
        # Actual result
        actual_result = parent_node.get_leftmost_leaf()
        # Test
        self.assertIs(expected_result, actual_result)
        
        # Test exception raising
        child3_node.children = [parent_node]
        # Test
        with self.assertRaises(CircularDependencyError):
            parent_node.get_leftmost_leaf()
        
    def test_get_rightmost_leaf(self):
        # Test parameter
        parent_node = ConstituencyTreeNode("parent")
        child1_node = ConstituencyTreeNode("child1")
        child2_node = ConstituencyTreeNode("child2")
        child3_node = ConstituencyTreeNode("child3")
        child4_node = ConstituencyTreeNode("child4")
        parent_node.children = [child1_node, child2_node]
        child2_node.children = [child3_node, child4_node]
        # Expected result
        expected_result = child4_node
        # Actual result
        actual_result = parent_node.get_rightmost_leaf()
        # Test
        self.assertIs(expected_result, actual_result)
        
        # Test exception raising
        child4_node.children = [parent_node]
        # Test
        with self.assertRaises(CircularDependencyError):
            parent_node.get_rightmost_leaf()
    
    def test_get_leaves_boundarie(self):
        # Test parameter
        parent_node = ConstituencyTreeNode("parent")
        child1_node = ConstituencyTreeNode("child1")
        child2_node = ConstituencyTreeNode("child2")
        child3_node = ConstituencyTreeNode("child3")
        child4_node = ConstituencyTreeNode("child4")
        child5_node = ConstituencyTreeNode("child5")
        child6_node = ConstituencyTreeNode("child6")
        parent_node.children = [child1_node, child2_node]
        child1_node.children = [child3_node, child4_node]
        child2_node.children = [child5_node, child6_node]
        # Expected result
        expected_result = (child3_node, child6_node)
        # Actual result
        actual_result = parent_node.get_leaves_boundaries()
        # Test
        g = (expected_result[i] is actual_result[i] for i in range(max(len(expected_result), len(actual_result))))
        self.assertTrue(all(g))
        
        # Test exception raising
        child3_node.children = [parent_node]
        # Test
        with self.assertRaises(CircularDependencyError):
            parent_node.get_leaves_boundaries()
    
    def test_get_extent_and_raw_extent(self):
        # Test parameters
        parent_node = ConstituencyTreeNode("parent")
        child1_node = ConstituencyTreeNode("child1")
        child2_node = ConstituencyTreeNode("child2")
        parent_node.children = [child1_node, child2_node]
        document = Document("test", "\nThis is great .")
        child1_node.token = Token("6,7", (6,7), document, "is")
        child2_node.token = Token("9,13", (9,13), document, "great")
        
        # Test
        ## Extent
        expected_result = (6, 7)
        actual_result = child1_node.extent
        self.assertEqual(expected_result, actual_result)
        
        ## Raw extent
        expected_result = (6, 13)
        actual_result = parent_node.raw_extent
        self.assertEqual(expected_result, actual_result)
    
    def test_get_raw_text_extent(self):
        # Test parameters
        document = Document("test", "\nIts president is Zheng Bing .")
        tokens = [Token("1,3", (1,3), document, "Its"), 
                  Token("5,13", (5,13), document, "president"), 
                  Token("15,16", (15,16), document, "is"), 
                  Token("18,22", (18,22), document, "Zheng"), 
                  Token("24,27", (24,27), document, "Bing"), 
                  Token("29,29", (29,29), document, ".")]
        tree_line = "(TOP (S (NP (PRP$ Its) (NN president)) (VP (VBZ is) (NP (NNP Zheng) (NNP Bing))) (. .)))"
        tree = ConstituencyTree.parse(tree_line)
        for n, t in zip(tree.leaves, tokens):
            n.token = t
        VP_node = tree.root.children[0].children[1]
        
        # Expected result
        expected_result = (15,27)
        # Actual result
        actual_result = VP_node.get_raw_text_extent()
        # Test
        self.assertEqual(expected_result, actual_result)
        
    def test_get_youngest_common_ancestor(self):
        # Test parameter
        parent_node = ConstituencyTreeNode("parent")
        child1_node = ConstituencyTreeNode("child1")
        child2_node = ConstituencyTreeNode("child2")
        child3_node = ConstituencyTreeNode("child3")
        child4_node = ConstituencyTreeNode("child4")
        child5_node = ConstituencyTreeNode("child5")
        child6_node = ConstituencyTreeNode("child6")
        parent_node.children = [child1_node, child2_node]
        child1_node.parent = parent_node
        child2_node.parent = parent_node
        child1_node.children = [child3_node, child4_node]
        child3_node.parent = child1_node
        child4_node.parent = child1_node
        child2_node.children = [child5_node, child6_node]
        child5_node.parent = child2_node
        child6_node.parent = child2_node
        
        test_node = child6_node
        
        # Expected result
        expected_result = parent_node
        # Actual result
        actual_result = child3_node.get_youngest_common_ancestor(test_node)
        # Test
        self.assertIs(expected_result, actual_result)
    
    def test_get_nodes_youngest_common_ancestor(self):
        # Test parameter
        parent_node = ConstituencyTreeNode("parent")
        child1_node = ConstituencyTreeNode("child1")
        child2_node = ConstituencyTreeNode("child2")
        child3_node = ConstituencyTreeNode("child3")
        child4_node = ConstituencyTreeNode("child4")
        child5_node = ConstituencyTreeNode("child5")
        child6_node = ConstituencyTreeNode("child6")
        child7_node = ConstituencyTreeNode("child7")
        child8_node = ConstituencyTreeNode("child8")
        parent_node.children = [child1_node, child2_node]
        child1_node.parent = parent_node
        child2_node.parent = parent_node
        child1_node.children = [child3_node, child4_node]
        child3_node.parent = child1_node
        child4_node.parent = child1_node
        child2_node.children = [child5_node, child6_node]
        child5_node.parent = child2_node
        child6_node.parent = child2_node
        child6_node.children = [child7_node, child8_node]
        child7_node.parent = child6_node
        child8_node.parent = child6_node
        
        data = [([child8_node, child5_node, child7_node], child2_node),
                ([child8_node, child4_node, child7_node, child5_node], parent_node)
                ]
        
        # Test
        for test_nodes, expected_result in data:
            actual_result = child3_node.get_nodes_youngest_common_ancestor(test_nodes)
            # Test
            self.assertIs(expected_result, actual_result)
    
    def test_get_youngest_tagged_ancestor(self):
        # Test parameter
        parent_node = ConstituencyTreeNode("parent")
        child1_node = ConstituencyTreeNode("child1")
        child2_node = ConstituencyTreeNode("child2")
        child3_node = ConstituencyTreeNode("child3")
        child4_node = ConstituencyTreeNode("child4")
        parent_node.children = [child1_node, child2_node]
        child1_node.parent = parent_node
        child2_node.parent = parent_node
        child1_node.children = [child3_node, child4_node]
        child3_node.parent = child1_node
        child4_node.parent = child1_node
        tags = ["parent"]
        
        # Expected result
        expected_result = parent_node
        # Actual result
        actual_result = child3_node.get_youngest_tagged_ancestor(tags)
        # Test
        self.assertIs(expected_result, actual_result)
    
    def test_get_path_to(self):
        # Test parameter
        parent_node = ConstituencyTreeNode("parent")
        child1_node = ConstituencyTreeNode("child1")
        child2_node = ConstituencyTreeNode("child2")
        child3_node = ConstituencyTreeNode("child3")
        child4_node = ConstituencyTreeNode("child4")
        child5_node = ConstituencyTreeNode("child5")
        child6_node = ConstituencyTreeNode("child6")
        parent_node.children = [child1_node, child2_node]
        child1_node.parent = parent_node
        child2_node.parent = parent_node
        child1_node.children = [child3_node, child4_node]
        child3_node.parent = child1_node
        child4_node.parent = child1_node
        child2_node.children = [child5_node, child6_node]
        child5_node.parent = child2_node
        child6_node.parent = child2_node
        
        data = [(child3_node, child6_node, [child3_node, child1_node, parent_node, child2_node, child6_node]), 
                (child3_node, parent_node, [child3_node, child1_node, parent_node]), 
                (child3_node, child3_node, [child3_node])
                ]
        
        # Test
        for start_node, end_node, expected_result in data:
            actual_result = start_node.get_path_to(end_node)
            g = (expected_result[i] is actual_result[i] for i in range(max(len(expected_result), len(actual_result))))
            self.assertTrue(all(g))
    
    def test_get_c_command_path(self):
        # Test parameter
        parent_node = ConstituencyTreeNode("parent")
        child1_node = ConstituencyTreeNode("child1")
        child2_node = ConstituencyTreeNode("child2")
        child3_node = ConstituencyTreeNode("child3")
        child4_node = ConstituencyTreeNode("child4")
        child5_node = ConstituencyTreeNode("child5")
        child6_node = ConstituencyTreeNode("child6")
        parent_node.children = [child1_node, child2_node]
        child1_node.parent = parent_node
        child2_node.parent = parent_node
        child1_node.children = [child3_node, child4_node]
        child3_node.parent = child1_node
        child4_node.parent = child1_node
        child2_node.children = [child5_node, child6_node]
        child5_node.parent = child2_node
        child6_node.parent = child2_node
        
        data = [(child3_node, child6_node, [child3_node, child1_node, parent_node, child2_node, child6_node]), 
                (child3_node, parent_node, None), 
                (child3_node, child3_node, None)
                ]
        
        # Test
        for start_node, end_node, expected_result in data:
            actual_result = start_node.get_c_command_path(end_node)
            if expected_result is None:
                self.assertIs(expected_result, actual_result)
            else:
                g = (expected_result[i] is actual_result[i] for i in range(max(len(expected_result), len(actual_result))))
                self.assertTrue(all(g))
    
    def test_get_self_depth(self):
        # Test parameters
        parent_node = ConstituencyTreeNode("parent")
        child1_node = ConstituencyTreeNode("child1")
        child3_node = ConstituencyTreeNode("child3")
        parent_node.children = [child1_node]
        child1_node.parent = parent_node
        child1_node.children = [child3_node]
        child3_node.parent = child1_node
        
        # Expected result
        expected_result = 2
        # Actual_result
        actual_result = child3_node.get_self_depth()
        # Test
        self.assertEqual(expected_result, actual_result)
    
    def test_count_type_to_root(self):
        # Test parameters
        parent_node = ConstituencyTreeNode("parent")
        child1_node = ConstituencyTreeNode("child1")
        child3_node = ConstituencyTreeNode("child3")
        parent_node.children = [child1_node]
        child1_node.parent = parent_node
        child1_node.children = [child3_node]
        child3_node.parent = child1_node
        tag = "child1"
        
        # Expected result
        expected_result = 1
        # Actual_result
        actual_result = child3_node.count_type_to_root(tag)
        # Test
        self.assertEqual(expected_result, actual_result)
    
    def test_ancestor_distance(self):
        # Test parameter
        parent_node = ConstituencyTreeNode("parent")
        child1_node = ConstituencyTreeNode("child1")
        child2_node = ConstituencyTreeNode("child2")
        child3_node = ConstituencyTreeNode("child3")
        child4_node = ConstituencyTreeNode("child4")
        child5_node = ConstituencyTreeNode("child5")
        child6_node = ConstituencyTreeNode("child6")
        parent_node.children = [child1_node, child2_node]
        child1_node.parent = parent_node
        child2_node.parent = parent_node
        child1_node.children = [child3_node, child4_node]
        child3_node.parent = child1_node
        child4_node.parent = child1_node
        child2_node.children = [child5_node, child6_node]
        child5_node.parent = child2_node
        child6_node.parent = child2_node
        
        data = [(child4_node, parent_node, 2), 
                (child4_node, child6_node, -1)]
        
        # Test
        for start_node, end_node, expected_result in data:
            actual_result = start_node.get_ancestor_distance(end_node)
            self.assertEqual(expected_result, actual_result)
        #with self.assertRaises(TypeError):
        #    actual_result = child4_node.get_ancestor_distance(None)
    
    def test_get_nodes(self):
        # Test parameter
        parent_node = ConstituencyTreeNode("child__r")
        child1_node = ConstituencyTreeNode("child_")
        child2_node = ConstituencyTreeNode("child_")
        child3_node = ConstituencyTreeNode("child__l")
        child4_node = ConstituencyTreeNode("child__r")
        child5_node = ConstituencyTreeNode("child__l")
        child6_node = ConstituencyTreeNode("child__r")
        parent_node.children = [child1_node, child2_node]
        child1_node.parent = parent_node
        child2_node.parent = parent_node
        child1_node.children = [child3_node, child4_node]
        child3_node.parent = child1_node
        child4_node.parent = child1_node
        child2_node.children = [child5_node, child6_node]
        child5_node.parent = child2_node
        child6_node.parent = child2_node
        node = parent_node
        
        data = [(parent_node, ["child__r"], [parent_node, child4_node, child6_node]),
                (parent_node, None, [parent_node, child1_node, child2_node, child3_node, child4_node, child5_node, child6_node])
                ]
        
        # Test
        for node, tags, expected_result in data:
            actual_result = node.get_tagged_nodes(tags)
            g = (expected_result[i] is actual_result[i] for i in range(max(len(expected_result), len(actual_result))))
            self.assertTrue(all(g))
    
    def test_get_all_nodes(self):
        # Test parameter
        parent_node = ConstituencyTreeNode("child__r")
        child1_node = ConstituencyTreeNode("child_")
        child2_node = ConstituencyTreeNode("child_")
        child3_node = ConstituencyTreeNode("child__l")
        child4_node = ConstituencyTreeNode("child__r")
        child5_node = ConstituencyTreeNode("child__l")
        child6_node = ConstituencyTreeNode("child__r")
        parent_node.children = [child1_node, child2_node]
        child1_node.parent = parent_node
        child2_node.parent = parent_node
        child1_node.children = [child3_node, child4_node]
        child3_node.parent = child1_node
        child4_node.parent = child1_node
        child2_node.children = [child5_node, child6_node]
        child5_node.parent = child2_node
        child6_node.parent = child2_node
        node = child1_node
        
        # Expected result
        expected_result = [child1_node, child3_node, child4_node]
        # Actual result
        actual_result = node.get_all_nodes()
        # Test
        g = (expected_result[i] is actual_result[i] for i in range(max(len(expected_result), len(actual_result))))
        self.assertTrue(all(g))
    
    def test_get_depth(self):
        # Test parameter
        parent_node = ConstituencyTreeNode("parent")
        child1_node = ConstituencyTreeNode("child1")
        child2_node = ConstituencyTreeNode("child2")
        child3_node = ConstituencyTreeNode("child3")
        child4_node = ConstituencyTreeNode("child4")
        child5_node = ConstituencyTreeNode("child5")
        child6_node = ConstituencyTreeNode("child6")
        child7_node = ConstituencyTreeNode("child7")
        child8_node = ConstituencyTreeNode("child8")
        parent_node.children = [child1_node, child2_node]
        child1_node.children = [child3_node, child4_node]
        child2_node.children = [child5_node, child6_node]
        child6_node.children = [child7_node, child8_node]
        # Expected result
        expected_result = 3
        # Actual result
        actual_result = parent_node.get_depth()
        # Test
        self.assertEqual(expected_result, actual_result)
        
        # Test exception raising
        child8_node.children = [parent_node]
        # Test
        with self.assertRaises(CircularDependencyError):
            parent_node.get_depth()



#@unittest.skip
class TestConstituencyTreeNodeCONLL2012StringRepresentationBuilder(unittest.TestCase):
    
    #@unittest.skip
    def test__convert_word_to_leaf_tag(self):
        # Test parameter
        data = []
        ## 
        data.append(("word", "word"))
        ##
        data.append(("(", LRB_cortex_symbol))
        ##
        data.append((")", RRB_cortex_symbol))
        ##
        data.append(("(...)", ELLIPSIS_cortex_symbol))
        #data.append(("(...)", "{}...{}".format(LRB_cortex_symbol, RRB_cortex_symbol)))
        ##
        data.append(("Royaume-Uni)1", "Royaume-Uni{}1".format(RRB_cortex_symbol)))
        
        # Test
        for word, expected_result in data:
            actual_result = ConstituencyTreeNodeCONLL2012StringRepresentationBuilder._convert_word_to_leaf_tag(word)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__convert_leaf_tag_to_word(self):
        # Test parameter
        data = []
        ## 
        data.append(("word", "word"))
        ##
        data.append((LRB_cortex_symbol, "("))
        ##
        data.append((RRB_cortex_symbol, ")"))
        ##
        data.append((ELLIPSIS_cortex_symbol, "(...)"))
        #data.append(("{}...{}".format(LRB_cortex_symbol, RRB_cortex_symbol), "(...)"))
        ##
        data.append(("Royaume-Uni{}1".format(RRB_cortex_symbol), "Royaume-Uni)1"))
        
        # Test
        for leaf_tag, expected_result in data:
            actual_result = ConstituencyTreeNodeCONLL2012StringRepresentationBuilder._convert_leaf_tag_to_word(leaf_tag)
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_flush(self):
        # Test parameter
        data = get_test_flush_data()
        
        # Test
        for builder, expected_string_representation in data:
            actual_string_representation = builder.flush()
            self.assertEqual(expected_string_representation, actual_string_representation)
    
    #@unittest.skip
    def test_convert_constituency_tree_node_to_string_representation(self):
        # Test parameters
        data = get_test_convert_constituency_tree_node_to_string_representation_data()
        
        # Test
        for (args, kwargs), expected_string_representation in data:
            actual_string_representation = ConstituencyTreeNodeCONLL2012StringRepresentationBuilder.convert_constituency_tree_node_to_string_representation(*args, **kwargs)
            self.maxDiff = None
            self.assertEqual(expected_string_representation, actual_string_representation)
    
    #@unittest.skip
    def test_parse_tree(self):
        # Test parameters
        data = get_test_parse_tree_data()
        
        # Test
        for string_representation, expected_result in data:
            actual_result = ConstituencyTreeNodeCONLL2012StringRepresentationBuilder.parse_tree(string_representation)
            self.assertTrue(*expected_result.root._inner_eq(actual_result.root))



#@unittest.skip
class TestConstituencyTree(unittest.TestCase):

    def test_parse(self):
        # Test parameters
        tree_line = "(TOP (S (NP (PRP$ Its) (NN president)) (VP (VBZ is) (NP (NNP Zheng) (NNP Bing))) (. .)))"
        tree = ConstituencyTree.parse(tree_line)
        # Expected result
        expected_result = "Its president is Zheng Bing .".split()
        # Actual_result
        actual_result = [leaf.syntactic_tag for leaf in tree.leaves]
        # Test
        self.assertEqual(expected_result, actual_result)
    
    ##@unittest.skip
    def test_parse_sentence_with_parentheses(self):
        # Test parameters
        s = "(ROOT (SENT (NP (PUNCT {LRB:}) (PP (ADP en) (NP (PUNCT {RRB:}) (PROPN Edward)))) (NP (PROPN J.) (PROPN Erickson) (PUNCT ,) (NP (PROPN Ordered) (ADP to) (DET Die))) (PUNCT :) (PP (DET A) (NP (MWN (NOUN History) (ADP of) (DET the)) (ADJ Ottoman) (NP (NOUN Army) (MWN (ADP in) (DET the) (ADJ First) (NOUN World) (NOUN War))) (PUNCT ,) (NP (MWN (PROPN Greenwood) (PROPN Publishing) (NOUN Group)) (PUNCT ,) (NUM 2001) (PUNCT [) (NOUN détail) (PP (ADP de) (NP (DET l') (NOUN édition)))))) (PUNCT ]) (NP (MWD (PUNCT {LRB:}) (NOUN ISBN) (PROPN 978) (PUNCT -) (NUM 0) (PUNCT -) (NUM 313) (PUNCT -) (NUM 31516))) (PUNCT -) (NP (NUM 9)) (PUNCT ,) (NP (MWD (PROPN LCCN) (NUM 00021562)) (MWN (PUNCT {RRB:}) (PUNCT {LRB:})) (PP (ADP en) (NP (PUNCT {RRB:}) (PROPN Eugene) (PROPN Hinterhoff) (PUNCT ,) (NP (PROPN Persia))))) (PUNCT :) (NP (MWN (DET The) (PROPN Stepping) (PROPN Stone) (PART To) (PROPN India))) (PUNCT .)))"
        tree_line = s.format(LRB=LRB_cortex_symbol, RRB=RRB_cortex_symbol)
        expected_leaves_raw_text = "( en ) Edward J. Erickson , Ordered to Die : A History of the Ottoman Army in the First World War , Greenwood Publishing Group , 2001 [ détail de l' édition ] ( ISBN 978 - 0 - 313 - 31516 - 9 , LCCN 00021562 ) ( en ) Eugene Hinterhoff , Persia : The Stepping Stone To India ."
        expected_string_representation = str(tree_line)
        
        # Test
        constituency_tree = ConstituencyTree.parse(tree_line)
        actual_leaves_raw_text = constituency_tree.leaves_raw_text
        actual_string_representation = constituency_tree.string_representation
        self.maxDiff = None
        self.assertEqual(expected_leaves_raw_text, actual_leaves_raw_text)
        self.assertEqual(expected_string_representation, actual_string_representation)
    
    def test_string_representation_attribute(self):
        # Test parameters
        tree_line = "(TOP (S (NP (PRP$ Its) (NN president)) (VP (VBZ is) (NP (NNP Zheng) (NNP Bing))) (. .)))"
        tree = ConstituencyTree.parse(tree_line)
        # Expected result
        expected_result = "(TOP (S (NP (PRP$ Its) (NN president)) (VP (VBZ is) (NP (NNP Zheng) (NNP Bing))) (. .)))"
        # Actual_result
        actual_result = tree.string_representation
        # Test
        self.assertEqual(expected_result, actual_result)
    
    def test_get_nodes(self):
        # Test parameters
        tree_line = "(TOP (S (NP (PRP$ Its) (NN president)) (VP (VBZ is) (NP (NNP Zheng) (NNP Bing))) (. .)))"
        tree = ConstituencyTree.parse(tree_line)
        tags = set(["NP", "VBZ"])
        # Expected result
        leaves = tree.leaves
        expected_result = [leaves[0].parent.parent, leaves[2].parent, leaves[3].parent.parent]
        # Actual_result
        actual_result = tree.get_tagged_nodes(tags)
        # Test
        g = (expected_result[i] is actual_result[i] for i in range(max(len(expected_result), len(actual_result))))
        self.assertTrue(all(g))
    
    ##@unittest.skip
    def test_get_all_nodes(self):
        # Test parameters
        tree_line = "(TOP (S (NP (PRP$ Its) (NN president)) (VP (VBZ is) (NP (NNP Zheng) (NNP Bing))) (. .)))"
        tree = ConstituencyTree.parse(tree_line)
        # Expected result
        expected_result = [tree.root] + list(tree.root.children) + list(tree.root.children[0].children)
        for node in tree.root.children[0].children:
            expected_result += list(node.children)
        expected_result += list(tree.leaves[:3]) + [tree.leaves[3].parent, tree.leaves[4].parent] + list(tree.leaves[3:5])
        
        # Actual_result
        actual_result = tree.get_all_nodes()
        # Test
        g = (expected_result[i] is actual_result[i] for i in range(max(len(expected_result), len(actual_result))))
        self.assertTrue(all(g))
    
    ##@unittest.skip
    def test_depth(self):
        # Test parameters
        tree_line = "(TOP (S (NP (PRP$ Its) (NN president)) (VP (VBZ is) (NP (NNP Zheng) (NNP Bing))) (. .)))"
        tree = ConstituencyTree.parse(tree_line)
        # Expected result
        expected_result = 4
        
        # Actual_result
        actual_result = tree.depth
        # Test
        self.assertEqual(expected_result, actual_result)


def get_test_parse_tree_data():
    data = []
    
    # Without parentheses
    string_representation = "(TOP (S (NP (PRP$ Its) (NN president)) (VP (VBZ is) (NP (NNP Zheng) (NNP Bing))) (. .)))"
    
    its_node = ConstituencyTreeNode("Its")
    PRPdollar_node = ConstituencyTreeNode("PRP$", children=(its_node,))
    president_node = ConstituencyTreeNode("president")
    NN_node = ConstituencyTreeNode("NN", children=(president_node,))
    NP1_node = ConstituencyTreeNode("NP", children=(PRPdollar_node,NN_node,))
    is_node = ConstituencyTreeNode("is",)
    VBZ_node = ConstituencyTreeNode("VBZ", children=(is_node,))
    Zheng_node = ConstituencyTreeNode("Zheng")
    NNP1_node = ConstituencyTreeNode("NNP", children=(Zheng_node,))
    Bing_node = ConstituencyTreeNode("Bing")
    NNP2_node = ConstituencyTreeNode("NNP", children=(Bing_node,))
    NP2_node = ConstituencyTreeNode("NP", children=(NNP1_node,NNP2_node,))
    VP1_node = ConstituencyTreeNode("VP", children=(VBZ_node,NP2_node,))
    point_node = ConstituencyTreeNode(".")
    POINT1_node = ConstituencyTreeNode(".", children=(point_node,))
    sentence_node = ConstituencyTreeNode("S", children=(NP1_node, VP1_node, POINT1_node))
    top_node = ConstituencyTreeNode("TOP", children=(sentence_node,))
    expected_leaves_raw_text = "Its president is Zheng Bing ."
    actual_leaves_raw_text = top_node.get_leaves_raw_text()
    assert expected_leaves_raw_text == actual_leaves_raw_text
    expected_result = ConstituencyTree(top_node)
    
    data.append((string_representation, expected_result))
    
    
    # With parentheses
    s = "(TOP (S (NP (PRP$ Its) (NN president)) (VP (VBZ is) (NP (PUNCT {LRB:}) (NNP Zheng) (PUNCT {RRB:}) (NNP Bing))) (. .)))"
    string_representation = s.format(LRB=LRB_cortex_symbol, RRB=RRB_cortex_symbol)
    
    its_node = ConstituencyTreeNode("Its")
    PRPdollar_node = ConstituencyTreeNode("PRP$", children=(its_node,))
    president_node = ConstituencyTreeNode("president")
    NN_node = ConstituencyTreeNode("NN", children=(president_node,))
    NP1_node = ConstituencyTreeNode("NP", children=(PRPdollar_node,NN_node,))
    is_node = ConstituencyTreeNode("is",)
    VBZ_node = ConstituencyTreeNode("VBZ", children=(is_node,))
    LRB_node = ConstituencyTreeNode("(")
    PUNCT1_node = ConstituencyTreeNode("PUNCT", children=(LRB_node,))
    Zheng_node = ConstituencyTreeNode("Zheng")
    RRB_node = ConstituencyTreeNode(")")
    PUNCT2_node = ConstituencyTreeNode("PUNCT", children=(RRB_node,))
    NNP1_node = ConstituencyTreeNode("NNP", children=(Zheng_node,))
    Bing_node = ConstituencyTreeNode("Bing")
    NNP2_node = ConstituencyTreeNode("NNP", children=(Bing_node,))
    NP2_node = ConstituencyTreeNode("NP", children=(PUNCT1_node,NNP1_node,PUNCT2_node,NNP2_node,))
    VP1_node = ConstituencyTreeNode("VP", children=(VBZ_node,NP2_node,))
    point_node = ConstituencyTreeNode(".")
    POINT1_node = ConstituencyTreeNode(".", children=(point_node,))
    sentence_node = ConstituencyTreeNode("S", children=(NP1_node, VP1_node, POINT1_node))
    top_node = ConstituencyTreeNode("TOP", children=(sentence_node,))
    expected_leaves_raw_text = "Its president is ( Zheng ) Bing ."
    actual_leaves_raw_text = top_node.get_leaves_raw_text()
    assert expected_leaves_raw_text == actual_leaves_raw_text
    expected_result = ConstituencyTree(top_node)
    
    data.append((string_representation, expected_result))
    
    return data


def get_test_flush_data():
    data = []
    
    # Without parentheses
    tags = ["(TOP", "(S", "(NP", "(PRP$ Its)",  "(NN president))", "(VP", "(VBZ is)", "(NP", "(NNP Zheng)", "(NNP Bing)))", "(. .)))"]
    builder = ConstituencyTreeNodeCONLL2012StringRepresentationBuilder()
    for tag in tags:
        builder.add_tag(tag)
    expected_string_representation = "(TOP (S (NP (PRP$ Its) (NN president)) (VP (VBZ is) (NP (NNP Zheng) (NNP Bing))) (. .)))"
    data.append((builder, expected_string_representation))
    
    # With parentheses
    builder = ConstituencyTreeNodeCONLL2012StringRepresentationBuilder()
    builder.add_tag("(TOP(S(NP*", ("PRP$", "Its"))
    builder.add_tag("*)", ("NN", "president"))
    builder.add_tag("(VP*", ("VBZ", "is"))
    builder.add_tag("(NP*", ("PUNCT", "("))
    builder.add_tag("*", ("NNP", "Zheng"))
    builder.add_tag("*", ("PUNCT", ")"))
    builder.add_tag("*))", ("NNP", "Bing"))
    builder.add_tag("*))", (".", "."))
    s = "(TOP (S (NP (PRP$ Its) (NN president)) (VP (VBZ is) (NP (PUNCT {LRB:}) (NNP Zheng) (PUNCT {RRB:}) (NNP Bing))) (. .)))"
    expected_string_representation = s.format(LRB=LRB_cortex_symbol, RRB=RRB_cortex_symbol)
    data.append((builder, expected_string_representation))
    
    return data


def get_test_convert_constituency_tree_node_to_string_representation_data():
    data = []
    
    # Without parentheses
    its_node = ConstituencyTreeNode("Its")
    PRPdollar_node = ConstituencyTreeNode("PRP$", children=(its_node,))
    president_node = ConstituencyTreeNode("president")
    NN_node = ConstituencyTreeNode("NN", children=(president_node,))
    NP1_node = ConstituencyTreeNode("NP", children=(PRPdollar_node,NN_node,))
    
    is_node = ConstituencyTreeNode("is",)
    VBZ_node = ConstituencyTreeNode("VBZ", children=(is_node,))
    Zheng_node = ConstituencyTreeNode("Zheng")
    NNP1_node = ConstituencyTreeNode("NNP", children=(Zheng_node,))
    Bing_node = ConstituencyTreeNode("Bing")
    NNP2_node = ConstituencyTreeNode("NNP", children=(Bing_node,))
    NP2_node = ConstituencyTreeNode("NP", children=(NNP1_node,NNP2_node,))
    VP1_node = ConstituencyTreeNode("VP", children=(VBZ_node,NP2_node,))
    
    point_node = ConstituencyTreeNode(".")
    POINT1_node = ConstituencyTreeNode(".", children=(point_node,))
    
    sentence_node = ConstituencyTreeNode("S", children=(NP1_node, VP1_node, POINT1_node))
    
    top_node = ConstituencyTreeNode("TOP", children=(sentence_node,))
    expected_leaves_raw_text = "Its president is Zheng Bing ."
    actual_leaves_raw_text = top_node.get_leaves_raw_text()
    assert expected_leaves_raw_text == actual_leaves_raw_text
    
    args = (top_node,)
    kwargs = {}
    expected_string_representation = "(TOP (S (NP (PRP$ Its) (NN president)) (VP (VBZ is) (NP (NNP Zheng) (NNP Bing))) (. .)))"
    
    data.append(((args, kwargs), expected_string_representation))
    
    
    # With parentheses
    its_node = ConstituencyTreeNode("Its")
    PRPdollar_node = ConstituencyTreeNode("PRP$", children=(its_node,))
    president_node = ConstituencyTreeNode("president")
    NN_node = ConstituencyTreeNode("NN", children=(president_node,))
    NP1_node = ConstituencyTreeNode("NP", children=(PRPdollar_node,NN_node,))
    
    is_node = ConstituencyTreeNode("is",)
    VBZ_node = ConstituencyTreeNode("VBZ", children=(is_node,))
    LRB_node = ConstituencyTreeNode("(")
    PUNCT1_node = ConstituencyTreeNode("PUNCT", children=(LRB_node,))
    Zheng_node = ConstituencyTreeNode("Zheng")
    RRB_node = ConstituencyTreeNode(")")
    PUNCT2_node = ConstituencyTreeNode("PUNCT", children=(RRB_node,))
    NNP1_node = ConstituencyTreeNode("NNP", children=(Zheng_node,))
    Bing_node = ConstituencyTreeNode("Bing")
    NNP2_node = ConstituencyTreeNode("NNP", children=(Bing_node,))
    NP2_node = ConstituencyTreeNode("NP", children=(PUNCT1_node,NNP1_node,PUNCT2_node,NNP2_node,))
    VP1_node = ConstituencyTreeNode("VP", children=(VBZ_node,NP2_node,))
    
    point_node = ConstituencyTreeNode(".")
    POINT1_node = ConstituencyTreeNode(".", children=(point_node,))
    
    sentence_node = ConstituencyTreeNode("S", children=(NP1_node, VP1_node, POINT1_node))
    
    top_node = ConstituencyTreeNode("TOP", children=(sentence_node,))
    expected_leaves_raw_text = "Its president is ( Zheng ) Bing ."
    actual_leaves_raw_text = top_node.get_leaves_raw_text()
    assert expected_leaves_raw_text == actual_leaves_raw_text
    
    args = (top_node,)
    kwargs = {}
    s = "(TOP (S (NP (PRP$ Its) (NN president)) (VP (VBZ is) (NP (PUNCT {LRB:}) (NNP Zheng) (PUNCT {RRB:}) (NNP Bing))) (. .)))"
    expected_string_representation = s.format(LRB=LRB_cortex_symbol, RRB=RRB_cortex_symbol)
    data.append(((args, kwargs), expected_string_representation))
    
    return data



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()