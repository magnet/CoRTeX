# -*- coding: utf-8 -*-

import unittest
from collections import OrderedDict


from cortex.api.document import Document
from cortex.api.document_buffer import (DocumentBuffer, transfer_coreference_partition_info, 
                                          deepcopy_document,
                                          )
from cortex.api.markable import Token, NamedEntity, Mention, Sentence, Quotation
from cortex.api.constituency_tree import ConstituencyTree
from cortex.api.entity import Entity
from cortex.api.coreference_partition import CoreferencePartition


from cortex.parameters.mention_data_tags import (UNKNOWN_VALUE_TAG, 
                                                  EXPANDED_PRONOUN_GRAM_TYPE_TAG, NAME_GRAM_TYPE_TAG, 
                                                  NOMINAL_GRAM_TYPE_TAG, VERB_GRAM_TYPE_TAG, 
                                                  EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS, 
                                                  NAME_GRAM_TYPE_SUBTYPE_TAGS, 
                                                  NOMINAL_GRAM_TYPE_SUBTYPE_TAGS, 
                                                  SINGULAR_NUMBER_TAG, PLURAL_NUMBER_TAG, 
                                                  MALE_GENDER_TAG, FEMALE_GENDER_TAG, 
                                                  NEUTRAL_GENDER_TAG, FIRST_PERSON_TAG, 
                                                  SECOND_PERSON_TAG, THIRD_PERSON_TAG)

from cortex.languages.english.parameters.ontonotes_v5_named_entities_data_tags import (PERSON_NAMED_ENTITY_TYPE_TAGS, 
                                                                                         ORGANIZATION_NAMED_ENTITY_TYPE_TAGS, 
                                                                                         LOCATION_NAMED_ENTITY_TYPE_TAGS,
                                                                                         ARTIFACT_NAMED_ENTITY_TYPE_TAGS)

PERSON_NAMED_ENTITY_TYPE_TAG = PERSON_NAMED_ENTITY_TYPE_TAGS[0]
ORGANIZATION_NAMED_ENTITY_TYPE_TAG = ORGANIZATION_NAMED_ENTITY_TYPE_TAGS[0] 
LOCATION_NAMED_ENTITY_TYPE_TAG = LOCATION_NAMED_ENTITY_TYPE_TAGS[0]
ARTIFACT_NAMED_ENTITY_TYPE_TAG = ARTIFACT_NAMED_ENTITY_TYPE_TAGS[0]

#@unittest.skip
class TestDocumentBuffer(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        document, expected_result = get_test___init__data()
        
        # Test
        actual_result = DocumentBuffer(document)
        self.assertEqual(expected_result, actual_result)
        
    #@unittest.skip
    def test__add_markables(self):
        # Test parameters
        data = get_test__add_markables_test_data()
        
        # Tests
        for markables_data, markable_class, test_document_buffer, expected_document_buffer in data:
            self.assertNotEqual(expected_document_buffer, test_document_buffer)
            test_document_buffer._add_markables(markables_data, markable_class)
            value, message = expected_document_buffer._inner_eq(test_document_buffer)
            self.assertTrue(value, message)
    
    #@unittest.skip
    def test_add_named_entities(self):
        # Test parameters
        named_entities_data, test_document_buffer, expected_document_buffer = get_test_add_named_entities_test_data()
        
        # Test
        self.assertNotEqual(expected_document_buffer, test_document_buffer)
        test_document_buffer.add_named_entities(named_entities_data)
        value, message = expected_document_buffer._inner_eq(test_document_buffer)
        self.assertTrue(value, message)
    
    #@unittest.skip
    def test_add_mentions(self):
        # Test parameters
        mentions_data, test_document_buffer, expected_document_buffer = get_test_add_mentions_test_data()
        
        # Test
        self.assertNotEqual(expected_document_buffer, test_document_buffer)
        test_document_buffer.add_mentions(mentions_data)
        value, message = expected_document_buffer._inner_eq(test_document_buffer)
        self.assertTrue(value, message)
    
    #@unittest.skip
    def test_add_quotations(self):
        # Test parameters
        quotations_data, test_document_buffer, expected_document_buffer = get_test_add_quotations_test_data()
        
        # Test
        self.assertNotEqual(expected_document_buffer, test_document_buffer)
        test_document_buffer.add_quotations(quotations_data)
        value, message = expected_document_buffer._inner_eq(test_document_buffer)
        self.assertTrue(value, message)
    
    #@unittest.skip
    def test__remove_markables(self):
        # Test parameters
        data = get_test__remove_markables_test_data()
        
        # Test named entities
        for markable_extents, markable_class, test_document_buffer, expected_document_buffer in data:
            self.assertNotEqual(expected_document_buffer, test_document_buffer)
            test_document_buffer._remove_markables(markable_extents, markable_class)
            value, message = expected_document_buffer._inner_eq(test_document_buffer)
            self.assertTrue(value, message)
    
    #@unittest.skip
    def test_remove_named_entities(self):
        # Test parameters
        named_entity_extents, test_document_buffer, expected_document_buffer = get_test_remove_named_entities_test_data()
        
        # Test
        self.assertNotEqual(expected_document_buffer, test_document_buffer)
        test_document_buffer.remove_named_entities(named_entity_extents)
        value, message = expected_document_buffer._inner_eq(test_document_buffer)
        self.assertTrue(value, message)
    
    #@unittest.skip
    def test_remove_mentions(self):
        # Test parameters
        mention_extents, test_document_buffer, expected_document_buffer = get_test_remove_mentions_test_data()
        
        # Test
        self.assertNotEqual(expected_document_buffer, test_document_buffer)
        test_document_buffer.remove_mentions(mention_extents)
        value, message = expected_document_buffer._inner_eq(test_document_buffer)
        self.assertTrue(value, message)
    
    #@unittest.skip
    def test__flush_modify_markables(self):
        # Test parameters
        document_buffer, markable_classes, test_document, expected_document = get_test__flush_modify_markables_test_data()
        
        # Test
        for markable_class in markable_classes:
            attribute_name = "_{}".format(markable_class._NAME["plr"])
            expected_markables = getattr(expected_document, attribute_name)
            actual_markables = getattr(test_document, attribute_name)
            self.assertNotEqual(expected_markables, actual_markables)
        for markable_class in markable_classes:
            document_buffer._flush_modify_markables(markable_class)
        for markable_class in markable_classes:
            attribute_name = "_{}".format(markable_class._NAME["plr"])
            expected_markables = getattr(expected_document, attribute_name)
            actual_markables = getattr(test_document, attribute_name)
            self.assertEqual(expected_markables, actual_markables)
    
    #@unittest.skip
    def test__synchronize_tokens_sentences(self):
        def _test_tokens_sentences_synchronization(test_document, expected_document):
            """
            :return: a (boolean, string) pair; the string specifies why the boolean is False if it is
            """
            test_sentences = test_document.sentences
            expected_sentences = expected_document.sentences
            
            if len(test_sentences) != len(expected_sentences):
                message = "'sentences' of the test document does not have the same length () as "\
                          "'sentences' of the expected document '{}'"
                message = message.format(len(test_sentences), len(expected_sentences))
                return False, message
            
            for i, (test_sentence, expected_sentence) in enumerate(zip(test_sentences, expected_sentences)):
                if test_sentence.extent != expected_sentence.extent:
                    message = "sentence n°{}: test sentence's extent '{}' is different from expected's '{}'"
                    message = message.format(i, test_sentence.extent, expected_sentence.extent)
                    return False, message
                
                test_tokens, expected_tokens = test_sentence.tokens, expected_sentence.tokens
                if test_tokens is None:
                    if expected_tokens is not None:
                        return False, "sentence n°{}: expected 'tokens' is None"
                elif len(test_tokens) != len(expected_tokens):
                    message = "sentence n°{}: expected 'tokens' does not have the same length '{}' as expected 'tokens' '{}'"
                    message = message.format(i, len(test_tokens), len(expected_tokens))
                    return False, message
                
                for j, (test_token, expected_token, test_constituency_tree_node) in enumerate(zip(test_tokens, expected_tokens, test_sentence.constituency_tree.leaves)):
                    if test_token.extent != expected_token.extent:
                        return False, "token n°{}'s extent (sentence n°{}): test = {}, expected = {}".format(j, i, test_token.extent, expected_token.extent)
                    if test_token.sentence is not test_sentence:
                        return False, "token n°{} (sentence n°{}): 'sentence' attribute is not the one expected".format(j, i)
                    if test_token.constituency_tree_node is not test_constituency_tree_node:
                        return False, "token n°{} (sentence n°{}): 'constituency_tree' attribute is not the one expected".format(j, i)
                
            return True, None
                
        # Test parameters
        test_document, _, _, _, _ = get_test_data(synchronize=False, find_and_synchronize_heads=False) #expected_document, modify_named_entities_data, modify_mentions_data, quotations_to_add_data
        expected_document, _, _, _, _ = get_test_data(synchronize=True, find_and_synchronize_heads=False) #expected_document, modify_named_entities_data, modify_mentions_data, quotations_to_add_data
        
        # Test
        self.assertFalse(*_test_tokens_sentences_synchronization(test_document, expected_document))
        document_buffer = DocumentBuffer(test_document)
        document_buffer._synchronize_tokens_sentences()
        self.assertTrue(*_test_tokens_sentences_synchronization(test_document, expected_document))
    
    #@unittest.skip
    def test__synchronize_mentions_sentences(self):
        def _test_mentions_sentences_synchronization(test_document, expected_document):
            """
            :return: a (boolean, string) pair; the string specifies why the boolean is False if it is
            """
            test_sentences = test_document.sentences
            expected_sentences = expected_document.sentences
            
            if len(test_sentences) != len(expected_sentences):
                message = "'sentences' of the test document does not have the same length () as "\
                          "'sentences' of the expected document '{}'"
                message = message.format(len(test_sentences), len(expected_sentences))
                return False, message
            
            for i, (test_sentence, expected_sentence) in enumerate(zip(test_sentences, expected_sentences)):
                if test_sentence.extent != expected_sentence.extent:
                    message = "sentence n°{}: test sentence's extent '{}' is different from expected's '{}'"
                    message = message.format(i, test_sentence.extent, expected_sentence.extent)
                    return False, message
                
                test_mentions, expected_mentions = test_sentence.mentions, expected_sentence.mentions
                if test_mentions is None:
                    if expected_mentions is not None:
                        return False, "sentence n°{}: expected 'mentions' is None"
                elif len(test_mentions) != len(expected_mentions):
                    message = "sentence n°{}: expected 'mentions' does not have the same length '{}' as expected 'mentions' '{}'"
                    message = message.format(i, len(test_mentions), len(expected_mentions))
                    return False, message
            
                for j, (test_mention, expected_mention) in enumerate(zip(test_mentions, expected_mentions)):
                    if test_mention.extent != expected_mention.extent:
                        return False, "mention n°{}'s extent (sentence n°{}): test = {}, expected = {}".format(j, i, test_mention.extent, expected_mention.extent)
                    if test_mention.sentence is not test_sentence:
                        return False, "mention n°{} (sentence n°{}): 'sentence' attribute is not the one expected".format(j, i)
                
            return True, None
                
        # Test parameters
        test_document, _, _, _, _ = get_test_data(synchronize=False, find_and_synchronize_heads=False) #expected_document, modify_named_entities_data, modify_mentions_data, quotations_to_add_data
        expected_document, _, _, _, _ = get_test_data(synchronize=True, find_and_synchronize_heads=False) #expected_document, modify_named_entities_data, modify_mentions_data, quotations_to_add_data
        
        # Test
        self.assertFalse(*_test_mentions_sentences_synchronization(test_document, expected_document))
        document_buffer = DocumentBuffer(test_document)
        document_buffer._synchronize_mentions_sentences()
        self.assertTrue(*_test_mentions_sentences_synchronization(test_document, expected_document))
    
    #@unittest.skip
    def test__synchronize_tokens_mentions(self):
        def _test_tokens_mentions_synchronization(test_document, expected_document):
            """
            :return: a (boolean, string) pair; the string specifies why the boolean is False if it is
            """
            test_mentions = test_document.mentions
            expected_mentions = expected_document.mentions
            
            if len(test_mentions) != len(expected_mentions):
                message = "'mentions' of the test document does not have the same length () as "\
                          "'mentions' of the expected document '{}'"
                message = message.format(len(test_mentions), len(expected_mentions))
                return False, message
            
            for i, (test_mention, expected_mention) in enumerate(zip(test_mentions, expected_mentions)):
                if test_mention.extent != expected_mention.extent:
                    message = "mention n°{}: test mention's extent '{}' is different from expected's '{}'"
                    message = message.format(i, test_mention.extent, expected_mention.extent)
                    return False, message
                
                test_tokens, expected_tokens = test_mention.tokens, expected_mention.tokens
                if test_tokens is None:
                    if expected_tokens is not None:
                        return False, "mention n°{}: expected 'tokens' is None"
                elif len(test_tokens) != len(expected_tokens):
                    message = "mention n°{}: expected 'tokens' does not have the same length '{}' as expected 'tokens' '{}'"
                    message = message.format(i, len(test_tokens), len(expected_tokens))
                    return False, message
            
                for j, (test_token, expected_token) in enumerate(zip(test_tokens, expected_tokens)):
                    if test_token.extent != expected_token.extent:
                        return False, "token n°{}'s extent (mention n°{}): test = {}, expected = {}".format(j, i, test_token.extent, expected_token.extent)
                
            return True, None
                
        # Test parameters
        test_document, _, _, _, _ = get_test_data(synchronize=False, find_and_synchronize_heads=False) #expected_document, modify_named_entities_data, modify_mentions_data, quotations_to_add_data
        expected_document, _, _, _, _ = get_test_data(synchronize=True, find_and_synchronize_heads=False) #expected_document, modify_named_entities_data, modify_mentions_data, quotations_to_add_data
        
        # Test
        self.assertFalse(*_test_tokens_mentions_synchronization(test_document, expected_document))
        document_buffer = DocumentBuffer(test_document)
        document_buffer._synchronize_tokens_mentions()
        self.assertTrue(*_test_tokens_mentions_synchronization(test_document, expected_document))
    
    #@unittest.skip
    def test__synchronize_head_tokens_mentions(self):
        def _test_head_tokens_mentions_synchronization(test_document, expected_document):
            """
            :return: a (boolean, string) pair; the string specifies why the boolean is False if it is
            """
            test_mentions = test_document.mentions
            expected_mentions = expected_document.mentions
            
            if len(test_mentions) != len(expected_mentions):
                message = "'mentions' of the test document does not have the same length () as "\
                          "'mentions' of the expected document '{}'"
                message = message.format(len(test_mentions), len(expected_mentions))
                return False, message
            
            for i, (test_mention, expected_mention) in enumerate(zip(test_mentions, expected_mentions)):
                if test_mention.extent != expected_mention.extent:
                    message = "mention n°{}: test mention's extent '{}' is different from expected's '{}'"
                    message = message.format(i, test_mention.extent, expected_mention.extent)
                    return False, message
                
                test_head_tokens, expected_head_tokens = test_mention.head_tokens, expected_mention.head_tokens
                if test_head_tokens is None:
                    if expected_head_tokens is not None:
                        return False, "mention n°{}: expected 'head_tokens' is None"
                elif len(test_head_tokens) != len(expected_head_tokens):
                    message = "mention n°{}: expected 'head_tokens' does not have the same length '{}' as expected 'head_tokens' '{}'"
                    message = message.format(i, len(test_head_tokens), len(expected_head_tokens))
                    return False, message
            
                for j, (test_head_token, expected_head_token) in enumerate(zip(test_head_tokens, expected_head_tokens)):
                    if test_head_token.extent != expected_head_token.extent:
                        return False, "head_token n°{}'s extent (mention n°{}): test = {}, expected = {}".format(j, i, test_head_token.extent, expected_head_token.extent)
                
            return True, None
                
        # Test parameters
        test_document, _, _, _, _ = get_test_data(synchronize=False, find_and_synchronize_heads=False) #expected_document, modify_named_entities_data, modify_mentions_data, quotations_to_add_data
        expected_document, _, _, _, _ = get_test_data(synchronize=True, find_and_synchronize_heads=True) #expected_document, modify_named_entities_data, modify_mentions_data, quotations_to_add_data
        strict = True
        
        # Test
        self.assertFalse(*_test_head_tokens_mentions_synchronization(test_document, expected_document))
        document_buffer = DocumentBuffer(test_document)
        document_buffer._synchronize_tokens_mentions()
        document_buffer._synchronize_head_tokens_mentions(strict=strict)
        self.assertTrue(*_test_head_tokens_mentions_synchronization(test_document, expected_document))
    
    #@unittest.skip
    def test__synchronize_named_entities_tokens(self):
        def _test_named_entities_tokens_synchronization(test_document, expected_document):
            """
            :return: a (boolean, string) pair; the string specifies why the boolean is False if it is
            """
            test_tokens = test_document.tokens
            expected_tokens = expected_document.tokens
            
            if len(test_tokens) != len(expected_tokens):
                message = "'tokens' of the test document does not have the same length () as "\
                          "'tokens' of the expected document '{}'"
                message = message.format(len(test_tokens), len(expected_tokens))
                return False, message
            
            for i, (test_token, expected_token) in enumerate(zip(test_tokens, expected_tokens)):
                if test_token.extent != expected_token.extent:
                    message = "token n°{}: test token's extent '{}' is different from expected's '{}'"
                    message = message.format(i, test_token.extent, expected_token.extent)
                    return False, message
                
                if test_token.named_entity != expected_token.named_entity:
                    return False, "named_entity on token n°{}".format(i)
                
            return True, None
                
        # Test parameters
        test_document, _, _, _, _ = get_test_data(synchronize=False, find_and_synchronize_heads=False) #expected_document, modify_named_entities_data, modify_mentions_data, quotations_to_add_data
        expected_document, _, _, _, _ = get_test_data(synchronize=True, find_and_synchronize_heads=False) #expected_document, modify_named_entities_data, modify_mentions_data, quotations_to_add_data
        
        # Test
        self.assertFalse(*_test_named_entities_tokens_synchronization(test_document, expected_document))
        document_buffer = DocumentBuffer(test_document)
        document_buffer._synchronize_named_entities_tokens()
        self.assertTrue(*_test_named_entities_tokens_synchronization(test_document, expected_document))
    
    #@unittest.skip
    def test__synchronize_named_entities_mentions(self):
        def _test_named_entities_mentions_synchronization(test_document, expected_document):
            """
            :return: a (boolean, string) pair; the string specifies why the boolean is False if it is
            """
            test_mentions = test_document.mentions
            expected_mentions = expected_document.mentions
            
            if len(test_mentions) != len(expected_mentions):
                message = "'mentions' of the test document does not have the same length () as "\
                          "'mentions' of the expected document '{}'"
                message = message.format(len(test_mentions), len(expected_mentions))
                return False, message
            
            for i, (test_mention, expected_mention) in enumerate(zip(test_mentions, expected_mentions)):
                if test_mention.extent != expected_mention.extent:
                    message = "mention n°{}: test mention's extent '{}' is different from expected's '{}'"
                    message = message.format(i, test_mention.extent, expected_mention.extent)
                    return False, message
                
                if test_mention.named_entity != expected_mention.named_entity:
                    return False, "named_entity on mention n°{}".format(i)
                
            return True, None
                
        # Test parameters
        test_document, _, _, _, _ = get_test_data(synchronize=False, find_and_synchronize_heads=False) #expected_document, modify_named_entities_data, modify_mentions_data, quotations_to_add_data
        expected_document, _, _, _, _ = get_test_data(synchronize=True, find_and_synchronize_heads=False) #expected_document, modify_named_entities_data, modify_mentions_data, quotations_to_add_data
        
        # Test
        self.assertFalse(*_test_named_entities_mentions_synchronization(test_document, expected_document))
        document_buffer = DocumentBuffer(test_document)
        document_buffer._synchronize_named_entities_mentions()
        self.assertTrue(*_test_named_entities_mentions_synchronization(test_document, expected_document))
    
    #@unittest.skip
    def test__synchronize_quotation_with_tokens_and_mentions(self):
        def _test_quotations_synchronization(test_document, expected_document):
            """
            :return: a (boolean, string) pair; the string specifies why the boolean is False if it is
            """
            test_quotations = test_document.quotations
            expected_quotations = expected_document.quotations
            
            if len(test_quotations) != len(expected_quotations):
                message = "'quotations' of the test document does not have the same length () as "\
                          "'quotations' of the expected document '{}'"
                message = message.format(len(test_quotations), len(expected_quotations))
                return False, message
            
            def _return_false_quotation_mentions(i, test_mentions, expected_mentions):
                message = "quotation n°{}: test quotation's mentions collection '{}' is different from expected's '{}'"
                message = message.format(i, test_mentions, expected_mentions)
                return False, message
            def _test(expected_mention, test_mention, message, i):
                if expected_mention is None and test_mention is None:
                    pass
                elif expected_mention is None or test_mention is None:
                    return False, message.format(i)
                
                
            for i, (test_quotation, expected_quotation) in enumerate(zip(test_quotations, expected_quotations)):
                if test_quotation.extent != expected_quotation.extent:
                    message = "quotation n°{}: test quotation's extent '{}' is different from expected's '{}'"
                    message = message.format(i, test_quotation.extent, expected_quotation.extent)
                    return False, message
                
                if expected_quotation.mentions is None:
                    if test_quotation.mentions is not None:
                        return _return_false_quotation_mentions(i, test_quotation.mentions, expected_quotation.mentions)
                else:
                    if test_quotation.mentions is None:
                        return _return_false_quotation_mentions(i, test_quotation.mentions, expected_quotation.mentions)
                    if tuple(m.extent for m in test_quotation.mentions) != tuple(m.extent for m in expected_quotation.mentions):
                        return _return_false_quotation_mentions(i, test_quotation.mentions, expected_quotation.mentions)
                
                
                # Speaker mention
                test = _test(expected_quotation.speaker_mention, test_quotation.speaker_mention, "speaker_mention on quotation n°{}", i)
                if test is not None:
                    return test
                
                # Speaker verb token
                test = _test(expected_quotation.speaker_verb_token, test_quotation.speaker_verb_token, "speaker_verb_token on quotation n°{}", i)
                if test is not None:
                    return test
                
                # Interlocutor mention
                test = _test(expected_quotation.interlocutor_mention, test_quotation.interlocutor_mention, "interlocutor_mention on quotation n°{}", i)
                if test is not None:
                    return test
                
            return True, None
                
        # Test parameters
        test_document, _, _, _, _ = get_test_data(synchronize=False, find_and_synchronize_heads=False) #expected_document, modify_named_entities_data, modify_mentions_data, quotations_to_add_data
        expected_document, _, _, _, _ = get_test_data(synchronize=True, find_and_synchronize_heads=False) #expected_document, modify_named_entities_data, modify_mentions_data, quotations_to_add_data
        
        # Test
        self.assertFalse(*_test_quotations_synchronization(test_document, expected_document))
        document_buffer = DocumentBuffer(test_document)
        document_buffer._synchronize_quotation_with_tokens_and_mentions()
        self.assertTrue(*_test_quotations_synchronization(test_document, expected_document))
    
    #@unittest.skip
    def test_flush(self):
        # Test1
        ## Test parameters
        data = get_test_flush_test_data()
        
        # Test1
        document_buffer, kwargs, _ = data[-1] #expected_document
        with self.assertRaises(ValueError):            
            _ = document_buffer.flush(**kwargs) #actual_document
        
        ## Test2
        data = data[:-1]
        for document_buffer, kwargs, expected_document in data:
            actual_document = document_buffer.flush(**kwargs)
            self.assertTrue(*expected_document._inner_eq(actual_document))
    
    #@unittest.skip
    def test_deepcopy_document(self):
        # Test parameters
        copy_document_ident = "test_copy"
        strict = True
        _, document, _, _, _ = get_test_data(synchronize=True, find_and_synchronize_heads=True) # test_document, modify_named_entities_data, modify_mentions_data, quotations_to_add_data
        
        # Test
        copy_document = deepcopy_document(document, copy_document_ident=copy_document_ident, strict=strict)
        ## Check that content is equal
        self.assertEqual(copy_document_ident, copy_document.ident)
        self.assertTrue(*document._inner_eq(copy_document))
        ## Check that content instances are different
        self.assertFalse(copy_document.info is document.info)
        for markables_name in ("tokens", "named_entities", "sentences", "mentions", "quotations"):
            markables_original_document = getattr(document, markables_name)
            markables_copy_document = getattr(copy_document, markables_name)
            for original_markable, copy_markable in zip(markables_original_document, markables_copy_document):
                self.assertFalse(copy_markable is original_markable)
        self.assertFalse(copy_document.coreference_partition is document.coreference_partition)
    
    
    #@unittest.skip
    def test_transfer_coreference_partition_info(self):
        # Test parameter
        source_document, destination_document, expected_coreference_partition = get_test_transfer_coreference_partition_info_data()
        
        # Test
        self.assertNotEqual(expected_coreference_partition, destination_document.coreference_partition)
        transfer_coreference_partition_info(source_document, destination_document)
        self.assertEqual(expected_coreference_partition, destination_document.coreference_partition)



def get_test_transfer_coreference_partition_info_data():
    raw_text = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    source_document = Document("source_doc", raw_text)
    destination_document = Document("destination_doc", raw_text)
    
    source_mention_extents = ((1,2),
                              (3,4),
                              (5,6),
                              (7,8),
                              (9,10),
                              (11,12),
                              (13,14),
                              (15,16),
                              )
    source_pseudo_entities = (((3,4),(7,8),(11,12)),
                              ((5,6),(9,10),(13,14)),
                              ((1,2),(15,16)),
                              )
    destination_mention_extents = ((7,8),
                                   (9,10),
                                   (11,12),
                                   (13,14),
                                   (15,16),
                                   (17,18),
                                   (19,20),
                                   (21,22),
                                   (23,24),
                                  )
    source_mention_data = {extent: {"ident": str(i)} for i, extent in enumerate(source_mention_extents)}
    source_coreference_partition = CoreferencePartition(entities=source_pseudo_entities, 
                                                        mention_extents=source_mention_extents)
    
    destination_mention_data = {extent: {"ident": str(i)} for i, extent in enumerate(destination_mention_extents)}
    
    document_buffer = DocumentBuffer(source_document)
    document_buffer.add_mentions(source_mention_data)
    document_buffer.set_coreference_partition(source_coreference_partition)
    source_document = document_buffer.flush()
    
    document_buffer.initialize_anew(destination_document)
    document_buffer.add_mentions(destination_mention_data)
    destination_document = document_buffer.flush()
    
    expected_coreference_partition = CoreferencePartition(entities=(((7,8),(11,12)),
                                                                    ((9,10),(13,14)),
                                                                    ((15,16),),
                                                                    ), 
                                                          mention_extents=destination_mention_extents)
    
    
    return source_document, destination_document, expected_coreference_partition



def _assemble_document_data(document, sentences_data, synchronize=True, find_and_synchronize_heads=False, 
                            flush_synchronize=False):
    expected_entities = {}
    sentences = []
    named_entities_list = []
    all_quotations = OrderedDict()
    for sentence_data in sentences_data:
        (ident, extent, raw_text, constituency_tree_string, speaker, predicate_arguments, 
         named_entities_data, tokens_data, mentions_data, quotations_data) = sentence_data
        sentence = Sentence(ident, extent, document, raw_text=raw_text)
        
        # Mentions
        mentions = OrderedDict()
        for mention_data in mentions_data:
            (extent, head_extent, entity_id, raw_text, gram_type, gram_subtype, number, gender, 
             referential_probability, wn_synonyms, wn_hypernyms) = mention_data
            ident = "{e[0]},{e[1]}".format(e=extent, he=head_extent)
            mention = Mention(ident, extent, document, raw_text=raw_text)
            mention.head_extent = head_extent
            mention.gram_type, mention.gram_subtype = gram_type, gram_subtype
            mention.number, mention.gender = number, gender
            mention.referential_probability = referential_probability
            mention.wn_synonyms = wn_synonyms
            mention.wn_hypernyms = wn_hypernyms
            if entity_id is not None:
                if entity_id not in expected_entities:
                    entity = Entity(ident=entity_id)
                    expected_entities[entity_id] = entity
                expected_entities[entity_id].add(mention.extent)
            mentions[extent] = mention
        mentions_list = list(mentions.values())
        
        # Tokens
        tokens = OrderedDict()
        for token_data in tokens_data:
            ident, extent, raw_text, lemma, POS_tag = token_data
            token = Token(ident, extent, document, raw_text=raw_text)
            token.lemma = lemma
            token.POS_tag = POS_tag
            tokens[extent] = token
        tokens_list = list(tokens.values())
        
        # Named entities
        named_entities = OrderedDict()
        for named_entity_data in named_entities_data:
            ident, extent, raw_text, type_, subtype, origin = named_entity_data
            named_entity = NamedEntity(ident, extent, document, type_, raw_text=raw_text, 
                                       subtype=subtype, origin=origin)
            named_entities[extent] = named_entity
        named_entities_list.extend(list(named_entities.values()))
        
        # Quotations
        quotations_data_ = OrderedDict()
        for quotation_data in quotations_data:
            extent, raw_text, speaker_mention_extent, speaker_verb_token_extent, interlocutor_mention_extent, mentions_extents = quotation_data
            ident = "{e[0]},{e[1]}".format(e=extent)
            quotation = Quotation(ident, extent, document, raw_text=raw_text, 
                                  speaker_mention_extent=speaker_mention_extent, 
                                  speaker_verb_token_extent=speaker_verb_token_extent, 
                                  interlocutor_mention_extent=interlocutor_mention_extent)
            quotations_data_[extent] = (quotation, mentions_extents)
        all_quotations.update((extent, q) for extent, (q, _) in quotations_data_.items())
        
        # ConstituencyTree
        sentence.constituency_tree = ConstituencyTree.parse(constituency_tree_string)
        
        # Speaker
        sentence.speaker = speaker
        
        # Predicate arguments
        sentence.predicate_arguments = predicate_arguments
        
        # Synchronize
        if synchronize:
            # Synchronization tokens <-> mentions
            for (ms, me), mention in mentions.items():
                mention.tokens = list(t for (ts, te), t in tokens.items() if ms <= ts and te <= me)
                if find_and_synchronize_heads:
                    mhs, mhe = mention.head_extent
                    mention.head_tokens = list(t for (ts, te), t in tokens.items() if mhs <= ts and te <= mhe)
            
            # Synchronization named_entities <-> mentions
            for extent, mention in mentions.items():
                if extent in named_entities:
                    mention.named_entity = named_entities.get(extent)
            
            # Synchronization named_entities <-> tokens
            for extent, token in tokens.items():
                token.named_entity = named_entities.get(extent)
            
            # Synchronization tokens <-> constituency tree leaves
            assert len(sentence.constituency_tree.leaves) == len(tokens), "Arg."
            for i, node in enumerate(sentence.constituency_tree.leaves):
                token = tokens_list[i]
                node.token = token
                token.constituency_tree_node = node
            
            # Synchronization quotations
            for quotation, mentions_extents in quotations_data_.values():
                quotation_mentions = []
                if mentions_extents is not None:
                    for mention_extent in mentions_extents:
                        mention = mentions[mention_extent]
                        quotation_mentions.append(mention)
                    quotation.mentions = quotation_mentions
                if quotation.speaker_mention_extent is not None and quotation.speaker_mention_extent in mentions:
                    quotation.speaker_mention = mentions[quotation.speaker_mention_extent]
                if quotation.speaker_verb_token_extent is not None:
                    quotation.speaker_verb = tokens[quotation.speaker_verb_token_extent]
                if quotation.interlocutor_mention_extent is not None and quotation.interlocutor_mention_extent in mentions:
                    quotation.interlocutor_mention = mentions[quotation.interlocutor_mention_extent]
        
        elif flush_synchronize:
            # Synchronization tokens <-> constituency tree leaves
            assert len(sentence.constituency_tree.leaves) == len(tokens), "Arg."
            for i, node in enumerate(sentence.constituency_tree.leaves):
                token = tokens_list[i]
                node.token = token
                token.constituency_tree_node = node
        
        # Set sentence's data
        sentence.mentions = tuple(mentions_list)
        sentence.tokens = tuple(tokens_list)
        
        # Finally
        sentences.append(sentence)
    
    # Add coreference partition data
    coreference_partition = None
    if expected_entities:
        coreference_partition = CoreferencePartition(entities=expected_entities.values())
    
    ## Finally, set the test_document's data
    document.tokens = tuple(token for sentence in sentences for token in sentence.tokens)
    document.mentions = tuple(mention for sentence in sentences for mention in sentence.mentions)
    document.sentences = tuple(sentences)
    document.named_entities = tuple(sorted(named_entities_list))
    document.quotations = tuple(sorted(all_quotations.values()))
    document.coreference_partition = coreference_partition

def create_base_test_data():
    # Define document data
    raw_text = "\nI say to you '' This is good '' . You say to him '' That is bad '' . He says to me '' This is bad '' ."
    
    sentences_data = []
    expected_sentences_data = []
    named_entities_to_add_data = {}
    named_entities_to_remove_extents = []
    mentions_to_add_data = {}
    mentions_to_remove_extents = []
    quotations_to_add_data = {}
    
    ## First sentence
    sentence_data = ["1", (1,33), "I say to you '' This is good '' .", 
                     "(ROOT (S (NP (PRP I)) (VP (VBP say) (PP (TO to) (NP (PRP you) ('' ''))) (NP (NP (DT This)) (SBAR (S (VP (VBZ is) (ADJP (JJ good)))))) ('' '')) (. .)))",
                     "speaker#1", 
                     (((1, 1, "ARG0"), (3, 5, "V"), (10, 12, "ARG2"), (17, 28, "ARG1")),
                     )
                    ]
    expected_sentence_data = list(sentence_data)
    
    sentence_tokens_data = [("1-1", (1,1), "I", "I", "PRP"), 
                            ("1-2", (3,5), "say", "say", "VBP"), 
                            ("1-3", (7,8), "to", "to", "TO"), 
                            ("1-4", (10,12), "you", "you", "PRP"), 
                            ("1-5", (14,15), "''", "''", "''"), 
                            ("1-6", (17,20), "This", "this", "DT"), 
                            ("1-7", (22,23), "is", "be", "VBZ"), 
                            ("1-8", (25,28), "good", "good", "JJ"), 
                            ("1-9", (30,31), "''", "''", "''"), 
                            ("1-10", (33,33), ".", ".", "."), 
                            ]
    expected_sentence_tokens_data = list(sentence_tokens_data)
    
    named_entities_data = [("1,1", (1,1), "I", "PERS", UNKNOWN_VALUE_TAG, "test"),
                           #("10,12", (10,12), "you", "PERS", UNKNOWN_VALUE_TAG, "test"),
                           ]
    expected_named_entities_data = [#("1,1", (1,1), "I", "PERS", UNKNOWN_VALUE_TAG, "test"),
                                    ("10,12", (10,12), "you", "PERS", UNKNOWN_VALUE_TAG, "test"),
                                    ]
    named_entities_to_add_data[(10,12)] = {"ident": "10,12", "extent": (10,12), "raw_text": "you", 
                                           "type": "PERS", "subtype": UNKNOWN_VALUE_TAG, "origin": "test"}
    named_entities_to_remove_extents.append((1,1))
    
    mentions_data = [((1,1), (1,1), 1, "I", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], SINGULAR_NUMBER_TAG, UNKNOWN_VALUE_TAG, None, [], []), 
                     #((10,12), (10,12), 2, "you", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], SINGULAR_NUMBER_TAG, UNKNOWN_VALUE_TAG, None, [], []), 
                     ((17,20), (17,20), 3, "This", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["DEM_PRO"], SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, [], []), 
                     ]
    expected_mentions_data = [((1,1), (1,1), 1, "I", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], SINGULAR_NUMBER_TAG, UNKNOWN_VALUE_TAG, None, [], []), 
                              ((10,12), (10,12), 2, "you", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], SINGULAR_NUMBER_TAG, UNKNOWN_VALUE_TAG, None, [], []), 
                              #((17,20), (17,20), 3, "This", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["DEM_PRO"], SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, [], []), 
                             ]
    mentions_to_add_data[(10,12)] = {"ident": "10,12", "extent": (10,12), "head_extent": (10,12), "raw_text": "you", 
                                     "gram_type": EXPANDED_PRONOUN_GRAM_TYPE_TAG, "gram_subtype": EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"],
                                     "number": SINGULAR_NUMBER_TAG, "gender": UNKNOWN_VALUE_TAG, 
                                     "referential_probability": None, "wn_synonyms": [], 
                                     "wn_hypernyms": []}
    mentions_to_remove_extents.append((17,20))
    
    quotations_data = [((17,28), "This is good", (1,1), (3,5), (10,12), ((17,20,),)),
                       ]
    expected_quotations_data = [((17,28), "This is good", (1,1), (3,5), (10,12), tuple()),
                                ]
    #quotations_to_add_data[] = {}
    
    sentence_data.append(named_entities_data)
    sentence_data.append(sentence_tokens_data)
    sentence_data.append(mentions_data)
    sentence_data.append(quotations_data)
    
    expected_sentence_data.append(expected_named_entities_data)
    expected_sentence_data.append(expected_sentence_tokens_data)
    expected_sentence_data.append(expected_mentions_data)
    expected_sentence_data.append(expected_quotations_data)
    
    sentences_data.append(sentence_data)
    expected_sentences_data.append(expected_sentence_data)
    
    ## Second sentence
    sentence_data = ["2", (35,68), "You say to him '' That is bad '' .", 
                     "(ROOT (S (NP (PRP You)) (VP (VBP say) (PP (TO to) (NP (PRP him) ('' ''))) (NP (NP (DT That)) (SBAR (S (VP (VBZ is) (ADJP (JJ bad)))))) ('' '')) (. .)))", 
                     "speaker#1", 
                     (((35, 37, "ARG0"), (39, 41, "V"), (46, 48, "ARG2"), (53, 63, "ARG1"), ),
                     )
                    ]
    expected_sentence_data = list(sentence_data)
    
    sentence_tokens_data = [("2-1", (35,37), "You", "you", "PRP"),
                            ("2-2", (39,41), "say", "say", "VBP"),
                            ("2-3", (43,44), "to", "to", "TO"),
                            ("2-4", (46,48), "him", "him", "PRP"),
                            ("2-5", (50,51), "''", "''", "''"),
                            ("2-6", (53,56), "That", "that", "DT"),
                            ("2-7", (58,59), "is", "be", "VBZ"),
                            ("2-8", (61,63), "bad", "bad", "JJ"),
                            ("2-9", (65,66), "''", "''", "''"),
                            ("2-10", (68,68), ".", ".", "."),
                           ]
    expected_sentence_tokens_data = list(sentence_tokens_data)
    
    named_entities_data = [#("35,37", (35,37), "You", "PERS", UNKNOWN_VALUE_TAG, "test"),
                           ("46,48", (46,48), "him", "PERS", UNKNOWN_VALUE_TAG, "test"),
                          ]
    expected_named_entities_data = [("35,37", (35,37), "You", "PERS", UNKNOWN_VALUE_TAG, "test"),
                                    #("46,48", (46,48), "him", "PERS", UNKNOWN_VALUE_TAG, "test"),
                                    ]
    named_entities_to_add_data[(35,37)] = {"ident": "35,37", "extent": (35,37), "raw_text": "You", 
                                           "type": "PERS", "subtype": UNKNOWN_VALUE_TAG, "origin": "test"}
    named_entities_to_remove_extents.append((46,48))
    
    mentions_data = [((35,37), (35,37), 2, "You", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], SINGULAR_NUMBER_TAG, UNKNOWN_VALUE_TAG, None, [], []), 
                     #((46,48), (46,48), 4, "him", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], SINGULAR_NUMBER_TAG, MALE_GENDER_TAG, None, [], []), 
                     #((53,56), (53,56), 5, "That", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["DEM_PRO"], SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, [], []), 
                     ]
    expected_mentions_data = [((35,37), (35,37), 2, "You", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], SINGULAR_NUMBER_TAG, UNKNOWN_VALUE_TAG, None, [], []), 
                              ((46,48), (46,48), 4, "him", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], SINGULAR_NUMBER_TAG, MALE_GENDER_TAG, None, [], []), 
                              ((53,56), (53,56), 5, "That", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["DEM_PRO"], SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, [], []), 
                             ]
    mentions_to_add_data[(46,48)] = {"ident": "46,48", "extent": (46,48), "head_extent": (46,48), 
                                     "raw_text": "him", "gram_type": EXPANDED_PRONOUN_GRAM_TYPE_TAG, 
                                     "gram_subtype": EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], 
                                     "number": SINGULAR_NUMBER_TAG, "gender": MALE_GENDER_TAG, 
                                     "referential_probability": None, "wn_synonyms": [], "wn_hypernyms": []
                                     }
    mentions_to_add_data[(53,56)] = {"ident": "53,56", "extent": (53,56), "head_extent": (53,56), 
                                     "raw_text": "That", "gram_type": EXPANDED_PRONOUN_GRAM_TYPE_TAG, 
                                     "gram_subtype": EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["DEM_PRO"], 
                                     "number": SINGULAR_NUMBER_TAG, "gender": NEUTRAL_GENDER_TAG, 
                                     "referential_probability": None, "wn_synonyms": [], "wn_hypernyms": []
                                     }
    #mentions_to_remove_extents.append()
    
    quotations_data = [#((53,63), "That is bad", (35,37), (39,41), (46,48), tuple()),
                       ]
    expected_quotations_data = [((53,63), "That is bad", (35,37), (39,41), (46,48), ((53,56),)),
                                ]
    quotations_to_add_data[(53,63)] = {"ident": "53,63", "extent": (53,63), "raw_text": "That is bad", 
                                       "speaker_mention_extent": (35,37), 
                                       "speaker_verb_token_extent": (39,41), 
                                       "interlocutor_mention_extent": (46,48)}
    
    sentence_data.append(named_entities_data)
    sentence_data.append(sentence_tokens_data)
    sentence_data.append(mentions_data)
    sentence_data.append(quotations_data)
    
    expected_sentence_data.append(expected_named_entities_data)
    expected_sentence_data.append(expected_sentence_tokens_data)
    expected_sentence_data.append(expected_mentions_data)
    expected_sentence_data.append(expected_quotations_data)
    
    sentences_data.append(sentence_data)
    expected_sentences_data.append(expected_sentence_data)
    
    
    ## Third sentence
    sentence_data = ["3", (70,102), "He says to me '' This is bad '' .", 
                     "(ROOT (S (NP (PRP He)) (VP (VBZ says) (PP (TO to) (NP (NP (PRP me)) ('' '') (SBAR (S (NP (DT This)) (VP (VBZ is) (ADJP (JJ bad) ('' '')))))))) (. .)))", 
                     "speaker#1", 
                     (((70, 71, "ARG0"), (73, 76, "V"), (87, 97, "ARG1"), ), #((70, 71, "ARG0"), (73, 76, "V"), (81, 82, "ARG2"), (87, 97, "ARG1"), ),
                     )
                    ]
    expected_sentence_data = list(sentence_data)
    
    sentence_tokens_data = [("3-1", (70,71), "He", "he", "PRP"),
                            ("3-2", (73,76), "says", "say", "VBZ"),
                            ("3-3", (78,79), "to", "to", "TO"),
                            ("3-4", (81,82), "me", "me", "PRP"),
                            ("3-5", (84,85), "''", "''", "''"),
                            ("3-6", (87,90), "This", "this", "DT"),
                            ("3-7", (92,93), "is", "be", "VBZ"),
                            ("3-8", (95,97), "bad", "bad", "JJ"),
                            ("3-9", (99,100), "''", "''", "''"),
                            ("3-10", (102,102), ".", ".", "."),
                           ]
    expected_sentence_tokens_data = list(sentence_tokens_data)
    
    named_entities_data = [("70,71", (70,71), "He", "PERS", UNKNOWN_VALUE_TAG, "test"),
                           #("81,82", (81,82), "me", "PERS", UNKNOWN_VALUE_TAG, "test"),
                          ]
    expected_named_entities_data = [("70,71", (70,71), "He", "PERS", UNKNOWN_VALUE_TAG, "test"),
                                    ("81,82", (81,82), "me", "PERS", UNKNOWN_VALUE_TAG, "test"),
                                    ]
    named_entities_to_add_data[(81,82)] = {"ident": "81,82", "extent": (81,82), "raw_text": "me", 
                                           "type": "PERS", "subtype": UNKNOWN_VALUE_TAG, "origin": "test"}
    #named_entities_to_remove_extents.append()
    
    mentions_data = [((70,71), (70,71), 4, "He", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], SINGULAR_NUMBER_TAG, MALE_GENDER_TAG, None, [], []), 
                     ((81,82), (81,82), 1, "me", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], SINGULAR_NUMBER_TAG, UNKNOWN_VALUE_TAG, None, [], []), 
                     #((87,90), (87,90), 3, "This", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["DEM_PRO"], SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, [], []), 
                     ]
    expected_mentions_data = [((70,71), (70,71), 4, "He", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], SINGULAR_NUMBER_TAG, MALE_GENDER_TAG, None, [], []), 
                              #((81,82), (81,82), 1, "me", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"], SINGULAR_NUMBER_TAG, UNKNOWN_VALUE_TAG, None, [], []), 
                              ((87,90), (87,90), 3, "This", EXPANDED_PRONOUN_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["DEM_PRO"], SINGULAR_NUMBER_TAG, NEUTRAL_GENDER_TAG, None, [], []), 
                             ]
    mentions_to_add_data[(87,90)] = {"ident": "87,90", "extent": (87,90), "head_extent": (87,90), 
                                     "raw_text": "This", "gram_type": EXPANDED_PRONOUN_GRAM_TYPE_TAG, 
                                     "gram_subtype": EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["DEM_PRO"], 
                                     "number": SINGULAR_NUMBER_TAG, "gender": NEUTRAL_GENDER_TAG, 
                                     "referential_probability": None, "wn_synonyms": [], "wn_hypernyms": []
                                     }
    mentions_to_remove_extents.append((81,82))
    
    quotations_data = [((87,97), "This is bad", (70,71), (73,76), (81,82), tuple()),
                       ]
    expected_quotations_data = [((87,97), "This is bad", (70,71), (73,76), (81,82), ((87,90),)),
                                ]
    #quotations_to_add_data[] = {}
    
    sentence_data.append(named_entities_data)
    sentence_data.append(sentence_tokens_data)
    sentence_data.append(mentions_data)
    sentence_data.append(quotations_data)
    
    expected_sentence_data.append(expected_named_entities_data)
    expected_sentence_data.append(expected_sentence_tokens_data)
    expected_sentence_data.append(expected_mentions_data)
    expected_sentence_data.append(expected_quotations_data)
    
    sentences_data.append(sentence_data)
    expected_sentences_data.append(expected_sentence_data)
    
    modify_named_entities_data = (named_entities_to_add_data, named_entities_to_remove_extents)
    modify_mentions_data = (mentions_to_add_data, mentions_to_remove_extents)
    
    return raw_text, sentences_data, expected_sentences_data, modify_named_entities_data, modify_mentions_data, quotations_to_add_data

def get_test_data(synchronize=False, find_and_synchronize_heads=False):
    # Get document data definition
    (raw_text, sentences_data, expected_sentences_data, modify_named_entities_data, \
     modify_mentions_data, quotations_to_add_data) = create_base_test_data()
    
    # Build the document
    test_document = Document("test_document", raw_text, info={"document_type": "test"})
    _assemble_document_data(test_document, sentences_data, synchronize=synchronize, 
                            find_and_synchronize_heads=find_and_synchronize_heads)
    
    expected_document = Document("expected_document", raw_text, info={"document_type": "test"})
    _assemble_document_data(expected_document, expected_sentences_data, synchronize=synchronize, 
                            find_and_synchronize_heads=find_and_synchronize_heads)
    
    return test_document, expected_document, modify_named_entities_data, modify_mentions_data, quotations_to_add_data

def get_test___init__data():
    test_document, _, _, _, _ = get_test_data() #expected_document, modify_named_entities_data, modify_mentions_data, quotations_to_add_data
    expected_result = DocumentBuffer(test_document)
    return test_document, expected_result
    
def get_test__add_markables_test_data():
    data = []
    test_document, _, (named_entities_data, _), _, _ = get_test_data() #expected_document, (_, named_entities_to_remove_extents), (_, mentions_to_remove_extents)
    
    # Named entities    
    document_buffer = DocumentBuffer(test_document)
    
    expected_result = DocumentBuffer(test_document)
    for extent, named_entity_data in named_entities_data.items():
        expected_result._extent2named_entity_data_add[extent] = named_entity_data
    data.append((named_entities_data, NamedEntity, document_buffer, expected_result))
    
    # Mentions
    test_document, _, _, (mentions_data,_), _ = get_test_data()
    document_buffer = DocumentBuffer(test_document)
    expected_result = DocumentBuffer(test_document)
    for extent, mention_data in mentions_data.items():
        expected_result._extent2mention_data_add[extent] = mention_data
    data.append((mentions_data, Mention, document_buffer, expected_result))
    
    # Quotations
    test_document, _, _, _, quotations_data = get_test_data()
    document_buffer = DocumentBuffer(test_document)
    expected_result = DocumentBuffer(test_document)
    for extent, quotation_data in quotations_data.items():
        expected_result._extent2quotation_data_add[extent] = quotation_data
    data.append((quotations_data, Quotation, document_buffer, expected_result))
    
    return data

def get_test_add_named_entities_test_data():
    data = get_test__add_markables_test_data()
    named_entities_data, _, document_buffer, expected_result = data[0]
    return named_entities_data, document_buffer, expected_result

def get_test_add_mentions_test_data():
    data = get_test__add_markables_test_data()
    mentions_data, _, document_buffer, expected_result = data[1]
    return mentions_data, document_buffer, expected_result

def get_test_add_quotations_test_data():
    data = get_test__add_markables_test_data()
    quotations_data, _, document_buffer, expected_result = data[2]
    return quotations_data, document_buffer, expected_result

def get_test__remove_markables_test_data():
    data = []
    test_document, _, (_, named_entities_extents), (_, mentions_extents), _ = get_test_data() #expected_document, (named_entities_to_add_data, _), (mentions_to_add_data, _), quotations_to_add_data
    
    # Named entities
    document_buffer = DocumentBuffer(test_document)
    expected_result = DocumentBuffer(test_document)
    for extent in named_entities_extents:
        expected_result._to_remove_named_entity_extents.add(extent)
    data.append((named_entities_extents, NamedEntity, document_buffer, expected_result))
    
    # Mentions
    document_buffer = DocumentBuffer(test_document)
    expected_result = DocumentBuffer(test_document)
    for extent in mentions_extents:
        expected_result._to_remove_mention_extents.add(extent)
    data.append((mentions_extents, Mention, document_buffer, expected_result))
    
    return data

def get_test_remove_named_entities_test_data():
    data = get_test__remove_markables_test_data()
    named_entity_extents, _, document_buffer, expected_result = data[0]
    return named_entity_extents, document_buffer, expected_result

def get_test_remove_mentions_test_data():
    data = get_test__remove_markables_test_data()
    mention_extents, _, document_buffer, expected_result = data[1]
    return mention_extents, document_buffer, expected_result

def get_test__flush_modify_markables_test_data():
    test_document, expected_document, _, _, _ = get_test_data(synchronize=False, find_and_synchronize_heads=False) #modify_named_entities_data, modify_mentions_data, quotations_to_add_data
    add_data = get_test__add_markables_test_data()
    remove_data = get_test__remove_markables_test_data()
    document_buffer = DocumentBuffer(test_document)
    
    markable_classes = {}
    # Add: named entities, mentions, quotations
    for markables_data, markable_class, _, _ in add_data:
        document_buffer._add_markables(markables_data, markable_class)
        markable_classes[str(markable_class.__name__)] = markable_class
    # Remove: named entities and mentions
    for markable_extents, markable_class, _, _ in remove_data:
        document_buffer._remove_markables(markable_extents, markable_class)
        markable_classes[str(markable_class.__name__)] = markable_class
    
    markable_classes = tuple(markable_classes.values())
    
    return document_buffer, markable_classes, test_document, expected_document

def get_test_flush_test_data():
    """ Prepare test data for the flush method, different data for different configuration of coreference partition data
    Use 'strict = True' to force the raising of an Exception if unable to synchronize head tokens for a given Mention instance
    """
    # Create the test parameters
    data = []
    
    # Get document data definition
    (raw_text, sentences_data, expected_sentences_data, modify_named_entities_data, \
     modify_mentions_data, quotations_to_add_data) = create_base_test_data()
    
    named_entities_to_add_data, named_entities_to_remove_extents = modify_named_entities_data
    mentions_to_add_data, mentions_to_remove_extents = modify_mentions_data
    
    # Build document data
    ## Document1: test that coreference data is indeed updated after having added and removed mentions
    test_document1 = Document("test_document1", raw_text, info={"document_type": "test"})
    _assemble_document_data(test_document1, sentences_data, synchronize=False, 
                            find_and_synchronize_heads=False, flush_synchronize=True)
    strict = True
    enforce_coreference_partition_consistency = False
    expected_document1 = Document("expected_document1", raw_text, info={"document_type": "test"})
    _assemble_document_data(expected_document1, expected_sentences_data, synchronize=True, 
                            find_and_synchronize_heads=True, flush_synchronize=False)
    
    # (10,12) => 2
    # (46,48) => 4
    # (53,56) => 5
    # (87,90) => 3
    pseudo_entities = (((1,1),),
                       ((10,12), (35,37),),
                       ((46,48), (70,71),),
                       ((53,56),),
                       ((87,90),),
                       )
    new_coreference_partition = CoreferencePartition(entities=pseudo_entities)
    expected_document1.coreference_partition = new_coreference_partition
    
    document_buffer1 = DocumentBuffer(test_document1)
    document_buffer1.add_named_entities(named_entities_to_add_data)
    document_buffer1.remove_named_entities(named_entities_to_remove_extents)
    document_buffer1.add_mentions(mentions_to_add_data)
    document_buffer1.remove_mentions(mentions_to_remove_extents)
    document_buffer1.add_quotations(quotations_to_add_data)
    document_buffer1.set_coreference_partition(new_coreference_partition)
    
    kwargs1 = {"strict": strict, 
               "enforce_coreference_partition_consistency": enforce_coreference_partition_consistency}
    
    data.append((document_buffer1, kwargs1, expected_document1))
    
    ## Document2: another test like the first (this time we manipulate a bit the coreference partition before, to simulate the fact that it can be mutated)
    test_document2 = Document("test_document2", raw_text, info={"document_type": "test"})
    _assemble_document_data(test_document2, sentences_data, synchronize=False, 
                            find_and_synchronize_heads=False, flush_synchronize=True)
    strict = True
    enforce_coreference_partition_consistency = False
    expected_document2 = Document("expected_document2", raw_text, info={"document_type": "test"})
    _assemble_document_data(expected_document2, expected_sentences_data, synchronize=True, 
                            find_and_synchronize_heads=True, flush_synchronize=False)
    
    test_document2.coreference_partition.remove_entity((1,1)),
    # (10,12) => 2
    # (46,48) => 4
    # (53,56) => 5
    # (87,90) => 3
    pseudo_entities = (((10,12), (35,37),),
                       ((46,48), (70,71),),
                       ((53,56),),
                       ((87,90),),
                       )
    new_coreference_partition = CoreferencePartition(entities=pseudo_entities)
    expected_document2.coreference_partition = new_coreference_partition
    
    document_buffer2 = DocumentBuffer(test_document2)
    document_buffer2.add_named_entities(named_entities_to_add_data)
    document_buffer2.remove_named_entities(named_entities_to_remove_extents)
    document_buffer2.add_mentions(mentions_to_add_data)
    document_buffer2.remove_mentions(mentions_to_remove_extents)
    document_buffer2.add_quotations(quotations_to_add_data)
    document_buffer2.set_coreference_partition(new_coreference_partition)
    
    kwargs2 = {"strict": strict, 
               "enforce_coreference_partition_consistency": enforce_coreference_partition_consistency}
    
    data.append((document_buffer2, kwargs2, expected_document2))
    
    ## Document3 & 4
    ### Document3: test that the coreference partition is indeed silently modified so as not to contain extent that do not refer to Mention in the modified Document
    test_document3 = Document("test_document3", raw_text, info={"document_type": "test"})
    _assemble_document_data(test_document3, sentences_data, synchronize=False, 
                            find_and_synchronize_heads=False, flush_synchronize=True)
    strict = True
    enforce_coreference_partition_consistency = True
    expected_document3 = Document("expected_document3", raw_text, info={"document_type": "test"})
    _assemble_document_data(expected_document3, expected_sentences_data, synchronize=True, 
                            find_and_synchronize_heads=True, flush_synchronize=False)
    
    # (10,12) => 2
    # (46,48) => 4
    # (53,56) => 5
    # (87,90) => 3
    pseudo_entities = (((1,1),),
                       ((35,37),),
                       ((70,71),),
                       )
    new_coreference_partition = CoreferencePartition(entities=pseudo_entities)
    expected_document3.coreference_partition = new_coreference_partition
    
    document_buffer3 = DocumentBuffer(test_document3)
    document_buffer3.add_named_entities(named_entities_to_add_data)
    document_buffer3.remove_named_entities(named_entities_to_remove_extents)
    document_buffer3.add_mentions(mentions_to_add_data)
    document_buffer3.remove_mentions(mentions_to_remove_extents)
    document_buffer3.add_quotations(quotations_to_add_data)
    #document_buffer3.set_coreference_partition(new_coreference_partition)
    
    kwargs3 = {"strict": strict, 
               "enforce_coreference_partition_consistency": enforce_coreference_partition_consistency}
    
    data.append((document_buffer3, kwargs3, expected_document3))
    
    ### Document4: test that an Exception is indeed raised because the coreference partition contains extent that do not refer to Mention in the modified Document
    test_document4 = Document("test_document4", raw_text, info={"document_type": "test"})
    _assemble_document_data(test_document4, sentences_data, synchronize=False, 
                            find_and_synchronize_heads=False, flush_synchronize=True)
    strict = True
    enforce_coreference_partition_consistency = False
    expected_document4 = Document("expected_document4", raw_text, info={"document_type": "test"})
    _assemble_document_data(expected_document4, expected_sentences_data, synchronize=True, 
                            find_and_synchronize_heads=True, flush_synchronize=False)
    
    # (10,12) => 2
    # (46,48) => 4
    # (53,56) => 5
    # (87,90) => 3
    pseudo_entities = (((1,1),),
                       ((35,37),),
                       ((70,71),),
                       )
    new_coreference_partition = CoreferencePartition(entities=pseudo_entities)
    expected_document4.coreference_partition = new_coreference_partition
    
    document_buffer4 = DocumentBuffer(test_document4)
    document_buffer4.add_named_entities(named_entities_to_add_data)
    document_buffer4.remove_named_entities(named_entities_to_remove_extents)
    document_buffer4.add_mentions(mentions_to_add_data)
    document_buffer4.remove_mentions(mentions_to_remove_extents)
    document_buffer4.add_quotations(quotations_to_add_data)
    #document_buffer4.set_coreference_partition(new_coreference_partition)
    
    kwargs4 = {"strict": strict, 
               "enforce_coreference_partition_consistency": enforce_coreference_partition_consistency}
    
    data.append((document_buffer4, kwargs4, expected_document4))
    
    return data


"""
l1 = expected_second_document._mentions
        l2 = actual_second_document._mentions
        i = 0
        m1 = l1[i]
        m2 = l2[i]
        
        attribute_name = "head_extent"
        attr1 = getattr(m1, attribute_name)
        attr2 = getattr(m2, attribute_name)
        print(attr1)
        print(attr2)
        #result3, message3 = attr1._inner_eq(attr2)
        #self.assertTrue(result3, message3)
        result2, message2 = m1._inner_eq(m2)
        self.assertTrue(result2, message2)
        """
"""
l1 = expected_document.sentences
        l2 = actual_document.sentences
        i = 1
        m1 = l1[i]
        m2 = l2[i]
        attr_names = ["ident", "_tokens", "_mentions", "constituency_tree", "predicate_arguments", "speaker"]
        for name in attr_names:
            print(getattr(m1, name))
            print(getattr(m2, name))
        self.assertEqual(m1, m2)
        self.assertEqual(l1, l2)
"""

"""
c1 = expected_document.coreference_partition
        c2 = actual_document.coreference_partition
        self.assertEqual(c1._mention_to_entity.keys(), c2._mention_to_entity.keys())
        #print(list(c1))
        #print(list(c2))
        for m in sorted(c1._mention_to_entity.keys()):
            e1 = c1._mention_to_entity[m]
            e2 = c2._mention_to_entity[m]
            print("'{}'  =>  '{}'".format(m, e1))
            print("'{}'  =>  '{}'".format(m, e2))
            self.assertEqual(e1, e2)
            print()
        
        self.assertEqual(c1, c2)
        """



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()