# -*- coding: utf-8 -*-

import unittest
from collections import OrderedDict


from cortex.api.document import Document
from cortex.api.markable import Token, NamedEntity, Mention, Sentence, Quotation
from cortex.api.coreference_partition import CoreferencePartition


class TestDocument(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        (args, kwargs), attribute_name_expected_result_pairs = get_test___init___data()
        
        # Test
        document = Document(*args, **kwargs)
        for attribute_name, expected_result in attribute_name_expected_result_pairs:
            actual_result = getattr(document, attribute_name)
            self.assertEqual(expected_result, actual_result, attribute_name)
        
    #@unittest.skip
    def test__inner_eq(self):
        # Test parameters
        data = get_test__inner_eq_data()
        
        # Tests
        for document1, document2, expected_result in data:
            self.assertEqual(expected_result, document1._inner_eq(document2))


def get_test___init___data():
    attribute_name_expected_result_pairs = []
    
    ident = "test_document"
    attribute_name_expected_result_pairs.append(("ident", ident))
    
    raw_text = "\I said '' This is a test '' ."
    attribute_name_expected_result_pairs.append(("raw_text", raw_text))
    
    info = {"Test_field1": "Test_value1", "Test_field2": 42}
    attribute_name_expected_result_pairs.append(("info", info))
    
    args = (ident, raw_text)
    kwargs = {"info": info}
    
    return (args, kwargs), attribute_name_expected_result_pairs


def get_test__inner_eq_data():
    data = []
    
    (args, kwargs), _ = get_test___init___data() #attribute_name_expected_result_pairs
    
    document1 = Document(*args, **kwargs)
    document2 = Document(*args, **kwargs)
    expected_result = (True, None)
    data.append((document1, document2, expected_result))
    
    document3 = Document(*args, **kwargs)
    quotations = (Quotation("test_quotation", (11,24), document3),)
    document3.quotations = quotations
    
    expected_result = (False, "_quotations")
    data.append((document1, document3, expected_result))
    
    return data


"""
l1 = expected_second_document._mentions
        l2 = actual_second_document._mentions
        i = 0
        m1 = l1[i]
        m2 = l2[i]
        
        attribute_name = "head_extent"
        attr1 = getattr(m1, attribute_name)
        attr2 = getattr(m2, attribute_name)
        print(attr1)
        print(attr2)
        #result3, message3 = attr1._inner_eq(attr2)
        #self.assertTrue(result3, message3)
        result2, message2 = m1._inner_eq(m2)
        self.assertTrue(result2, message2)
        """
"""
l1 = expected_document.sentences
        l2 = actual_document.sentences
        i = 1
        m1 = l1[i]
        m2 = l2[i]
        attr_names = ["ident", "_tokens", "_mentions", "constituency_tree", "predicate_arguments", "speaker"]
        for name in attr_names:
            print(getattr(m1, name))
            print(getattr(m2, name))
        self.assertEqual(m1, m2)
        self.assertEqual(l1, l2)
"""

"""
c1 = expected_document.coreference_partition
        c2 = actual_document.coreference_partition
        self.assertEqual(c1._mention_to_entity.keys(), c2._mention_to_entity.keys())
        #print(list(c1))
        #print(list(c2))
        for m in sorted(c1._mention_to_entity.keys()):
            e1 = c1._mention_to_entity[m]
            e2 = c2._mention_to_entity[m]
            print("'{}'  =>  '{}'".format(m, e1))
            print("'{}'  =>  '{}'".format(m, e2))
            self.assertEqual(e1, e2)
            print()
        
        self.assertEqual(c1, c2)
        """



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()