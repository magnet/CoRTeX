# -*- coding: utf-8 -*-

from cortex.tools.mst import prim_mst, prim_msf, kruskal_mst, kruskal_msf

import unittest

#@unittest.skip
class Test(unittest.TestCase):
    
    #@unittest.skip
    def test_prim_mst(self):
        # Test parameters
        vertices = (1,2,3,4)
        weights = {(1,2): 2.0, 
                   (1,3): 1.0, 
                   (2,3): 2.0, 
                   (3,4): 3.0,
                  }
        
        # Test
        ## Min spanning tree
        spanning_tree_type = "min"
        expected_result = (set((1,2,3,4)), [(1,3), (1,2), (3,4)], 6.0)
        actual_result = prim_mst(vertices, weights, spanning_tree_type=spanning_tree_type)
        self.assertEqual(expected_result, actual_result)
        
        ## Max spanning tree
        spanning_tree_type = "max"
        expected_result = (set((1,2,3,4)), [(1,2), (2,3), (3,4)], 7.0)
        actual_result = prim_mst(vertices, weights, spanning_tree_type=spanning_tree_type)
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_prim_msf(self):
        # Test parameters
        vertices = (1,2,3,4,5,6,7,8)
        weights = {(1,2): 2.0, 
                   (1,3): 1.0, 
                   (2,3): 2.0, 
                   (3,4): 3.0,
                   (5,6): 7.0, 
                   (5,7): 6.0, 
                   (6,7): 7.0, 
                   (7,8): 8.0,
                  }
        
        # Test
        ## Min spanning tree
        spanning_tree_type = "min"
        expected_result = ((set((1,2,3,4)), [(1,3), (1,2), (3,4)], 6.0), 
                           (set((5,6,7,8)), [(5,7), (5,6), (7,8)], 21.0),
                           )
        actual_result = prim_msf(vertices, weights, spanning_tree_type=spanning_tree_type)
        self.assertEqual(expected_result, actual_result)
        
        ## Max spanning tree
        spanning_tree_type = "max"
        expected_result = ((set((1,2,3,4)), [(1,2), (2,3), (3,4)], 7.0), 
                           (set((5,6,7,8)), [(5,6), (6,7), (7,8)], 22.0), 
                           )
        actual_result = prim_msf(vertices, weights, spanning_tree_type=spanning_tree_type)
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_kruskal_mst(self):
        # Test parameters
        vertices = (1,2,3,4)
        weights = {(1,2): 2.0, 
                   (1,3): 1.0, 
                   (2,3): 2.0, 
                   (3,4): 3.0,
                  }
        
        # Test
        ## Min spanning tree
        spanning_tree_type = "min"
        expected_result = (set((1,2,3,4)), [(1,3), (1,2), (3,4)], 6.0)
        actual_result = kruskal_mst(vertices, weights, spanning_tree_type=spanning_tree_type)
        self.assertEqual(expected_result, actual_result)
        
        ## Max spanning tree
        spanning_tree_type = "max"
        expected_result = (set((1,2,3,4)), [(3,4), (2,3), (1,2)], 7.0)
        actual_result = kruskal_mst(vertices, weights, spanning_tree_type=spanning_tree_type)
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_kruskal_msf(self):
        # Test parameters
        vertices = (1,2,3,4,5,6,7,8)
        weights = {(1,2): 2.0, 
                   (1,3): 1.0, 
                   (2,3): 2.0, 
                   (3,4): 3.0,
                   (5,6): 7.0, 
                   (5,7): 6.0, 
                   (6,7): 7.0, 
                   (7,8): 8.0,
                  }
        
        # Test
        ## Min spanning tree
        spanning_tree_type = "min"
        expected_result = ((set((1,2,3,4)), [(1,3), (1,2), (3,4)], 6.0), 
                           (set((5,6,7,8)), [(5,7), (5,6), (7,8)], 21.0),
                           )
        actual_result = kruskal_msf(vertices, weights, spanning_tree_type=spanning_tree_type)
        self.assertEqual(expected_result, actual_result)
        
        ## Max spanning tree
        spanning_tree_type = "max"
        expected_result = ((set((1,2,3,4)), [(3,4), (2,3), (1,2)], 7.0), 
                           (set((5,6,7,8)), [(7,8), (6,7), (5,6)], 22.0), 
                           )
        actual_result = kruskal_msf(vertices, weights, spanning_tree_type=spanning_tree_type)
        self.assertEqual(expected_result, actual_result)



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()