# Copyright 2004-2010 by Vinay Sajip. All Rights Reserved.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any purpose and without fee is hereby granted,
# provided that the above copyright notice appear in all copies and that
# both that copyright notice and this permission notice appear in
# supporting documentation, and that the name of Vinay Sajip
# not be used in advertising or publicity pertaining to distribution
# of the software without specific, written prior permission.
# VINAY SAJIP DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
# ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL
# VINAY SAJIP BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
# ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
# IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
# OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

"""
Test harness for the configuration module "config" for Python.
"""

import unittest

import os
from io import StringIO
import tempfile
import shutil

from cortex.tools.configuration import (ConfigurationReader, Configuration, Mapping, 
                                        MappingMerger, MappingList, 
                                        MappingError, MappingFormatError, 
                                        ConfigurationResolutionError, Sequence, Mapping, 
                                        carry_out_overwrite_merge_resolution, _is_word, get_path, #make_path, 
                                        NEWLINE, MappingFormatError, 
                                        PossiblePythonMagicAttributeNameError, 
                                        deepcopy_mapping,
                                        convert_configuration_to_mapping)

STREAMS = {
    "simple_1" :
"""
message: 'Hello, world!'
""",
    "malformed_1" :
"""
123
""",
    "malformed_2" :
"""
[ 123, 'abc' ]
""",
    "malformed_3" :
"""
{ a : 7, b : 1.3, c : 'test' }
""",
    "malformed_4" :
"""
test: $a [7] # note space before bracket
""",
    "malformed_5" :
"""
test: 'abc'
test: 'def'
""",
    "wellformed_1" :
"""
test: $a[7] # note no space before bracket
""",
    "boolean_1":
"""
test : False
another_test: True
""",
    "boolean_2":
"""
test : false
another_test: true
""",
    "none_1":
"""
test : None
""",
    "none_2":
"""
test : none
""",
    "number_1":
"""
root: 1
stream: 1.7
neg: -1
negfloat: -2.0
posexponent: 2.0999999e-08
negexponent: -2.0999999e-08
exponent: 2.0999999e08
""",
    "sequence_1":
"""
mixed: [ "VALIGN", [ 0, 0 ], [ -1, -1 ], "TOP" ]
simple: [1, 2]
nested: [1, [2, 3], [4, [5, 6]]]
""",
    "sequence_2":
"""
mixed: [ "VALIGN", [ 0, 0 ], [ -1, -1 ], "TOP" ]
simple: [
            1
            {
                test1: glip
                test2: {
                            hehe: haha
                        }
            }
        ]
""",
    "include_1":
"""
included: @'include_2'
""",
    "include_2":
"""
test: 123
another_test: 'abc'
""",
    "expr_1":
"""
value1 : 10
value2 : 5
value3 : 'abc'
value4 : 'ghi'
value5 : 0
value6 : { 'a' : $value1, 'b': $value2 }
derived1 : $value1 + $value2
derived2 : $value1 - $value2
derived3 : $value1 * $value2
derived4 : $value1 / $value2
derived5 : $value1 % $value2
derived6 : $value3 + $value4
derived7 : $value3 + 'def' + $value4
derived8 : $value3 - $value4 # meaningless
derived9 : $value1 / $value5    # div by zero
derived10 : $value1 % $value5   # div by zero
derived11 : $value17    # doesn't exist
derived12 : $value6.a + $value6.b
""",
    "eval_1":
"""
stderr : `sys.stderr`
stdout : `sys.stdout`
stdin : `sys.stdin`
debug : `debug`
DEBUG : `DEBUG`
derived: $DEBUG * 10
""",
    "merge_1":
"""
value1: True
value3: [1, 2, 3]
value5: [ 7 ]
value6: { 'a' : 1, 'c' : 3 }
""",
    "merge_2":
"""
value2: False
value4: [4, 5, 6]
value5: ['abc']
value6: { 'b' : 2, 'd' : 4 }
""",
    "merge_3":
"""
value1: True
value2: 3
value3: [1, 3, 5]
value4: [1, 3, 5]
""",
    "merge_4":
"""
value1: False
value2: 4
value3: [2, 4, 6]
value4: [2, 4, 6]
""",
    "list_1":
"""
verbosity : 1
""",
    "list_2":
"""
verbosity : 2
program_value: 4
""",
    "list_3":
"""
verbosity : 3
suite_value: 5
""",
    "get_1":
"""
value1 : 123
value2 : 'abcd'
value3 : True
value4 : None
value5:
{
    value1 : 123
    value2 : 'abcd'
    value3 : True
    value4 : None
}
""",
    "multiline_1":
"""
value1: '''Value One
Value Two
'''
value2: \"\"\"Value Three
Value Four\"\"\"
""",
    "tuple":
"""
value1: [1, 2, 3, 4]
value2: 42
""",
    "dict":
"""
"test_field1": 42
"test_field2": {"incepttest_field": "incepttest_value", "deeper": {"glap": "glop"}}
""",
    "magic_attribute_name2":
"""
"field_name1": False
"field_name2": True
"__field_name3__": -42.657
"final_field_name": 67
""",
    "test_convert_to_mapping":
"""
# GLOBAL:
corpora_folder_path: "corpora_files"
document_version: "3_no_verb_characterized_mentions"
documents_root_folder_path: $document_version + "/"
# TRAIN
train:
{
    corpus_file_path: $corpora_folder_path + "/" + "small_test_corpus.txt"
    documents_root_folder_path: $documents_root_folder_path
}
# TEST
test:
{
    corpus_file_path: $corpora_folder_path + "/" + "small_test_corpus.txt"
    documents_root_folder_path: $documents_root_folder_path
}
""",
    "test_convert_to_mapping2":
"""
# GLOBAL:
corpora_folder_path: "corpora_files"
document_version: "3_no_verb_characterized_mentions"
documents_root_folder_path: $document_version + "/"
# TRAIN
train:
{
    corpus_file_path: $corpora_folder_path + "/" + "small_test_corpus.txt"
    documents_root_folder_path: $documents_root_folder_path
}
# TEST
test:
{
    corpus_file_path: $corpora_folder_path + "/" + "small_test_corpus.txt"
    documents_root_folder_path: $train.documents_root_folder_path
}
"""
}

CONFIG_READER_EMBEDDED = ConfigurationReader(lambda s: StringIO(STREAMS[s]))

def create_file_stream_from_config_name(name):
    return StringIO(STREAMS[name])

def create_config_from_config_name(name, **kwargs):
    with StringIO(STREAMS[name]) as s:
        s.name = name
        configuration = Configuration(stream_or_file_path=s, **kwargs)
    return configuration

class OutStream(StringIO):
    def close(self):
        self.value = self.getvalue()
        StringIO.close(self)


#@unittest.skip
class TestConfiguration(unittest.TestCase):
    
    #@unittest.skip
    def test_creation(self):
        cfg = Configuration()
        self.assertEqual(0, len(cfg))  # should be empty
        
        d = {"field_name1": "field_value1", "field_name2": 42}
        cfg2 = Configuration(d.items())
        self.assertEqual(2, len(cfg2))
        self.assertEqual("field_value1", cfg2["field_name1"])
        self.assertEqual(42, cfg2["field_name2"])
        
        s = (("field_name1", "field_value1"), ("field_name2", 42))
        cfg3 = Configuration(s)
        self.assertEqual(2, len(cfg3))
        self.assertEqual("field_value1", cfg3["field_name1"])
        self.assertEqual(42, cfg3["field_name2"])

    #@unittest.skip
    def test_change(self):
        cfg = CONFIG_READER_EMBEDDED.load("simple_1")
        cfg.message = "Goodbye, cruel world!"
        self.assertEqual("Goodbye, cruel world!", cfg.message)

    #@unittest.skip
    def test_save(self):
        # Test1
        cfg = CONFIG_READER_EMBEDDED.load("simple_1")
        cfg.message = "Goodbye, cruel world!"
        expected_result = "message : 'Goodbye, cruel world!'" + NEWLINE
        with OutStream() as out:
            type(cfg).save(cfg, out)
        actual_result = out.value
        self.assertEqual(expected_result, actual_result)
        
        # Test2
        temp_file_path = tempfile.mktemp()
        try:
            type(cfg).save(cfg, temp_file_path)
            with open(temp_file_path, "r", encoding="utf-8") as f:
                actual_result = f.read()
        finally:
            if os.path.isfile(temp_file_path):
                os.remove(temp_file_path)
        self.assertEqual(expected_result, actual_result)

    #@unittest.skip
    def test_eval(self):
        import sys, logging
        cfg = CONFIG_READER_EMBEDDED.load("eval_1")
        self.assertEqual(sys.stderr, cfg.stderr)
        self.assertEqual(sys.stdout, cfg.stdout)
        self.assertEqual(sys.stdin, cfg.stdin)
        self.assertRaises(ConfigurationResolutionError, lambda x: x.debug, cfg)
        
        type(cfg).add_namespace(cfg, logging.Logger)
        self.assertEqual(logging.Logger.debug, cfg.debug)
        self.assertRaises(ConfigurationResolutionError, lambda x: x.DEBUG, cfg)
        
        type(cfg).add_namespace(cfg, logging)
        self.assertEqual(logging.DEBUG, cfg.DEBUG)
        
        type(cfg).remove_namespace(cfg, logging.Logger)
        self.assertEqual(logging.debug, cfg.debug)
        self.assertEqual(logging.DEBUG * 10, cfg.derived)

    #@unittest.skip
    def test_get(self):
        cfg = CONFIG_READER_EMBEDDED.load("get_1")
        self.assertEqual(123, Configuration.get(cfg, "value1"))
        self.assertEqual(123, Configuration.get(cfg, "value1", -123))
        self.assertEqual(-123, Configuration.get(cfg, "value11", -123))
        self.assertEqual("abcd", Configuration.get(cfg, "value2"))
        self.assertTrue(Configuration.get(cfg, "value3") is not None)
        self.assertFalse(Configuration.get(cfg, "value4") is not None)
        self.assertEqual(123, Configuration.get(cfg, "value1"))
        self.assertEqual(123, Configuration.get(cfg, "value1", -123))
        self.assertEqual(-123, Configuration.get(cfg, "value11", -123))
        self.assertEqual("abcd", Configuration.get(cfg, "value2"))
        self.assertTrue(Configuration.get(cfg, "value3") is not None)
        self.assertFalse(Configuration.get(cfg, "value4") is not None)
    
    #@unittest.skip
    def test_equal(self):
        # Test parameters
        d_1 = {"field_1": True, "field_2": "value_2", "field_3": Sequence(item_iterable=[5,6,Configuration({"glop": "glap"}.items())])}
        d_2 = {"field_3":Sequence(item_iterable=[5,6,Configuration({"glop": "glap"}.items())]), "field_1": True, "field_2": "value_2"}
        configuration1 = Configuration(d_1.items())
        configuration2 = Configuration(d_2.items())
        
        # Test
        self.assertEqual(configuration1, configuration2)
    
    #@unittest.skip
    def test_save_and_load(self):
        temp_dir_path = tempfile.mkdtemp()
        try:
            temp_file_path = tempfile.mktemp(dir=temp_dir_path)
            # Test parameters
            sequence = (("name", "Glop"), 
                        ("config", Mapping((("field1", "value1"), 
                                           ("field2", Sequence(item_iterable=(Mapping({"subfield11": "subvalue11", "subfield12": "subvalue12"}.items()), 
                                                                              Mapping({"subfield21": "subvalue21", "subfield22": "subvalue22"}.items()),
                                                                              )
                                                               )
                                            )
                                           ),
                                          )
                         ),
                        )
            configuration = Configuration(sequence)
            self.assertIsInstance(configuration.config.field2[0], Mapping)
            expected_configuration = configuration
            with open(temp_file_path, "w", encoding="utf-8") as f:
                Configuration.save(configuration, f)
            #with open(temp_file_path, "r", encoding="utf-8") as f:
            #    actual_configuration = ConfigurationReader().load(f)
            actual_configuration = Configuration.load(temp_file_path)
            self.assertTrue(*Configuration._inner_eq(expected_configuration, actual_configuration))
        finally:
            if os.path.isdir(temp_dir_path):
                shutil.rmtree(temp_dir_path)
    
    
    def test_del(self):
        mapping = Mapping({"field1_name": "field2_value", "field2_name": 23, "field3_name": False})
        attribute_to_remove_name = "field2_name"
        expected_value = Mapping({"field1_name": "field2_value", "field3_name": False})
        
        self.assertNotEqual(expected_value, mapping)
        del mapping[attribute_to_remove_name]
        self.assertEqual(expected_value, mapping)
        
        mapping2 = Mapping({"field1_name": "field2_value", "field2_name": 23, "field3_name": False})
        expected_value2 = Mapping({"field2_name": 23, "field3_name": False})
        
        self.assertNotEqual(expected_value2, mapping2)
        del mapping2.field1_name
        self.assertEqual(expected_value2, mapping2)
    
    #@unittest.skip
    def test_raise_exception(self):
        d = {"field_name1": 42, "__magic_python__": True}
        with self.assertRaises(PossiblePythonMagicAttributeNameError):
            _ = Mapping(d)
    
    def test_deepcopy_mapping(self):
        # Test parameters
        a = Sequence([8,9])
        bn = Mapping({"nnnest1": "val_nnest1"})
        b = Mapping({"nest1": "val_nest1", "nest2": bn})
        mapping = Mapping({"1": a, "2": b})
        
        deepcopied_mapping = deepcopy_mapping(mapping)
        
        # Test
        self.assertTrue(Mapping._inner_eq(mapping, deepcopied_mapping))
        self.assertTrue(deepcopied_mapping["2"].nest2 is not mapping["2"].nest2)
        self.assertTrue(deepcopied_mapping["2"] is not mapping["2"])
        self.assertTrue(deepcopied_mapping["1"] is not mapping["1"])
    
    def test_update(self):
        # Test parameters
        mapping1 = Mapping({"a": 1, "b": Mapping({"test": "value"})})
        mapping2 = Mapping({"b": Mapping({"test2": "value2"}), "c": None})
        expected_result = Mapping({"a": 1, "b": Mapping({"test2": "value2"}), "c": None})
        
        # Test
        self.assertFalse(*Mapping._inner_eq(expected_result, mapping1))
        Mapping.update(mapping1, mapping2)
        self.assertTrue(*Mapping._inner_eq(expected_result, mapping1))
    
    def test_convert_configuration_to_mapping(self):
        # Test parameters
        expected_result = Mapping({"corpora_folder_path": "corpora_files",
                                   "document_version": "3_no_verb_characterized_mentions",
                                   "documents_root_folder_path": "3_no_verb_characterized_mentions/",
                                   "train": {"corpus_file_path": "corpora_files/small_test_corpus.txt",
                                             "documents_root_folder_path": "3_no_verb_characterized_mentions/",
                                             },
                                   "test": {"corpus_file_path": "corpora_files/small_test_corpus.txt",
                                            "documents_root_folder_path": "3_no_verb_characterized_mentions/",
                                            }
                                   })
        
        
        # Test
        ## Test 1
        configuration = CONFIG_READER_EMBEDDED.load("test_convert_to_mapping")
        actual_result = convert_configuration_to_mapping(configuration)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))
        
        ## Test 2
        configuration = CONFIG_READER_EMBEDDED.load("test_convert_to_mapping2")
        actual_result = convert_configuration_to_mapping(configuration)
        self.assertTrue(*Mapping._inner_eq(expected_result, actual_result))



#@unittest.skip
class TestFunctions(unittest.TestCase):
    
    #@unittest.skip
    def test_functions(self):
        #self.assertEqual("suffix", make_path("", "suffix"))
        #self.assertEqual("suffix", make_path(None, "suffix"))
        #self.assertEqual("prefix.suffix", make_path("prefix", "suffix"))
        #self.assertEqual("prefix[1]", make_path("prefix", "[1]"))
        self.assertTrue(_is_word("a9"))
        self.assertTrue(_is_word("9a"))    #perverse, but there you go #oh yeah
        self.assertFalse(_is_word(9))
        self.assertFalse(_is_word(None))
        self.assertFalse(_is_word(self))
        self.assertFalse(_is_word(""))


#@unittest.skip
class TestExpression(unittest.TestCase):
    
    #@unittest.skip
    def test_expression(self):
        cfg = CONFIG_READER_EMBEDDED.load("expr_1")
        self.assertEqual(15, cfg.derived1)
        self.assertEqual(5, cfg.derived2)
        self.assertEqual(50, cfg.derived3)
        self.assertEqual(2, cfg.derived4)
        self.assertEqual(0, cfg.derived5)
        self.assertEqual("abcghi", cfg.derived6)
        self.assertEqual("abcdefghi", cfg.derived7)
        self.assertRaises(TypeError, lambda x: x.derived8, cfg)
        self.assertRaises(ZeroDivisionError, lambda x: x.derived9, cfg)
        self.assertRaises(ZeroDivisionError, lambda x: x.derived10, cfg)
        self.assertRaises(ConfigurationResolutionError, lambda x: x.derived11, cfg)
        self.assertEqual(15, cfg.derived12)
    
    #@unittest.skip
    def test_get_string_representation(self):
        data = []
        cfg = CONFIG_READER_EMBEDDED.load("expr_1")
        
        expression, _ = object.__getattribute__(cfg, "data")["derived1"] #comment
        expected_result = "$value1 + $value2"
        data.append((expression, expected_result))
        
        expression, _ = object.__getattribute__(cfg, "data")["derived12"] #comment
        expected_result = "$value6.a + $value6.b"
        data.append((expression, expected_result))
        
        for expression, expected_result in data:
            actual_result = expression.get_string_representation()
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test___repr__(self):
        data = []
        cfg = CONFIG_READER_EMBEDDED.load("expr_1")
        
        expression, _ = object.__getattribute__(cfg, "data")["derived1"] #comment
        expected_result = "Expression(op=\"'+'\", lhs=\"Reference(type=\"'$'\", elements=\"['value1']\", configuration=\"...\")\", rhs=\"Reference(type=\"'$'\", elements=\"['value2']\", configuration=\"...\")\")"
        data.append((expression, expected_result))
        
        expression, _ = object.__getattribute__(cfg, "data")["derived12"] #comment
        expected_result = "Expression(op=\"'+'\", lhs=\"Reference(type=\"'$'\", elements=\"['value6', ('.', 'a')]\", configuration=\"...\")\", rhs=\"Reference(type=\"'$'\", elements=\"['value6', ('.', 'b')]\", configuration=\"...\")\")"
        data.append((expression, expected_result))
        
        for expression, expected_result in data:
            actual_result = expression.__repr__()
            self.assertEqual(expected_result, actual_result)



#@unittest.skip
class TestConfigurationReader(unittest.TestCase):
    
    #@unittest.skip
    def test_simple(self):
        cfg = CONFIG_READER_EMBEDDED.load("simple_1")
        self.assertTrue("message" in cfg)
        self.assertFalse("root" in cfg)
        self.assertFalse("stream" in cfg)
        self.assertFalse("load" in cfg)
        self.assertFalse("save" in cfg)
    
    #@unittest.skip
    def test_value_only(self):
        names = ("malformed_1", "malformed_2", "malformed_3")
        for name in names:
            with StringIO(STREAMS[name]) as s:
                s.name = name
                self.assertRaises(MappingError, CONFIG_READER_EMBEDDED.load, s)

    #@unittest.skip
    def test_bad_bracket(self):
        name = "malformed_4"
        with StringIO(STREAMS[name]) as s:
            s.name = name
            self.assertRaises(MappingError, CONFIG_READER_EMBEDDED.load, s)

    #@unittest.skip
    def test_duplicate(self):
        name = "malformed_5"
        with StringIO(STREAMS[name]) as s:
            s.name = name
            self.assertRaises(MappingError, CONFIG_READER_EMBEDDED.load, s)
    
    #@unittest.skip
    def test_good_bracket(self):
        CONFIG_READER_EMBEDDED.load("wellformed_1")

    #@unittest.skip
    def test_boolean(self):
        cfg = CONFIG_READER_EMBEDDED.load("boolean_1")
        self.assertEqual(True, cfg.another_test)
        self.assertEqual(False, cfg.test)

    #@unittest.skip
    def test_not_boolean(self):
        cfg = CONFIG_READER_EMBEDDED.load("boolean_2")
        self.assertEqual("true", cfg.another_test)
        self.assertEqual("false", cfg.test)

    #@unittest.skip
    def test_none(self):
        cfg = CONFIG_READER_EMBEDDED.load("none_1")
        self.assertEqual(None, cfg.test)

    #@unittest.skip
    def test_not_none(self):
        cfg = CONFIG_READER_EMBEDDED.load("none_2")
        self.assertEqual("none", cfg.test)

    #@unittest.skip
    def test_number(self):
        cfg = CONFIG_READER_EMBEDDED.load("number_1")
        self.assertEqual(1, cfg.root)
        self.assertEqual(1.7, cfg.stream)
        self.assertEqual(-1, cfg.neg)
        self.assertEqual(-2.0, cfg.negfloat)
        self.assertAlmostEqual(-2.0999999e-08, cfg.negexponent)
        self.assertAlmostEqual(2.0999999e-08, cfg.posexponent)
        self.assertAlmostEqual(2.0999999e08, cfg.exponent)
    
    #@unittest.skip
    def test_include(self):
        cfg = CONFIG_READER_EMBEDDED.load("include_1")
        with OutStream() as out:
            type(cfg).save(cfg, out)
        value = out.value
        s = "included :%s{%s    test : 123%s    another_test : 'abc'%s}%s" % (5 * (NEWLINE,))
        self.assertEqual(s, value)
    
    #@unittest.skip
    def test_multiline(self):
        cfg = CONFIG_READER_EMBEDDED.load("multiline_1")
        self.assertEqual("Value One\nValue Two\n", Configuration.get(cfg, "value1"))
        self.assertEqual("Value Three\nValue Four", Configuration.get(cfg, "value2"))

    #@unittest.skip
    def test_JSON(self):
        current_file_folder_path = os.path.dirname(os.path.abspath(__file__))
        test_file_path = os.path.join(current_file_folder_path, "styles.json")
        with open(test_file_path, "r") as f:
            s = f.read()
        with StringIO("dummy: " + s) as data:
            _ = CONFIG_READER_EMBEDDED.load(data)
    
    #@unittest.skip
    def test_tuple(self):
        cfg = CONFIG_READER_EMBEDDED.load("tuple")
        expected_result = Sequence([1, 2, 3, 4])
        actual_result = cfg.value1
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_dict(self):
        cfg = CONFIG_READER_EMBEDDED.load("dict")
        expected_result = Mapping({"incepttest_field": "incepttest_value", "deeper": Mapping({"glap": "glop"}.items())}.items())
        actual_result = cfg.test_field2
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_repr(self):
        cfg = CONFIG_READER_EMBEDDED.load("sequence_2")
        expected_result = "Configuration({'mixed': Sequence(['VALIGN', Sequence([0, 0,]), Sequence([-1, -1,]), 'TOP',]), 'simple': Sequence([1, Mapping({'test1': 'glip', 'test2': Mapping({'hehe': 'haha'})}),])}, Namespaces=[Namespace('os,sys')])"
        actual_result = repr(cfg)
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_str(self):
        # Test1
        cfg = CONFIG_READER_EMBEDDED.load("sequence_2")
        expected_result = \
"""Configuration({
    'mixed': Sequence([
        'VALIGN',
        Sequence([
            0,
            0,
        ]),
        Sequence([
            -1,
            -1,
        ]),
        'TOP',
    ])
    'simple': Sequence([
        1,
        Mapping({
            'test1': 'glip'
            'test2': Mapping({
                'hehe': 'haha'
            })
        }),
    ])
},
Namespaces=[Namespace('os,sys')]
)"""
        actual_result = str(cfg)
        self.assertEqual(expected_result, actual_result)
        
        # Test2
        cfg = CONFIG_READER_EMBEDDED.load("include_1")
        expected_result = \
"""Configuration({
    'included': Configuration({
        'test': 123
        'another_test': 'abc'
    },
    Namespaces=[Namespace('os,sys')]
    )
},
Namespaces=[Namespace('os,sys')]
)"""
        actual_result = str(cfg)
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_raise_exception(self):
        with self.assertRaises(MappingFormatError):
            _ = CONFIG_READER_EMBEDDED.load("magic_attribute_name2")


#@unittest.skip
class TestSequence(unittest.TestCase):
    
    #@unittest.skip
    def test_sequence(self):
        cfg = CONFIG_READER_EMBEDDED.load("sequence_1")
        
        self.assertEqual("Sequence([1, 2,])", repr(cfg.simple))
        self.assertEqual("Sequence([1, Sequence([2, 3,]), Sequence([4, Sequence([5, 6,]),]),])", repr(cfg.nested))
        self.assertEqual("Sequence(['VALIGN', Sequence([0, 0,]), Sequence([-1, -1,]), 'TOP',])", repr(cfg.mixed))
        
        data = []
        expected_result = "Sequence([\n    1,\n    2,\n])"
        actual_result = str(cfg.simple)
        data.append((expected_result, actual_result))
        
        expected_result = "Sequence([\n    1,\n    Sequence([\n        2,\n        3,\n    ]),\n    Sequence([\n        4,\n        Sequence([\n            5,\n            6,\n        ]),\n    ]),\n])"
        actual_result = str(cfg.nested)
        data.append((expected_result, actual_result))
        
        expected_result = "Sequence([\n    'VALIGN',\n    Sequence([\n        0,\n        0,\n    ]),\n    Sequence([\n        -1,\n        -1,\n    ]),\n    'TOP',\n])"
        actual_result = str(cfg.mixed)
        data.append((expected_result, actual_result))
        
        for expected_result, actual_result in data:
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_insert_tuple(self):
        sequence = Sequence([3,"ga"])
        t = (9,'(', [1,])
        d = {"test_field": "test_value", "test_field2": "test_value2"}
        sequence.append(t)
        sequence.append(d)
        
        data = []
        
        expected_result = Sequence((9,'(', [1,]))
        actual_result = sequence[2]
        data.append((expected_result, actual_result))
        
        expected_result = Mapping(d.items())
        actual_result = sequence[3]
        data.append((expected_result, actual_result))
        
        for expected_result, actual_result in data:
            self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_path(self):
        # Test parameters
        data = []
        
        cfg = CONFIG_READER_EMBEDDED.load("sequence_2")
        expected_path = "simple[1].test2"
        actual_path = get_path(cfg.simple[1].test2)#_get_attribute(cfg.simple[1].test2, "path")
        data.append((expected_path, actual_path))
        
        cfg2 = Configuration()
        cfg2.test1 = Mapping()
        cfg2.test1.seq1 = Sequence(item_iterable=(Mapping({"glip": "glop"}.items()),
                                                  "test",
                                                  )
                                   )
        expected_path = "test1.seq1[0]"
        actual_path = get_path(cfg2.test1.seq1[0])#_get_attribute(cfg2.test1.seq1[0], "path")
        #data.append((expected_path, actual_path))
        
        # Test
        for expected_path, actual_path in data:
            self.assertEqual(expected_path, actual_path)
        
    #@unittest.skip
    def test__get_base_hash_string(self):
        self.maxDiff = None
        
        # Test 1
        ## Test parameters
        m = Mapping([("Krunch", 2),
                     ("abalone", -42),
                     ])
        expected_result = """Krunch:
2
abalone:
-42"""
        ## Test
        actual_result = type(m).get_base_hash_string(m)
        self.assertEqual(expected_result, actual_result)
        
        # Test 2
        ## Test parameters
        s = Sequence([-42, "za"])
        expected_result = """-42
'za'"""
        ## Test
        actual_result = type(s).get_base_hash_string(s)
        self.assertEqual(expected_result, actual_result)
        
        # Test 
        ## Test parameters
        m = Mapping([("sequence", Sequence([-42,
                                            Mapping([("Krunch", 2),
                                                     ("abalone", -62),
                                                     ]),
                                            ])),
                     ("name", "glop"),
                     ("config", Mapping([("Krack", 3),
                                         ("Kombat", "mortal"),
                                         ])
                      ),
                     ])
        expected_result = """config:
Kombat:
'mortal'
Krack:
3
name:
'glop'
sequence:
-42
Krunch:
2
abalone:
-62"""
        
        ## Test
        actual_result = type(m).get_base_hash_string(m)
        self.assertEqual(expected_result, actual_result)
        
        # Test 4
        ## Test parameters
        m = CONFIG_READER_EMBEDDED.load("expr_1")
        del m["derived8"]
        del m["derived9"]
        del m["derived10"]
        del m["derived11"]
        expected_result = """derived1:
15
derived12:
15
derived2:
5
derived3:
50
derived4:
2.0
derived5:
0
derived6:
'abcghi'
derived7:
'abcdefghi'
value1:
10
value2:
5
value3:
'abc'
value4:
'ghi'
value5:
0
value6:
a:
10
b:
5"""     
        
        # Test
        actual_result = type(m).get_base_hash_string(m)
        self.assertEqual(expected_result, actual_result)
        



#@unittest.skip
class TestConfigurationMerge(unittest.TestCase):
    
    #@unittest.skip
    def test_merge(self):
        # Test 1
        cfg1 = CONFIG_READER_EMBEDDED.load("merge_1")
        cfg2 = CONFIG_READER_EMBEDDED.load("merge_2")
        MappingMerger().merge(cfg1, cfg2)
        merged = cfg1
        cfg1 = CONFIG_READER_EMBEDDED.load("merge_1")
        for i in range(0, 5):
            key = "value%d" % (i + 1,)
            self.assertTrue(key in merged)
        self.assertEqual(len(cfg1.value5) + len(cfg2.value5), len(merged.value5))
    
        # Test 2
        cfg3 = CONFIG_READER_EMBEDDED.load("merge_3")
        cfg4 = CONFIG_READER_EMBEDDED.load("merge_4")
        merger = MappingMerger()
        self.assertRaises(MappingError, merger.merge, cfg3, cfg4)

        # Test 3
        cfg3 = CONFIG_READER_EMBEDDED.load("merge_3")
        cfg4 = CONFIG_READER_EMBEDDED.load("merge_4")
        merger = MappingMerger(carry_out_overwrite_merge_resolution)
        merger.merge(cfg3, cfg4)
        self.assertEqual(False, cfg3["value1"])
        self.assertEqual(4, cfg3["value2"])
        
        # Test 4
        def custom_merge_resolve(map1, map2, key):
            if key == "value3":
                rv = "overwrite"
            else:
                rv = carry_out_overwrite_merge_resolution(map1, map2, key)
            return rv

        cfg3 = CONFIG_READER_EMBEDDED.load("merge_3")
        cfg4 = CONFIG_READER_EMBEDDED.load("merge_4")
        merger = MappingMerger(custom_merge_resolve)
        merger.merge(cfg3, cfg4)
        self.assertEqual("Sequence([2, 4, 6,])", repr(cfg3.value3))
        self.assertEqual("Sequence([1, 3, 5, 2, 4, 6,])", repr(cfg3.value4))



#@unittest.skip
class TestMappingList(unittest.TestCase):
    
    #@unittest.skip
    def test_list(self):
        config_list = MappingList()
        config_list.append(CONFIG_READER_EMBEDDED.load("list_1"))
        config_list.append(CONFIG_READER_EMBEDDED.load("list_2"))
        config_list.append(CONFIG_READER_EMBEDDED.load("list_3"))
        self.assertEqual(1, config_list.get_by_path("verbosity"))
        self.assertEqual(4, config_list.get_by_path("program_value"))
        self.assertEqual(5, config_list.get_by_path("suite_value"))
        self.assertRaises(MappingError, config_list.get_by_path, "nonexistent_value")



if __name__ == "__main__":
    #import sys;sys.argv = ["", "Test.testName']
    unittest.main()
