# -*- coding: utf-8 -*-

import unittest
import os

from cortex.tools.wrapper.conll2012_scorer.wrapper import CONLL2012ScorerWrapper

CURRENT_FOLDER_PATH = os.path.dirname(__file__)
DATA_FOLDER_PATH = os.path.join(CURRENT_FOLDER_PATH, "data")


class TestCONLL2012ScorerWrapper(unittest.TestCase):
    
    #@unittest.skip
    def test_evaluate(self):
        # Test parameters
        data = get_test_evaluate_data()
        conll2012_scorer_wrapper = CONLL2012ScorerWrapper()
        
        # Test
        for (args, kwargs), expected_results in data:
            actual_results = conll2012_scorer_wrapper.evaluate(*args, **kwargs)
            self.assertEqual(expected_results, actual_results)


def get_test_evaluate_data():
    data = []
    
    # No document
    ## ceafe
    metric = "ceafe"
    key_multi_documents_conll2012_file_path = os.path.join(DATA_FOLDER_PATH, "reference_documents_skip_singleton=True.conll")
    response_multi_documents_conll2012_file_path = os.path.join(DATA_FOLDER_PATH, "system_documents_skip_singleton=True.conll")
    name = "none"
    args = (metric, key_multi_documents_conll2012_file_path, response_multi_documents_conll2012_file_path)
    kwargs = {"name": name}
    expected_ceafe_overall_results = {"overall_mention_identification_recall_num": 6.,
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,
                                      "overall_mention_identification_precision_num": 6.,
                                      "overall_mention_identification_precision_denom": 7.,
                                      "overall_mention_identification_precision": 85.71,
                                      "overall_mention_identification_f1": 63.15,
                                      "recall_num": 1.52380952380952,
                                      "recall_denom": 4.,
                                      "recall": 38.09,
                                      "precision_num": 1.52380952380952,
                                      "precision_denom": 3.,
                                      "precision": 50.79,
                                      "f1": 43.53,
                                      }
    expected_ceafe_results_per_document = []
    expected_metrics_results_collection = [("ceafe", (expected_ceafe_overall_results, expected_ceafe_results_per_document)),
                                           ]
    expected_metrics_results_collection = tuple(expected_metrics_results_collection)
    data.append(((args, kwargs), expected_metrics_results_collection))
    
    ## blanc
    metric = "blanc"
    key_multi_documents_conll2012_file_path = os.path.join(DATA_FOLDER_PATH, "reference_documents_skip_singleton=True.conll")
    response_multi_documents_conll2012_file_path = os.path.join(DATA_FOLDER_PATH, "system_documents_skip_singleton=True.conll")
    name = "none"
    args = (metric, key_multi_documents_conll2012_file_path, response_multi_documents_conll2012_file_path)
    kwargs = {"name": name}
    expected_blanc_overall_results = {"overall_mention_identification_recall_num": 6., 
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,  
                                      "overall_mention_identification_precision_num": 6., 
                                      "overall_mention_identification_precision_denom": 7., 
                                      "overall_mention_identification_precision": 85.71, 
                                      "overall_mention_identification_f1": 63.15,
                                      
                                      "coreference_recall_num": 4., 
                                      "coreference_recall_denom": 14.,
                                      "coreference_recall": 28.57,  
                                      "coreference_precision_num": 4., 
                                      "coreference_precision_denom": 5., 
                                      "coreference_precision": 80., 
                                      "coreference_f1": 42.1,
                                      
                                      "non_coreference_recall_num": 0., 
                                      "non_coreference_recall_denom": 16.,
                                      "non_coreference_recall": 0.,  
                                      "non_coreference_precision_num": 0., 
                                      "non_coreference_precision_denom": 6., 
                                      "non_coreference_precision": 0., 
                                      "non_coreference_f1": 0.,
                                      
                                      "blanc_recall_num": 0.142857142857143, 
                                      "blanc_recall_denom": 1.,
                                      "blanc_recall": 14.28,  
                                      "blanc_precision_num": 0.4, 
                                      "blanc_precision_denom": 1., 
                                      "blanc_precision": 40., 
                                      "blanc_f1": 21.05,
                                      }
    expected_blanc_results_per_document = []
    expected_metrics_results_collection = [("blanc", (expected_blanc_overall_results, expected_blanc_results_per_document)),
                                           ]
    expected_metrics_results_collection = tuple(expected_metrics_results_collection)
    data.append(((args, kwargs), expected_metrics_results_collection))
    
    ## all
    metric = "all"
    key_multi_documents_conll2012_file_path = os.path.join(DATA_FOLDER_PATH, "reference_documents_skip_singleton=True.conll")
    response_multi_documents_conll2012_file_path = os.path.join(DATA_FOLDER_PATH, "system_documents_skip_singleton=True.conll")
    name = "none"
    args = (metric, key_multi_documents_conll2012_file_path, response_multi_documents_conll2012_file_path)
    kwargs = {"name": name}
    
    expected_metrics_results_collection = []
    ### muc
    expected_muc_overall_results = {"overall_mention_identification_recall_num": 6.,
                                    "overall_mention_identification_recall_denom": 12.,
                                    "overall_mention_identification_recall": 50.,
                                    "overall_mention_identification_precision_num": 6.,
                                    "overall_mention_identification_precision_denom": 7.,
                                    "overall_mention_identification_precision": 85.71,
                                    "overall_mention_identification_f1": 63.15,
                                    "recall_num": 3.,
                                    "recall_denom": 8.,
                                    "recall": 37.5,
                                    "precision_num": 3.,
                                    "precision_denom": 4.,
                                    "precision": 75.,
                                    "f1": 50.,
                                    }
    expected_muc_results_per_document = []
    expected_metrics_results_collection.append(("muc", (expected_muc_overall_results, expected_muc_results_per_document)))
    ### bcub
    expected_bcub_overall_results = {"overall_mention_identification_recall_num": 6.,
                                     "overall_mention_identification_recall_denom": 12.,
                                     "overall_mention_identification_recall": 50.,
                                     "overall_mention_identification_precision_num": 6.,
                                     "overall_mention_identification_precision_denom": 7.,
                                     "overall_mention_identification_precision": 85.71,
                                     "overall_mention_identification_f1": 63.15,
                                     "recall_num": 3.5,
                                     "recall_denom": 12.,
                                     "recall": 29.16,
                                     "precision_num": 5.5,
                                     "precision_denom": 7.,
                                     "precision": 78.57,
                                     "f1": 42.54,
                                     }
    expected_bcub_results_per_document = []
    expected_metrics_results_collection.append(("bcub", (expected_bcub_overall_results, expected_bcub_results_per_document)))
    ### ceafm
    expected_ceafm_overall_results = {"overall_mention_identification_recall_num": 6.,
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,
                                      "overall_mention_identification_precision_num": 6.,
                                      "overall_mention_identification_precision_denom": 7.,
                                      "overall_mention_identification_precision": 85.71,
                                      "overall_mention_identification_f1": 63.15,
                                      "recall_num": 5.,
                                      "recall_denom": 12.,
                                      "recall": 41.66,
                                      "precision_num": 5.,
                                      "precision_denom": 7.,
                                      "precision": 71.42,
                                      "f1": 52.63,
                                      }
    expected_ceafm_results_per_document = []
    expected_metrics_results_collection.append(("ceafm", (expected_ceafm_overall_results, expected_ceafm_results_per_document)))
    ### ceafe
    expected_ceafe_overall_results = {"overall_mention_identification_recall_num": 6.,
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,
                                      "overall_mention_identification_precision_num": 6.,
                                      "overall_mention_identification_precision_denom": 7.,
                                      "overall_mention_identification_precision": 85.71,
                                      "overall_mention_identification_f1": 63.15,
                                      "recall_num": 1.52380952380952,
                                      "recall_denom": 4.,
                                      "recall": 38.09,
                                      "precision_num": 1.52380952380952,
                                      "precision_denom": 3.,
                                      "precision": 50.79,
                                      "f1": 43.53,
                                      }
    expected_ceafe_results_per_document = []
    expected_metrics_results_collection.append(("ceafe", (expected_ceafe_overall_results, expected_ceafe_results_per_document)))
    ### blanc
    expected_blanc_overall_results = {"overall_mention_identification_recall_num": 6., 
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,  
                                      "overall_mention_identification_precision_num": 6., 
                                      "overall_mention_identification_precision_denom": 7., 
                                      "overall_mention_identification_precision": 85.71, 
                                      "overall_mention_identification_f1": 63.15,
                                   
                                      "coreference_recall_num": 4., 
                                      "coreference_recall_denom": 14.,
                                      "coreference_recall": 28.57,  
                                      "coreference_precision_num": 4., 
                                      "coreference_precision_denom": 5., 
                                      "coreference_precision": 80., 
                                      "coreference_f1": 42.1,
                                      
                                      "non_coreference_recall_num": 0., 
                                      "non_coreference_recall_denom": 16.,
                                      "non_coreference_recall": 0.,  
                                      "non_coreference_precision_num": 0., 
                                      "non_coreference_precision_denom": 6., 
                                      "non_coreference_precision": 0., 
                                      "non_coreference_f1": 0.,
                                   
                                      "blanc_recall_num": 0.142857142857143, 
                                      "blanc_recall_denom": 1.,
                                      "blanc_recall": 14.28,  
                                      "blanc_precision_num": 0.4, 
                                      "blanc_precision_denom": 1., 
                                      "blanc_precision": 40., 
                                      "blanc_f1": 21.05,
                                      }
    expected_blanc_results_per_document = []
    expected_metrics_results_collection.append(("blanc", (expected_blanc_overall_results, expected_blanc_results_per_document)))
    expected_metrics_results_collection = tuple(expected_metrics_results_collection)
    data.append(((args, kwargs), expected_metrics_results_collection))
    
    
    # All documents
    ## ceafe
    metric = "ceafe"
    key_multi_documents_conll2012_file_path = os.path.join(DATA_FOLDER_PATH, "reference_documents_skip_singleton=True.conll")
    response_multi_documents_conll2012_file_path = os.path.join(DATA_FOLDER_PATH, "system_documents_skip_singleton=True.conll")
    name = None
    args = (metric, key_multi_documents_conll2012_file_path, response_multi_documents_conll2012_file_path)
    kwargs = {"name": name}
    
    expected_ceafe_overall_results = {"overall_mention_identification_recall_num": 6.,
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,
                                      "overall_mention_identification_precision_num": 6.,
                                      "overall_mention_identification_precision_denom": 7.,
                                      "overall_mention_identification_precision": 85.71,
                                      "overall_mention_identification_f1": 63.15,
                                      "recall_num": 1.52380952380952,
                                      "recall_denom": 4.,
                                      "recall": 38.09,
                                      "precision_num": 1.52380952380952,
                                      "precision_denom": 3.,
                                      "precision": 50.79,
                                      "f1": 43.53,
                                      }
    expected_ceafe_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6,
                                                                 "total_response_mentions_nb": 5, 
                                                                 "strictly_correct_identified_mentions_nb": 4,  
                                                                 "coreference_recall_num": 0.857142857142857, 
                                                                 "coreference_recall_denom": 2.,
                                                                 "coreference_recall": 42.85,  
                                                                 "coreference_precision_num": 0.857142857142857, 
                                                                 "coreference_precision_denom": 2., 
                                                                 "coreference_precision": 42.85, 
                                                                 "coreference_f1": 42.85,
                                                                  }),
                                           (("(doc2); part 001", "(doc2); part 001"), {"total_key_mentions_nb": 6,
                                                                 "total_response_mentions_nb": 2, 
                                                                 "strictly_correct_identified_mentions_nb": 2,  
                                                                 "coreference_recall_num": 0.666666666666667, 
                                                                 "coreference_recall_denom": 2.,
                                                                 "coreference_recall": 33.33,  
                                                                 "coreference_precision_num": 0.666666666666667, 
                                                                 "coreference_precision_denom": 1., 
                                                                 "coreference_precision": 66.66, 
                                                                 "coreference_f1": 44.44,
                                                                  }),
                                            ]
    expected_metrics_results_collection = [("ceafe", (expected_ceafe_overall_results, expected_ceafe_results_per_document)),
                                           ]
    expected_metrics_results_collection = tuple(expected_metrics_results_collection)
    data.append(((args, kwargs), expected_metrics_results_collection))
    
    ## blanc
    metric = "blanc"
    key_multi_documents_conll2012_file_path = os.path.join(DATA_FOLDER_PATH, "reference_documents_skip_singleton=True.conll")
    response_multi_documents_conll2012_file_path = os.path.join(DATA_FOLDER_PATH, "system_documents_skip_singleton=True.conll")
    name = None
    args = (metric, key_multi_documents_conll2012_file_path, response_multi_documents_conll2012_file_path)
    kwargs = {"name": name}
    
    expected_blanc_overall_results = {"overall_mention_identification_recall_num": 6.,
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,
                                      "overall_mention_identification_precision_num": 6.,
                                      "overall_mention_identification_precision_denom": 7.,
                                      "overall_mention_identification_precision": 85.71,
                                      "overall_mention_identification_f1": 63.15,
                                      
                                      "coreference_recall_num": 4., 
                                      "coreference_recall_denom": 14.,
                                      "coreference_recall": 28.57,  
                                      "coreference_precision_num": 4., 
                                      "coreference_precision_denom": 5., 
                                      "coreference_precision": 80., 
                                      "coreference_f1": 42.1,
                                      
                                      "non_coreference_recall_num": 0., 
                                      "non_coreference_recall_denom": 16.,
                                      "non_coreference_recall": 0.,  
                                      "non_coreference_precision_num": 0., 
                                      "non_coreference_precision_denom": 6., 
                                      "non_coreference_precision": 0., 
                                      "non_coreference_f1": 0.,
                                   
                                      "blanc_recall_num": 0.142857142857143, 
                                      "blanc_recall_denom": 1.,
                                      "blanc_recall": 14.28,  
                                      "blanc_precision_num": 0.4, 
                                      "blanc_precision_denom": 1., 
                                      "blanc_precision": 40., 
                                      "blanc_f1": 21.05,
                                      }
    expected_blanc_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6, 
                                                              "total_response_mentions_nb": 5, 
                                                              "strictly_correct_identified_mentions_nb": 4
                                                              }), 
                                           (("(doc2); part 001", "(doc2); part 001"), {"total_key_mentions_nb": 6, 
                                                              "total_response_mentions_nb": 2, 
                                                              "strictly_correct_identified_mentions_nb": 2
                                                              }), 
                                           ]
    expected_metrics_results_collection = [("blanc", (expected_blanc_overall_results, expected_blanc_results_per_document)),
                                           ]
    expected_metrics_results_collection = tuple(expected_metrics_results_collection)
    data.append(((args, kwargs), expected_metrics_results_collection))
    
    ## all
    metric = "all"
    key_multi_documents_conll2012_file_path = os.path.join(DATA_FOLDER_PATH, "reference_documents_skip_singleton=True.conll")
    response_multi_documents_conll2012_file_path = os.path.join(DATA_FOLDER_PATH, "system_documents_skip_singleton=True.conll")
    name = None
    args = (metric, key_multi_documents_conll2012_file_path, response_multi_documents_conll2012_file_path)
    kwargs = {"name": name}
    
    expected_metrics_results_collection = []
    ### muc
    expected_muc_overall_results = {"overall_mention_identification_recall_num": 6.,
                                    "overall_mention_identification_recall_denom": 12.,
                                    "overall_mention_identification_recall": 50.,
                                    "overall_mention_identification_precision_num": 6.,
                                    "overall_mention_identification_precision_denom": 7.,
                                    "overall_mention_identification_precision": 85.71,
                                    "overall_mention_identification_f1": 63.15,
                                    "recall_num": 3.,
                                    "recall_denom": 8.,
                                    "recall": 37.5,
                                    "precision_num": 3.,
                                    "precision_denom": 4.,
                                    "precision": 75.,
                                    "f1": 50.,
                                    }
    expected_muc_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6,
                                                                             "total_response_mentions_nb": 5, 
                                                                             "strictly_correct_identified_mentions_nb": 4,  
                                                                             "coreference_recall_num": 2., 
                                                                             "coreference_recall_denom": 4.,
                                                                             "coreference_recall": 50.,  
                                                                             "coreference_precision_num": 2., 
                                                                             "coreference_precision_denom": 3., 
                                                                             "coreference_precision": 66.66, 
                                                                             "coreference_f1": 57.14,
                                                                              }),
                                         (("(doc2); part 001", "(doc2); part 001"), {"total_key_mentions_nb": 6,
                                                                             "total_response_mentions_nb": 2, 
                                                                             "strictly_correct_identified_mentions_nb": 2,  
                                                                             "coreference_recall_num": 1., 
                                                                             "coreference_recall_denom": 4.,
                                                                             "coreference_recall": 25.,  
                                                                             "coreference_precision_num": 1., 
                                                                             "coreference_precision_denom": 1., 
                                                                             "coreference_precision": 100., 
                                                                             "coreference_f1": 40.,
                                                                              }),
                                         ]
    expected_metrics_results_collection.append(("muc", (expected_muc_overall_results, expected_muc_results_per_document)))
    ### bcub
    expected_bcub_overall_results = {"overall_mention_identification_recall_num": 6.,
                                     "overall_mention_identification_recall_denom": 12.,
                                     "overall_mention_identification_recall": 50.,
                                     "overall_mention_identification_precision_num": 6.,
                                     "overall_mention_identification_precision_denom": 7.,
                                     "overall_mention_identification_precision": 85.71,
                                     "overall_mention_identification_f1": 63.15,
                                     "recall_num": 3.5,
                                     "recall_denom": 12.,
                                     "recall": 29.16,
                                     "precision_num": 5.5,
                                     "precision_denom": 7.,
                                     "precision": 78.57,
                                     "f1": 42.54,
                                     }
    expected_bcub_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6,
                                                                             "total_response_mentions_nb": 5, 
                                                                             "strictly_correct_identified_mentions_nb": 4,  
                                                                             "coreference_recall_num": 2.5, 
                                                                             "coreference_recall_denom": 6.,
                                                                             "coreference_recall": 41.66,  
                                                                             "coreference_precision_num": 3.5, 
                                                                             "coreference_precision_denom": 5., 
                                                                             "coreference_precision": 70., 
                                                                             "coreference_f1": 52.23,
                                                                              }),
                                          (("(doc2); part 001", "(doc2); part 001"), {"total_key_mentions_nb": 6,
                                                                             "total_response_mentions_nb": 2, 
                                                                             "strictly_correct_identified_mentions_nb": 2,  
                                                                             "coreference_recall_num": 1., 
                                                                             "coreference_recall_denom": 6.,
                                                                             "coreference_recall": 16.66,  
                                                                             "coreference_precision_num": 2., 
                                                                             "coreference_precision_denom": 2., 
                                                                             "coreference_precision": 100., 
                                                                             "coreference_f1": 28.57,
                                                                              }),
                                          ]
    expected_metrics_results_collection.append(("bcub", (expected_bcub_overall_results, expected_bcub_results_per_document)))
    ### ceafm
    expected_ceafm_overall_results = {"overall_mention_identification_recall_num": 6.,
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,
                                      "overall_mention_identification_precision_num": 6.,
                                      "overall_mention_identification_precision_denom": 7.,
                                      "overall_mention_identification_precision": 85.71,
                                      "overall_mention_identification_f1": 63.15,
                                      "recall_num": 5.,
                                      "recall_denom": 12.,
                                      "recall": 41.66,
                                      "precision_num": 5.,
                                      "precision_denom": 7.,
                                      "precision": 71.42,
                                      "f1": 52.63,
                                      }
    expected_ceafm_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6,
                                                                             "total_response_mentions_nb": 5, 
                                                                             "strictly_correct_identified_mentions_nb": 4,  
                                                                             "coreference_recall_num": 3., 
                                                                             "coreference_recall_denom": 6.,
                                                                             "coreference_recall": 50.,  
                                                                             "coreference_precision_num": 3., 
                                                                             "coreference_precision_denom": 5., 
                                                                             "coreference_precision": 60., 
                                                                             "coreference_f1": 54.54,
                                                                              }),
                                           (("(doc2); part 001", "(doc2); part 001"), {"total_key_mentions_nb": 6,
                                                                             "total_response_mentions_nb": 2, 
                                                                             "strictly_correct_identified_mentions_nb": 2,  
                                                                             "coreference_recall_num": 2., 
                                                                             "coreference_recall_denom": 6.,
                                                                             "coreference_recall": 33.33,  
                                                                             "coreference_precision_num": 2., 
                                                                             "coreference_precision_denom": 2., 
                                                                             "coreference_precision": 100., 
                                                                             "coreference_f1": 50.,
                                                                              }),
                                           ]
    expected_metrics_results_collection.append(("ceafm", (expected_ceafm_overall_results, expected_ceafm_results_per_document)))
    ### ceafe
    expected_ceafe_overall_results = {"overall_mention_identification_recall_num": 6.,
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,
                                      "overall_mention_identification_precision_num": 6.,
                                      "overall_mention_identification_precision_denom": 7.,
                                      "overall_mention_identification_precision": 85.71,
                                      "overall_mention_identification_f1": 63.15,
                                      "recall_num": 1.52380952380952,
                                      "recall_denom": 4.,
                                      "recall": 38.09,
                                      "precision_num": 1.52380952380952,
                                      "precision_denom": 3.,
                                      "precision": 50.79,
                                      "f1": 43.53,
                                      }
    expected_ceafe_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6,
                                                                             "total_response_mentions_nb": 5, 
                                                                             "strictly_correct_identified_mentions_nb": 4,  
                                                                             "coreference_recall_num": 0.857142857142857, 
                                                                             "coreference_recall_denom": 2.,
                                                                             "coreference_recall": 42.85,  
                                                                             "coreference_precision_num": 0.857142857142857, 
                                                                             "coreference_precision_denom": 2., 
                                                                             "coreference_precision": 42.85, 
                                                                             "coreference_f1": 42.85,
                                                                              }),
                                            (("(doc2); part 001", "(doc2); part 001"), {"total_key_mentions_nb": 6,
                                                                             "total_response_mentions_nb": 2, 
                                                                             "strictly_correct_identified_mentions_nb": 2,  
                                                                             "coreference_recall_num": 0.666666666666667, 
                                                                             "coreference_recall_denom": 2.,
                                                                             "coreference_recall": 33.33,  
                                                                             "coreference_precision_num": 0.666666666666667, 
                                                                             "coreference_precision_denom": 1., 
                                                                             "coreference_precision": 66.66, 
                                                                             "coreference_f1": 44.44,
                                                                              }),
                                           ]
    expected_metrics_results_collection.append(("ceafe", (expected_ceafe_overall_results, expected_ceafe_results_per_document)))
    ### blanc
    expected_blanc_overall_results = {"overall_mention_identification_recall_num": 6., 
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,  
                                      "overall_mention_identification_precision_num": 6., 
                                      "overall_mention_identification_precision_denom": 7., 
                                      "overall_mention_identification_precision": 85.71, 
                                      "overall_mention_identification_f1": 63.15,
                                   
                                      "coreference_recall_num": 4., 
                                      "coreference_recall_denom": 14.,
                                      "coreference_recall": 28.57,  
                                      "coreference_precision_num": 4., 
                                      "coreference_precision_denom": 5., 
                                      "coreference_precision": 80., 
                                      "coreference_f1": 42.1,
                                      
                                      "non_coreference_recall_num": 0., 
                                      "non_coreference_recall_denom": 16.,
                                      "non_coreference_recall": 0.,  
                                      "non_coreference_precision_num": 0., 
                                      "non_coreference_precision_denom": 6., 
                                      "non_coreference_precision": 0., 
                                      "non_coreference_f1": 0.,
                                   
                                      "blanc_recall_num": 0.142857142857143, 
                                      "blanc_recall_denom": 1.,
                                      "blanc_recall": 14.28,  
                                      "blanc_precision_num": 0.4, 
                                      "blanc_precision_denom": 1., 
                                      "blanc_precision": 40., 
                                      "blanc_f1": 21.05,
                                      }
    expected_blanc_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6,
                                                                             "total_response_mentions_nb": 5, 
                                                                             "strictly_correct_identified_mentions_nb": 4}),
                                           (("(doc2); part 001", "(doc2); part 001"), {"total_key_mentions_nb": 6,
                                                                             "total_response_mentions_nb": 2, 
                                                                             "strictly_correct_identified_mentions_nb": 2}),
                                           ]
    expected_metrics_results_collection.append(("blanc", (expected_blanc_overall_results, expected_blanc_results_per_document)))
    expected_metrics_results_collection = tuple(expected_metrics_results_collection)
    data.append(((args, kwargs), expected_metrics_results_collection))
     
    # One specific document
    ## ceafe
    metric = "ceafe"
    key_multi_documents_conll2012_file_path = os.path.join(DATA_FOLDER_PATH, "reference_documents_skip_singleton=True.conll")
    response_multi_documents_conll2012_file_path = os.path.join(DATA_FOLDER_PATH, "system_documents_skip_singleton=True.conll")
    name = "(doc1); part 000"
    args = (metric, key_multi_documents_conll2012_file_path, response_multi_documents_conll2012_file_path)
    kwargs = {"name": name}
    
    expected_ceafe_overall_results = {"overall_mention_identification_recall_num": 4.,
                                      "overall_mention_identification_recall_denom": 6.,
                                      "overall_mention_identification_recall": 66.66,
                                      "overall_mention_identification_precision_num": 4.,
                                      "overall_mention_identification_precision_denom": 5.,
                                      "overall_mention_identification_precision": 80.,
                                      "overall_mention_identification_f1": 72.72,
                                      "recall_num": 0.857142857142857,
                                      "recall_denom": 2.,
                                      "recall": 42.85,
                                      "precision_num": 0.857142857142857,
                                      "precision_denom": 2.,
                                      "precision": 42.85,
                                      "f1": 42.85,
                                      }
    expected_ceafe_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6,
                                                                             "total_response_mentions_nb": 5, 
                                                                             "strictly_correct_identified_mentions_nb": 4,  
                                                                             "coreference_recall_num": 0.857142857142857, 
                                                                             "coreference_recall_denom": 2.,
                                                                             "coreference_recall": 42.85,  
                                                                             "coreference_precision_num": 0.857142857142857, 
                                                                             "coreference_precision_denom": 2., 
                                                                             "coreference_precision": 42.85, 
                                                                             "coreference_f1": 42.85,
                                                                              }),
                                           ]
    expected_metrics_results_collection = [("ceafe", (expected_ceafe_overall_results, expected_ceafe_results_per_document)),
                                           ]
    expected_metrics_results_collection = tuple(expected_metrics_results_collection)
    data.append(((args, kwargs), expected_metrics_results_collection))
    
    ## blanc
    metric = "blanc"
    key_multi_documents_conll2012_file_path = os.path.join(DATA_FOLDER_PATH, "reference_documents_skip_singleton=True.conll")
    response_multi_documents_conll2012_file_path = os.path.join(DATA_FOLDER_PATH, "system_documents_skip_singleton=True.conll")
    name = "(doc1); part 000"
    args = (metric, key_multi_documents_conll2012_file_path, response_multi_documents_conll2012_file_path)
    kwargs = {"name": name}
    
    expected_blanc_overall_results = {"overall_mention_identification_recall_num": 4.,
                                      "overall_mention_identification_recall_denom": 6.,
                                      "overall_mention_identification_recall": 66.66,
                                      "overall_mention_identification_precision_num": 4.,
                                      "overall_mention_identification_precision_denom": 5.,
                                      "overall_mention_identification_precision": 80.,
                                      "overall_mention_identification_f1": 72.72,
                                      
                                      "coreference_recall_num": 3., 
                                      "coreference_recall_denom": 7.,
                                      "coreference_recall": 42.85,  
                                      "coreference_precision_num": 3., 
                                      "coreference_precision_denom": 4., 
                                      "coreference_precision": 75., 
                                      "coreference_f1": 54.54,
                                      
                                      "non_coreference_recall_num": 0., 
                                      "non_coreference_recall_denom": 8.,
                                      "non_coreference_recall": 0.,  
                                      "non_coreference_precision_num": 0., 
                                      "non_coreference_precision_denom": 6., 
                                      "non_coreference_precision": 0., 
                                      "non_coreference_f1": 0.,
                                   
                                      "blanc_recall_num": 0.214285714285714, 
                                      "blanc_recall_denom": 1.,
                                      "blanc_recall": 21.42,  
                                      "blanc_precision_num": 0.375, 
                                      "blanc_precision_denom": 1., 
                                      "blanc_precision": 37.5, 
                                      "blanc_f1": 27.27,
                                      }
    expected_blanc_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6,
                                                                             "total_response_mentions_nb": 5, 
                                                                             "strictly_correct_identified_mentions_nb": 4,
                                                                             
                                                                             "coreference_recall_num": 3., 
                                                                             "coreference_recall_denom": 7.,
                                                                             "coreference_recall": 42.85,  
                                                                             "coreference_precision_num": 3., 
                                                                             "coreference_precision_denom": 4., 
                                                                             "coreference_precision": 75., 
                                                                             "coreference_f1": 54.54,
                                                                             
                                                                             "non_coreference_recall_num": 0., 
                                                                             "non_coreference_recall_denom": 8.,
                                                                             "non_coreference_recall": 0.,  
                                                                             "non_coreference_precision_num": 0., 
                                                                             "non_coreference_precision_denom": 6., 
                                                                             "non_coreference_precision": 0., 
                                                                             "non_coreference_f1": 0.,
                                                                             
                                                                             "blanc_recall_num": 0.214285714285714, 
                                                                             "blanc_recall_denom": 1.,
                                                                             "blanc_recall": 21.42,  
                                                                             "blanc_precision_num": 0.375, 
                                                                             "blanc_precision_denom": 1., 
                                                                             "blanc_precision": 37.5, 
                                                                             "blanc_f1": 27.27,
                                                                             }),
                                           ]
    expected_metrics_results_collection = [("blanc", (expected_blanc_overall_results, expected_blanc_results_per_document)),
                                           ]
    expected_metrics_results_collection = tuple(expected_metrics_results_collection)
    data.append(((args, kwargs), expected_metrics_results_collection))
    
    ## all
    metric = "all"
    key_multi_documents_conll2012_file_path = os.path.join(DATA_FOLDER_PATH, "reference_documents_skip_singleton=True.conll")
    response_multi_documents_conll2012_file_path = os.path.join(DATA_FOLDER_PATH, "system_documents_skip_singleton=True.conll")
    name = "(doc1); part 000"
    args = (metric, key_multi_documents_conll2012_file_path, response_multi_documents_conll2012_file_path)
    kwargs = {"name": name}
    
    expected_metrics_results_collection = []
    ### muc
    expected_muc_overall_results = {"overall_mention_identification_recall_num": 4.,
                                      "overall_mention_identification_recall_denom": 6.,
                                      "overall_mention_identification_recall": 66.66,
                                      "overall_mention_identification_precision_num": 4.,
                                      "overall_mention_identification_precision_denom": 5.,
                                      "overall_mention_identification_precision": 80.,
                                      "overall_mention_identification_f1": 72.72,
                                      "recall_num": 2.,
                                      "recall_denom": 4.,
                                      "recall": 50.,
                                      "precision_num": 2.,
                                      "precision_denom": 3.,
                                      "precision": 66.66,
                                      "f1": 57.14,
                                      }
    expected_muc_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6,
                                                                           "total_response_mentions_nb": 5, 
                                                                           "strictly_correct_identified_mentions_nb": 4,  
                                                                           "coreference_recall_num": 2., 
                                                                           "coreference_recall_denom": 4.,
                                                                           "coreference_recall": 50.,  
                                                                           "coreference_precision_num": 2., 
                                                                           "coreference_precision_denom": 3., 
                                                                           "coreference_precision": 66.66, 
                                                                           "coreference_f1": 57.14,
                                                                           }),
                                         ]
    expected_metrics_results_collection.append(("muc", (expected_muc_overall_results, expected_muc_results_per_document)))
    ### bcub
    expected_bcub_overall_results = {"overall_mention_identification_recall_num": 4.,
                                      "overall_mention_identification_recall_denom": 6.,
                                      "overall_mention_identification_recall": 66.66,
                                      "overall_mention_identification_precision_num": 4.,
                                      "overall_mention_identification_precision_denom": 5.,
                                      "overall_mention_identification_precision": 80.,
                                      "overall_mention_identification_f1": 72.72,
                                      "recall_num": 2.5,
                                      "recall_denom": 6.,
                                      "recall": 41.66,
                                      "precision_num": 3.5,
                                      "precision_denom": 5.,
                                      "precision": 70.,
                                      "f1": 52.23,
                                      }
    expected_bcub_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6,
                                                                             "total_response_mentions_nb": 5, 
                                                                             "strictly_correct_identified_mentions_nb": 4,  
                                                                             "coreference_recall_num": 2.5, 
                                                                             "coreference_recall_denom": 6.,
                                                                             "coreference_recall": 41.66,  
                                                                             "coreference_precision_num": 3.5, 
                                                                             "coreference_precision_denom": 5., 
                                                                             "coreference_precision": 70., 
                                                                             "coreference_f1": 52.23,
                                                                              }),
                                          ]
    expected_metrics_results_collection.append(("bcub", (expected_bcub_overall_results, expected_bcub_results_per_document)))
    ### ceafm
    expected_ceafm_overall_results = {"overall_mention_identification_recall_num": 4.,
                                      "overall_mention_identification_recall_denom": 6.,
                                      "overall_mention_identification_recall": 66.66,
                                      "overall_mention_identification_precision_num": 4.,
                                      "overall_mention_identification_precision_denom": 5.,
                                      "overall_mention_identification_precision": 80.,
                                      "overall_mention_identification_f1": 72.72,
                                      "recall_num": 3.,
                                      "recall_denom": 6.,
                                      "recall": 50.,
                                      "precision_num": 3.,
                                      "precision_denom": 5.,
                                      "precision": 60.,
                                      "f1": 54.54,
                                      }
    expected_ceafm_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6,
                                                                                     "total_response_mentions_nb": 5, 
                                                                                     "strictly_correct_identified_mentions_nb": 4,  
                                                                                     "coreference_recall_num": 3., 
                                                                                     "coreference_recall_denom": 6.,
                                                                                     "coreference_recall": 50.,  
                                                                                     "coreference_precision_num": 3., 
                                                                                     "coreference_precision_denom": 5., 
                                                                                     "coreference_precision": 60., 
                                                                                     "coreference_f1": 54.54,
                                                                                      }),
                                           ]
    expected_metrics_results_collection.append(("ceafm", (expected_ceafm_overall_results, expected_ceafm_results_per_document)))
    ### ceafe
    expected_ceafe_overall_results = {"overall_mention_identification_recall_num": 4.,
                                      "overall_mention_identification_recall_denom": 6.,
                                      "overall_mention_identification_recall": 66.66,
                                      "overall_mention_identification_precision_num": 4.,
                                      "overall_mention_identification_precision_denom": 5.,
                                      "overall_mention_identification_precision": 80.,
                                      "overall_mention_identification_f1": 72.72,
                                      "recall_num": 0.857142857142857,
                                      "recall_denom": 2.,
                                      "recall": 42.85,
                                      "precision_num": 0.857142857142857,
                                      "precision_denom": 2.,
                                      "precision": 42.85,
                                      "f1": 42.85,
                                      }
    expected_ceafe_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6,
                                                                             "total_response_mentions_nb": 5, 
                                                                             "strictly_correct_identified_mentions_nb": 4,  
                                                                             "coreference_recall_num": 0.857142857142857, 
                                                                             "coreference_recall_denom": 2.,
                                                                             "coreference_recall": 42.85,  
                                                                             "coreference_precision_num": 0.857142857142857, 
                                                                             "coreference_precision_denom": 2., 
                                                                             "coreference_precision": 42.85, 
                                                                             "coreference_f1": 42.85,
                                                                              }),
                                           ]
    expected_metrics_results_collection.append(("ceafe", (expected_ceafe_overall_results, expected_ceafe_results_per_document)))
    ### blanc
    expected_blanc_overall_results = {"overall_mention_identification_recall_num": 4.,
                                      "overall_mention_identification_recall_denom": 6.,
                                      "overall_mention_identification_recall": 66.66,
                                      "overall_mention_identification_precision_num": 4.,
                                      "overall_mention_identification_precision_denom": 5.,
                                      "overall_mention_identification_precision": 80.,
                                      "overall_mention_identification_f1": 72.72,
                                      
                                      "coreference_recall_num": 3., 
                                      "coreference_recall_denom": 7.,
                                      "coreference_recall": 42.85,  
                                      "coreference_precision_num": 3., 
                                      "coreference_precision_denom": 4., 
                                      "coreference_precision": 75., 
                                      "coreference_f1": 54.54,
                                      
                                      "non_coreference_recall_num": 0., 
                                      "non_coreference_recall_denom": 8.,
                                      "non_coreference_recall": 0.,  
                                      "non_coreference_precision_num": 0., 
                                      "non_coreference_precision_denom": 6., 
                                      "non_coreference_precision": 0., 
                                      "non_coreference_f1": 0.,
                                   
                                      "blanc_recall_num": 0.214285714285714, 
                                      "blanc_recall_denom": 1.,
                                      "blanc_recall": 21.42,  
                                      "blanc_precision_num": 0.375, 
                                      "blanc_precision_denom": 1., 
                                      "blanc_precision": 37.5, 
                                      "blanc_f1": 27.27,
                                      }
    expected_blanc_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6,
                                                                             "total_response_mentions_nb": 5, 
                                                                             "strictly_correct_identified_mentions_nb": 4,
                                                                             
                                                                             "coreference_recall_num": 3., 
                                                                             "coreference_recall_denom": 7.,
                                                                             "coreference_recall": 42.85,  
                                                                             "coreference_precision_num": 3., 
                                                                             "coreference_precision_denom": 4., 
                                                                             "coreference_precision": 75., 
                                                                             "coreference_f1": 54.54,
                                                                             
                                                                             "non_coreference_recall_num": 0., 
                                                                             "non_coreference_recall_denom": 8.,
                                                                             "non_coreference_recall": 0.,  
                                                                             "non_coreference_precision_num": 0., 
                                                                             "non_coreference_precision_denom": 6., 
                                                                             "non_coreference_precision": 0., 
                                                                             "non_coreference_f1": 0.,
                                                                             
                                                                             "blanc_recall_num": 0.214285714285714, 
                                                                             "blanc_recall_denom": 1.,
                                                                             "blanc_recall": 21.42,  
                                                                             "blanc_precision_num": 0.375, 
                                                                             "blanc_precision_denom": 1., 
                                                                             "blanc_precision": 37.5, 
                                                                             "blanc_f1": 27.27,
                                                                             }),
                                           ]
    expected_metrics_results_collection.append(("blanc", (expected_blanc_overall_results, expected_blanc_results_per_document)))
    expected_metrics_results_collection = tuple(expected_metrics_results_collection)
    data.append(((args, kwargs), expected_metrics_results_collection))
    
    
    # One specific document that does not exist
    ## ceafe
    metric = "ceafe"
    key_multi_documents_conll2012_file_path = os.path.join(DATA_FOLDER_PATH, "reference_documents_skip_singleton=True.conll")
    response_multi_documents_conll2012_file_path = os.path.join(DATA_FOLDER_PATH, "system_documents_skip_singleton=True.conll")
    name = "glop"
    args = (metric, key_multi_documents_conll2012_file_path, response_multi_documents_conll2012_file_path)
    kwargs = {"name": name}
    
    expected_ceafe_overall_results = {"overall_mention_identification_recall_num": 0.,
                                      "overall_mention_identification_recall_denom": 0.,
                                      "overall_mention_identification_recall": 0.,
                                      "overall_mention_identification_precision_num": 0.,
                                      "overall_mention_identification_precision_denom": 0.,
                                      "overall_mention_identification_precision": 0.,
                                      "overall_mention_identification_f1": 0.,
                                      "recall_num": 0.,
                                      "recall_denom": 0.,
                                      "recall": 0.,
                                      "precision_num": 0.,
                                      "precision_denom": 0.,
                                      "precision": 0.,
                                      "f1": 0.,
                                      }
    expected_ceafe_results_per_document = [(("", ""), {"total_key_mentions_nb": 0,
                                                                             "total_response_mentions_nb": 0, 
                                                                             "strictly_correct_identified_mentions_nb": 0,  
                                                                             "coreference_recall_num": 0., 
                                                                             "coreference_recall_denom": 0.,
                                                                             "coreference_recall": 0.,  
                                                                             "coreference_precision_num": 0., 
                                                                             "coreference_precision_denom": 0., 
                                                                             "coreference_precision": 0., 
                                                                             "coreference_f1": 0.,
                                                                              }),
                                           ]
    expected_metrics_results_collection = [("ceafe", (expected_ceafe_overall_results, expected_ceafe_results_per_document)),
                                           ]
    expected_metrics_results_collection = tuple(expected_metrics_results_collection)
    data.append(((args, kwargs), expected_metrics_results_collection))
    
    ## blanc
    metric = "blanc"
    key_multi_documents_conll2012_file_path = os.path.join(DATA_FOLDER_PATH, "reference_documents_skip_singleton=True.conll")
    response_multi_documents_conll2012_file_path = os.path.join(DATA_FOLDER_PATH, "system_documents_skip_singleton=True.conll")
    name = "glop"
    args = (metric, key_multi_documents_conll2012_file_path, response_multi_documents_conll2012_file_path)
    kwargs = {"name": name}
    
    expected_blanc_overall_results = {"overall_mention_identification_recall_num": 0.,
                                      "overall_mention_identification_recall_denom": 0.,
                                      "overall_mention_identification_recall": 0.,
                                      "overall_mention_identification_precision_num": 0.,
                                      "overall_mention_identification_precision_denom": 0.,
                                      "overall_mention_identification_precision": 0.,
                                      "overall_mention_identification_f1": 0.,
                                      
                                      "coreference_recall_num": 0., 
                                      "coreference_recall_denom": 0.,
                                      "coreference_recall": 0.,  
                                      "coreference_precision_num": 0., 
                                      "coreference_precision_denom": 0., 
                                      "coreference_precision": 0., 
                                      "coreference_f1": 0.,
                                      
                                      "non_coreference_recall_num": 0., 
                                      "non_coreference_recall_denom": 0.,
                                      "non_coreference_recall": 0.,  
                                      "non_coreference_precision_num": 0., 
                                      "non_coreference_precision_denom": 0., 
                                      "non_coreference_precision": 0., 
                                      "non_coreference_f1": 0.,
                                   
                                      "blanc_recall_num": 0., 
                                      "blanc_recall_denom": 1.,
                                      "blanc_recall": 0.,  
                                      "blanc_precision_num": 0., 
                                      "blanc_precision_denom": 1., 
                                      "blanc_precision": 0., 
                                      "blanc_f1": 0.,
                                      }
    expected_blanc_results_per_document = [(("", ""), {"total_key_mentions_nb": 0,
                                                                             "total_response_mentions_nb": 0, 
                                                                             "strictly_correct_identified_mentions_nb": 0,
                                                                             
                                                                             "coreference_recall_num": 0., 
                                                                             "coreference_recall_denom": 0.,
                                                                             "coreference_recall": 0.,  
                                                                             "coreference_precision_num": 0., 
                                                                             "coreference_precision_denom": 0., 
                                                                             "coreference_precision": 0., 
                                                                             "coreference_f1": 0.,
                                                                             
                                                                             "non_coreference_recall_num": 0., 
                                                                             "non_coreference_recall_denom": 0.,
                                                                             "non_coreference_recall": 0.,  
                                                                             "non_coreference_precision_num": 0., 
                                                                             "non_coreference_precision_denom": 0., 
                                                                             "non_coreference_precision": 0., 
                                                                             "non_coreference_f1": 0.,
                                                                             
                                                                             "blanc_recall_num": 0., 
                                                                             "blanc_recall_denom": 1.,
                                                                             "blanc_recall": 0.,  
                                                                             "blanc_precision_num": 0., 
                                                                             "blanc_precision_denom": 1., 
                                                                             "blanc_precision": 0., 
                                                                             "blanc_f1": 0.,
                                                                              }),
                                           ]
    expected_metrics_results_collection = [("blanc", (expected_blanc_overall_results, expected_blanc_results_per_document)),
                                           ]
    expected_metrics_results_collection = tuple(expected_metrics_results_collection)
    data.append(((args, kwargs), expected_metrics_results_collection))
    
    ## all
    metric = "all"
    key_multi_documents_conll2012_file_path = os.path.join(DATA_FOLDER_PATH, "reference_documents_skip_singleton=True.conll")
    response_multi_documents_conll2012_file_path = os.path.join(DATA_FOLDER_PATH, "system_documents_skip_singleton=True.conll")
    name = "glop"
    args = (metric, key_multi_documents_conll2012_file_path, response_multi_documents_conll2012_file_path)
    kwargs = {"name": name}
    
    expected_metrics_results_collection = []
    ### muc
    expected_muc_overall_results = {"overall_mention_identification_recall_num": 0.,
                                      "overall_mention_identification_recall_denom": 0.,
                                      "overall_mention_identification_recall": 0.,
                                      "overall_mention_identification_precision_num": 0.,
                                      "overall_mention_identification_precision_denom": 0.,
                                      "overall_mention_identification_precision": 0.,
                                      "overall_mention_identification_f1": 0.,
                                      "recall_num": 0.,
                                      "recall_denom": 0.,
                                      "recall": 0.,
                                      "precision_num": 0.,
                                      "precision_denom": 0.,
                                      "precision": 0.,
                                      "f1": 0.,
                                      }
    expected_muc_results_per_document = [(("", ""), {"total_key_mentions_nb": 0,
                                                                             "total_response_mentions_nb": 0, 
                                                                             "strictly_correct_identified_mentions_nb": 0,  
                                                                             "coreference_recall_num": 0., 
                                                                             "coreference_recall_denom": 0.,
                                                                             "coreference_recall": 0.,  
                                                                             "coreference_precision_num": 0., 
                                                                             "coreference_precision_denom": 0., 
                                                                             "coreference_precision": 0., 
                                                                             "coreference_f1": 0.,
                                                                              }),
                                         ]
    expected_metrics_results_collection.append(("muc", (expected_muc_overall_results, expected_muc_results_per_document)))
    ### bcub
    expected_bcub_overall_results = {"overall_mention_identification_recall_num": 0.,
                                      "overall_mention_identification_recall_denom": 0.,
                                      "overall_mention_identification_recall": 0.,
                                      "overall_mention_identification_precision_num": 0.,
                                      "overall_mention_identification_precision_denom": 0.,
                                      "overall_mention_identification_precision": 0.,
                                      "overall_mention_identification_f1": 0.,
                                      "recall_num": 0.,
                                      "recall_denom": 0.,
                                      "recall": 0.,
                                      "precision_num": 0.,
                                      "precision_denom": 0.,
                                      "precision": 0.,
                                      "f1": 0.,
                                      }
    expected_bcub_results_per_document = [(("", ""), {"total_key_mentions_nb": 0,
                                                                             "total_response_mentions_nb": 0, 
                                                                             "strictly_correct_identified_mentions_nb": 0,  
                                                                             "coreference_recall_num": 0., 
                                                                             "coreference_recall_denom": 0.,
                                                                             "coreference_recall": 0.,  
                                                                             "coreference_precision_num": 0., 
                                                                             "coreference_precision_denom": 0., 
                                                                             "coreference_precision": 0., 
                                                                             "coreference_f1": 0.,
                                                                              }),
                                          ]
    expected_metrics_results_collection.append(("bcub", (expected_bcub_overall_results, expected_bcub_results_per_document)))
    ### ceafm
    expected_ceafm_overall_results = {"overall_mention_identification_recall_num": 0.,
                                      "overall_mention_identification_recall_denom": 0.,
                                      "overall_mention_identification_recall": 0.,
                                      "overall_mention_identification_precision_num": 0.,
                                      "overall_mention_identification_precision_denom": 0.,
                                      "overall_mention_identification_precision": 0.,
                                      "overall_mention_identification_f1": 0.,
                                      "recall_num": 0.,
                                      "recall_denom": 0.,
                                      "recall": 0.,
                                      "precision_num": 0.,
                                      "precision_denom": 0.,
                                      "precision": 0.,
                                      "f1": 0.,
                                      }
    expected_ceafm_results_per_document = [(("", ""), {"total_key_mentions_nb": 0,
                                                                             "total_response_mentions_nb": 0, 
                                                                             "strictly_correct_identified_mentions_nb": 0,  
                                                                             "coreference_recall_num": 0., 
                                                                             "coreference_recall_denom": 0.,
                                                                             "coreference_recall": 0.,  
                                                                             "coreference_precision_num": 0., 
                                                                             "coreference_precision_denom": 0., 
                                                                             "coreference_precision": 0., 
                                                                             "coreference_f1": 0.,
                                                                              }),
                                           ]
    expected_metrics_results_collection.append(("ceafm", (expected_ceafm_overall_results, expected_ceafm_results_per_document)))
    ### ceafe
    expected_ceafe_overall_results = {"overall_mention_identification_recall_num": 0.,
                                      "overall_mention_identification_recall_denom": 0.,
                                      "overall_mention_identification_recall": 0.,
                                      "overall_mention_identification_precision_num": 0.,
                                      "overall_mention_identification_precision_denom": 0.,
                                      "overall_mention_identification_precision": 0.,
                                      "overall_mention_identification_f1": 0.,
                                      "recall_num": 0.,
                                      "recall_denom": 0.,
                                      "recall": 0.,
                                      "precision_num": 0.,
                                      "precision_denom": 0.,
                                      "precision": 0.,
                                      "f1": 0.,
                                      }
    expected_ceafe_results_per_document = [(("", ""), {"total_key_mentions_nb": 0,
                                                                             "total_response_mentions_nb": 0, 
                                                                             "strictly_correct_identified_mentions_nb": 0,  
                                                                             "coreference_recall_num": 0., 
                                                                             "coreference_recall_denom": 0.,
                                                                             "coreference_recall": 0.,  
                                                                             "coreference_precision_num": 0., 
                                                                             "coreference_precision_denom": 0., 
                                                                             "coreference_precision": 0., 
                                                                             "coreference_f1": 0.,
                                                                              }),
                                           ]
    expected_metrics_results_collection.append(("ceafe", (expected_ceafe_overall_results, expected_ceafe_results_per_document)))
    ### blanc
    expected_blanc_overall_results = {"overall_mention_identification_recall_num": 0.,
                                      "overall_mention_identification_recall_denom": 0.,
                                      "overall_mention_identification_recall": 0.,
                                      "overall_mention_identification_precision_num": 0.,
                                      "overall_mention_identification_precision_denom": 0.,
                                      "overall_mention_identification_precision": 0.,
                                      "overall_mention_identification_f1": 0.,
                                      
                                      "coreference_recall_num": 0., 
                                      "coreference_recall_denom": 0.,
                                      "coreference_recall": 0.,  
                                      "coreference_precision_num": 0., 
                                      "coreference_precision_denom": 0., 
                                      "coreference_precision": 0., 
                                      "coreference_f1": 0.,
                                      
                                      "non_coreference_recall_num": 0., 
                                      "non_coreference_recall_denom": 0.,
                                      "non_coreference_recall": 0.,  
                                      "non_coreference_precision_num": 0., 
                                      "non_coreference_precision_denom": 0., 
                                      "non_coreference_precision": 0., 
                                      "non_coreference_f1": 0.,
                                   
                                      "blanc_recall_num": 0., 
                                      "blanc_recall_denom": 1.,
                                      "blanc_recall": 0.,  
                                      "blanc_precision_num": 0., 
                                      "blanc_precision_denom": 1., 
                                      "blanc_precision": 0., 
                                      "blanc_f1": 0.,
                                      }
    expected_blanc_results_per_document = [(("", ""), {"total_key_mentions_nb": 0,
                                                                             "total_response_mentions_nb": 0, 
                                                                             "strictly_correct_identified_mentions_nb": 0,
                                                                             
                                                                             "coreference_recall_num": 0., 
                                                                             "coreference_recall_denom": 0.,
                                                                             "coreference_recall": 0.,  
                                                                             "coreference_precision_num": 0., 
                                                                             "coreference_precision_denom": 0., 
                                                                             "coreference_precision": 0., 
                                                                             "coreference_f1": 0.,
                                                                             
                                                                             "non_coreference_recall_num": 0., 
                                                                             "non_coreference_recall_denom": 0.,
                                                                             "non_coreference_recall": 0.,  
                                                                             "non_coreference_precision_num": 0., 
                                                                             "non_coreference_precision_denom": 0., 
                                                                             "non_coreference_precision": 0., 
                                                                             "non_coreference_f1": 0.,
                                                                             
                                                                             "blanc_recall_num": 0., 
                                                                             "blanc_recall_denom": 1.,
                                                                             "blanc_recall": 0.,  
                                                                             "blanc_precision_num": 0., 
                                                                             "blanc_precision_denom": 1., 
                                                                             "blanc_precision": 0., 
                                                                             "blanc_f1": 0.,
                                                                              }),
                                           ]
    expected_metrics_results_collection.append(("blanc", (expected_blanc_overall_results, expected_blanc_results_per_document)))
    expected_metrics_results_collection = tuple(expected_metrics_results_collection)
    data.append(((args, kwargs), expected_metrics_results_collection))
    
    return data


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()