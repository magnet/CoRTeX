# -*- coding: utf-8 -*-

import unittest

from cortex.tools.wrapper.conll2012_scorer.output_parser import (_parse_conll_metric_single_document_results, 
                                                                 _parse_conll_metric_multi_documents_results, 
                                                                 _parse_conll_blanc_single_document_results, 
                                                                 _parse_conll_blanc_multi_document_results,
                                                                 parse_conll_scoring_software_output,
                                                                )


class Test(unittest.TestCase):
    
    #@unittest.skip
    def test_conll_ceafe_document_result_parser(self):
        # Test parameters
        string = """====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11) (20,20)
Entity 1: (6,9) (16,17)
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11)
Entity 1: (13,17) (20,20)
(doc1); part 000:
Total key mentions: 6
Total response mentions: 5
Strictly correct identified mentions: 4
Partially correct identified mentions: 0
No identified: 2
Invented: 1
Recall: (0.857142857142857 / 2) 42.85%    Precision: (0.857142857142857 / 2) 42.85%    F1: 42.85%
--------------------------------------------------------------------------
"""     
        expected_found_doc_names = ("(doc1); part 000", "(doc1); part 000")
        expected_total_key_mentions_nb = 6
        expected_total_response_mentions_nb = 5
        expected_strictly_correct_identified_mentions_nb = 4
        expected_coreference_recall_num = 0.857142857142857
        expected_coreference_recall_denom = 2.
        expected_coreference_recall = 42.85
        expected_coreference_precision_num = 0.857142857142857
        expected_coreference_precision_denom = 2.
        expected_coreference_precision = 42.85
        expected_coreference_f1 = 42.85
        expected_results = {"total_key_mentions_nb": expected_total_key_mentions_nb,
                           "total_response_mentions_nb": expected_total_response_mentions_nb, 
                           "strictly_correct_identified_mentions_nb": expected_strictly_correct_identified_mentions_nb,  
                           "coreference_recall_num": expected_coreference_recall_num, 
                           "coreference_recall_denom": expected_coreference_recall_denom,
                           "coreference_recall": expected_coreference_recall,  
                           "coreference_precision_num": expected_coreference_precision_num, 
                           "coreference_precision_denom": expected_coreference_precision_denom, 
                           "coreference_precision": expected_coreference_precision, 
                           "coreference_f1": expected_coreference_f1,
                          }
        
        
        # Test
        line_iterator = iter(string.split("\n"))
        actual_doc_name, actual_results = _parse_conll_metric_single_document_results(line_iterator)
        self.assertEqual(expected_found_doc_names, actual_doc_name)
        self.assertEqual(expected_results, actual_results)
    
    
    #@unittest.skip
    def test_conll_muc_multi_document_result_parser(self):
        # Test parameters
        string = """====> (doc2); part 001:
File (doc2); part 001:
Entity 0: (0,1) (3,9) (11,11) (20,20)
Entity 1: (6,9) (16,17)
====> (doc2); part 001:
File (doc2); part 001:
Entity 0: (0,1) (11,11)
(doc2); part 001:
Total key mentions: 6
Total response mentions: 2
Strictly correct identified mentions: 2
Partially correct identified mentions: 0
No identified: 4
Invented: 0
Recall: (0.666666666666667 / 2) 33.33%    Precision: (0.666666666666667 / 1) 66.66%    F1: 44.44%
--------------------------------------------------------------------------
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11) (20,20)
Entity 1: (6,9) (16,17)
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11)
Entity 1: (13,17) (20,20)
(doc1); part 000:
Total key mentions: 6
Total response mentions: 5
Strictly correct identified mentions: 4
Partially correct identified mentions: 0
No identified: 2
Invented: 1
Recall: (0.857142857142857 / 2) 42.85%    Precision: (0.857142857142857 / 2) 42.85%    F1: 42.85%
--------------------------------------------------------------------------

====== TOTALS =======
Identification of Mentions: Recall: (6 / 12) 50%    Precision: (6 / 7) 85.71%    F1: 63.15%
--------------------------------------------------------------------------
Coreference: Recall: (1.52380952380952 / 4) 38.09%    Precision: (1.52380952380952 / 3) 50.79%    F1: 43.53%
--------------------------------------------------------------------------

"""     
        
        expected_overall_results = {"overall_mention_identification_recall_num": 6.,
                                    "overall_mention_identification_recall_denom": 12.,
                                    "overall_mention_identification_recall": 50.,
                                    "overall_mention_identification_precision_num": 6.,
                                    "overall_mention_identification_precision_denom": 7.,
                                    "overall_mention_identification_precision": 85.71,
                                    "overall_mention_identification_f1": 63.15,
                                    "recall_num": 1.52380952380952,
                                    "recall_denom": 4.,
                                    "recall": 38.09,
                                    "precision_num": 1.52380952380952,
                                    "precision_denom": 3.,
                                    "precision": 50.79,
                                    "f1": 43.53,
                                    }
        
        expected_results_per_document = [(("(doc2); part 001", "(doc2); part 001"), 
                                          {"total_key_mentions_nb": 6,
                                           "total_response_mentions_nb": 2, 
                                           "strictly_correct_identified_mentions_nb": 2,  
                                           "coreference_recall_num": 0.666666666666667, 
                                           "coreference_recall_denom": 2,
                                           "coreference_recall": 33.33,  
                                           "coreference_precision_num": 0.666666666666667, 
                                           "coreference_precision_denom": 1., 
                                           "coreference_precision": 66.66, 
                                           "coreference_f1": 44.44,
                                          }),
                                         (("(doc1); part 000", "(doc1); part 000"),
                                          {"total_key_mentions_nb": 6,
                                           "total_response_mentions_nb": 5, 
                                           "strictly_correct_identified_mentions_nb": 4,  
                                           "coreference_recall_num": 0.857142857142857, 
                                           "coreference_recall_denom": 2.,
                                           "coreference_recall": 42.85,  
                                           "coreference_precision_num": 0.857142857142857, 
                                           "coreference_precision_denom": 2., 
                                           "coreference_precision": 42.85, 
                                           "coreference_f1": 42.85,
                                          }),
                                         ]
        
        # Test
        line_iterator = iter(string.split("\n"))
        actual_overall_results, actual_results_per_document = _parse_conll_metric_multi_documents_results(line_iterator)
        self.assertEqual(expected_overall_results, actual_overall_results)
        self.assertEqual(expected_results_per_document, actual_results_per_document)
    
    
    #@unittest.skip
    def test_conll_blanc_document_result_parser(self):
        # Test parameters
        string = """(doc1); part 000:
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11) (20,20)
Entity 1: (6,9) (16,17)
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11)
Entity 1: (13,17) (20,20)
Total key mentions: 6
Total response mentions: 5
Strictly correct identified mentions: 4
Partially correct identified mentions: 0
No identified: 2
Invented: 1

====== TOTALS =======
Identification of Mentions: Recall: (4 / 6) 66.66%    Precision: (4 / 5) 80%    F1: 72.72%
--------------------------------------------------------------------------

Coreference:
Coreference links: Recall: (3 / 7) 42.85%    Precision: (3 / 4) 75%    F1: 54.54%
--------------------------------------------------------------------------
Non-coreference links: Recall: (0 / 8) 0%    Precision: (0 / 6) 0%    F1: 0%
--------------------------------------------------------------------------
BLANC: Recall: (0.214285714285714 / 1) 21.42%    Precision: (0.375 / 1) 37.5%    F1: 27.27%
--------------------------------------------------------------------------
"""     
        expected_found_doc_names = ("(doc1); part 000", "(doc1); part 000")
        expected_total_key_mentions_nb = 6
        expected_total_response_mentions_nb = 5
        expected_strictly_correct_identified_mentions_nb = 4
        expected_results = {"total_key_mentions_nb": expected_total_key_mentions_nb,
                            "total_response_mentions_nb": expected_total_response_mentions_nb, 
                            "strictly_correct_identified_mentions_nb": expected_strictly_correct_identified_mentions_nb,
                           
                            "coreference_recall_num": 3., 
                            "coreference_recall_denom": 7.,
                            "coreference_recall": 42.85,  
                            "coreference_precision_num": 3., 
                            "coreference_precision_denom": 4., 
                            "coreference_precision": 75., 
                            "coreference_f1": 54.54,
                           
                            "non_coreference_recall_num": 0., 
                            "non_coreference_recall_denom": 8.,
                            "non_coreference_recall": 0.,  
                            "non_coreference_precision_num": 0., 
                            "non_coreference_precision_denom": 6., 
                            "non_coreference_precision": 0., 
                            "non_coreference_f1": 0.,
                           
                            "blanc_recall_num": 0.214285714285714, 
                            "blanc_recall_denom": 1.,
                            "blanc_recall": 21.42,  
                            "blanc_precision_num": 0.375, 
                            "blanc_precision_denom": 1., 
                            "blanc_precision": 37.5, 
                            "blanc_f1": 27.27,
                          }
        
        
        # Test
        line_iterator = iter(string.split("\n"))
        actual_doc_name, actual_results = _parse_conll_blanc_single_document_results(line_iterator)
        self.assertEqual(expected_found_doc_names, actual_doc_name)
        self.assertEqual(expected_results, actual_results)
    
    
    
    #@unittest.skip
    def test_conll_blanc_multi_document_result_parser(self):
        # Test parameters
        string = """====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11) (20,20)
Entity 1: (6,9) (16,17)
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11)
Entity 1: (13,17) (20,20)
:
Total key mentions: 6
Total response mentions: 5
Strictly correct identified mentions: 4
Partially correct identified mentions: 0
No identified: 2
Invented: 1
====> (doc2); part 001:
File (doc2); part 001:
Entity 0: (0,1) (3,9) (11,11) (20,20)
Entity 1: (6,9) (16,17)
====> (doc2); part 001:
File (doc2); part 001:
Entity 0: (0,1) (11,11)
:
Total key mentions: 6
Total response mentions: 2
Strictly correct identified mentions: 2
Partially correct identified mentions: 0
No identified: 4
Invented: 0

====== TOTALS =======
Identification of Mentions: Recall: (6 / 12) 50%    Precision: (6 / 7) 85.71%    F1: 63.15%
--------------------------------------------------------------------------

Coreference:
Coreference links: Recall: (4 / 14) 28.57%    Precision: (4 / 5) 80%    F1: 42.1%
--------------------------------------------------------------------------
Non-coreference links: Recall: (0 / 16) 0%    Precision: (0 / 6) 0%    F1: 0%
--------------------------------------------------------------------------
BLANC: Recall: (0.142857142857143 / 1) 14.28%    Precision: (0.4 / 1) 40%    F1: 21.05%
--------------------------------------------------------------------------
"""     
        expected_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), 
                                         {"total_key_mentions_nb": 6, 
                                          "total_response_mentions_nb": 5, 
                                          "strictly_correct_identified_mentions_nb": 4
                                          }), 
                                         (("(doc2); part 001", "(doc2); part 001"), 
                                         {"total_key_mentions_nb": 6, 
                                          "total_response_mentions_nb": 2, 
                                          "strictly_correct_identified_mentions_nb": 2
                                          }), 
                                         ]
        expected_results = {"overall_mention_identification_recall_num": 6., 
                            "overall_mention_identification_recall_denom": 12.,
                            "overall_mention_identification_recall": 50.,  
                            "overall_mention_identification_precision_num": 6., 
                            "overall_mention_identification_precision_denom": 7., 
                            "overall_mention_identification_precision": 85.71, 
                            "overall_mention_identification_f1": 63.15,
                           
                            "coreference_recall_num": 4., 
                            "coreference_recall_denom": 14.,
                            "coreference_recall": 28.57,  
                            "coreference_precision_num": 4., 
                            "coreference_precision_denom": 5., 
                            "coreference_precision": 80., 
                            "coreference_f1": 42.1,
                           
                            "non_coreference_recall_num": 0., 
                            "non_coreference_recall_denom": 16.,
                            "non_coreference_recall": 0.,  
                            "non_coreference_precision_num": 0., 
                            "non_coreference_precision_denom": 6., 
                            "non_coreference_precision": 0., 
                            "non_coreference_f1": 0.,
                           
                            "blanc_recall_num": 0.142857142857143, 
                            "blanc_recall_denom": 1.,
                            "blanc_recall": 14.28,  
                            "blanc_precision_num": 0.4, 
                            "blanc_precision_denom": 1., 
                            "blanc_precision": 40., 
                            "blanc_f1": 21.05,
                            }
        
        # Test
        line_iterator = iter(string.split("\n"))
        actual_results, actual_results_per_document = _parse_conll_blanc_multi_document_results(line_iterator)
        self.assertEqual(expected_results_per_document, actual_results_per_document)
        self.assertEqual(expected_results, actual_results)
    
    
    #@unittest.skip
    def test_parse_conll_scoring_software_output(self):
        # Test parameters
        data = get_test_parse_conll_scoring_software_output_data()
        
        # Test
        for (args, kwargs), expected_metrics_results_collection in data:
            actual_metrics_results_collection = parse_conll_scoring_software_output(*args, **kwargs)
            self.assertEqual(expected_metrics_results_collection, actual_metrics_results_collection)


def get_test_parse_conll_scoring_software_output_data():
    data = []
    
    # No document
    ## ceafe
    string = """version: 8.0 ../lib/CorScorer.pm

====== TOTALS =======
Identification of Mentions: Recall: (6 / 12) 50%    Precision: (6 / 7) 85.71%    F1: 63.15%
--------------------------------------------------------------------------
Coreference: Recall: (1.52380952380952 / 4) 38.09%    Precision: (1.52380952380952 / 3) 50.79%    F1: 43.53%
--------------------------------------------------------------------------
"""
    line_iterator = iter(string.split("\n"))
    metric = "ceafe"
    which_document = "none"
    args = (line_iterator, metric)
    kwargs = {"which_document": which_document}
    expected_ceafe_overall_results = {"overall_mention_identification_recall_num": 6.,
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,
                                      "overall_mention_identification_precision_num": 6.,
                                      "overall_mention_identification_precision_denom": 7.,
                                      "overall_mention_identification_precision": 85.71,
                                      "overall_mention_identification_f1": 63.15,
                                      "recall_num": 1.52380952380952,
                                      "recall_denom": 4.,
                                      "recall": 38.09,
                                      "precision_num": 1.52380952380952,
                                      "precision_denom": 3.,
                                      "precision": 50.79,
                                      "f1": 43.53,
                                      }
    expected_ceafe_results_per_document = []
    expected_metrics_results_collection = [("ceafe", (expected_ceafe_overall_results, expected_ceafe_results_per_document)),
                                           ]
    expected_metrics_results_collection = tuple(expected_metrics_results_collection)
    data.append(((args, kwargs), expected_metrics_results_collection))
    
    ## blanc
    string = """version: 8.0 ../lib/CorScorer.pm

====== TOTALS =======
Identification of Mentions: Recall: (6 / 12) 50%    Precision: (6 / 7) 85.71%    F1: 63.15%
--------------------------------------------------------------------------

Coreference:
Coreference links: Recall: (4 / 14) 28.57%    Precision: (4 / 5) 80%    F1: 42.1%
--------------------------------------------------------------------------
Non-coreference links: Recall: (0 / 16) 0%    Precision: (0 / 6) 0%    F1: 0%
--------------------------------------------------------------------------
BLANC: Recall: (0.142857142857143 / 1) 14.28%    Precision: (0.4 / 1) 40%    F1: 21.05%
--------------------------------------------------------------------------
"""
    line_iterator = iter(string.split("\n"))
    metric = "blanc"
    which_document = "none"
    args = (line_iterator, metric)
    kwargs = {"which_document": which_document}
    expected_blanc_overall_results = {"overall_mention_identification_recall_num": 6., 
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,  
                                      "overall_mention_identification_precision_num": 6., 
                                      "overall_mention_identification_precision_denom": 7., 
                                      "overall_mention_identification_precision": 85.71, 
                                      "overall_mention_identification_f1": 63.15,
                                      
                                      "coreference_recall_num": 4., 
                                      "coreference_recall_denom": 14.,
                                      "coreference_recall": 28.57,  
                                      "coreference_precision_num": 4., 
                                      "coreference_precision_denom": 5., 
                                      "coreference_precision": 80., 
                                      "coreference_f1": 42.1,
                                      
                                      "non_coreference_recall_num": 0., 
                                      "non_coreference_recall_denom": 16.,
                                      "non_coreference_recall": 0.,  
                                      "non_coreference_precision_num": 0., 
                                      "non_coreference_precision_denom": 6., 
                                      "non_coreference_precision": 0., 
                                      "non_coreference_f1": 0.,
                                      
                                      "blanc_recall_num": 0.142857142857143, 
                                      "blanc_recall_denom": 1.,
                                      "blanc_recall": 14.28,  
                                      "blanc_precision_num": 0.4, 
                                      "blanc_precision_denom": 1., 
                                      "blanc_precision": 40., 
                                      "blanc_f1": 21.05,
                                      }
    expected_blanc_results_per_document = []
    expected_metrics_results_collection = [("blanc", (expected_blanc_overall_results, expected_blanc_results_per_document)),
                                           ]
    expected_metrics_results_collection = tuple(expected_metrics_results_collection)
    data.append(((args, kwargs), expected_metrics_results_collection))
    
    ## all
    string = """version: 8.0 ../lib/CorScorer.pm

METRIC muc:

====== TOTALS =======
Identification of Mentions: Recall: (6 / 12) 50%    Precision: (6 / 7) 85.71%    F1: 63.15%
--------------------------------------------------------------------------
Coreference: Recall: (3 / 8) 37.5%    Precision: (3 / 4) 75%    F1: 50%
--------------------------------------------------------------------------

METRIC bcub:

====== TOTALS =======
Identification of Mentions: Recall: (6 / 12) 50%    Precision: (6 / 7) 85.71%    F1: 63.15%
--------------------------------------------------------------------------
Coreference: Recall: (3.5 / 12) 29.16%    Precision: (5.5 / 7) 78.57%    F1: 42.54%
--------------------------------------------------------------------------

METRIC ceafm:

====== TOTALS =======
Identification of Mentions: Recall: (6 / 12) 50%    Precision: (6 / 7) 85.71%    F1: 63.15%
--------------------------------------------------------------------------
Coreference: Recall: (5 / 12) 41.66%    Precision: (5 / 7) 71.42%    F1: 52.63%
--------------------------------------------------------------------------

METRIC ceafe:

====== TOTALS =======
Identification of Mentions: Recall: (6 / 12) 50%    Precision: (6 / 7) 85.71%    F1: 63.15%
--------------------------------------------------------------------------
Coreference: Recall: (1.52380952380952 / 4) 38.09%    Precision: (1.52380952380952 / 3) 50.79%    F1: 43.53%
--------------------------------------------------------------------------

METRIC blanc:

====== TOTALS =======
Identification of Mentions: Recall: (6 / 12) 50%    Precision: (6 / 7) 85.71%    F1: 63.15%
--------------------------------------------------------------------------

Coreference:
Coreference links: Recall: (4 / 14) 28.57%    Precision: (4 / 5) 80%    F1: 42.1%
--------------------------------------------------------------------------
Non-coreference links: Recall: (0 / 16) 0%    Precision: (0 / 6) 0%    F1: 0%
--------------------------------------------------------------------------
BLANC: Recall: (0.142857142857143 / 1) 14.28%    Precision: (0.4 / 1) 40%    F1: 21.05%
--------------------------------------------------------------------------
"""
    line_iterator = iter(string.split("\n"))
    metric = "all"
    which_document = "none"
    args = (line_iterator, metric)
    kwargs = {"which_document": which_document}
    
    expected_metrics_results_collection = []
    ### muc
    expected_muc_overall_results = {"overall_mention_identification_recall_num": 6.,
                                    "overall_mention_identification_recall_denom": 12.,
                                    "overall_mention_identification_recall": 50.,
                                    "overall_mention_identification_precision_num": 6.,
                                    "overall_mention_identification_precision_denom": 7.,
                                    "overall_mention_identification_precision": 85.71,
                                    "overall_mention_identification_f1": 63.15,
                                    "recall_num": 3.,
                                    "recall_denom": 8.,
                                    "recall": 37.5,
                                    "precision_num": 3.,
                                    "precision_denom": 4.,
                                    "precision": 75.,
                                    "f1": 50.,
                                    }
    expected_muc_results_per_document = []
    expected_metrics_results_collection.append(("muc", (expected_muc_overall_results, expected_muc_results_per_document)))
    ### bcub
    expected_bcub_overall_results = {"overall_mention_identification_recall_num": 6.,
                                     "overall_mention_identification_recall_denom": 12.,
                                     "overall_mention_identification_recall": 50.,
                                     "overall_mention_identification_precision_num": 6.,
                                     "overall_mention_identification_precision_denom": 7.,
                                     "overall_mention_identification_precision": 85.71,
                                     "overall_mention_identification_f1": 63.15,
                                     "recall_num": 3.5,
                                     "recall_denom": 12.,
                                     "recall": 29.16,
                                     "precision_num": 5.5,
                                     "precision_denom": 7.,
                                     "precision": 78.57,
                                     "f1": 42.54,
                                     }
    expected_bcub_results_per_document = []
    expected_metrics_results_collection.append(("bcub", (expected_bcub_overall_results, expected_bcub_results_per_document)))
    ### ceafm
    expected_ceafm_overall_results = {"overall_mention_identification_recall_num": 6.,
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,
                                      "overall_mention_identification_precision_num": 6.,
                                      "overall_mention_identification_precision_denom": 7.,
                                      "overall_mention_identification_precision": 85.71,
                                      "overall_mention_identification_f1": 63.15,
                                      "recall_num": 5.,
                                      "recall_denom": 12.,
                                      "recall": 41.66,
                                      "precision_num": 5.,
                                      "precision_denom": 7.,
                                      "precision": 71.42,
                                      "f1": 52.63,
                                      }
    expected_ceafm_results_per_document = []
    expected_metrics_results_collection.append(("ceafm", (expected_ceafm_overall_results, expected_ceafm_results_per_document)))
    ### ceafe
    expected_ceafe_overall_results = {"overall_mention_identification_recall_num": 6.,
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,
                                      "overall_mention_identification_precision_num": 6.,
                                      "overall_mention_identification_precision_denom": 7.,
                                      "overall_mention_identification_precision": 85.71,
                                      "overall_mention_identification_f1": 63.15,
                                      "recall_num": 1.52380952380952,
                                      "recall_denom": 4.,
                                      "recall": 38.09,
                                      "precision_num": 1.52380952380952,
                                      "precision_denom": 3.,
                                      "precision": 50.79,
                                      "f1": 43.53,
                                      }
    expected_ceafe_results_per_document = []
    expected_metrics_results_collection.append(("ceafe", (expected_ceafe_overall_results, expected_ceafe_results_per_document)))
    ### blanc
    expected_blanc_overall_results = {"overall_mention_identification_recall_num": 6., 
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,  
                                      "overall_mention_identification_precision_num": 6., 
                                      "overall_mention_identification_precision_denom": 7., 
                                      "overall_mention_identification_precision": 85.71, 
                                      "overall_mention_identification_f1": 63.15,
                                   
                                      "coreference_recall_num": 4., 
                                      "coreference_recall_denom": 14.,
                                      "coreference_recall": 28.57,  
                                      "coreference_precision_num": 4., 
                                      "coreference_precision_denom": 5., 
                                      "coreference_precision": 80., 
                                      "coreference_f1": 42.1,
                                      
                                      "non_coreference_recall_num": 0., 
                                      "non_coreference_recall_denom": 16.,
                                      "non_coreference_recall": 0.,  
                                      "non_coreference_precision_num": 0., 
                                      "non_coreference_precision_denom": 6., 
                                      "non_coreference_precision": 0., 
                                      "non_coreference_f1": 0.,
                                   
                                      "blanc_recall_num": 0.142857142857143, 
                                      "blanc_recall_denom": 1.,
                                      "blanc_recall": 14.28,  
                                      "blanc_precision_num": 0.4, 
                                      "blanc_precision_denom": 1., 
                                      "blanc_precision": 40., 
                                      "blanc_f1": 21.05,
                                      }
    expected_blanc_results_per_document = []
    expected_metrics_results_collection.append(("blanc", (expected_blanc_overall_results, expected_blanc_results_per_document)))
    expected_metrics_results_collection = tuple(expected_metrics_results_collection)
    data.append(((args, kwargs), expected_metrics_results_collection))
    
    
    # All documents
    ## ceafe
    string = """version: 8.0 ../lib/CorScorer.pm
====> (doc2); part 001:
File (doc2); part 001:
Entity 0: (0,1) (3,9) (11,11) (20,20)
Entity 1: (6,9) (16,17)
====> (doc2); part 001:
File (doc2); part 001:
Entity 0: (0,1) (11,11)
(doc2); part 001:
Total key mentions: 6
Total response mentions: 2
Strictly correct identified mentions: 2
Partially correct identified mentions: 0
No identified: 4
Invented: 0
Recall: (0.666666666666667 / 2) 33.33%    Precision: (0.666666666666667 / 1) 66.66%    F1: 44.44%
--------------------------------------------------------------------------
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11) (20,20)
Entity 1: (6,9) (16,17)
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11)
Entity 1: (13,17) (20,20)
(doc1); part 000:
Total key mentions: 6
Total response mentions: 5
Strictly correct identified mentions: 4
Partially correct identified mentions: 0
No identified: 2
Invented: 1
Recall: (0.857142857142857 / 2) 42.85%    Precision: (0.857142857142857 / 2) 42.85%    F1: 42.85%
--------------------------------------------------------------------------

====== TOTALS =======
Identification of Mentions: Recall: (6 / 12) 50%    Precision: (6 / 7) 85.71%    F1: 63.15%
--------------------------------------------------------------------------
Coreference: Recall: (1.52380952380952 / 4) 38.09%    Precision: (1.52380952380952 / 3) 50.79%    F1: 43.53%
--------------------------------------------------------------------------
"""
    line_iterator = iter(string.split("\n"))
    metric = "ceafe"
    which_document = None
    args = (line_iterator, metric)
    kwargs = {"which_document": which_document}
    expected_ceafe_overall_results = {"overall_mention_identification_recall_num": 6.,
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,
                                      "overall_mention_identification_precision_num": 6.,
                                      "overall_mention_identification_precision_denom": 7.,
                                      "overall_mention_identification_precision": 85.71,
                                      "overall_mention_identification_f1": 63.15,
                                      "recall_num": 1.52380952380952,
                                      "recall_denom": 4.,
                                      "recall": 38.09,
                                      "precision_num": 1.52380952380952,
                                      "precision_denom": 3.,
                                      "precision": 50.79,
                                      "f1": 43.53,
                                      }
    expected_ceafe_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6,
                                                                                         "total_response_mentions_nb": 5, 
                                                                                         "strictly_correct_identified_mentions_nb": 4,  
                                                                                         "coreference_recall_num": 0.857142857142857, 
                                                                                         "coreference_recall_denom": 2.,
                                                                                         "coreference_recall": 42.85,  
                                                                                         "coreference_precision_num": 0.857142857142857, 
                                                                                         "coreference_precision_denom": 2., 
                                                                                         "coreference_precision": 42.85, 
                                                                                         "coreference_f1": 42.85,
                                                                                          }),
                                           (("(doc2); part 001", "(doc2); part 001"), {"total_key_mentions_nb": 6,
                                                                                     "total_response_mentions_nb": 2, 
                                                                                     "strictly_correct_identified_mentions_nb": 2,  
                                                                                     "coreference_recall_num": 0.666666666666667, 
                                                                                     "coreference_recall_denom": 2.,
                                                                                     "coreference_recall": 33.33,  
                                                                                     "coreference_precision_num": 0.666666666666667, 
                                                                                     "coreference_precision_denom": 1., 
                                                                                     "coreference_precision": 66.66, 
                                                                                     "coreference_f1": 44.44,
                                                                                      }),
                                            ]
    expected_metrics_results_collection = [("ceafe", (expected_ceafe_overall_results, expected_ceafe_results_per_document)),
                                           ]
    expected_metrics_results_collection = tuple(expected_metrics_results_collection)
    data.append(((args, kwargs), expected_metrics_results_collection))
    
    ## blanc
    string = """version: 8.0 ../lib/CorScorer.pm
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11) (20,20)
Entity 1: (6,9) (16,17)
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11)
Entity 1: (13,17) (20,20)
:
Total key mentions: 6
Total response mentions: 5
Strictly correct identified mentions: 4
Partially correct identified mentions: 0
No identified: 2
Invented: 1
====> (doc2); part 001:
File (doc2); part 001:
Entity 0: (0,1) (3,9) (11,11) (20,20)
Entity 1: (6,9) (16,17)
====> (doc2); part 001:
File (doc2); part 001:
Entity 0: (0,1) (11,11)
:
Total key mentions: 6
Total response mentions: 2
Strictly correct identified mentions: 2
Partially correct identified mentions: 0
No identified: 4
Invented: 0

====== TOTALS =======
Identification of Mentions: Recall: (6 / 12) 50%    Precision: (6 / 7) 85.71%    F1: 63.15%
--------------------------------------------------------------------------

Coreference:
Coreference links: Recall: (4 / 14) 28.57%    Precision: (4 / 5) 80%    F1: 42.1%
--------------------------------------------------------------------------
Non-coreference links: Recall: (0 / 16) 0%    Precision: (0 / 6) 0%    F1: 0%
--------------------------------------------------------------------------
BLANC: Recall: (0.142857142857143 / 1) 14.28%    Precision: (0.4 / 1) 40%    F1: 21.05%
--------------------------------------------------------------------------
"""    
    line_iterator = iter(string.split("\n"))
    metric = "blanc"
    which_document = None
    args = (line_iterator, metric)
    kwargs = {"which_document": which_document}
    expected_blanc_overall_results = {"overall_mention_identification_recall_num": 6.,
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,
                                      "overall_mention_identification_precision_num": 6.,
                                      "overall_mention_identification_precision_denom": 7.,
                                      "overall_mention_identification_precision": 85.71,
                                      "overall_mention_identification_f1": 63.15,
                                      
                                      "coreference_recall_num": 4., 
                                      "coreference_recall_denom": 14.,
                                      "coreference_recall": 28.57,  
                                      "coreference_precision_num": 4., 
                                      "coreference_precision_denom": 5., 
                                      "coreference_precision": 80., 
                                      "coreference_f1": 42.1,
                                      
                                      "non_coreference_recall_num": 0., 
                                      "non_coreference_recall_denom": 16.,
                                      "non_coreference_recall": 0.,  
                                      "non_coreference_precision_num": 0., 
                                      "non_coreference_precision_denom": 6., 
                                      "non_coreference_precision": 0., 
                                      "non_coreference_f1": 0.,
                                   
                                      "blanc_recall_num": 0.142857142857143, 
                                      "blanc_recall_denom": 1.,
                                      "blanc_recall": 14.28,  
                                      "blanc_precision_num": 0.4, 
                                      "blanc_precision_denom": 1., 
                                      "blanc_precision": 40., 
                                      "blanc_f1": 21.05,
                                      }
    expected_blanc_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6, 
                                                                                      "total_response_mentions_nb": 5, 
                                                                                      "strictly_correct_identified_mentions_nb": 4
                                                                                      }), 
                                           (("(doc2); part 001", "(doc2); part 001"), {"total_key_mentions_nb": 6, 
                                                                                      "total_response_mentions_nb": 2, 
                                                                                      "strictly_correct_identified_mentions_nb": 2
                                                                                      }), 
                                           ]
    expected_metrics_results_collection = [("blanc", (expected_blanc_overall_results, expected_blanc_results_per_document)),
                                           ]
    expected_metrics_results_collection = tuple(expected_metrics_results_collection)
    data.append(((args, kwargs), expected_metrics_results_collection))
    
    ## all
    string = """version: 8.0 ../lib/CorScorer.pm

METRIC muc:
====> (doc2); part 001:
File (doc2); part 001:
Entity 0: (0,1) (3,9) (11,11) (20,20)
Entity 1: (6,9) (16,17)
====> (doc2); part 001:
File (doc2); part 001:
Entity 0: (0,1) (11,11)
(doc2); part 001:
Total key mentions: 6
Total response mentions: 2
Strictly correct identified mentions: 2
Partially correct identified mentions: 0
No identified: 4
Invented: 0
Recall: (1 / 4) 25%    Precision: (1 / 1) 100%    F1: 40%
--------------------------------------------------------------------------
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11) (20,20)
Entity 1: (6,9) (16,17)
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11)
Entity 1: (13,17) (20,20)
(doc1); part 000:
Total key mentions: 6
Total response mentions: 5
Strictly correct identified mentions: 4
Partially correct identified mentions: 0
No identified: 2
Invented: 1
Recall: (2 / 4) 50%    Precision: (2 / 3) 66.66%    F1: 57.14%
--------------------------------------------------------------------------

====== TOTALS =======
Identification of Mentions: Recall: (6 / 12) 50%    Precision: (6 / 7) 85.71%    F1: 63.15%
--------------------------------------------------------------------------
Coreference: Recall: (3 / 8) 37.5%    Precision: (3 / 4) 75%    F1: 50%
--------------------------------------------------------------------------

METRIC bcub:
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11) (20,20)
Entity 1: (6,9) (16,17)
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11)
Entity 1: (13,17) (20,20)
(doc1); part 000:
Total key mentions: 6
Total response mentions: 5
Strictly correct identified mentions: 4
Partially correct identified mentions: 0
No identified: 2
Invented: 1
Recall: (2.5 / 6) 41.66%    Precision: (3.5 / 5) 70%    F1: 52.23%
--------------------------------------------------------------------------
====> (doc2); part 001:
File (doc2); part 001:
Entity 0: (0,1) (3,9) (11,11) (20,20)
Entity 1: (6,9) (16,17)
====> (doc2); part 001:
File (doc2); part 001:
Entity 0: (0,1) (11,11)
(doc2); part 001:
Total key mentions: 6
Total response mentions: 2
Strictly correct identified mentions: 2
Partially correct identified mentions: 0
No identified: 4
Invented: 0
Recall: (1 / 6) 16.66%    Precision: (2 / 2) 100%    F1: 28.57%
--------------------------------------------------------------------------

====== TOTALS =======
Identification of Mentions: Recall: (6 / 12) 50%    Precision: (6 / 7) 85.71%    F1: 63.15%
--------------------------------------------------------------------------
Coreference: Recall: (3.5 / 12) 29.16%    Precision: (5.5 / 7) 78.57%    F1: 42.54%
--------------------------------------------------------------------------

METRIC ceafm:
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11) (20,20)
Entity 1: (6,9) (16,17)
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11)
Entity 1: (13,17) (20,20)
(doc1); part 000:
Total key mentions: 6
Total response mentions: 5
Strictly correct identified mentions: 4
Partially correct identified mentions: 0
No identified: 2
Invented: 1
Recall: (3 / 6) 50%    Precision: (3 / 5) 60%    F1: 54.54%
--------------------------------------------------------------------------
====> (doc2); part 001:
File (doc2); part 001:
Entity 0: (0,1) (3,9) (11,11) (20,20)
Entity 1: (6,9) (16,17)
====> (doc2); part 001:
File (doc2); part 001:
Entity 0: (0,1) (11,11)
(doc2); part 001:
Total key mentions: 6
Total response mentions: 2
Strictly correct identified mentions: 2
Partially correct identified mentions: 0
No identified: 4
Invented: 0
Recall: (2 / 6) 33.33%    Precision: (2 / 2) 100%    F1: 50%
--------------------------------------------------------------------------

====== TOTALS =======
Identification of Mentions: Recall: (6 / 12) 50%    Precision: (6 / 7) 85.71%    F1: 63.15%
--------------------------------------------------------------------------
Coreference: Recall: (5 / 12) 41.66%    Precision: (5 / 7) 71.42%    F1: 52.63%
--------------------------------------------------------------------------

METRIC ceafe:
====> (doc2); part 001:
File (doc2); part 001:
Entity 0: (0,1) (3,9) (11,11) (20,20)
Entity 1: (6,9) (16,17)
====> (doc2); part 001:
File (doc2); part 001:
Entity 0: (0,1) (11,11)
(doc2); part 001:
Total key mentions: 6
Total response mentions: 2
Strictly correct identified mentions: 2
Partially correct identified mentions: 0
No identified: 4
Invented: 0
Recall: (0.666666666666667 / 2) 33.33%    Precision: (0.666666666666667 / 1) 66.66%    F1: 44.44%
--------------------------------------------------------------------------
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11) (20,20)
Entity 1: (6,9) (16,17)
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11)
Entity 1: (13,17) (20,20)
(doc1); part 000:
Total key mentions: 6
Total response mentions: 5
Strictly correct identified mentions: 4
Partially correct identified mentions: 0
No identified: 2
Invented: 1
Recall: (0.857142857142857 / 2) 42.85%    Precision: (0.857142857142857 / 2) 42.85%    F1: 42.85%
--------------------------------------------------------------------------

====== TOTALS =======
Identification of Mentions: Recall: (6 / 12) 50%    Precision: (6 / 7) 85.71%    F1: 63.15%
--------------------------------------------------------------------------
Coreference: Recall: (1.52380952380952 / 4) 38.09%    Precision: (1.52380952380952 / 3) 50.79%    F1: 43.53%
--------------------------------------------------------------------------

METRIC blanc:
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11) (20,20)
Entity 1: (6,9) (16,17)
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11)
Entity 1: (13,17) (20,20)
:
Total key mentions: 6
Total response mentions: 5
Strictly correct identified mentions: 4
Partially correct identified mentions: 0
No identified: 2
Invented: 1
====> (doc2); part 001:
File (doc2); part 001:
Entity 0: (0,1) (3,9) (11,11) (20,20)
Entity 1: (6,9) (16,17)
====> (doc2); part 001:
File (doc2); part 001:
Entity 0: (0,1) (11,11)
:
Total key mentions: 6
Total response mentions: 2
Strictly correct identified mentions: 2
Partially correct identified mentions: 0
No identified: 4
Invented: 0

====== TOTALS =======
Identification of Mentions: Recall: (6 / 12) 50%    Precision: (6 / 7) 85.71%    F1: 63.15%
--------------------------------------------------------------------------

Coreference:
Coreference links: Recall: (4 / 14) 28.57%    Precision: (4 / 5) 80%    F1: 42.1%
--------------------------------------------------------------------------
Non-coreference links: Recall: (0 / 16) 0%    Precision: (0 / 6) 0%    F1: 0%
--------------------------------------------------------------------------
BLANC: Recall: (0.142857142857143 / 1) 14.28%    Precision: (0.4 / 1) 40%    F1: 21.05%
--------------------------------------------------------------------------
"""    
    line_iterator = iter(string.split("\n"))
    metric = "all"
    which_document = None
    args = (line_iterator, metric)
    kwargs = {"which_document": which_document}
    
    expected_metrics_results_collection = []
    ### muc
    expected_muc_overall_results = {"overall_mention_identification_recall_num": 6.,
                                    "overall_mention_identification_recall_denom": 12.,
                                    "overall_mention_identification_recall": 50.,
                                    "overall_mention_identification_precision_num": 6.,
                                    "overall_mention_identification_precision_denom": 7.,
                                    "overall_mention_identification_precision": 85.71,
                                    "overall_mention_identification_f1": 63.15,
                                    "recall_num": 3.,
                                    "recall_denom": 8.,
                                    "recall": 37.5,
                                    "precision_num": 3.,
                                    "precision_denom": 4.,
                                    "precision": 75.,
                                    "f1": 50.,
                                    }
    expected_muc_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6,
                                                                                 "total_response_mentions_nb": 5, 
                                                                                 "strictly_correct_identified_mentions_nb": 4,  
                                                                                 "coreference_recall_num": 2., 
                                                                                 "coreference_recall_denom": 4.,
                                                                                 "coreference_recall": 50.,  
                                                                                 "coreference_precision_num": 2., 
                                                                                 "coreference_precision_denom": 3., 
                                                                                 "coreference_precision": 66.66, 
                                                                                 "coreference_f1": 57.14,
                                                                                  }),
                                        (("(doc2); part 001", "(doc2); part 001"), {"total_key_mentions_nb": 6,
                                                                                     "total_response_mentions_nb": 2, 
                                                                                     "strictly_correct_identified_mentions_nb": 2,  
                                                                                     "coreference_recall_num": 1., 
                                                                                     "coreference_recall_denom": 4.,
                                                                                     "coreference_recall": 25.,  
                                                                                     "coreference_precision_num": 1., 
                                                                                     "coreference_precision_denom": 1., 
                                                                                     "coreference_precision": 100., 
                                                                                     "coreference_f1": 40.,
                                                                                      }),
                                         ]
    expected_metrics_results_collection.append(("muc", (expected_muc_overall_results, expected_muc_results_per_document)))
    ### bcub
    expected_bcub_overall_results = {"overall_mention_identification_recall_num": 6.,
                                     "overall_mention_identification_recall_denom": 12.,
                                     "overall_mention_identification_recall": 50.,
                                     "overall_mention_identification_precision_num": 6.,
                                     "overall_mention_identification_precision_denom": 7.,
                                     "overall_mention_identification_precision": 85.71,
                                     "overall_mention_identification_f1": 63.15,
                                     "recall_num": 3.5,
                                     "recall_denom": 12.,
                                     "recall": 29.16,
                                     "precision_num": 5.5,
                                     "precision_denom": 7.,
                                     "precision": 78.57,
                                     "f1": 42.54,
                                     }
    expected_bcub_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6,
                                                                                     "total_response_mentions_nb": 5, 
                                                                                     "strictly_correct_identified_mentions_nb": 4,  
                                                                                     "coreference_recall_num": 2.5, 
                                                                                     "coreference_recall_denom": 6.,
                                                                                     "coreference_recall": 41.66,  
                                                                                     "coreference_precision_num": 3.5, 
                                                                                     "coreference_precision_denom": 5., 
                                                                                     "coreference_precision": 70., 
                                                                                     "coreference_f1": 52.23,
                                                                                      }),
                                          (("(doc2); part 001", "(doc2); part 001"), {"total_key_mentions_nb": 6,
                                                                                     "total_response_mentions_nb": 2, 
                                                                                     "strictly_correct_identified_mentions_nb": 2,  
                                                                                     "coreference_recall_num": 1., 
                                                                                     "coreference_recall_denom": 6.,
                                                                                     "coreference_recall": 16.66,  
                                                                                     "coreference_precision_num": 2., 
                                                                                     "coreference_precision_denom": 2., 
                                                                                     "coreference_precision": 100., 
                                                                                     "coreference_f1": 28.57,
                                                                                      }),
                                          ]
    expected_metrics_results_collection.append(("bcub", (expected_bcub_overall_results, expected_bcub_results_per_document)))
    ### ceafm
    expected_ceafm_overall_results = {"overall_mention_identification_recall_num": 6.,
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,
                                      "overall_mention_identification_precision_num": 6.,
                                      "overall_mention_identification_precision_denom": 7.,
                                      "overall_mention_identification_precision": 85.71,
                                      "overall_mention_identification_f1": 63.15,
                                      "recall_num": 5.,
                                      "recall_denom": 12.,
                                      "recall": 41.66,
                                      "precision_num": 5.,
                                      "precision_denom": 7.,
                                      "precision": 71.42,
                                      "f1": 52.63,
                                      }
    expected_ceafm_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6,
                                                                                     "total_response_mentions_nb": 5, 
                                                                                     "strictly_correct_identified_mentions_nb": 4,  
                                                                                     "coreference_recall_num": 3., 
                                                                                     "coreference_recall_denom": 6.,
                                                                                     "coreference_recall": 50.,  
                                                                                     "coreference_precision_num": 3., 
                                                                                     "coreference_precision_denom": 5., 
                                                                                     "coreference_precision": 60., 
                                                                                     "coreference_f1": 54.54,
                                                                                      }),
                                           (("(doc2); part 001", "(doc2); part 001"), {"total_key_mentions_nb": 6,
                                                                                     "total_response_mentions_nb": 2, 
                                                                                     "strictly_correct_identified_mentions_nb": 2,  
                                                                                     "coreference_recall_num": 2., 
                                                                                     "coreference_recall_denom": 6.,
                                                                                     "coreference_recall": 33.33,  
                                                                                     "coreference_precision_num": 2., 
                                                                                     "coreference_precision_denom": 2., 
                                                                                     "coreference_precision": 100., 
                                                                                     "coreference_f1": 50.,
                                                                                      }),
                                           ]
    expected_metrics_results_collection.append(("ceafm", (expected_ceafm_overall_results, expected_ceafm_results_per_document)))
    ### ceafe
    expected_ceafe_overall_results = {"overall_mention_identification_recall_num": 6.,
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,
                                      "overall_mention_identification_precision_num": 6.,
                                      "overall_mention_identification_precision_denom": 7.,
                                      "overall_mention_identification_precision": 85.71,
                                      "overall_mention_identification_f1": 63.15,
                                      "recall_num": 1.52380952380952,
                                      "recall_denom": 4.,
                                      "recall": 38.09,
                                      "precision_num": 1.52380952380952,
                                      "precision_denom": 3.,
                                      "precision": 50.79,
                                      "f1": 43.53,
                                      }
    expected_ceafe_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6,
                                                                                     "total_response_mentions_nb": 5, 
                                                                                     "strictly_correct_identified_mentions_nb": 4,  
                                                                                     "coreference_recall_num": 0.857142857142857, 
                                                                                     "coreference_recall_denom": 2.,
                                                                                     "coreference_recall": 42.85,  
                                                                                     "coreference_precision_num": 0.857142857142857, 
                                                                                     "coreference_precision_denom": 2., 
                                                                                     "coreference_precision": 42.85, 
                                                                                     "coreference_f1": 42.85,
                                                                                      }),
                                            (("(doc2); part 001", "(doc2); part 001"), {"total_key_mentions_nb": 6,
                                                                                         "total_response_mentions_nb": 2, 
                                                                                         "strictly_correct_identified_mentions_nb": 2,  
                                                                                         "coreference_recall_num": 0.666666666666667, 
                                                                                         "coreference_recall_denom": 2.,
                                                                                         "coreference_recall": 33.33,  
                                                                                         "coreference_precision_num": 0.666666666666667, 
                                                                                         "coreference_precision_denom": 1., 
                                                                                         "coreference_precision": 66.66, 
                                                                                         "coreference_f1": 44.44,
                                                                                          }),
                                           ]
    expected_metrics_results_collection.append(("ceafe", (expected_ceafe_overall_results, expected_ceafe_results_per_document)))
    ### blanc
    expected_blanc_overall_results = {"overall_mention_identification_recall_num": 6., 
                                      "overall_mention_identification_recall_denom": 12.,
                                      "overall_mention_identification_recall": 50.,  
                                      "overall_mention_identification_precision_num": 6., 
                                      "overall_mention_identification_precision_denom": 7., 
                                      "overall_mention_identification_precision": 85.71, 
                                      "overall_mention_identification_f1": 63.15,
                                   
                                      "coreference_recall_num": 4., 
                                      "coreference_recall_denom": 14.,
                                      "coreference_recall": 28.57,  
                                      "coreference_precision_num": 4., 
                                      "coreference_precision_denom": 5., 
                                      "coreference_precision": 80., 
                                      "coreference_f1": 42.1,
                                      
                                      "non_coreference_recall_num": 0., 
                                      "non_coreference_recall_denom": 16.,
                                      "non_coreference_recall": 0.,  
                                      "non_coreference_precision_num": 0., 
                                      "non_coreference_precision_denom": 6., 
                                      "non_coreference_precision": 0., 
                                      "non_coreference_f1": 0.,
                                   
                                      "blanc_recall_num": 0.142857142857143, 
                                      "blanc_recall_denom": 1.,
                                      "blanc_recall": 14.28,  
                                      "blanc_precision_num": 0.4, 
                                      "blanc_precision_denom": 1., 
                                      "blanc_precision": 40., 
                                      "blanc_f1": 21.05,
                                      }
    expected_blanc_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6,
                                                                                     "total_response_mentions_nb": 5, 
                                                                                     "strictly_correct_identified_mentions_nb": 4}),
                                           (("(doc2); part 001", "(doc2); part 001"), {"total_key_mentions_nb": 6,
                                                                                     "total_response_mentions_nb": 2, 
                                                                                     "strictly_correct_identified_mentions_nb": 2}),
                                           ]
    expected_metrics_results_collection.append(("blanc", (expected_blanc_overall_results, expected_blanc_results_per_document)))
    expected_metrics_results_collection = tuple(expected_metrics_results_collection)
    data.append(((args, kwargs), expected_metrics_results_collection))
     
    # One specific document
    ## ceafe
    string = """version: 8.0 ../lib/CorScorer.pm
(doc1); part 000:
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11) (20,20)
Entity 1: (6,9) (16,17)
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11)
Entity 1: (13,17) (20,20)
Total key mentions: 6
Total response mentions: 5
Strictly correct identified mentions: 4
Partially correct identified mentions: 0
No identified: 2
Invented: 1
Recall: (0.857142857142857 / 2) 42.85%    Precision: (0.857142857142857 / 2) 42.85%    F1: 42.85%
--------------------------------------------------------------------------

====== TOTALS =======
Identification of Mentions: Recall: (4 / 6) 66.66%    Precision: (4 / 5) 80%    F1: 72.72%
--------------------------------------------------------------------------
Coreference: Recall: (0.857142857142857 / 2) 42.85%    Precision: (0.857142857142857 / 2) 42.85%    F1: 42.85%
--------------------------------------------------------------------------
""" 
    line_iterator = iter(string.split("\n"))
    metric = "ceafe"
    which_document = "(doc1); part 000"
    args = (line_iterator, metric)
    kwargs = {"which_document": which_document}
    expected_ceafe_overall_results = {"overall_mention_identification_recall_num": 4.,
                                      "overall_mention_identification_recall_denom": 6.,
                                      "overall_mention_identification_recall": 66.66,
                                      "overall_mention_identification_precision_num": 4.,
                                      "overall_mention_identification_precision_denom": 5.,
                                      "overall_mention_identification_precision": 80.,
                                      "overall_mention_identification_f1": 72.72,
                                      "recall_num": 0.857142857142857,
                                      "recall_denom": 2.,
                                      "recall": 42.85,
                                      "precision_num": 0.857142857142857,
                                      "precision_denom": 2.,
                                      "precision": 42.85,
                                      "f1": 42.85,
                                      }
    expected_ceafe_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6,
                                                                                     "total_response_mentions_nb": 5, 
                                                                                     "strictly_correct_identified_mentions_nb": 4,  
                                                                                     "coreference_recall_num": 0.857142857142857, 
                                                                                     "coreference_recall_denom": 2.,
                                                                                     "coreference_recall": 42.85,  
                                                                                     "coreference_precision_num": 0.857142857142857, 
                                                                                     "coreference_precision_denom": 2., 
                                                                                     "coreference_precision": 42.85, 
                                                                                     "coreference_f1": 42.85,
                                                                                      }),
                                           ]
    expected_metrics_results_collection = [("ceafe", (expected_ceafe_overall_results, expected_ceafe_results_per_document)),
                                           ]
    expected_metrics_results_collection = tuple(expected_metrics_results_collection)
    data.append(((args, kwargs), expected_metrics_results_collection))
    
    ## blanc
    string = """version: 8.0 ../lib/CorScorer.pm
(doc1); part 000:
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11) (20,20)
Entity 1: (6,9) (16,17)
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11)
Entity 1: (13,17) (20,20)
Total key mentions: 6
Total response mentions: 5
Strictly correct identified mentions: 4
Partially correct identified mentions: 0
No identified: 2
Invented: 1

====== TOTALS =======
Identification of Mentions: Recall: (4 / 6) 66.66%    Precision: (4 / 5) 80%    F1: 72.72%
--------------------------------------------------------------------------

Coreference:
Coreference links: Recall: (3 / 7) 42.85%    Precision: (3 / 4) 75%    F1: 54.54%
--------------------------------------------------------------------------
Non-coreference links: Recall: (0 / 8) 0%    Precision: (0 / 6) 0%    F1: 0%
--------------------------------------------------------------------------
BLANC: Recall: (0.214285714285714 / 1) 21.42%    Precision: (0.375 / 1) 37.5%    F1: 27.27%
--------------------------------------------------------------------------
"""
    line_iterator = iter(string.split("\n"))
    metric = "blanc"
    which_document = "(doc1); part 000"
    args = (line_iterator, metric)
    kwargs = {"which_document": which_document}
    expected_blanc_overall_results = {"overall_mention_identification_recall_num": 4.,
                                      "overall_mention_identification_recall_denom": 6.,
                                      "overall_mention_identification_recall": 66.66,
                                      "overall_mention_identification_precision_num": 4.,
                                      "overall_mention_identification_precision_denom": 5.,
                                      "overall_mention_identification_precision": 80.,
                                      "overall_mention_identification_f1": 72.72,
                                      
                                      "coreference_recall_num": 3., 
                                      "coreference_recall_denom": 7.,
                                      "coreference_recall": 42.85,  
                                      "coreference_precision_num": 3., 
                                      "coreference_precision_denom": 4., 
                                      "coreference_precision": 75., 
                                      "coreference_f1": 54.54,
                                      
                                      "non_coreference_recall_num": 0., 
                                      "non_coreference_recall_denom": 8.,
                                      "non_coreference_recall": 0.,  
                                      "non_coreference_precision_num": 0., 
                                      "non_coreference_precision_denom": 6., 
                                      "non_coreference_precision": 0., 
                                      "non_coreference_f1": 0.,
                                   
                                      "blanc_recall_num": 0.214285714285714, 
                                      "blanc_recall_denom": 1.,
                                      "blanc_recall": 21.42,  
                                      "blanc_precision_num": 0.375, 
                                      "blanc_precision_denom": 1., 
                                      "blanc_precision": 37.5, 
                                      "blanc_f1": 27.27,
                                      }
    expected_blanc_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6,
                                                                                     "total_response_mentions_nb": 5, 
                                                                                     "strictly_correct_identified_mentions_nb": 4,
                                                                                     
                                                                                     "coreference_recall_num": 3., 
                                                                                     "coreference_recall_denom": 7.,
                                                                                     "coreference_recall": 42.85,  
                                                                                     "coreference_precision_num": 3., 
                                                                                     "coreference_precision_denom": 4., 
                                                                                     "coreference_precision": 75., 
                                                                                     "coreference_f1": 54.54,
                                                                                     
                                                                                     "non_coreference_recall_num": 0., 
                                                                                     "non_coreference_recall_denom": 8.,
                                                                                     "non_coreference_recall": 0.,  
                                                                                     "non_coreference_precision_num": 0., 
                                                                                     "non_coreference_precision_denom": 6., 
                                                                                     "non_coreference_precision": 0., 
                                                                                     "non_coreference_f1": 0.,
                                                                                     
                                                                                     "blanc_recall_num": 0.214285714285714, 
                                                                                     "blanc_recall_denom": 1.,
                                                                                     "blanc_recall": 21.42,  
                                                                                     "blanc_precision_num": 0.375, 
                                                                                     "blanc_precision_denom": 1., 
                                                                                     "blanc_precision": 37.5, 
                                                                                     "blanc_f1": 27.27,
                                                                                     }),
                                           ]
    expected_metrics_results_collection = [("blanc", (expected_blanc_overall_results, expected_blanc_results_per_document)),
                                           ]
    expected_metrics_results_collection = tuple(expected_metrics_results_collection)
    data.append(((args, kwargs), expected_metrics_results_collection))
    
    ## all
    string = """version: 8.0 ../lib/CorScorer.pm

METRIC muc:
(doc1); part 000:
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11) (20,20)
Entity 1: (6,9) (16,17)
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11)
Entity 1: (13,17) (20,20)
Total key mentions: 6
Total response mentions: 5
Strictly correct identified mentions: 4
Partially correct identified mentions: 0
No identified: 2
Invented: 1
Recall: (2 / 4) 50%    Precision: (2 / 3) 66.66%    F1: 57.14%
--------------------------------------------------------------------------

====== TOTALS =======
Identification of Mentions: Recall: (4 / 6) 66.66%    Precision: (4 / 5) 80%    F1: 72.72%
--------------------------------------------------------------------------
Coreference: Recall: (2 / 4) 50%    Precision: (2 / 3) 66.66%    F1: 57.14%
--------------------------------------------------------------------------

METRIC bcub:
(doc1); part 000:
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11) (20,20)
Entity 1: (6,9) (16,17)
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11)
Entity 1: (13,17) (20,20)
Total key mentions: 6
Total response mentions: 5
Strictly correct identified mentions: 4
Partially correct identified mentions: 0
No identified: 2
Invented: 1
Recall: (2.5 / 6) 41.66%    Precision: (3.5 / 5) 70%    F1: 52.23%
--------------------------------------------------------------------------

====== TOTALS =======
Identification of Mentions: Recall: (4 / 6) 66.66%    Precision: (4 / 5) 80%    F1: 72.72%
--------------------------------------------------------------------------
Coreference: Recall: (2.5 / 6) 41.66%    Precision: (3.5 / 5) 70%    F1: 52.23%
--------------------------------------------------------------------------

METRIC ceafm:
(doc1); part 000:
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11) (20,20)
Entity 1: (6,9) (16,17)
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11)
Entity 1: (13,17) (20,20)
Total key mentions: 6
Total response mentions: 5
Strictly correct identified mentions: 4
Partially correct identified mentions: 0
No identified: 2
Invented: 1
Recall: (3 / 6) 50%    Precision: (3 / 5) 60%    F1: 54.54%
--------------------------------------------------------------------------

====== TOTALS =======
Identification of Mentions: Recall: (4 / 6) 66.66%    Precision: (4 / 5) 80%    F1: 72.72%
--------------------------------------------------------------------------
Coreference: Recall: (3 / 6) 50%    Precision: (3 / 5) 60%    F1: 54.54%
--------------------------------------------------------------------------

METRIC ceafe:
(doc1); part 000:
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11) (20,20)
Entity 1: (6,9) (16,17)
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11)
Entity 1: (13,17) (20,20)
Total key mentions: 6
Total response mentions: 5
Strictly correct identified mentions: 4
Partially correct identified mentions: 0
No identified: 2
Invented: 1
Recall: (0.857142857142857 / 2) 42.85%    Precision: (0.857142857142857 / 2) 42.85%    F1: 42.85%
--------------------------------------------------------------------------

====== TOTALS =======
Identification of Mentions: Recall: (4 / 6) 66.66%    Precision: (4 / 5) 80%    F1: 72.72%
--------------------------------------------------------------------------
Coreference: Recall: (0.857142857142857 / 2) 42.85%    Precision: (0.857142857142857 / 2) 42.85%    F1: 42.85%
--------------------------------------------------------------------------

METRIC blanc:
(doc1); part 000:
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11) (20,20)
Entity 1: (6,9) (16,17)
====> (doc1); part 000:
File (doc1); part 000:
Entity 0: (0,1) (3,9) (11,11)
Entity 1: (13,17) (20,20)
Total key mentions: 6
Total response mentions: 5
Strictly correct identified mentions: 4
Partially correct identified mentions: 0
No identified: 2
Invented: 1

====== TOTALS =======
Identification of Mentions: Recall: (4 / 6) 66.66%    Precision: (4 / 5) 80%    F1: 72.72%
--------------------------------------------------------------------------

Coreference:
Coreference links: Recall: (3 / 7) 42.85%    Precision: (3 / 4) 75%    F1: 54.54%
--------------------------------------------------------------------------
Non-coreference links: Recall: (0 / 8) 0%    Precision: (0 / 6) 0%    F1: 0%
--------------------------------------------------------------------------
BLANC: Recall: (0.214285714285714 / 1) 21.42%    Precision: (0.375 / 1) 37.5%    F1: 27.27%
--------------------------------------------------------------------------
"""
    line_iterator = iter(string.split("\n"))
    metric = "all"
    which_document = "(doc1); part 000"
    args = (line_iterator, metric)
    kwargs = {"which_document": which_document}
    
    expected_metrics_results_collection = []
    ### muc
    expected_muc_overall_results = {"overall_mention_identification_recall_num": 4.,
                                      "overall_mention_identification_recall_denom": 6.,
                                      "overall_mention_identification_recall": 66.66,
                                      "overall_mention_identification_precision_num": 4.,
                                      "overall_mention_identification_precision_denom": 5.,
                                      "overall_mention_identification_precision": 80.,
                                      "overall_mention_identification_f1": 72.72,
                                      "recall_num": 2.,
                                      "recall_denom": 4.,
                                      "recall": 50.,
                                      "precision_num": 2.,
                                      "precision_denom": 3.,
                                      "precision": 66.66,
                                      "f1": 57.14,
                                      }
    expected_muc_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6,
                                                                                   "total_response_mentions_nb": 5, 
                                                                                   "strictly_correct_identified_mentions_nb": 4,  
                                                                                   "coreference_recall_num": 2., 
                                                                                   "coreference_recall_denom": 4.,
                                                                                   "coreference_recall": 50.,  
                                                                                   "coreference_precision_num": 2., 
                                                                                   "coreference_precision_denom": 3., 
                                                                                   "coreference_precision": 66.66, 
                                                                                   "coreference_f1": 57.14,
                                                                                   }),
                                         ]
    expected_metrics_results_collection.append(("muc", (expected_muc_overall_results, expected_muc_results_per_document)))
    ### bcub
    expected_bcub_overall_results = {"overall_mention_identification_recall_num": 4.,
                                      "overall_mention_identification_recall_denom": 6.,
                                      "overall_mention_identification_recall": 66.66,
                                      "overall_mention_identification_precision_num": 4.,
                                      "overall_mention_identification_precision_denom": 5.,
                                      "overall_mention_identification_precision": 80.,
                                      "overall_mention_identification_f1": 72.72,
                                      "recall_num": 2.5,
                                      "recall_denom": 6.,
                                      "recall": 41.66,
                                      "precision_num": 3.5,
                                      "precision_denom": 5.,
                                      "precision": 70.,
                                      "f1": 52.23,
                                      }
    expected_bcub_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6,
                                                                                     "total_response_mentions_nb": 5, 
                                                                                     "strictly_correct_identified_mentions_nb": 4,  
                                                                                     "coreference_recall_num": 2.5, 
                                                                                     "coreference_recall_denom": 6.,
                                                                                     "coreference_recall": 41.66,  
                                                                                     "coreference_precision_num": 3.5, 
                                                                                     "coreference_precision_denom": 5., 
                                                                                     "coreference_precision": 70., 
                                                                                     "coreference_f1": 52.23,
                                                                                      }),
                                          ]
    expected_metrics_results_collection.append(("bcub", (expected_bcub_overall_results, expected_bcub_results_per_document)))
    ### ceafm
    expected_ceafm_overall_results = {"overall_mention_identification_recall_num": 4.,
                                      "overall_mention_identification_recall_denom": 6.,
                                      "overall_mention_identification_recall": 66.66,
                                      "overall_mention_identification_precision_num": 4.,
                                      "overall_mention_identification_precision_denom": 5.,
                                      "overall_mention_identification_precision": 80.,
                                      "overall_mention_identification_f1": 72.72,
                                      "recall_num": 3.,
                                      "recall_denom": 6.,
                                      "recall": 50.,
                                      "precision_num": 3.,
                                      "precision_denom": 5.,
                                      "precision": 60.,
                                      "f1": 54.54,
                                      }
    expected_ceafm_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6,
                                                                                     "total_response_mentions_nb": 5, 
                                                                                     "strictly_correct_identified_mentions_nb": 4,  
                                                                                     "coreference_recall_num": 3., 
                                                                                     "coreference_recall_denom": 6.,
                                                                                     "coreference_recall": 50.,  
                                                                                     "coreference_precision_num": 3., 
                                                                                     "coreference_precision_denom": 5., 
                                                                                     "coreference_precision": 60., 
                                                                                     "coreference_f1": 54.54,
                                                                                      }),
                                           ]
    expected_metrics_results_collection.append(("ceafm", (expected_ceafm_overall_results, expected_ceafm_results_per_document)))
    ### ceafe
    expected_ceafe_overall_results = {"overall_mention_identification_recall_num": 4.,
                                      "overall_mention_identification_recall_denom": 6.,
                                      "overall_mention_identification_recall": 66.66,
                                      "overall_mention_identification_precision_num": 4.,
                                      "overall_mention_identification_precision_denom": 5.,
                                      "overall_mention_identification_precision": 80.,
                                      "overall_mention_identification_f1": 72.72,
                                      "recall_num": 0.857142857142857,
                                      "recall_denom": 2.,
                                      "recall": 42.85,
                                      "precision_num": 0.857142857142857,
                                      "precision_denom": 2.,
                                      "precision": 42.85,
                                      "f1": 42.85,
                                      }
    expected_ceafe_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6,
                                                                                     "total_response_mentions_nb": 5, 
                                                                                     "strictly_correct_identified_mentions_nb": 4,  
                                                                                     "coreference_recall_num": 0.857142857142857, 
                                                                                     "coreference_recall_denom": 2.,
                                                                                     "coreference_recall": 42.85,  
                                                                                     "coreference_precision_num": 0.857142857142857, 
                                                                                     "coreference_precision_denom": 2., 
                                                                                     "coreference_precision": 42.85, 
                                                                                     "coreference_f1": 42.85,
                                                                                      }),
                                           ]
    expected_metrics_results_collection.append(("ceafe", (expected_ceafe_overall_results, expected_ceafe_results_per_document)))
    ### blanc
    expected_blanc_overall_results = {"overall_mention_identification_recall_num": 4.,
                                      "overall_mention_identification_recall_denom": 6.,
                                      "overall_mention_identification_recall": 66.66,
                                      "overall_mention_identification_precision_num": 4.,
                                      "overall_mention_identification_precision_denom": 5.,
                                      "overall_mention_identification_precision": 80.,
                                      "overall_mention_identification_f1": 72.72,
                                      
                                      "coreference_recall_num": 3., 
                                      "coreference_recall_denom": 7.,
                                      "coreference_recall": 42.85,  
                                      "coreference_precision_num": 3., 
                                      "coreference_precision_denom": 4., 
                                      "coreference_precision": 75., 
                                      "coreference_f1": 54.54,
                                      
                                      "non_coreference_recall_num": 0., 
                                      "non_coreference_recall_denom": 8.,
                                      "non_coreference_recall": 0.,  
                                      "non_coreference_precision_num": 0., 
                                      "non_coreference_precision_denom": 6., 
                                      "non_coreference_precision": 0., 
                                      "non_coreference_f1": 0.,
                                   
                                      "blanc_recall_num": 0.214285714285714, 
                                      "blanc_recall_denom": 1.,
                                      "blanc_recall": 21.42,  
                                      "blanc_precision_num": 0.375, 
                                      "blanc_precision_denom": 1., 
                                      "blanc_precision": 37.5, 
                                      "blanc_f1": 27.27,
                                      }
    expected_blanc_results_per_document = [(("(doc1); part 000", "(doc1); part 000"), {"total_key_mentions_nb": 6,
                                                                                     "total_response_mentions_nb": 5, 
                                                                                     "strictly_correct_identified_mentions_nb": 4,
                                                                                     
                                                                                     "coreference_recall_num": 3., 
                                                                                     "coreference_recall_denom": 7.,
                                                                                     "coreference_recall": 42.85,  
                                                                                     "coreference_precision_num": 3., 
                                                                                     "coreference_precision_denom": 4., 
                                                                                     "coreference_precision": 75., 
                                                                                     "coreference_f1": 54.54,
                                                                                     
                                                                                     "non_coreference_recall_num": 0., 
                                                                                     "non_coreference_recall_denom": 8.,
                                                                                     "non_coreference_recall": 0.,  
                                                                                     "non_coreference_precision_num": 0., 
                                                                                     "non_coreference_precision_denom": 6., 
                                                                                     "non_coreference_precision": 0., 
                                                                                     "non_coreference_f1": 0.,
                                                                                     
                                                                                     "blanc_recall_num": 0.214285714285714, 
                                                                                     "blanc_recall_denom": 1.,
                                                                                     "blanc_recall": 21.42,  
                                                                                     "blanc_precision_num": 0.375, 
                                                                                     "blanc_precision_denom": 1., 
                                                                                     "blanc_precision": 37.5, 
                                                                                     "blanc_f1": 27.27,
                                                                                     }),
                                           ]
    expected_metrics_results_collection.append(("blanc", (expected_blanc_overall_results, expected_blanc_results_per_document)))
    expected_metrics_results_collection = tuple(expected_metrics_results_collection)
    data.append(((args, kwargs), expected_metrics_results_collection))
    
    
    # One specific document that does not exist
    ## ceafe
    string = """version: 8.0 ../lib/CorScorer.pm
test:
====> :
File :
====> :
File :
Total key mentions: 0
Total response mentions: 0
Strictly correct identified mentions: 0
Partially correct identified mentions: 0
No identified: 0
Invented: 0
Recall: (0 / 0) 0%    Precision: (0 / 0) 0%    F1: 0%
--------------------------------------------------------------------------

====== TOTALS =======
Identification of Mentions: Recall: (0 / 0) 0%    Precision: (0 / 0) 0%    F1: 0%
--------------------------------------------------------------------------
Coreference: Recall: (0 / 0) 0%    Precision: (0 / 0) 0%    F1: 0%
--------------------------------------------------------------------------
"""
    line_iterator = iter(string.split("\n"))
    metric = "ceafe"
    which_document = "(doc1); part 000"
    args = (line_iterator, metric)
    kwargs = {"which_document": which_document}
    expected_ceafe_overall_results = {"overall_mention_identification_recall_num": 0.,
                                      "overall_mention_identification_recall_denom": 0.,
                                      "overall_mention_identification_recall": 0.,
                                      "overall_mention_identification_precision_num": 0.,
                                      "overall_mention_identification_precision_denom": 0.,
                                      "overall_mention_identification_precision": 0.,
                                      "overall_mention_identification_f1": 0.,
                                      "recall_num": 0.,
                                      "recall_denom": 0.,
                                      "recall": 0.,
                                      "precision_num": 0.,
                                      "precision_denom": 0.,
                                      "precision": 0.,
                                      "f1": 0.,
                                      }
    expected_ceafe_results_per_document = [(("", ""), {"total_key_mentions_nb": 0,
                                                     "total_response_mentions_nb": 0, 
                                                     "strictly_correct_identified_mentions_nb": 0,  
                                                     "coreference_recall_num": 0., 
                                                     "coreference_recall_denom": 0.,
                                                     "coreference_recall": 0.,  
                                                     "coreference_precision_num": 0., 
                                                     "coreference_precision_denom": 0., 
                                                     "coreference_precision": 0., 
                                                     "coreference_f1": 0.,
                                                      }),
                                           ]
    expected_metrics_results_collection = [("ceafe", (expected_ceafe_overall_results, expected_ceafe_results_per_document)),
                                           ]
    expected_metrics_results_collection = tuple(expected_metrics_results_collection)
    data.append(((args, kwargs), expected_metrics_results_collection))
    
    ## blanc
    string = """version: 8.0 ../lib/CorScorer.pm
test:
====> :
File :
====> :
File :
Total key mentions: 0
Total response mentions: 0
Strictly correct identified mentions: 0
Partially correct identified mentions: 0
No identified: 0
Invented: 0

====== TOTALS =======
Identification of Mentions: Recall: (0 / 0) 0%    Precision: (0 / 0) 0%    F1: 0%
--------------------------------------------------------------------------

Coreference:
Coreference links: Recall: (0 / 0) 0%    Precision: (0 / 0) 0%    F1: 0%
--------------------------------------------------------------------------
Non-coreference links: Recall: (0 / 0) 0%    Precision: (0 / 0) 0%    F1: 0%
--------------------------------------------------------------------------
BLANC: Recall: (0 / 1) 0%    Precision: (0 / 1) 0%    F1: 0%
--------------------------------------------------------------------------
"""
    line_iterator = iter(string.split("\n"))
    metric = "blanc"
    which_document = "(doc1); part 000"
    args = (line_iterator, metric)
    kwargs = {"which_document": which_document}
    expected_blanc_overall_results = {"overall_mention_identification_recall_num": 0.,
                                      "overall_mention_identification_recall_denom": 0.,
                                      "overall_mention_identification_recall": 0.,
                                      "overall_mention_identification_precision_num": 0.,
                                      "overall_mention_identification_precision_denom": 0.,
                                      "overall_mention_identification_precision": 0.,
                                      "overall_mention_identification_f1": 0.,
                                      
                                      "coreference_recall_num": 0., 
                                      "coreference_recall_denom": 0.,
                                      "coreference_recall": 0.,  
                                      "coreference_precision_num": 0., 
                                      "coreference_precision_denom": 0., 
                                      "coreference_precision": 0., 
                                      "coreference_f1": 0.,
                                      
                                      "non_coreference_recall_num": 0., 
                                      "non_coreference_recall_denom": 0.,
                                      "non_coreference_recall": 0.,  
                                      "non_coreference_precision_num": 0., 
                                      "non_coreference_precision_denom": 0., 
                                      "non_coreference_precision": 0., 
                                      "non_coreference_f1": 0.,
                                   
                                      "blanc_recall_num": 0., 
                                      "blanc_recall_denom": 1.,
                                      "blanc_recall": 0.,  
                                      "blanc_precision_num": 0., 
                                      "blanc_precision_denom": 1., 
                                      "blanc_precision": 0., 
                                      "blanc_f1": 0.,
                                      }
    expected_blanc_results_per_document = [(("", ""), {"total_key_mentions_nb": 0,
                                                     "total_response_mentions_nb": 0, 
                                                     "strictly_correct_identified_mentions_nb": 0,
                                                     
                                                     "coreference_recall_num": 0., 
                                                     "coreference_recall_denom": 0.,
                                                     "coreference_recall": 0.,  
                                                     "coreference_precision_num": 0., 
                                                     "coreference_precision_denom": 0., 
                                                     "coreference_precision": 0., 
                                                     "coreference_f1": 0.,
                                                     
                                                     "non_coreference_recall_num": 0., 
                                                     "non_coreference_recall_denom": 0.,
                                                     "non_coreference_recall": 0.,  
                                                     "non_coreference_precision_num": 0., 
                                                     "non_coreference_precision_denom": 0., 
                                                     "non_coreference_precision": 0., 
                                                     "non_coreference_f1": 0.,
                                                     
                                                     "blanc_recall_num": 0., 
                                                     "blanc_recall_denom": 1.,
                                                     "blanc_recall": 0.,  
                                                     "blanc_precision_num": 0., 
                                                     "blanc_precision_denom": 1., 
                                                     "blanc_precision": 0., 
                                                     "blanc_f1": 0.,
                                                      }),
                                           ]
    expected_metrics_results_collection = [("blanc", (expected_blanc_overall_results, expected_blanc_results_per_document)),
                                           ]
    expected_metrics_results_collection = tuple(expected_metrics_results_collection)
    data.append(((args, kwargs), expected_metrics_results_collection))
    
    ## all
    string = """version: 8.0 ../lib/CorScorer.pm

METRIC muc:
test:
====> :
File :
====> :
File :
Total key mentions: 0
Total response mentions: 0
Strictly correct identified mentions: 0
Partially correct identified mentions: 0
No identified: 0
Invented: 0
Recall: (0 / 0) 0%    Precision: (0 / 0) 0%    F1: 0%
--------------------------------------------------------------------------

====== TOTALS =======
Identification of Mentions: Recall: (0 / 0) 0%    Precision: (0 / 0) 0%    F1: 0%
--------------------------------------------------------------------------
Coreference: Recall: (0 / 0) 0%    Precision: (0 / 0) 0%    F1: 0%
--------------------------------------------------------------------------

METRIC bcub:
test:
====> :
File :
====> :
File :
Total key mentions: 0
Total response mentions: 0
Strictly correct identified mentions: 0
Partially correct identified mentions: 0
No identified: 0
Invented: 0
Recall: (0 / 0) 0%    Precision: (0 / 0) 0%    F1: 0%
--------------------------------------------------------------------------

====== TOTALS =======
Identification of Mentions: Recall: (0 / 0) 0%    Precision: (0 / 0) 0%    F1: 0%
--------------------------------------------------------------------------
Coreference: Recall: (0 / 0) 0%    Precision: (0 / 0) 0%    F1: 0%
--------------------------------------------------------------------------

METRIC ceafm:
test:
====> :
File :
====> :
File :
Total key mentions: 0
Total response mentions: 0
Strictly correct identified mentions: 0
Partially correct identified mentions: 0
No identified: 0
Invented: 0
Recall: (0 / 0) 0%    Precision: (0 / 0) 0%    F1: 0%
--------------------------------------------------------------------------

====== TOTALS =======
Identification of Mentions: Recall: (0 / 0) 0%    Precision: (0 / 0) 0%    F1: 0%
--------------------------------------------------------------------------
Coreference: Recall: (0 / 0) 0%    Precision: (0 / 0) 0%    F1: 0%
--------------------------------------------------------------------------

METRIC ceafe:
test:
====> :
File :
====> :
File :
Total key mentions: 0
Total response mentions: 0
Strictly correct identified mentions: 0
Partially correct identified mentions: 0
No identified: 0
Invented: 0
Recall: (0 / 0) 0%    Precision: (0 / 0) 0%    F1: 0%
--------------------------------------------------------------------------

====== TOTALS =======
Identification of Mentions: Recall: (0 / 0) 0%    Precision: (0 / 0) 0%    F1: 0%
--------------------------------------------------------------------------
Coreference: Recall: (0 / 0) 0%    Precision: (0 / 0) 0%    F1: 0%
--------------------------------------------------------------------------

METRIC blanc:
test:
====> :
File :
====> :
File :
Total key mentions: 0
Total response mentions: 0
Strictly correct identified mentions: 0
Partially correct identified mentions: 0
No identified: 0
Invented: 0

====== TOTALS =======
Identification of Mentions: Recall: (0 / 0) 0%    Precision: (0 / 0) 0%    F1: 0%
--------------------------------------------------------------------------

Coreference:
Coreference links: Recall: (0 / 0) 0%    Precision: (0 / 0) 0%    F1: 0%
--------------------------------------------------------------------------
Non-coreference links: Recall: (0 / 0) 0%    Precision: (0 / 0) 0%    F1: 0%
--------------------------------------------------------------------------
BLANC: Recall: (0 / 1) 0%    Precision: (0 / 1) 0%    F1: 0%
--------------------------------------------------------------------------
"""
    line_iterator = iter(string.split("\n"))
    metric = "all"
    which_document = "(doc1); part 000"
    args = (line_iterator, metric)
    kwargs = {"which_document": which_document}
    
    expected_metrics_results_collection = []
    ### muc
    expected_muc_overall_results = {"overall_mention_identification_recall_num": 0.,
                                      "overall_mention_identification_recall_denom": 0.,
                                      "overall_mention_identification_recall": 0.,
                                      "overall_mention_identification_precision_num": 0.,
                                      "overall_mention_identification_precision_denom": 0.,
                                      "overall_mention_identification_precision": 0.,
                                      "overall_mention_identification_f1": 0.,
                                      "recall_num": 0.,
                                      "recall_denom": 0.,
                                      "recall": 0.,
                                      "precision_num": 0.,
                                      "precision_denom": 0.,
                                      "precision": 0.,
                                      "f1": 0.,
                                      }
    expected_muc_results_per_document = [(("", ""), {"total_key_mentions_nb": 0,
                                                     "total_response_mentions_nb": 0, 
                                                     "strictly_correct_identified_mentions_nb": 0,  
                                                     "coreference_recall_num": 0., 
                                                     "coreference_recall_denom": 0.,
                                                     "coreference_recall": 0.,  
                                                     "coreference_precision_num": 0., 
                                                     "coreference_precision_denom": 0., 
                                                     "coreference_precision": 0., 
                                                     "coreference_f1": 0.,
                                                      }),
                                         ]
    expected_metrics_results_collection.append(("muc", (expected_muc_overall_results, expected_muc_results_per_document)))
    ### bcub
    expected_bcub_overall_results = {"overall_mention_identification_recall_num": 0.,
                                      "overall_mention_identification_recall_denom": 0.,
                                      "overall_mention_identification_recall": 0.,
                                      "overall_mention_identification_precision_num": 0.,
                                      "overall_mention_identification_precision_denom": 0.,
                                      "overall_mention_identification_precision": 0.,
                                      "overall_mention_identification_f1": 0.,
                                      "recall_num": 0.,
                                      "recall_denom": 0.,
                                      "recall": 0.,
                                      "precision_num": 0.,
                                      "precision_denom": 0.,
                                      "precision": 0.,
                                      "f1": 0.,
                                      }
    expected_bcub_results_per_document = [(("", ""), {"total_key_mentions_nb": 0,
                                                     "total_response_mentions_nb": 0, 
                                                     "strictly_correct_identified_mentions_nb": 0,  
                                                     "coreference_recall_num": 0., 
                                                     "coreference_recall_denom": 0.,
                                                     "coreference_recall": 0.,  
                                                     "coreference_precision_num": 0., 
                                                     "coreference_precision_denom": 0., 
                                                     "coreference_precision": 0., 
                                                     "coreference_f1": 0.,
                                                      }),
                                          ]
    expected_metrics_results_collection.append(("bcub", (expected_bcub_overall_results, expected_bcub_results_per_document)))
    ### ceafm
    expected_ceafm_overall_results = {"overall_mention_identification_recall_num": 0.,
                                      "overall_mention_identification_recall_denom": 0.,
                                      "overall_mention_identification_recall": 0.,
                                      "overall_mention_identification_precision_num": 0.,
                                      "overall_mention_identification_precision_denom": 0.,
                                      "overall_mention_identification_precision": 0.,
                                      "overall_mention_identification_f1": 0.,
                                      "recall_num": 0.,
                                      "recall_denom": 0.,
                                      "recall": 0.,
                                      "precision_num": 0.,
                                      "precision_denom": 0.,
                                      "precision": 0.,
                                      "f1": 0.,
                                      }
    expected_ceafm_results_per_document = [(("", ""), {"total_key_mentions_nb": 0,
                                                                             "total_response_mentions_nb": 0, 
                                                                             "strictly_correct_identified_mentions_nb": 0,  
                                                                             "coreference_recall_num": 0., 
                                                                             "coreference_recall_denom": 0.,
                                                                             "coreference_recall": 0.,  
                                                                             "coreference_precision_num": 0., 
                                                                             "coreference_precision_denom": 0., 
                                                                             "coreference_precision": 0., 
                                                                             "coreference_f1": 0.,
                                                                              }),
                                           ]
    expected_metrics_results_collection.append(("ceafm", (expected_ceafm_overall_results, expected_ceafm_results_per_document)))
    ### ceafe
    expected_ceafe_overall_results = {"overall_mention_identification_recall_num": 0.,
                                      "overall_mention_identification_recall_denom": 0.,
                                      "overall_mention_identification_recall": 0.,
                                      "overall_mention_identification_precision_num": 0.,
                                      "overall_mention_identification_precision_denom": 0.,
                                      "overall_mention_identification_precision": 0.,
                                      "overall_mention_identification_f1": 0.,
                                      "recall_num": 0.,
                                      "recall_denom": 0.,
                                      "recall": 0.,
                                      "precision_num": 0.,
                                      "precision_denom": 0.,
                                      "precision": 0.,
                                      "f1": 0.,
                                      }
    expected_ceafe_results_per_document = [(("", ""), 
                                           {"total_key_mentions_nb": 0,
                                             "total_response_mentions_nb": 0, 
                                             "strictly_correct_identified_mentions_nb": 0,  
                                             "coreference_recall_num": 0., 
                                             "coreference_recall_denom": 0.,
                                             "coreference_recall": 0.,  
                                             "coreference_precision_num": 0., 
                                             "coreference_precision_denom": 0., 
                                             "coreference_precision": 0., 
                                             "coreference_f1": 0.,
                                             }),
                                           ]
    expected_metrics_results_collection.append(("ceafe", (expected_ceafe_overall_results, expected_ceafe_results_per_document)))
    ### blanc
    expected_blanc_overall_results = {"overall_mention_identification_recall_num": 0.,
                                      "overall_mention_identification_recall_denom": 0.,
                                      "overall_mention_identification_recall": 0.,
                                      "overall_mention_identification_precision_num": 0.,
                                      "overall_mention_identification_precision_denom": 0.,
                                      "overall_mention_identification_precision": 0.,
                                      "overall_mention_identification_f1": 0.,
                                      
                                      "coreference_recall_num": 0., 
                                      "coreference_recall_denom": 0.,
                                      "coreference_recall": 0.,  
                                      "coreference_precision_num": 0., 
                                      "coreference_precision_denom": 0., 
                                      "coreference_precision": 0., 
                                      "coreference_f1": 0.,
                                      
                                      "non_coreference_recall_num": 0., 
                                      "non_coreference_recall_denom": 0.,
                                      "non_coreference_recall": 0.,  
                                      "non_coreference_precision_num": 0., 
                                      "non_coreference_precision_denom": 0., 
                                      "non_coreference_precision": 0., 
                                      "non_coreference_f1": 0.,
                                   
                                      "blanc_recall_num": 0., 
                                      "blanc_recall_denom": 1.,
                                      "blanc_recall": 0.,  
                                      "blanc_precision_num": 0., 
                                      "blanc_precision_denom": 1., 
                                      "blanc_precision": 0., 
                                      "blanc_f1": 0.,
                                      }
    expected_blanc_results_per_document = [(("", ""), 
                                           {"total_key_mentions_nb": 0,
                                             "total_response_mentions_nb": 0, 
                                             "strictly_correct_identified_mentions_nb": 0,
                                             
                                             "coreference_recall_num": 0., 
                                             "coreference_recall_denom": 0.,
                                             "coreference_recall": 0.,  
                                             "coreference_precision_num": 0., 
                                             "coreference_precision_denom": 0., 
                                             "coreference_precision": 0., 
                                             "coreference_f1": 0.,
                                             
                                             "non_coreference_recall_num": 0., 
                                             "non_coreference_recall_denom": 0.,
                                             "non_coreference_recall": 0.,  
                                             "non_coreference_precision_num": 0., 
                                             "non_coreference_precision_denom": 0., 
                                             "non_coreference_precision": 0., 
                                             "non_coreference_f1": 0.,
                                             
                                             "blanc_recall_num": 0., 
                                             "blanc_recall_denom": 1.,
                                             "blanc_recall": 0.,  
                                             "blanc_precision_num": 0., 
                                             "blanc_precision_denom": 1., 
                                             "blanc_precision": 0., 
                                             "blanc_f1": 0.,
                                              }),
                                           ]
    expected_metrics_results_collection.append(("blanc", (expected_blanc_overall_results, expected_blanc_results_per_document)))
    expected_metrics_results_collection = tuple(expected_metrics_results_collection)
    data.append(((args, kwargs), expected_metrics_results_collection))
    
    return data



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()