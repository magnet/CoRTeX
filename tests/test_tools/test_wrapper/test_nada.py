# -*- coding: utf-8 -*-

import unittest
import os
import tempfile
import pytest

from cortex.parameters import ENCODING
from cortex.tools.wrapper.nada import NadaWrapper

@pytest.mark.need_nada
class TestNadaWrapper(unittest.TestCase):
    
    #@unittest.skip
    def test__apply_nada_exec_on_file(self):
        # Test parameters
        raw_text, expected_result = get__apply_nada_exec_test_data()
        input_file_name = "glop.txt"
        output_file_name = "glip.txt"
        err_file_name = "glap.txt"
        nada_wrapper = NadaWrapper()
        
        # Actual result
        with tempfile.TemporaryDirectory() as input_folder_path:
            input_file_path = os.path.join(input_folder_path, input_file_name)
            output_file_path = os.path.join(input_folder_path, output_file_name)
            err_file_path = os.path.join(input_folder_path, err_file_name)
            with open(input_file_path, "w", encoding=ENCODING) as f_input:
                f_input.write(raw_text)
            nada_wrapper._apply_nada_exec_on_file(input_file_path, output_file_path, err_file_path)
            with open(output_file_path, "r", encoding=ENCODING) as f_output:
                actual_result = f_output.read()
            
        # Test
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test__apply_nada_exec_on_string(self):
        # Test parameters
        raw_text, expected_result = get__apply_nada_exec_test_data()
        nada_wrapper = NadaWrapper()
        
        # Actual result
        stdout_data, _ = nada_wrapper._apply_nada_exec_on_string(raw_text) # stderr_data
        actual_result = stdout_data
            
        # Test
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_process_sentences(self):
        # Test parameters
        tokenized_sentences, expected_result = get_process_sentences_test_data()
        nada_wrapper = NadaWrapper()
        
        # Actual result
        actual_result = nada_wrapper.process_sentences(tokenized_sentences)
            
        # Test
        self.assertEqual(expected_result, actual_result)
        


def get__apply_nada_exec_test_data():
    raw_text_array = ("Let me make it clear that I am not happy .", 
                      "I think he likes it .", 
                      "He said it would take three days to clear it all away .")
    raw_text = "\n".join(raw_text_array)
    expected_raw_text_array = ("Let me make it clear that I am not happy .\t3:0.000", 
                               "I think he likes it .\t4:0.837", 
                               "He said it would take three days to clear it all away .\t2:0.430\t9:0.876\n")
    expected_result = "\n".join(expected_raw_text_array)
    
    return raw_text, expected_result

def get_process_sentences_test_data():
    tokenized_sentences = ("Let me make it clear that I am not happy .", 
                           "I think he likes it .", 
                           "He said it would take three days to clear it all away ."
                           )
    expected_result = (((3, 0.0), ), 
                        ((4, 0.837), ), 
                        ((2, 0.430), (9, 0.876))
                        )
    return tokenized_sentences, expected_result

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()