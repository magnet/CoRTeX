# -*- coding: utf-8 -*-

import unittest
import os
import pytest

from cortex.utils.io import TemporaryDirectory
from cortex.parameters import ENCODING
from cortex.tools.wrapper.stanford_corenlp import StanfordCoreNLPWrapper

@pytest.mark.need_stanford_core_nlp
class TestStanfordCoreNLPWrapper(unittest.TestCase):

    def test_process(self):
        # Test parameters
        raw_text, expected_result_file_path, init_kwargs = get_data()
        input_file_name = "glop.txt"
        output_format = init_kwargs["output_format"]
        output_file_name = "{}.{}".format(input_file_name, output_format)
        stanford_corenlp_wrapper = StanfordCoreNLPWrapper(**init_kwargs)
        
        # Expected result
        with open(expected_result_file_path, "r", encoding=ENCODING) as f:
            expected_result = f.read()
        expected_result = expected_result.strip()
        
        # Actual result
        with TemporaryDirectory() as temp_input_folder, \
                TemporaryDirectory() as temp_output_folder:
            input_folder_path = temp_input_folder.temporary_folder_path
            output_folder_path = temp_output_folder.temporary_folder_path
            input_file_path = os.path.join(input_folder_path, input_file_name)
            output_file_path = os.path.join(output_folder_path, output_file_name)
            with open(input_file_path, "w", encoding=ENCODING) as f:
                f.write(raw_text)
            stanford_corenlp_wrapper.process(input_folder_path, output_folder_path)
            with open(output_file_path, "r", encoding=ENCODING) as f:
                actual_result = f.read()
            actual_result = actual_result.strip()
            
        # Test
        self.assertEqual(expected_result, actual_result)
        


def get_data():
    raw_text = "Stanford University is located in California. It is a great university, founded in 1891."
    expected_result_file_name = "test_file.txt.xml"
    expected_result_file_path = os.path.join(os.path.dirname(__file__), "data", "corenlp_pipeline", 
                                             expected_result_file_name)
    init_kwargs = {"annotators": ['tokenize', 'ssplit', 'pos', 'lemma', 'ner', 'parse'], 
                   "lang": 'en', "output_format": 'xml', "options": None}
    
    return raw_text, expected_result_file_path, init_kwargs


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()