# -*- coding: utf-8 -*-

import unittest

import numpy as np

from cortex.tools.ml_features import (NumericFeature, MultiNumericFeature, CategoricalFeature, 
                                        SampleVectorizer, 
                                        create_features_product, quantize_numeric_feature,
                                        NUMERIC_FEATURE_TYPE, MULTI_NUMERIC_FEATURE_TYPE,
                                        CATEGORICAL_FEATURE_TYPE, 
                                        UnknownCategoricalFeatureValueError,)
from scipy import sparse

#@unittest.skip
class TestNumericFeature(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        compute_fct = lambda x: -x**2
        feature = NumericFeature("test_numeric_feature", compute_fct)
        attribute_name_expected_result_pairs = (("FEATURE_TYPE", NUMERIC_FEATURE_TYPE),
                                                ("name", "test_numeric_feature"),
                                                ("numeric_size", 1),
                                                ("_compute_fct", compute_fct),
                                                )
        
        # Test
        for attribute_name, expected_value in attribute_name_expected_result_pairs:
            actual_value = getattr(feature, attribute_name)
            self.assertEqual(expected_value, actual_value)
    
    #@unittest.skip
    def test_compute_value(self):
        # Test parameters
        compute_fct = lambda x: -x**2
        feature = NumericFeature("test_numeric_feature", compute_fct)
        input_ = 5
        expected_result = -25
        
        # Test
        actual_result = feature.compute_value(input_)
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_fill_numeric_vector(self):
        # Test parameters
        compute_fct = lambda x: -x**2
        feature = NumericFeature("test_numeric_feature", compute_fct)
        input_ = 5
        row_id = 0
        expected_result = np.array((-25,), dtype=np.float)
        actual_result = np.empty((1,1))
        
        # Test
        self.assertFalse(np.allclose(expected_result, actual_result))
        feature.fill_numeric_vector(input_, actual_result, row_id)
        self.assertTrue(np.array_equiv(expected_result, actual_result))
    
    #@unittest.skip
    def test_to_numeric_dense_vector(self):
        # Test parameters
        compute_fct = lambda x: -x**2
        feature = NumericFeature("test_numeric_feature", compute_fct)
        input_ = 5
        expected_result = np.array(((-25,),), dtype=int)
        
        # Test
        actual_result = feature.to_numeric_dense_vector(input_)
        self.assertEqual(expected_result.shape, actual_result.shape)
        self.assertTrue(np.array_equiv(expected_result, actual_result))
    
    #@unittest.skip
    def test_to_numeric_sparse_vector(self):
        # Test parameters
        compute_fct = lambda x: -x**2
        feature = NumericFeature("test_numeric_feature", compute_fct)
        input_ = 5
        expected_result = sparse.csr_matrix(np.array(((-25,),), dtype=int))
        
        # Test
        actual_result = feature.to_numeric_sparse_vector(input_)
        self.assertTrue(np.array_equiv(expected_result.toarray(), actual_result.toarray()))



#@unittest.skip
class TestMultiNumericFeature(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        compute_fct = lambda x: -x**2 + np.array(((1,3,9),), dtype=np.float)
        feature_names = tuple("col_n°{}".format(i) for i in range(3))
        feature = MultiNumericFeature("test_multi_numeric_feature", compute_fct, feature_names)
        attribute_name_expected_result_pairs = (("FEATURE_TYPE", MULTI_NUMERIC_FEATURE_TYPE),
                                                ("name", "test_multi_numeric_feature"),
                                                ("numeric_size", 3),
                                                ("column_names", ("col_n°0", "col_n°1", "col_n°2")),
                                                ("numeric_column_names", ("[test_multi_numeric_feature]__(col_n°0)", 
                                                                            "[test_multi_numeric_feature]__(col_n°1)", 
                                                                            "[test_multi_numeric_feature]__(col_n°2)")),
                                                ("_compute_fct", compute_fct),
                                                )
        
        # Test
        for attribute_name, expected_value in attribute_name_expected_result_pairs:
            actual_value = getattr(feature, attribute_name)
            self.assertEqual(expected_value, actual_value)
    
    #@unittest.skip
    def test_compute_value(self):
        # Test parameters
        compute_fct = lambda x: -x**2 + np.array(((1,3,9),), dtype=np.float)
        feature_names = tuple("col_n°{}".format(i) for i in range(3))
        feature = MultiNumericFeature("test_numeric_feature", compute_fct, feature_names)
        input_ = 5
        expected_result = np.array(((-24,-22,-16),), dtype=np.float)
        
        # Test
        actual_result = feature.compute_value(input_)
        self.assertTrue(np.array_equiv(expected_result, actual_result))
    
    #@unittest.skip
    def test_fill_numeric_vector(self):
        # Test parameters
        def _fct(x, matrix=None, row_id=0, column_offset=0):
            if matrix is None:
                return -x**2 + np.array(((1,3,9),), dtype=np.float)
            else:
                matrix[row_id,column_offset:column_offset+3] = -x**2 + np.array(((1,3,9),), dtype=np.float)
        compute_fct = _fct
        feature_names = tuple("col_n°{}".format(i) for i in range(3))
        feature = MultiNumericFeature("test_numeric_feature", compute_fct, feature_names)
        input_ = 5
        row_id = 0
        expected_result = np.array(((-24,-22,-16),), dtype=np.float)
        actual_result = np.zeros((1,3))
        
        # Test
        self.assertFalse(np.allclose(expected_result, actual_result))
        feature.fill_numeric_vector(input_, actual_result, row_id)
        self.assertTrue(np.array_equiv(expected_result, actual_result))
    
    #@unittest.skip
    def test_to_numeric_dense_vector(self):
        # Test parameters
        def _fct(x, matrix=None, row_id=0, column_offset=0):
            if matrix is None:
                return -x**2 + np.array(((1,3,9),), dtype=np.float)
            else:
                matrix[row_id,column_offset:column_offset+3] = -x**2 + np.array(((1,3,9),), dtype=np.float)
        compute_fct = _fct
        feature_names = tuple("col_n°{}".format(i) for i in range(3))
        feature = MultiNumericFeature("test_numeric_feature", compute_fct, feature_names)
        input_ = 5
        expected_result = np.array(((-24,-22,-16),), dtype=np.float)
        
        # Test
        actual_result = feature.to_numeric_dense_vector(input_)
        self.assertEqual(expected_result.shape, actual_result.shape)
        self.assertTrue(np.array_equiv(expected_result, actual_result))
    
    #@unittest.skip
    def test_to_numeric_sparse_vector(self):
        # Test parameters
        def _fct(x, matrix=None, row_id=0, column_offset=0):
            if matrix is None:
                return -x**2 + np.array(((1,3,9),), dtype=np.float)
            else:
                matrix[row_id,column_offset:column_offset+3] = -x**2 + np.array(((1,3,9),), dtype=np.float)
        compute_fct = _fct
        feature_names = tuple("col_n°{}".format(i) for i in range(3))
        feature = MultiNumericFeature("test_numeric_feature", compute_fct, feature_names)
        input_ = 5
        expected_result = sparse.csr_matrix(np.array(((-24,-22,-16),), dtype=np.float))
        
        # Test
        actual_result = feature.to_numeric_sparse_vector(input_)
        self.assertTrue(np.array_equiv(expected_result.toarray(), actual_result.toarray()))



#@unittest.skip
class TestCategoricalFeature(unittest.TestCase):
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        compute_fct = lambda x: "yes" if -x**2 < -26 else "no"
        possible_values = ("yes", "no")
        strict = True
        feature = CategoricalFeature("test_categorical_feature", compute_fct, possible_values, strict=strict)
        attribute_name_expected_result_pairs = (("FEATURE_TYPE", CATEGORICAL_FEATURE_TYPE),
                                                ("name", "test_categorical_feature"),
                                                ("numeric_size", 2),
                                                ("_possible_values", ("yes", "no")),
                                                ("strict", True),
                                                #("_compute_fct", compute_fct),
                                                )
        expected_numeric_column_names_value = ("[test_categorical_feature]__(yes)", "[test_categorical_feature]__(no)")
        
        # Test
        actual_numeric_column_names_value = feature.numeric_column_names
        for attribute_name, expected_value in attribute_name_expected_result_pairs:
            actual_value = getattr(feature, attribute_name)
            self.assertEqual(expected_value, actual_value)
        self.assertEqual(expected_numeric_column_names_value, actual_numeric_column_names_value)
    
    #@unittest.skip
    def test_compute_value(self):
        # Test parameters
        compute_fct = lambda x: "yes" if -x**2 < -26 else "no"
        possible_values = ("yes", "no")
        strict = True
        feature = CategoricalFeature("test_categorical_feature", compute_fct, possible_values, strict=strict)
        input_ = 5
        expected_result = "no"
        
        # Test
        actual_result = feature.compute_value(input_)
        self.assertEqual(expected_result, actual_result)
        
        # Test parameters
        data = []
        compute_fct = lambda x: str(x)
        possible_values = ("1", "2", "3")
        strict = True
        feature = CategoricalFeature("test_categorical_feature", compute_fct, possible_values, strict=strict)
        input_ = 5
        actual_result = np.zeros((1,2))
        data.append(actual_result)
        data.append(actual_result)
        
        # Test
        for actual_result in data:
            with self.assertRaises(UnknownCategoricalFeatureValueError):
                feature.compute_value(input_)
            
    
    #@unittest.skip
    def test_fill_numeric_vector(self):
        # Test parameters
        data = []
        compute_fct = lambda x: "yes" if -x**2 < -26 else "no"
        possible_values = ("yes", "no")
        strict = True
        feature = CategoricalFeature("test_categorical_feature", compute_fct, possible_values, strict=strict)
        input_ = 5
        row_id = 0
        expected_result = np.array(((0,1),), dtype=np.float)
        actual_result = np.zeros((1,2))
        data.append((expected_result, actual_result, lambda x,y: np.array_equiv(x, y)))
        expected_result = sparse.lil_matrix(expected_result)
        actual_result = sparse.lil_matrix(np.zeros((1,2)))
        data.append((expected_result, actual_result, lambda x,y: np.array_equiv(x.toarray(), y.toarray())))
        
        # Test
        for expected_result, actual_result, comp_fct in data:
            self.assertFalse(comp_fct(expected_result, actual_result))
            feature.fill_numeric_vector(input_, actual_result, row_id)
            self.assertTrue(comp_fct(expected_result, actual_result))
        
        # Test parameters
        data = []
        compute_fct = lambda x: str(x)
        possible_values = ("1", "2", "3")
        strict = True
        feature = CategoricalFeature("test_categorical_feature", compute_fct, possible_values, strict=strict)
        input_ = 5
        row_id = 0
        actual_result = np.zeros((1,2))
        data.append(actual_result)
        actual_result = sparse.lil_matrix(np.zeros((1,2)))
        data.append(actual_result)
        
        # Test
        for actual_result in data:
            with self.assertRaises(UnknownCategoricalFeatureValueError):
                feature.fill_numeric_vector(input_, actual_result, row_id)
    
    #@unittest.skip
    def test_to_numeric_dense_vector(self):
        # Test parameters
        compute_fct = lambda x: "yes" if -x**2 < -26 else "no"
        possible_values = ("yes", "no")
        strict = True
        feature = CategoricalFeature("test_categorical_feature", compute_fct, possible_values, strict=strict)
        input_ = 5
        expected_result = np.array(((0,1),), dtype=np.float)
        
        # Test
        actual_result = feature.to_numeric_dense_vector(input_)
        self.assertEqual(expected_result.shape, actual_result.shape)
        self.assertTrue(np.array_equiv(expected_result, actual_result))
    
    #@unittest.skip
    def test_to_numeric_sparse_vector(self):
        # Test parameters
        compute_fct = lambda x: "yes" if -x**2 < -26 else "no"
        possible_values = ("yes", "no")
        strict = True
        feature = CategoricalFeature("test_categorical_feature", compute_fct, possible_values, strict=strict)
        input_ = 5
        expected_result = sparse.csr_matrix(np.array(((0,1),), dtype=np.float))
        
        # Test
        actual_result = feature.to_numeric_sparse_vector(input_)
        self.assertTrue(np.array_equiv(expected_result.toarray(), actual_result.toarray()))



#@unittest.skip
class TestFunctions(unittest.TestCase):
    
    #@unittest.skip
    def test_create_features_product(self):
        # Test1: two categorical features
        ## Test parameters
        ### Feature 1
        compute_fct = lambda x: "yes" if -x**3 < -28 else "no"
        possible_values = ("yes", "no")
        strict = True
        feature1 = CategoricalFeature("test_categorical_feature1", compute_fct, possible_values, strict=strict)
        ### Feature 2
        compute_fct = lambda x: "yep" if x < 0 else "nope"
        possible_values = ("yep", "nope")
        strict = True
        feature2 = CategoricalFeature("test_categorical_feature2", compute_fct, possible_values, strict=strict)
        ### Sample
        data = []
        data.append((3, "(no)_(nope)"))
        data.append((-5, "(no)_(yep)"))
        data.append((5, "(yes)_(nope)"))
        ### Expected result
        attribute_name_expected_result_pairs = (("name", "product_[test_categorical_feature1]_[test_categorical_feature2]"),
                                                ("numeric_size", 4),
                                                ("possible_values", ("(yes)_(yep)", "(yes)_(nope)", "(no)_(yep)", "(no)_(nope)")),
                                                ("numeric_column_names", ("[product_[test_categorical_feature1]_[test_categorical_feature2]]__((yes)_(yep))",
                                                                            "[product_[test_categorical_feature1]_[test_categorical_feature2]]__((yes)_(nope))",
                                                                            "[product_[test_categorical_feature1]_[test_categorical_feature2]]__((no)_(yep))",
                                                                            "[product_[test_categorical_feature1]_[test_categorical_feature2]]__((no)_(nope))",
                                                                            )),
                                                )
        
        ## Test
        product_feature = create_features_product(feature1, feature2)
        for attribute_name, expected_value in attribute_name_expected_result_pairs:
            actual_value = getattr(product_feature, attribute_name)
            self.assertEqual(expected_value, actual_value)
        
        for sample, expected_value in data:
            actual_value = product_feature.compute_value(sample)
            self.assertEqual(expected_value, actual_value)
        
        # Test2: one categorical feature and one numeric feature
        ## Test parameters
        ### Feature 1
        compute_fct = lambda x: -x**2 
        feature1 = NumericFeature("test_numeric_feature", compute_fct, )
        ### Feature 2
        compute_fct = lambda x: "yep" if x < 0 else "nope"
        possible_values = ("yep", "nope")
        strict = True
        feature2 = CategoricalFeature("test_categorical_feature", compute_fct, possible_values, strict=strict)
        ### Sample
        data = []
        data.append((3, sparse.csr_matrix(np.array(((0,-9),), dtype=np.float))))
        data.append((-5, sparse.csr_matrix(np.array(((-25,0),), dtype=np.float))))
        data.append((5, sparse.csr_matrix(np.array(((0,-25),), dtype=np.float))))
        ### Expected result
        attribute_name_expected_result_pairs = (("name", "product_[test_numeric_feature]_[test_categorical_feature]"),
                                                ("numeric_size", 2),
                                                ("numeric_column_names", ("[product_[test_numeric_feature]_[test_categorical_feature]]__(weighed_(yep))",
                                                                            "[product_[test_numeric_feature]_[test_categorical_feature]]__(weighed_(nope))",
                                                                            )),
                                                )
        
        ## Test
        product_feature = create_features_product(feature1, feature2)
        for attribute_name, expected_value in attribute_name_expected_result_pairs:
            actual_value = getattr(product_feature, attribute_name)
            self.assertEqual(expected_value, actual_value)
        
        for sample, expected_value in data:
            actual_value = product_feature.compute_value(sample)
            self.assertTrue(np.array_equiv(expected_value.todense(), actual_value.todense()))
        
        # Test3: one categorical feature and one multi numeric feature
        ## Test parameters
        ### Feature 1
        def _fct(x, matrix=None, row_id=0, column_offset=0):
            if matrix is None:
                return -x**2 + np.array(((1,3,9),), dtype=np.float)
            else:
                matrix[row_id,column_offset:column_offset+3] = -x**2 + np.array(((1,3,9),), dtype=np.float)
        compute_fct = _fct
        feature_names = tuple("col_n°{}".format(i) for i in range(3))
        feature1 = MultiNumericFeature("test_multi_numeric_feature", compute_fct, feature_names)
        ### Feature 2
        compute_fct = lambda x: "yep" if x < 0 else "nope"
        possible_values = ("yep", "nope")
        strict = True
        feature2 = CategoricalFeature("test_categorical_feature", compute_fct, possible_values, strict=strict)
        ### Sample
        data = []
        data.append((3, sparse.csr_matrix(np.array(((0,0,0,-8,-6,0),), dtype=np.float))))
        data.append((-5, sparse.csr_matrix(np.array(((-24,-22,-16,0,0,0),), dtype=np.float))))
        data.append((5, sparse.csr_matrix(np.array(((0,0,0,-24,-22,-16),), dtype=np.float))))
        ### Expected result
        attribute_name_expected_result_pairs = (("name", "product_[test_multi_numeric_feature]_[test_categorical_feature]"),
                                                ("numeric_size", 6),
                                                ("numeric_column_names", ("[product_[test_multi_numeric_feature]_[test_categorical_feature]]__((yep)_(col_n°0))",
                                                                            "[product_[test_multi_numeric_feature]_[test_categorical_feature]]__((yep)_(col_n°1))",
                                                                            "[product_[test_multi_numeric_feature]_[test_categorical_feature]]__((yep)_(col_n°2))",
                                                                            "[product_[test_multi_numeric_feature]_[test_categorical_feature]]__((nope)_(col_n°0))",
                                                                            "[product_[test_multi_numeric_feature]_[test_categorical_feature]]__((nope)_(col_n°1))",
                                                                            "[product_[test_multi_numeric_feature]_[test_categorical_feature]]__((nope)_(col_n°2))",
                                                                            )),
                                                )
        
        ## Test
        product_feature = create_features_product(feature1, feature2)
        for attribute_name, expected_value in attribute_name_expected_result_pairs:
            actual_value = getattr(product_feature, attribute_name)
            self.assertEqual(expected_value, actual_value)
        
        for sample, expected_value in data:
            actual_value = product_feature.compute_value(sample)
            self.assertTrue(np.array_equiv(expected_value.todense(), actual_value.todense()))
        
        # Test4: two multinumeric features
        ## Test parameters
        ### Feature 1
        def _fct2(x, matrix=None, row_id=0, column_offset=0):
            if matrix is None:
                return -x**2 + np.array(((1,3,9),), dtype=np.float)
            else:
                matrix[row_id,column_offset:column_offset+3] = -x**2 + np.array(((1,3,9),), dtype=np.float)
        compute_fct = _fct2
        feature_names = tuple("col_n°{}".format(i) for i in range(3))
        feature1 = MultiNumericFeature("test_multi_numeric_feature1", compute_fct, feature_names)
        ### Feature 2
        def _fct3(x, matrix=None, row_id=0, column_offset=0):
            if matrix is None:
                return np.sign(x)*np.array(((1,0,2,0),), dtype=np.float)
            else:
                matrix[row_id,column_offset:column_offset+4] = np.sign(x)*np.array(((1,0,2,0),), dtype=np.float)
        compute_fct = _fct3
        feature_names = tuple("col_n°{}".format(i) for i in range(4))
        feature2 = MultiNumericFeature("test_multi_numeric_feature2", compute_fct, feature_names)
        ### Sample
        data = []
        data.append((3, np.array(((-8,0,-16,0,-6,0,-12,0,0,0,0,0),), dtype=np.float)))
        data.append((-5, np.array(((24,0,48,0,22,0,44,0,16,0,32,0),), dtype=np.float)))
        data.append((5, np.array(((-24,0,-48,0,-22,0,-44,0,-16,0,-32,0),), dtype=np.float)))
        ### Expected result
        attribute_name_expected_result_pairs = (("name", "product_[test_multi_numeric_feature1]_[test_multi_numeric_feature2]"),
                                                ("numeric_size", 12),
                                                ("numeric_column_names", ("[product_[test_multi_numeric_feature1]_[test_multi_numeric_feature2]]__((col_n°0)_(col_n°0))",
                                                                            "[product_[test_multi_numeric_feature1]_[test_multi_numeric_feature2]]__((col_n°0)_(col_n°1))",
                                                                            "[product_[test_multi_numeric_feature1]_[test_multi_numeric_feature2]]__((col_n°0)_(col_n°2))",
                                                                            "[product_[test_multi_numeric_feature1]_[test_multi_numeric_feature2]]__((col_n°0)_(col_n°3))",
                                                                            "[product_[test_multi_numeric_feature1]_[test_multi_numeric_feature2]]__((col_n°1)_(col_n°0))",
                                                                            "[product_[test_multi_numeric_feature1]_[test_multi_numeric_feature2]]__((col_n°1)_(col_n°1))",
                                                                            "[product_[test_multi_numeric_feature1]_[test_multi_numeric_feature2]]__((col_n°1)_(col_n°2))",
                                                                            "[product_[test_multi_numeric_feature1]_[test_multi_numeric_feature2]]__((col_n°1)_(col_n°3))",
                                                                            "[product_[test_multi_numeric_feature1]_[test_multi_numeric_feature2]]__((col_n°2)_(col_n°0))",
                                                                            "[product_[test_multi_numeric_feature1]_[test_multi_numeric_feature2]]__((col_n°2)_(col_n°1))",
                                                                            "[product_[test_multi_numeric_feature1]_[test_multi_numeric_feature2]]__((col_n°2)_(col_n°2))",
                                                                            "[product_[test_multi_numeric_feature1]_[test_multi_numeric_feature2]]__((col_n°2)_(col_n°3))",
                                                                            )),
                                                )
        
        ## Test
        product_feature = create_features_product(feature1, feature2)
        for attribute_name, expected_value in attribute_name_expected_result_pairs:
            actual_value = getattr(product_feature, attribute_name)
            self.assertEqual(expected_value, actual_value)
        
        for sample, expected_value in data:
            actual_value = product_feature.compute_value(sample)
            self.assertTrue(np.array_equiv(expected_value, actual_value))
    
    #@unittest.skip
    def test_quantize_numeric_feature(self):
        # Test parameters
        ## Original numeric feature
        compute_fct = lambda x: -x**3
        feature = NumericFeature("test_numeric_feature", compute_fct)
        threshold_values = (56.9, -37, 12, -7, 100)
        ## Sample
        data = []
        data.append((-2, "-7.0<=value<12.0"))
        data.append((-50, "100.0<=value"))
        data.append((50, "value<-37.0"))
        ## Expected result
        attribute_name_expected_result_pairs = (("name", "quantized_[test_numeric_feature]"),
                                                ("numeric_size", 6),
                                                ("possible_values", ("value<-37.0", "-37.0<=value<-7.0", "-7.0<=value<12.0", "12.0<=value<56.9", "56.9<=value<100.0", "100.0<=value")),
                                                ("numeric_column_names", ("[quantized_[test_numeric_feature]]__(value<-37.0)", 
                                                                            "[quantized_[test_numeric_feature]]__(-37.0<=value<-7.0)", 
                                                                            "[quantized_[test_numeric_feature]]__(-7.0<=value<12.0)", 
                                                                            "[quantized_[test_numeric_feature]]__(12.0<=value<56.9)", 
                                                                            "[quantized_[test_numeric_feature]]__(56.9<=value<100.0)", 
                                                                            "[quantized_[test_numeric_feature]]__(100.0<=value)")),
                                                )
        
        # Test
        quantized_feature = quantize_numeric_feature(feature, threshold_values)
        for attribute_name, expected_value in attribute_name_expected_result_pairs:
            actual_value = getattr(quantized_feature, attribute_name)
            self.assertEqual(expected_value, actual_value)
        
        for sample, expected_value in data:
            actual_value = quantized_feature.compute_value(sample)
            self.assertEqual(expected_value, actual_value)


#@unittest.skip
class TestSampleVectorizer(unittest.TestCase):
    
    def setUp(self):
        unittest.TestCase.setUp(self)
        
        features = []
        ## Numeric feature
        compute_fct = lambda x: -x**2
        feature = NumericFeature("test_numeric_feature", compute_fct)
        features.append(feature)
        ## Categorical feature 1
        compute_fct = lambda x: "yes" if -x**2 < -26 else "no"
        possible_values = ("yes", "no")
        strict = True
        feature = CategoricalFeature("test_categorical_feature1", compute_fct, possible_values, strict=strict)
        features.append(feature)
        ## Categorical feature 2
        compute_fct = lambda x: "yep" if x < 0 else "nope"
        possible_values = ("yep", "nope")
        strict = True
        feature = CategoricalFeature("test_categorical_feature2", compute_fct, possible_values, strict=strict)
        features.append(feature)
        ## Group features
        first_group = (("test_categorical_feature1",), ("test_categorical_feature2",))
        second_group = (("test_categorical_feature2",), ("test_categorical_feature1",))
        features_groups = (first_group, second_group, )
        feature_name2feature = {feature.name: feature for feature in features}
        group_features = []
        for features_group in features_groups:
            first_features_names, second_features_names = features_group
            for feature_name1 in first_features_names:
                feature1 = feature_name2feature[feature_name1]
                for feature_name2 in second_features_names:
                    feature2 = feature_name2feature[feature_name2]
                    feature_product = create_features_product(feature1, feature2)
                    group_features.append(feature_product)
        ## Finally, the FeatureEncoder instance
        features = features + group_features
        features_vectorizer = SampleVectorizer(features)
        
        self.features_vectorizer = features_vectorizer
    
    #@unittest.skip
    def test___init__(self):
        # Test parameters
        features_vectorizer = self.features_vectorizer
        attribute_name_expected_result_pairs = (("features_names", ("test_numeric_feature", 
                                                                    "test_categorical_feature1",
                                                                    "test_categorical_feature2",
                                                                    "product_[test_categorical_feature1]_[test_categorical_feature2]",
                                                                    "product_[test_categorical_feature2]_[test_categorical_feature1]",)),
                                                ("numeric_column_names", ("test_numeric_feature",  
                                                                            "[test_categorical_feature1]__(yes)",
                                                                            "[test_categorical_feature1]__(no)",
                                                                            "[test_categorical_feature2]__(yep)",
                                                                            "[test_categorical_feature2]__(nope)", 
                                                                            "[product_[test_categorical_feature1]_[test_categorical_feature2]]__((yes)_(yep))",
                                                                            "[product_[test_categorical_feature1]_[test_categorical_feature2]]__((yes)_(nope))",
                                                                            "[product_[test_categorical_feature1]_[test_categorical_feature2]]__((no)_(yep))",
                                                                            "[product_[test_categorical_feature1]_[test_categorical_feature2]]__((no)_(nope))",
                                                                            "[product_[test_categorical_feature2]_[test_categorical_feature1]]__((yep)_(yes))",
                                                                            "[product_[test_categorical_feature2]_[test_categorical_feature1]]__((yep)_(no))",
                                                                            "[product_[test_categorical_feature2]_[test_categorical_feature1]]__((nope)_(yes))",
                                                                            "[product_[test_categorical_feature2]_[test_categorical_feature1]]__((nope)_(no))")),
                                                ("features_nb", 5),
                                                ("numeric_column_nb", 13),
                                                )
        
        # Test
        for attribute_name, expected_value in attribute_name_expected_result_pairs:
            actual_value = getattr(features_vectorizer, attribute_name)
            self.assertEqual(expected_value, actual_value)
    
    #@unittest.skip
    def test_compute_value(self):
        # Test parameters
        features_vectorizer = self.features_vectorizer
        input_ = 5
        expected_result = (-25, "no", "nope", "(no)_(nope)", "(nope)_(no)")
        
        # Test
        actual_result = features_vectorizer.compute_value(input_)
        self.assertEqual(expected_result, actual_result)
    
    #@unittest.skip
    def test_fill_matrix(self):
        # Test parameters
        features_vectorizer = self.features_vectorizer
        matrix = np.matrix(np.zeros((2,3+features_vectorizer.numeric_column_nb+5), dtype=np.float))
        input_ = 5
        row_id = 1
        column_offset = 3
        expected_result = np.array(((0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0), 
                                    (0,0,0,-25, 0,1, 0,1, 0,0,0,1, 0,0,0,1,0,0,0,0,0),
                                    ),dtype=np.float)
        
        # Test
        actual_result = matrix
        self.assertFalse(np.array_equiv(expected_result, actual_result))
        features_vectorizer.fill_matrix(input_, matrix, row_id=row_id, column_offset=column_offset)
        self.assertTrue(np.array_equiv(expected_result, actual_result))
    
    #@unittest.skip
    def test__vectorize_sample(self):
        # Test parameters
        features_vectorizer = self.features_vectorizer
        matrix = np.matrix(np.zeros((2,features_vectorizer.numeric_column_nb), dtype=np.float))
        input_ = 5
        row_id = 1
        expected_result = np.array(((0,0,0,0,0,0,0,0,0,0,0,0,0), 
                                    (-25, 0,1, 0,1, 0,0,0,1, 0,0,0,1),
                                    ),dtype=np.float)
        
        # Test
        actual_result = matrix
        self.assertFalse(np.array_equiv(expected_result, actual_result))
        features_vectorizer._vectorize_sample(input_, matrix, row_id)
        self.assertTrue(np.array_equiv(expected_result, actual_result))
    
    #@unittest.skip
    def test_vectorize__dense(self):
        # Test parameters
        features_vectorizer = self.features_vectorizer
        output_format = "dense"
        inputs = (5, -9)
        expected_result = np.array(((-25, 0,1, 0,1, 0,0,0,1, 0,0,0,1),
                                    (-81, 1,0, 1,0, 1,0,0,0, 1,0,0,0),),dtype=np.float)
        
        # Test
        actual_result = features_vectorizer.vectorize(inputs, output_format=output_format)
        self.assertTrue(np.array_equiv(expected_result, actual_result))
    
    #@unittest.skip
    def test_vectorize__sparse(self):
        # Test parameters
        features_vectorizer = self.features_vectorizer
        output_format = "sparse"
        inputs = (5, -9)
        expected_result = sparse.csr_matrix(np.array(((-25, 0,1, 0,1, 0,0,0,1, 0,0,0,1),
                                                      (-81, 1,0, 1,0, 1,0,0,0, 1,0,0,0),),dtype=np.float))
        
        # Test
        actual_result = features_vectorizer.vectorize(inputs, output_format=output_format)
        self.assertTrue(np.array_equiv(expected_result.todense(), actual_result.todense()))
    
    #@unittest.skip
    def test_vectorize_to_dense_numeric(self):
        # Test parameters
        features_vectorizer = self.features_vectorizer
        input_ = 5
        expected_result = np.array(((-25, 0,1, 0,1, 0,0,0,1, 0,0,0,1),),dtype=np.float)
        
        # Test
        actual_result = features_vectorizer.vectorize_to_dense_numeric(input_)
        self.assertTrue(np.array_equiv(expected_result, actual_result))
    
    #@unittest.skip
    def test_vectorize_to_sparse_numeric(self):
        # Test parameters
        features_vectorizer = self.features_vectorizer
        input_ = 5
        expected_result = sparse.csr_matrix(np.array(((-25, 0,1, 0,1, 0,0,0,1, 0,0,0,1),),dtype=np.float))
        
        # Test
        actual_result = features_vectorizer.vectorize_to_sparse_numeric(input_)
        self.assertEqual(type(expected_result), type(actual_result))
        self.assertTrue(np.array_equiv(expected_result.todense(), actual_result.todense()))
    
    #@unittest.skip
    def test_decode_numeric_vector(self):
        # Test parameters
        features_vectorizer = self.features_vectorizer
        vector = sparse.csr_matrix(np.array(((-25, 0,1, 0,1, 0,0,0,1, 0,0,0,1),),dtype=np.float))
        expected_result = ((0, "test_numeric_feature",-25),
                           (2, "[test_categorical_feature1]__(no)",1),
                           (4, "[test_categorical_feature2]__(nope)",1),
                           (8, "[product_[test_categorical_feature1]_[test_categorical_feature2]]__((no)_(nope))",1),
                           (12, "[product_[test_categorical_feature2]_[test_categorical_feature1]]__((nope)_(no))",1),
                           )
        
        # Test
        actual_result = features_vectorizer.decode_numeric_vector(vector)
        self.assertEqual(expected_result, actual_result)



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()