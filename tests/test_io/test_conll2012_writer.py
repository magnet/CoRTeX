# -*- coding: utf-8 -*-

import unittest
import os
from collections import OrderedDict

from cortex.parameters import ENCODING
from cortex.io.conll2012_writer import CONLL2012DocumentWriter, CONLL2012MultiDocumentWriter, MentionDoesNotExistError
from cortex.io.conll2012_reader import CONLL2012DocumentReader
from cortex.api.document import Document
from cortex.api.document_buffer import deepcopy_document
from cortex.api.markable import Token, Mention, Sentence, NamedEntity
from cortex.api.constituency_tree import ConstituencyTree
from cortex.api.entity import Entity
from cortex.api.coreference_partition import CoreferencePartition
from cortex.utils.io import TemporaryDirectory


#@unittest.skip
class TestCONLL2012DocumentWriter(unittest.TestCase):
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.maxDiff = None
    
    #@unittest.skip
    def test_write_using_file_path(self):
        # Test parameters & expected results
        (test_folder_path, file_base_name_and_parameters_pairs), document = get_test_data_document_write()
        
        # Actual result & tests
        with TemporaryDirectory(delete_upon_exit=True) as tmp_directory:
            tmp_folder_path = tmp_directory.temporary_folder_path
            for file_base_name, kwargs in file_base_name_and_parameters_pairs:
                # Write
                file_path = os.path.join(tmp_folder_path, file_base_name)
                CONLL2012DocumentWriter.write(document, file_path, **kwargs)
                # Fetch actual result
                file_base_name = "{}.conll2012".format(file_base_name) 
                expected_file_path = os.path.join(test_folder_path, file_base_name)
                actual_file_path = os.path.join(tmp_folder_path, file_base_name)
                self.assertTrue(os.path.isfile(expected_file_path), expected_file_path)
                self.assertTrue(os.path.isfile(actual_file_path), actual_file_path)
                with open(expected_file_path, "r", encoding=ENCODING) as f:
                    expected_string = f.read()
                with open(actual_file_path, "r", encoding=ENCODING) as f:
                    actual_string = f.read()
                self.assertEqual(expected_string, actual_string)
    
    #@unittest.skip
    def test_write_using_line_buffer(self):
        # Test parameters & expected results
        (test_folder_path, file_base_name_parameters_pairs), document = get_test_data_document_write()
        
        conll2012_document_reader = CONLL2012DocumentReader()
        
        # Actual result & tests
        with TemporaryDirectory(delete_upon_exit=True) as tmp_directory:
            tmp_folder_path = tmp_directory.temporary_folder_path
            for file_base_name, kwargs in file_base_name_parameters_pairs:
                # Write
                file_path = os.path.join(tmp_folder_path, file_base_name)
                actual_file_path = file_path + ".conll2012"
                with open(actual_file_path, "w", encoding=ENCODING) as f:
                    CONLL2012DocumentWriter.write(document, f, **kwargs)
                # Fetch actual result
                file_name = "{}.conll2012".format(file_base_name) 
                expected_file_path = os.path.join(test_folder_path, file_name)
                actual_file_path = os.path.join(tmp_folder_path, file_name)
                self.assertTrue(os.path.isfile(expected_file_path))
                self.assertTrue(os.path.isfile(actual_file_path))
                with open(expected_file_path, "r", encoding=ENCODING) as f:
                    expected_string = f.read()
                with open(actual_file_path, "r", encoding=ENCODING) as f:
                    actual_string = f.read()
                self.assertEqual(expected_string, actual_string)
                reread_document = conll2012_document_reader.parse(actual_file_path)
                self.assertEqual(document.raw_text, reread_document.raw_text)
    
    #@unittest.skip
    def test_write_strict(self):
        # Test parameters
        data = get_test_data_document_write_strict()
        
        # Test
        with TemporaryDirectory() as temp_dir:
            # Case 1
            test_file_name, (document, kwargs) = data[0]
            test_file_path = os.path.join(temp_dir.temporary_folder_path, test_file_name)
            # Assert that exception is raised in this case
            with self.assertRaises(MentionDoesNotExistError):
                with open(test_file_path, "w", encoding="utf-8") as f:
                    CONLL2012DocumentWriter.write(document, f, **kwargs)
            
            # Check that everything goes smoothly for the non-strict case
            test_file_name, (document, kwargs) = data[1]
            test_file_path = os.path.join(temp_dir.temporary_folder_path, test_file_name)
            with open(test_file_path, "w", encoding="utf-8") as f:
                CONLL2012DocumentWriter.write(document, f, **kwargs)


#@unittest.skip
class TestCONLL2012MultiDocumentWriter(unittest.TestCase):
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.maxDiff = None
    
    #@unittest.skip
    def test_write_using_file_path(self):
        # Test parameters & expected results
        (test_folder_path, file_base_name_parameters_pairs), documents = get_test_data_multidocument_write()
        
        # Actual result & tests
        with TemporaryDirectory(delete_upon_exit=True) as tmp_directory:
            tmp_folder_path = tmp_directory.temporary_folder_path
            for document_ident, kwargs in file_base_name_parameters_pairs:
                # Write
                file_path = os.path.join(tmp_folder_path, document_ident)
                CONLL2012MultiDocumentWriter.write(documents, file_path, **kwargs)
                # Fetch actual result
                file_name = "{}.multi_conll2012".format(document_ident) 
                expected_file_path = os.path.join(test_folder_path, file_name)
                actual_file_path = os.path.join(tmp_folder_path, file_name)
                self.assertTrue(os.path.isfile(expected_file_path), expected_file_path)
                self.assertTrue(os.path.isfile(actual_file_path), actual_file_path)
                with open(expected_file_path, "r", encoding=ENCODING) as f:
                    expected_string = f.read()
                with open(actual_file_path, "r", encoding=ENCODING) as f:
                    actual_string = f.read()
                self.assertEqual(expected_string, actual_string)
    
    #@unittest.skip
    def test_write_using_line_buffer(self):
        # Test parameters & expected results
        (test_folder_path, file_base_name_parameters_pairs), documents = get_test_data_multidocument_write()
        
        # Actual result & tests
        with TemporaryDirectory(delete_upon_exit=True) as tmp_directory:
            tmp_folder_path = tmp_directory.temporary_folder_path
            for document_ident, kwargs in file_base_name_parameters_pairs:
                # Write
                conll2012_multidocument_writer = CONLL2012MultiDocumentWriter()
                file_path = os.path.join(tmp_folder_path, document_ident)
                actual_file_path = file_path + ".multi_conll2012"
                with open(actual_file_path, "w", encoding=ENCODING) as f:
                    conll2012_multidocument_writer.write(documents, f, **kwargs)
                # Fetch actual result
                file_name = "{}.multi_conll2012".format(document_ident) 
                expected_file_path = os.path.join(test_folder_path, file_name)
                actual_file_path = os.path.join(tmp_folder_path, file_name)
                self.assertTrue(os.path.isfile(expected_file_path), expected_file_path)
                self.assertTrue(os.path.isfile(actual_file_path), actual_file_path)
                with open(expected_file_path, "r", encoding=ENCODING) as f:
                    expected_string = f.read()
                with open(actual_file_path, "r", encoding=ENCODING) as f:
                    actual_string = f.read()
                self.assertEqual(expected_string, actual_string)



def get_test_data_document_write_strict():
    data = []
    
    _, document = get_test_data_document_write() #(test_folder_path, file_base_name_parameters_pairs)
    
    inconsistent_coreference_partition = CoreferencePartition(mention_extents=((96,108),))
    document.coreference_partition = inconsistent_coreference_partition
    
    test_file_name = "test.conll2012"
    kwargs = {"skip_singleton": False, "strict": True}
    data.append((test_file_name, (document, kwargs)))
    
    test_file_name = "test.conll2012"
    kwargs = {"skip_singleton": False, "strict": False}
    data.append((test_file_name, (document, kwargs)))
    
    return data


def get_test_data_multidocument_write():
    (test_folder_path, file_base_name_parameters_pairs), document = get_test_data_document_write()
    document1 = document
    document1.ident = "test_conll2012_multidocument_writer1"
    # Bypassing the deepcopy consistency enforcement
    coref_part1 = document1.coreference_partition
    document1.coreference_partition = None
    document2 = deepcopy_document(document1, 
                                  copy_document_ident="test_conll2012_multidocument_writer2", 
                                  strict=False)
    document1.coreference_partition = coref_part1
    document2.coreference_partition = coref_part1.copy()
    documents = [document1, document2]
    
    file_base_name_parameters_pairs = [(doc_ident+"_multi", kwargs) for doc_ident, kwargs in file_base_name_parameters_pairs]
    
    return (test_folder_path, file_base_name_parameters_pairs), documents


def get_test_data_document_write():
    # Test parameters
    test_folder_path = os.path.join(os.path.dirname(__file__), "data", "conll2012_writer", "test_correct_write")
    file_base_name_and_parameters_pairs = [("test_not_skip_singleton", {"skip_singleton": False, 
                                                                        "strict": False, 
                                                                        "offset": False}), 
                                           ("test_skip_singleton", {"skip_singleton": True, 
                                                                    "strict": False, 
                                                                    "offset": False}),
                                           ("test_token_index", {"skip_singleton": True, 
                                                                 "strict": False, 
                                                                 "offset": True}),
                                           ]
    
    # Expected result
    doc_ident = "test_conll2012_document_writer"
    raw_text = "\nIn June of 2004 , the Shanxi Yongji Peasants Association was formally "\
                        "established ; it now has 3,800 members .\nAccordingly , this is the "\
                        "first peasant organization to be legally registered and designated a "\
                        "peasant association since the founding of the PRC ."
    document = Document(doc_ident, raw_text, info={"original_corpus_format": "conll"})
    
    ## First expected sentence
    sentences_data = []
    sentence_data = ["1", (1,110), "In June of 2004 , the Shanxi Yongji Peasants Association was formally established ; it now has 3,800 members .", 
                     "(TOP (S (S (PP (IN In) (NP (NP (NNP June)) (PP (IN of) (NP (CD 2004))))) (, ,) (NP (DT the) (NNP Shanxi) (NNP Yongji) (NNPS Peasants) (NNP Association)) (VP (VBD was) (ADVP (RB formally)) (VP (VBN established)))) (: ;) (S (NP (PRP it)) (ADVP (RB now)) (VP (VBZ has) (NP (CD 3,800) (NNS members)))) (. .)))",
                     "speaker#1", 
                     (((58, 60, "V"),), # Warning: this data is not present in the original CONLL file?
                      ((1, 15, "ARGM-TMP"), (19, 56, "ARG1"), (62, 69, "ARGM-MNR"), (71, 81, "V")),
                      ((85, 86, "ARG0"), (88, 90, "ARGM-TMP"), (92, 94, "V"), (96, 108, "ARG1"))
                     ),
                     [("4,15", (4,15), "June of 2004", "DATE", None),
                      ("19,56", (19,56), "the Shanxi Yongji Peasants Association", "ORG", None),
                      ("96,100", (96,100), "3,800", "CARDINAL", None)
                     ]
                    ]
    sentence_tokens_data = [("1-1", (1,2), "In", "In", "IN"), 
                   ("1-2", (4,7), "June", "June", "NNP"), 
                   ("1-3", (9,10), "of", "of", "IN"), 
                   ("1-4", (12,15), "2004", "2004", "CD"),
                   ("1-5", (17,17), ",", ",", ","),
                   ("1-6", (19,21), "the", "the", "DT"),
                   ("1-7", (23,28), "Shanxi", "Shanxi", "NNP"),
                   ("1-8", (30,35), "Yongji", "Yongji", "NNP"),
                   ("1-9", (37,44), "Peasants", "Peasants", "NNPS"),
                   ("1-10", (46,56), "Association", "Association", "NNP"), 
                   ("1-11", (58,60), "was", "be", "VBD"),
                   ("1-12", (62,69), "formally", "formally", "RB"),
                   ("1-13", (71,81), "established", "establish", "VBN"),
                   ("1-14", (83,83), ";", ";", ":"),
                   ("1-15", (85,86), "it", "it", "PRP"),
                   ("1-16", (88,90), "now", "now", "RB"),
                   ("1-17", (92,94), "has", "have", "VBZ"),
                   ("1-18", (96,100), "3,800", "3,800", "CD"),
                   ("1-19", (102,108), "members", "members", "NNS"),
                   ("1-20", (110,110), ".", ".", ".")]
    sentence_mentions_data = [((19,56), (46,56), 0, "the Shanxi Yongji Peasants Association", "NAME", "LONG_NAME", "sg", "unk", "ORG", None, None), 
                              ((85,86), (85,86), 0, "it", "PRONOUN", "PERS_PRO", "sg", "neut", None, None, 0.991)]
    sentence_data.append(sentence_tokens_data)
    sentence_data.append(sentence_mentions_data)
    sentences_data.append(sentence_data)
    
    ## Second expected sentence
    sentence_data = ["2", (112,257), "Accordingly , this is the first peasant organization to be legally registered and designated a peasant association since the founding of the PRC .", 
                       "(TOP (S (ADVP (RB Accordingly)) (, ,) (NP (DT this)) (VP (VBZ is) (NP (NP (DT the) (JJ first) (NN peasant) (NN organization)) (SBAR (S (VP (TO to) (VP (VB be) (ADVP (RB legally)) (VP (VP (VBN registered)) (CC and) (VP (VBN designated) (S (NP (DT a) (NN peasant) (NN association)))) (PP (IN since) (NP (NP (DT the) (NN founding)) (PP (IN of) (NP (DT the) (NNP PRC)))))))))))) (. .)))", 
                      "speaker#1", 
                      (((112, 122, "ARGM-DIS"), (126, 129, "ARG1"), (131, 132, "V"), (134, 255, "ARG2")),
                       ((134, 163, "ARG1"), (171, 177, "ARGM-MNR"), (179, 188, "V"), (227, 255, "ARGM-TMP")) ,
                       ((134, 163, "ARG1"), (171, 177, "ARGM-MNR"), (194, 203, "V"), (205, 225, "ARG2"), (227, 255, "ARGM-TMP")), 
                       ((168, 169, "V"),)           
                      ),
                      [("138,142", (138,142), "first", "ORDINAL", None),
                      ("253,255", (253,255), "PRC", "GPE", None)
                      ]
                    ]
    sentence_tokens_data = [("2-1", (112,122), "Accordingly", "Accordingly", "RB"),
                   ("2-2", (124,124), ",", ",", ","),
                   ("2-3", (126,129), "this", "this", "DT"),
                   ("2-4", (131,132), "is", "be", "VBZ"),
                   ("2-5", (134,136), "the", "the", "DT"),
                   ("2-6", (138,142), "first", "first", "JJ"),
                   ("2-7", (144,150), "peasant", "peasant", "NN"),
                   ("2-8", (152,163), "organization", "organization", "NN"),
                   ("2-9", (165,166), "to", "to", "TO"),
                   ("2-10", (168,169), "be", "be", "VB"),
                   ("2-11", (171,177), "legally", "legally", "RB"),
                   ("2-12", (179,188), "registered", "register", "VBN"),
                   ("2-13", (190,192), "and", "and", "CC"),
                   ("2-14", (194,203), "designated", "designate", "VBN"),
                   ("2-15", (205,205), "a", "a", "DT"),
                   ("2-16", (207,213), "peasant", "peasant", "NN"),
                   ("2-17", (215,225), "association", "association", "NN"),
                   ("2-18", (227,231), "since", "since", "IN"),
                   ("2-19", (233,235), "the", "the", "DT"),
                   ("2-20", (237,244), "founding", "found", "NN"),
                   ("2-21", (246,247), "of", "of", "IN"),
                   ("2-22", (249, 251), "the", "the", "DT"),
                   ("2-23", (253,255), "PRC", "PRC", "NNP"),
                   ("2-24", (257,257), ".", ".", ".")]
    sentence_mentions_data = [((126,129), (126,129), 0, "this", "PRONOUN", "DEM_PRO", "sg", "neut", None, None, None), 
                              ((249,255), (253,255), 1, "the PRC", "NAME", "LONG_NAME", "sg", "neut", None, None, None)] # Warning: this mention has been added to the test document, compared to the original document where it does not appear.
    sentence_data.append(sentence_tokens_data)
    sentence_data.append(sentence_mentions_data)
    sentences_data.append(sentence_data)
    
    ## Set the sentences' data
    expected_entities = {}
    sentences = []
    named_entities_list = []
    for sentence_data in sentences_data:
        (ident, extent, raw_text, constituency_tree_string, speaker, predicate_arguments, 
         named_entities_data, tokens_data, mentions_data) = sentence_data
        sentence = Sentence(ident, extent, document, raw_text=raw_text)
        
        # Mentions
        mentions = OrderedDict()
        for mention_data in mentions_data:
            (extent, head_extent, entity_id, raw_text, gram_type, gram_subtype, number, gender, 
             ne_type, ne_subtype, referential_probability) = mention_data
            ident = "{e[0]},{e[1]}".format(e=extent)
            mention = Mention(ident, extent, document, raw_text=raw_text)
            mention.head_extent = head_extent
            mention.gram_type, mention.gram_subtype = gram_type, gram_subtype
            mention.number, mention.gender = number, gender
            if ne_type is not None:
                named_entity = NamedEntity(str(extent), extent, document, ne_type, raw_text=raw_text)
                named_entity.subtype = ne_subtype
                mention.named_entity = named_entity
            mention.referential_probability = referential_probability
            if entity_id not in expected_entities:
                entity = Entity(ident=entity_id)
                expected_entities[entity_id] = entity
            expected_entities[entity_id].add(mention.extent)
            mentions[extent] =  mention
        mentions_list = list(mentions.values())
        
        # Tokens
        tokens = OrderedDict()
        for token_data in tokens_data:
            ident, extent, raw_text, lemma, POS_tag = token_data
            token = Token(ident, extent, document, raw_text=raw_text)
            token.lemma = lemma
            token.POS_tag = POS_tag
            tokens[extent] = token
        tokens_list = list(tokens.values())
        
        # Named entities
        named_entities = OrderedDict()
        for named_entity_data in named_entities_data:
            ident, extent, raw_text, type_, subtype = named_entity_data
            named_entity = NamedEntity(ident, extent, document, type_, raw_text=raw_text)
            named_entity.subtype = subtype
            named_entities[extent] = named_entity
        named_entities_list.extend(list(named_entities.values()))
        
        # ConstituencyTree
        sentence.constituency_tree = ConstituencyTree.parse(constituency_tree_string)
        
        # Speaker
        sentence.speaker = speaker
        
        # Predicate arguments
        sentence.predicate_arguments = predicate_arguments
        
        # Synchronization tokens <-> mentions
        for (ms, me), mention in mentions.items():
            mention.tokens = list(t for (ts, te), t in tokens.items() if ms <= ts and te <= me)
            mhs, mhe = mention.head_extent
            mention.head_tokens = list(t for (ts, te), t in tokens.items() if mhs <= ts and te <= mhe)
        
        # Synchronization named_entities <-> mentions
        for extent, mention in mentions.items():
            mention.named_entity = named_entities.get(extent)
        
        # Synchronization tokens <-> constituency tree leaves
        assert len(sentence.constituency_tree.leaves) == len(tokens), "Arg."
        for i, node in enumerate(sentence.constituency_tree.leaves):
            token = tokens_list[i]
            node.token = token
            token.constituency_tree_node = node
        
        # Set sentence's data
        sentence.mentions = mentions_list
        sentence.tokens = tokens_list
        #sentence.named_entities = named_entities_list
        
        # Finally
        sentences.append(sentence)
    
    ## Finally, set the document's data
    document.tokens = list(token for sentence in sentences for token in sentence.tokens)
    document.mentions = list(mention for sentence in sentences for mention in sentence.mentions)[:1]
    document.sentences = sentences
    document.named_entities = named_entities_list
    document.coreference_partition = CoreferencePartition(entities=expected_entities.values())
    
    return (test_folder_path, file_base_name_and_parameters_pairs), document




"""
l1 = expected_document._mentions
        l2 = actual_document._mentions
        i = 0
        m1 = l1[i]
        m2 = l2[i]
        print(m1)
        print(m2)
        print(m1.ident)
        print(m2.ident)
        print(m1.head_extent)
        print(m2.head_extent)
        print(m1.gram_type)
        print(m2.gram_type)
        print(m1.gram_subtype)
        print(m2.gram_subtype)
        print(m1.gender)
        print(m2.gender)
        print(m1.number)
        print(m2.number)
        print(m1.wn_synonyms)
        print(m2.wn_synonyms)
        print(m1.wn_hypernyms)
        print(m2.wn_hypernyms)
        print(m1.named_entity)
        print(m2.named_entity)
        print(m1.referential_probability)
        print(m2.referential_probability)
        print(m1._tokens)
        print(m2._tokens)
        print(m1._head_tokens)
        print(m2._head_tokens)
        """
"""
l1 = expected_document.sentences
        l2 = actual_document.sentences
        i = 1
        m1 = l1[i]
        m2 = l2[i]
        attr_names = ["ident", "_tokens", "_mentions", "constituency_tree", "predicate_arguments", "speaker"]
        for name in attr_names:
            print(getattr(m1, name))
            print(getattr(m2, name))
        self.assertEqual(m1, m2)
        self.assertEqual(l1, l2)
"""

"""
c1 = expected_document.coreference_partition
        c2 = actual_document.coreference_partition
        self.assertEqual(c1._mention_to_entity.keys(), c2._mention_to_entity.keys())
        #print(list(c1))
        #print(list(c2))
        for m in sorted(c1._mention_to_entity.keys()):
            e1 = c1._mention_to_entity[m]
            e2 = c2._mention_to_entity[m]
            print("'{}'  =>  '{}'".format(m, e1))
            print("'{}'  =>  '{}'".format(m, e2))
            self.assertEqual(e1, e2)
            print()
        
        self.assertEqual(c1, c2)
        """

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()