# -*- coding: utf-8 -*-

import unittest
import os
import itertools
from collections import OrderedDict

from cortex.io.corpus_io import (_create_corpus_persist_params_string, 
                                 _read_corpus_parsing_parameters_string,
                                 write_corpus, parse_corpus_documents,)
from cortex.io.pivot_reader import PivotReader
from cortex.io.conll2012_reader import CONLL2012DocumentReader, CONLL2012MultiDocumentReader
from cortex.io.conllu_reader import CONLLUDocumentReader
from cortex.utils.io import TemporaryDirectory
from cortex.parameters import ENCODING


#@unittest.skip
class Test(unittest.TestCase):
    
    #@unittest.skip
    def test__create_corpus_persist_params_string(self):
        # Test parameters
        format_ = "conll2012"
        d_ = OrderedDict((("field1", "value1"), ("field2", 2), ("field3", None), ("field4", True)))
        expected_string = '{"format_": "conll2012", "field1": "value1", "field2": 2, "field3": null, "field4": true}'
        def _assert_good_string(expected_s, actual_s):
            convert_ = lambda string: dict(tuple(ss.strip() for ss in s.strip().split(":")) for s in string[1:-1].split(","))
            expected_d = convert_(expected_s)
            actual_d = convert_(actual_s)
            self.assertEqual(expected_d, actual_d)
        
        # Test
        actual_string = _create_corpus_persist_params_string(format_, **d_)
        _assert_good_string(expected_string, actual_string)
    
    #@unittest.skip
    def test__read_corpus_parsing_parameters_string(self):
        # Test parameters
        json_string = '{"format_": "conll2012", "field1": "value1", "field2": 2, "field3": null}'
        expected_format_ = "conll2012"
        expected_params = {"field1": "value1", "field2": 2, "field3": None}
        
        # Test
        actual_format_, actual_params = _read_corpus_parsing_parameters_string(json_string)
        self.assertEqual(expected_format_, actual_format_)
        self.assertEqual(expected_params, actual_params)
    
    #@unittest.skip
    def test_parse_corpus_document(self):
        # Test parameters
        data = get_test_parse_corpus_data()
        
        # Tests
        for (args, kwargs), expected_documents in data:
            actual_documents = parse_corpus_documents(*args, **kwargs)
            self.assertEqual(len(expected_documents), len(actual_documents))
            for expected_document, actual_document in zip(expected_documents, actual_documents):
                self.assertEqual(expected_document.ident, actual_document.ident)
                self.assertTrue(*expected_document._inner_eq(actual_document))
    
    #@unittest.skip
    def test_write_corpus(self):
        # Test parameters
        data = get_test_write_corpus_data()
        self.maxDiff = None
        
        # Test
        for (pseudo_args, pseudo_kwargs), (expected_corpus_file_content, expected_created_document_file_or_folder_names) in data:
            documents_iterable, corpus_file_name, format_, get_path = pseudo_args
            with TemporaryDirectory() as temp_dir:
                ## Set up correct parameters
                temporary_folder_path = temp_dir.temporary_folder_path
                root_folder_path = temporary_folder_path
                os.mkdir(os.path.join(root_folder_path, "documents"))
                get_path2 = (lambda document: os.path.join("documents", get_path(document))) if not isinstance(get_path, str) else os.path.join("documents", get_path)
                corpus_file_path = os.path.join(temporary_folder_path, corpus_file_name)
                args = (documents_iterable, corpus_file_path, format_, get_path2)
                kwargs = dict(pseudo_kwargs)
                kwargs["root_path"] = root_folder_path
                expected_corpus_file_path = corpus_file_path
                expected_document_object_path = tuple(os.path.join(root_folder_path, "documents", name)\
                                                      for name in expected_created_document_file_or_folder_names
                                                      )
                ## Test that expected things are not there yet
                self.assertFalse(os.path.exists(expected_corpus_file_path))
                for expected_path in expected_document_object_path:
                    self.assertFalse(os.path.exists(expected_path))
                ## Act
                write_corpus(*args, **kwargs)
                ## Test that expected things exists now
                self.assertTrue(os.path.exists(expected_corpus_file_path))
                for expected_path in expected_document_object_path:
                    self.assertTrue(os.path.exists(expected_path))
                ## Test that content of corpus is the same as what is expected
                with open(expected_corpus_file_path, "r", encoding=ENCODING) as f:
                    actual_corpus_file_content = f.read().strip()
                self.assertEqual(expected_corpus_file_content, actual_corpus_file_content)
    

def get_test_write_corpus_data():
    root_folder_path = os.path.dirname(__file__)
    data_folder_path = os.path.join(root_folder_path, "data", "corpus_io")
    
    data = []
    
    # Pivot corpus
    doc1_folder_path = os.path.join(data_folder_path, "documents", "test_document1")
    doc1 = PivotReader.parse(doc1_folder_path, strict=True)
    doc1.ident = "test_document1"
    doc2_folder_path = os.path.join(data_folder_path, "documents", "test_document2")
    doc2 = PivotReader.parse(doc2_folder_path, strict=True)
    doc2.ident = "test_document2"
    documents = (doc1, doc2)
    
    documents_iterable = iter(documents)
    corpus_file_name = "test_pivot.txt"
    format_ = "pivot"
    get_path = lambda document: document.ident
    skip_singleton = False
    strict = True
    verbose = True
    pseudo_args = (documents_iterable, corpus_file_name, format_, get_path)
    pseudo_kwargs ={"skip_singleton": skip_singleton, "strict": strict, "verbose": verbose}
    
    expected_corpus_file_content = """{"format_": "pivot"}
documents/test_document1
documents/test_document2"""
    expected_document_created_file_or_folder_names = (os.path.join("test_document1", "coreference_partition.txt"), 
                                                      os.path.join("test_document1", "info.txt"), 
                                                      os.path.join("test_document1", "mentions_synthesis.xml"), 
                                                      os.path.join("test_document1", "mentions.xml"), 
                                                      os.path.join("test_document1", "named_entities.xml"), 
                                                      os.path.join("test_document1", "quotations.xml"), 
                                                      os.path.join("test_document1", "raw.txt"), 
                                                      os.path.join("test_document1", "sentences.xml"), 
                                                      os.path.join("test_document1", "tokens.xml"), 
                                                      os.path.join("test_document2", "coreference_partition.txt"), 
                                                      os.path.join("test_document2", "info.txt"), 
                                                      os.path.join("test_document2", "mentions_synthesis.xml"), 
                                                      os.path.join("test_document2", "mentions.xml"), 
                                                      os.path.join("test_document2", "named_entities.xml"), 
                                                      os.path.join("test_document2", "quotations.xml"), 
                                                      os.path.join("test_document2", "raw.txt"), 
                                                      os.path.join("test_document2", "sentences.xml"), 
                                                      os.path.join("test_document2", "tokens.xml"), 
                                                      )
    
    data.append( ((pseudo_args, pseudo_kwargs), (expected_corpus_file_content, expected_document_created_file_or_folder_names)) )
    
    # CONLL2012Document corpus
    doc1_folder_path = os.path.join(data_folder_path, "documents", "test_not_skip_singleton.conll2012")
    doc1 = CONLL2012DocumentReader.parse(doc1_folder_path)
    doc1.ident = "test_not_skip_singleton"
    doc2_folder_path = os.path.join(data_folder_path, "documents", "test_skip_singleton.conll2012")
    doc2 = CONLL2012DocumentReader.parse(doc2_folder_path)
    doc2.ident = "test_skip_singleton"
    documents = (doc1, doc2)
    
    documents_iterable = iter(documents)
    corpus_file_name = "test_conll2012.txt"
    format_ = "conll2012"
    get_path = lambda document: document.ident
    skip_singleton = False
    strict = True
    verbose = True
    pseudo_args = (documents_iterable, corpus_file_name, format_, get_path)
    pseudo_kwargs ={"skip_singleton": skip_singleton, "strict": strict, "verbose": verbose}
    
    expected_corpus_file_content = """{"format_": "conll2012"}
documents/test_not_skip_singleton.conll2012
documents/test_skip_singleton.conll2012"""
    expected_document_created_file_or_folder_names = ("test_not_skip_singleton.conll2012", "test_skip_singleton.conll2012")
    
    data.append( ((pseudo_args, pseudo_kwargs), (expected_corpus_file_content, expected_document_created_file_or_folder_names)) )
    
    # CONLL2012MultiDocument corpus
    doc1_folder_path = os.path.join(data_folder_path, "documents", "test_not_skip_singleton_multi.multi_conll2012")
    documents = CONLL2012MultiDocumentReader.parse(doc1_folder_path)
    
    documents_iterable = iter(documents)
    corpus_file_name = "test_multiconll2012.txt"
    format_ = "multiconll2012"
    get_path = "test_not_skip_singleton_multi"
    skip_singleton = False
    strict = True
    verbose = True
    pseudo_args = (documents_iterable, corpus_file_name, format_, get_path)
    pseudo_kwargs ={"skip_singleton": skip_singleton, "strict": strict, "verbose": verbose}
    
    expected_corpus_file_content = """{"format_": "multiconll2012"}
documents/test_not_skip_singleton_multi.multi_conll2012"""
    expected_document_created_file_or_folder_names = ("test_not_skip_singleton_multi.multi_conll2012",)
    
    data.append( ((pseudo_args, pseudo_kwargs), (expected_corpus_file_content, expected_document_created_file_or_folder_names)) )
    
    return data


def get_test_parse_corpus_data():
    root_folder_path = os.path.dirname(__file__)
    data_folder_path = os.path.join(root_folder_path, "data", "corpus_io")
    corpora_folder_path = os.path.join(data_folder_path, "corpora")
    
    data = []
    
    # Pivot corpus
    corpus_file_name = "test_pivot.txt"
    corpus_file_path = os.path.join(corpora_folder_path, corpus_file_name)
    root_path = data_folder_path
    parsing_corpus_options = {"strict": True}
    verbose = True
    args = (corpus_file_path,)
    kwargs = {"root_path": root_path, "parsing_corpus_options": parsing_corpus_options, "verbose": verbose}
    doc1_folder_path = os.path.join(data_folder_path, "documents", "test_document1")
    doc1 = PivotReader.parse(doc1_folder_path, strict=True)
    doc2_folder_path = os.path.join(data_folder_path, "documents", "test_document2")
    doc2 = PivotReader.parse(doc2_folder_path, strict=True)
    expected_documents = (doc1, doc2)
    
    data.append(((args, kwargs), expected_documents))
    
    # CONLL2012 corpus
    corpus_file_name = "test_conll2012.txt"
    corpus_file_path = os.path.join(corpora_folder_path, corpus_file_name)
    root_path = data_folder_path
    parsing_corpus_options = None#{"glop": True}
    verbose = True
    args = (corpus_file_path,)
    kwargs = {"root_path": root_path, "parsing_corpus_options": parsing_corpus_options, "verbose": verbose}
    doc1_file_path = os.path.join(data_folder_path, "documents", "test_not_skip_singleton.conll2012")
    doc1 = CONLL2012DocumentReader.parse(doc1_file_path)
    doc2_file_path = os.path.join(data_folder_path, "documents", "test_skip_singleton.conll2012")
    doc2 = CONLL2012DocumentReader.parse(doc2_file_path)
    expected_documents = (doc1, doc2)
    
    data.append(((args, kwargs), expected_documents))
    
    # MultiCONLL2012 corpus
    corpus_file_name = "test_multiconll2012.txt"
    corpus_file_path = os.path.join(corpora_folder_path, corpus_file_name)
    root_path = data_folder_path
    parsing_corpus_options = None
    verbose = True
    args = (corpus_file_path,)
    kwargs = {"root_path": root_path, "parsing_corpus_options": parsing_corpus_options, "verbose": verbose}
    doc1_file_path = os.path.join(data_folder_path, "documents", "test_not_skip_singleton_multi.multi_conll2012")
    docs1 = CONLL2012MultiDocumentReader.parse(doc1_file_path)
    doc2_file_path = os.path.join(data_folder_path, "documents", "test_skip_singleton_multi.multi_conll2012")
    docs2 = CONLL2012MultiDocumentReader.parse(doc2_file_path)
    expected_documents = tuple(itertools.chain(docs1, docs2))
    
    data.append(((args, kwargs), expected_documents))
    
    # CONLLU corpus
    corpus_file_name = "test_conllu.txt"
    corpus_file_path = os.path.join(corpora_folder_path, corpus_file_name)
    root_path = data_folder_path
    parsing_corpus_options = None
    verbose = True
    args = (corpus_file_path,)
    kwargs = {"root_path": root_path, "parsing_corpus_options": parsing_corpus_options, "verbose": verbose}
    doc1_file_path = os.path.join(data_folder_path, "documents", "test1.conllu")
    doc1 = CONLLUDocumentReader.parse(doc1_file_path)
    doc1.ident = "documents/test1.conllu"
    doc2_file_path = os.path.join(data_folder_path, "documents", "test2.conllu")
    doc2 = CONLLUDocumentReader.parse(doc2_file_path)
    doc2.ident = "documents/test2.conllu"
    expected_documents = (doc1, doc2)
    
    data.append(((args, kwargs), expected_documents))
    
    return data
    




"""
l1 = expected_second_document._mentions
        l2 = actual_second_document._mentions
        i = 0
        m1 = l1[i]
        m2 = l2[i]
        
        attribute_name = "head_extent"
        attr1 = getattr(m1, attribute_name)
        attr2 = getattr(m2, attribute_name)
        print(attr1)
        print(attr2)
        #result3, message3 = attr1._inner_eq(attr2)
        #self.assertTrue(result3, message3)
        result2, message2 = m1._inner_eq(m2)
        self.assertTrue(result2, message2)
        """
"""
l1 = expected_document.sentences
        l2 = actual_document.sentences
        i = 1
        m1 = l1[i]
        m2 = l2[i]
        attr_names = ["ident", "_tokens", "_mentions", "constituency_tree", "predicate_arguments", "speaker"]
        for name in attr_names:
            print(getattr(m1, name))
            print(getattr(m2, name))
        self.assertEqual(m1, m2)
        self.assertEqual(l1, l2)
"""

"""
c1 = expected_document.coreference_partition
        c2 = actual_document.coreference_partition
        self.assertEqual(c1._mention_to_entity.keys(), c2._mention_to_entity.keys())
        #print(list(c1))
        #print(list(c2))
        for m in sorted(c1._mention_to_entity.keys()):
            e1 = c1._mention_to_entity[m]
            e2 = c2._mention_to_entity[m]
            print("'{}'  =>  '{}'".format(m, e1))
            print("'{}'  =>  '{}'".format(m, e2))
            self.assertEqual(e1, e2)
            print()
        
        self.assertEqual(c1, c2)
        """

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()