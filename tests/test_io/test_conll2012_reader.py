# -*- coding: utf-8 -*-

import unittest
import os
from collections import OrderedDict

from cortex.parameters import LRB_cortex_symbol, RRB_cortex_symbol
from cortex.io.conll2012_reader import (CONLL2012DocumentReader, CONLL2012MultiDocumentReader, 
                                        OverlappingNamedEntitiesError, 
                                        OverlappingCoreferenceEntitiesError,
                                        )
from cortex.api.document import Document
from cortex.api.markable import Token, Mention, Sentence, NamedEntity
from cortex.api.constituency_tree import ConstituencyTree
from cortex.api.entity import Entity
from cortex.api.coreference_partition import CoreferencePartition
from cortex.api.document_buffer import deepcopy_document


#@unittest.skip
class TestCONLL2012DocumentReader(unittest.TestCase):
    
    #@unittest.skip
    def test_parse_from_file_path(self):
        # Test one document by directly providing its file path
        ## Test parameters & expected result
        conll2012_document_path, expected_document = get_test_data_document_parse()
        
        ## Actual result
        conll2012_document_reader = CONLL2012DocumentReader()
        actual_document = conll2012_document_reader.parse(conll2012_document_path)
        
        ## Tests
        ### Test synchronization of raw texts
        for sentence in actual_document.sentences:
            self.assertEqual(sentence.raw_text, " ".join(n.syntactic_tag for n in sentence.constituency_tree.leaves))
            self.assertEqual(sentence.raw_text, " ".join(n.token.raw_text for n in sentence.constituency_tree.leaves))
        for mention in actual_document.mentions:
            self.assertEqual(mention.raw_text, " ".join(t.raw_text for t in mention.tokens))
        
        ### Equality
        self.assertEqual(expected_document.ident, actual_document.ident)
        result, message = expected_document._inner_eq(actual_document)
        self.assertTrue(result, message)
    
    #@unittest.skip
    def test_parse_from_line_buffer(self):
        # Test one document by providing a line buffer instead
        ## Test parameters & expected result
        conll2012_document_path, expected_document = get_test_data_document_parse()
        
        ## Actual result
        conll2012_document_reader = CONLL2012DocumentReader()
        with open(conll2012_document_path, "r", encoding="utf-8") as f:
            actual_document = conll2012_document_reader.parse(f)
        
        ## Tests
        ### Test synchronization of raw texts
        for sentence in actual_document.sentences:
            self.assertEqual(sentence.raw_text, " ".join(n.syntactic_tag for n in sentence.constituency_tree.leaves))
            self.assertEqual(sentence.raw_text, " ".join(n.token.raw_text for n in sentence.constituency_tree.leaves))
        for mention in actual_document.mentions:
            self.assertEqual(mention.raw_text, " ".join(t.raw_text for t in mention.tokens))
        
        ### Equality
        self.assertEqual(expected_document.ident, actual_document.ident)
        result, message = expected_document._inner_eq(actual_document)
        self.assertTrue(result, message)
    
    #@unittest.skip
    def test_parse_text_with_parentheses(self):
        # Test parameters
        conll2012_file_content = """
32b8fb54ee7d.ann.extended	*	1	(	PUNCT	(ROOT(SENT(NP*	(	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	2	en	ADP	(PP*	en	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	3	)	PUNCT	(NP*	)	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	4	Edward	PROPN	*)))	Edward	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	5	J.	PROPN	(NP*	J.	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	6	Erickson	PROPN	*	Erickson	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	7	,	PUNCT	*	,	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	8	Ordered	PROPN	(NP*	Ordered	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	9	to	ADP	*	to	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	10	Die	DET	*))	der	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	11	:	PUNCT	*	:	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	12	A	DET	(PP*	a	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	13	History	NOUN	(NP(MWN*	history	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	14	of	ADP	*	of	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	15	the	DET	*)	the	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	16	Ottoman	ADJ	*	ottoman	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	17	Army	NOUN	(NP*	army	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	18	in	ADP	(MWN*	in	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	19	the	DET	*	the	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	20	First	ADJ	*	first	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	21	World	NOUN	*	world	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	22	War	NOUN	*))	war	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	23	,	PUNCT	*	,	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	24	Greenwood	PROPN	(NP(MWN*	Greenwood	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	25	Publishing	PROPN	*	Publishing	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	26	Group	NOUN	*)	group	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	27	,	PUNCT	*	,	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	28	2001	NUM	*	2001	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	29	[	PUNCT	*	[	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	30	détail	NOUN	*	détail	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	31	de	ADP	(PP*	de	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	32	l'	DET	(NP*	le	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	33	édition	NOUN	*)))))	édition	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	34	]	PUNCT	*	]	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	35	(	PUNCT	(NP(MWD*	(	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	36	ISBN	NOUN	*	ISBN	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	37	978	PROPN	*	978	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	38	-	PUNCT	*	-	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	39	0	NUM	*	0	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	40	-	PUNCT	*	-	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	41	313	NUM	*	313	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	42	-	PUNCT	*	-	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	43	31516	NUM	*))	31516	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	44	-	PUNCT	*	-	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	45	9	NUM	(NP*)	9	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	46	,	PUNCT	*	,	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	47	LCCN	PROPN	(NP(MWD*	LCCN	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	48	00021562	NUM	*)	00021562	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	49	)	PUNCT	(MWN*	)	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	50	(	PUNCT	*)	(	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	51	en	ADP	(PP*	en	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	52	)	PUNCT	(NP*	)	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	53	Eugene	PROPN	*	Eugene	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	54	Hinterhoff	PROPN	*	Hinterhoff	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	55	,	PUNCT	*	,	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	56	Persia	PROPN	(NP*))))	Persia	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	57	:	PUNCT	*	:	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	58	The	DET	(NP(MWN*	the	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	59	Stepping	PROPN	*	Stepping	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	60	Stone	PROPN	*	Stone	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	61	To	PART	*	to	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	62	India	PROPN	*))	India	-	-	-	*	-
32b8fb54ee7d.ann.extended	*	63	.	PUNCT	*))	.	-	-	-	*	-
"""
        conll2012_file_content = conll2012_file_content[1:]
        conll2012_reader = CONLL2012DocumentReader()
        line_iterator = iter(conll2012_file_content.split("\n"))
        
        expected_ident = "32b8fb54ee7d.ann.extended"
        expected_sentence_raw_text = "( en ) Edward J. Erickson , Ordered to Die : A History of the Ottoman Army in the First World War , Greenwood Publishing Group , 2001 [ détail de l' édition ] ( ISBN 978 - 0 - 313 - 31516 - 9 , LCCN 00021562 ) ( en ) Eugene Hinterhoff , Persia : The Stepping Stone To India ."
        expected_constituency_tree_leaves_raw_text = expected_sentence_raw_text
        s = "(ROOT (SENT (NP (PUNCT {LRB:}) (PP (ADP en) (NP (PUNCT {RRB:}) (PROPN Edward)))) (NP (PROPN J.) (PROPN Erickson) (PUNCT ,) (NP (PROPN Ordered) (ADP to) (DET Die))) (PUNCT :) (PP (DET A) (NP (MWN (NOUN History) (ADP of) (DET the)) (ADJ Ottoman) (NP (NOUN Army) (MWN (ADP in) (DET the) (ADJ First) (NOUN World) (NOUN War))) (PUNCT ,) (NP (MWN (PROPN Greenwood) (PROPN Publishing) (NOUN Group)) (PUNCT ,) (NUM 2001) (PUNCT [) (NOUN détail) (PP (ADP de) (NP (DET l') (NOUN édition)))))) (PUNCT ]) (NP (MWD (PUNCT {LRB:}) (NOUN ISBN) (PROPN 978) (PUNCT -) (NUM 0) (PUNCT -) (NUM 313) (PUNCT -) (NUM 31516))) (PUNCT -) (NP (NUM 9)) (PUNCT ,) (NP (MWD (PROPN LCCN) (NUM 00021562)) (MWN (PUNCT {RRB:}) (PUNCT {LRB:})) (PP (ADP en) (NP (PUNCT {RRB:}) (PROPN Eugene) (PROPN Hinterhoff) (PUNCT ,) (NP (PROPN Persia))))) (PUNCT :) (NP (MWN (DET The) (PROPN Stepping) (PROPN Stone) (PART To) (PROPN India))) (PUNCT .)))"
        expected_constituency_tree_string_representation = s.format(LRB=LRB_cortex_symbol, RRB=RRB_cortex_symbol)
        
        # Test
        document = conll2012_reader.parse(line_iterator)
        sentence = document.sentences[0]
        actual_sentence_raw_text = sentence.raw_text
        actual_constituency_tree_leaves_raw_text = sentence.constituency_tree.leaves_raw_text
        actual_constituency_tree_string_representation = sentence.constituency_tree.string_representation
        self.assertEqual(expected_ident, document.ident)
        self.assertEqual(expected_sentence_raw_text, actual_sentence_raw_text)
        self.assertEqual(expected_constituency_tree_leaves_raw_text, actual_constituency_tree_leaves_raw_text)
        self.assertEqual(expected_constituency_tree_string_representation, actual_constituency_tree_string_representation)
    
    #@unittest.skip
    def test_parse_text_with_nested_named_entities(self):
        # Test parameters
        data = []
        ## Nested named entities
        conll2012_file_content = """
0b0062b0-aa16-4bdb-bb97-f0d00d0ddf32.ann.extended.conllu    0    1    Thermes    NC    (ROOT(SENT(NP*    therme    *    *    *    (Place    (1
0b0062b0-aa16-4bdb-bb97-f0d00d0ddf32.ann.extended.conllu    0    2    de    P    (PP*    de    *    *    *    -    -
0b0062b0-aa16-4bdb-bb97-f0d00d0ddf32.ann.extended.conllu    0    3    Caracalla    NPP    (NP*)))))    Caracalla    *    *    *    (Person)|Place)    (5)|1)
"""
        conll2012_file_content = conll2012_file_content[1:]
        line_iterator = iter(conll2012_file_content.split("\n"))
        args = (line_iterator,)
        kwargs = {}
        
        expected_sentence_raw_text = "Thermes de Caracalla"
        expected_constituency_tree_leaves_raw_text = "Thermes de Caracalla"
        expected_constituency_tree_string_representation = "(ROOT (SENT (NP (NC Thermes) (PP (P de) (NP (NPP Caracalla))))))"
        expected_named_entities_data = ({"ident": "1,20", "extent": (1,20), "raw_text": "Thermes de Caracalla", "type": "Place"},
                                        {"ident": "12,20", "extent": (12,20), "raw_text": "Caracalla", "type": "Person"}
                                        )
        data.append(((args, kwargs), 
                     (expected_sentence_raw_text, expected_constituency_tree_leaves_raw_text, expected_constituency_tree_string_representation, expected_named_entities_data)
                     )
                    )
        
        ## Nested named entities with same type
        conll2012_file_content = """
0b0062b0-aa16-4bdb-bb97-f0d00d0ddf32.ann.extended.conllu    0    1    Thermes    NC    (ROOT(SENT(NP*    therme    *    *    *    (Place    (1
0b0062b0-aa16-4bdb-bb97-f0d00d0ddf32.ann.extended.conllu    0    2    de    P    (PP*    de    *    *    *    -    -
0b0062b0-aa16-4bdb-bb97-f0d00d0ddf32.ann.extended.conllu    0    3    Caracalla    NPP    (NP*)))))    Caracalla    *    *    *    (Place)|Place)    (5)|1)
"""
        conll2012_file_content = conll2012_file_content[1:]
        line_iterator = iter(conll2012_file_content.split("\n"))
        args = (line_iterator,)
        kwargs = {}
        
        expected_sentence_raw_text = "Thermes de Caracalla"
        expected_constituency_tree_leaves_raw_text = "Thermes de Caracalla"
        expected_constituency_tree_string_representation = "(ROOT (SENT (NP (NC Thermes) (PP (P de) (NP (NPP Caracalla))))))"
        expected_named_entities_data = ({"ident": "1,20", "extent": (1,20), "raw_text": "Thermes de Caracalla", "type": "Place"},
                                        {"ident": "12,20", "extent": (12,20), "raw_text": "Caracalla", "type": "Place"}
                                        )
        data.append(((args, kwargs), 
                     (expected_sentence_raw_text, expected_constituency_tree_leaves_raw_text, expected_constituency_tree_string_representation, expected_named_entities_data)
                     )
                    )
        
        # Test
        conll2012_reader = CONLL2012DocumentReader()
        for (args, kwargs), (expected_sentence_raw_text, expected_constituency_tree_leaves_raw_text, expected_constituency_tree_string_representation, expected_named_entities_data) in data:
            document = conll2012_reader.parse(*args, **kwargs)
            sentence = document.sentences[0]
            actual_sentence_raw_text = sentence.raw_text
            actual_constituency_tree_leaves_raw_text = sentence.constituency_tree.leaves_raw_text
            actual_constituency_tree_string_representation = sentence.constituency_tree.string_representation
            actual_named_entities_data = tuple(named_entity.to_dict() for named_entity in document.named_entities)
            self.assertEqual(expected_sentence_raw_text, actual_sentence_raw_text)
            self.assertEqual(expected_constituency_tree_leaves_raw_text, actual_constituency_tree_leaves_raw_text)
            self.maxDiff = None
            self.assertEqual(expected_constituency_tree_string_representation, actual_constituency_tree_string_representation)
            self.assertEqual(expected_named_entities_data, actual_named_entities_data)
    
    #@unittest.skip
    def test_parse_text_with_overlapping_named_entities(self):
        # Test parameters
        data = []
        
        ## Overlapping named entities 1
        conll2012_file_content = """
0b0062b0-aa16-4bdb-bb97-f0d00d0ddf32.ann.extended.conllu    0    1    Thermes    NC    (ROOT(SENT(NP*    therme    *    *    *    (Concept|(Place    -
0b0062b0-aa16-4bdb-bb97-f0d00d0ddf32.ann.extended.conllu    0    2    de    P    (PP*    de    *    *    *    Concept)    -
0b0062b0-aa16-4bdb-bb97-f0d00d0ddf32.ann.extended.conllu    0    3    Caracalla    NPP    (NP*)))))    Caracalla    *    *    *    Place)    -
"""
        conll2012_file_content = conll2012_file_content[1:]
        line_iterator = iter(conll2012_file_content.split("\n"))
        args = (line_iterator,)
        kwargs = {}
        data.append((args, kwargs))
        
        ## Overlapping named entities 2
        conll2012_file_content = """
0b0062b0-aa16-4bdb-bb97-f0d00d0ddf32.ann.extended.conllu    0    1    Thermes    NC    (ROOT(SENT(NP*    therme    *    *    *    (Place    -
0b0062b0-aa16-4bdb-bb97-f0d00d0ddf32.ann.extended.conllu    0    2    de    P    (PP*    de    *    *    *    -    -
0b0062b0-aa16-4bdb-bb97-f0d00d0ddf32.ann.extended.conllu    0    3    Caracalla    NPP    (NP*)))))    Caracalla    *    *    *    (Person|Place)|Person)    -
"""
        conll2012_file_content = conll2012_file_content[1:]
        line_iterator = iter(conll2012_file_content.split("\n"))
        args = (line_iterator,)
        kwargs = {}
        data.append((args, kwargs))
        
        # Test
        conll2012_reader = CONLL2012DocumentReader()
        for (args, kwargs) in data:
            with self.assertRaises(OverlappingNamedEntitiesError):
                _ = conll2012_reader.parse(*args, **kwargs) #document
    '''
    #@unittest.skip
    def test_parse_text_with_overlapping_coreference_entities(self):
        # Test parameters
        data = []
        
        ## Overlapping named entities 1
        conll2012_file_content = """
0b0062b0-aa16-4bdb-bb97-f0d00d0ddf32.ann.extended.conllu    0    1    Thermes    NC    (ROOT(SENT(NP*    therme    *    *    *    -    (5|(1
0b0062b0-aa16-4bdb-bb97-f0d00d0ddf32.ann.extended.conllu    0    2    de    P    (PP*    de    *    *    *    -    5)
0b0062b0-aa16-4bdb-bb97-f0d00d0ddf32.ann.extended.conllu    0    3    Caracalla    NPP    (NP*)))))    Caracalla    *    *    *    -    1)
"""
        conll2012_file_content = conll2012_file_content[1:]
        line_iterator = iter(conll2012_file_content.split("\n"))
        args = (line_iterator,)
        kwargs = {}
        data.append((args, kwargs))
        
        ## Overlapping named entities 2
        conll2012_file_content = """
0b0062b0-aa16-4bdb-bb97-f0d00d0ddf32.ann.extended.conllu    0    1    Thermes    NC    (ROOT(SENT(NP*    therme    *    *    *    -    (1
0b0062b0-aa16-4bdb-bb97-f0d00d0ddf32.ann.extended.conllu    0    2    de    P    (PP*    de    *    *    *    -    -
0b0062b0-aa16-4bdb-bb97-f0d00d0ddf32.ann.extended.conllu    0    3    Caracalla    NPP    (NP*)))))    Caracalla    *    *    *    -    (5|1)|5)
"""
        conll2012_file_content = conll2012_file_content[1:]
        line_iterator = iter(conll2012_file_content.split("\n"))
        args = (line_iterator,)
        kwargs = {}
        data.append((args, kwargs))
        
        # Test
        conll2012_reader = CONLL2012DocumentReader()
        for (args, kwargs) in data:
            with self.assertRaises(OverlappingCoreferenceEntitiesError):
                _ = conll2012_reader.parse(*args, **kwargs) #document
    '''

#@unittest.skip
class TestCONLL2012MultiDocumentReader(unittest.TestCase):

    def test_parse(self):
        # Test multidocument
        ## Test parameters & expected results
        conll2012_multi_document_path, (expected_document_nb, expected_second_document, expected_third_document) = get_test_data_multidocuments_parse()
        
        ## Actual result
        conll2012_multi_document_reader = CONLL2012MultiDocumentReader()
        actual_documents = tuple(conll2012_multi_document_reader.parse(conll2012_multi_document_path))
        actual_document_nb = len(actual_documents)
        actual_second_document = actual_documents[1]
        actual_third_document = actual_documents[2]
        
        ## Tests
        self.assertEqual(expected_document_nb, actual_document_nb)
        ### Test synchronization of raw texts
        for sentence in actual_second_document.sentences:
            self.assertEqual(sentence.raw_text, " ".join(n.syntactic_tag for n in sentence.constituency_tree.leaves))
            self.assertEqual(sentence.raw_text, " ".join(n.token.raw_text for n in sentence.constituency_tree.leaves))
        for mention in actual_second_document.mentions:
            self.assertEqual(mention.raw_text, " ".join(t.raw_text for t in mention.tokens))
        ### Equality
        #### Second document
        self.assertEqual(expected_second_document.ident, actual_second_document.ident)
        result, message = expected_second_document._inner_eq(actual_second_document)
        self.assertTrue(result, message)
        #### Third document
        self.assertEqual(expected_third_document.ident, actual_third_document.ident)
        result, message = expected_third_document._inner_eq(actual_third_document)
        self.assertTrue(result, message)
    
    def test__parse_line_iterator(self):
        # Test multidocument
        ## Test parameters & expected results
        line_iterator, (expected_document_nb, expected_second_document) = get_test_data_multidocuments__parse_line_iterator_data()
        
        ## Actual result
        conll2012_multi_document_reader = CONLL2012MultiDocumentReader()
        actual_documents = tuple(conll2012_multi_document_reader._parse_line_iterator(line_iterator))
        actual_document_nb = len(actual_documents)
        actual_second_document = actual_documents[1]
        
        ## Tests
        self.assertEqual(expected_document_nb, actual_document_nb)
        ### Test synchronization of raw texts
        for sentence in actual_second_document.sentences:
            self.assertEqual(sentence.raw_text, " ".join(n.syntactic_tag for n in sentence.constituency_tree.leaves))
            self.assertEqual(sentence.raw_text, " ".join(n.token.raw_text for n in sentence.constituency_tree.leaves))
        for mention in actual_second_document.mentions:
            self.assertEqual(mention.raw_text, " ".join(t.raw_text for t in mention.tokens))
        ### Equality
        self.assertEqual(expected_second_document.ident, actual_second_document.ident)
        result, message = expected_second_document._inner_eq(actual_second_document)
        self.assertTrue(result, message)



def get_test_data_document_parse():
    conll2012_multi_document_path, (_, expected_document, _) = get_test_data_multidocuments_parse()
    conll2012_document_folder_path, _ = os.path.split(conll2012_multi_document_path)
    conll2012_document_path = os.path.join(conll2012_document_folder_path, "test.conll2012")
    expected_document.ident = "bc/phoenix/00/phoenix_0001__part__0"
    expected_document.info = {}
    return conll2012_document_path, expected_document


def get_test_data_multidocuments_parse():
    # Test parameters
    conll2012_multi_document_path = os.path.join(os.path.dirname(__file__), "data", "conll2012_reader", 
                                             "test_correct_parse", "test_multi.multi_conll2012")
    documents_nb = 3
    
    # Expected result
    doc_ident = "bc/phoenix/00/phoenix_0001__part__0"
    raw_text = "\nIn June of 2004 , the Shanxi Yongji Peasants Association was formally "\
                        "established ; it now has 3,800 members .\nAccordingly , this is the "\
                        "first peasant organization to be legally registered and designated a "\
                        "peasant association since the founding of the PRC ."
    info = OrderedDict()
    second_document = Document(doc_ident, raw_text, info=info)
    
    ## First expected sentence
    sentences_data = []
    sentence_data = ["1", (1,110), "In June of 2004 , the Shanxi Yongji Peasants Association was formally established ; it now has 3,800 members .", 
                     "(TOP (S (S (PP (IN In) (NP (NP (NNP June)) (PP (IN of) (NP (CD 2004))))) (, ,) (NP (DT the) (NNP Shanxi) (NNP Yongji) (NNPS Peasants) (NNP Association)) (VP (VBD was) (ADVP (RB formally)) (VP (VBN established)))) (: ;) (S (NP (PRP it)) (ADVP (RB now)) (VP (VBZ has) (NP (CD 3,800) (NNS members)))) (. .)))",
                     "speaker#1", 
                     (((1, 15, "ARGM-TMP"), (19, 56, "ARG1"), (62, 69, "ARGM-MNR"), (71, 81, "V")),
                      ((85, 86, "ARG0"), (88, 90, "ARGM-TMP"), (92, 94, "V"), (96, 108, "ARG1"))
                     ),
                     [("4,15", (4,15), "June of 2004", "DATE", None, None),
                      ("19,56", (19,56), "the Shanxi Yongji Peasants Association", "ORG", None, None),
                      ("96,100", (96,100), "3,800", "CARDINAL", None, None)
                     ]
                    ]
    sentence_tokens_data = [("1-1", (1,2), "In", "In", "IN"), 
                   ("1-2", (4,7), "June", "June", "NNP"), 
                   ("1-3", (9,10), "of", "of", "IN"), 
                   ("1-4", (12,15), "2004", "2004", "CD"),
                   ("1-5", (17,17), ",", ",", ","),
                   ("1-6", (19,21), "the", "the", "DT"),
                   ("1-7", (23,28), "Shanxi", "Shanxi", "NNP"),
                   ("1-8", (30,35), "Yongji", "Yongji", "NNP"),
                   ("1-9", (37,44), "Peasants", "Peasants", "NNPS"),
                   ("1-10", (46,56), "Association", "Association", "NNP"), 
                   ("1-11", (58,60), "was", "be", "VBD"),
                   ("1-12", (62,69), "formally", "formally", "RB"),
                   ("1-13", (71,81), "established", "establish", "VBN"),
                   ("1-14", (83,83), ";", ";", ":"),
                   ("1-15", (85,86), "it", "it", "PRP"),
                   ("1-16", (88,90), "now", "now", "RB"),
                   ("1-17", (92,94), "has", "have", "VBZ"),
                   ("1-18", (96,100), "3,800", "3,800", "CD"),
                   ("1-19", (102,108), "members", "member", "NNS"),
                   ("1-20", (110,110), ".", ".", ".")]
    sentence_mentions_data = [((19,56), None, 1, "the Shanxi Yongji Peasants Association", None, None, None, None, None), 
                              ((71,81), None, 2, "established", None, None, None, None, None), 
                              ((85,86), None, 1, "it", None, None, None, None, None)]
    sentence_data.append(sentence_tokens_data)
    sentence_data.append(sentence_mentions_data)
    sentences_data.append(sentence_data)
    
    ## Second expected sentence
    sentence_data = ["2", (112,257), "Accordingly , this is the first peasant organization to be legally registered and designated a peasant association since the founding of the PRC .", 
                       "(TOP (S (ADVP (RB Accordingly)) (, ,) (NP (DT this)) (VP (VBZ is) (NP (NP (DT the) (JJ first) (NN peasant) (NN organization)) (SBAR (S (VP (TO to) (VP (VB be) (ADVP (RB legally)) (VP (VP (VBN registered)) (CC and) (VP (VBN designated) (NP (DT a) (NN peasant) (NN association)) (PP (IN since) (NP (NP (DT the) (NN founding)) (PP (IN of) (NP (DT the) (NNP PRC))))))))))))) (. .)))", 
                      "speaker#1", 
                      (((112, 122, "ARGM-DIS"), (126, 129, "ARG1"), (131, 132, "V"), (134, 255, "ARG2")),
                       ((171, 177, 'ARGM-MNR'), (179, 188, 'V')),
                       ((194, 203, 'V'), (205, 225, 'ARG1'), (227, 255, 'ARGM-TMP'))          
                      ),
                      [("138,142", (138,142), "first", "ORDINAL", None, None),
                      ("253,255", (253,255), "PRC", "GPE", None, None)
                      ]
                    ]
    sentence_tokens_data = [("2-1", (112,122), "Accordingly", "Accordingly", "RB"),
                   ("2-2", (124,124), ",", ",", ","),
                   ("2-3", (126,129), "this", "this", "DT"),
                   ("2-4", (131,132), "is", "be", "VBZ"),
                   ("2-5", (134,136), "the", "the", "DT"),
                   ("2-6", (138,142), "first", "first", "JJ"),
                   ("2-7", (144,150), "peasant", "peasant", "NN"),
                   ("2-8", (152,163), "organization", "organization", "NN"),
                   ("2-9", (165,166), "to", "to", "TO"),
                   ("2-10", (168,169), "be", "be", "VB"),
                   ("2-11", (171,177), "legally", "legally", "RB"),
                   ("2-12", (179,188), "registered", "register", "VBN"),
                   ("2-13", (190,192), "and", "and", "CC"),
                   ("2-14", (194,203), "designated", "designate", "VBN"),
                   ("2-15", (205,205), "a", "a", "DT"),
                   ("2-16", (207,213), "peasant", "peasant", "NN"),
                   ("2-17", (215,225), "association", "association", "NN"),
                   ("2-18", (227,231), "since", "since", "IN"),
                   ("2-19", (233,235), "the", "the", "DT"),
                   ("2-20", (237,244), "founding", "found", "NN"),
                   ("2-21", (246,247), "of", "of", "IN"),
                   ("2-22", (249, 251), "the", "the", "DT"),
                   ("2-23", (253,255), "PRC", "PRC", "NNP"),
                   ("2-24", (257,257), ".", ".", ".")]
    sentence_mentions_data = [((126,129), None, 1, "this", None, None, None, None, None)]
    sentence_data.append(sentence_tokens_data)
    sentence_data.append(sentence_mentions_data)
    sentences_data.append(sentence_data)
    
    ## Set the sentences' data
    expected_entities = {}
    sentences = []
    named_entities_list = []
    for sentence_data in sentences_data:
        (ident, extent, raw_text, constituency_tree_string, speaker, predicate_arguments, 
         named_entities_data, tokens_data, mentions_data) = sentence_data
        sentence = Sentence(ident, extent, second_document, raw_text=raw_text)
        
        # Mentions
        mentions = OrderedDict()
        for mention_data in mentions_data:
            (extent, head_extent, entity_id, raw_text, gram_type, gram_subtype, number, gender, 
             referential_probability) = mention_data
            ident = "{e[0]},{e[1]}".format(e=extent)
            mention = Mention(ident, extent, second_document, raw_text=raw_text)
            mention.head_extent = head_extent
            mention.gram_type, mention.gram_subtype = gram_type, gram_subtype
            mention.number, mention.gender = number, gender
            mention.referential_probability = referential_probability
            if entity_id not in expected_entities:
                entity = Entity(ident=entity_id)
                expected_entities[entity_id] = entity
            expected_entities[entity_id].add(mention.extent)
            mentions[extent] =  mention
        mentions_list = list(mentions.values())
        
        # Tokens
        tokens = OrderedDict()
        for token_data in tokens_data:
            ident, extent, raw_text, lemma, POS_tag = token_data
            token = Token(ident, extent, second_document, raw_text=raw_text)
            token.lemma = lemma
            token.POS_tag = POS_tag
            tokens[extent] = token
        tokens_list = list(tokens.values())
        
        # Named entities
        named_entities = OrderedDict()
        for named_entity_data in named_entities_data:
            ident, extent, raw_text, type_, subtype, origin = named_entity_data
            named_entity = NamedEntity(ident, extent, second_document, type_, raw_text=raw_text, 
                                       subtype=subtype, origin=origin)
            named_entities[extent] = named_entity
        named_entities_list.extend(list(named_entities.values()))
        
        # ConstituencyTree
        sentence.constituency_tree = ConstituencyTree.parse(constituency_tree_string)
        
        # Speaker
        sentence.speaker = speaker
        
        # Predicate arguments
        sentence.predicate_arguments = predicate_arguments
        
        # Synchronization tokens <-> mentions
        for (ms, me), mention in mentions.items():
            mention.tokens = list(t for (ts, te), t in tokens.items() if ms <= ts and te <= me)
            #mhs, mhe = mention.head_extent
            #mention.head_tokens = list(t for (ts, te), t in tokens.items() if mhs <= ts and te <= mhe)
        
        # Synchronization named_entities <-> mentions
        for extent, mention in mentions.items():
            mention.named_entity = named_entities.get(extent)
        
        # Synchronization tokens <-> constituency tree leaves
        assert len(sentence.constituency_tree.leaves) == len(tokens), "Arg."
        for i, node in enumerate(sentence.constituency_tree.leaves):
            token = tokens_list[i]
            node.token = token
            token.constituency_tree_node = node
        
        # Synchronization named_entities <-> tokens
        for extent, token in tokens.items():
            token.named_entity = named_entities.get(extent)
        
        # Set sentence's data
        sentence.mentions = mentions_list
        sentence.tokens = tokens_list
        #sentence.named_entities = named_entities_list
        
        # Finally
        sentences.append(sentence)
    
    ## Finally, set the second_document's data
    second_document.tokens = list(token for sentence in sentences for token in sentence.tokens)
    second_document.mentions = list(mention for sentence in sentences for mention in sentence.mentions)
    second_document.sentences = sentences
    second_document.named_entities = named_entities_list
    second_document.coreference_partition = CoreferencePartition(entities=expected_entities.values())
    
    # Create expected third document
    third_document = deepcopy_document(second_document, 
                                       copy_document_ident="bc/phoenix/00/phoenix_0001_glop", strict=False)
    
    return conll2012_multi_document_path, (documents_nb, second_document, third_document)


def get_test_data_multidocuments__parse_line_iterator_data():
    _, (_, second_document, _) = get_test_data_multidocuments_parse() #conll2012_multi_document_path
    multi_document_file_content	=	"""#begin	document	(bc/phoenix/00/phoenix_0001);	part	001
bc/phoenix/00/phoenix_0001	1	0	Were	VBD	(TOP(SQ*	be	-	3	speaker#2	*	*	-
bc/phoenix/00/phoenix_0001	1	1	there	EX	(NP*)	-	-	-	speaker#2	*	*	-
bc/phoenix/00/phoenix_0001	1	2	people	NNS	(NP(NP*)	people	-	1	speaker#2	*	(ARG1*)	-
bc/phoenix/00/phoenix_0001	1	3	who	WP	(SBAR(WHNP*)	-	-	-	speaker#2	*	(R-ARG1*)	-
bc/phoenix/00/phoenix_0001	1	4	could	MD	(S(VP*	-	-	-	speaker#2	*	(ARGM-MOD*)	-
bc/phoenix/00/phoenix_0001	1	5	not	RB	*	-	-	-	speaker#2	*	(ARGM-NEG*)	-
bc/phoenix/00/phoenix_0001	1	6	get	VB	(VP*	get	05	3	speaker#2	*	(V*)	-
bc/phoenix/00/phoenix_0001	1	7	in	RB	(ADVP*)	-	-	-	speaker#2	*	(ARG2*)	-
bc/phoenix/00/phoenix_0001	1	8	to	IN	(PP*	-	-	-	speaker#2	*	(C-ARG2*	-
bc/phoenix/00/phoenix_0001	1	9	the	DT	(NP*	-	-	-	speaker#2	*	*	(42
bc/phoenix/00/phoenix_0001	1	10	class	NN	*)))))))	class	-	3	speaker#2	*	*)	42)
bc/phoenix/00/phoenix_0001	1	11	?	.	*))	-	-	-	speaker#2	*	*	-

bc/phoenix/00/phoenix_0001	1	0	More	JJR	(TOP(S(NP(QP*	-	-	-	zheng_bing	(CARDINAL*	(ARG1*	(80
bc/phoenix/00/phoenix_0001	1	1	than	IN	*	-	-	-	zheng_bing	*	*	-
bc/phoenix/00/phoenix_0001	1	2	400	CD	*)	-	-	-	zheng_bing	*)	*	-
bc/phoenix/00/phoenix_0001	1	3	people	NNS	*)	people	-	1	zheng_bing	*	*)	80)
bc/phoenix/00/phoenix_0001	1	4	came	VBD	(VP*	come	01	1	zheng_bing	*	(V*)	-
bc/phoenix/00/phoenix_0001	1	5	to	IN	(PP*	-	-	-	zheng_bing	*	(ARG4*	-
bc/phoenix/00/phoenix_0001	1	6	the	DT	(NP*	-	-	-	zheng_bing	*	*	(42
bc/phoenix/00/phoenix_0001	1	7	class	NN	*))	class	-	3	zheng_bing	*	*)	42)
bc/phoenix/00/phoenix_0001	1	8	that	DT	(NP*	-	-	-	zheng_bing	(DATE*	(ARGM-TMP*	-
bc/phoenix/00/phoenix_0001	1	9	day	NN	*))	day	-	3	zheng_bing	*)	*)	-
bc/phoenix/00/phoenix_0001	1	10	.	.	*))	-	-	-	zheng_bing	*	*	-

#end	document
#begin	document	(bc/phoenix/00/phoenix_0001);	part	000
bc/phoenix/00/phoenix_0001	0	0	In	IN	(TOP(S(S(PP*	-	-	-	speaker#1	*	(ARGM-TMP*	*	-
bc/phoenix/00/phoenix_0001	0	1	June	NNP	(NP(NP*)	-	-	-	speaker#1	(DATE*	*	*	-
bc/phoenix/00/phoenix_0001	0	2	of	IN	(PP*	-	-	-	speaker#1	*	*	*	-
bc/phoenix/00/phoenix_0001	0	3	2004	CD	(NP*))))	-	-	-	speaker#1	*)	*)	*	-
bc/phoenix/00/phoenix_0001	0	4	,	,	*	-	-	-	speaker#1	*	*	*	-
bc/phoenix/00/phoenix_0001	0	5	the	DT	(NP*	-	-	-	speaker#1	(ORG*	(ARG1*	*	(63
bc/phoenix/00/phoenix_0001	0	6	Shanxi	NNP	*	-	-	-	speaker#1	*	*	*	-
bc/phoenix/00/phoenix_0001	0	7	Yongji	NNP	*	-	-	-	speaker#1	*	*	*	-
bc/phoenix/00/phoenix_0001	0	8	Peasants	NNPS	*	-	-	-	speaker#1	*	*	*	-
bc/phoenix/00/phoenix_0001	0	9	Association	NNP	*)	-	-	-	speaker#1	*)	*)	*	63)
bc/phoenix/00/phoenix_0001	0	10	was	VBD	(VP*	be	-	-	speaker#1	*	*	*	-
bc/phoenix/00/phoenix_0001	0	11	formally	RB	(ADVP*)	-	-	-	speaker#1	*	(ARGM-MNR*)	*	-
bc/phoenix/00/phoenix_0001	0	12	established	VBN	(VP*)))	establish	01	1	speaker#1	*	(V*)	*	(18)
bc/phoenix/00/phoenix_0001	0	13	;	:	*	-	-	-	speaker#1	*	*	*	-
bc/phoenix/00/phoenix_0001	0	14	it	PRP	(S(NP*)	-	-	-	speaker#1	*	*	(ARG0*)	(63)
bc/phoenix/00/phoenix_0001	0	15	now	RB	(ADVP*)	-	-	-	speaker#1	*	*	(ARGM-TMP*)	-
bc/phoenix/00/phoenix_0001	0	16	has	VBZ	(VP*	have	03	2	speaker#1	*	*	(V*)	-
bc/phoenix/00/phoenix_0001	0	17	3,800	CD	(NP*	-	-	-	speaker#1	(CARDINAL)	*	(ARG1*	-
bc/phoenix/00/phoenix_0001	0	18	members	NNS	*)))	member	-	1	speaker#1	*	*	*)	-
bc/phoenix/00/phoenix_0001	0	19	.	.	*))	-	-	-	speaker#1	*	*	*	-

bc/phoenix/00/phoenix_0001	0	0	Accordingly	RB	(TOP(S(ADVP*)	-	-	-	speaker#1	*	(ARGM-DIS*)	*	*	-
bc/phoenix/00/phoenix_0001	0	1	,	,	*	-	-	-	speaker#1	*	*	*	*	-
bc/phoenix/00/phoenix_0001	0	2	this	DT	(NP*)	-	-	-	speaker#1	*	(ARG1*)	*	*	(63)
bc/phoenix/00/phoenix_0001	0	3	is	VBZ	(VP*	be	01	1	speaker#1	*	(V*)	*	*	-
bc/phoenix/00/phoenix_0001	0	4	the	DT	(NP(NP*	-	-	-	speaker#1	*	(ARG2*	*	*	-
bc/phoenix/00/phoenix_0001	0	5	first	JJ	*	-	-	-	speaker#1	(ORDINAL)	*	*	*	-
bc/phoenix/00/phoenix_0001	0	6	peasant	NN	*	-	-	-	speaker#1	*	*	*	*	-
bc/phoenix/00/phoenix_0001	0	7	organization	NN	*)	organization	-	1	speaker#1	*	*	*	*	-
bc/phoenix/00/phoenix_0001	0	8	to	TO	(SBAR(S(VP*	-	-	-	speaker#1	*	*	*	*	-
bc/phoenix/00/phoenix_0001	0	9	be	VB	(VP*	be	-	-	speaker#1	*	*	*	*	-
bc/phoenix/00/phoenix_0001	0	10	legally	RB	(ADVP*)	-	-	-	speaker#1	*	*	(ARGM-MNR*)	*	-
bc/phoenix/00/phoenix_0001	0	11	registered	VBN	(VP(VP*)	register	02	1	speaker#1	*	*	(V*)	*	-
bc/phoenix/00/phoenix_0001	0	12	and	CC	*	-	-	-	speaker#1	*	*	*	*	-
bc/phoenix/00/phoenix_0001	0	13	designated	VBN	(VP*	designate	01	2	speaker#1	*	*	*	(V*)	-
bc/phoenix/00/phoenix_0001	0	14	a	DT	(NP*	-	-	-	speaker#1	*	*	*	(ARG1*	-
bc/phoenix/00/phoenix_0001	0	15	peasant	NN	*	-	-	-	speaker#1	*	*	*	*	-
bc/phoenix/00/phoenix_0001	0	16	association	NN	*)	association	-	1	speaker#1	*	*	*	*)	-
bc/phoenix/00/phoenix_0001	0	17	since	IN	(PP*	-	-	-	speaker#1	*	*	*	(ARGM-TMP*	-
bc/phoenix/00/phoenix_0001	0	18	the	DT	(NP(NP*	-	-	-	speaker#1	*	*	*	*	-
bc/phoenix/00/phoenix_0001	0	19	founding	NN	*)	found	-	-	speaker#1	*	*	*	*	-
bc/phoenix/00/phoenix_0001	0	20	of	IN	(PP*	-	-	-	speaker#1	*	*	*	*	-
bc/phoenix/00/phoenix_0001	0	21	the	DT	(NP*	-	-	-	speaker#1	*	*	*	*	-
bc/phoenix/00/phoenix_0001	0	22	PRC	NNP	*))))))))))))	-	-	-	speaker#1	(GPE)	*)	*	*)	-
bc/phoenix/00/phoenix_0001	0	23	.	.	*))	-	-	-	speaker#1	*	*	*	*	-

#end    document
"""
    line_iterator = iter(multi_document_file_content.split("\n"))
    documents_nb = 2
    return line_iterator, (documents_nb, second_document)



"""
l1 = expected_second_document._mentions
        l2 = actual_second_document._mentions
        i = 0
        m1 = l1[i]
        m2 = l2[i]
        
        attribute_name = "head_extent"
        attr1 = getattr(m1, attribute_name)
        attr2 = getattr(m2, attribute_name)
        print(attr1)
        print(attr2)
        #result3, message3 = attr1._inner_eq(attr2)
        #self.assertTrue(result3, message3)
        result2, message2 = m1._inner_eq(m2)
        self.assertTrue(result2, message2)
        """
"""
l1 = expected_document.sentences
        l2 = actual_document.sentences
        i = 1
        m1 = l1[i]
        m2 = l2[i]
        attr_names = ["ident", "_tokens", "_mentions", "constituency_tree", "predicate_arguments", "speaker"]
        for name in attr_names:
            print(getattr(m1, name))
            print(getattr(m2, name))
        self.assertEqual(m1, m2)
        self.assertEqual(l1, l2)
"""

"""
c1 = expected_document.coreference_partition
        c2 = actual_document.coreference_partition
        self.assertEqual(c1._mention_to_entity.keys(), c2._mention_to_entity.keys())
        #print(list(c1))
        #print(list(c2))
        for m in sorted(c1._mention_to_entity.keys()):
            e1 = c1._mention_to_entity[m]
            e2 = c2._mention_to_entity[m]
            print("'{}'  =>  '{}'".format(m, e1))
            print("'{}'  =>  '{}'".format(m, e2))
            self.assertEqual(e1, e2)
            print()
        
        self.assertEqual(c1, c2)
        """

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()