# -*- coding: utf-8 -*-

import unittest
from collections import OrderedDict

from cortex.io.conllu_reader import CONLLUDocumentReader
from cortex.api.document import Document
from cortex.api.markable import Token, Mention, Sentence, NamedEntity
from cortex.api.constituency_tree import ConstituencyTree
from cortex.api.entity import Entity
from cortex.api.coreference_partition import CoreferencePartition


#@unittest.skip
class TestCONLLUDocumentReader(unittest.TestCase):
    
    #@unittest.skip
    def test__parse_line_iterator(self):
        # Test one document by providing a line buffer instead
        ## Test parameters & expected result
        line_iterator, expected_document = get_test_parse_line_iterator_data()
        #print(tuple(line_iterator))
        ## Actual result
        conll_document_reader = CONLLUDocumentReader()
        actual_document = conll_document_reader._parse_line_iterator(line_iterator)
        
        ## Tests
        ### Test synchronization of raw texts
        '''
        for sentence in actual_document.sentences:
            self.assertEqual(sentence.raw_text, " ".join(n.syntactic_tag for n in sentence.constituency_tree.leaves))
            self.assertEqual(sentence.raw_text, " ".join(n.token.raw_text for n in sentence.constituency_tree.leaves))
        '''
        for mention in actual_document.mentions:
            self.assertEqual(mention.raw_text, " ".join(t.raw_text for t in mention.tokens))
        
        ### Equality
        self.assertEqual(expected_document.ident, actual_document.ident)
        result, message = expected_document._inner_eq(actual_document)
        self.assertTrue(result, message)



def get_test_parse_line_iterator_data():
    # Input
    raw_conll_line_string =\
"""1	Jean	Jean	PROPN	_	Gender=Masc|Number=Sing|NE=Pers	0	root	B:coref1	_
2	:	:	PUNCT	_	_	1	ponct	_	_
3	Que	que	PRON	_	_	4	obj	_	_
4	puis	pouvoir	VERB	_	_	1	mod_rel	_	_
5	-je	-je	PRON	_	Number=Sing|Person=1|PronType=Prs	4	suj	B:coref1	_
6	pour	pour	ADP	_	_	4	mod	_	_
7	vous	lui	PRON	_	_	6	obj	B:coref2	_
8	?	?	PUNCT	_	Number=Sing|Person=2|PronType=Prs	1	eos	_	_

1	Amélie	Amélie	PROPN	_	Gender=Fem|Number=Sing|NE=Pers	7	suj	B:coref2	_
2	:	:	PUNCT	_	_	1	ponct	_	_
3	Mon	son	DET	_	_	4	det	B:coref3	_
4	décodeur	décodeur	NOUN	_	_	1	mod	I:coref3,B:coref4	_
5	TV	TV	NOUN	_	_	4	mod	I:coref3,I:coref4	_
6	est	être	AUX	_	_	7	aux_tps	_	_
7	tombé	tomber	VERB	_	_	0	root	_	_
8	en	en	ADP	_	_	7	mod	_	_
9	panne	panne	NOUN	_	_	8	obj	_	_
10	hier	hier	ADV	_	NE=Date	7	mod	_	_
11	.	.	PUNCT	_	_	7	eos	_	_
"""
    line_iterator = iter("{}\n".format(s) for s in raw_conll_line_string.split("\n"))
    
    # Expected result
    doc_ident = "conllu_parsed"
    raw_text = "\nJean : Que puis -je pour vous ?\nAmélie : Mon décodeur TV est tombé en panne hier ."
    info = OrderedDict({"original_corpus_format": "conllu"})
    expected_document = Document(doc_ident, raw_text, info=info)
    
    ## First expected sentence
    sentences_data = []
    sentence_data = ["1", (1,31), "Jean : Que puis -je pour vous ?", 
                     "", #constituency_tree_string
                     None, # Speaker 
                     tuple(), # Predicate arguments
                     [("1,4", (1,4), "Jean", "Pers", None, "conll_corpus"),
                      ] # Named entities
                    ]
    sentence_tokens_data = [("1-1", (1,4), "Jean", "Jean", "PROPN", OrderedDict((("Gender", "Masc"), ("Number", "Sing"), ("NE", "Pers")))),
                            ("1-2", (6,6), ":", ":", "PUNCT", None),
                            ("1-3", (8,10), "Que", "que", "PRON", None),
                            ("1-4", (12,15), "puis", "pouvoir", "VERB", None),
                            ("1-5", (17,19), "-je", "-je", "PRON", OrderedDict((("Number", "Sing"), ("Person", "1"), ("PronType", "Prs")))),
                            ("1-6", (21,24), "pour", "pour", "ADP", None),
                            ("1-7", (26,29), "vous", "lui", "PRON", None),
                            ("1-8", (31,31), "?", "?", "PUNCT", OrderedDict((("Number", "Sing"), ("Person", "2"), ("PronType", "Prs")))),]
    sentence_mentions_data = [((1,4), None, 1, "Jean", None, None, None, None, None), 
                              ((17,19), None, 1, "-je", None, None, None, None, None), 
                              ((26,29), None, 2, "vous", None, None, None, None, None)]
    sentence_data.append(sentence_tokens_data)
    sentence_data.append(sentence_mentions_data)
    sentences_data.append(sentence_data)
    
    ## Second expected sentence
    sentence_data = ["2", (33,82), "Amélie : Mon décodeur TV est tombé en panne hier .", 
                       "", #constituency_tree_string
                      None, # Speaker
                      tuple(), # Predicate arguments
                      [("33,38", (33,38), "Amélie", "Pers", None, "conll_corpus"),
                       ("77,80", (77,80), "hier", "Date", None, "conll_corpus"),
                       ] # Named entities
                    ]
    sentence_tokens_data = [("2-1", (33,38), "Amélie", "Amélie", "PROPN", OrderedDict((("Gender", "Fem"), ("Number", "Sing"), ("NE", "Pers")))),
                            ("2-2", (40,40), ":", ":", "PUNCT", None),
                            ("2-3", (42,44), "Mon", "son", "DET", None),
                            ("2-4", (46,53), "décodeur", "décodeur", "NOUN", None),
                            ("2-5", (55,56), "TV", "TV", "NOUN", None),
                            ("2-6", (58,60), "est", "être", "AUX", None),
                            ("2-7", (62,66), "tombé", "tomber", "VERB", None),
                            ("2-8", (68,69), "en", "en", "ADP", None),
                            ("2-9", (71,75), "panne", "panne", "NOUN", None),
                            ("2-10", (77,80), "hier", "hier", "ADV", OrderedDict((("NE", "Date"),))),
                            ("2-11", (82,82), ".", ".", "PUNCT", None),]
    sentence_mentions_data = [((33,38), None, 2, "Amélie", None, None, None, None, None),
                              ((42,56), None, 3, "Mon décodeur TV", None, None, None, None, None),
                              ((46,56), None, 4, "décodeur TV", None, None, None, None, None),
                              ]
    sentence_data.append(sentence_tokens_data)
    sentence_data.append(sentence_mentions_data)
    sentences_data.append(sentence_data)
    
    ## Set the sentences' data
    expected_entities = {}
    sentences = []
    named_entities_list = []
    for sentence_data in sentences_data:
        (ident, extent, raw_text, constituency_tree_string, speaker, predicate_arguments, 
         named_entities_data, tokens_data, mentions_data) = sentence_data
        sentence = Sentence(ident, extent, expected_document, raw_text=raw_text)
        
        # Mentions
        mentions = OrderedDict()
        for mention_data in mentions_data:
            (extent, head_extent, entity_id, raw_text, gram_type, gram_subtype, number, gender, 
             referential_probability) = mention_data
            ident = "{e[0]},{e[1]}".format(e=extent)
            mention = Mention(ident, extent, expected_document, raw_text=raw_text)
            mention.head_extent = head_extent
            mention.gram_type, mention.gram_subtype = gram_type, gram_subtype
            mention.number, mention.gender = number, gender
            mention.referential_probability = referential_probability
            if entity_id not in expected_entities:
                entity = Entity(ident=entity_id)
                expected_entities[entity_id] = entity
            expected_entities[entity_id].add(mention.extent)
            mentions[extent] =  mention
        mentions_list = list(mentions.values())
        
        # Tokens
        tokens = OrderedDict()
        for token_data in tokens_data:
            ident, extent, raw_text, lemma, POS_tag, UD_features = token_data
            token = Token(ident, extent, expected_document, raw_text=raw_text)
            token.lemma = lemma
            token.POS_tag = POS_tag
            token.UD_features = UD_features
            tokens[extent] = token
        tokens_list = list(tokens.values())
        
        # Named entities
        named_entities = OrderedDict()
        for named_entity_data in named_entities_data:
            ident, extent, raw_text, type_, subtype, origin = named_entity_data
            named_entity = NamedEntity(ident, extent, expected_document, type_, raw_text=raw_text, 
                                       subtype=subtype, origin=origin)
            named_entities[extent] = named_entity
        named_entities_list.extend(list(named_entities.values()))
        
        # ConstituencyTree
        if constituency_tree_string != "":
            sentence.constituency_tree = ConstituencyTree.parse(constituency_tree_string)
        
        # Speaker
        sentence.speaker = speaker
        
        # Predicate arguments
        sentence.predicate_arguments = predicate_arguments
        
        # Synchronization tokens <-> mentions
        for (ms, me), mention in mentions.items():
            mention.tokens = list(t for (ts, te), t in tokens.items() if ms <= ts and te <= me)
            #mhs, mhe = mention.head_extent
            #mention.head_tokens = list(t for (ts, te), t in tokens.items() if mhs <= ts and te <= mhe)
        
        # Synchronization named_entities <-> mentions
        for extent, mention in mentions.items():
            mention.named_entity = named_entities.get(extent)
        
        # Synchronization tokens <-> constituency tree leaves
        if sentence.constituency_tree is not None:
            assert len(sentence.constituency_tree.leaves) == len(tokens), "Arg."
            for i, node in enumerate(sentence.constituency_tree.leaves):
                token = tokens_list[i]
                node.token = token
                token.constituency_tree_node = node
        
        # Synchronization named_entities <-> tokens
        for extent, token in tokens.items():
            token.named_entity = named_entities.get(extent)
        
        # Set sentence's data
        sentence.mentions = mentions_list
        sentence.tokens = tokens_list
        #sentence.named_entities = named_entities_list
        
        # Finally
        sentences.append(sentence)
    
    ## Finally, set the expected_document's data
    expected_document.tokens = list(token for sentence in sentences for token in sentence.tokens)
    expected_document.mentions = list(mention for sentence in sentences for mention in sentence.mentions)
    expected_document.sentences = sentences
    expected_document.named_entities = named_entities_list
    expected_document.coreference_partition = CoreferencePartition(entities=expected_entities.values())
    
    return line_iterator, expected_document




"""
l1 = expected_second_document._mentions
        l2 = actual_second_document._mentions
        i = 0
        m1 = l1[i]
        m2 = l2[i]
        
        attribute_name = "head_extent"
        attr1 = getattr(m1, attribute_name)
        attr2 = getattr(m2, attribute_name)
        print(attr1)
        print(attr2)
        #result3, message3 = attr1._inner_eq(attr2)
        #self.assertTrue(result3, message3)
        result2, message2 = m1._inner_eq(m2)
        self.assertTrue(result2, message2)
        """
"""
l1 = expected_document.sentences
        l2 = actual_document.sentences
        i = 1
        m1 = l1[i]
        m2 = l2[i]
        attr_names = ["ident", "_tokens", "_mentions", "constituency_tree", "predicate_arguments", "speaker"]
        for name in attr_names:
            print(getattr(m1, name))
            print(getattr(m2, name))
        self.assertEqual(m1, m2)
        self.assertEqual(l1, l2)
"""

"""
c1 = expected_document.coreference_partition
        c2 = actual_document.coreference_partition
        self.assertEqual(c1._mention_to_entity.keys(), c2._mention_to_entity.keys())
        #print(list(c1))
        #print(list(c2))
        for m in sorted(c1._mention_to_entity.keys()):
            e1 = c1._mention_to_entity[m]
            e2 = c2._mention_to_entity[m]
            print("'{}'  =>  '{}'".format(m, e1))
            print("'{}'  =>  '{}'".format(m, e2))
            self.assertEqual(e1, e2)
            print()
        
        self.assertEqual(c1, c2)
        """

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()