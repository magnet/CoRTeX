# -*- coding: utf-8 -*-

import unittest
import os
from collections import OrderedDict

from cortex.parameters import LRB_cortex_symbol, RRB_cortex_symbol
from cortex.io.stanford_xml_reader import StanfordXMLReader
from cortex.api.document import Document
from cortex.api.markable import Token, Sentence, NamedEntity
from cortex.api.constituency_tree import ConstituencyTree


#@unittest.skip
class TestStanfordXMLReader(unittest.TestCase):
    
    #@unittest.skip
    def test_parse(self):
        # Test parameters & expected results
        data = get_test_data() + get_test_with_parentheses_data()
        #data = get_test_with_parentheses_data()
        stanford_xml_reader = StanfordXMLReader()
        
        # Tests
        for args, kwargs, expected_document in data:
            # Actual result
            actual_document_buffer = stanford_xml_reader.parse(*args, **kwargs)
            actual_document = actual_document_buffer.flush()
            
            ## Test synchronization of raw texts
            for sentence in actual_document.sentences:
                self.assertEqual(sentence.raw_text, " ".join(n.syntactic_tag for n in sentence.constituency_tree.leaves))
                self.assertEqual(sentence.raw_text, " ".join(n.token.raw_text for n in sentence.constituency_tree.leaves))
            
            ## Equality
            self.assertTrue(*expected_document._inner_eq(actual_document))
    
    #@unittest.skip
    def test_parse_with_named_entities(self):
        # Test parameters & expected results
        args, kwargs, expected_document = get_test_parse_with_named_entities_data()
        stanford_xml_reader = StanfordXMLReader()
        
        # Tests
        # Actual result
        actual_document_buffer = stanford_xml_reader.parse(*args, **kwargs)
        actual_document = actual_document_buffer.flush()
        
        ## Test synchronization of raw texts
        for sentence in actual_document.sentences:
            self.assertEqual(sentence.raw_text, " ".join(n.syntactic_tag for n in sentence.constituency_tree.leaves))
            self.assertEqual(sentence.raw_text, " ".join(n.token.raw_text for n in sentence.constituency_tree.leaves))
        
        ## Equality
        self.assertTrue(*expected_document._inner_eq(actual_document))
        


def _fill_and_synchronize_content(document, sentences_data):
    sentences = []
    named_entities_list = []
    for sentence_data in sentences_data:
        (ident, extent, raw_text, constituency_tree_string, tokens_data, named_entities_data) = sentence_data
        sentence = Sentence(ident, extent, document, raw_text=raw_text)
        
        # Tokens
        tokens = OrderedDict()
        for token_data in tokens_data:
            ident, extent, raw_text, lemma, POS_tag = token_data
            token = Token(ident, extent, document, raw_text=raw_text)
            token.lemma = lemma
            token.POS_tag = POS_tag
            tokens[extent] = token
        tokens_list = list(tokens.values())
        
        # Named entities
        named_entities = OrderedDict()
        for (start_token_index, end_token_index), (ne_type, ne_subtype) in named_entities_data:
            ne_extent = (tokens_list[start_token_index-1].extent[0], tokens_list[end_token_index-1].extent[-1])
            ne_ident = "{},{}".format(*ne_extent)
            named_entity = NamedEntity(ne_ident, ne_extent, document, ne_type)
            named_entity.subtype = ne_subtype
            named_entities[ne_extent] = named_entity
            if start_token_index == end_token_index:
                tokens_list[start_token_index-1].named_entity = named_entity
        named_entities_list.extend(sorted(named_entities.values()))
        
        # ConstituencyTree
        sentence.constituency_tree = ConstituencyTree.parse(constituency_tree_string)
        
        # Synchronization tokens <-> constituency tree leaves
        assert len(sentence.constituency_tree.leaves) == len(tokens), "Arg."
        for i, node in enumerate(sentence.constituency_tree.leaves):
            token = tokens_list[i]
            node.token = token
            token.constituency_tree_node = node
        
        # Synchronization named_entities <-> tokens
        for extent, token in tokens.items():
            token.named_entity = named_entities.get(extent)
        
        # Set sentence's data
        sentence.tokens = tokens_list
        #sentence.named_entities = named_entities_list
        
        # Finally
        sentences.append(sentence)
    
    ## Finally, set the document's data
    document.tokens = list(token for sentence in sentences for token in sentence.tokens)
    document.sentences = sentences
    document.named_entities = named_entities_list

def _define_sentences_data():
    ## First expected sentence
    sentences_data = []
    sentence_data = ["1", (1,46), "Stanford University is located in California .", 
                     "(ROOT (S (NP (NNP Stanford) (NNP University)) (VP (VBZ is) (ADJP (JJ located) (PP (IN in) (NP (NNP California))))) (. .)))"
                    ]
    sentence_tokens_data = [("1-1", (1,8), "Stanford", "Stanford", "NNP"), 
                            ("1-2", (10,19), "University", "University", "NNP"),
                            ("1-3", (21,22), "is", "be", "VBZ"),
                            ("1-4", (24,30), "located", "located", "JJ"),
                            ("1-5", (32,33), "in", "in", "IN"),
                            ("1-6", (35,44), "California", "California", "NNP"),
                            ("1-7", (46,46), ".", ".", ".")
                           ]
    named_entities_data = [((1,2),("ORGANIZATION",None)), ((6,6),("LOCATION",None)),]
    sentence_data.append(sentence_tokens_data)
    sentence_data.append(named_entities_data)
    sentences_data.append(sentence_data)
    
    ## Second expected sentence
    sentence_data = ["2", (48,91), "It is a great university , founded in 1891 .", 
                     "(ROOT (S (NP (PRP It)) (VP (VBZ is) (NP (NP (DT a) (JJ great) (NN university)) (, ,) (VP (VBN founded) (PP (IN in) (NP (CD 1891)))))) (. .)))"
                    ]
    sentence_tokens_data = [("2-1", (48,49), "It", "it", "PRP"),
                            ("2-2", (51,52), "is", "be", "VBZ"),
                            ("2-3", (54,54), "a", "a", "DT"),
                            ("2-4", (56,60), "great", "great", "JJ"),
                            ("2-5", (62,71), "university", "university", "NN"),
                            ("2-6", (73,73), ",", ",", ","),
                            ("2-7", (75,81), "founded", "found", "VBN"),
                            ("2-8", (83,84), "in", "in", "IN"),
                            ("2-9", (86,89), "1891", "1891", "CD"),
                            ("2-10", (91,91), ".", ".", ".")
                           ]
    named_entities_data = [((9,9),("DATE",None)),]
    sentence_data.append(sentence_tokens_data)
    sentence_data.append(named_entities_data)
    sentences_data.append(sentence_data)
    
    return sentences_data


def get_test_data():
    # Test parameters
    expected_result_file_name = "test_file.txt.xml"
    expected_result_file_path = os.path.join(os.path.dirname(__file__), "data", 
                                             "stanford_xml_reader", expected_result_file_name)
    
    # Expected result
    data = []
    
    ## Document where raw text's sentences are separated by a whitespace
    raw_text = "\nStanford University is located in California . It is a great university , founded in 1891 ."
    sentence_sep = " "
    doc_ident = "test"
    info = OrderedDict()
    info["original_corpus_format"] = "stanford_core_nlp_xml"
    document = Document(doc_ident, raw_text, info=info)
    sentences_data = _define_sentences_data()
    _fill_and_synchronize_content(document, sentences_data)
    
    args = (expected_result_file_path,)
    kwargs = {"sentence_sep": sentence_sep}
    expected_result = document
    
    data.append((args, kwargs, expected_result))
    
    ## Document where raw text's sentences are separated by a '\n'
    raw_text = "\nStanford University is located in California .\nIt is a great university , founded in 1891 ."
    sentence_sep = "\n"
    enforce_lemma = True
    doc_ident = "test"
    info = OrderedDict()
    info["original_corpus_format"] = "stanford_core_nlp_xml"
    document = Document(doc_ident, raw_text, info=info)
    sentences_data = _define_sentences_data()
    _fill_and_synchronize_content(document, sentences_data)
    
    args = (expected_result_file_path,)
    kwargs = {"sentence_sep": sentence_sep, "enforce_lemma": enforce_lemma}
    expected_result = document
    
    data.append((args, kwargs, expected_result))
    
    return data



def _define_sentences_with_parentheses_data():
    ## First expected sentence
    sentences_data = []
    sentence_data = ["1", (1, 41), "frise de la grotte de Cussac ( Dordogne )", 
                     "(ROOT (SENT (COORD (VN (V frise)) (NP (MWP (P de)) (DET la) (NC grotte) (PP (P de) (NP (NPP Cussac) (NP (DET {}) (NPP Dordogne) (NP (DET {})))))))))".format(LRB_cortex_symbol, RRB_cortex_symbol)
                    ]
    sentence_tokens_data = [("1-1", (1, 5), "frise", None, "V"), 
                            ("1-2", (7, 8), "de", None, "P"),
                            ("1-3", (10, 11), "la", None, "DET"),
                            ("1-4", (13, 18), "grotte", None, "NC"),
                            ("1-5", (20, 21), "de", None, "P"),
                            ("1-6", (23, 28), "Cussac", None, "NPP"),
                            ("1-7", (30, 30), "(", None, "DET"),
                            ("1-8", (32, 39), "Dordogne", None, "NPP"),
                            ("1-9", (41, 41), ")", None, "DET"),
                           ]
    named_entities_data = []
    sentence_data.append(sentence_tokens_data)
    sentence_data.append(named_entities_data)
    sentences_data.append(sentence_data)
    
    return sentences_data

def get_test_with_parentheses_data():
    # Test parameters
    expected_result_file_name = "test_file_with_parentheses.txt.xml"
    expected_result_file_path = os.path.join(os.path.dirname(__file__), "data", 
                                             "stanford_xml_reader", expected_result_file_name)
    
    # Expected result
    data = []
    
    ## Document where raw text's sentences are separated by a whitespace
    raw_text = "\nfrise de la grotte de Cussac ( Dordogne )"
    sentence_sep = " "
    enforce_lemma = False
    doc_ident = "test"
    info = OrderedDict()
    info["original_corpus_format"] = "stanford_core_nlp_xml"
    document = Document(doc_ident, raw_text, info=info)
    sentences_data = _define_sentences_with_parentheses_data()
    _fill_and_synchronize_content(document, sentences_data)
    document._named_entities = None
    document.extent2named_entity = None
    
    args = (expected_result_file_path,)
    kwargs = {"sentence_sep": sentence_sep, "enforce_lemma": enforce_lemma}
    expected_result = document
    
    data.append((args, kwargs, expected_result))
    
    return data


def get_test_parse_with_named_entities_data():
    # Test parameters
    input_file_name = "test_file_named_entities.txt.xml"
    input_file_path = os.path.join(os.path.dirname(__file__), "data", "stanford_xml_reader", 
                                   input_file_name)
    
    # Expected result
    sentence_sep = " "
    enforce_lemma = False
    args = (input_file_path,)
    kwargs = {"sentence_sep": sentence_sep, "enforce_lemma": enforce_lemma}
    
    from cortex.io.pivot_reader import PivotReader
    expected_document_folder_path = os.path.join(os.path.dirname(__file__), "data", "stanford_xml_reader", 
                                                 "expected_test_file_named_entities")
    expected_document = PivotReader.parse(expected_document_folder_path)
    
    return args, kwargs, expected_document


"""
l1 = expected_document._mentions
        l2 = actual_document._mentions
        i = 0
        m1 = l1[i]
        m2 = l2[i]
        attribute_name = "named_entity"
        attr1 = getattr(m1, attribute_name)
        attr2 = getattr(m2, attribute_name)
        print(type(attr1.subtype))
        print(type(attr2.subtype))
        result3, message3 = attr1._inner_eq(attr2)
        self.assertTrue(result3, message3)
        result2, message2 = m1._inner_eq(m2)
        self.assertTrue(result2, message2)
        """
"""
l1 = expected_document.sentences
        l2 = actual_document.sentences
        i = 1
        m1 = l1[i]
        m2 = l2[i]
        attr_names = ["ident", "_tokens", "_mentions", "constituency_tree", "predicate_arguments", "speaker"]
        for name in attr_names:
            print(getattr(m1, name))
            print(getattr(m2, name))
        self.assertEqual(m1, m2)
        self.assertEqual(l1, l2)
"""

"""
c1 = expected_document.coreference_partition
        c2 = actual_document.coreference_partition
        self.assertEqual(c1._mention_to_entity.keys(), c2._mention_to_entity.keys())
        #print(list(c1))
        #print(list(c2))
        for m in sorted(c1._mention_to_entity.keys()):
            e1 = c1._mention_to_entity[m]
            e2 = c2._mention_to_entity[m]
            print("'{}'  =>  '{}'".format(m, e1))
            print("'{}'  =>  '{}'".format(m, e2))
            self.assertEqual(e1, e2)
            print()
        
        self.assertEqual(c1, c2)
        """

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()