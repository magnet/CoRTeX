#!/usr/bin/env python
# -*- coding: utf-8 -*-
 
from setuptools import setup, find_packages
from distutils.util import convert_path

ver_path = convert_path("cortex/VERSION.txt")
with open(ver_path, "r", encoding="utf-8") as f:
    __version__ = f.read().strip()

readme_content = ""
'''
import os
project_root_folder_path = os.path.dirname(__file__)
with open(os.path.join(project_root_folder_path,"README.md"), "r", encoding="utf-8") as f:
    readme_content = f.read()
'''

setup(
    name='cortex',
    version=__version__,
    packages=find_packages(),
    author="François NOYEZ",
    author_email="francois.noyez@inria.fr'",
    description="CorTeX reimplementation",
    long_description=readme_content,
    install_requires=["numpy", "scipy", "scikit-learn==0.19.1", "nltk", "config", "configparser", "cityhash", "pytest"],
    include_package_data=True, # Activate usage of the 'MANIFEST.in' file
    package_data={
        '': ['*'],
    }, 
    url="",

    classifiers=[
        #"Programming Language:: Python",
        #"Development Status:: 1 - Planning",
        #"License:: OSI Approved",
        #"Natural Language:: English",
        #"Operating System:: OS Independent",
        #"Programming Language:: Python:: 2.7",
        #"Topic:: Communications",
    ],
    #entry_points={
    #    'console_scripts': [
    #        'magneto = magnet.cli:main',
    #    ],
        # 'gui_scripts': [
        #    'magneto-gui = magneto:gui:start',
        # ]
    #},
    license="",
    test_suite='nose.collector',
    tests_require=['nose', 'pytest'],
)