# -*- coding: utf-8 -*-

"""
Defines utilities implementing knows set(s) of rules to resolve a coreference partition for a 
French document

However, no set of rules used to do this is known for the French language. 
This package exists for reason of compatibility with the current architecture of the toolbox.
"""

__all__ = ["RULES_TYPE2COREF_RESOLVE_FCT", "RULES_TYPE2EDGES_CONSTRAINTS_FCT",]

RULES_TYPE2COREF_RESOLVE_FCT = {}

RULES_TYPE2EDGES_CONSTRAINTS_FCT = {}