# -*- coding: utf-8 -*-

"""
Defines a class used to compute data and values corresponding to grammatical information, also called 
grammatical features here, from instances of the Mention class, defined for document written in French.
"""

__all__ = ["FrenchMentionFeatures", 
           "FRENCH_MENTION_FEATURES",
           ]

from ..common.mention_features import MentionFeatures

from .knowledge import lexicon

from .constituency_tree_features import FRENCH_CONSTITUENCY_TREE_NODE_FEATURES
from .parameters import modified_CC_treebank_constituent_tags as POS_tags_module
from .parameters import base_named_entities_data_tags as NE_tags_module

class FrenchMentionFeatures(MentionFeatures):
    """ A class defining the grammatical features that can be interesting to know about a mention 
    of a document written in French.
    
    Assumes that they are part of a document whose sentences are characterized by their respective 
    constituency tree, annotated with tag values of the "modified CC treebank" tags set, and that 
    each mention has been synchronized with its corresponding tokens.
    
    Attributes:
        POS_tags_module: the python module defining the constituency tree tags used to characterize 
            the constituency tree of the document which the mentions originate from (the 
            "modified_CC_treebank_constituent_tags" module)
        
        NE_tags_module: the python module defining the named entities tags used to characterize 
            those of the document which the mentions originate from (the "base_named_entities_data_tags" 
            module)
        
        lexicon: the python module defining French lexical data (ex: collection of words...) that 
            are known to be associated to definite roles, and so can be leveraged to infer grammatical 
            information.
        
        constituency_tree_node_features: a FrenchConstituencyTreeNodeFeatures instance, the one to use 
            internally, when computing features values
    """
    
    def __init__(self):
        super().__init__(POS_tags_module, NE_tags_module, lexicon, FRENCH_CONSTITUENCY_TREE_NODE_FEATURES)
    
    ## Instance methods
    def is_indefinite(self, mention):
        """ Determines whether the input Mention instance refers to an indefinite entity.
        
        Assumes that the Mention instance's gram type has been characterized; and, if mention is 
        nominal, assumes that mention has been synchronized with its characterized tokens, which 
        themselves have been synchronized with their respective ConstituencyTreeNode;
        
        Args:
            mention: a Mention instance

        Returns:
            a boolean
        """
        if self.is_expanded_pronoun(mention):
            return mention.raw_text.lower() in self.lexicon.INDEFINITE_PRONOUN_MENTIONS
        elif self.is_nominal(mention):
            token = mention.tokens[0]  # First token
            # N/A for French POS tags used for now
            '''
            # Test whether or not the first token is a cardinal number
            if token.POS_tag == self.POS_tags_module.CARDINAL_NUMBER_TAG:
                return True
            '''
            # Test whether or not the first token is an indefinite article
            if token.POS_tag == self.POS_tags_module.DETERMINER_TAG \
                 and token.raw_text.lower() in ["un", "une", "des"]:
                return True
            # Test presence of a quantifier before the head
            if token.raw_text.lower() in self.lexicon.QUANTIFIERS:
                return True
            # Test whether content before the head is a partitive
            before_head_tokens = []
            for t in mention.tokens:
                if t.extent < mention.head_extent:
                    before_head_tokens.append(t)
                else:
                    break
            if before_head_tokens:
                test_string = " ".join(t.raw_text for t in before_head_tokens) if before_head_tokens[0].POS_tag != self.POS_tags_module.DETERMINER_TAG else " ".join(t.raw_text for t in before_head_tokens[1:])
                if test_string in self.lexicon.PARTITIVES:
                    return True
        return False
    
    def is_quantified(self, mention):
        """ Determines whether the input Mention instance refers to an quantified entity.
        
        Assumes that the Mention instance's gram type has been characterized; and, if mention is 
        nominal, assume that mention has been synchronized with its characterized tokens.
        
        Args:
            mention: a Mention instance

        Returns:
            a boolean
        """
        if self.is_nominal(mention):
            token = mention.tokens[0]  # First token
            # Test presence of a quantifier before the head
            if token.raw_text.lower() in self.lexicon.QUANTIFIERS:
                return True
            # Test whether content before the head is a partitive
            before_head_tokens = []
            for t in mention.tokens:
                if t.extent < mention.head_extent:
                    before_head_tokens.append(t)
                else:
                    break
            if before_head_tokens:
                test_string = " ".join(t.raw_text for t in before_head_tokens) if before_head_tokens[0].POS_tag != self.POS_tags_module.DETERMINER_TAG else " ".join(t.raw_text for t in before_head_tokens[1:])
                if test_string in self.lexicon.PARTITIVES:
                    return True
        return False
    
    def is_referential(self, mention, referential_probability_threshold):
        """ Determines whether or not the input mention is referential.
        
        Returns True if the refrential_probability attribute value of the input Mention instance is 
        strictly superior to the input threshold value, or if its value is None.
        
        Args:
            mention: a Mention instance
            referential_probability_threshold: a float, between 0. and 1.

        Returns:
            a boolean
        """
        proba = mention.referential_probability
        if proba is None or proba > referential_probability_threshold:
            return True
        return False
    
    
    
FRENCH_MENTION_FEATURES = FrenchMentionFeatures()
