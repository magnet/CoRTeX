# -*- coding: utf-8 -*-

"""
Defines a class used to compute data and values corresponding to grammatical information, also called 
grammatical features here, from instances of the Token class defined for document written in French.
"""

__all__ = ["FrenchTokenFeatures", 
           "FRENCH_TOKEN_FEATURES",
           ]

from .knowledge import lexicon


class FrenchTokenFeatures(object):
    """ A class defining the grammatical features that can be interesting to know about a token 
    of a document written in French.
    
    Assumes that they are part of a document which was POS annotated with tags defined in the 
    "modified CC treebank" tags set.
    
    Attributes:
        POS_tags_module: the python module defining the constituency tree tags used to characterize 
            the constituency tree of the document which the mentions originate from (the 
            "modified_CC_treebank_constituent_tags" module)
    
    Notes:
        No need to call 'lower' on the raw_text when trying to match regex, as the regex used here is 
        case insensitive.
    """
    
    def __init__(self):
        # Loading the resources to be used to define the 'feature' methods
        from .parameters import modified_CC_treebank_constituent_tags as POS_tags_module
        self.POS_tags_module = POS_tags_module
    
    ## Instance methods
    ### Determiner
    def is_poss_det(self, token):
        """ Determines whether ot not the input token corresponds to a possessive determiner.
        
        Args:
            token: a Token instance.

        Returns:
            a boolean
        """
        if token.POS_tag != self.POS_tags_module.DETERMINER_TAG:
            return False
        UD_features = token.UD_features
        return UD_features is not None and UD_features.get("Poss") == "Yes"
    def is_def_det(self, token): # FIXME: use lemma
        """ Determines whether ot not the input token corresponds to a definite determiner.
        
        Args:
            token: a Token instance

        Returns:
            a boolean
        """
        return token.POS_tag == self.POS_tags_module.DETERMINER_TAG \
            and lexicon.DEF_DET_REGEX.match(token.raw_text) is not None
    def is_indef_det(self, token):
        """ Determines whether ot not the input token corresponds to an indefinite determiner.
            token: a Token instance

        Returns:
            boolean
        """
        return token.POS_tag == self.POS_tags_module.DETERMINER_TAG \
            and lexicon.INDEF_DET_REGEX.match(token.raw_text) is not None
    def is_dem(self, token): # det or pro
        """ Determines whether ot not the input token corresponds to a demonstrative determiner or 
        pronoun.
            token: a Token instance

        Returns:
            boolean
        """
        return lexicon.DEM_PRO_REGEX.match(token.raw_text) is not None
    
    ### Pronoun
    def is_pers_pro(self, token):
        """ Determines whether ot not the input token corresponds to a personal pronoun.
        
        Args:
            token: a Token instance

        Returns:
            a boolean
        """
        return token.POS_tag in self.POS_tags_module.PERSONAL_PRONOUN_TAGS
    def is_expand_poss_pro(self, token):
        """ Determines whether ot not the input token corresponds to an expanded possessive pronoun.
        
        Args:
            token: a Token instance

        Returns:
            a boolean
        """
        return (token.POS_tag == self.POS_tags_module.FULL_PRONOUN_TAG \
                and token.raw_text.lower() in lexicon.EXPANDED_POSSESSIVE_PRONOUNS) or self.is_poss_det(token)
    
    ### Noun
    def is_common_noun(self, token):
        """ Determine whether ot not the input token corresponds to a common noun.
        
        Args:
            token: a Token instance

        Returns:
            boolean
        """
        return token.POS_tag in self.POS_tags_module.COMMON_NOUN_POS_TAGS
    def is_name(self, token):
        """ Determine whether ot not the input token corresponds to a proper name (proper noun).
        
        Args:
            token: a Token instance

        Returns:
            boolean
        """
        return token.POS_tag in self.POS_tags_module.NAME_POS_TAGS
    def is_noun(self, token):
        """ Determines whether ot not the input token corresponds to a noun (either proper or common).
        
        Args:
            token: a Token instance

        Returns:
            a boolean
        """
        return token.POS_tag in self.POS_tags_module.NOUN_POS_TAGS
    
    ### Verb
    def is_verb(self, token):
        """ Determines whether ot not the input token corresponds to a verb form.
        
        Args:
            token: a Token instance

        Returns:
            a boolean
        """
        return token.POS_tag in self.POS_tags_module.VERBS_POS_TAGS

    ### Misc.
    def is_CD(self, token):
        """ Determines whether ot not the input token corresponds to a cardinal.
        
        Args:
            token: a Token instance

        Returns:
            a boolean
        """
        UD_features = token.UD_features
        if UD_features is not None and UD_features.get("NumType") == "Card":
            return True
        return False

FRENCH_TOKEN_FEATURES = FrenchTokenFeatures()
