# -*- coding: utf-8 -*-

"""
Defines utilities used to find the head child node of a ConstituencyTreeNode instance that was 
instantiated from French document data

Only compatible with tagset used by the Stanford Core NLP French models of June 2017, using what is 
referred to as 'modified CC' tagset:
    - A (adjective)
    - ADV (adverb)
    - C (conjunction and subordinating conjunction)
    - CL (clitics)
    - CS (subordinating conjunction) but occurs only once!
    - D (determiner)
    - ET (foreign word)
    - I (interjection)
    - N (noun)
    - P (preposition)
    - PREF (prefix)
    - PRO (strong pronoun -- very confusing)
    - V (verb)
    - PUNC (punctuation)

Head finding rules were apparently described by Ms. Ane Dybro Johansen during her master's thesis at 
Université Paris Diderot in 2004.

Could not get a hold of this master's thesis, so instead looked at the stanford core nlp's code.

At the time of implementation, this code is located at: 
https://github.com/stanfordnlp/CoreNLP/blob/master/src/edu/stanford/nlp/trees/international/french/DybroFrenchHeadFinder.java
. 
"""

# When comes the time to handle a tag, the aforementioned code will type to fetch the category to 
# which the tag belongs, and then compare it with the value currently looked up in the rules.
# So, should modify the CollinsHeadFinder code to implement this behavior. That means that there 
# should be a category tag for each constituent tag, not only the POS tags, but the phrasal and 
# clause tags as well.

__all__ = ["DybroJohansenHeadFinder",
           ]

from ..common.constituency_tree_node_head_finder import _BaseRulesConstituencyTreeNodeHeadFinder
from .parameters.modified_CC_treebank_constituent_tags import (CONSTITUENT_TAG2CATEGORY_TAG, 
                                                               PUNCTUATION_CATEGORY_TAG)


POSSIBLE_PRECEDENCE_DIRECTION = set(("left", "right", "leftdis", "rightdis", "leftexcept", "rightexcept"))
# A 'rule' is a collection of constituency category tags
DYBRO_JOHANSEN_HEADER_FINDER_RULES = {# "sentence"
    "ROOT": (("right", ("VN", "AP", "NP", "Srel", "VPpart", "AdP", "I", "Ssub", "VPinf", "PP", "ADV",)),
             ("right", tuple()),
            ),
    
    "SENT": (("right", ("VN", "AP", "NP", "Srel", "VPpart", "AdP", "I", "Ssub", "VPinf", "PP", "ADV",)),
             ("right", tuple()),
            ),
    
    # adjectival phrases
    "AP": (("right", ("A", "ET", "V", "ADV",)),
            ),
    
    # adverbial phrases
    "AdP": (("right", ("ADV",)),
            ("right", tuple()),
            ),
    
    # coordinated phrases
    "COORD": (("leftdis", ("C", "CC", "CS",)),
            ("left", tuple()),
            ),
    
    # noun phrases
    "NP": (("leftdis", ("N", "PRO",)),
           ("left", ("NP", "A", "AP", "I", "VPpart", "ADV", "AdP", "ET", "D",)),
          ),
    
    # prepositional phrases
    "PP": (("left", ("P",)),
           ("left", tuple()),
          ),
    
    # verbal nucleus
    "VN": (("right", ("V", "VPinf",)),
           ("right", tuple()),
          ),
    
    # infinitive clauses
    "VPinf": (("left", ("VN", "V",)),
               ("left", tuple()),
              ),
    
    # nonfinite clauses
    "VPpart": (("left", ("V", "VN",)),
               ("left", tuple()),
              ),
    
    # relative clauses
    "Srel": (("right", ("VN", "AP", "NP",)),
             ("right", tuple()),
            ),
    
    # subordinate clauses
    "Ssub": (("right", ("VN", "AP", "NP", "PP", "VPinf", "Ssub", "VPpart", "A", "ADV",)),
             ("right", tuple()),
            ),
    
    # parenthetical clauses
    "Sint": (("right", ("VN", "AP", "NP", "PP", "VPinf", "Ssub", "VPpart", "A", "ADV",)),
             ("right", tuple()),
            ),
    
    # adverbs
    "ADV": (("left", ("ADV", "PP", "P",)),
            ),
    
    # compound categories: start with MW: D, A, C, N, ADV, V, P, PRO, CL, I, ET
    "MWD": (("left", ("D",)),
            ("left", tuple()),
            ),
    "MWA": (("left", ("P",)),
            ("left", ("N",)),
            ("right", ("A",)),
            ("right", tuple()),
            ),
    "MWC": (("left", ("C", "CS",)),
            ("left", tuple()),
            ),
    "MWN": (("left", ("N", "ET",)),
            ("right", tuple()),
            ),
    "MWV": (("left", ("V",)),
            ("left", tuple()),
            ),
    "MWP": (("left", ("P", "ADV", "PRO",)),
            ("left", tuple()),
            ),
    "MWPRO": (("left", ("PRO", "CL", "N", "A",)),
              ("left", tuple()),
            ),
    "MWCL": (("left", ("CL",)),
               ("left", tuple()),
              ),
    "MWADV": (("left", ("P", "ADV",)),
              ("left", tuple()),
             ),
    "MWI": (("left", ("N", "ADV", "P",)),
            ("left", tuple()),
            ),
    "MWET": (("left", ("ET", "N",)),
             ("left", tuple()),
            ),
    
    # phrasal nodes that lacked a label.
    "NOPARSE": (("left", tuple()),
                ),
                             }


class DybroJohansenHeadFinder(_BaseRulesConstituencyTreeNodeHeadFinder):
    """ Class whose instance shall be used to find the head child node of a ConstituencyTreeNode 
    instance defined on a document written in French, using  Dybro-Johansen's rules. 
    
    Cf this module's docstring.
    
    That means the document, whose ConstituencyTreeNode instance this class will be used for, must 
    have been annotated in constituent using the "modified CC treebank" tagset.
    
    Here, a rule is a (precedence direction; rule elements) pair, 
    where:
        - 'rule elements' is a collection of rule elements, which are syntactic tag values
        - 'precedence direction' is a string, specifying the order in which to iterate over (rule element; potential head) 
          pairs, as well as how to interpret the rule, when applying a rule on a node in order to 
          determine its head child node. 
          It can take the following values:
              - 'left': iterate first over the rule elements (so, slower), and then over the potential 
                head nodes (so, faster), in the nodes' reading order. The first node which matches the rule 
                element is considered to be the head node, and the search stops.
                rule element is considered to be the head node, and the search stops.
              - 'right': iterate first over the rule elements (so, slower), and then over the potential 
                head nodes (so, faster), in the inverse of the nodes' reading order. The first node which 
                matches the rule element is considered to be the head node, and the search stops.
              - 'rightdis': iterate first over the potential head nodes (so, slower), and then over the 
                rule elements (so, faster), in the inverse of the nodes' reading order
              - 'leftexcept': iterate first over the potential head nodes (so, slower), and then over the 
                rule elements (so, faster), in the nodes' reading order. The first node which matches 
                absolutely no rule element is considered to be the head node, and the search stops.
              - 'rightexcept': iterate first over the potential head nodes (so, slower), and then over the 
                rule elements (so, faster), in the inverse of the nodes' reading order. The first node which 
                matches absolutely no rule element is considered to be the head node, and the search stops.
    
    Attributes:
        RULES: a "syntactic tag value => rule" map corresponding to the Dybro-Johansen's rules
    
    Source: https://github.com/stanfordnlp/CoreNLP/blob/master/src/edu/stanford/nlp/trees/international/french/DybroFrenchHeadFinder.java
    """
    
    _RULES = DYBRO_JOHANSEN_HEADER_FINDER_RULES
    
    @staticmethod
    def _tag_belongs_to_category(constituent_tag, category_tag):
        """ Check whether or not the input constituent tag belongs to the specified category.
            constituent_tag: string
            category_tag: string

        Returns:
            boolean
        """
        return CONSTITUENT_TAG2CATEGORY_TAG.get(constituent_tag) == category_tag
    
    @classmethod
    def _get_head_from_rule(cls, node, rule, last_resort):
        """ Find the head node of a list of children, by carrying out a linear search using the 
        provided rule.
        
        Args:
            node: a ConstituencyTreeNode instance
            rule: the rule to use
            last_resort: boolean; if True, then if the search carried out using the input rule 
                did not turn out a result, then a final, default rule will be used; if False, then if the 
                search carried out using the input rule did not turn out a result, then None will be returned.

        Returns:
            a ConstituencyTreeNode instance, or None
        """
        children = node.children
        if len(children) == 0:
            raise ValueError("The node has no children to perform the head search on.")
        
        found_head_index = 0
        found = False
        precedence_direction = rule[0]
        rule_category_tags = rule[1]
        if precedence_direction not in POSSIBLE_PRECEDENCE_DIRECTION:
            raise ValueError("Incorrect precedence_direction value ('{}'), possible values are among: {}."\
                             .format(precedence_direction, POSSIBLE_PRECEDENCE_DIRECTION))
        
        if precedence_direction == "left":
            # Return the first node that respects at least one rule; reading first the RULES, then the children (from left to right)
            for rule_category_tag in rule_category_tags:
                for node_index, node in enumerate(children):
                    if cls._tag_belongs_to_category(node.syntactic_tag, rule_category_tag):
                        found_head_index = node_index
                        found = True
                        break
                if found:
                    break
        
        elif precedence_direction == "leftdis":
            # Return the first node that respects at least one rule; reading first the children (from left to right), then the RULES
            for node_index, node in enumerate(children):
                syntactic_tag = node.syntactic_tag
                for rule_category_tag in rule_category_tags:
                    if cls._tag_belongs_to_category(syntactic_tag, rule_category_tag):
                        found_head_index = node_index
                        found = True
                        break
                if found:
                    break
        
        elif precedence_direction == "right":
            # Return the first node that respects at least one rule; reading first the RULES, then the children (from right to left)
            for rule_category_tag in rule_category_tags:
                for node_index in range(len(children)-1, -1, -1):
                    node = children[node_index]
                    if cls._tag_belongs_to_category(node.syntactic_tag, rule_category_tag):
                        found_head_index = node_index
                        found = True
                        break
                if found:
                    break
        
        elif precedence_direction == "rightdis":
            # Return the first node that respects at least one rule; reading first the children (from right to left), then the RULES
            for node_index in range(len(children)-1, -1, -1):
                node = children[node_index]
                syntactic_tag = node.syntactic_tag
                for rule_category_tag in rule_category_tags:
                    if cls._tag_belongs_to_category(syntactic_tag, rule_category_tag):
                        found_head_index = node_index
                        found = True
                        break
                if found:
                    break
        
        elif precedence_direction == "leftexcept":
            for node_index, node in enumerate(children):
                syntactic_tag = node.syntactic_tag
                found = True
                for rule_category_tag in rule_category_tags:
                    if cls._tag_belongs_to_category(syntactic_tag, rule_category_tag):
                        found = False
                        break
                if found:
                    found_head_index = node_index
                    break
        
        elif precedence_direction == "rightexcept":
            # Return the first node index whose node respects no RULES; reading first the heads (from right to left), then the RULES
            for node_index in range(len(children)-1, -1, -1):
                node = children[node_index]
                syntactic_tag = node.syntactic_tag
                found = True
                for rule_category_tag in rule_category_tags:
                    if cls._tag_belongs_to_category(syntactic_tag, rule_category_tag):
                        found = False;
                        break
                if found:
                    found_head_index = node_index
                    break
        
        found_head = children[found_head_index]
        if not found: # What happens if our rule didn't match anything
            found_head = None
            if last_resort:
                # When parsing the sentence in the specified order, choose as head the first token whose tag does not belong to the following set.
                # Basically, anything that is not punctuation.
                category_tags = (PUNCTUATION_CATEGORY_TAG,)
                if str(precedence_direction).startswith("left"):
                    rule = ("leftexcept", category_tags)
                else:
                    rule = ("rightexcept", category_tags)
                found_head = cls._get_head_from_rule(node, rule, False)
        
        return found_head
