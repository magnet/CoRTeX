# -*- coding: utf-8 -*-

"""
Defines basic named entities types to qualify French documents.

The following named entity types are 
supported:
    - PERSON: 'Person'
    - ORGANIZATION: 'Organization' 
    - DATE: 'Date'
    - LOCATION: 'Place'
    - EVENT: 'Event'
    - WORK: 'Work'
"""

import itertools

# Types
PERSON_NAMED_ENTITY_TYPE_TAGS = ("Person",)
ORGANIZATION_NAMED_ENTITY_TYPE_TAGS = ("Organization",)
DATE_NAMED_ENTITY_TYPE_TAGS = ("Date",)
LOCATION_NAMED_ENTITY_TYPE_TAGS = ("Place",)
EVENT_NAMED_ENTITY_TYPE_TAGS = ("Event",)
WORK_NAMED_ENTITY_TYPE_TAGS = ("Work",)

PROBLEM_NAMED_ENTITY_TAGS = tuple()

# Groups of types
ANIMATES_NAMED_ENTITY_TYPE_TAGS = set(PERSON_NAMED_ENTITY_TYPE_TAGS +\
                                      ORGANIZATION_NAMED_ENTITY_TYPE_TAGS +\
                                      EVENT_NAMED_ENTITY_TYPE_TAGS)
INANIMATES_NAMED_ENTITY_TYPE_TAGS = set(LOCATION_NAMED_ENTITY_TYPE_TAGS + DATE_NAMED_ENTITY_TYPE_TAGS +\
                                        WORK_NAMED_ENTITY_TYPE_TAGS)

NAMED_ENTITY_TYPE_TAGS = set(itertools.chain(PERSON_NAMED_ENTITY_TYPE_TAGS +\
                                             ORGANIZATION_NAMED_ENTITY_TYPE_TAGS +\
                                             DATE_NAMED_ENTITY_TYPE_TAGS +\
                                             LOCATION_NAMED_ENTITY_TYPE_TAGS +\
                                             EVENT_NAMED_ENTITY_TYPE_TAGS +\
                                             WORK_NAMED_ENTITY_TYPE_TAGS)
                             )