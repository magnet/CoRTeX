# -*- coding: utf-8 -*-

"""
Defines the syntactic tags used by the French Crabbe & Candito Treebank, modified to account for multi-words.
Also include tags that may be produced by parsing tools models, such as those used by the stanford Core NLP suite.

Source (Clause level and phrase 
level):
    - 'Cross Parser Evaluation and Tagset Variation: a French Treebank Study' (http://pauillac.inria.fr/~seddah/seddah_IWPT09.pdf)
    
    OR
    
    - http://ftb.linguist.univ-paris-diderot.fr/fichiers/public/guide-constit.pdf


    Source (Word level, i.e. POS 
    tags):
    - 'Cross Parser Evaluation and Tagset Variation: a French Treebank Study' (http://pauillac.inria.fr/~seddah/seddah_IWPT09.pdf)
    
    OR
    - http://french-postaggers.tiddlyspot.com/

Ref:
    Benoît Crabbe, Marie Candito. Expériences d’analyse syntaxique statistique du français. 15ème
    conférence sur le Traitement Automatique des Langues Naturelles - TALN’08, Jun 2008, Avignon, France.
    pp.  44-54, 2008.
    
    https://hal-univ-diderot.archives-ouvertes.fr/file/index/docid/341093/filename/crabbecandi-taln2008-final.pdf
"""


###########
# Summary #
###########
############################################
# Bracket Labels # (a.k.a. Syntactic tags) #
############################################
# Clause Level
RELATIVE_CLAUSE_TAG                             = "Srel"
SUBORDINATING_CONJUNCTION_CLAUSE_TAG            = "Ssub"
INTERROGATIVE_CLAUSE_TAG                        = "Sint" # FIXME: is that the correct interpretation

SIMPLE_DECLARATIVE_CLAUSE_TAG                   = "SENT"
ROOT_TAG                                        = "ROOT"
UNKNOWN_TAG                                     = "X"
NOT_PARSED_TAG                                  = "NOPARSE" # No syntactic parsing could be carried out

SENTENCE_TAGS = set((SIMPLE_DECLARATIVE_CLAUSE_TAG, RELATIVE_CLAUSE_TAG, 
                     SUBORDINATING_CONJUNCTION_CLAUSE_TAG, INTERROGATIVE_CLAUSE_TAG))
ROOT_TAGS = set((ROOT_TAG,))

# Phrase Level
ADJECTIVE_PHRASE_TAG = "AP" # adjective phrase
ADVERB_PHRASE_TAG = "AdP" # adverb phrase
COORDINATION_PHRASE_TAG = "COORD" # coordination conjunction (or comma which has the same role)
NOUN_PHRASE_TAG = "NP" # noun phrase
PREPOSITIONAL_PHRASE_TAG = "PP" # preposition phrase (ou amalgamated pronoun phrase)
VERB_BASE_FORM_PHRASE_TAG = "VPinf" # verbal phrase whose head is a verb under its base form
VERB_PAST_PARTICIPLE_PHRASE_TAG = "VPpart" # verbal phrase whose head is a verb under it past participle form 
VERB_OTHER_PHRASE_TAG = "VN" # verbal phrase whose head is a verb under neither its base form nor its past participle form

MULTIWORD_ADJECTIVE_TAG = "MWA"
MULTIWORD_ADVERB_TAG = "MWADV"
MULTIWORD_CONJUNCTION_TAG = "MWC"
MULTIWORD_CLITIC_TAG = "MWCL"
MULTIWORD_DETERMINER_TAG = "MWD"
MULTIWORD_FOREIGN_WORD_TAG = "ET"
MULTIWORD_INTERJECTION_TAG = "MWI"
MULTIWORD_NOUN_TAG = "MWN"
MULTIWORD_PREPOSITION_TAG = "MWP" # FIXME: corrcet interpretation?
MULTIWORD_PRONOUN_TAG = "MWPRO"
MULTIWORD_VERB_TAG = "MWV"

# Word level, i.e. POS tags
ADJECTIVE_TAG                                   = "ADJ"    # adjective
ADJECTIVE_INTERROGATIVE_TAG                     = "ADJWH"    #interrogative adjective
ADVERB_TAG                                      = "ADV"    # adverb
ADVERB_INTERROGATIVE_TAG                        = "ADVWH"    # interrogative adverb
COORDINATING_CONJUNCTION_TAG                    = "CC"    # coordination conjunction
SUBORDINATING_CONJUNCTION_TAG                   = "CS"    # subordination conjunction
CLITIC_PRONOUN_OBJECT_TAG                       = "CLO"    # object clitic pronoun
CLITIC_REFLEXIVE_PRONOUN_TAG                    = "CLR"    # reflexive clitic pronoun
CLITIC_SUBJECT_PRONOUN_TAG                      = "CLS"    # subject clitic pronoun
DETERMINER_TAG                                  = "DET"    # determiner
WH_DETERMINER_TAG                               = "DETWH"    # interrogative determiner
FOREIGN_WORD_TAG                                = "ET"    # foreign word
INTERJECTION_TAG                                = "I"    # interjection
COMMON_NOUN_TAG                                 = "NC"    # common noun
PROPER_NOUN_TAG                                 = "NPP"    # proper noun
PREPOSITION_TAG                                 = "P"    # preposition
PREPOSITION_DETERMINER_AMALGAM_TAG              = "P+D"    # preposition+determiner amalgam
PREPOSITION_PRONOUN_AMALGAM_TAG                 = "P+PRO"    # preposition+pronoun amalgam
PUNCTUATION_TAG                                 = "PUNC"    # punctuation mark
FULL_PRONOUN_TAG                                = "PRO"    # full pronoun
PRONOUN_RELATIVE_TAG                            = "PROREL"    # relative pronoun
PRONOUN_INTERROGATIVE_TAG                       = "PROWH"    # interrogative pronoun
VERB_INDICATIVE_CONDITIONNAL_FORM_TAG           = "V"    # indicative or conditional verb form
VERB_IMPERATIVE_FORM_TAG                        = "VIMP"    # imperative verb form
VERB_INFINITIVE_FORM_TAG                        = "VINF"    # infinitive verb form
VERB_PAST_PARTICIPLE_FORMA_TAG                  = "VPP"    # past participle
VERB_PRESENT_PARTICIPLE_FORM_TAG                = "VPR"    # present participle
VERB_SUBJUNCTIVE_FORMA_TAG                      = "VS"    # subjunctive verb form

PREFIX_TAG                                      = "PREF"    # prefix

# Category tags, can be also found used as POS tag from time to time especially for token in multiword structures / multiword node subtrees.
# Warning: There have been occurrences where a category tag is used as a POS tag, for instance when the annotating tool does not know which specific POS tag to assign, one can guess.
ADJECTIVE_CATEGORY_TAG = "A"
ADVERB_CATEGORY_TAG = "ADV"
CONJUNCTION_CATEGORY_TAG = "C"
CLITIC_CATEGORY_TAG = "CL"
DETERMINER_CATEGORY_TAG = "D"
NOUN_CATEGORY_TAG = "N"
PREPOSITION_CATEGORY_TAG = "P"
PRONOUN_CATEGORY_TAG = "PRO"
VERB_CATEGORY_TAG = "V"
FOREIGN_WORD_CATEGORY_TAG = "ET"
INTERJECTION_CATEGORY_TAG = "I"
PUNCTUATION_CATEGORY_TAG = PUNCTUATION_TAG

PREFIX_CATEGORY_TAG = PREFIX_TAG


CONSTITUENT_TAG2CATEGORY_TAG = {# Clause tags
                                RELATIVE_CLAUSE_TAG: RELATIVE_CLAUSE_TAG,
                                SUBORDINATING_CONJUNCTION_CLAUSE_TAG: SUBORDINATING_CONJUNCTION_CLAUSE_TAG,
                                INTERROGATIVE_CLAUSE_TAG: INTERROGATIVE_CLAUSE_TAG,
                                
                                SIMPLE_DECLARATIVE_CLAUSE_TAG: SIMPLE_DECLARATIVE_CLAUSE_TAG,
                                ROOT_TAG: ROOT_TAG,
                                UNKNOWN_TAG: UNKNOWN_TAG,
                                NOT_PARSED_TAG: NOT_PARSED_TAG,
                                
                                # Phrasal tags
                                ADJECTIVE_PHRASE_TAG: ADJECTIVE_PHRASE_TAG,
                                ADVERB_PHRASE_TAG: ADVERB_PHRASE_TAG,
                                COORDINATION_PHRASE_TAG: COORDINATION_PHRASE_TAG,
                                NOUN_PHRASE_TAG: NOUN_PHRASE_TAG,
                                PREPOSITIONAL_PHRASE_TAG: PREPOSITIONAL_PHRASE_TAG,
                                VERB_BASE_FORM_PHRASE_TAG: VERB_BASE_FORM_PHRASE_TAG,
                                VERB_PAST_PARTICIPLE_PHRASE_TAG: VERB_PAST_PARTICIPLE_PHRASE_TAG,
                                VERB_OTHER_PHRASE_TAG: VERB_OTHER_PHRASE_TAG,
                                
                                MULTIWORD_ADJECTIVE_TAG: MULTIWORD_ADJECTIVE_TAG,
                                MULTIWORD_ADVERB_TAG: MULTIWORD_ADVERB_TAG,
                                MULTIWORD_CONJUNCTION_TAG: MULTIWORD_CONJUNCTION_TAG,
                                MULTIWORD_CLITIC_TAG: MULTIWORD_CLITIC_TAG,
                                MULTIWORD_DETERMINER_TAG: MULTIWORD_DETERMINER_TAG,
                                MULTIWORD_FOREIGN_WORD_TAG: MULTIWORD_FOREIGN_WORD_TAG,
                                MULTIWORD_INTERJECTION_TAG: MULTIWORD_INTERJECTION_TAG,
                                MULTIWORD_NOUN_TAG: MULTIWORD_NOUN_TAG,
                                MULTIWORD_PREPOSITION_TAG: MULTIWORD_PREPOSITION_TAG,
                                MULTIWORD_PRONOUN_TAG: MULTIWORD_PRONOUN_TAG,
                                MULTIWORD_VERB_TAG: MULTIWORD_VERB_TAG,
                                
                                # POS tags 
                                ADJECTIVE_TAG: ADJECTIVE_CATEGORY_TAG,
                                ADJECTIVE_INTERROGATIVE_TAG: ADJECTIVE_CATEGORY_TAG,
                                ADVERB_TAG: ADVERB_CATEGORY_TAG,
                                ADVERB_INTERROGATIVE_TAG: ADVERB_CATEGORY_TAG,
                                COORDINATING_CONJUNCTION_TAG: CONJUNCTION_CATEGORY_TAG,
                                SUBORDINATING_CONJUNCTION_TAG: CONJUNCTION_CATEGORY_TAG,
                                CLITIC_PRONOUN_OBJECT_TAG: CLITIC_CATEGORY_TAG,
                                CLITIC_REFLEXIVE_PRONOUN_TAG: CLITIC_CATEGORY_TAG,
                                CLITIC_SUBJECT_PRONOUN_TAG: CLITIC_CATEGORY_TAG,
                                DETERMINER_TAG: DETERMINER_CATEGORY_TAG,
                                WH_DETERMINER_TAG: DETERMINER_CATEGORY_TAG,
                                FOREIGN_WORD_TAG: FOREIGN_WORD_CATEGORY_TAG,
                                INTERJECTION_TAG: INTERJECTION_CATEGORY_TAG,
                                COMMON_NOUN_TAG: NOUN_CATEGORY_TAG,
                                PROPER_NOUN_TAG: NOUN_CATEGORY_TAG,
                                PREPOSITION_TAG: PREPOSITION_CATEGORY_TAG,
                                PREPOSITION_DETERMINER_AMALGAM_TAG: PREPOSITION_CATEGORY_TAG,
                                PREPOSITION_PRONOUN_AMALGAM_TAG: PREPOSITION_CATEGORY_TAG,
                                PUNCTUATION_TAG: PUNCTUATION_CATEGORY_TAG,
                                FULL_PRONOUN_TAG: PRONOUN_CATEGORY_TAG,
                                PRONOUN_RELATIVE_TAG: PRONOUN_CATEGORY_TAG,
                                PRONOUN_INTERROGATIVE_TAG: PRONOUN_CATEGORY_TAG,
                                VERB_INDICATIVE_CONDITIONNAL_FORM_TAG: VERB_CATEGORY_TAG,
                                VERB_IMPERATIVE_FORM_TAG: VERB_CATEGORY_TAG,
                                VERB_INFINITIVE_FORM_TAG: VERB_CATEGORY_TAG,
                                VERB_PAST_PARTICIPLE_FORMA_TAG: VERB_CATEGORY_TAG,
                                VERB_PRESENT_PARTICIPLE_FORM_TAG: VERB_CATEGORY_TAG,
                                VERB_SUBJUNCTIVE_FORMA_TAG: VERB_CATEGORY_TAG,
                                
                                PREFIX_TAG: PREFIX_CATEGORY_TAG,
                                
                                # There have been occurrences where a category tag is used as a POS tag, so we must include them here as well (nevermind the possible redundancies of values, this is a dict)
                                ADJECTIVE_CATEGORY_TAG: ADJECTIVE_CATEGORY_TAG,
                                ADVERB_CATEGORY_TAG: ADVERB_CATEGORY_TAG,
                                CONJUNCTION_CATEGORY_TAG: CONJUNCTION_CATEGORY_TAG,
                                CLITIC_CATEGORY_TAG: CLITIC_CATEGORY_TAG,
                                DETERMINER_CATEGORY_TAG: DETERMINER_CATEGORY_TAG,
                                NOUN_CATEGORY_TAG: NOUN_CATEGORY_TAG,
                                PREPOSITION_CATEGORY_TAG: PREPOSITION_CATEGORY_TAG,
                                PRONOUN_CATEGORY_TAG: PRONOUN_CATEGORY_TAG,
                                VERB_CATEGORY_TAG: VERB_CATEGORY_TAG,
                                FOREIGN_WORD_CATEGORY_TAG: FOREIGN_WORD_CATEGORY_TAG,
                                INTERJECTION_CATEGORY_TAG: INTERJECTION_CATEGORY_TAG,
                                PUNCTUATION_CATEGORY_TAG: PUNCTUATION_CATEGORY_TAG,
                                
                                PREFIX_CATEGORY_TAG: PREFIX_CATEGORY_TAG,
                                }



######################
# Using the resource #
######################
SYNTACTIC_TAGS = set((RELATIVE_CLAUSE_TAG,
                      SUBORDINATING_CONJUNCTION_CLAUSE_TAG,
                      INTERROGATIVE_CLAUSE_TAG,
                      SIMPLE_DECLARATIVE_CLAUSE_TAG,
                      ROOT_TAG ,
                      UNKNOWN_TAG ,
                      NOT_PARSED_TAG ,
                      
                      ADJECTIVE_PHRASE_TAG,
                      ADVERB_PHRASE_TAG,
                      COORDINATION_PHRASE_TAG,
                      NOUN_PHRASE_TAG,
                      PREPOSITIONAL_PHRASE_TAG,
                      VERB_BASE_FORM_PHRASE_TAG,
                      VERB_PAST_PARTICIPLE_PHRASE_TAG,
                      VERB_OTHER_PHRASE_TAG,
                      MULTIWORD_ADJECTIVE_TAG,
                      MULTIWORD_ADVERB_TAG,
                      MULTIWORD_CONJUNCTION_TAG,
                      MULTIWORD_DETERMINER_TAG,
                      MULTIWORD_INTERJECTION_TAG,
                      MULTIWORD_NOUN_TAG,
                      MULTIWORD_PREPOSITION_TAG,
                      MULTIWORD_PRONOUN_TAG,
                      MULTIWORD_VERB_TAG,
                    ))

POS_TAGS = set((ADJECTIVE_TAG,
                ADJECTIVE_INTERROGATIVE_TAG,
                ADVERB_TAG,
                ADVERB_INTERROGATIVE_TAG,
                COORDINATING_CONJUNCTION_TAG,
                CLITIC_PRONOUN_OBJECT_TAG,
                CLITIC_REFLEXIVE_PRONOUN_TAG,
                CLITIC_SUBJECT_PRONOUN_TAG,
                SUBORDINATING_CONJUNCTION_TAG,
                DETERMINER_TAG,
                WH_DETERMINER_TAG,
                FOREIGN_WORD_TAG,
                INTERJECTION_TAG,
                COMMON_NOUN_TAG,
                PROPER_NOUN_TAG,
                PREPOSITION_TAG,
                PREPOSITION_DETERMINER_AMALGAM_TAG,
                PREPOSITION_PRONOUN_AMALGAM_TAG,
                PUNCTUATION_TAG,
                PREFIX_TAG,
                FULL_PRONOUN_TAG,
                PRONOUN_RELATIVE_TAG,
                PRONOUN_INTERROGATIVE_TAG,
                VERB_INDICATIVE_CONDITIONNAL_FORM_TAG,
                VERB_IMPERATIVE_FORM_TAG,
                VERB_INFINITIVE_FORM_TAG,
                VERB_PAST_PARTICIPLE_FORMA_TAG,
                VERB_PRESENT_PARTICIPLE_FORM_TAG,
                VERB_SUBJUNCTIVE_FORMA_TAG,
                
                # There have been occurrences where a category tag is used as a POS tag, so we must include them here as well (nevermind the possible redundancies of values, this is a dict)
                ADJECTIVE_CATEGORY_TAG,
                ADVERB_CATEGORY_TAG,
                CONJUNCTION_CATEGORY_TAG,
                CLITIC_CATEGORY_TAG,
                DETERMINER_CATEGORY_TAG,
                NOUN_CATEGORY_TAG,
                PREPOSITION_CATEGORY_TAG,
                PRONOUN_CATEGORY_TAG,
                VERB_CATEGORY_TAG,
                FOREIGN_WORD_CATEGORY_TAG,
                INTERJECTION_CATEGORY_TAG,
                PUNCTUATION_CATEGORY_TAG,
                
                PREFIX_CATEGORY_TAG,
                ))

CONSTITUENT_TAGS = SYNTACTIC_TAGS.union(POS_TAGS)

# Create lists / sets to be used to define attributes of the Tokens
NN_POS_TAGS = [COMMON_NOUN_TAG]
COMMON_NOUN_POS_TAGS = [COMMON_NOUN_TAG]
NAME_POS_TAGS = [PROPER_NOUN_TAG]
NOUN_POS_TAGS = COMMON_NOUN_POS_TAGS + NAME_POS_TAGS + [NOUN_CATEGORY_TAG]

RELATIVE_PRONOUN_POS_TAGS = set([PRONOUN_RELATIVE_TAG])

VERBS_POS_TAGS = set([VERB_INDICATIVE_CONDITIONNAL_FORM_TAG,
                      VERB_IMPERATIVE_FORM_TAG,
                      VERB_INFINITIVE_FORM_TAG,
                      VERB_PAST_PARTICIPLE_FORMA_TAG,
                      VERB_PRESENT_PARTICIPLE_FORM_TAG,
                      VERB_SUBJUNCTIVE_FORMA_TAG,
                      VERB_CATEGORY_TAG,
                      ])
PERSONAL_PRONOUN_TAGS = set([CLITIC_PRONOUN_OBJECT_TAG, CLITIC_REFLEXIVE_PRONOUN_TAG, CLITIC_SUBJECT_PRONOUN_TAG, CLITIC_CATEGORY_TAG])

NOUN_PHRASE_TAGS = set((NOUN_PHRASE_TAG,))
CLITIC_POS_TAGS = set((CLITIC_PRONOUN_OBJECT_TAG, CLITIC_REFLEXIVE_PRONOUN_TAG, CLITIC_SUBJECT_PRONOUN_TAG, CLITIC_CATEGORY_TAG,))
PREPOSITIONAL_PHRASE_TAGS = set((PREPOSITIONAL_PHRASE_TAG,))
VERB_PHRASE_TAGS = set((VERB_BASE_FORM_PHRASE_TAG, VERB_PAST_PARTICIPLE_PHRASE_TAG, VERB_OTHER_PHRASE_TAG))

# FIXME: that is a 'hack', so that the code of the 'get_NP_enum' method of the ConstituencyTreeNodeFeatures is common bot th English and French
PROPER_NOUN_SINGULAR_TAG = PROPER_NOUN_TAG
