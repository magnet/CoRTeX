# -*- coding: utf-8 -*-

"""
Defines the syntactic tags used by the French "CC" Treebank

Source (Clause level and phrase 
level):
    - 'Cross Parser Evaluation and Tagset Variation: a French Treebank Study' (http://pauillac.inria.fr/~seddah/seddah_IWPT09.pdf)
    
    OR
    
    - http://ftb.linguist.univ-paris-diderot.fr/fichiers/public/guide-constit.pdf


Source (Word level, i.e. POS 
tags):
    - 'Cross Parser Evaluation and Tagset Variation: a French Treebank Study' (http://pauillac.inria.fr/~seddah/seddah_IWPT09.pdf)
    
    OR
    - http://french-postaggers.tiddlyspot.com/

Ref:
    Benoît Crabbe, Marie Candito. Expériences d’analyse syntaxique statistique du français. 15ème
    conférence sur le Traitement Automatique des Langues Naturelles - TALN’08, Jun 2008, Avignon, France.
    pp.  44-54, 2008.
    
    https://hal-univ-diderot.archives-ouvertes.fr/file/index/docid/341093/filename/crabbecandi-taln2008-final.pdf
"""


###########
# Summary #
###########
############################################
# Bracket Labels # (a.k.a. Syntactic tags) #
############################################
# Clause Level
RELATIVE_CLAUSE_TAG                             = "Srel"
SUBORDINATING_CONJUNCTION_CLAUSE_TAG            = "Ssub"
INTERROGATIVE_CLAUSE_TAG                        = "Sint" # FIXME: is that the correct interpretation

SIMPLE_DECLARATIVE_CLAUSE_TAG                   = "SENT"
ROOT_TAG                                            = "ROOT_TAG"
UNKNOWN_TAG                                         = "X"
NOT_PARSED_TAG                          = "NOPARSE" # No syntactic parsing could be carried out
SENTENCE_TAGS = set((SIMPLE_DECLARATIVE_CLAUSE_TAG, RELATIVE_CLAUSE_TAG, 
                     SUBORDINATING_CONJUNCTION_CLAUSE_TAG, INTERROGATIVE_CLAUSE_TAG))
ROOT_TAGS = set((ROOT_TAG, "ROOT_TAG"))

# Phrase Level
ADJECTIVE_PHRASE_TAG = "AP" # adjective phrase
ADVERB_PHRASE_TAG = "AdP" # adverb phrase
COORDINATION_PHRASE_TAG = "COORD" # coordination conjunction (or comma which has the same role)
NOUN_PHRASE_TAG = "NP" # noun phrase
PREPOSITIONAL_PHRASE_TAG = "PP" # preposition phrase (ou amalgamated pronoun phrase)
VERB_BASE_FORM_PHRASE_TAG = "VPinf" # verbal phrase whose head is a verb under its base form
VERB_PAST_PARTICIPLE_PHRASE_TAG = "VPpart" # verbal phrase whose head is a verb under it past participle form 
VERB_OTHER_PHRASE_TAG = "VN" # verbal phrase whose head is a verb under neither its base form nor its past participle form


# Word level, i.e. POS tags
ADJECTIVE_TAG                                   = "ADJ"    # adjective
ADJECTIVE_INTERROGATIVE_TAG                     = "ADJWH"    #interrogative adjective
ADVERB_TAG                                      = "ADV"    # adverb
ADVERB_INTERROGATIVE_TAG                        = "ADVWH"    # interrogative adverb
COORDINATING_CONJUNCTION_TAG                    = "CC"    # coordination conjunction
CLITIC_PRONOUN_OBJECT                           = "CLO"    # object clitic pronoun
CLITIC_REFLEXIVE_PRONOUN                        = "CLR"    # reflexive clitic pronoun
CLITIC_SUBJECT_PRONOUN                          = "CLS"    # subject clitic pronoun
SUBORDINATING_CONJUNCTION_TAG                   = "CS"    # subordination conjunction
DETERMINER_TAG                                  = "DET"    # determiner
WH_DETERMINER_TAG                               = "DETWH"    # interrogative determiner
FOREIGN_WORD_TAG                                = "ET"    # foreign word
INTERJECTION_TAG                                = "I"    # interjection
COMMON_NOUN_TAG                                     = "NC"    # common noun
PROPER_NOUN_TAG                                     = "NPP"    # proper noun
PREPOSITION_TAG                                 = "P"    # preposition
PREPOSITION_DETERMINER_AMALGAM_TAG              = "P+D"    # preposition+determiner amalgam
PREPOSITION_PRONOUN_AMALGAM_TAG                 = "P+PRO"    # preposition+pronoun amalgam
PUNCTUATION_TAG                                 = "PONCT"    # punctuation mark
PREFIX_TAG                                      = "PREF"    # prefix
FULL_PRONOUN_TAG                                = "PRO"    # full pronoun
PRONOUN_RELATIVE_TAG                            = "PROREL"    # relative pronoun
PRONOUN_INTERROGATIVE_TAG                       = "PROWH"    # interrogative pronoun
VERB_INDICATIVE_CONDITIONNAL_FORM_TAG           = "V"    # indicative or conditional verb form
VERB_IMPERATIVE_FORM_TAG                        = "VIMP"    # imperative verb form
VERB_INFINITIVE_FORM_TAG                        = "VINF"    # infinitive verb form
VERB_PAST_PARTICIPLE_FORMA_TAG                  = "VPP"    # past participle
VERB_PRESENT_PARTICIPLE_FORM_TAG                = "VPR"    # present participle
VERB_SUBJUNCTIVE_FORMA_TAG                      = "VS"    # subjunctive verb form
