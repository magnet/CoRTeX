# -*- coding: utf-8 -*-

"""
Defines data that specify ways to qualify text written in French (e.g: different sets of syntactic tags...)
"""

"""
Available submodules(s)
-----------------------
CC_treebank_constituent_tags
    Defines the syntactic tags used by the French "CC" Treebank

modified_CC_treebank_constituent_tags
    Defines the syntactic tags used by the French "CC" Treebank, modified to account for multi-words

base_named_entities_data_tags
    Defines basic named entities types to qualify French documents
"""