# -*- coding: utf-8 -*-

"""
Defines data specific to the French language, and rather constant (e.g.: collection of names, etc.)
"""

"""
Available submodule(s)
-----------------------
lexicon
    Defines relevant set of French words, and association between French words
"""
