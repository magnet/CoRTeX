# -*- coding: utf-8 -*-

"""
Define relevant set of French words, and association between French words, as well as compiled 
regexes to check whether or not a string contains a certain type of word, such as personal pronouns 
for instance.

Here, EXPANDED_PRONOUN means to include also other kind of words, such as demonstrative determiners, 
that can also act as a reference to a grammatical person. 
"""

import re

######## REGEXES ########
create_regex = lambda iterable: re.compile("^(" + "|".join(iterable) + ")", re.IGNORECASE)

## Pronoun regular expressions
dict_ = {}
EXPAND_PRO_PATTERNS = ("je", "moi", "m'", "mon", "ma", "mes", "mien", "mienne", "miens", "miennes", "moi-même", 
                        "tu", "toi", "t'", "ton", "ta", "tes", "tien", "tienne", "tiens", "tiennes", "toi-même",
                        "il", "elle", "lui", "le", "la", "l'", "sien", "sienne", "siens", "siennes", "lui-même", "elle-même", "en", "y", "on",
                        "nous", "notre", "nos", "nôtre", "nôtres", "nous-même",
                        "vous", "votre", "vôtre", "vôtres", "vous-même",
                        "ils", "elles", "les","leur", "leurs", "eux", "eux-même", "elles-même", 
                        "ce", "c'", "ceci", "celui-ci", "celle-ci", "celles-ci", "ceux-ci", "cela", "ça", "celui-là", "celle-là", "ceux-là", "celles-là"
                        )

dict_["EXPAND_PRO"] = EXPAND_PRO_PATTERNS
EXPAND_PERS_PRO_PATTERNS = ("je", "moi", "m'", "mon", "ma", "mes", "mien", "mienne", "miens", "miennes", "moi-même", 
                            "tu", "toi", "t'", "ton", "ta", "tes", "tien", "tienne", "tiens", "tiennes", "toi-même",
                            "il", "elle", "lui", "le", "la", "l'", "sien", "sienne", "siens", "siennes", "lui-même", "elle-même", "en", "y", "on",
                            "nous", "notre", "nos", "nôtre", "nôtres", "nous-même",
                            "vous", "votre", "vôtre", "vôtres", "vous-même",
                            "ils", "elles", "les","leur", "leurs", "eux", "eux-même", "elles-même", 
                            )
dict_["EXPAND_PERS_PRO"] = EXPAND_PERS_PRO_PATTERNS
EXPAND_FIRST_PERS_PRO_PATTERNS = ("je", "moi", "mon", "ma", "mes", "mien", "mienne", "miens", "miennes", "moi-même", 
                                  "nous", "notre", "nos", "nôtre", "nôtres", "nous-même",)
dict_["EXPAND_FIRST_PERS_PRO"] = EXPAND_FIRST_PERS_PRO_PATTERNS 
EXPAND_SECOND_PERS_PRO_PATTERNS = ("tu", "toi", "ton", "ta", "tes", "tien", "tienne", "tiens", "tiennes", "toi-même", 
                                   "vous", "votre", "vôtre", "vôtres", "vous-même",)
dict_["EXPAND_SECOND_PERS_PRO"] = EXPAND_SECOND_PERS_PRO_PATTERNS
EXPAND_THIRD_PERS_PRO_PATTERNS = ("il", "elle", "lui", "son", "sa", "ses", "sien", "sienne", "siens", "siennes", "lui-même", "elle-même", "le", "la", "en", "y", "on",
                                  "ils", "elles", "leur", "leurs", "eux", "eux-même", "elles-même", "les")
dict_["EXPAND_THIRD_PERS_PRO"] = EXPAND_THIRD_PERS_PRO_PATTERNS
EXPAND_THIRD_SG_PRO_PATTERNS = ("il", "elle", "lui", "elle", "son", "sa", "ses", "lui-même", "elle-même", "le", "la", "en", "y", "on",)
dict_["EXPAND_THIRD_SG_PRO"] = EXPAND_THIRD_SG_PRO_PATTERNS 
EXPAND_THIRD_PL_PRO_PATTERNS = ("ils", "elles", "leur", "leurs", "eux", "eux-même", "elles-même", "les",)
dict_["EXPAND_THIRD_PL_PRO"] = EXPAND_THIRD_PL_PRO_PATTERNS

EXPAND_MASC_PRO_PATTERNS = ("il", "lui", "le", "lui-même", "ils", "eux-même") # Even though 'ils' can refer to a group consisting of females and males, here we only care about the grammatical gender.
dict_["EXPAND_MASC_PRO"] = EXPAND_MASC_PRO_PATTERNS
EXPAND_FEM_PRO_PATTERNS = ("elle", "la", "elle-même", "elles", "elles-même",) # We could be adding the possessive determiners, but in French they carry the gender of what is possessed, not of the possessor, so there is no use adding them here.
dict_["EXPAND_FEM_PRO"] = EXPAND_FEM_PRO_PATTERNS

EXPAND_SG_PRO_PATTERNS = ("je", "moi", "m'", "mon", "ma", "mes", "moi-même", "mien", "mienne", "miens", "miennes", "moi-même", 
                          "tu", "toi", "t'", "ton", "ta", "tes", "toi-même", "tien", "tienne", "tiens", "tiennes", "toi-même",
                          "il", "elle", "le", "la", "l'", "lui", "son", "sa", "ses", "lui-même", "elle-même", 
                          "ce", "c'", "ceci", "cela", "ça")
dict_["EXPAND_SG_PRO"] = EXPAND_SG_PRO_PATTERNS
EXPAND_PL_PRO_PATTERNS = ("nous", "notre", "nos", 
                             #"vous", "votre", "vos", # "vous" is a common polite form to adress someone in French, cannot rely on it alone to determine that mention is plural
                             "ils", "elles", "eux", "les", "leur", "leurs", "eux-même", "elles-même", 
                             "ces", "ceux-ci", "celles-ci", "ceux-là", "celles-là")
dict_["EXPAND_PL_PRO"] = EXPAND_PL_PRO_PATTERNS 

POSS_DET_PATTERNS = ("mon", "ma", "mes", 
                    "ton", "ta", "tes", 
                    "son", "sa", "ses", 
                    "notre", "nos", 
                    "votre", "vos", 
                    "leur", "leurs")
dict_["POSS_DET"] = POSS_DET_PATTERNS
DEM_PRO_PATTERNS = ("ce", "c'", "ceci", "cela", "ça", "celui", "celle", "celui-ci", "celle-ci", "celui-là", "celle-là", 
                    "ces", "ceux", "celles", "ceux-ci", "celles-ci", "ceux-là", "celles-là")
dict_["DEM_PRO"] = DEM_PRO_PATTERNS


## Types of determiners
DEF_DET_PATTERNS = ("le", "la", "l'")
dict_["DEF_DET"] = DEF_DET_PATTERNS
INDEF_DET_PATTERNS = ("un", "une", "des", "quelques un(e)?s", "plusieurs", "beaucoup")
dict_["INDEF_DET"] = INDEF_DET_PATTERNS


# Person title regexes
# TITLE = re.compile("^[A-Z][a-z]+\\.$|^[A-Z][b-df-hj-np-tv-xz]+",re.IGNORECASE)
MALE_TITLE_PATTERNS = ("amiral", "archevêque", "baron", "frère", "chevalier", "comte", "dark", 
                        "docteurr", "dr", "dr.", "éminence", "excellence", "père", "généralissime", 
                        "her", "herr", "son", "jonkheer", "roi", "seigneur", "marquis", "maréchal", "maître", 
                        "mister", "mon", "mon.", "monsieur", "mr", "mr.", "pasteurr", "prince", "rabbin", 
                        "raja", "rev.", "révérend", "senor", "señor", "sheik", "sheikh", "sir", "sir", 
                        "sr", "sr.", "viscomte", "entraineur", "général", "président", "vice-président", 
                        "représentant", "senateur", "sergent",)
dict_["MALE_TITLE"] = MALE_TITLE_PATTERNS
FEMALE_TITLE_PATTERNS = ("baronne", "comtesse", "dame", "lady", "madame", "marchioness", "marquise", 
                        "mata", "miss", "missus", "mrs", "ms", "princesse", "reine", "rani", "senorita", 
                        "señora", "señorita", "soeur", "sra", "sra.", "srta", "srta.", "viscomtesse", 
                        "entraineuse", "générale", "présidente", "vice-présidente", "représentante",
                        "senatrice", "sergente",)
dict_["FEMALE_TITLE"] = FEMALE_TITLE_PATTERNS
UNI_TITLE_PATTERNS = ("capt.", "capitaine", "démiurge", "gen", "gen.", "hon", 
                    "hon.", "honorable", "maj", "maj.", "major", "pres", "pres.", "prof", 
                    "prof.", "rep", "rep.", "rev", "sen", "sen.", 
                    "sgt", "sgt.", "vénérable",)
dict_["UNI_TITLE"] = UNI_TITLE_PATTERNS


# TODO: person suffix: I, II, III, PhD, M.D., ...

# Location regexes: e.g. Austin, TX
# TODO


# Finally, create the regexes
dict_2 = dict((name, create_regex(patterns)) for name, patterns in dict_.items())
del dict_

EXPAND_PRO_REGEX = dict_2["EXPAND_PRO"]
EXPAND_PERS_PRO_REGEX = dict_2["EXPAND_PERS_PRO"]
EXPAND_FIRST_PERS_PRO_REGEX = dict_2["EXPAND_FIRST_PERS_PRO"]
EXPAND_SECOND_PERS_PRO_REGEX = dict_2["EXPAND_SECOND_PERS_PRO"]
EXPAND_THIRD_PERS_PRO_REGEX = dict_2["EXPAND_THIRD_PERS_PRO"]
EXPAND_THIRD_SG_PRO_REGEX = dict_2["EXPAND_THIRD_SG_PRO"]
EXPAND_THIRD_PL_PRO_REGEX = dict_2["EXPAND_THIRD_PL_PRO"]
EXPAND_MASC_PRO_REGEX = dict_2["EXPAND_MASC_PRO"]
EXPAND_FEM_PRO_REGEX = dict_2["EXPAND_FEM_PRO"]
EXPAND_SG_PRO_REGEX = dict_2["EXPAND_SG_PRO"]
EXPAND_PL_PRO_REGEX = dict_2["EXPAND_PL_PRO"]
POSS_DET_REGEX = dict_2["POSS_DET"]
DEM_PRO_REGEX = dict_2["DEM_PRO"]
DEF_DET_REGEX = dict_2["DEF_DET"]
INDEF_DET_REGEX = dict_2["INDEF_DET"]
MALE_TITLE_REGEX = dict_2["MALE_TITLE"]
FEMALE_TITLE_REGEX = dict_2["FEMALE_TITLE"]
UNI_TITLE_REGEX = dict_2["UNI_TITLE"]

del dict_2



######## Words enumerations ########
######## PRONOUNS ########
# PRONOUNS: NUMBER
"""
We should not include things such as 'le mien', because 'le mien' may refer to a whole other object; 
only 'mien' refers to the person. Though I thought that those should be included because I saw the 
word 'mine' being included in the equivalent collection for the English language... What am I to do:0 ?
In fact, it depends on how those collections will be used, and what one can infer from the fact that
 a string belongs to it.
In English, 'mine' is ambiguous; in my opinion it can refer both to 'me' and to 'the object that 
belongs to me'. Pascal said that we should interpret it as a reference to 'me', though.
But in French there no such ambiguity: 'mien' refers to 'me', and 'le mien' refers to 'the object 
that belongs to me' Both contain an expanded pronominal structure.
Since here the collections' only use seems to be to distinguish between singular and plural expanded 
pronominal structures, this point is moot: I should just put things such as 'les miens' in the 
'plural' collection, for instance.
Hm, but I guess a mention such as 'le mien' should be a nominal mention instead of an 
'expanded_pronominal' mention, so it should not be put through the mention characterizer's method 
that uses those collections, since this method is specific to 'expanded_pronoun' mentions only...
==> We do not include things such as 'le mien'.
"""
EXPANDED_SINGULAR_PRONOUN_MENTIONS = set(["je", "me", "m’", "moi", "mon", "ma", "mes", "mien", "mienne", "miens", "miennes", "moi-même", 
                                         "tu", "te", "t'", "toi", "ton", "ta", "tes", "tien", "tienne", "tiens", "tiennes", "toi-même", 
                                         "il", "elle", "on", "se", "s'", "lui", "en", "y", "soi", "son", "sa", "ses", "sien", "sienne", "siens", "siennes", "soi-même", "lui-même", "elle-même", 
                                         "on", "soi", "soi-même", "propre",]) # FIXME: is that correct?
EXPANDED_PLURAL_PRONOUN_MENTIONS = set(["nous", "notre", "nos", "nôtre", "nôtres", "nous-même", 
                                           #"vous", "votre", "vos", "vôtre", "vôtres", "vous-même", # "vous" is a common polite form to adress someone in French, cannot rely on it alone to determine that mention is plural
                                           "ils", "elles", "leur", "leurs", "eux", "eux-même", "elles-mêmes",
                                        ])

# PRONOUNS: PERSON 1/2/3
# FIXME: 
EXPANDED_SINGULAR_FIRST_PERSON_PERSONAL_PRONOUN_MENTIONS = set(("je", "me", "m’", "moi", "mon", "ma", "mes", "mien", "mienne", "miens", "miennes", "moi-même"))
EXPANDED_PLURAL_FIRST_PERSON_PERSONAL_PRONOUN_MENTIONS = set(("nous", "notre", "nos", "nôtre", "nôtres", "nous-même"))
EXPANDED_SINGULAR_SECOND_PERSON_PERSONAL_PRONOUN_MENTIONS = set(("tu", "te", "t'", "toi", "ton", "ta", "tes", "tien", "tienne", "tiens", "tiennes", "toi-même"))

EXPANDED_FIRST_PERSON_PERSONAL_PRONOUN_MENTIONS = set(["je", "me", "m’", "moi", "mon", "ma", "mes", "mien", "mienne", "miens", "miennes", "moi-même", 
                                                        "nous", "notre", "nos", "nôtre", "nôtres", "nous-même",
                                                        ])
EXPANDED_SECOND_PERSON_PERSONAL_PRONOUN_MENTIONS = set(["tu", "te", "t'", "toi", "ton", "ta", "tes", "tien", "tienne", "tiens", "tiennes", "toi-même", 
                                                          "vous", "votre", "vos", "vôtre", "vôtres", "vous-même",
                                                          ])
EXPANDED_THIRD_PERSON_PERSONAL_PRONOUN_MENTIONS = set(["il", "elle", "on", "se", "s'", "lui", "en", "y", "soi", "son", "sa", "ses", "sien", "sienne", "siens", "siennes", "soi-même", "lui-même", "elle-même", 
                                                       "ils", "elles", "leur", "leurs", "eux", "eux-même", "elles-mêmes",
                                                       ])
EXPANDED_PERSONAL_PRONOUN_MENTIONS = EXPANDED_FIRST_PERSON_PERSONAL_PRONOUN_MENTIONS\
                                        .union(EXPANDED_SECOND_PERSON_PERSONAL_PRONOUN_MENTIONS)\
                                        .union(EXPANDED_THIRD_PERSON_PERSONAL_PRONOUN_MENTIONS)

# PRONOUNS: POSSESSIVE/REFLEXIVE/RELATIVE/DEMONSTRATIVE/INDEFINITE/WH--/MISC
POSSESSIVE_DETERMINERS = set(["mon", "ma", "mes", 
                              "ton", "ta", "tes", 
                              "son", "sa", "ses", 
                              "notre", "nos", 
                              "votre", "vos", 
                              "leur", "leurs",
                              ])
EXPANDED_POSSESSIVE_PRONOUNS = set(["mon", "ma", "mes", "mien", "mienne", "miens", "miennes", 
                                    "ton", "ta", "tes", "tien", "tienne", "tiens", "tiennes", 
                                    "son", "sa", "ses", "sien", "sienne", "siens", "siennes", 
                                    "notre", "nos", "nôtre", "nôtres", 
                                    "votre", "vos", "vôtre", "vôtres", 
                                    "leur", "leurs", 
                                    ])
EXPANDED_POSSESSIVE_PRONOUN_MENTIONS = set(["mon", "ma", "mes", "mien", "mienne", "miens", "miennes", 
                                            "ton", "ta", "tes", "tien", "tienne", "tiens", "tiennes", "tiens", "tiennes", 
                                            "son", "sa", "ses", "sien", "sienne", "siens", "siennes", 
                                            "notre", "nos", "nôtre", "nôtres", 
                                            "votre", "vos", "vôtre", "vôtres", 
                                            "leur", "leurs",
                                            "à qui"])

RELATIVE_PRONOUN_MENTIONS = set(["qui", "que", "quoi", "dont", "où", "lequel", "laquelle", "lesquels", "duquel", "de laquelle"])
REFLEXIVE_PRONOUN_MENTIONS = set(["moi-même", "toi-même", "soi-même", "lui-même", "elle-même", 
                                  "nous-même", "vous-même", "eux-même", "elles-même"])
INDEFINITE_PRONOUN_MENTIONS = set(["on", "tout un", "une", "l'un", "l'une", "les uns", "les unes", "un autre", 
                                   "une autre", "d'autres", "l'autre", "les autres", "aucun", "aucune", "aucuns", 
                                   "aucunes", "certains", "certaine", "certains", "certaines", "tel", "telle", 
                                    "tels", "telles", "tout", "toute", "tous", "toutes", "le même", "la même", 
                                    "les mêmes", "nul", "nulle", "nuls", "nulles", "quelqu'un", "quelqu'une", 
                                    "quelques uns", "quelques unes", "personne", "autrui", "quiconque", 
                                    "d’aucuns"])
# DEMONSTRATIVE PRONOUNS
DEMONSTRATIVE_PRONOUN_MENTIONS = set(["ce", "c'", "ceci", "celui-ci", "celle-ci", "ceux-ci", "celles-ci", 
                                        "cela", "ça", "celui-là", "celle-là", "ceux-là", "celles-là",
                                       ])

# PRONOUNS: ANIMACY
EXPANDED_ANIMATE_PRONOUN_MENTIONS = set(["je", "moi", "mien", "mienne", "le mien", "la mienne", "miens", "miennes", "les miens", "les miennes", "mon", "ma", "mes", "moi-même", 
                                         "tu", "toi", "tien", "tienne", "le tien", "la tienne", "tiens", "tiennes", "les tiens", "les tiennes", "ton", "ta", "tes", "toi-même", 
                                         "nous", "nôtre", "le nôtre", "la nôtre", "les nôtres", "notre", "nos", "nous-même", 
                                         "vous", "vôtre", "le vôtre", "la vôtre", "les vôtres", "votre", "vos", "vous-même",
                                        ])

# PRONOUNS: NE
#EXPANDED_MONEY_PERCENT_NUMBER_PRONOUN_MENTIONS = set(["il", "son", "sa", "ses"])
DATE_TIME_PRONOUN_MENTIONS = set(["quand"])
EXPANDED_ORGANIZATION_PRONOUN_MENTIONS = set(["il", "son", "sa", "ses", "leur", "leurs", "eux", "ils", "qui"])
#EXPANDED_GPE_PRONOUN_MENTIONS = set(["il", "son", "sa", "ses", "lui-même", "ils", "où"])
EXPANDED_LOCATION_PRONOUN_MENTIONS = set(["ici", "il", "son", "sa", "ses", "là", "où"])
#EXPANDED_FACILITY_VEHICLE_WEAPON_PRONOUN_MENTIONS = set(["il", "son", "sa", "ses", "lui-même", "ils", "où"])

######## QUANTIFIERS / PARTITIVES ########
QUANTIFIERS = set(["tout", "toute", "tous", "toutes", "n'importe lequel", "n'importe laquelle", "n'importe quoi", "les deux", 
                "tous deux", "chacun", "chacune", "assez", "chaque", "peu", "un peu", "moins", 
                "petit", "un petit peu", "moins", "beaucoup", "plus", "aucun", "aucune", "rien", "quelque", "quelques", "beaucoup de"])
PARTITIVES = set("{} {}".format(part, art) for part in [#"tous les",
                                                        "quantité", "milliard", "milliards", "bout", "bouts", "morceau", "morceaux", "tas",
                                                        "douzaine", "douzaines", "dizaine", "dizaines", "groupe", "groupes", "poignée", "poignées",
                                                        "centaine", "centaines", "type", "types", "beaucoup", "millions", "millions"
                                                        "nombre", "nombres", "morceau", "morceaux", "pincée", "pincées", 
                                                        "quantitées", "tranche", 
                                                        "millier", "milliers", "total",
                                                        ] for art in ["de", "d'"])
'''
QUANTIFIERS = set(["all", "any", "anything", "both", "each", "enough", "every", "everything", "few", 
                "a few", "fewer", "little", "a little", "less", "many", "more", "none", "not", 
                "nothing", "some", "a lot of"])
PARTITIVES = set(["all of", "amount of", "billion of", "billions of", "bit of", "bits of", "bunch of", 
                "dozens of", "group of", "groups of", "handful of", "handfuls of", "hundred of", 
                "hundreds of", "kind of", "kinds of", "lot of", "lots of", "million of", "millions of", 
                "number of", "numbers of", "piece of", "pieces of", "pinch of", "pounds of", 
                "quantities of", "slice of", "some of", "sort of", "sorts of", "stroke of", "tens of", 
                "thousand of", "thousands of", "total of"])
'''

######## STOP WORDS/ NON WORDS ########
NON_WORDS = set(["euh", "mm", "hmm", "ahem", "um"])

######## VERBS ########
COPULAR_VERBS = set(["suis", "es", "est", "sommes", "êtes", "sont", "fus", "fut", "fûmes", "fûtes", "furent", "être", "été",
                    "deviens", "devient", "devenons", "devenez", "deviennent", "devenir", "devins", "devint", "devinmes", "devintes", "devinrent", "devenu",
                    "semble", "sembles", "semblons", "semblez", "semblent", "semblai", "semblas", "sembla", "semblâmes", "semblâtes", "senblèrent", "sembler", "semblé",
                    "reste", "restes", "restons", "restez", "restent", "resta", "restas", "restâmes", "restâtes", "restèrent", "rester", "resté"
                    ]) # FIXME: is that all?
