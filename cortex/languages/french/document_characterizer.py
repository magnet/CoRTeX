# -*- coding: utf-8 -*-

"""
Defines a class used to characterize documents (annotate its sentences and tokens, with sufficient 
data, so as to be able to be processed by this library) that are written in French.

Here the library relies on an external tool, the StanfordCoreNLP software suite, which is is accessed 
through the use of the StanfordCoreNLPWrapper class.

For French, the stanford core nlp annotation models that can be used are the one defined in the 
"stanford-french-corenlp-2017-06-09-models.jar" file.

Regarding the POS tagger models, two are 
available:
    - "FrenchTagger" : uses tags belonging to the "modified CC treebank" constituent tags set 
      (cf the 'parameters.modified_CC_treebank_constituent_tags.py' module)
    - "FrenchUDTagger" : uses tags belonging to the "Universal Dependencies" constituent tags set 
      (cf http://universaldependencies.org/u/pos/all.html)

Regarding the parse tagger models (i.e. the constituent tree annotation models), three are 
available:
    - "FrenchFactored"
    - "SR"
    - "SRBeam" 
All uses tags belonging to the "modified CC treebank" constituent tags set. The default value to be 
used is 'FrenchFactored', seems to be a good compromise between annotation performances on the one 
hand, and execution speed on the other hand.

Since the toolbox' support for documents written in French is only for documents annotated with tags 
coming from the "modified CC treebank" constituent tags set, then only the 'FrenchTagger' POS tagger 
model is allowed to be used.
"""

__all__ = ["FrenchDocumentCharacterizer",
           ]

import os
import tempfile
import glob
import re
from collections import OrderedDict

from cortex.parameters  import ENCODING
from cortex.languages.common.document_characterizer import _DocumentCharacterizer
from cortex.tools.wrapper.stanford_corenlp import _create_stanford_core_nlp_wrapper_instance
from cortex.io.stanford_xml_reader import StanfordXMLReader
from cortex.utils.io import TemporaryDirectory

STANFORD_LANGUAGE_PARAMETER = "fr"
STANFORD_OUTPUT_FORMAT = "xml"
FORMAT_PATTERN = "document_n{}.txt"
GLOB_PATTERN = "document_n*.txt.{}".format(STANFORD_OUTPUT_FORMAT)
REGEX_PATTERN = "document_n(.+).txt" + ".{}".format(STANFORD_OUTPUT_FORMAT)
REGEX = re.compile(REGEX_PATTERN)

## (use_existing_tokenization, use_existing_sentence) => (get_text_fct, stanford_core_nlp_options)
PARAMETERS2GET_TEXT_FCT = {(True, True): (lambda document: "\n".join(" ".join(token.raw_text for token in sentence.tokens) for sentence in document.sentences), 
                                          ["-ssplit.eolonly", "-tokenize.whitespace"],
                                          ),
                           (True, False): (lambda document: " ".join(token.raw_text for token in document.tokens),
                                           ["-tokenize.whitespace",]),
                           (False, False): (lambda document: document.raw_text, []),
                           (False, True): (lambda document: "\n".join(sentence.raw_text for sentence in document.sentences),
                                           ["-ssplit.eolonly",]),
                           }

POSSIBLE_POS_MODELS = {"FrenchTagger": "edu/stanford/nlp/models/pos-tagger/french/french.tagger", 
                       #"FrenchUDTagger": "edu/stanford/nlp/models/pos-tagger/french/french-ud.tagger",
                       }
DEFAULT_POS_MODEL = "FrenchTagger"
POSSIBLE_PARSE_MODELS = {"FrenchFactored": "edu/stanford/nlp/models/lexparser/frenchFactored.ser.gz", 
                         "SR": "edu/stanford/nlp/models/srparser/frenchSR.ser.gz", 
                         "SRBeam": "edu/stanford/nlp/models/srparser/frenchSR.beam.ser.gz",
                         }
DEFAULT_PARSE_MODEL = "FrenchFactored"

class FrenchDocumentCharacterizer(_DocumentCharacterizer):
    """ Class used to characterize documents (annotate its sentences and tokens, with sufficient 
    data, so as to be able to be processed by this library) that are written in French, using the 
    "modified CC treebank" constituent tags set.
    
    Can work by only assuming that the document only has a raw text, and possibly mention & coreference 
    data, in which case, if possible, the mentions definitions, and coreference partition info will 
    be transfered to the created, characterized copy of the input document.
    Assume that the sentences of the raw text are either separated by a ' ' (a whitespace), or by a 
    '\\\\n' (a return-to-the-line character).
    """
    
    @classmethod
    def characterize_documents(cls, documents, pos_model=DEFAULT_POS_MODEL, parse_model=DEFAULT_PARSE_MODEL, 
                               use_existing_tokenization=False, use_existing_sentence_split=False, 
                               strict=True, tempfolder_root_path=None, delete_upon_exit=True):
        """ Computes information about the sentences and tokens of Document instances only from their 
        'raw_text' attribute value, and create a corresponding 'characterized document' for each of 
        them.
        
        The information regarding the potentially existing mentions will be carried over into the new 
        document, under the condition that the original document raw text's sentences are separated 
        either by a ' ' character (a whitespace) or by a '\\\\n' character (a return-to-the-line 
        character, and under the condition that this character is used nowhere else in the raw text, 
        except for one potential occurrence at the very beginning (first character of the raw text)). 
        Else, an exception will be raised.
        
        Args:
            documents: an iterable of Document instances, which we assume possess only potentially 
                mentions and a coreference partition
            pos_model: string, must belong to {'FrenchTagger'}, the POS tagger model to be used by 
                StanfordCoreNLP to carry POS tags annotation
            parse_model: string, must belong to {"FrenchFactored", "SR", "SRBeam"}, the constituent 
                tags annotation model to be used by the StanfordCoreNLP
            use_existing_tokenization: boolean (default = False); make it so that the tool used 
                to characterize a Document respect a pre-existing tokenization. Assume that all the input 
                Document instances possess a correct 'tokens' attribute value (not None). If one of them 
                does not, an exception will be raised.
            use_existing_sentences_split: boolean (default = False); make it so that the tool used 
                to characterize a Document respect a pre-existing sentence split. Assume that all the input 
                Document instances possess a correct 'sentences' attribute value (not None). If one of them 
                does not, an exception will be raised.
            strict: boolean (defaut = True), whether or not to raise an Exception in case where 
                it is impossible to align the raw_text attribute value of the input and of the characterized 
                Document instances in the case that mention data exists in the former. If set to False, the 
                characterized Document instance will be returned, but without any mention data.
            tempfolder_root_path: string, or None; path to a folder that shall be used a the root 
                folder for temporary folders that may need to be created and used during the call to this method
            delete_upon_exit: boolean, whether or not to delete the temporary folders that were used, 
                when they no longer are necessary. Useful for debugging purposes.

        Returns:
            the collection of characterized Document instances, in the order their respective 
            original version were input
        """
        characterized_documents = []
        
        # Parsing method's parameters
        ## Tempdir
        previous_tmpdir_value = tempfile.tempdir
        if tempfolder_root_path:
            tempfile.tempdir = tempfolder_root_path
        ## use_existing_tokenization, use_existing_sentence_split
        get_raw_text, stanford_core_nlp_options = PARAMETERS2GET_TEXT_FCT[(use_existing_tokenization, 
                                                                           use_existing_sentence_split)]
        
        ## pos model
        try:
            pos_model_option = POSSIBLE_POS_MODELS[pos_model]
        except KeyError:
            msg = "Incorrect input 'pos_model' parameter value '{}', possible values are: {}."
            msg = msg.format(pos_model, set(POSSIBLE_POS_MODELS.keys()))
            raise ValueError(msg) from None
        stanford_core_nlp_options.extend(("-pos.model", pos_model_option))
        ## parse model
        try:
            parse_model_option = POSSIBLE_PARSE_MODELS[parse_model]
        except KeyError:
            msg = "Incorrect input 'parse_model' parameter value '{}', possible values are: {}."
            msg = msg.format(pos_model, set(POSSIBLE_PARSE_MODELS.keys()))
            raise ValueError(msg) from None
        stanford_core_nlp_options.extend(("-parse.model", parse_model_option))
        # Carry out the characterization
        with TemporaryDirectory(delete_upon_exit=delete_upon_exit) as temp_input_folder:
            input_folder_path = temp_input_folder.temporary_folder_path
            with TemporaryDirectory(delete_upon_exit=delete_upon_exit) as temp_output_folder:
                output_folder_path = temp_output_folder.temporary_folder_path
                # Write the file containing the raw_text of each documents
                file_names = []
                documents_cache = {}
                for i, document in enumerate(documents):
                    ident = str(i)
                    sentence_sep = cls._compute_sentence_sep(document)
                    documents_cache[ident] = (document, sentence_sep)
                    file_name = FORMAT_PATTERN.format(ident)
                    file_path = os.path.join(input_folder_path, file_name)
                    with open(file_path, "w", encoding=ENCODING) as f:
                        raw_text = get_raw_text(document)
                        f.write(raw_text)
                    file_names.append(file_name)
                if file_names:
                    # Apply Stanford pipeline
                    init_kwargs = {"annotators": ["tokenize", "ssplit", "pos", "parse"], 
                                   "lang": "fr", 
                                   "output_format": STANFORD_OUTPUT_FORMAT, 
                                   "options": stanford_core_nlp_options}
                    stanford_corenlp_wrapper = _create_stanford_core_nlp_wrapper_instance(**init_kwargs)
                    stanford_corenlp_wrapper.process(input_folder_path, output_folder_path)
                    # Read back the documents
                    stanford_xml_reader = StanfordXMLReader()
                    file_name_pattern = GLOB_PATTERN
                    pattern = os.path.join(output_folder_path, file_name_pattern)
                    for file_path in glob.glob(pattern):
                        # Fetch the corresponding original document using the id number embedding into the file name
                        file_name = os.path.basename(file_path)
                        ident = REGEX.match(file_name).groups()[0]
                        original_document, sentence_sep = documents_cache[ident]
                        # Parse the characterized document, and associate it with data regarding mentions and coreference partition of original document, if present and if aligned
                        characterized_document_buffer = stanford_xml_reader.parse(file_path, sentence_sep=sentence_sep, enforce_lemma=False)
                        actual_document = cls._reassemble_mentions_and_entities_data(original_document, 
                                                                                      characterized_document_buffer, 
                                                                                      strict=strict)
                        # Update 'info' attribute of the characterized Document instance
                        new_info = OrderedDict(original_document.info)
                        new_info["processes"] = new_info.get("processes", "") + " , French document characterization"
                        actual_document.info = new_info
                        # Update 'ident' info of the characterized Document instance
                        new_ident = "characterized_{}".format(original_document.ident)
                        actual_document.ident = new_ident
                        # Append the document to the collection being built
                        characterized_documents.append((ident, actual_document))
                    characterized_documents = tuple(d for _, d in sorted(characterized_documents, key=lambda x: int(x[0])))
        tempfile.tempdir = previous_tmpdir_value
        return characterized_documents

