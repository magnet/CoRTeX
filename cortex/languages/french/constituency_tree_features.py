# -*- coding: utf-8 -*-

"""
Defines base classes and utilities used to compute data and values corresponding to grammatical 
information, also called grammatical features here, from instances of the ConstituencyTreeNode and 
of the ConstituencyTree classes, created from French document data.

Assumes that the constituency tree tags used are the "modified CC treebank" constituent tags, and the 
head finder used is the "Dybro Johansen" rules based head finder.

Assumes that trees are spread on their width rather than on their height (so no binary trees) - parse 
trees produced by the Stanford Core NLP parser should be fine.
"""

__all__ = ["FrenchConstituencyTreeNodeFeatures", 
           "FrenchConstituencyTreeFeatures", 
           "FRENCH_CONSTITUENCY_TREE_NODE_FEATURES", 
           "FRENCH_CONSTITUENCY_TREE_FEATURES",
           ]

from ..common.constituency_tree_features import (ConstituencyTreeNodeFeatures, 
                                                 _ConstituencyTreeFeatures)

from .token_features import FRENCH_TOKEN_FEATURES
from .constituency_tree_node_head_finder import DybroJohansenHeadFinder
from .parameters import modified_CC_treebank_constituent_tags as POS_tags_module
FRENCH_NODE_HEAD_FINDER = DybroJohansenHeadFinder()

class FrenchConstituencyTreeNodeFeatures(ConstituencyTreeNodeFeatures):
    """ A class defining grammatical features that can be defined on a ConstituencyTreeNode instance, 
    created from data coming from a document written in French.
    
    Assumes that constituency trees are spread on their width rather than on their height (so no 
    "only binary trees") - parse trees produced by the Stanford Core NLP parser should be fine.
    
    Here, the constituency tree tags used are the "modified CC treebank" constituent tags, and the 
    head finder used is the "Dybro Johansen" rules based head finder.
    
    Attributes:
        POS_tags_module: the python module defining the constituency tree tags used to characterize 
            the constituency tree of the document which the mentions originate from (the 
            "modified_CC_treebank_constituent_tags" module)
        
        node_head_finder: the _ConstituencyTreeNodeHeadFinder child class instance used under the 
            hood; here, a DybroJohansenHeadFinder instance
    """
    
    def __init__(self):
        super().__init__(POS_tags_module, FRENCH_NODE_HEAD_FINDER)
    
    ## Instance methods
    def _get_expanded_possessive_leaves_boundaries(self, node):
        """ Returns a collection of (node leaves boundaries, node's head node leaves boundaries) pairs 
        from iterating over the leaf nodes of the input node, as long as their respective corresponding 
        token is a 'possessive_determiner'.
        
        Assume that the input node's leaves have been synchronized with their corresponding token.
        
        Args:
            node: a ConstituencyTreeNode instance

        Returns:
            a collection of (ConstituencyTreeNode instance pair, ConstituencyTreeNode instance pair) pair
        """
        get_leaves_boundaries_fct = lambda node, current_leaves_boundaries: current_leaves_boundaries
        found = []
        for current in node.get_leaves():
            if FRENCH_TOKEN_FEATURES.is_expand_poss_pro(current.token):
                leaves_boundaries = current.get_leaves_boundaries()
                head_leaves_boundaries = get_leaves_boundaries_fct(current, leaves_boundaries)
                found.append((leaves_boundaries, head_leaves_boundaries))
        return found


class FrenchConstituencyTreeFeatures(_ConstituencyTreeFeatures):
    """ A class defining grammatical features that can be defined on a ConstituencyTree instance, 
    created from data coming from a document written in French.
    
    Assumes that constituency trees are spread on their width rather than on their height (so no 
    "only binary trees") - parse trees produced by the Stanford Core NLP parser should be fine.
    
    Attributes:
        constituency_tree_node_features: the value of the ConstituencyTreeNodeFeatures instance 
            used under the hood; here, a FrenchConstituencyTreeNodeFeatures instance
    """
    
    def __init__(self):
        constituency_tree_node_features = FrenchConstituencyTreeNodeFeatures()
        super().__init__(constituency_tree_node_features)
    
    ## Instance methods
    def _get_expanded_possessive_leaves_boundaries(self, constituency_tree):
        """ Returns a collection of (node leaves boundaries, node's head node leaves boundaries) pairs 
        from iterating over the leaf nodes of the root of the input constituency tree, as long as 
        their respective corresponding token is a 'possessive_determiner'.
        
        Assumes that the input node's leaves have been synchronized with their corresponding token.
        
        Args:
             constituency_tree: a ConstituencyTree instance

        Returns:
            a collection of (ConstituencyTreeNode instance pair, ConstituencyTreeNode instance pair) pair
        """
        return self.constituency_tree_node_features._get_expanded_possessive_leaves_boundaries(constituency_tree.root)


FRENCH_CONSTITUENCY_TREE_NODE_FEATURES = FrenchConstituencyTreeNodeFeatures()
FRENCH_CONSTITUENCY_TREE_FEATURES = FrenchConstituencyTreeFeatures()