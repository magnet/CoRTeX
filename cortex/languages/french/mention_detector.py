# -*- coding: utf-8 -*-

"""
Defines a class used to detect mentions in an document written in French.

Uses FrenchConstituencyTreeFeature and FrenchConstituencyTreeNodeFeatures instances, which themselves 
support only documents whose constituency tree annotations use tags defined in the 
"modified CC treebank" tags set. 
"""

__all__ = ["FrenchMentionDetector",
           ]

from collections import defaultdict

from cortex.utils import get_unique_elts
from cortex.api.document import IncorrectExtentError
from ..common.mention_detector import _MentionDetector

from .knowledge import lexicon
from .parameters import modified_CC_treebank_constituent_tags as POS_tags_module
from .parameters import base_named_entities_data_tags as NE_tags
from .token_features import FRENCH_TOKEN_FEATURES
from .constituency_tree_features import (FRENCH_CONSTITUENCY_TREE_FEATURES, 
                                         FRENCH_CONSTITUENCY_TREE_NODE_FEATURES)


NON_MENTION_NAMED_ENTITY_TAGS = set()
for set_like in tuple(): #NE_tags.PERCENT_NAMED_ENTITY_TYPE_TAGS, NE_tags.MONEY_NAMED_ENTITY_TYPE_TAGS, NE_tags.QUANTITY_NAMED_ENTITY_TYPE_TAGS, NE_tags.CARDINAL_NAMED_ENTITY_TYPE_TAGS
    NON_MENTION_NAMED_ENTITY_TAGS.update(set_like)


class FrenchMentionDetector(_MentionDetector):
    """ Class whose role is to detect mentions in a document written in French.
    
    The document will have to be annotated in constituency trees using tags defined in the 
    "modified CC treebank" tags set.
    
    The aim is to extract mention candidates corresponding to 'noun phrases'.
    """
    _TOKEN_FEATURES = FRENCH_TOKEN_FEATURES
    _CONSTITUENCY_TREE_FEATURES = FRENCH_CONSTITUENCY_TREE_FEATURES
    _CONSTITUENCY_TREE_NODE_FEATURES = FRENCH_CONSTITUENCY_TREE_NODE_FEATURES
    
    @classmethod
    def _get_NP_leaves_boundaries(cls, constituency_tree, head_search_heuristic="leaf"):
        """ Returns collection of (node leaves boundaries, node's head node leaves boundaries) pairs 
        from iterating over nodes, starting from the root of the input tree and then carrying a 
        breadth-first search on its children, which correspond to 'noun phrase'.
        
        Args:
            constituency_tree: a ConstituencyTree instance
            head_search_heuristic: string, must belong to {"rule", "leaf"}, parameter specifying 
                the heuristic to use when carrying out a search for the head node among a node's descendants

        Returns:
            a collection of (ConstituencyTreeNode instance pair, ConstituencyTreeNode instance pair) pairs
        """
        tags = [POS_tags_module.NOUN_PHRASE_TAG]
        return cls._CONSTITUENCY_TREE_FEATURES.get_tags_leaves_boundaries(constituency_tree, tags, 
                                                                         find_head=True,
                                                                         head_search_heuristic=head_search_heuristic)
    
    @classmethod
    def _get_clitic_leaves_boundaries(cls, constituency_tree, head_search_heuristic="leaf"):
        """ Returns collection of (node leaves boundaries, node's head node leaves boundaries) pairs 
        from iterating over nodes, starting from the root of the input tree and then carrying a 
        breadth-first search on its children, which correspond to 'clitic' words.
        
        Args:
            constituency_tree: a ConstituencyTree instance
            head_search_heuristic: string, must belong to {"rule", "leaf"}, parameter specifying 
                the heuristic to use when carrying out a search for the head node among a node's descendants

        Returns:
            a collection of (ConstituencyTreeNode instance pair, ConstituencyTreeNode instance pair) pairs
        """
        tags = POS_tags_module.CLITIC_POS_TAGS
        return cls._CONSTITUENCY_TREE_FEATURES.get_tags_leaves_boundaries(constituency_tree, tags, 
                                                                         find_head=True,
                                                                         head_search_heuristic=head_search_heuristic)
    
    @classmethod
    def _get_expanded_possessive_leaves_boundaries(cls, constituency_tree):
        """ Returns a collection of (node leaves boundaries, node's head node leaves boundaries) pairs 
        from iterating over the leaf nodes of the root of the input constituency tree, as long as 
        their respective corresponding token is a 'possessive_determiner'.
        Assume that the input node's leaves have been synchronized with their corresponding token.
        
        Args:
             constituency_tree: a ConstituencyTree instance

        Returns:
            a collection of (ConstituencyTreeNode instance pair, ConstituencyTreeNode instance pair) pair
        """
        return cls._CONSTITUENCY_TREE_FEATURES._get_expanded_possessive_leaves_boundaries(constituency_tree)
    
    @classmethod
    def _get_verb_leaves_boundaries(cls, constituency_tree):
        """ Returns collection of (node leaves boundaries, node's head node leaves boundaries) pairs 
        from iterating over nodes, starting from the root of the input tree and then carrying a 
        breadth-first search on its children, which correspond to 'verb' words.
        
        Args:
            constituency_tree: a ConstituencyTree instance
            head_search_heuristic: string, must belong to {"rule", "leaf"}, parameter specifying 
                the heuristic to use when carrying out a search for the head node among a node's descendants

        Returns:
            a collection of (ConstituencyTreeNode instance pair, ConstituencyTreeNode instance pair) pairs
        """
        tags = POS_tags_module.VERBS_POS_TAGS
        return cls._CONSTITUENCY_TREE_FEATURES.get_tags_leaves_boundaries(constituency_tree, tags, 
                                                                         find_head=False)
    
    @staticmethod
    def _convert_tokens_leaves_boundaries_pairs_to_candidate_mentions(tokens_leaves_boundaries_pairs):
        """ Converts the input collection of pairs of pairs of nodes into a map linking mention 
        candidates' head extent to extent values.
        
        Args:
            tokens_leaves_boundaries_pairs: collection of (ConstituencyTreeNode instance pair, 
                ConstituencyTreeNode instance pair) pairs, each representing a 
                (node leaves boundaries, node's head node leaves boundaries) pair

        Returns:
            a "candidate mention head extent > candidate mention extent" map
        """
        candidate_mention_head_extent2mention_extent = {}
        for (leaves_boundaries, head_leaves_boundaries) in tokens_leaves_boundaries_pairs: # Leaves_boundaries in constituency tree
            leftmost_leaf, rightmost_leaf = leaves_boundaries # leftmost_leaf, rightmost_leaf are nodes in constituency tree
            head_leftmost_leaf, head_rightmost_leaf = head_leaves_boundaries # These are nodes in constituency tree
            extent = (leftmost_leaf.start, rightmost_leaf.end) # Mention's extent in text
            head_extent = (head_leftmost_leaf.start, head_rightmost_leaf.end) # Mention's head's extent in text
            candidate_mention_head_extent2mention_extent[head_extent] = extent
        return candidate_mention_head_extent2mention_extent
    
    @classmethod
    def _detect_sentence_nominal_candidate_mentions(cls, sentence):
        """ Detects in the input sentence the mention candidates that are 'noun phrase'.
        
        Args: 
            sentence: a Sentence instance

        Returns:
            a "candidate mention head extent > candidate mention extent" map
        """
        # Token leaves boundaries representing parts of text that are candidates to be mention (to be converted to raw leaves_boundaries)
        tokens_leaves_boundaries_pairs = cls._get_NP_leaves_boundaries(sentence.constituency_tree) # Leaves_boundaries elements are leaves of the parse tree
        # WARNING: above, if no head node could be found, the 'head leaves boundaries' might encompass several tokens instead
        candidate_mention_head_extent2mention_extent = cls._convert_tokens_leaves_boundaries_pairs_to_candidate_mentions(tokens_leaves_boundaries_pairs)
        return candidate_mention_head_extent2mention_extent
    
    @classmethod
    def _detect_sentence_clitic_candidate_mentions(cls, sentence):
        """ Detects in the input sentence the mention candidates that are 'clitic words'.
        
        Args: 
            sentence: a Sentence instance

        Returns:
            a "candidate mention head extent > candidate mention extent" map
        """
        # Token leaves boundaries representing parts of text that are candidates to be mention (to be converted to raw leaves_boundaries)
        tokens_leaves_boundaries_pairs = cls._get_clitic_leaves_boundaries(sentence.constituency_tree) # Leaves_boundaries elements are leaves of the parse tree
        # WARNING: above, if no head node could be found, the 'head leaves boundaries' might encompass several tokens instead
        candidate_mention_head_extent2mention_extent = cls._convert_tokens_leaves_boundaries_pairs_to_candidate_mentions(tokens_leaves_boundaries_pairs)
        return candidate_mention_head_extent2mention_extent
    
    @classmethod
    def _detect_sentence_expanded_possessive_pronouns_candidate_mentions(cls, sentence):
        """ Detects in the input sentence the mention candidates that are 'expanded possessive pronouns'.
        
        Args: 
            sentence: a Sentence instance

        Returns:
            a "candidate mention head extent > candidate mention extent" map
        """
        # Token leaves boundaries representing parts of text that are candidates to be mention (to be converted to raw leaves_boundaries)
        tokens_leaves_boundaries_pairs = cls._get_expanded_possessive_leaves_boundaries(sentence.constituency_tree) # Leaves_boundaries elements are leaves of the parse tree
        # WARNING: above, if no head node could be found, the 'head leaves boundaries' might encompass several tokens instead
        candidate_mention_head_extent2mention_extent = cls._convert_tokens_leaves_boundaries_pairs_to_candidate_mentions(tokens_leaves_boundaries_pairs)
        return candidate_mention_head_extent2mention_extent
    
    @classmethod
    def _detect_sentence_verbal_candidate_mentions(cls, sentence):
        """ Detects in the input sentence the mention candidates that are 'verb phrase'.
        
        Args: 
            sentence: a Sentence instance

        Returns:
            a "candidate mention head extent > candidate mention extent" map
        """
        # Detect verbal mentions (filtered in "verb-pronoun", "verb-verb" and "verb-noun" patterns)
        verb2leaves_boundaries_pairs = defaultdict(list)
        kept_leaves_boundaries_pairs = [] # WARNING: the 'head extent' here might encompass several tokens
        # Token leaves boundaries representing parts of text that are candidates to be mention (to be converted to raw leaves_boundaries)
        tokens_leaves_boundaries_pairs = cls._get_verb_leaves_boundaries(sentence.constituency_tree)
        #sentences_nb = len(sentence.document.sentences)
        #sentence_index = sentence.document.extent2sentence_index[sentence.extent]
        for (leaves_boundaries, head_leaves_boundaries) in tokens_leaves_boundaries_pairs:
            _, rightmost_leaf = leaves_boundaries #leftmost_leaf
            v_lemma = rightmost_leaf.token.lemma.lower()
            # Exclude some stop words
            if v_lemma in ("être", "avoir", "dire", "aller"):
                continue
            
            # Keep in memory the leaves boundaries pairs associated to each verb lemma observed
            verb2leaves_boundaries_pairs[v_lemma].append((leaves_boundaries, head_leaves_boundaries))
            # Do not use the following for now, since I do not know if it can be adapted / how to adapt it for French (ex: which list of possible pronouns, instead of 'it', 'this' and 'that'?)
            '''
            # Find "verb-pronoun" patterns (pronoun = it/this/that)
            v_end = rightmost_leaf.token.end # Raw end position
            pattern_found = False
            for j in range(sentence_index, min(sentence_index+2, sentences_nb)): # Current and next sentence
                s_constituency_tree = sentences[j].constituency_tree
                # We fetch the NP leaves boundaries found in the verb candidate mention's sentence and the following sentence, and for each of them...
                for ((s_leftmost_leaf, s_rightmost_leaf), _) in cls._get_NP_leaves_boundaries(s_constituency_tree):
                    # ...if they are made of only on token...
                    if s_leftmost_leaf == s_rightmost_leaf: # FIXME: use 'is' instead of '=='?
                        s_token = s_leftmost_leaf.token
                        t_start = s_token.start
                        # ...; if the NP token begins before the end of the current verb candidate mention, and is one of the appropriate pronouns, then we decide to keep the verb candidate mention.
                        # FIXME: why do we check the next sentence? Any One token NP found in the next sentence will begin after the end of the verb candidate mention, right? So the following condition will never be met, right?
                        if t_start > v_end and s_token.raw_text.lower() in (""): #("it", "that", "this")
                            pattern_found = True
                            break
                if pattern_found:
                    break
            if pattern_found:
                kept_leaves_boundaries_pairs.append((leaves_boundaries, head_leaves_boundaries))
            '''
        # Find "verb-verb" patterns (verbs appearing several times in the document)
        for leaves_boundaries_pairs_ in verb2leaves_boundaries_pairs.values():
            if len(leaves_boundaries_pairs_) > 1:
                kept_leaves_boundaries_pairs.extend(leaves_boundaries_pairs_)
        # Lack the ressources to do the same with French for now
        """
        # Find "verb-noun" patterns (verb and its nominalization)
        noun2verb = {}
        added = set()
        for v_lemma in verb2leaves_boundaries_pairs.keys():
            nouns_ = lexicon.VERB_TO_NOUN.get(v_lemma, tuple())
            if nouns_:
                for noun in nouns_:
                    noun2verb[noun] = v_lemma
        for token in tokens:
            lemma = token.lemma.lower()
            if lemma in noun2verb:
                v_lemma = noun2verb[lemma]
                if v_lemma not in added:
                    kept_leaves_boundaries_pairs.extend(verb2leaves_boundaries_pairs[v_lemma])
                    added.add(v_lemma)
        """
        candidate_mention_head_extent2mention_extent = cls._convert_tokens_leaves_boundaries_pairs_to_candidate_mentions(kept_leaves_boundaries_pairs)
        return candidate_mention_head_extent2mention_extent
    
    @classmethod
    def _identify_unacceptable_mentions(cls, document, mention_head_extent2mention_extent, 
                                        start_index2token_index_and_token_pair):
        """ Remove mention candidates that are incorrect / unacceptable.
        
        Args:
            document: a Document instance, the document on which mention detection is carried out
            mention_head_extent2mention_extent: the "candidate mention head extent > candidate mention extent" 
                map representing the currently detected mention candidates
            start_index2token_index_and_token_pair: a "token start index => (token index; token) pair" 
                map, representing a useful utility for the function

        Returns:
            a collection of mention candidates' head extent to remove
        """
        tokens = document.tokens
        mention_head_extent_to_remove = []
        ## Remove unacceptable mentions
        named_entity_extent2named_entity_type = {}
        if document.named_entities:
            for named_entity in document.named_entities:
                named_entity_extent2named_entity_type[named_entity.extent] = named_entity.type
        for head_extent, mention_extent in mention_head_extent2mention_extent.items():
            # Get text extent and candidate mention's lowercase text
            s, e = mention_extent
            m_text = document.get_raw_text_extract((s,e)).lower().strip()
            # Remove percent, money, cardinal and quantities
            if named_entity_extent2named_entity_type.get(mention_extent, None) in NON_MENTION_NAMED_ENTITY_TAGS:
                mention_head_extent_to_remove.append(head_extent)
            # Remove "vous/eux/nous" preceded by "tous", "many of"; also remove things such as "nous deux", "vous quatre", etc
            try:
                right_text = document.get_raw_text_extract((e+1,e+5)).strip()
            except IncorrectExtentError:
                right_text = ""
            left_text = document.get_raw_text_extract((max(s-17,0),s-1)).strip()
            if m_text in ("eux", "nous", "vous"):
                for suffix in ("tous", "deux"):
                    if right_text.endswith(suffix):
                        mention_head_extent_to_remove.append(head_extent)
                        break
                for prefix in ("certains d'entre", "beaucoup d'entre"):
                    if left_text.endswith(prefix):
                        mention_head_extent_to_remove.append(head_extent)
                        break
                # FIXME: code common to MentionCharacterizer, right ? Is it worth it then to build a TokenFeatures instance method ?
                mention_first_token_index, _ = start_index2token_index_and_token_pair[s]
                mention_potential_second_token_index = mention_first_token_index+1
                if mention_potential_second_token_index < len(tokens): # If the potential second token of the mention indeed exists
                    if cls._TOKEN_FEATURES.is_CD(tokens[mention_first_token_index+1]):
                        mention_head_extent_to_remove.append(head_extent)
            # Remove quantified expressions
            if not (m_text.startswith("eux") or m_text.startswith("nous") or m_text.startswith("vous")):
                # Keep expressions like "all of them", "both of you", etc.
                for quantifier in lexicon.QUANTIFIERS:
                    if quantifier in ("tout", "toute", "tous", "toutes", "quelque", "quelques", "beaucoup", "un peu"): # Some mentions of this form
                        continue
                    if m_text.startswith(quantifier):
                        mention_head_extent_to_remove.append(head_extent)
                        break
            # Remove NPs preceded by partitive (e.g. milliers de)
            prev_text = document.get_raw_text_extract((max(s-20,0),s-1)).rstrip() # Take enough text
            for partitive in lexicon.PARTITIVES:
                if prev_text.endswith(partitive):
                    mention_head_extent_to_remove.append(head_extent)
                    break
            # Remove non-words
            if m_text in lexicon.NON_WORDS:
                mention_head_extent_to_remove.append(head_extent)
            # Remove expressions
            if document.get_raw_text_extract((max(s-3,0),e)).strip().lower() == "en fait":
                mention_head_extent_to_remove.append(head_extent)
            # Remove some stop words
            # N/A for French, is there other kind of special cases for French?
            """
            # FIXME: code common to MentionCharacterizer, right ? Is it worth it then to build a TokenFeatures instance method ?
            if m_text in ["'s"]:
                mention_head_extent_to_remove.append(head_extent)
            """
            # FIXME: what to do about this legacy code?
            '''
#            if m_text in STOP_WORDS:
#                mention_head_extent_to_remove.append(head_extent)
            '''
            
        # Remove duplicate entries
        mention_head_extent_to_remove = get_unique_elts(mention_head_extent_to_remove)
        
        return mention_head_extent_to_remove
    
    @classmethod
    def _detect_named_entities_candidate_mentions(cls, document, start_index2token_index_and_token_pair):
        """ Removes mention candidates that are incorrect / unacceptable.
        
        Args:
            document: a Document instance, the document on which mention detection is carried out
            start_index2token_index_and_token_pair: a "token start index => (token index; token) pair" 
                map, representing a useful utility for the function

        Returns:
            a "candidate mention head extent > candidate mention extent" map
        """
        candidate_mention_head_extent2mention_extent = {}
        
        # We should add those candidate mentions detected there only if a mention with the same extent does not already exist, since the info provided before regarding the head extent may be better 
        # FIXME: move this upstream ? (legacy fixme) (I can understand, since this is a DETECTION phase...)
        ## Detection itself
        if document.named_entities:
            for named_entity in document.named_entities:
                # If the named entity's extent does not correspond to the head of a candidate mention, and has not been seen up until now, then we add it as a candidate mention, with it being its own head.
                extent = named_entity.extent
                if extent not in candidate_mention_head_extent2mention_extent:
                    candidate_mention_head_extent2mention_extent[extent] = extent
                # Furthermore, if it is a ORGANIZATION type named entity that is not being preceded by a definite determiner, we also add it as a candidate mention.
                # FIXME: do not understand: whether the condition is met or not, we already added the mention if a named entity existed for it (but this time we can bypass seen_mention_extent check... why?)
                ne_type = named_entity.type
                s, _ = extent
                test_text = document.get_raw_text_extract((max(s-4,0),s-1)).lower()
                if ne_type in NE_tags.ORGANIZATION_NAMED_ENTITY_TYPE_TAGS and all(def_det_text not in test_text for def_det_text in ("le","la", "l", "les")): # FIXME: find a better fix than that
                    candidate_mention_head_extent2mention_extent[extent] = extent
                # FIXME: what to do about this commented-out legacy code?
                # I think this code aims to add the would-be mention only if the would-be mention's youngest ancestor node's leaves are consistent with the tokens constituting the would-be mention
            
            '''
    #            s, e = extent
    #            if (s,e) not in candidate_mention_head_extent2mention_extent:
    #                # Find a head_extent
    #                ## Consider the first and last token of the would-be mention
    #                _, start_token = start_index2token_index_and_token_pair[s]
    #                _, end_token = end_index2token_index_and_token_pair[e]
    #                # Consider their respective constituency_tree_node
    #                start_node = start_token.constituency_tree_node
    #                end_node = end_token.constituency_tree_node
    #                # Compute their youngest ancestor node
    #                ancestor_node = start_node.get_youngest_common_ancestor(end_node)
    #                # We proceed only if the youngest ancestor node exists and spans exactly the would-be mention
    #                if ancestor_node is not None and ancestor_node.raw_extent == (s,e): # only keep span corresponding to a subtree in parse
    #                    hs,he = s,e # Default mention head extent, will be modified if head finder works
    #                    head_node = cls._CONSTITUENCY_TREE_NODE_FEATURES.get_head(ancestor_node)
    #                    if head_node is not None and head_node.is_leaf():
    #                        hs,he = head_node.token.extent
    #                        if (hs,he) not in candidate_mention_head_extent2mention_extent: # FIXME: no check with 'seen_mention_extent'?
    #                            candidate_mention_head_extent2mention_extent[(hs,he)] = (s,e)
            '''
        
        # In case there was no NE detection for persons, carry out a search based on recognizing a proper name.
        # Cannot do it for now for French, for lack of resources
        """
        # FIXME: move this code upstream (part of a DETECTION phase?)?
        mention_extents = set(candidate_mention_head_extent2mention_extent.values())
        for token in tokens:
            extent = token.extent
            text = token.raw_text
            if NAMES_CORPUS_READER.is_male_name(text) or NAMES_CORPUS_READER.is_female_name(text):
                if extent not in mention_extents and extent not in candidate_mention_head_extent2mention_extent: # If the extent is neither an already found mention's head extent nor a mention extent, we add it
                    candidate_mention_head_extent2mention_extent[extent] = extent
        """
        
        return candidate_mention_head_extent2mention_extent
    
    @classmethod
    def _detect_mentions(cls, document, detect_verbs=False):
        """Detects mentions in the document, replace potentially already existing mentions and 
        coreference partition information.
        
        Assumes that the document has been characterized (notably, info about sentence's constituency 
        tree and tokens, with synchronization between tokens and constituency tree nodes).
        
        Args:
            detect_verb: whether or not to consider some verbal mention candidates during the search

        Returns:
            a "mention_head_extent => mention_extent" mapping (some 'head_extent' might in fact encompass several tokens instead of one)
        """
        # Build useful utilities: 'leaves_boundaries to tokens' map
        start_index2token_index_and_token_pair = {}
        end_index2token_index_and_token_pair = {}
        for token_index, token in enumerate(document.tokens):
            s,e = token.extent
            start_index2token_index_and_token_pair[s] = (token_index, token)
            end_index2token_index_and_token_pair[e] = (token_index, token)
        
        # Detect mentions
        ## Using syntactic data
        syntactically_based_candidate_mention_head_extent2mention_extent = {}
        for sentence in document.sentences:
            # DETECT Noun phrases
            syntactically_based_candidate_mention_head_extent2mention_extent.update(cls._detect_sentence_nominal_candidate_mentions(sentence))
            # DETECT Clitics (mostly pronouns)
            syntactically_based_candidate_mention_head_extent2mention_extent.update(cls._detect_sentence_clitic_candidate_mentions(sentence))
            # DETECT Expanded possessive pronouns (notably possessive determiners)
            syntactically_based_candidate_mention_head_extent2mention_extent.update(cls._detect_sentence_expanded_possessive_pronouns_candidate_mentions(sentence))
            # DETECT Verbal phrases
            if detect_verbs:
                syntactically_based_candidate_mention_head_extent2mention_extent.update(cls._detect_sentence_verbal_candidate_mentions(sentence))
        ## Using named entities data (if something is a named entity, then it is a mention)
        named_entities_candidate_mention_head_extent2mention_extent = cls._detect_named_entities_candidate_mentions(document, start_index2token_index_and_token_pair)
        
        # Aggregate detected mention data
        mention_head_extent2mention_extent = {}
        ## Trim candidate mentions: keep only one mention per head extent (the largest mention)
        seen_mention_extents = set() # To avoid duplicate mention extent
        for candidate_mention_head_extent2mention_extent in (syntactically_based_candidate_mention_head_extent2mention_extent,
                                                              named_entities_candidate_mention_head_extent2mention_extent): # It is important that named entity based candidate mention data be placed after syntactically based candidate mention data, since its head extent data is not precise at all
            for head_extent, extent in candidate_mention_head_extent2mention_extent.items():
                hs, he = head_extent
                s, e = extent
                if (hs,he) in mention_head_extent2mention_extent:
                    # Take the largest extent, i.e. the outermost mention
                    # FIXME: why delete the innermost mentions?
                    previous_extent = mention_head_extent2mention_extent[(hs,he)]
                    s2, e2 = previous_extent
                    s_min = min(s, s2)
                    e_max = max(e, e2)
                    mention_head_extent2mention_extent[(hs,he)] = (s_min,e_max)
                    if s_min != s2 or e_max != e2:
                        seen_mention_extents.remove((s2,e2))
                        seen_mention_extents.add((s_min,e_max))
                else:
                    if (s,e) not in seen_mention_extents:
                        mention_head_extent2mention_extent[(hs,he)] = (s,e)
                        seen_mention_extents.add((s,e))
        ## Remove unacceptable candidate mention
        ### Identify the candidate mention to remove
        mention_head_extent_to_remove = cls._identify_unacceptable_mentions(document, mention_head_extent2mention_extent, start_index2token_index_and_token_pair)
        ### Execute the removal
        for head_extent in mention_head_extent_to_remove:
            mention_head_extent2mention_extent.pop(head_extent)
        
        return mention_head_extent2mention_extent
