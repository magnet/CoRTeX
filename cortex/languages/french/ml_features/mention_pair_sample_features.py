# -*- coding: utf-8 -*-

"""
Define functions to compute values or data from a MentionPairSample instance, as well as utilities 
used to create a collection of 'cortex.tools.ml_features._Features' child class instances from those functions
"""

"""
Here, a 'feature' is an instance of a '_Feature' child class of the 'ml_fetures' module.

The aim of this module is to prepare the parametrized creation of features, and to define the collection 
of features and of feature group names that shall be used to create a ml sample vectorizer.
A group of feature names is a collection of features names. It can be used to specify that operation 
made between features from two distinct groups should be made for all pairs defined in the cardinal 
product of two group.

In this specific case, the ml samples will be assumed to be instances of the MentionPairSample class. 

It does this by creating functions that can take as 
inputs:
    - those parameters (notably, 'quantize' and 'strict')
    - structures that store the created features,
    - a structure to record the group each features belongs to
Those functions' respective name follows the following pattern: 'create_..._feature'.

Most of the features that can be created use functions that can be used by other feature creation 
processes, so those functions are shared by being put in a module of their own, here the module 'mention.py'.

Those functions are embedded in a tuple of data (referred later in the code as 'mention_feature_data', 
since those function are defined on a 'Mention' instance input). The first element of the tuple is 
a string defining the type of feature that this function is destined to be used by (such as 'numerical', 
'categorical' or 'multi_numeric'), the second element is the function itself, and the rest of the tuple
elements are elements that are specific to each feature instance type (such as 'column_names' for 
'multi_numeric' features, or 'possible_values' for 'categorical' features).

In the end, the only function that the user will use from this module is the 'prepare_features_creation' 
function, that return the collection of features creation functions to use, as well as the collection 
of group names pairs (whose role is to specify the groups of features for which a product feature shall 
be created for each pair of features that can be defined from the two groups).
"""

__all__ = ["prepare_features_creation",]

import itertools
from scipy import sparse
import numpy as np

from cortex.tools.ml_features import (NumericFeature, CategoricalFeature, MultiNumericFeature, 
                                        create_features_product, quantize_numeric_feature, 
                                        CATEGORICAL_FEATURE_TYPE, NUMERIC_FEATURE_TYPE)

from cortex.parameters.mention_data_tags import (FIRST_PERSON_TAG, SECOND_PERSON_TAG, 
                                                  THIRD_PERSON_TAG, SINGULAR_NUMBER_TAG, 
                                                  PLURAL_NUMBER_TAG,  UNKNOWN_VALUE_TAG,
                                                  GRAM_TYPE_TAGS, GRAM_SUBTYPE_TAGS)

from ..knowledge import lexicon
from ..mention_features import FRENCH_MENTION_FEATURES as MENTION_FEATURES
from ..parameters import modified_CC_treebank_constituent_tags as POS_tags
from ..parameters import base_named_entities_data_tags as NE_tags


from .mention import (create_mention_feature_from_data, 
                    _get_named_entity_attributes, 
                    _classify_ne_type_tag_if_possible, 
                    _classify_ne_subtype_tag_if_possible, 
                    named_entity_subtype_cluster_data, 
                    is_enumeration_data, 
                    gram_type_data, gram_subtype_data,
                    named_entity_type_cluster_data,  
                    )
from .mention_singleton_features import (grammatical_role_features_data as singleton_grammatical_role_features_data, 
                                       internal_morphosyntactic_features_data as singleton_internal_morphosyntactic_features_data,
                                       semantic_environment_features_data as singleton_semantic_environment_features_data,
                                       word_count_features_data as singleton_word_count_features_data,
                                       linguistic_form_features_data as singleton_linguistic_form_features_data, 
                                       gram_type_features_data, 
                                       gram_subtype_features_data, 
                                       #syntactic_features_data, 
                                       normalized_syntactic_features_data, 
                                       )


#############
### Utils ###
#############
_get_left_mention_from_mention_pair_sample_fct = lambda mention_pair_sample: mention_pair_sample.left
_get_right_mention_from_mention_pair_sample_fct = lambda mention_pair_sample: mention_pair_sample.right
def _create_mention_pair_sample_feature_from_mention_feature_data(feature_data, name, left=True, 
                                                                 quantize=False, strict=False):
    """ Is a wrapper around the 'create_mention_feature_from_data', when feature is destined to be 
    fed MentionPairSample instance as input, but the feature's function need to work with only 
    either the left or the right mention of the mention pair. That is, creates a feature that is 
    destined to take a MentionPairSample instance as an input.
    
    Args:
        feature_data: tuple of elements needed to create a _Feature child class instance. Cf the 
            module's documentation.
        name: name to give the created _Feature child class instance
        left: boolean; specify whether the left mention of a mention pair should be used as input 
            to the underlying function, or the right mention
        quantize: whether or not to quantize a Numeric or MultiNumeric feature during its creation. 
            If True, the numeric values contained in the 'QUANTIFICATION_THRESHOLD_VALUES' module variable 
            will be used as threshold to define the bins; and the output feature will be a CategoricalFeature 
            instance.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of 

    Returns:
        a _Feature child class instance, that takes a MentionPairSample instance as input
    """
    get_mention_from_sample_fct = _get_right_mention_from_mention_pair_sample_fct
    if left:
        get_mention_from_sample_fct = _get_left_mention_from_mention_pair_sample_fct
    quantification_threshold_values = QUANTIFICATION_THRESHOLD_VALUES if quantize else None
    return create_mention_feature_from_data(get_mention_from_sample_fct, feature_data, 
                                            name, strict=strict, 
                                            quantification_threshold_values=quantification_threshold_values)

def _create_categorical_mention_pair_feature_from_mention_feature_data(feature_data, name, strict=False):
    """ Automatize the creation of a CategoricalFeature instance that take a MentionPairSample as 
    input, when the output value is just the combination of the output of a CategoricalFeature 
    applied to both mentions of the MentionPairSample. That is, creates a feature that is destined 
    to take a MentionPairSample instance as an input.
    
    Args:
        feature_data: 'mention_feature_data' whose type is 'categorical'. Cf the module's documentation.
        name: name to give the created _Feature child class instance
        strict: a boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of

    Returns:
        a CategoricalFeature instance
    
    Raises:
        ValueError: if the input 'feature_data' is not about creating a CategoricalFeature
    """
    feature_type = feature_data[0]
    if feature_type == CATEGORICAL_FEATURE_TYPE:
        base_format_string = "{}-{}"
        compute_fct_, possible_values_ = feature_data[1:]
        possible_values = tuple(sorted(base_format_string.format(v1,v2) for v1,v2 in itertools.product(possible_values_, repeat=2)))
        compute_fct = lambda mention_pair_sample: base_format_string.format(compute_fct_(mention_pair_sample.left), compute_fct_(mention_pair_sample.right))
        feature = CategoricalFeature(name, compute_fct, possible_values, strict=strict)
    else:
        msg = "Bad feature type '{}', must be one of the following: {}."
        msg = msg.format(feature_type, (CATEGORICAL_FEATURE_TYPE,))
        raise ValueError(msg)
    return feature

def _create_mention_data_based_features(mention_features_data, feature_name2feature, feature_names, 
                                       prefix, quantize, strict, not_to_use_data_names=None):
    """ Function whose role is to automatize the process of carrying out the parametrized creation 
    of a feature, and to carry it out for several 'features that take a MentionPairSample' at once.
    
    Once a feature has been created, it will added to the input 'feature_name2feature' map.
    
    Args:
        mention_features_data: a collection of ('feature_base_name, 'mention_feature_data') pairs; 
            see module's documentation. 'feature_base_name' is a string that will be used to define the name 
            attributed to the feature built from using the corresponding 'mention_feature_data'.
        feature_name2feature: the 'feature name => feature' map which the created feature must be added to
        feature_names: list of feature names to which the name of the created feature must be added
        prefix: string, prefix to use when defining 
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
        not_to_use_data_names: collection of 'feature_base_name' strings whose corresponding 
            feature creation data the user does not want to use.
    """
    if not_to_use_data_names is None:
        not_to_use_data_names = tuple()
    not_to_use_data_names = set(not_to_use_data_names)
    mention_features_data = tuple(t for t in mention_features_data if t[0] not in not_to_use_data_names)
    # Create first for left, then for right, so as to maintain a consistent ordering in the respective features names
    for base_name, data in mention_features_data:
        name = "{}_{}Left".format(prefix, base_name)
        feature = _create_mention_pair_sample_feature_from_mention_feature_data(data, name, left=True, quantize=quantize, strict=strict)
        _post_feature_creation(feature, feature_name2feature, feature_names)
    for base_name, data in mention_features_data:
        name = "{}_{}Right".format(prefix, base_name)
        feature = _create_mention_pair_sample_feature_from_mention_feature_data(data, name, left=False, quantize=quantize, strict=strict)
        _post_feature_creation(feature, feature_name2feature, feature_names)

QUANTIFICATION_THRESHOLD_VALUES = (0,1,2,3,6,11)#(3,6,11)

def _post_feature_creation(feature, feature_maps, *args):
    """ Automatizes the process of adding a newly created feature to the structure tasked with 
    identifying and storing them, as well as registering the feature as being part of any number of 
    group(s).
    
    Args:
        feature: a _Feature child class instance
        feature_maps: the 'feature name => feature' map to which the input feature must be added
        *args: a collection of lists, each list representing a group to which the feature must 
            be added (represented by its name)
    """
    name = feature.name
    feature_maps[name] = feature
    for group_feature_names in args:
        group_feature_names.append(name)




#############################################
# Function whose role is to create features #
#############################################
### Mention types features ###
def _create_mention_gram_type_features(feature_name2feature, group_name2feature_names, quantize=False, 
                                      strict=False):
    """ Creates, in a parametrized fashion, feature(s) representing some kinds of 'grammatical type'    
    information about the mentions of a pair, and add them to the input feature definition and group 
    definition structures.
    
    Features with the following respective base names will be 
    created:
        - "LeftGramType"
        - "LeftGramSubType"
        - "RightGramType"
        - "RightGramSubType"
    The created feature related to 'gram type' will constitute a group named "C_GramType".
    The created feature related to 'gram subtype' will constitute a group named "C_GramSubType".
    
    Args:
        feature_name2feature: a 'feature name => feature' map, to which created feature shall be 
            added for future easy access
        group_name2feature_names: a 'group name => feature names' map, used to define the which 
            of the features created by this function that shall be considered as a group
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
    """
    ## GramType
    C_GramType_feature_names = []
    _create_mention_data_based_features(gram_type_features_data, feature_name2feature, 
                                       C_GramType_feature_names, "C", quantize, strict)
    group_name2feature_names["C_GramType"] = C_GramType_feature_names
    
    ## GramSubType
    C_GramSubType_feature_names = []
    _create_mention_data_based_features(gram_subtype_features_data, feature_name2feature, 
                                       C_GramSubType_feature_names, "C", quantize, strict)
    group_name2feature_names["C_GramSubType"] = C_GramSubType_feature_names



### Mention pair type features ###
def _create_mention_pair_type_features(feature_name2feature, group_name2feature_names, quantize=False, 
                                      strict=False):
    """ Creates, in a parametrized fashion, feature(s) representing some kinds of 'grammatical type'    
    information, but crossed together, about the mentions of a pair, and add them to the input feature 
    definition and group definition structures.
    
    Features with the following respective base names will be 
    created:
        - "GramTypePair"
        - "GramSubtypePair"
    The created feature will constitute a group named "C_MentionType".
    
    Args:
        feature_name2feature: a 'feature name => feature' map, to which created feature shall be 
            added for future easy access
        group_name2feature_names: a 'group name => feature names' map, used to define the which 
            of the features created by this function that shall be considered as a group
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
    """
    C_MentionPairType_feature_names = []
    
    ## GramTypePair
    name = "C_GramTypePair"
    data = gram_type_data
    feature = _create_categorical_mention_pair_feature_from_mention_feature_data(data, name, strict=strict)
    _post_feature_creation(feature, feature_name2feature, C_MentionPairType_feature_names)
    
    ## GramSubtypePair
    name = "C_GramSubtypePair"
    data = gram_subtype_data
    feature = _create_categorical_mention_pair_feature_from_mention_feature_data(data, name, strict=strict)
    _post_feature_creation(feature, feature_name2feature, C_MentionPairType_feature_names)
    
    group_name2feature_names["C_MentionType"] = C_MentionPairType_feature_names



### String relation features ###
def _create_string_relation_features(feature_name2feature, group_name2feature_names, quantize=False, 
                                    strict=False):
    """ Creates, in a parametrized fashion, feature(s) representing some kinds of 'string relations'  
    information about the mentions of a pair, and add them to the input feature definition and group 
    definition structures.
    
    Features with the following respective base names will be 
    created:
        - "HeadMatch"
        - "TextMatch"
        - "SubstringLeftRight"
        - "SubstringRightLeft"
        - "ModifierMatchLeftRight"
        - "ModifierMatchRightLeft"
        - "IsAlias"
        - "IndefiniteRight"
    The name of the group that the created features will constitute is the 
    following:
        - "C_StringRelation"
    
    Args:
        feature_name2feature: a 'feature name => feature' map, to which created feature shall be 
            added for future easy access
        group_name2feature_names: a 'group name => feature names' map, used to define the which 
            of the features created by this function that shall be considered as a group
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
    """
    C_StringRelation_feature_names = []
    
    ## HeadMatch
    name = "C_HeadMatch"
    compute_fct = lambda mention_pair_sample: int(mention_pair_sample.left.head_raw_text.lower() == mention_pair_sample.right.head_raw_text.lower())
    feature = NumericFeature(name, compute_fct)
    _post_feature_creation(feature, feature_name2feature, C_StringRelation_feature_names)
    
    ## TextMatch
    name = "C_TextMatch"
    compute_fct = lambda mention_pair_sample: int(mention_pair_sample.left.raw_text.lower() == mention_pair_sample.right.raw_text.lower())
    feature = NumericFeature(name, compute_fct)
    _post_feature_creation(feature, feature_name2feature, C_StringRelation_feature_names)
    
    ## Substring
    def _is_substring(encompassed_mention, encompassing_mention):
        return encompassed_mention.head_raw_text.lower() in encompassing_mention.head_raw_text.lower()
    ##
    name = "C_SubstringLeftRight"
    compute_fct = lambda mention_pair_sample: int(_is_substring(mention_pair_sample.left, mention_pair_sample.right))
    feature = NumericFeature(name, compute_fct)
    _post_feature_creation(feature, feature_name2feature, C_StringRelation_feature_names)
    ##
    name = "C_SubstringRightLeft"
    compute_fct = lambda mention_pair_sample: int(_is_substring(mention_pair_sample.right, mention_pair_sample.left))
    feature = NumericFeature(name, compute_fct)
    _post_feature_creation(feature, feature_name2feature, C_StringRelation_feature_names)
    
    ## ModifierMatch
    def _modifier_match(left, right):
        s,_ = left.extent
        hs,_ = left.head_extent
        left_mod_text = left.raw_text[:hs-s+1].lower()
        right_text = right.raw_text.lower()
        return left_mod_text==right_text
    ##
    name = "C_ModifierMatchLeftRight"
    compute_fct = lambda mention_pair_sample: int(_modifier_match(*mention_pair_sample.pair))
    feature = NumericFeature(name, compute_fct)
    _post_feature_creation(feature, feature_name2feature, C_StringRelation_feature_names)
    ##
    name = "C_ModifierMatchRightLeft"
    compute_fct = lambda mention_pair_sample: int(_modifier_match(*mention_pair_sample.pair[::-1]))
    feature = NumericFeature(name, compute_fct)
    _post_feature_creation(feature, feature_name2feature, C_StringRelation_feature_names)
    
    ## IsAlias
    name = "C_IsAlias"
    def C_IsAlias_fct(mention_pair_sample):
        if MENTION_FEATURES.is_name(mention_pair_sample.left) and MENTION_FEATURES.is_name(mention_pair_sample.right):
            return str(mention_pair_sample.left.raw_text != mention_pair_sample.right.raw_text \
                       and MENTION_FEATURES.abbrev(mention_pair_sample.left) == MENTION_FEATURES.abbrev(mention_pair_sample.right))
        return "N/A" 
    compute_fct = C_IsAlias_fct
    possible_values = (str(True), str(False), "N/A")
    feature = CategoricalFeature(name, compute_fct, possible_values, strict=strict)
    _post_feature_creation(feature, feature_name2feature, C_StringRelation_feature_names)
    
    ## Indefinite (right is indefinite NP and not in an appositive relationship)
    # FIXME: Should there be a link with the 'is_indefinite' fct in the EnglishMentionFeature class?
    name = "C_IndefiniteRight"
    def C_IndefiniteRight_fct(mention_pair_sample):
        is_indefinite_right = False
        appos = False
        if MENTION_FEATURES.is_nominal(mention_pair_sample.right) or MENTION_FEATURES.is_nominal(mention_pair_sample.left): # At least one must be nominal
            appos = MENTION_FEATURES.is_appositive(mention_pair_sample.right, mention_pair_sample.left)
        if not appos:
            # Is the right mention indefinite?
            token = mention_pair_sample.right.tokens[0]
            if (token.POS_tag == POS_tags.DETERMINER_TAG and token.raw_text.lower() in ("un", "une", "des")):
                is_indefinite_right = True
        return is_indefinite_right
    compute_fct = lambda mention_pair_sample: int(C_IndefiniteRight_fct(mention_pair_sample))
    feature = NumericFeature(name, compute_fct)
    _post_feature_creation(feature, feature_name2feature, C_StringRelation_feature_names)
    
    group_name2feature_names["C_StringRelation"] = C_StringRelation_feature_names



### Semantic features ###
def _create_semantic_features(feature_name2feature, group_name2feature_names, quantize=False, 
                             strict=False):
    """ Creates, in a parametrized fashion, feature(s) representing some kinds of 'semantic'  
    information about the mentions of a pair, and add them to the input feature definition and group 
    definition structures.
    
    Features with the following respective base names will be 
    created:
        - "GenderMatch"
        - "NumberMatch"
        - "GenderAndNumberMatch"
        - "Synonyms"
        - "Hypernyms"
        - "BothAnimate"
        - "BothQuotationSpeaker"
    The name of the group that the created features will constitute is the 
    following:
        - "C_Semantic"
    
    Args:
        feature_name2feature: a 'feature name => feature' map, to which created feature shall be 
            added for future easy access
        group_name2feature_names: a 'group name => feature names' map, used to define the which 
            of the features created by this function that shall be considered as a group
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
    """
    C_Semantic_feature_names = []
    
    ## Gender Match
    name = "C_GenderMatch"
    compute_fct = lambda mention_pair_sample: MENTION_FEATURES.gender_agr(mention_pair_sample.left, mention_pair_sample.right)
    feature = NumericFeature(name, compute_fct)
    _post_feature_creation(feature, feature_name2feature, C_Semantic_feature_names)
    
    ## Number Match
    name = "C_NumberMatch"
    compute_fct = lambda mention_pair_sample: MENTION_FEATURES.number_agr(mention_pair_sample.left, mention_pair_sample.right)
    feature = NumericFeature(name, compute_fct)
    _post_feature_creation(feature, feature_name2feature, C_Semantic_feature_names)
    
    ## Gender & Number Match
    name = "C_GenderAndNumberMatch"
    compute_fct = lambda mention_pair_sample: MENTION_FEATURES.morph_agr(mention_pair_sample.left, mention_pair_sample.right)
    feature = NumericFeature(name, compute_fct)
    _post_feature_creation(feature, feature_name2feature, C_Semantic_feature_names)
    
    ## Synonyms
    name = "C_Synonyms"
    def C_Synonyms_fct(mention_pair_sample):
        left_syn = mention_pair_sample.left.wn_synonyms
        right_syn = mention_pair_sample.right.wn_synonyms
        match = False
        if left_syn is not None and right_syn is not None:
            match = bool(set(left_syn).intersection(set(right_syn)))
        return match
    compute_fct = lambda mention_pair_sample: int(C_Synonyms_fct(mention_pair_sample))
    feature = NumericFeature(name, compute_fct)
    _post_feature_creation(feature, feature_name2feature, C_Semantic_feature_names)
    
    ## Antonyms
    # TODO: 
    
    ## Hypernyms
    name = "C_Hypernyms"
    def C_Hypernyms_fct(mention_pair_sample):
        left_hyper = mention_pair_sample.left.wn_hypernyms
        right_hyper = mention_pair_sample.right.wn_hypernyms
        match = False
        if left_hyper is not None and right_hyper is not None:
            match = bool(set(left_hyper).intersection(set(right_hyper)))
        return match
    compute_fct = lambda mention_pair_sample: int(C_Hypernyms_fct(mention_pair_sample))
    feature = NumericFeature(name, compute_fct)
    _post_feature_creation(feature, feature_name2feature, C_Semantic_feature_names)
    
    ## Both animate
    name = "C_BothAnimate"
    def C_BothAnimate_fct(mention_pair_sample):
        is_animate1 = MENTION_FEATURES.is_animated(mention_pair_sample.left)
        if is_animate1 == UNKNOWN_VALUE_TAG:
            return UNKNOWN_VALUE_TAG
        is_animate2 = MENTION_FEATURES.is_animated(mention_pair_sample.right)
        if is_animate2 == UNKNOWN_VALUE_TAG:
            return UNKNOWN_VALUE_TAG
        return str(is_animate1 and is_animate2)
    compute_fct = C_BothAnimate_fct
    possible_values = (str(True), str(False), UNKNOWN_VALUE_TAG)
    feature = CategoricalFeature(name, compute_fct, possible_values, strict=strict)
    _post_feature_creation(feature, feature_name2feature, C_Semantic_feature_names)
    
    ## Both Speak
    name = "C_BothQuotationSpeaker"
    def C_BothQuotationSpeaker(mention_pair_sample):
        quotations = mention_pair_sample.document.quotations
        if quotations is None:
            return UNKNOWN_VALUE_TAG
        left_mention, right_mention = mention_pair_sample.pair
        left_is_quotation_speaker = False
        right_is_quotation_speaker = False
        for quotation in quotations:
            if quotation.speaker_mention is left_mention:
                left_is_quotation_speaker = True
            if quotation.speaker_mention is right_mention:
                right_is_quotation_speaker = True
            if left_is_quotation_speaker and right_is_quotation_speaker:
                return str(True)
        return str(False)
    compute_fct = C_BothQuotationSpeaker
    possible_values = (str(True), str(False), UNKNOWN_VALUE_TAG)
    feature = CategoricalFeature(name, compute_fct, possible_values, strict=strict)
    _post_feature_creation(feature, feature_name2feature, C_Semantic_feature_names)
    
    ## Both contain a same family name
    # TODO: FAMILY_NAMES_SET
    '''
    name = "C_SameFamilyName"
    def C_SameFamilyName_fct(mention_pair_sample):
        family_set = FAMILY_NAMES_SET
        left_set = set()
        right_set = set()
        for token in mention_pair_sample.left.tokens: # Try head tokens
            text = token.raw_text
            if text in family_set:
                left_set.add(text)
        for token in mention_pair_sample.right.tokens: # Try head tokens
            text = token.raw_text
            if text in family_set:
                right_set.add(text)
        return bool(left_set.intersection(right_set))
    compute_fct = lambda mention_pair_sample: int(C_SameFamilyName_fct(mention_pair_sample))
    feature1 = NumericFeature(name, compute_fct)
    feature2 = feature_name2feature["C_GramTypePair"]
    feature = create_features_product(feature1, feature2, new_name=name)
    _post_feature_creation(feature, feature_name2feature, C_Semantic_feature_names)
    '''
    group_name2feature_names["C_Semantic"] = C_Semantic_feature_names



### Relative location features ###
def _create_relative_location_features(feature_name2feature, group_name2feature_names, quantize=False, 
                                      strict=False):
    """ Creates, in a parametrized fashion, feature(s) representing some kinds of 'relative location'  
    information about the mentions of a pair, and add them to the input feature definition and group 
    definition structures.
    
    Features with the following respective base names will be 
    created:
        - "InclusionLeftRight"
        - "InclusionRightLeft"
        - "FinishLeftRight"
        - "FinishRightLeft"
        - "Apposition"
        - "IsCopula"
        - "RelativePronoun"
        - "NormalizedMentionDistance"
        - "NormalizedSentenceDistance"
    The name of the group that the created features will constitute is the 
    following:
        - "C_RelativeLocation".
    The features whose base name is in ["NormalizedMentionDistance", "NormalizedSentenceDistance"] 
    will also constitute a group, named "C_Distance".
    
    Args:
        feature_name2feature: a 'feature name => feature' map, to which created feature shall be 
            added for future easy access
        group_name2feature_names: a 'group name => feature names' map, used to define the which 
            of the features created by this function that shall be considered as a group
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
    """
    C_RelativeLocation_feature_names = []
    C_Distance_feature_names = []
    
    ## Inclusion
    ##
    name = "C_InclusionLeftRight"
    compute_fct = lambda mention_pair_sample: int(mention_pair_sample.left.includes(mention_pair_sample.right))
    feature = NumericFeature(name, compute_fct)
    _post_feature_creation(feature, feature_name2feature, C_RelativeLocation_feature_names)
    ##
    name = "C_InclusionRightLeft"
    compute_fct = lambda mention_pair_sample: int(mention_pair_sample.right.includes(mention_pair_sample.left))
    feature = NumericFeature(name, compute_fct)
    _post_feature_creation(feature, feature_name2feature, C_RelativeLocation_feature_names)
    
    ## Finish
    ##
    name = "C_FinishLeftRight"
    compute_fct = lambda mention_pair_sample: int(mention_pair_sample.left.finishes(mention_pair_sample.right))
    feature = NumericFeature(name, compute_fct)
    _post_feature_creation(feature, feature_name2feature, C_RelativeLocation_feature_names)
    ##
    name = "C_FinishRightLeft"
    compute_fct = lambda mention_pair_sample: int(mention_pair_sample.right.finishes(mention_pair_sample.left))
    feature = NumericFeature(name, compute_fct)
    _post_feature_creation(feature, feature_name2feature, C_RelativeLocation_feature_names)
    
    ## Apposition
    name = "C_Apposition"
    compute_fct = lambda mention_pair_sample: int(MENTION_FEATURES.is_appositive(mention_pair_sample.right, mention_pair_sample.left) if MENTION_FEATURES.is_nominal(mention_pair_sample.right) or MENTION_FEATURES.is_nominal(mention_pair_sample.left) else False) # At least one must be nominal for the test to make sense
    feature = NumericFeature(name, compute_fct)
    _post_feature_creation(feature, feature_name2feature, C_RelativeLocation_feature_names)
    
    ## Copula
    name = "C_IsCopula"
    compute_fct = lambda mention_pair_sample: int(MENTION_FEATURES.is_copula(mention_pair_sample.left, mention_pair_sample.right))
    feature = NumericFeature(name, compute_fct)
    _post_feature_creation(feature, feature_name2feature, C_RelativeLocation_feature_names)
    
    ## Relative Pronoun
    name = "C_RelativePronoun"
    compute_fct = lambda mention_pair_sample: int(MENTION_FEATURES.is_appositive(mention_pair_sample.right, mention_pair_sample.left) if MENTION_FEATURES.is_expanded_pronoun(mention_pair_sample.right) else False)
    feature = NumericFeature(name, compute_fct)
    _post_feature_creation(feature, feature_name2feature, C_RelativeLocation_feature_names)
    
    ## Mention Distance
    def C_MentionDistance_fct(mention_pair_sample):
        return MENTION_FEATURES.mention_distance(mention_pair_sample.left, mention_pair_sample.right)
    def C_NormalizedMentionDistance_fct(mention_pair_sample):
        distance = MENTION_FEATURES.mention_distance(mention_pair_sample.left, mention_pair_sample.right)
        maximal_distance = len(mention_pair_sample.left.document.mentions)-1
        normalized_distance = distance / maximal_distance if maximal_distance else 0. # If maximal_distance == 0, that means there is only one mention in the Document
        return normalized_distance
    #name = "C_MentionDistance"
    #compute_fct = C_MentionDistance_fct
    name = "C_NormalizedMentionDistance"
    compute_fct = C_NormalizedMentionDistance_fct
    feature = NumericFeature(name, compute_fct)
    _post_feature_creation(feature, feature_name2feature, C_RelativeLocation_feature_names, C_Distance_feature_names)
    
    ## Sentence Distance
    def C_SentenceDistance_fct(mention_pair_sample):
        return MENTION_FEATURES.sentence_distance(mention_pair_sample.left, mention_pair_sample.right)
    def C_NormalizedSentenceDistance_fct(mention_pair_sample):
        distance = MENTION_FEATURES.sentence_distance(mention_pair_sample.left, mention_pair_sample.right)
        maximal_distance = len(mention_pair_sample.left.document.sentences)-1
        normalized_distance = distance / maximal_distance if maximal_distance else 0. # If maximal_distance == 0, that means there is only one mention in the Document 
        return normalized_distance
    #name = "C_SentenceDistance"
    #compute_fct = C_SentenceDistance_fct
    name = "C_NormalizedSentenceDistance"
    compute_fct = C_NormalizedSentenceDistance_fct
    feature = NumericFeature(name, compute_fct)
    _post_feature_creation(feature, feature_name2feature, C_RelativeLocation_feature_names, C_Distance_feature_names)
    
    group_name2feature_names["C_RelativeLocation"] = C_RelativeLocation_feature_names
    group_name2feature_names["C_Distance"] = C_Distance_feature_names



### Named entities type and subtype features ###
named_entity_type_cluster_features_data = (("NamedEntityTypeCluster",named_entity_type_cluster_data)
                                           ,)
def _create_named_entities_type_features(feature_name2feature, group_name2feature_names, 
                                        quantize=False, strict=False):
    """ Creates, in a parametrized fashion, feature(s) representing some kinds of 'named entity type match'    
    information about the mentions of a pair, and add them to the input feature definition and group 
    definition structures.
    
    Features with the following respective base names will be 
    created:
        - "NamedEntityTypeClusterMatch"
        - "NamedEntityTypeMatch"
    The name of the group that the created features will constitute is the 
    following:
        - "C_NamedEntityTypeMatch"
    
    Args:
        feature_name2feature: a 'feature name => feature' map, to which created feature shall be 
            added for future easy access
        group_name2feature_names: a 'group name => feature names' map, used to define the which 
            of the features created by this function that shall be considered as a group
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
    """
    ## Named entity type cluster
    C_NamedEntityType_feature_names = []
    _create_mention_data_based_features(named_entity_type_cluster_features_data, feature_name2feature, 
                                       C_NamedEntityType_feature_names, "C", quantize, strict)
    group_name2feature_names["C_NamedEntityType"] = C_NamedEntityType_feature_names
    
    
    C_NamedEntityTypeMatch_feature_names = []
    
    ## Match cluster
    name = "C_NamedEntityTypeClusterMatch"
    def C_NamedEntityTypeClusterMatch(mention_pair_sample):
        left_cluster = _classify_ne_type_tag_if_possible(mention_pair_sample.left)
        right_cluster = _classify_ne_type_tag_if_possible(mention_pair_sample.right)
        if left_cluster == "N/A" or right_cluster == "N/A":
            return "N/A"
        return str(left_cluster == right_cluster)
    compute_fct = C_NamedEntityTypeClusterMatch
    possible_values = (str(True), str(False), "N/A")
    feature = CategoricalFeature(name, compute_fct, possible_values, strict=strict)
    _post_feature_creation(feature, feature_name2feature, C_NamedEntityTypeMatch_feature_names)
    
    ## Match
    name = "C_NamedEntityTypeMatch"
    def C_NamedEntityTypeMatch(mention_pair_sample):
        left_type, _ = _get_named_entity_attributes(mention_pair_sample.left)
        right_type, _ = _get_named_entity_attributes(mention_pair_sample.right)
        if left_type is not None and right_type is not None:
            return str(left_type==right_type)
        return "N/A"
    compute_fct = C_NamedEntityTypeMatch
    possible_values = (str(True), str(False), "N/A")
    feature = CategoricalFeature(name, compute_fct, possible_values, strict=strict)
    _post_feature_creation(feature, feature_name2feature, C_NamedEntityTypeMatch_feature_names)
    
    group_name2feature_names["C_NamedEntityTypeMatch"] = C_NamedEntityTypeMatch_feature_names



has_named_entity_subtype_data = (NUMERIC_FEATURE_TYPE, lambda mention: int(_get_named_entity_attributes(mention)[1] is not None), False)
named_entity_subtype_cluster_features_data = (("HasNamedEntitySubtype",has_named_entity_subtype_data)
                                              ,)
def _create_named_entities_subtype_features(feature_name2feature, group_name2feature_names, 
                                           quantize=False, strict=False):
    """ Creates, in a parametrized fashion, feature(s) representing some kinds of 'named entity subtype match'    
    information about the mentions of a pair, and add them to the input feature definition and group 
    definition structures.
    
    Features with the following respective base names will be 
    created:
        - "NamedEntitySubTypeClusterMatch"
        - "NamedEntitySubTypeMatch"
    The name of the group that the created features will constitute is the 
    following:
        - "C_NamedEntitySubTypeMatch"
    
    Args:
        feature_name2feature: a 'feature name => feature' map, to which created feature shall be 
            added for future easy access
        group_name2feature_names: a 'group name => feature names' map, used to define the which 
            of the features created by this function that shall be considered as a group
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
    """
    C_NamedEntitySubType_feature_names = []
    
    ## Named entity subtype cluster
    _create_mention_data_based_features(named_entity_subtype_cluster_features_data, feature_name2feature, 
                                       C_NamedEntitySubType_feature_names, "C", quantize, strict)
    
    ## Match cluster
    name = "C_NamedEntitySubTypeClusterMatch"
    def C_NamedEntitySubTypeClusterMatch(mention_pair_sample):
        left_cluster = _classify_ne_subtype_tag_if_possible(mention_pair_sample.left)
        right_cluster = _classify_ne_subtype_tag_if_possible(mention_pair_sample.right)
        if left_cluster == "N/A" or right_cluster == "N/A":
            return "N/A"
        return str(left_cluster == right_cluster)
    compute_fct = C_NamedEntitySubTypeClusterMatch
    possible_values = (str(True), str(False), "N/A")
    feature = CategoricalFeature(name, compute_fct, possible_values, strict=strict)
    _post_feature_creation(feature, feature_name2feature, C_NamedEntitySubType_feature_names)
    
    ## Match
    name = "C_NamedEntitySubtypeMatch"
    def C_EntitySubtypeMatch_fct(mention_pair_sample):
        _, left_subtype = _get_named_entity_attributes(mention_pair_sample.left)
        _, right_subtype = _get_named_entity_attributes(mention_pair_sample.right)
        if left_subtype is not None and right_subtype is not None:
            return str(left_subtype==right_subtype)
        return "N/A"
    compute_fct = C_EntitySubtypeMatch_fct
    possible_values = (str(True), str(False), "N/A")
    feature = CategoricalFeature(name, compute_fct, possible_values, strict=strict)
    _post_feature_creation(feature, feature_name2feature, C_NamedEntitySubType_feature_names)
    
    group_name2feature_names["C_NamedEntitySubType"] = C_NamedEntitySubType_feature_names



### Named entities pair types features ###
def _create_named_entities_pair_types_features(feature_name2feature, group_name2feature_names, 
                                              quantize=False, strict=False):
    """ Creates, in a parametrized fashion, feature(s) representing some kinds of 'named entity type and subtype'    
    information, but crossed together, about the mentions of a pair, and add them to the input feature 
    definition and group definition structures.
    
    Features with the following respective base names will be 
    created:
        - "NamedEntityPairClusterType"
        - "NamedEntityPairClusterSubtype"
    The name of the group that the created features will constitute is the 
    following:
        - "C_NamedEntityPairType"
    
    Args:
        feature_name2feature: a 'feature name => feature' map, to which created feature shall be 
            added for future easy access
        group_name2feature_names: a 'group name => feature names' map, used to define the which 
            of the features created by this function that shall be considered as a group
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
    """
    C_NamedEntityPairType_feature_names = []
    
    ## Type
    name = "C_NamedEntityPairClusterType"
    data = named_entity_type_cluster_data
    feature = _create_categorical_mention_pair_feature_from_mention_feature_data(data, name, strict=strict)
    _post_feature_creation(feature, feature_name2feature, C_NamedEntityPairType_feature_names)
    
    ## Subtype
    name = "C_NamedEntityPairClusterSubtype"
    data = named_entity_subtype_cluster_data
    feature = _create_categorical_mention_pair_feature_from_mention_feature_data(data, name, strict=strict)
    _post_feature_creation(feature, feature_name2feature, C_NamedEntityPairType_feature_names)
    
    group_name2feature_names["C_NamedEntityPairType"] = C_NamedEntityPairType_feature_names


### Syntax features ###
def _create_syntax_features(feature_name2feature, group_name2feature_names, quantize=False, strict=False):
    """ Creates, in a parametrized fashion, feature(s) representing some kinds of 'syntactic'    
    information about the mentions of a pair, and add them to the input feature definition and group 
    definition structures.
    
    Features with the following respective base names will be 
    created:
        - "NormalizedParseDistance"
        - "SentenceTypes"
        - "NormalizedNPAboveNb"
        - "NormalizedPPAboveNb"
        - "NormalizedVPAboveNb"
        - "HeadMatchGramType"
        - "HeadMatchGramSubType"
        - "IsEnum"
    The name of the group that the created features will constitute is the 
    following:
        - "C_Syntax"
    
    Args:
        feature_name2feature: a 'feature name => feature' map, to which created feature shall be 
            added for future easy access
        group_name2feature_names: a 'group name => feature names' map, used to define the which 
            of the features created by this function that shall be considered as a group
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
    """
    C_Syntax_feature_names = []
    
    ## Constituency tree distance
    def C_ParseDistance_fct(mention_pair_sample):
        return MENTION_FEATURES.constituency_tree_distance(mention_pair_sample.left, mention_pair_sample.right)
    def C_NormalizedParseDistance_fct(mention_pair_sample):
        distance = MENTION_FEATURES.constituency_tree_distance(mention_pair_sample.left, mention_pair_sample.right)
        maximal_distance = 2*mention_pair_sample.left.sentence.constituency_tree.depth
        normalized_distance = distance / maximal_distance
        return normalized_distance
    #name = C_ParseDistance"
    #compute_fct = C_ParseDistance_fct
    name = "C_NormalizedParseDistance"
    compute_fct = C_NormalizedParseDistance_fct
    feature = NumericFeature(name, compute_fct)
    _post_feature_creation(feature, feature_name2feature, C_Syntax_feature_names)
    
    ## Sentence types
    name = "C_SentenceTypes"
    def C_SentenceTypes_fct(mention_pair_sample):
        left_s_type = MENTION_FEATURES.get_sentence_type(mention_pair_sample.left)
        if left_s_type is None:
            left_s_type = UNKNOWN_VALUE_TAG
        right_s_type = MENTION_FEATURES.get_sentence_type(mention_pair_sample.right)
        if right_s_type is None:
            right_s_type = UNKNOWN_VALUE_TAG
        return "{}_{}".format(left_s_type, right_s_type)
    compute_fct = C_SentenceTypes_fct
    possible_values = tuple("{}_{}".format(v1,v2) for v1,v2 in itertools.product(tuple(sorted(tuple(POS_tags.SENTENCE_TAGS)+(UNKNOWN_VALUE_TAG,))), repeat=2))
    feature = CategoricalFeature(name, compute_fct, possible_values, strict=strict)
    _post_feature_creation(feature, feature_name2feature, C_Syntax_feature_names)
    
    ## Number of NP/PP/VP above mention
    #features_data = syntactic_features_data
    features_data = normalized_syntactic_features_data
    _create_mention_data_based_features(features_data, feature_name2feature, C_Syntax_feature_names, 
                                       "C", quantize, strict)
    
    ## C-command and reflexive/normal pronouns (from Luo et al 2005)
    # TODO: or to drop, for the nothing equivalent for the French?
    '''
    def C_c_Command_fct(mention_pair_sample, matrix=None, row_id=0, column_offset=0):
        if matrix is None:
            result = sparse.lil_matrix((1,7), dtype=np.int)
            C_Normalized_c_Command_fct(mention_pair_sample, matrix=result, row_id=0, column_offset=0)
            return result.tocsr()
        data = MENTION_FEATURES.get_c_command_and_reflexive_normal_pronouns_counts(mention_pair_sample.left, mention_pair_sample.right)
        if data is not None:
            pronoun_type, NP_nb, VP_nb, S_nb, _ = data #tot_nb
            offset = 0 if pronoun_type == "_refl" else 1
            matrix[row_id,column_offset+offset*3:column_offset+(offset+1)*3] = NP_nb, VP_nb, S_nb
        else:
            matrix[row_id,column_offset+6] = 1
        return matrix
    def C_Normalized_c_Command_fct(mention_pair_sample, matrix=None, row_id=0, column_offset=0):
        if matrix is None:
            result = sparse.lil_matrix((1,7), dtype=np.int)
            C_Normalized_c_Command_fct(mention_pair_sample, matrix=result, row_id=0, column_offset=0)
            return result.tocsr()
        data = MENTION_FEATURES.get_c_command_and_reflexive_normal_pronouns_counts(mention_pair_sample.left, mention_pair_sample.right)
        if data is not None:
            pronoun_type, NP_nb, VP_nb, S_nb, tot_nb = data
            offset = 0 if pronoun_type == "_refl" else 1
            NP_nb, VP_nb, S_nb = NP_nb/tot_nb, VP_nb/tot_nb, S_nb/tot_nb
            matrix[row_id,column_offset+offset*3:column_offset+(offset+1)*3] = NP_nb, VP_nb, S_nb
        else:
            matrix[row_id,column_offset+6] = 1
        return matrix
    #name = "C_c_Command"
    #compute_fct = C_c_Command_fct
    name = "C_Normalized_c_Command"
    compute_fct = C_Normalized_c_Command_fct
    column_names = ("NP_refl", "VP_refl", "S_refl", "NP_norm", "VP_norm", "S_norm", "N/A")
    feature = MultiNumericFeature(name, compute_fct, column_names)
    _post_feature_creation(feature, feature_name2feature, C_Syntax_feature_names)
    '''
    ## Dependency features (from Luo et al 2005, but modified so as to use gram type and gram subtype values instead of POS tag, so as to be able to control the feature space definition)
    name = "C_HeadMatchGramType"
    feature1 = feature_name2feature["C_GramTypeLeft"]
    feature2 = NumericFeature("", lambda mention_pair_sample: int(mention_pair_sample.left.head_tokens[-1].raw_text == mention_pair_sample.right.head_tokens[-1].raw_text))
    feature = create_features_product(feature1, feature2, new_name=name)
    _post_feature_creation(feature, feature_name2feature, C_Syntax_feature_names)
    name = "C_HeadMatchGramSubType"
    feature1 = feature_name2feature["C_GramSubTypeLeft"]
    feature2 = NumericFeature("", lambda mention_pair_sample: int(mention_pair_sample.left.head_tokens[-1].raw_text == mention_pair_sample.right.head_tokens[-1].raw_text))
    feature = create_features_product(feature1, feature2, new_name=name)
    _post_feature_creation(feature, feature_name2feature, C_Syntax_feature_names)
    
    ## Enumeration
    features_data = (("IsEnum",is_enumeration_data),)
    _create_mention_data_based_features(features_data, feature_name2feature, C_Syntax_feature_names, 
                                       "C", quantize, strict)
    
    group_name2feature_names["C_Syntax"] = C_Syntax_feature_names


### Anaphority features ###
def _get_entity_head_extents_attribute(mention_pair_sample):
    if not mention_pair_sample.entity_head_extents:
        msg = "Incorrect 'entity_head_extents' attribute value for '{}', must be a set-like value."
        msg = msg.format(mention_pair_sample)
        raise ValueError(msg)
    return mention_pair_sample.entity_head_extents
def _create_anaphoricity_features(feature_name2feature, group_name2feature_names, quantize=False, 
                                 strict=False):
    """ Creates, in a parametrized fashion, feature(s) representing some kinds of 'previously predicted anaphoricity'    
    information about the mentions of a pair, and add them to the input feature definition and group 
    definition structures.
    
    Features with the following respective base names will be 
    created:
        - "IsAnaphoricRight"
    The name of the group that the created features will constitute is the 
    following:
        - "C_Anaphoricity"
    
    Args:
        feature_name2feature: a 'feature name => feature' map, to which created feature shall be 
            added for future easy access
        group_name2feature_names: a 'group name => feature names' map, used to define the which 
            of the features created by this function that shall be considered as a group
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
    """
    C_Anaphoricity_feature_names = []
    ## 
    name = "C_IsAnaphoricRight"
    compute_fct = lambda mention_pair_sample: int(not mention_pair_sample.right.extent in _get_entity_head_extents_attribute(mention_pair_sample))
    feature = NumericFeature(name, compute_fct)
    _post_feature_creation(feature, feature_name2feature, C_Anaphoricity_feature_names)
    
    group_name2feature_names["C_Anaphoricity"] = C_Anaphoricity_feature_names


### Singleton ###
def _get_singleton_extents_attribute(mention_pair_sample):
    if not mention_pair_sample.singleton_extents:
        msg = "Incorrect 'singleton_extents' attribute value for '{}', must be a set-like value."
        msg = msg.format(mention_pair_sample)
        raise ValueError(msg)
    return mention_pair_sample.singleton_extents
def _create_singleton_features(feature_name2feature, group_name2feature_names, quantize=False, 
                              strict=False):
    """ Creates, in a parametrized fashion, feature(s) representing some kinds of 'previously predicted singleton'    
    information about the mentions of a pair, and add them to the input feature definition and group 
    definition structures.
    
    Features with the following respective base names will be 
    created:
        - "IsSingletonLeft"
        - "IsSingletonRight"
    The name of the group that the created features will constitute is the 
    following:
        - "C_Singleton"
    
    Args:
        feature_name2feature: a 'feature name => feature' map, to which created feature shall be 
            added for future easy access
        group_name2feature_names: a 'group name => feature names' map, used to define the which 
            of the features created by this function that shall be considered as a group
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
    """
    C_Singleton_feature_names = []
    ## 
    name = "C_IsSingletonLeft"
    compute_fct = lambda mention_pair_sample: int(mention_pair_sample.left.extent in _get_singleton_extents_attribute(mention_pair_sample))
    feature = NumericFeature(name, compute_fct)
    _post_feature_creation(feature, feature_name2feature, C_Singleton_feature_names)
    ##
    name = "C_IsSingletonRight"
    compute_fct = lambda mention_pair_sample: int(mention_pair_sample.right.extent in _get_singleton_extents_attribute(mention_pair_sample))
    feature = NumericFeature(name, compute_fct)
    _post_feature_creation(feature, feature_name2feature, C_Singleton_feature_names)
    
    group_name2feature_names["C_Singleton"] = C_Singleton_feature_names


### Speaker features ###
def _create_speaker_features(feature_name2feature, group_name2feature_names, quantize=False, strict=False):
    """ Creates, in a parametrized fashion, feature(s) representing some kinds of 'speaker'    
    information about the mentions of a pair, and add them to the input feature definition and group 
    definition structures.
    
    Features with the following respective base names will be 
    created:
        - "SameSpeakerPronoun"
    The name of the group that the created features will constitute is the 
    following:
        - "C_Speaker"
    
    Args:
        feature_name2feature: a 'feature name => feature' map, to which created feature shall be 
            added for future easy access
        group_name2feature_names: a 'group name => feature names' map, used to define the which 
            of the features created by this function that shall be considered as a group
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
    """
    C_Speaker_feature_names = []
    ##
    name = "C_SameSpeakerPronoun"
    def C_SameSpeakerPronoun_fct(mention_pair_sample, matrix=None, row_id=0, column_offset=0):
        if matrix is None:
            result = sparse.lil_matrix((1,7), dtype=np.float)
            C_SameSpeakerPronoun_fct(mention_pair_sample, matrix=result, row_id=0, column_offset=0)
            return result.tocsr()
        data = MENTION_FEATURES.get_same_speaker_pronoun_data(mention_pair_sample.left, mention_pair_sample.right)
        if data is not None:
            for i, value in enumerate(data):
                offset = int(not value)
                matrix[row_id,column_offset+i*2+offset] = 1.0
        else:
            matrix[row_id,column_offset+6] = 1
        return matrix
    compute_fct = C_SameSpeakerPronoun_fct
    column_names = ("1sg_True", "1sg_False", "1pl_True", "1pl_False", "2sg_True", "2sg_False", "N/A")
    feature = MultiNumericFeature(name, compute_fct, column_names)
    _post_feature_creation(feature, feature_name2feature, C_Speaker_feature_names)
    
    group_name2feature_names["C_Speaker"] = C_Speaker_feature_names


### Verb to noun features ###
# TODO: to remove? Still here to remember that such features could be created if we had the corresponding resources, and we supported verbal mentions
def _create_verb2noun_features(feature_name2feature, group_name2feature_names, quantize=False, strict=False):
    C_IsVerb2Noun_feature_names = []
    ##
    # TODO: lexicon.VERB2NOUN
    '''
    name = "C_IsVerb2Noun"
    def C_IsVerb2Noun_fct(mention_pair_sample):
        verb_text = None
        noun_text = None
        if MENTION_FEATURES.is_verb(mention_pair_sample.left) and MENTION_FEATURES.is_nominal(mention_pair_sample.right):
            verb_text = mention_pair_sample.left.head_tokens[-1].lemma.lower()
            noun_text = mention_pair_sample.right.head_tokens[-1].lemma.lower()
        elif MENTION_FEATURES.is_verb(mention_pair_sample.right) and MENTION_FEATURES.is_nominal(mention_pair_sample.left):
            verb_text = mention_pair_sample.right.head_tokens[-1].lemma.lower()
            noun_text = mention_pair_sample.left.head_tokens[-1].lemma.lower()
        if noun_text is None or verb_text is None:
            return "N/A"
        return str(noun_text in lexicon.VERB_TO_NOUN.get(verb_text, set()))
    compute_fct = C_IsVerb2Noun_fct
    possible_values = (str(True), str(False), "N/A")
    feature = CategoricalFeature(name, compute_fct, possible_values)
    _post_feature_creation(feature, feature_name2feature, C_IsVerb2Noun_feature_names)
    '''
    group_name2feature_names["C_IsVerb2Noun"] = C_IsVerb2Noun_feature_names


### Pronoun compatibility features ###
def _create_pronoun_compatibility_features(feature_name2feature, group_name2feature_names, 
                                          quantize=False, strict=False):
    """ Creates, in a parametrized fashion, feature(s) representing some kinds of 'pronoun compatibility'    
    information about the mentions of a pair, and add them to the input feature definition and group 
    definition structures.
    
    Features with the following respective base names will be 
    created:
        - "PronounCompat"
    The name of the group that the created features will constitute is the 
    following:
        - "C_ProComp"
    
    Args:
        feature_name2feature: a 'feature name => feature' map, to which created feature shall be 
            added for future easy access
        group_name2feature_names: a 'group name => feature names' map, used to define the which 
            of the features created by this function that shall be considered as a group
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
    """
    C_ProComp_feature_names = []
    ##
    name = "C_PronounCompat"
    def C_PronounCompat_fct(mention_pair_sample):
        mp = mn = None
        if MENTION_FEATURES.is_expanded_pronoun(mention_pair_sample.left) and not MENTION_FEATURES.is_expanded_pronoun(mention_pair_sample.right):
            mp = mention_pair_sample.left
            mn = mention_pair_sample.right
        if not MENTION_FEATURES.is_expanded_pronoun(mention_pair_sample.left) and MENTION_FEATURES.is_expanded_pronoun(mention_pair_sample.right):
            mp = mention_pair_sample.right
            mn = mention_pair_sample.left
        if mp is not None: # If indeed a pronoun/noun pair
            text_p = mp.raw_text.lower()
            mn_named_entity = mn.named_entity
            if mn_named_entity:
                type_n = mn_named_entity.type
                compatibility = False
                #if text_p in lexicon.EXPANDED_MONEY_PERCENT_NUMBER_PRONOUNS and type_n in NE_tags.MONEY_PERCENT_NUMBER_NAMED_ENTITY_TYPE_TAGS:
                #    compatibility = True
                if text_p in lexicon.DATE_TIME_PRONOUN_MENTIONS and type_n in NE_tags.DATE_NAMED_ENTITY_TYPE_TAGS:
                    compatibility = True
                if text_p in lexicon.EXPANDED_ORGANIZATION_PRONOUN_MENTIONS and type_n in NE_tags.ORGANIZATION_NAMED_ENTITY_TYPE_TAGS:
                    compatibility = True
                #if text_p in lexicon.EXPANDED_GPE_PRONOUNS and type_n in NE_tags.GPE_NAMED_ENTITY_TYPE_TAGS:
                #    compatibility = True
                if text_p in lexicon.EXPANDED_LOCATION_PRONOUN_MENTIONS and type_n in NE_tags.LOCATION_NAMED_ENTITY_TYPE_TAGS:
                    compatibility = True
                #if text_p in lexicon.EXPANDED_FACILITY_VEHICLE_WEAPON_PRONOUNS and type_n in NE_tags.FACILITY_VEHICLE_WEAPON_NAMED_ENTITY_TYPE_TAGS:
                #    compatibility = True
                return str(compatibility)
        return "N/A"
    compute_fct = C_PronounCompat_fct
    possible_values = (str(True), str(False), "N/A")
    feature = CategoricalFeature(name, compute_fct, possible_values)
    _post_feature_creation(feature, feature_name2feature, C_ProComp_feature_names)
    
    group_name2feature_names["C_ProComp"] = C_ProComp_feature_names


### Dictionary features ###
def _create_dictionary_features(feature_name2feature, group_name2feature_names, quantize=False, strict=False):
    C_CorefDict_feature_names = []
    ##
    # TODO: or to drop it, for the French?
    '''
    name = "C_CorefDict"
    def C_CorefDict_fct(mention_pair_sample, matrix=None, row_id=0, column_offset=0):
        if matrix is None:
            result = sparse.lil_matrix((1,4), dtype=np.float)
            C_CorefDict_fct(mention_pair_sample, matrix=result, row_id=0, column_offset=0)
            return result.tocsr()
        left_text = mention_pair_sample.left.raw_text.lower()
        right_text = mention_pair_sample.right.raw_text.lower()
        left_head_text = mention_pair_sample.left.head_raw_text.lower()
        right_head_text = mention_pair_sample.right.head_raw_text.lower()
        matrix[row_id,column_offset+0] = int(coref_dictionary_reader.freq_in_dict1(left_head_text, right_head_text) >= 8)
        matrix[row_id,column_offset+1] = int(coref_dictionary_reader.freq_in_dict2(left_text, right_text) >= 2)
        matrix[row_id,column_offset+2] = int(coref_dictionary_reader.freq_in_dict3(left_text, right_text) >= 2)
        matrix[row_id,column_offset+3] = int(coref_dictionary_reader.freq_in_dict4(left_text, right_text) >= 2)
        return matrix
    compute_fct = C_CorefDict_fct
    column_names = ("CorefDict1", "CorefDict2", "CorefDict3", "CorefDict4")
    feature = MultiNumericFeature(name, compute_fct, column_names)
    _post_feature_creation(feature, feature_name2feature, C_CorefDict_feature_names)
    '''
    group_name2feature_names["C_CorefDict"] = C_CorefDict_feature_names



#### Singleton features ####
### Singleton internal morphosyntactic features ###
def _create_singleton_internal_morphosyntactic_features(feature_name2feature, group_name2feature_names, 
                                                       quantize=False, strict=False):
    """ Creates, in a parametrized fashion, feature(s) representing some kinds of 'internal morphosyntactic' 
    information about the mentions of a pair, and add them to the input feature definition and group 
    definition structures.
    
    Features with the following respective base names will be 
    created:
        - "IsPronoun"
        - "IsName"
        - "IsAnimated"
        - "Person"
        - "Number"
        - "IsIndefinite"
        - "IsQuantified"
    The name of the group that the created features will constitute is the 
    following:
        - "SC_InternMS"
    
    Args:
        feature_name2feature: a 'feature name => feature' map, to which created feature shall be 
            added for future easy access
        group_name2feature_names: a 'group name => feature names' map, used to define the which 
            of the features created by this function that shall be considered as a group
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
    """
    SC_InternMS_feature_names = []
    
    _create_mention_data_based_features(singleton_internal_morphosyntactic_features_data, 
                                         feature_name2feature, SC_InternMS_feature_names, "SC", 
                                         quantize, strict)
    
    group_name2feature_names["SC_InternMS"] = SC_InternMS_feature_names



### Singleton grammatical role features ###
def _create_singleton_grammatical_role_features(feature_name2feature, group_name2feature_names, 
                                               quantize=False, strict=False):
    """ Creates, in a parametrized fashion, feature(s) representing some kinds of 'grammatical role' 
    information about the mentions of a pair, and add them to the input feature definition and group 
    definition structures.
    
    Features with the following respective base names will be 
    created:
        - "Position"
        - "InCoord"
        - "IsCoord"
    The name of the group that the created features will constitute is the 
    following:
        - "SC_GramRole"
    
    Args:
        feature_name2feature: a 'feature name => feature' map, to which created feature shall be 
            added for future easy access
        group_name2feature_names: a 'group name => feature names' map, used to define the which 
            of the features created by this function that shall be considered as a group
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
    """
    SC_GramRole_feature_names = []
    
    not_to_use_data_names = ("IsEnum", # Because it is implemented in 'create_syntax_features'
                             )
    _create_mention_data_based_features(singleton_grammatical_role_features_data, 
                                         feature_name2feature, SC_GramRole_feature_names, "SC", 
                                         quantize, strict, not_to_use_data_names=not_to_use_data_names)
    
    group_name2feature_names["SC_GramRole"] = SC_GramRole_feature_names



### Singleton semantic environment features ###
# TODO: not used for now because it needs dependency node trees
def _create_coref_singleton_semantic_environment_features(feature_name2feature, group_name2feature_names, 
                                                         quantize=False, strict=False):
    SC_SemEnv_feature_names = []
    
    _create_mention_data_based_features(singleton_semantic_environment_features_data, 
                                         feature_name2feature, SC_SemEnv_feature_names, "SC", 
                                         quantize, strict)
    
    group_name2feature_names["SC_SemEnv"] = SC_SemEnv_feature_names



### Singleton word count ###
def _create_singleton_word_count_features(feature_name2feature, group_name2feature_names, 
                                         quantize=False, strict=False):
    """ Creates, in a parametrized fashion, feature(s) representing some kinds of 'words number'  
    information about the mentions of a pair, and add them to the input feature definition and group 
    definition structures.
    
    Features with the following respective base names will be 
    created:
        - "NumberWords"
    The name of the group that the created features will constitute is the 
    following:
        - "SC_WordCount"
    
    Args:
        feature_name2feature: a 'feature name => feature' map, to which created feature shall be 
            added for future easy access
        group_name2feature_names: a 'group name => feature names' map, used to define the which 
            of the features created by this function that shall be considered as a group
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
    """
    SC_WordCount_feature_names = []
    
    _create_mention_data_based_features(singleton_word_count_features_data, 
                                         feature_name2feature, SC_WordCount_feature_names, "SC", 
                                         quantize, strict)
    
    group_name2feature_names["SC_WordCount"] = SC_WordCount_feature_names



### Singleton linguistic form features ###
def _create_linguistic_form_features(feature_name2feature, group_name2feature_names, quantize=False, 
                                    strict=False):
    """ Creates, in a parametrized fashion, feature(s) representing some kinds of 'linguistic form'  
    information about the mentions of a pair, and add them to the input feature definition and group 
    definition structures.
    
    Features with the following respective base names will be 
    created:
        - "LinguisticForm"
    The name of the group that the created features will constitute is the 
    following:
        - "SC_LinguisticForm"
    
    Args:
        feature_name2feature: a 'feature name => feature' map, to which created feature shall be 
            added for future easy access
        group_name2feature_names: a 'group name => feature names' map, used to define the which 
            of the features created by this function that shall be considered as a group
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
    """
    SC_LinguisticForm_feature_names = []
    
    _create_mention_data_based_features(singleton_linguistic_form_features_data, 
                                         feature_name2feature, SC_LinguisticForm_feature_names, "SC", 
                                         quantize, strict)
    
    group_name2feature_names["SC_LinguisticForm"] = SC_LinguisticForm_feature_names



'''
_feature_names = []
##
name = 
compute_fct = 
possible_values = 
feature = CategoricalFeature(name, compute_fct, possible_values, strict=strict)
_post_feature_creation(feature, feature_name2feature, _feature_names)
group_name2feature_names[""] = _feature_names
'''

def prepare_features_creation(with_anaphoricity_features=False, with_singleton_features=False):
    """ Defines the feature creation functions that shall be used when creating a vectorizer for 
    MentionPairSample instances for the currently parametrized French language, as well as the pairs 
    of group that shall be used to create feature products.
    
    Returns:
        a (create_functions_collection, features_groups_features_names) pair, 
        where:
            - 'create_functions_collection' is the collection of features creation functions to use to create 
              the intended collection of _Feature child classes instances
            - 'features_groups_features_names' is the collection of (group1 name; group2 name) pairs, whose 
              role is to specify the pairs of groups of features for which a product feature shall be created 
              for each pair of features that can be defined from the cardinal product of the two groups
    """
    # Prepare features creation
    create_functions_collection = []
    create_functions_collection.append(_create_mention_gram_type_features)
    create_functions_collection.append(_create_mention_pair_type_features)
    create_functions_collection.append(_create_string_relation_features)
    create_functions_collection.append(_create_semantic_features)
    create_functions_collection.append(_create_relative_location_features)
    create_functions_collection.append(_create_named_entities_type_features)
    #create_functions_collection.append(_create_named_entities_subtype_features) # FIXME: legacy commented out, do we keep?
    create_functions_collection.append(_create_named_entities_pair_types_features)
    create_functions_collection.append(_create_syntax_features)
    if with_anaphoricity_features:
        create_functions_collection.append(_create_anaphoricity_features)
    if with_singleton_features:
        create_functions_collection.append(_create_singleton_features)
    create_functions_collection.append(_create_speaker_features)
    #create_functions_collection.append(_create_verb2noun_features) # TODO: when verbal mention will be supported
    create_functions_collection.append(_create_pronoun_compatibility_features)
    #create_functions_collection.append(_create_dictionary_features) # FIXME: legacy commented out, do we keep?
    
    # Singleton features
    create_functions_collection.append(_create_singleton_internal_morphosyntactic_features)
    #create_functions_collection.append(_create_singleton_grammatical_role_features) # INEFFICIENT FOR COREF
    #create_functions_collection.append(_create_coref_singleton_semantic_environment_features) # TODO: not used for now because it needs dependency node trees
    #create_functions_collection.append(_create_singleton_word_count_features) # INEFFICIENT FOR COREF
    #create_functions_collection.append(_create_linguistic_form_features) # INEFFICIENT FOR COREF
    
    # Define group products to create
    features_groups_features_names = []
    features_groups_features_names.append(("C_NamedEntityType", "C_StringRelation"))
    features_groups_features_names.append(("C_NamedEntityType", "C_RelativeLocation"))
    features_groups_features_names.append(("C_Semantic", "C_StringRelation"))
    features_groups_features_names.append(("C_Distance", "C_StringRelation"))
    features_groups_features_names.append(("C_Distance", "C_Semantic"))
    features_groups_features_names.append(("C_Distance", "C_NamedEntityPairType"))
    
    return create_functions_collection, features_groups_features_names

