# -*- coding: utf-8 -*-

"""
Defines utilities used to create a vectorization of samples based on structures instantiated with 
French document data (e.g.: pairs of mentions that come from a text written in French)
"""

"""
Available submodule(s)
-----------------------
mention
    Defines functions to compute values or data from a Mention instance

mention_singleton_features
    Defines functions to compute values or data from a SingletonSample instance, as well as utilities 
    used to create a collection of 'cortex.tools.ml_features.Features' instances from those functions

mention_anaphoricity_features
    Defines functions to compute values or data from a AnaphoricitySample instance, as well as utilities 
    used to create a collection of 'cortex.tools.ml_features.Features' instances from those functions

mention_pair_sample_features
    Defines functions to compute values or data from a MentionPairSample instance, as well as utilities 
    used to create a collection of 'cortex.tools.ml_features.Features' instances from those functions
"""