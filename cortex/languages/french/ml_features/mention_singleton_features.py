# -*- coding: utf-8 -*-

"""
Defines functions to compute values or data from a SingletonSample instance, as well as utilities 
used to create a collection of 'cortex.tools.ml_features._Features' child class instances from those functions
"""

"""
Here, a 'feature' is an instance of a '_Feature' child class of the 'ml_features' module.

The aim of this module is to prepare the parametrized creation of features, and to define the collection 
of features, and of the groups of feature names that shall be used to create a ml sample vectorizer.
A group of feature names is a collection of features names. It can be used to specify that operation 
made between features from two distinct groups should be made for all pairs defined in the cardinal 
product of two group.

In this specific case, the ml samples will be assumed to be instances of the SingletonSample class. 

This does this by creating functions that can take as 
inputs:
    - those parameters (notably, 'quantize' and 'strict')
    - structures that store the created features,
    - a structure to record the group each features belongs to
Those functions' respective name follows the following pattern: 'create_..._feature'.

Most of the features that can be created use functions that can be used by other feature creation 
processes, so those functions are shared by being put in a module of their own, here the module 'mention.py'.

Those functions are embedded in a tuple of data (referred later in the code as 'mention_feature_data', 
since those function are defined on a 'Mention' instance input). The first element of the tuple is 
a string defining the type of feature that this function is destined to be used by (such as 'numerical', 
'categorical' or 'multi_numeric'), the second element is the function itself, and the rest of the tuple
elements are elements that are specific to each feature instance type (such as 'column_names' for 
'multi_numeric' features, or 'possible_values' for 'categorical' features).

In the end, the only function that the user will use from this module is the 'prepare_features_creation' 
function, that return the collection of features creation functions to use, as well as the collection 
of group names pairs (whose role is to specify the groups of features for which a product feature shall 
be created for each pair of features that can be defined from the cardinal product of the two groups).
"""

__all__ = ["prepare_features_creation",]

from .mention import (create_mention_feature_from_data,
                      number_words_data, linguistic_form_data, 
                      sentence_index_data, normalized_sentence_index_data, 
                      at_sentence_rank_data, normalized_at_sentence_rank_data, 
                      at_sentence_inverse_rank_data, normalized_at_sentence_inverse_rank_data, 
                      sentence_position_data, np_above_nb_data, 
                      normalized_np_above_nb_data, pp_above_nb_data, 
                      normalized_pp_above_nb_data, vp_above_nb_data, 
                      normalized_vp_above_nb_data, is_pronoun_data, 
                      is_name_data, is_animated_data, person_data, 
                      number_data, is_indefinite_data, is_quantified_data, 
                      position_data, in_coordination_data, 
                      is_coordination_data, is_enumeration_data, 
                      is_embedd_data, string_match_with_other_data, 
                      in_apposition_data, alias_with_other_data,
                      gram_type_data, gram_subtype_data,
                      named_entity_type_cluster_data, 
                      has_named_entity_subtype_data, 
                      )

QUANTIFICATION_THRESHOLD_VALUES = (0,1,2,3,6,11)#(3,6,11)


#############
### Utils ###
#############
_get_mention_from_singleton_sample_fct = lambda singleton_sample: singleton_sample.mention
def _create_singleton_sample_feature_from_data(feature_data, name, quantize=False, strict=False):
    """ Is a wrapper around the 'create_mention_feature_from_data', when feature is destined to be 
    fed SingletonSample instance as input. That is, creates a feature that is destined to take a 
    SingletonSample instance as an input.
    
    Args:
        feature_data: tuple of elements needed to create a _Feature child class instance. Cf the 
            module's documentation.
        name: name to give the created _Feature child class instance
        quantize: whether or not to quantize a Numeric or MultiNumeric feature during its creation. 
            If True, the numeric values contained in the 'QUANTIFICATION_THRESHOLD_VALUES' module variable 
            will be used as threshold to define the bins; and the output feature will be a CategoricalFeature 
            instance.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of 

    Returns:
        a _Feature child class instance, that takes a SingletonSample instance as input
    """
    quantification_threshold_values = QUANTIFICATION_THRESHOLD_VALUES if quantize else None
    return create_mention_feature_from_data(_get_mention_from_singleton_sample_fct, feature_data, 
                                            name, strict=strict, 
                                            quantification_threshold_values=quantification_threshold_values)

def _post_feature_creation(feature, feature_maps, *args):
    """ Automatize the process of adding a newly created _Feature instance to the structure tasked with 
    identifying and storing them, as well as registering the feature as being part of any number of 
    group(s). 
    
    Args:
        feature: a _Feature child class instance
        feature_maps: the 'feature name => feature' map to which the input feature must be added
        *args: a collection of lists, each list representing a group to which the feature must 
            be added (represented by its name)
    """
    name = feature.name
    feature_maps[name] = feature
    for group_feature_names in args:
        group_feature_names.append(name)

def _create_mention_data_based_features(mention_features_data, feature_name2feature, feature_names, 
                                       quantize, strict, not_to_use_data_names=None):
    """ Function whose role is to automatize the process of carrying out the parametrized creation 
    of a feature, and to carry it out for several 'features that take a SingletonSample' at once.
    Once a feature has been created, it will added to the input 'feature_name2feature' map.
    
    Args:
        mention_features_data: a collection of ('feature_base_name, 'mention_feature_data') pairs; 
            see module's documentation. 'feature_base_name' is a string that will be used to define the name 
            attributed to the feature built from using the corresponding 'mention_feature_data'.
        feature_name2feature: the 'feature name => feature' map which the created feature must be added to
        feature_names: list of feature names to which the name of the created feature must be added
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
        not_to_use_data_names: collection of 'feature_base_name' strings whose corresponding 
            feature creation data the user does not want to use.
    """
    if not_to_use_data_names is None:
        not_to_use_data_names = tuple()
    not_to_use_data_names = set(not_to_use_data_names)
    mention_features_data = tuple(t for t in mention_features_data if t[0] not in not_to_use_data_names)
    for base_name, data in mention_features_data:
        name = "S_{}".format(base_name)
        feature = _create_singleton_sample_feature_from_data(data, name, quantize=quantize, strict=strict)
        _post_feature_creation(feature, feature_name2feature, feature_names)




#############################################
# Function whose role is to create features #
#############################################
# FIXME: do we do the same for anaphoricity features?
internal_morphosyntactic_features_data = (("IsPronoun", is_pronoun_data), ## Grammatical type
                                          ("IsName", is_name_data), ## Grammatical type
                                          ("IsAnimated", is_animated_data), ## Animated
                                          ("Person", person_data), ## Person
                                          ("Number", number_data), ## Number
                                          ("IsIndefinite", is_indefinite_data), ## Quantifier
                                          ("IsQuantified", is_quantified_data), ## Quantifier
                                          )
def _create_internal_morphosyntactic_features(feature_name2feature, group_name2feature_names, 
                                             quantize=False, strict=False):
    """ Creates, in a parametrized fashion, feature(s) representing some kinds of 'internal morphosyntactic' 
    information about a mention, and add them to the input feature definition and group definition 
    structures.
    
    Features with the following respective base names will be 
    created:
        - "IsPronoun"
        - "IsName"
        - "IsAnimated"
        - "Person"
        - "Number"
        - "IsIndefinite"
        - "IsQuantified"
    The name of the group that the created features will constitute is the 
    following:
        - "S_InternMS"
    
    Args:
        feature_name2feature: a 'feature name => feature' map, to which created feature shall be 
            added for future easy access
        group_name2feature_names: a 'group name => feature names' map, used to define the which 
            of the features created by this function that shall be considered as a group
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
    """
    S_InternMS_feature_names = []
    ##
    _create_mention_data_based_features(internal_morphosyntactic_features_data, feature_name2feature, 
                                       S_InternMS_feature_names, quantize, strict)
    
    group_name2feature_names["S_InternMS"] = S_InternMS_feature_names



grammatical_role_features_data = (("Position",position_data),
                                  ("InCoord",in_coordination_data),
                                  ("IsCoord",is_coordination_data),
                                  ("IsEnum",is_enumeration_data),
                                  )
def _create_grammatical_role_features(feature_name2feature, group_name2feature_names, quantize=False, 
                                     strict=False):
    """ Creates, in a parametrized fashion, feature(s) representing some kinds of 'grammatical role' 
    information about a mention, and add them to the input feature definition and group definition 
    structures.
    
    Features with the following respective base names will be 
    created:
        - "Position"
        - "InCoord"
        - "IsCoord"
        - "IsEnum"
    The name of the group that the created features will constitute is the 
    following:
        - "S_GramRole"
    
    Args:
        feature_name2feature: a 'feature name => feature' map, to which created feature shall be 
            added for future easy access
        group_name2feature_names: a 'group name => feature names' map, used to define the which 
            of the features created by this function that shall be considered as a group
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
    """
    S_GramRole_feature_names = []
    ##
    _create_mention_data_based_features(grammatical_role_features_data, feature_name2feature, 
                                       S_GramRole_feature_names, quantize, strict)
    
    group_name2feature_names["S_GramRole"] = S_GramRole_feature_names



# TODO: not used for now because it needs dependency node trees
semantic_environment_features_data = tuple()
def _create_semantic_environment_features(feature_name2feature, group_name2feature_names, quantize=False, strict=False):
    S_SemEnv_feature_names = []
    ##
    _create_mention_data_based_features(semantic_environment_features_data, feature_name2feature, 
                                       S_SemEnv_feature_names, quantize, strict)
    
    group_name2feature_names["S_SemEnv"] = S_SemEnv_feature_names



word_count_features_data = (("NumberWords",number_words_data),
                            )
def _create_word_count_features(feature_name2feature, group_name2feature_names, quantize=False, 
                               strict=False):
    """ Creates, in a parametrized fashion, feature(s) representing some kinds of 'words number'  
    information about a mention, and add them to the input feature definition and group definition 
    structures.
    
    Features with the following respective base names will be 
    created:
        - "NumberWords"
    The name of the group that the created features will constitute is the 
    following:
        - "S_WordCount"
    
    Args:
        feature_name2feature: a 'feature name => feature' map, to which created feature shall be 
            added for future easy access
        group_name2feature_names: a 'group name => feature names' map, used to define the which 
            of the features created by this function that shall be considered as a group
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
    """
    S_WordCount_feature_names = []
    ##
    _create_mention_data_based_features(word_count_features_data, feature_name2feature, 
                                       S_WordCount_feature_names, quantize, strict)
    
    group_name2feature_names["S_WordCount"] = S_WordCount_feature_names



linguistic_form_features_data = (("LinguisticForm",linguistic_form_data),
                                 )
def _create_linguistic_form_features(feature_name2feature, group_name2feature_names, quantize=False, 
                                    strict=False):
    """ Creates, in a parametrized fashion, feature(s) representing some kinds of 'linguistic form'  
    information about a mention, and add them to the input feature definition and group definition 
    structures.
    
    Features with the following respective base names will be 
    created:
        - "LinguisticForm"
    The name of the group that the created features will constitute is the 
    following:
        - "S_LinguisticForm"
    
    Args:
        feature_name2feature: a 'feature name => feature' map, to which created feature shall be 
            added for future easy access
        group_name2feature_names: a 'group name => feature names' map, used to define the which 
            of the features created by this function that shall be considered as a group
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
    """
    S_LinguisticForm_feature_names = []
    ##
    _create_mention_data_based_features(linguistic_form_features_data, feature_name2feature, 
                                       S_LinguisticForm_feature_names, quantize, strict)
    
    group_name2feature_names["S_LinguisticForm"] = S_LinguisticForm_feature_names


position_in_text_features_data = (("SentenceNumber",sentence_index_data), 
                                  ("AtSentenceRank",at_sentence_rank_data), 
                                  ("AtSentenceInverseRank",at_sentence_inverse_rank_data), 
                                  )
normalized_position_in_text_features_data = (("NormalizedSentenceNumber",normalized_sentence_index_data), 
                                             ("NormalizedAtSentenceRank",normalized_at_sentence_rank_data), 
                                             ("NormalizedAtSentenceInverseRank",normalized_at_sentence_inverse_rank_data), 
                                             )
def _create_position_in_text_features(feature_name2feature, group_name2feature_names, quantize=False, 
                                     strict=False):
    """ Create, in a parametrized fashion, feature(s) representing some kinds of 'text position'   
    information about a mention, and add them to the input feature definition and group definition 
    structures.
    
    Features with the following respective base names will be 
    created:
        - "NormalizedSentenceNumber"
        - "NormalizedAtSentenceRank"
        - "NormalizedAtSentenceInverseRank"
        - "SentencePosition"
    The name of the group that the created features will constitute is the 
    following:
        - "S_PositionInText"
    
    Args:
        feature_name2feature: a 'feature name => feature' map, to which created feature shall be 
            added for future easy access
        group_name2feature_names: a 'group name => feature names' map, used to define the which 
            of the features created by this function that shall be considered as a group
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
    """
    S_PositionInText_feature_names = []
    
    # Sentence number and mention rank in sentence
    #features_data = position_in_text_features_data
    features_data = normalized_position_in_text_features_data
    _create_mention_data_based_features(features_data, feature_name2feature, 
                                       S_PositionInText_feature_names, quantize, strict)
    
    # Appears in which sentence
    name = "S_SentencePosition"
    data = sentence_position_data
    feature = _create_singleton_sample_feature_from_data(data, name, quantize=quantize, strict=strict)
    _post_feature_creation(feature, feature_name2feature, S_PositionInText_feature_names)
    
    group_name2feature_names["S_PositionInText"] = S_PositionInText_feature_names



def _create_relation_to_other_mentions_features(feature_name2feature, group_name2feature_names, 
                                                  quantize=False, strict=False):
    """ Creates, in a parametrized fashion, feature(s) representing some kinds of 'relation to other mentions'  
    information about a mention, and add them to the input feature definition and group definition 
    structures.
    
    Features with the following respective base names will be 
    created:
        - "IsEmbedd"
        - "StringMatchWithOther"
        - "InApposition"
        - "AliasWithOther"
    The name of the group that the created features will constitute is the 
    following:
        - "S_RelationWithOther"
    
    Args:
        feature_name2feature: a 'feature name => feature' map, to which created feature shall be 
            added for future easy access
        group_name2feature_names: a 'group name => feature names' map, used to define the which 
            of the features created by this function that shall be considered as a group
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
    """
    S_RelationWithOther_feature_names = []
    
    # Is embedding another mention / embedded by another mention
    name = "S_IsEmbedd"
    data = is_embedd_data
    feature = _create_singleton_sample_feature_from_data(data, name, quantize=quantize, strict=strict)
    _post_feature_creation(feature, feature_name2feature, S_RelationWithOther_feature_names)
    
    # Matching with other mentions
    name = "S_StringMatchWithOther"
    data = string_match_with_other_data
    feature = _create_singleton_sample_feature_from_data(data, name, quantize=quantize, strict=strict)
    _post_feature_creation(feature, feature_name2feature, S_RelationWithOther_feature_names)
    
    # Apposition with another mention
    name = "S_InApposition"
    data = in_apposition_data
    feature = _create_singleton_sample_feature_from_data(data, name, quantize=quantize, strict=strict)
    _post_feature_creation(feature, feature_name2feature, S_RelationWithOther_feature_names)
    
    # Share acronym with a previous mention
    name = "S_AliasWithOther"
    data = alias_with_other_data
    feature = _create_singleton_sample_feature_from_data(data, name, quantize=quantize, strict=strict)
    _post_feature_creation(feature, feature_name2feature, S_RelationWithOther_feature_names)
    
    group_name2feature_names["S_RelationWithOther"] = S_RelationWithOther_feature_names



syntactic_features_data = (("NPAboveNb", np_above_nb_data),
                           ("PPAboveNb", pp_above_nb_data),
                           ("VPAboveNb", vp_above_nb_data),
                           )
normalized_syntactic_features_data = (("NormalizedNPAboveNb", normalized_np_above_nb_data),
                                      ("NormalizedPPAboveNb", normalized_pp_above_nb_data),
                                      ("NormalizedVPAboveNb", normalized_vp_above_nb_data),
                                      )
def _create_syntactic_features(feature_name2feature, group_name2feature_names, quantize=False, strict=False):
    """ Creates, in a parametrized fashion, feature(s) representing some kinds of 'syntactic'    
    information about a mention, and add them to the input feature definition and group definition 
    structures.
    
    Features with the following respective base names will be 
    created:
        - "NormalizedNPAboveNb"
        - "NormalizedPPAboveNb"
        - "NormalizedVPAboveNb"
    The name of the group that the created features will constitute is the 
    following:
        - "S_Syntax"
    
    Args:
        feature_name2feature: a 'feature name => feature' map, to which created feature shall be 
            added for future easy access
        group_name2feature_names: a 'group name => feature names' map, used to define the which 
            of the features created by this function that shall be considered as a group
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
    """
    S_Syntax_feature_names = []
    #features_data = syntactic_features_data
    features_data = normalized_syntactic_features_data
    _create_mention_data_based_features(features_data, feature_name2feature, 
                                       S_Syntax_feature_names, quantize, strict)
    
    group_name2feature_names["S_Syntax"] = S_Syntax_feature_names



gram_type_features_data = (("GramType",gram_type_data),
                           )
gram_subtype_features_data = (("GramSubType",gram_subtype_data),
                              )
def _create_gram_type_features(feature_name2feature, group_name2feature_names, quantize=False, strict=False):
    """ Create, in a parametrized fashion, feature(s) representing some kinds of 'grammatical type'    
    information about a mention, and add them to the input feature definition and group definition 
    structures.
    
    Features with the following respective base names will be 
    created:
        - "GramType"
        - "GramSubType"
    The name of the group that the created features will constitute is the 
    following:
        - "S_GramType"
    
    Args:
        feature_name2feature: a 'feature name => feature' map, to which created feature shall be 
            added for future easy access
        group_name2feature_names: a 'group name => feature names' map, used to define the which 
            of the features created by this function that shall be considered as a group
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
    """
    S_GramType_feature_names = []
    ##
    _create_mention_data_based_features(gram_type_features_data, feature_name2feature, 
                                       S_GramType_feature_names, quantize, strict)
    _create_mention_data_based_features(gram_subtype_features_data, feature_name2feature, 
                                       S_GramType_feature_names, quantize, strict)
    
    group_name2feature_names["S_GramType"] = S_GramType_feature_names



def _create_named_entity_type_features(feature_name2feature, group_name2feature_names, quantize=False, 
                                      strict=False):
    """ Create, in a parametrized fashion, feature(s) representing some kinds of 'named entity type'    
    information about a mention, and add them to the input feature definition and group definition 
    structures.
    
    Features with the following respective base names will be 
    created:
        - "S_NamedEntityTypeCluster"
        - "S_HasNamedEntitySubType"
    The name of the group that the created features will constitute is the 
    following:
        - "S_NamedEntityType"
    
    Args:
        feature_name2feature: a 'feature name => feature' map, to which created feature shall be 
            added for future easy access
        group_name2feature_names: a 'group name => feature names' map, used to define the which 
            of the features created by this function that shall be considered as a group
        quantize: whether or not to quantize a Numerical or MultiNumeric feature during its creation.
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
    """
    S_NamedEntityType_feature_names = []
    ##
    name = "S_NamedEntityTypeCluster"
    data = named_entity_type_cluster_data
    feature = _create_singleton_sample_feature_from_data(data, name, quantize=quantize, strict=strict)
    _post_feature_creation(feature, feature_name2feature, S_NamedEntityType_feature_names)
    ##
    name = "S_HasNamedEntitySubType"
    data = has_named_entity_subtype_data
    feature = _create_singleton_sample_feature_from_data(data, name, quantize=quantize, strict=strict)
    _post_feature_creation(feature, feature_name2feature, S_NamedEntityType_feature_names)
    
    group_name2feature_names["S_NamedEntityType"] = S_NamedEntityType_feature_names


'''
_feature_names = []
##
name = 
compute_fct = 
possible_values = 
feature = CategoricalFeature(name, compute_fct, possible_values, strict=strict)
_post_feature_creation(feature, feature_name2feature, _feature_names)
group_name2feature_names[""] = _feature_names
'''

def prepare_features_creation():
    """ Defines the feature creation functions that shall be used when creating a vectorizer for 
    SingletonSample instances for the currently parametrized French language, as well as the pairs 
    of group that shall be used to create feature products.
    
    Returns:
        a (create_functions_collection, features_groups_features_names) pair, 
        where:
            - 'create_functions_collection' is the collection of features creation functions to use to create 
              the intended collection of _Feature child classes instances
            - 'features_groups_features_names' is the collection of (group1 name; group2 name) pairs, whose 
              role is to specify the pairs of groups of features for which a product feature shall be created 
              for each pair of features that can be defined from the cardinal product of the two groups
    """
    # Prepare features creation
    create_functions_collection = []
    create_functions_collection.append(_create_internal_morphosyntactic_features)
    create_functions_collection.append(_create_grammatical_role_features)
    #create_functions_collection.append(_create_semantic_environment_features) # TODO: not used for now because it needs dependency node trees
    create_functions_collection.append(_create_word_count_features)
    create_functions_collection.append(_create_linguistic_form_features)
    create_functions_collection.append(_create_position_in_text_features)
    create_functions_collection.append(_create_relation_to_other_mentions_features)
    create_functions_collection.append(_create_syntactic_features)
    create_functions_collection.append(_create_gram_type_features)
    create_functions_collection.append(_create_named_entity_type_features)
    
    # Define group products to create # FIXME: why are they all commented out?
    features_groups_features_names = []
    #features_groups_features_names.append(("S_PositionInText", "S_LinguisticForm"))
    #features_groups_features_names.append(("S_PositionInText", "S_RelationWithOther"))
    #features_groups_features_names.append(("S_PositionInText", "S_SemEnv"))
    #features_groups_features_names.append(("S_PositionInText", "S_Syntax"))
    
    return create_functions_collection, features_groups_features_names

