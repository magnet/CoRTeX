# -*- coding: utf-8 -*-

"""
Defines utilities / data pertaining to the French language.

Currently, the tools defined in this package work by assuming that the documents to be processed are 
annotated with the same annotations used for the French documents of the 
"modified Crabbe & Candito French Treebank" corpus, and that the named entity tags are basic ones.

The parse tags and POS tags are technically defined in the 
'parameters.modified_CC_treebank_constituent_tags.py' module, and the named entity tags are 
technically defined in the 'parameters.base_named_entities_data_tags.py' module.

It is possible to annotate new documents with a compatible annotation by using the Stanford Core NLP 
suite, with suitable models. The 'document_characterizer' module provides a class that is using 
Stanford Core NLP under the hood to do this.

The 'LANGUAGE_PARAMETER_VERSION' variable is here to reference this set of tags that is currently 
used by the tools associated to the language, as well as specify the current version number associated 
to this parametrization: its version suffix should change each time the following data or processes 
are modified:

- the parameters used by the toolbox (package 'parameters')
- the inner functioning of 
  the:
    - constituency_tree_head_finder
    - constituency_tree_features
    - document_characterizer
    - mention_characterizer
    - quotation_detector
    - token_features
    - sentence_features
    - ml_features
- the knowledge sources used by the 'mention_characterizer' (in the 'knowledge' package)

Basically, anything than can impact the attribute value found during the characterization of a 
mention (hence 'mention_characterizer)), AND / OR anything that can modify the vectorization value 
of a ml_sample based on the value of mentions, supposing those values are constant.
The value of this variable is notably used during the caching of vectorization data, which is why it 
is important that its version number reflects any changes that can result in a change in the 
vectorization of a sample built from elements of documents that are annotated in the specified manner.
"""

"""
Available subpackage(s)
-----------------------
knowledge
    Defines data specific to the French language, and rather constant (e.g.: collection of names, etc.)

parameters
    Defines data that specify ways to qualify text written in French (e.g: different sets of syntactic tags...)

ml_features
    Defines utilities used to create a vectorization of samples based on structures instantiated with 
    French document data (e.g.: pairs of mentions that come from a text written in French) 

rules
    Defines utilities implementing knows set(s) of rules to resolve a coreference partition for an 
    French document
    
"""

"""
Available submodule(s)
-----------------------
constituency_tree_features
    Defines classes and utilities used to compute data and values from instances of the 
    ConstituencyTreeNode and of ConstituencyTree classes, instantiated from French document data

constituency_tree_node_head_finder
    Defines utilities used to find the head child node of a ConstituencyTreeNode instance that was 
    instantiated from French document data

document_characterizer
    Defines utilities used to characterize documents with compatible 'language_parameter_version' data

mention_characterizer
    Defines a class used to qualify the mention of documents, i.e. notably to determine grammatical 
    information about them 

mention_detector
    Defines a class used to detect mentions in an French document

mention_features
    Defines a class used to compute data and values from instances of the Mention class that were 
    instantiated with French document data with compatible 'language_parameter_version'

token_features
    Defines a class used to compute data and values from instances of the Token class that were 
    instantiated with French document data with compatible 'language_parameter_version'
"""

__all__ = ["LANGUAGE_PARAMETER_VERSION",
           "get_french_object",
           ]

LANGUAGE_PARAMETER_VERSION = "modified_CC_constituent_base_ne_types-1.0.6"

from cortex.utils.memoize import Memoized

FRENCH_SUPPORTED_OBJECT_TYPES = {"mention_features", 
                                  "token_features",  
                                  "mention_singleton_features_prepare_creation_fct",
                                  "mention_anaphoricity_features_prepare_creation_fct",
                                  "mention_pair_sample_features_prepare_creation_fct", 
                                  "rules2coref_resolve_fct", 
                                  "rules2edges_constraints_fct",
                                  "document_characterizer",
                                  "mention_detector",
                                  "mention_characterizer",
                                  "LANGUAGE_PARAMETER_VERSION",
                                 }
def get_french_object(object_type):
    """ Returns the CoRteX' utility python object, made for the English language, corresponding to 
    the input value.
    
    Args:
        object_type: a string, possible values are the 
            following:
                * "mention_features"
                * "token_features"
                * "rules2coref_resolve_fct"
                * "rules2edges_constraints_fct"
                * "mention_singleton_features_prepare_creation_fct"
                * "mention_anaphoricity_features_prepare_creation_fct"
                * "mention_pair_sample_features_prepare_creation_fct"
                * "document_characterizer"
                * "mention_detector"
                * "mention_characterizer"
                * "LANGUAGE_PARAMETER_VERSION"
    
    Raises:
        ValueError: if the input value does not correspond to 
    """
    SUPPORTED_OBJECT_TYPES = FRENCH_SUPPORTED_OBJECT_TYPES
    # Check input
    if object_type not in SUPPORTED_OBJECT_TYPES:
        message = "The input 'object_type' value '{}' is not supported for French; supported values are: {}."
        message = message.format(object_type, tuple(sorted(SUPPORTED_OBJECT_TYPES)))
        raise ValueError(message)
    
    # Fetch the desired object
    if object_type == "mention_features":
        from .mention_features import FRENCH_MENTION_FEATURES
        return FRENCH_MENTION_FEATURES
    
    elif object_type == "token_features":
        from .token_features import FRENCH_TOKEN_FEATURES
        return FRENCH_TOKEN_FEATURES
    
    elif object_type == "rules2coref_resolve_fct":
        from .rules import RULES_TYPE2COREF_RESOLVE_FCT as FRENCH_RULES_TYPE2COREF_RESOLVE_FCT
        return FRENCH_RULES_TYPE2COREF_RESOLVE_FCT
    
    elif object_type == "rules2edges_constraints_fct":
        from .rules import RULES_TYPE2EDGES_CONSTRAINTS_FCT as FRENCH_RULES_TYPE2EDGES_CONSTRAINTS_FCT
        return FRENCH_RULES_TYPE2EDGES_CONSTRAINTS_FCT
    
    elif object_type == "mention_singleton_features_prepare_creation_fct":
        from .ml_features.mention_singleton_features import prepare_features_creation as french_singleton_sample_prepare_features_creation
        return french_singleton_sample_prepare_features_creation
    
    elif object_type == "mention_anaphoricity_features_prepare_creation_fct":
        from .ml_features.mention_anaphoricity_features import prepare_features_creation as french_anaphoricity_sample_prepare_features_creation
        return french_anaphoricity_sample_prepare_features_creation
    
    elif object_type == "mention_pair_sample_features_prepare_creation_fct":
        from .ml_features.mention_pair_sample_features import prepare_features_creation as french_mention_pair_sample_prepare_features_creation
        return french_mention_pair_sample_prepare_features_creation
    
    elif object_type == "document_characterizer":
        from .document_characterizer import FrenchDocumentCharacterizer
        return FrenchDocumentCharacterizer
    
    elif object_type == "mention_detector":
        from .mention_detector import FrenchMentionDetector
        return FrenchMentionDetector
    
    elif object_type == "mention_characterizer":
        from .mention_characterizer import FrenchMentionCharacterizer
        return FrenchMentionCharacterizer
    
    elif object_type == "LANGUAGE_PARAMETER_VERSION":
        return LANGUAGE_PARAMETER_VERSION
