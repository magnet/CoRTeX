# -*- coding: utf-8 -*-

"""
Defines a base class used to characterize documents (annotate its sentences and tokens, with sufficient 
data, so as to be able to be processed by this library).
"""

__all__ = ["_DocumentCharacterizer",
           "RawTextAlignmentError",
           ]

import abc
import re
from collections import OrderedDict

from cortex.api.markable import Mention
from cortex.api.coreference_partition import CoreferencePartition
from cortex.utils import CoRTeXException


class _DocumentCharacterizer(abc.ABC):
    """ Base class for classes used to characterize documents; that is, to annotate its sentences 
    and tokens, with sufficient data, so as to be able to be processed by this library.
    
    Can work by only assuming that the document only has a raw text, and possibly mention & coreference 
    data, in which case, if possible, the mentions definitions, and coreference partition info will 
    be transfered to the created, characterized copy of the input document.
    Assume that the sentences of the raw text are either separated by a ' ' (a whitespace), or by a 
    '\n' (a return-to-the-line character).
    """
    
    @classmethod
    def characterize_document(cls, document, **kwargs):
        """ Computes information about the sentences and tokens of a Document instance only from its 
        'raw_text' attribute value, and create a corresponding 'characterized document' .
        
        Args:
            document: a Document instance, which may or may not possess only mentions and a 
                coreference partition
            **kwargs: cf the arguments of the 'characterize_documents' method

        Returns:
            a Document instance, corresponding to the characterized deepcopy of the input Document 
            instance
        """
        return cls.characterize_documents((document,), **kwargs)[0]
    
    @classmethod
    @abc.abstractmethod
    def characterize_documents(cls, documents):
        """ Computes information about the sentences and tokens of Document instances only from their 
        'raw_text' attribute value, and create a corresponding 'characterized document' for each of 
        them.
        
        Args:
            documents: an iterable of Document instances, which may or may not possess only mentions 
                and a coreference partition

        Returns:
            a collection of Document instances, corresponding to characterized deepcopies of the 
            respective input Document instances
        """
        pass
    
    @staticmethod
    def _compute_sentence_sep(document):
        """ Determines whether a new version of a document's raw space's sentence separator is a 
        carriage return character '\n', or a space ' '.
        Used as input to the decision about which separator string to use when reconstructing a 
        document's raw text, after its characterization, and its separation in tokens and sentences.
        
        Args:
            document: a Document instance

        Returns:
            a string
        """
        raw_text = document.raw_text
        if re.search("\n", raw_text if len(raw_text) < 2 else raw_text[1:]):
            return "\n"
        return " "
    
    @classmethod
    def _reassemble_mentions_and_entities_data(cls, original_document, characterized_document_buffer, 
                                               strict=True):
        """ The aim of this method is, if mentions existed in the original document, to create a 
        corresponding set of mentions for the newly characterized document (and do the same for the 
        potentially associated coreference_partition data).
        What is tricky is the fact that the 'raw_text' might have been offset, that is why we cannot 
        directly use the same extent.
        
        Args:
            original_document: the Document instance representing the original document
            characterized_document_buffer: DocumentBuffer instance currently holding the data 
                about the characterized Document instance
            strict: a boolean, whether or not to raise an Exception in case where 
                it is impossible to align the raw_text attribute value of the input and the characterized 
                Document instances in the case that mention data exists in the former. If set to False, the 
                characterized Document instance will be returned, but without any mention data.

        Returns:
            the characterized Document with the potential mention and coreference partition 
            info held by the original document
        
        Raises:
            RawTextAlignmentError: if strict is True, and impossible to align previous and new 
                raw text strings
        """
        if original_document.mentions:
            # Define function to use to create a mention's ident from its extent value
            create_mention_ident_from_extent_fct = lambda extent: "{},{}".format(*extent)
            
            # Check whether or not only difference between the raw text of the characterized document 
            # and that of the original document is an offset.
            # Raise an exception, or return characterized document with no mention data if not the case.
            test_raw_text = original_document.raw_text#re.sub("\n", " ", original_document.raw_text)
            offset = 0 # Will be added to the extent '(start, end)'
            offset1 = test_raw_text.find(characterized_document_buffer.raw_text)
            # First try to see if the raw text of the characterized document is included in that of the original document
            if offset1 > -1:
                # If we found it to be the case, then we compute the 'offset' value that will be added to extents values later
                offset = -1 * offset1
            # ELse we try the same by reversing the roles
            else:
                offset2 = characterized_document_buffer.raw_text.find(test_raw_text)
                if offset2 > -1:
                    offset = offset2
                else:
                    # If we could not find just an offset to apply, then we either raise an exception, 
                    # or we return the characterized document, but without mentions or coreference 
                    # entities data.
                    if strict:
                        message = "Cannot align original 'raw_text' with parsed 'raw_text', and so "\
                                  "cannot align existing mentions with newly parsed document."
                        raise RawTextAlignmentError(message, original_raw_text=test_raw_text, 
                                                    characterized_document_raw_text=characterized_document_buffer.raw_text)
                    return characterized_document_buffer.flush(strict=False, 
                                                               enforce_coreference_partition_consistency=False)
            
            def _apply_offset(extent):
                return tuple(e + offset for e in extent)
            
            # Collect the mention data
            original_extent2new_extent = {}
            characterized_document_mentions_data = OrderedDict()
            for original_mention in original_document.mentions:
                new_extent = _apply_offset(original_mention.extent)
                original_extent2new_extent[original_mention.extent] = new_extent
                head_extent = original_mention.head_extent
                new_head_extent = None
                if head_extent is not None:
                    new_head_extent = _apply_offset(head_extent)
                new_ident = create_mention_ident_from_extent_fct(new_extent)
                raw_text = original_mention.raw_text
                mention_data = {"ident": new_ident, "extent": new_extent, "raw_text": raw_text, 
                                "head_extent": new_head_extent}
                for attribute_name in Mention._OPTIONAL_ATTRIBUTE_NAMES:
                    if attribute_name != "head_extent":
                        mention_data[attribute_name] = getattr(original_mention, attribute_name)
                characterized_document_mentions_data[new_extent] = mention_data
            
            # Add the mention data
            characterized_document_buffer.add_mentions(characterized_document_mentions_data)
            
            # Build anew the coreference entities data, if it exists
            original_coreference_partition = original_document.coreference_partition
            if original_coreference_partition is not None:
                def _gen_new_pseudo_entities():
                    """ Creates a generator over collections of extents, each collection representing 
                    a coreference entity found in the 'original_coreference_partition' value, but with 
                    the extents, which each define a mention, having been offset
                    
                    Yields:
                        a collection of extents
                    """
                    for original_entity in original_coreference_partition:
                        yield tuple(_apply_offset(extent) for extent in original_entity)
                new_pseudo_entities = _gen_new_pseudo_entities()
                coreference_partition = CoreferencePartition(entities=new_pseudo_entities)
                characterized_document_buffer.set_coreference_partition(coreference_partition)
            
        return characterized_document_buffer.flush(strict=False, 
                                                   enforce_coreference_partition_consistency=False)


# Exceptions
class RawTextAlignmentError(CoRTeXException):
    """ Exception to be raised when finding that the difference between an original document's raw text 
    the corresponding characterized document's cannot be reduced to a simple offset.
    
    Arguments:
        original_raw_text: string, or None; the original document's raw text
        
        characterized_document_raw_text: string, or None, the characterized document's raw text
    
    Attributes:
        original_raw_text: the value corresponding to the input 'original_raw_text' parameter
        
        characterized_document_raw_text: the value corresponding to the input 
            'characterized_document_raw_text' parameter
    """
    
    def __init__(self, *args, original_raw_text=None, characterized_document_raw_text=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.original_raw_text = original_raw_text
        self.characterized_document_raw_text = characterized_document_raw_text
    def __str__(self):
        s = super().__str__()
        new_s = "{}\n\nOriginal raw_text:\n'{}'\n\nCharacterized document raw text:\n'{}'"
        new_s = new_s.format(s, self.original_raw_text, self.characterized_document_raw_text)
        return new_s
