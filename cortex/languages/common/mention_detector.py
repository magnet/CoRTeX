# -*- coding: utf-8 -*-

"""
Defines a base class used to detect mentions in a document.
"""

__all__ = ["_MentionDetector",
           "evaluate_mention_detection",
           ]

import abc
from collections import OrderedDict

from cortex.api.document_buffer import (DocumentBuffer, deepcopy_document, 
                                        remove_mentions_and_coreference_partition_info,)
from cortex.utils import compute_f1_score, normalize


class _MentionDetector(object, metaclass=abc.ABCMeta):
    """ Base class for classes whose role is to detect mentions in a document.
    A priori, the document will have to be annotated.
    
    The aim is to extract mention candidates corresponding to 'noun phrases'.
    """
    
    @classmethod
    def oracle_detect_mentions(cls, document):
        """ Creates a deepcopy of the input document, without coreference partition data, and with only 
        absolute minimal information on the mentions, as if this copy of the document just had been 
        through the 'detect_mentions' method.
        
        Args:
            document: a Document instance

        Returns:
            a Document instance
        """
        # Copy the document to create the prediction document
        copy_document_ident = "{}_oracle_detected_mentions".format(document.ident)
        copy_document = deepcopy_document(document, copy_document_ident=copy_document_ident)
        
        # Incorporate the original document's mention definitions in the copy document
        ## Wipe the new document clean as far as mentions and coreference partition are concerned: 
        ## 'mentions' is an empty collection, and 'coreference_partition' is None
        remove_mentions_and_coreference_partition_info(copy_document, inplace=True)
        ## Carry out mention data addition
        mentions_data = OrderedDict()
        if document.mentions:
            # Create mentions data for data buffering
            for mention in document.mentions:
                extent = mention.extent
                ident = "{},{}".format(*extent)
                mention_data = {"ident": ident, "extent": extent, 
                                #"head_extent": head_extent,
                                }
                mentions_data[extent] = mention_data
            # Add mention data
            document_buffer = DocumentBuffer(copy_document)
            document_buffer.add_mentions(mentions_data)
            document_buffer.flush(strict=False)
        
        return copy_document
    
    @classmethod
    def detect_mentions(cls, document, detect_verbs=False, inplace=False):
        """ Detects mentions in the document, and update its 'mentions' and 'coreference_partition' 
        attributes, potentially removing already existing mentions and coreference partition information.
        
        Assumes that the document has been characterized (notably, info about sentence's constituency 
        tree and tokens, with synchronization between tokens and constituency tree nodes).
        
        Args:
            detect_verb: whether or not to consider some verbal mention candidates during the search
            inplace: boolean: if True, carries out the detection directly on the input Document instance 
                (which means loss of information regarding potentially present previous mention and coreference 
                partition data); if False (default), creates first a copy of the input Document instance, and 
                then work on it.

        Returns:
            the Document instance which has been modified to host the result of the mention 
            detection (either the input, or a copy of the input, dependening on the value of the 'inplace' 
            parameter)
        """
        # Whether or not to work inplace
        if not inplace:
            copy_document_ident = "{}_detected_mentions".format(document.ident)
            document = deepcopy_document(document, copy_document_ident=copy_document_ident)
        
        # Detect mentions' extent
        mention_head_extent2mention_extent = cls._detect_mentions(document, detect_verbs=detect_verbs) # Warning: assumes that it is possible that some 'head_extent' might in fact encompass several tokens instead of one
        
        # Incorporate detected mentions in the document
        ## Wipe the new document clean as far as mentions and coreference partition are concerned: 
        ## 'mentions' is an empty collection, and 'coreference_partition' is None
        remove_mentions_and_coreference_partition_info(document, inplace=True)
        ## Carry out mention data addition
        mentions_data = OrderedDict()
        if mention_head_extent2mention_extent:
            # Create mentions data for data buffering
            for _, extent in mention_head_extent2mention_extent.items(): #head_extent
                ident = "{},{}".format(*extent)
                mention_data = {"ident": ident, "extent": extent, 
                                #"head_extent": head_extent,
                                }
                mentions_data[extent] = mention_data
            # Add mention data
            document_buffer = DocumentBuffer(document)
            document_buffer.add_mentions(mentions_data)
            document_buffer.flush(strict=False)
        
        # Add info about the process carried out
        new_info = OrderedDict(document.info)
        new_info["processes"] = new_info.get("processes", "") + " , English mention detector"
        document.info = new_info
        
        return document
    
    @classmethod
    @abc.abstractmethod
    def _detect_mentions(cls, document, detect_verbs=False):
        """ Detects mentions in the document, and return the corresponding mention definition data.
        
        Assumes that the document has been characterized (notably, info about sentence's constituency 
        tree and tokens, with synchronization between tokens and constituency tree nodes).
        
        Args:
            detect_verb: whether or not to consider some verbal mention candidates during the search

        Returns:
            a "mention_head_extent => mention_extent" map 
        """
        pass



def evaluate_mention_detection(sys_document, ref_document):
    """ Evaluates the result of a mention detection procedure by comparing the 'mentions' attribute 
    respective values of the input documents.
    
    Args:
        sys_document: a Document instance containing the predicted / detected mentions data
        ref_document: a Document instance containing the ground truth mentions data

    Returns:
        a map with the following 
        entries:
            * "ref_mentions_nb"
            * "sys_mentions_nb"
            * "common_mentions_nb"
            * "recall" : corresponds to "common_mentions_nb" divided "ref_mentions_nb"
            * "precision": corresponds to "common_mentions_nb" divided "sys_mentions_nb"
            * "f1" : combination of "recall" and "precision"
    """
    sys_partition_universe, ref_partition_universe = tuple(set(m.extent for m in doc.mentions) for doc in (sys_document, ref_document))
    ref_mentions_nb = len(ref_partition_universe) # Number of mentions in the 'ref_partition' CoreferencePartition
    sys_mentions_nb = len(sys_partition_universe) # Number of mentions in the 'sys_partition' CoreferencePartition
    common_mentions_nb = len(sys_partition_universe.intersection(ref_partition_universe)) # Number of mentions in the 'sys_partition' CoreferencePartition whose extent correspond to that of a mention of the 'ref_partition' CorefPartition
    recall = normalize(common_mentions_nb, ref_mentions_nb)
    precision = normalize(common_mentions_nb, sys_mentions_nb)
    f1 = compute_f1_score(recall, precision)
    scores = {"recall": recall, "precision": precision, "f1": f1, 
              "ref_mentions_nb": ref_mentions_nb, "sys_mentions_nb": sys_mentions_nb, 
              "common_mentions_nb": common_mentions_nb}
    return scores
