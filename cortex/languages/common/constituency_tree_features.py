# -*- coding: utf-8 -*-

"""
Defines base classes and utilities used to compute data and values corresponding to grammatical 
information, also called grammatical features here, from instances of the ConstituencyTreeNode and 
of the ConstituencyTree classes.
    
Assumes trees are spread on their width rather than on their height (so no "only binary trees") 
- parse trees produced by the Stanford CoreNLP parser should be fine.
"""

__all__ = ["ConstituencyTreeNodeFeatures", 
           "_ConstituencyTreeFeatures",
           ]

import abc

from cortex.api.constituency_tree import CircularDependencyError, InfiniteRecursionChecker

class ConstituencyTreeNodeFeatures(object):
    """ A class defining grammatical features that can be defined on a ConstituencyTreeNode instance.
    
    Assumes that constituency trees are spread on their width rather than on their height (so no 
    "only binary trees") - parse trees produced by the Stanford CoreNLP parser should be fine.
    
    Arguments:
        POS_tags_module: the python module defining the constituency tree tags used to characterize 
            the constituency tree of the document which the mentions originate from. Must be consistent 
            with the language of the documents which the mentions originate from, as well as the 
            'constituency_tree_node_features' input value.
        
        node_head_finder: a _ConstituencyTreeNodeHeadFinder child class instance. Must be consistent 
            with the language of the documents which the mentions originate from, as well as the 
            'POS_tags_module' input value 
    
    Attributes:
        POS_tags_module: the value of the 'POS_tags_module' parameter used to initialize the model 
            class instance
        
        node_head_finder: the value of the 'node_head_finder' parameter used to initialize the model 
            class instance
    """
    
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = ("POS_tags_module", "node_head_finder")
    
    def __init__(self, POS_tags_module, node_head_finder):
        self.POS_tags_module = POS_tags_module
        self.node_head_finder = node_head_finder
    
    ## Instance methods
    def get_head(self, node, head_search_heuristic="leaf"):
        """ Returns the head node of the input node's children.
        
        There is at least two different heuristic to choose from when parsing the subtree 
        corresponding to the input node in search of the head node:
            - "rule": fetch the collection of rules to be applied for the input node, and then iterate 
              over it until one rule application produces a head
            - "leaf": recursively applies the "rules" heuristic, first on the input node, and then on the 
              head produced by each appication of the "rules" heuristic, until the found head node 
              is a leaf node
        If no head node is found, may return None.
        
        Args:
            node: a ConstituencyTreeNode instance
            head_search_heuristic: string, among {"rule","leaf"}

        Returns:
            a ConstituencyTreeNode instance, or None
        """
        return self.node_head_finder.get_head(node, head_search_heuristic=head_search_heuristic)
    
    def is_NP(self, node):
        """ Check whether or not the input node corresponds to a 'noun phrase'.
        
        Args:
            node: a ConstituencyTreeNode instance

        Returns:
            a boolean
        """
        return node.syntactic_tag == self.POS_tags_module.NOUN_PHRASE_TAG
    
    def get_dominating_NP_ancestor(self, node):
        """ Returns the youngest ancestor of input node (included) which corresponds to a 'noun phrase'.
        
        Args:
            node: a ConstituencyTreeNode instance
        
        Returns:
            a boolean
        """
        with InfiniteRecursionChecker("Circular dependency using the attribute 'parent'.") as checker:
            while node is not None and not self.is_NP(node):
                checker.check(node)
                node = node.parent
        return node
    
    def is_maximal_NP(self, node):
        """ Returns whether or not the input node is a maximal spanning 'noun phrase', as far as 
        ancestry is concerned, i.e. "the input node corresponds to a 'noun phrase', and none of its 
        ancestor does".
        
        Args:
            node: a ConstituencyTreeNode instance

        Returns:
            a boolean
        """
        if not self.is_NP(node):
            return False
        ancestor = self.get_dominating_NP_ancestor(node.parent)
        if ancestor is node: # Check for circular dependency
            raise CircularDependencyError("Circular dependency using the attribute 'parent'.")
        if ancestor is None: # We reached the root without finding an ancestor who is NP
            return True
        return False
    
    def get_NP_enum(self, node):
        """ Returns the collection of leaf nodes corresponding to the child nodes of the input node 
        if they define an enumeration of 'noun phrase' or 'proper noun' nodes; return None otherwise.
        
        Args:
            node: ConsistencyTreeNode instance

        Returns:
            collection of ConsistencuTreeNode instances, or None
        
        Examples:
            Assuming a proper constituency tree tagging:
            
            "the huge person and their dog" => ["the", "huge", "person", "their", "dog"]
            
            "Mary, John, and their dog" => ["Mary", "John", "their", "dog"]
        
        Warning:
            Makes the assumption that the tree structure is flat, and not strictly binary: a node 
            can have more than two children.
        """
        if not self.is_NP(node):
            return None
        
        nodes_list = [x for x in node.children]
        failed = False
        found_CC = False # Need at least one CC
        state = 0 # Waiting to read "NP"
        enumerated = []
        NOUNS = [self.POS_tags_module.NOUN_PHRASE_TAG, self.POS_tags_module.PROPER_NOUN_SINGULAR_TAG]
        COORDINATIONSS = [",", self.POS_tags_module.COORDINATING_CONJUNCTION_TAG]
        while not failed and nodes_list:
            node = nodes_list.pop(0)
            tag = node.syntactic_tag
            if tag == self.POS_tags_module.COORDINATING_CONJUNCTION_TAG:
                found_CC = True
            if state == 0 and tag in NOUNS:
                state = 1 # Has read "NP", waiting to read either "," or "CC"
                enumerated.append(tuple(node.get_leaves()))
            elif state == 1 and tag in COORDINATIONSS:
                state = 2
            elif state == 2 and tag in NOUNS:
                state = 3
                enumerated.append(tuple(node.get_leaves()))
            elif state == 2 and tag == self.POS_tags_module.COORDINATING_CONJUNCTION_TAG:
                state = 2 # ACCEPT , followed by CC
            elif state == 3 and tag in NOUNS:
                state = 2
                enumerated.append(tuple(node.get_leaves()))
            elif state == 3 and tag in COORDINATIONSS:
                state = 2 
            else:
                failed = True
                enumerated = None
        
        if state != 3 or not found_CC:
            enumerated = None
        
        enumerated = tuple(enumerated) if enumerated is not None else enumerated
        return enumerated
    '''
    # FIXME: test that new function is OK by parsing the corpus, and checking that the new method recognize at least the same enumeration as did the previous method
    # Check also that the newly recognized enumerations are indeed enumerations
    # Well that went badly, going back to the previous algorithm for now
    def get_NP_enum(self, node):
        """ Returns collection of leaves referring to iterated token if the input node is an NP that 
        defines an enumeration, or None otherwise.
        
        Something akin to "NP CC NP" or "NP , ... NP , CC NP" or "NP , ... NP CC NP" 
        
        Args:
            node: ConsistencyTreeNode instance

        Returns:
            collection or ConstituencyTreeNode instances, or None
        
        Warning:
            Makes the assumption that the tree structure is flat, and not strictly binary: a node 
            can have more than two children.
        """
        if not self.is_NP(node):
            return None
        
        # First, separate the leaves into groups separated by "," node
        # Return an empty list already if  there was no "," node
        NOUNS = [self.POS_tags_module.NOUN_PHRASE_TAG, self.POS_tags_module.PROPER_NOUN_SINGULAR_TAG]
        COORDINATIONSS = [",", self.POS_tags_module.COORDINATING_CONJUNCTION_TAG]
        leaves_list = [x for x in node.get_leaves()]
        groups = []
        found_CC = False
        current_group = []
        while leaves_list:
            leaf = leaves_list.pop(0)
            tag = leaf.parent.syntactic_tag
            if tag == self.POS_tags_module.COORDINATING_CONJUNCTION_TAG:
                found_CC = True
            if tag in COORDINATIONSS:
                if current_group:
                    groups.append(current_group)
                    current_group = []
            else:
                current_group.append(leaf)
        if current_group:
            groups.append(current_group)
        if not found_CC: # Need at least one CC
            return None
        
        # Then, for each group of node, compute the youngest common ancestor of each group
        fct = groups[0][0].get_nodes_youngest_common_ancestor
        ancestors = tuple(fct(t) if len(t) > 1 else t[0].parent for t in groups)
        
        # Check that each ancestor node is a NP; if yes, send out the tuple of nodes tuples
        failed = False
        for ancestor_node in ancestors:
            if ancestor_node.syntactic_tag not in NOUNS:
                failed = True
                break
        if failed:
            return None
        
        # None of the ancestors must be the ancestor of another, apart itself
        failed = False
        for i, node in enumerate(ancestors):
            for other_node in ancestors[i+1:]:
                dist = node.get_ancestor_distance(other_node)
                if dist > -1:
                    failed = True
                    break
                dist = other_node.get_ancestor_distance(node)
                if dist > -1:
                    failed = True
                    break
            if failed:
                break
        if failed:
            return None
        
        # If everything is good, output the groups of leaves
        enumeration = tuple(tuple(g) for g in groups)
        return enumeration
    '''
    def is_NP_enum(self, node): # TODO: find a more elegant way to write this function
        """ Checks that the node's child nodes define an enumeration of 'noun phrase' or 'proper noun' 
        nodes.
        
        Args:
            node: a ConsistencyTreeNode instance

        Returns:
            a boolean
        
        Warning:
            Makes the assumption that the tree structure is flat, and not strictly binary: a node 
            can have more than two children.
        """
        if self.get_NP_enum(node) is not None:
            return True
        return False
    
    def get_tags_leaves_boundaries(self, node, tags, find_head=False, head_search_heuristic="leaf"):
        """ Returns collection of (node's leaves boundaries, node's head node's leaves boundaries) pairs 
        from iterating over nodes, starting from the input and then carrying a breadth-first search 
        on its children, as long as their respective syntactic tag belongs to the input syntactic tags 
        set-like.
        
        Args:
            node: a ConstituencyTreeNode instance
            tags: a set-like object containing the syntactic tags that the nodes' must belong to
            find_head: a boolean; if False, then, for one given node, the leaves boundaries of the 
                head node will be considered to be the leaves boundaries of the node itself; if True, the 
                node_head_finder will be used, but only if the considered node is not NP_enum (if it is, the leaves 
                boundaries of the head node will be considered to be the leaves boundaries of the node itself)
            head_search_heuristic: a string, parameter for the 'get_head' method of the node_head_finder; 
                heuristic with which the head node will be searched for

        Returns:
            a collection of (ConstituencyTreeNode instances pair, ConstituencyTreeNode instances pair) pairs
        """
        get_leaves_boundaries_fct = lambda node_, default_leaves_boundaries: default_leaves_boundaries
        if find_head:
            def _get_head_leaves_boundaries(node_, default_leaves_boundaries):
                """ Returns the leaves boundaries of the head node found for the input node.
                It might be that the concept of head is not defined for some input node (if it 
                corresponds to an enumeration for instance), in which case, for API compatibility 
                purposes, a default value for the output must be provided.
                
                    node_: ConstituencyTreeNode instance, node whose head will be searched for
                    default_leaves_boundaries: default value to output if it is legitimate for a 
                head value not to be found for the input node

                Returns:
                    ConstituencyTreeNode instances pair
                """
                leaves_boundaries = default_leaves_boundaries
                if not self.is_NP_enum(node_):# No head for enumerations
                    head = self.get_head(node_, head_search_heuristic=head_search_heuristic)
                    if head is not None:
                        leaves_boundaries = head.get_leaves_boundaries()
                return leaves_boundaries
            get_leaves_boundaries_fct = _get_head_leaves_boundaries
        
        found = []
        remaining = [node]
        with InfiniteRecursionChecker("Circular dependency using the attribute 'children'.") as checker:
            while remaining:
                current = remaining.pop()
                checker.check(current)
                if current.syntactic_tag in tags:
                    leaves_boundaries = current.get_leaves_boundaries()
                    head_leaves_boundaries = get_leaves_boundaries_fct(current, leaves_boundaries)
                    found.append((leaves_boundaries, head_leaves_boundaries))
                remaining.extend(current.children)
        
        return found
    
    def count_NP_above(self, node):
        """ Computes the number of nodes constituting the ancestry of the input node, and whose type 
        is that of a 'noun phrase'.
        
        Args:
            node: a ConstituencyTreeNode instance

        Returns:
            an int
        """
        return node.count_type_to_root(self.POS_tags_module.NOUN_PHRASE_TAGS)
    
    def count_PP_above(self, node):
        """ Computes the number of nodes constituting the ancestry of the input node, and whose type 
        is that of a 'prepositional phrase'.
        
        Args:
            node: a ConstituencyTreeNode instance

        Returns:
            an nt
        """
        return node.count_type_to_root(self.POS_tags_module.PREPOSITIONAL_PHRASE_TAGS)
    
    def count_VP_above(self, node):
        """ Computes the number of nodes constituting the ancestry of the input node, and whose type 
        is that of a 'verbal phrase'.
        
        Args:
            node: a ConstituencyTreeNode instance

        Returns:
            an int
        """
        return node.count_type_to_root(self.POS_tags_module.VERB_PHRASE_TAGS)



class _ConstituencyTreeFeatures(object, metaclass=abc.ABCMeta):
    """ Base class for classes defining grammatical features that can be defined on a 
    ConstituencyTree instance.
    
    Assumes that constituency trees are spread on their width rather than on their height (so no 
    "only binary trees") - parse trees produced by the Stanford CoreNLP parser should be fine.
    
    Arguments:
        constituency_tree_node_features: a ConstituencyTreeNodeFeatures instance, used under the hood
    
    Attributes:
        constituency_tree_node_features: the value of the 'constituency_tree_node_features' parameter 
            used to initialize the class instance
    """
    
    @abc.abstractmethod
    def __init__(self, constituency_tree_node_features):
        self.constituency_tree_node_features = constituency_tree_node_features
    
    ## Instance methods
    def get_tags_leaves_boundaries(self, constituency_tree, tags, find_head=False, head_search_heuristic="leaf"):
        """ Returns collection of (node leaves boundaries, node's head node leaves boundaries) pairs 
        from iterating over nodes, starting from the root of the input tree and then carrying a 
        breadth-first search on its children, whose syntactic tag belongs to the input syntactic 
        tags set-like.
        
        Args:
            constituency_tree: a ConstituencyTree instance
            tags: a set-like object containing the syntactic tags that the nodes' must belong to
            find_head: a boolean; if False, then, for one given node, the leaves boundaries of the 
                head node will be considered to be the leaves boundaries of the node itself; if True, the 
                node_head_finder will be used, but if the considered node is not NP_enum, in which case the leaves 
                boundaries of the head node will be considered to be the leaves boundaries of the node itself
            head_search_heuristic: a string, parameter for the 'get_head' method of the node_head_finder; 
                heuristic with which the head node will be searched for

        Returns:
            a collection of (ConstituencyTreeNode instance pair, ConstituencyTreeNode instance pair) pairs
        """
        return self.constituency_tree_node_features.get_tags_leaves_boundaries(constituency_tree.root, 
                                                                              tags, find_head=find_head, 
                                                                              head_search_heuristic=head_search_heuristic)