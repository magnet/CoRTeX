# -*- coding: utf-8 -*-

"""
Defines utilities / data that may be shared by several languages
"""

"""
Available submodule(s)
-----------------------
constituency_tree_features
    Defines base classes and utilities used to compute data and values from instances of the 
    ConstituencyTreeNode and of ConstituencyTree classes

constituency_tree_node_head_finder
    Defines base classes and utilities used to find the head child node of a ConstituencyTreeNode 
    instance

document_characterizer
    Defines a base class used to characterize documents

mention_detector
    Defines a base class used to detect mentions in a document

mention_features
    Defines a class used to compute data and values from instances of the Mention class
"""