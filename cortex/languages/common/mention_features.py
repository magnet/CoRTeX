# -*- coding: utf-8 -*-

"""
Defines a class used to compute data and values corresponding to grammatical information, also called 
grammatical features here, from instances of the Mention class.
"""

__all__ = ["MentionFeatures",
           ]

from cortex.utils import EqualityMixIn
from cortex.parameters.mention_data_tags import (FIRST_PERSON_TAG, SECOND_PERSON_TAG, 
                                                   THIRD_PERSON_TAG, EXPANDED_PRONOUN_GRAM_TYPE_TAG, 
                                                   NAME_GRAM_TYPE_TAG, NOMINAL_GRAM_TYPE_TAG, 
                                                   VERB_GRAM_TYPE_TAG, UNKNOWN_VALUE_TAG)

MAXIMUM_INTERVAL_LENGTH_BETWEEN_COPULAR_MENTIONS = 14 # Arbitrary set to contain a copular verb # TODO: parameterize this
WN_PERSON_HYPERNYM = "person.n.01"
WN_ANIMAL_HYPERNYM = "animal.n.01"
WN_HOMO_HYPERNYM = "homo.n.02"

class MentionFeatures(EqualityMixIn):
    """ A class defining the grammatical features that can be interesting to know about a mention.
    
    Is adapted to current implementations of the support for the English and French languages.
    When used on mentions, generally assume that they are part of a document whose sentences are 
    characterized by their respective constituency tree, and that each mention has been synchronized 
    with its corresponding tokens.
    
    Arguments:
        POS_tags_module: the python module defining the constituency tree tags used to characterize 
            the constituency tree of the document which the mentions originate from. Must be consistent with 
            the language of the documents which the mentions originate from, as well as the 
            'constituency_tree_node_features' input value.
        
        NE_tags_module: the python module defining the named entities tags used to characterize 
            those of the document which the mentions originate from. Must be consistent with 
            the language of the documents which the mentions originate from, as well as the 
            'constituency_tree_node_features' input value.
        
        lexicon: the python module defining lexical data (ex: collection of words...) that are known 
            to be associated to definite roles, and so can be leveraged to infer grammatical information. 
            Must be consistent with the language of the documents which the mentions originate from.
        
        constituency_tree_node_features: a ConstituencyTreeNodeFeatures instance, the one to use 
            internally, when computing features values. Must be consistent with the language of the 
            documents which the mentions originate from, as well as the POS tags used and defined in the 
            'POS_tags_module' input value.
    
    Attributes:
        POS_tags_module: the value of the 'POS_tags_module' parameter used to initialize the model 
            class instance
        
        NE_tags_module: the value of the 'NE_tags_module' parameter used to initialize the model 
            class instance
        
        lexicon: the value of the 'lexicon' parameter used to initialize the class instance
        
        constituency_tree_node_features: the value of the 'constituency_tree_node_features' parameter 
            used to initialize the class instance
    """
    
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = ("POS_tags_module", "NE_tags_module", "lexicon", 
                                                "constituency_tree_node_features")
    
    def __init__(self, POS_tags_module, NE_tags_module, lexicon, constituency_tree_node_features):
        self.POS_tags_module = POS_tags_module
        self.NE_tags_module = NE_tags_module
        self.lexicon = lexicon
        self.constituency_tree_node_features = constituency_tree_node_features
    
    ## Instance methods
    def get_dominating_node(self, mention):
        """ Determines the youngest ConstituencyTreeNode that is so that all tokens constituting this 
        mention are associated to nodes that are descendant of this node.
        
        Assumes that the mention has been synchronized with its tokens.
        
        Args:
            mention: a Mention instance

        Returns:
            a ConstituencyTreeNode instance
        """
        # Get dominating node in parse tree
        lnode = mention.tokens[0].constituency_tree_node
        rnode = mention.tokens[-1].constituency_tree_node
        dominating_node = lnode.get_youngest_common_ancestor(rnode)
        # In the case that the mention is made of only one token, we want the node that carries the constituent tag info, which is the parent
        if dominating_node.is_leaf():
            dominating_node = dominating_node.parent
        return dominating_node
    
    def count_above(self, mention):
        """ Computes the number of nodes constituting the ancestry of the constituency tree node 
        associated to the head token of the mention.
        
        Args:
            mention: a Mention instance

        Returns:
            an int
        """
        node = mention.head_tokens[-1].constituency_tree_node
        return node.get_self_depth()
    
    def is_expanded_pronoun(self, mention):
        """ Determines whether or not a mention has been found to be of 'extended pronoun' grammatical type.
        Assume mention's gram type has been characterized.
        
        Args:
            mention: a Mention instance

        Returns:
            a boolean
        """
        return mention.gram_type == EXPANDED_PRONOUN_GRAM_TYPE_TAG
    
    def is_name(self, mention):
        """ Determines whether or not a mention has been found to be of 'name' grammatical type.
        
        Assumes mention's gram type has been characterized.
        
        Args:
            mention: a Mention instance

        Returns:
            a boolean
        """
        return mention.gram_type == NAME_GRAM_TYPE_TAG
    
    def is_nominal(self, mention):
        """ Determines whether or not a mention has been found to be of 'nominal' grammatical type.
        
        Assumes mention's gram type has been characterized.
        
        Args:
            mention: a Mention instance

        Returns:
            a boolean
        """
        return mention.gram_type == NOMINAL_GRAM_TYPE_TAG
    
    def is_verb(self, mention):
        """ Determines whether or not a mention has been found to be of 'verb' grammatical type.
        
        Assumes mention's gram type has been characterized.
        
        Args:
            mention: a Mention instance

        Returns:
            a boolean
        """
        return mention.gram_type == VERB_GRAM_TYPE_TAG
    
    def abbrev(self, mention):
        """ Returns an abbreviated form of the words of a mention, if susceptible to.
        
        Examples:
            'I.B.M.' => 'IBM' (if mention only one word, remove the dots)
            
            'Federal Bureau of Investigation' => 'FBI' (else, return string made one first character of 
            words starting with an uppercase character)
            
            As such, the result may be empty if the mention is not susceptible to being abbreviated.
        
        Args:
            mention: a Mention instance

        Returns:
            a string (possible empty)
        """
        words = mention.raw_text.split()
        if len(words) == 1:
            short_form = mention.raw_text.replace(".", "")  # Ex: convert "I.B.M" to "IBM"
        else:  # convert "International Business Machine" to "IBM"
            short_form_array = []
            for word in words:
                if word[0].isupper():
                    short_form_array.append(word[0])
            short_form = "".join(short_form_array)
        return short_form
    
    def gender_agr(self, mention1, mention2):
        """ Checks whether two mentions have been associated to the same grammatical gender value, or 
        not. 
        
        If the value is unknown for at least one mention, or if it was not found, still returns True.
        
        Args:
            mention1: a Mention instance, first mention to be compared
            mention2: a Mention instance, second mention to be compared

        Returns:
            a boolean
        """
        g1 = mention1.gender
        g2 = mention2.gender
        return (g1 == g2) or (g1 in (None, UNKNOWN_VALUE_TAG)) or (g2 in (None, UNKNOWN_VALUE_TAG))

    def number_agr(self, mention1, mention2):
        """ Check whether two mentions have been associated to the same grammatical number value, or 
        not.
        
        If the value is unknown for at least one mention, or if it was not found, still returns True.
        
        Args:
            mention1: a Mention instance, first mention to be compared
            mention2: a Mention instance, second mention to be compared

        Returns:
            a boolean
        """
        n1 = mention1.number
        n2 = mention2.number
        return (n1 == n2) or (n1 in (None, UNKNOWN_VALUE_TAG)) or (n2 in (None, UNKNOWN_VALUE_TAG))
    
    def morph_agr(self, mention1, mention2):
        """ Checks whether two mentions' respective grammatical gender and grammatical number agree, 
        or not.
        
        For one of those two characteristics, if the value is unknown for at least one mention, 
        or if it was not found, the agreement for this characteristic is still considered to be True.
        
        Args:
            mention1: a Mention instance, first mention to be compared
            mention2: a Mention instance, second mention to be compared

        Returns:
            a boolean
        """
        return self.gender_agr(mention1, mention2) and self.number_agr(mention1, mention2)
    
    def is_appositive(self, apposed_mention, encompassing_mention):
        """ Determine whether or not a mention can be considered to be an (extra) appositive to the 
        first part of another, encompassing mention. 'extract' here means that the apposed mention must 
        be separated from the first part of the encompassing mention by a comma.
        
        Args:
            apposed_mention: a Mention instance, 
            encompassing_mention: a Mention instance, 

        Returns:
            a boolean
        
        Examples:
            If 'apposed_mention' <=> "Mary" and 'encompassing_mention' <=> "a listener, Mary", then
            is_appositive(apposed_mention, encompassing_mention) = True.
        """
        if apposed_mention.finishes(encompassing_mention): # Rel. position
            app_m_str = apposed_mention.raw_text
            enc_m_str = encompassing_mention.raw_text[:-len(app_m_str)].rstrip()
            if enc_m_str.endswith(","): # Comma
                return True
        return False
    
    def constituency_tree_distance(self, mention1, mention2):
        """ Returns the distance in the constituency tree between the nodes respectively associated 
        to the head token of each of the input mentions.
        
        Assumes synchronization with head tokens. Assumes that mentions are part of the same sentence, 
        and such share references to the same constituency tree.
        
        Args:
            mention1: a Mention instance, first mention to be compared
            mention2: a Mention instance, second mention to be compared

        Returns:
            an int
        """
        # TODO: change the algorithm of the distance computation for: 'Dist(n1, n2) = Dist(root, n1) + Dist(root, n2) - 2*Dist(root, lca)'
        token1 = mention1.head_tokens[-1]
        token2 = mention2.head_tokens[-1]
        tleaf1 = token1.constituency_tree_node
        tleaf2 = token2.constituency_tree_node
        common_node = tleaf1.get_youngest_common_ancestor(tleaf2)
        # if common_node != None: # FIXME: (legacy) --> infinity if not in the same sentence ?
        '''
        if common_node is None:
            msg = "constituency_tree_distance({}, {}): common ancestor is None: bad synchronization "\
                  "with tokens? Mentions do not belongs to same Document?"
            msg = msg.format(mention1, mention2)
            raise ValueError(msg)
        '''
        tdist1 = tleaf1.get_ancestor_distance(common_node)
        tdist2 = tleaf2.get_ancestor_distance(common_node)
        dist = int(tdist1) + int(tdist2)
        return dist
    
    ########### Position and distance methods ############
    def mention_index(self, mention):
        """ Return the index of the input mention w.r.t. to the collection of mentions associated 
        to the document it originates from.
        
        Args:
            mention: a Mention instance

        Returns:
            an int
        """
        return mention.document.extent2mention_index[mention.extent]
    
    def mention_sentence_index(self, mention):
        """ Returns the index of the input mention's sentence w.r.t. to the collection of sentences 
        associated to the document it originates from.
        
        Args:
            mention: a Mention instance

        Returns:
           an int
        """
        return mention.document.extent2sentence_index[mention.sentence.extent]
    
    def mention_in_sentence_index(self, mention):
        """ Returns the index of the input mention w.r.t. to the collection of mentions associated 
        to the sentence it originates from.
        
        Args:
            mention: a Mention instance

        Returns:
            an int
        """
        return mention.sentence.extent2mention_index[mention.extent]
    
    def mention_distance(self, mention1, mention2):
        """ Computes the distance between two mentions, defined as the absolute value of the difference 
        between their respective 'document' index value (Cf the 'mention_index' method).
        
        Assumes that both mentions are part of the same document.
        
        Args:
            mention1: a Mention instance, first mention to be compared
            mention2: a Mention instance, second mention to be compared

        Returns:
            an int
        """
        return abs(self.mention_index(mention1) - self.mention_index(mention2))

    def sentence_distance(self, mention1, mention2):
        """ Computes the distance between two mentions, defined as the absolute value of the difference 
        between their respective 'sentence' index value (Cf the 'mention_sentence_index' method).
        
        Assumes that both mentions are part of the same document.
        
        Args:
            mention1: a Mention instance, first mention to be compared
            mention2: a Mention instance, second mention to be compared

        Returns:
            an int
        """
        return abs(self.mention_sentence_index(mention1) - self.mention_sentence_index(mention2))
    
    ############ Alias methods ############
    def is_alias_of(self, studied_mention, compared_mention):
        """ Determine whether or not one mention an alias of another.
        
        This is the case 
        if:
            - both mentions are of 'name' grammatical type
            
            and
            
            - they share the same raw text, or can be abbreviated to the same value.
        
        Args:
            studied_mention: a Mention instance
            compared_mention: a Mention instance
        
        Returns:
            a boolean
        """
        if self.is_name(studied_mention) and self.is_name(compared_mention):
            # FIXME: legacy comment
            # if self.is_NNP() and other_mention.is_NNP(): # FIXME should be is characterizer ?
            return studied_mention.raw_text != compared_mention.raw_text and self.abbrev(studied_mention) == self.abbrev(compared_mention)
        return False
    
    #######################################
    def get_dominating_NP_ancestor_node(self, mention):
        """ Fetch the youngest constituency tree node that encompasses the input mention and which 
        is a 'noun phrase'-type node.
        Assume that the mention has been synchronized with its tokens.
        
        Args:
            mention: a Mention instance

        Returns:
            a ConstituencyTreeNode instance
        """
        dominating_node = self.get_dominating_node(mention)
        return self.constituency_tree_node_features.get_dominating_NP_ancestor(dominating_node)
    
    def get_sentence_type(self, mention):
        """ Fetches the youngest constituency tree node that encompasses the input mention and which 
        is a 'sentence'-type node, and return its tag.
        
        Assumes that the mention has head tokens and has been synchronized with them.
        If it is not possible to find such a node, returns None.
        
        Args:
            mention: a Mention instance

        Returns:
            a string, or None
        """
        node = mention.head_tokens[-1].constituency_tree_node
        sentence_node = node.get_youngest_tagged_ancestor(self.POS_tags_module.SENTENCE_TAGS)
        if sentence_node is not None:
            return sentence_node.syntactic_tag
        return None
    
    def get_enumeration(self, mention):
        """ Returns the ordered subcollection of the tokens of the mention, corresponding to the elements 
        that are enumerated, if the mention is an enumeration (None otherwise).
        
        Assumes that the mention has been synchronized with its tokens.
        
        Args:
            mention: a Mention instance

        Returns:
            a collection of Token instance, or None
        
        Examples:
            mention <=> "John , Jessie , Jack and Julia"
            
            output <=> [Token(John), Token(Jessie), Token(Jack), Token(Julia)]
        """
        dom_NP_node = self.get_dominating_NP_ancestor_node(mention)
        if dom_NP_node is not None:
            if not dom_NP_node.raw_extent == mention.extent:
                return None
            nodes_enumeration = self.constituency_tree_node_features.get_NP_enum(dom_NP_node)
            if nodes_enumeration is None:
                return None
            
            nodes_tokens = [tuple(node.token for node in nodes) for nodes in nodes_enumeration]
            enumeration = nodes_tokens
            return enumeration
        return None
    
    def is_enumeration(self, mention):
        """ Determines whether or not a mention is an enumeration.
        
        Assumes that the mention has been synchronized with its tokens.
        
        Args:
            mention: a Mention instance

        Returns:
            a boolean
        """
        is_enum = False
        dom_NP_node = self.get_dominating_NP_ancestor_node(mention)
        if dom_NP_node is not None:
            is_enum = dom_NP_node.raw_extent == mention.extent and self.get_enumeration(mention) is not None
        return is_enum
    
    def find_mention_head_extent(self, mention):
        """ Finds the extent of the head of a Mention instance.
        
        The Mention instance must have been synchronized with its Tokens, and the Tokens must have 
        been synchronized with their respective ConstituencyTreeNode instances.
        
        Args:
            mention: Mention instance

        Returns:
            a (start, end) tuple of int
        
        Warning:
            If the HeadFinder cannot find a head, the returned extent will correspond to the 
            extent of the mention root constituency tree node, which is the youngest common ancestor 
            between those of the first and last token constituting the mention. This extent will be 
            constrained to be included within the extent of the Mention instance itself.
        """
        tokens = mention.tokens
        if not tokens:
            raise ValueError("'tokens' attribute is None or empty for Mention '{}', cannot proceed.".format(mention))
        
        mention_root_node = self.get_dominating_node(mention)
        if mention_root_node is None:
            raise ValueError("Could not find a root constituency tree node ('{}') for the mention "\
                             "'{}', cannot proceed.".format(mention_root_node, mention))
        
        # The default 'head_extent' value is the mention's
        head_extent = mention.extent#mention_root_node.raw_extent
        head_node = self.constituency_tree_node_features.get_head(mention_root_node, head_search_heuristic="leaf")
        if head_node is not None:
            (h_left_leaf, h_right_leaf) = head_node.get_leaves_boundaries()
            head_start = h_left_leaf.start
            head_end = h_right_leaf.end
            m_start, m_end = mention.extent
            if head_start < m_start: # Warning:this might result in a head_extent encompassing several tokens
                head_start = m_start
            if head_end > m_end: # Warning: this might result in a head_extent encompassing several tokens
                head_end = m_end
            # FIXME: the following condition is because the mention_root_node may encompass more tokens than the mention refers to, and the head token may be one of those extra-token
            if head_start > m_end or head_end < m_start: # If using the first_token yields inconsistent results with the mention's boundaries, we use the last_token
                head_start, head_end = tokens[-1].extent
            head_extent = (head_start, head_end)
        
        return head_extent
    
    def count_NP_above(self, mention):
        """ Computes the number of nodes constituting the ancestry of the constituency tree node 
        associated to the head token of the mention, and whose type is that of a 'noun phrase'.
        
        Assumes that the mention has head tokens and has been synchronized with them.
        
        Args:
            mention: a Mention instance

        Returns:
            an int
        """
        node = mention.head_tokens[-1].constituency_tree_node
        return self.constituency_tree_node_features.count_NP_above(node)
    
    def count_PP_above(self, mention):
        """ Computes the number of nodes constituting the ancestry of the constituency tree node 
        associated to the head token of the mention, and whose type is that of a 'prepositional phrase'.
        
        Assumes that the mention has head tokens and has been synchronized with them.
        
        Args:
            mention: a Mention instance

        Returns:
            an int
        """
        node = mention.head_tokens[-1].constituency_tree_node
        return self.constituency_tree_node_features.count_PP_above(node)
    
    def count_VP_above(self, mention):
        """ Computes the number of nodes constituting the ancestry of the constituency tree node 
        associated to the head token of the mention, and whose type is that of a 'verbal phrase'.
        
        Assumes that the mention has head tokens and has been synchronized with them.
        
        Args:
            mention: a Mention instance

        Returns:
            an int
        """
        node = mention.head_tokens[-1].constituency_tree_node
        return self.constituency_tree_node_features.count_VP_above(node)
    
    def get_same_speaker_pronoun_data(self, antecedent_mention, subsequent_mention):
        """ Tests that input mentions are expanded pronouns of the same grammatical number (for singular 
        first, plural first, singular second), and that the speaker information associated to their 
        respective sentence, if present, is the same.
        
        Assumes that both mentions originate from the same document.
        If speaker data is not present for one of the mentions, or if at least one of the mention is 
        not of 'expanded pronoun' grammatical type, then this function is not applicable, and will 
        return None.
        
        Args:
            antecedent_mention: a Mention instance, that comes before the 'subsequent_mention' in the document
            subsequent_mention: a Mention instance, that comes after the 'antecedent_mention' in the document
        
        Returns:
            a (same_speaker_pronoun_1sg_value, same_speaker_pronoun_1pl_value, same_speaker_pronoun_2sg_value) 
            triplet, or None if not applicable:
                
                - 'same_speaker_pronoun_1sg_value': boolean, False if mentions' grammatical number 
                  is not first person singular
                - 'same_speaker_pronoun_1pl_value': boolean, False if mentions' grammatical number 
                  is not first person plural
                - 'same_speaker_pronoun_2sg_value': boolean, False if mentions' grammatical number 
                  is not second person singular
        """
        # Speaker
        l_speaker = antecedent_mention.sentence.speaker
        r_speaker = subsequent_mention.sentence.speaker
        l_text = antecedent_mention.raw_text.lower()
        r_text = subsequent_mention.raw_text.lower()
        if l_speaker is not None and r_speaker is not None:
            if self.is_expanded_pronoun(antecedent_mention) and self.is_expanded_pronoun(subsequent_mention):
                same_speaker_pronoun_1sg_value = (l_speaker==r_speaker and l_text in self.lexicon.EXPANDED_SINGULAR_FIRST_PERSON_PERSONAL_PRONOUN_MENTIONS and r_text in self.lexicon.EXPANDED_SINGULAR_FIRST_PERSON_PERSONAL_PRONOUN_MENTIONS)
                same_speaker_pronoun_1pl_value = (l_speaker==r_speaker and l_text in self.lexicon.EXPANDED_SINGULAR_FIRST_PERSON_PERSONAL_PRONOUN_MENTIONS and r_text in self.lexicon.EXPANDED_SINGULAR_FIRST_PERSON_PERSONAL_PRONOUN_MENTIONS)
                same_speaker_pronoun_2sg_value = (l_speaker==r_speaker and l_text in self.lexicon.EXPANDED_SINGULAR_FIRST_PERSON_PERSONAL_PRONOUN_MENTIONS and r_text in self.lexicon.EXPANDED_SINGULAR_FIRST_PERSON_PERSONAL_PRONOUN_MENTIONS)
                return same_speaker_pronoun_1sg_value, same_speaker_pronoun_1pl_value, same_speaker_pronoun_2sg_value
            return None
        return None
    
    def is_copula(self, antecedent_mention, subsequent_mention):
        """ Determines whether or not two mentions form a copula, based on the study of the presence 
        of a copular verb between the two, in the document.
        
        Assumes that both mention come from the same document.
        
        Args:
            antecedent_mention: a Mention instance, that comes before the 'subsequent_mention' 
                in the document
            subsequent_mention: a Mention instance, that comes after the 'antecedent_mention' 
                in the document

        Returns:
            a boolean
        """
        # The pattern to look for is: "antecedent_mention COPULA subsequent_mention".
        if subsequent_mention <= antecedent_mention:
            return False
        document = antecedent_mention.document
        if antecedent_mention.sentence is subsequent_mention.sentence:
            e = antecedent_mention.end
            s = subsequent_mention.start
            interval_length = s-e
            if interval_length >= 2 and interval_length <= MAXIMUM_INTERVAL_LENGTH_BETWEEN_COPULAR_MENTIONS:
                inter_mention_text_extent = (e+1,s-1)
                inter_mention_text = document.get_raw_text_extract(inter_mention_text_extent).lower()
                for w in inter_mention_text.split():
                    if w in self.lexicon.COPULAR_VERBS:
                        return True
                return False
        return False
    
    def in_copula_with(self, antecedent_mention, subsequent_mention):
        """ Determines whether or not two mentions form a copula based on their sentence's predicate 
        argument data, if it exists.
        
        Args:
            antecedent_mention: a Mention instance, that comes before the 'subsequent_mention' 
                in the document
            subsequent_mention: a Mention instance, that comes after the 'antecedent_mention' 
                in the document

        Returns:
            a boolean
        """
        # The pattern to look for is: "antecedent_mention COPULA subsequent_mention".
        if subsequent_mention <= antecedent_mention:
            return False
        sentence = antecedent_mention.sentence
        document = antecedent_mention.document
        if sentence == subsequent_mention.sentence and sentence.predicate_arguments:
            s, e = antecedent_mention.extent
            s2, e2 = subsequent_mention.extent
            for predicate_argument in sentence.predicate_arguments:
                match_antecedent_mention = match_subsequent_mention = match_verb = False
                for s3, e3, t in predicate_argument:
                    if t in ("ARG0", "ARG1", "ARG2"):
                        if s == s3 and e == e3:
                            match_antecedent_mention = True
                        if s2 == s3 and e2 == e3:
                            match_subsequent_mention = True
                    if t == "V":
                        verb_token = document.extent2token((s3, e3))
                        if verb_token.lemma in self.lexicon.COPULAR_VERBS:
                            match_verb = True
                if match_antecedent_mention and match_subsequent_mention and match_verb:
                    return True
        return False
    
    def is_reflexive_pronoun(self, mention):
        """ Determines whether or not the mention is a reflexive pronoun.
        
        Args:
            mention: a Mention instance
        
        Returns:
            a boolean
        """
        return mention.raw_text.lower() in self.lexicon.REFLEXIVE_PRONOUN_MENTIONS
    
    def get_person(self, mention):
        """ Defines the grammatical person tag to be associated to the input mention, if applicable.
        
        If not, return None.
        
        Args:
            mention: a Mention instance

        Returns:
            a string (person tag), or None if not a known pronoun
        """
        # FIXME: renamed into 'define_person'?
        text = mention.raw_text.lower()
        if text in self.lexicon.EXPANDED_FIRST_PERSON_PERSONAL_PRONOUN_MENTIONS:
            return FIRST_PERSON_TAG
        if text in self.lexicon.EXPANDED_SECOND_PERSON_PERSONAL_PRONOUN_MENTIONS:
            return SECOND_PERSON_TAG
        if text in self.lexicon.EXPANDED_THIRD_PERSON_PERSONAL_PRONOUN_MENTIONS:
            return THIRD_PERSON_TAG
        return None
    
    def is_coordination(self, mention):
        """ Determines whether or not a mention is a coordination from the point of view of its first 
        and last token.
        
        Assumes that the mention has been synchronized with its tokens.
        
        Args:
            mention: a Mention instance

        Returns:
            a boolean
        """
        is_coord = False
        tokens = mention.tokens
        left = tokens[0].constituency_tree_node
        right = tokens[-1].constituency_tree_node
        ancestor = left.get_youngest_common_ancestor(right)
        # If the found 'ancestor' value is consistent with the mention's definition
        if ancestor is not None and ancestor.raw_extent == mention.extent and ancestor.syntactic_tag == self.POS_tags_module.NOUN_PHRASE_TAG:
            # Then look for a child node that has a coordination role.
            for node in ancestor.children:
                if node.syntactic_tag == self.POS_tags_module.COORDINATING_CONJUNCTION_TAG:
                    is_coord = True
                    break
        return is_coord
    
    def in_coordination(self, mention):
        """ Determines whether or not a mention is part of a coordination, when viewed from the point 
        of view of its sentence's constituency tree.
        
        Assumesthat the mention has been synchronized with its tokens.
        
        Args:
            mention: a Mention instance

        Returns:
            a boolean
        """
        in_coord = False
        tokens = mention.tokens
        left = tokens[0].constituency_tree_node
        right = tokens[-1].constituency_tree_node
        ancestor = left.get_youngest_common_ancestor(right)
        domNP = self.constituency_tree_node_features.get_dominating_NP_ancestor(ancestor)
        if domNP is not None:
            # If the first 'domNP' node found corresponds exactly to the mention, we look for its own 
            # 'domNP' node, because we want to test whether there is something bigger than the mention 
            # that is a coordination.
            # Else, we assume that the dominating node does correspond to something bigger, else it 
            # would not be the mention's domination NP node.
            if domNP.raw_extent == mention.extent:
                parent = domNP.parent
                if parent is not None:
                    domNP = self.constituency_tree_node_features.get_dominating_NP_ancestor(parent)
            if domNP is not None:
                for node in domNP.children:
                    if node.syntactic_tag == self.POS_tags_module.COORDINATING_CONJUNCTION_TAG:
                        in_coord = True
                        break
        return in_coord
    
    def is_animated(self, mention):
        """ Determines whether or not a mention correspond to an animated entity.
        
        Args:
            mention: a Mention instance
        
        Returns:
            a boolean, or UNKNOWN_VALUE_TAG when could not answer with certainty
        """
        hyper = mention.wn_hypernyms
        named_entity = mention.named_entity
        ne_type = named_entity.type if named_entity else None
        if hyper is not None and (WN_PERSON_HYPERNYM in hyper or WN_ANIMAL_HYPERNYM in hyper or WN_HOMO_HYPERNYM in hyper):
            return True
        if ne_type in self.NE_tags_module.ANIMATES_NAMED_ENTITY_TYPE_TAGS:
            return True
        if ne_type in self.NE_tags_module.INANIMATES_NAMED_ENTITY_TYPE_TAGS:
            return False
        if mention.raw_text.lower() in self.lexicon.EXPANDED_ANIMATE_PRONOUN_MENTIONS:
            return True
        return UNKNOWN_VALUE_TAG
    
    def is_inanimated(self, mention):
        """ Determines whether or not a mention correspond to an inanimated entity.
        
        Returns:
            a boolean, or UNKNOWN_VALUE_TAG when could not answer with certainty
        """
        animated = self.is_animated(mention)
        if animated != UNKNOWN_VALUE_TAG:
            return not animated
        return UNKNOWN_VALUE_TAG
