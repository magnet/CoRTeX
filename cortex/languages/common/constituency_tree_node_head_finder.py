# -*- coding: utf-8 -*-

"""
Defines base classes defining utilities used to find the head child node of a ConstituencyTreeNode instance.
"""

__all__ = ["_BaseRulesConstituencyTreeNodeHeadFinder",
           "_ConstituencyTreeNodeHeadFinder",
           "ConstituencyTreeNodeHeadNotFoundError",
           ]

import abc
from copy import deepcopy

from cortex.utils import CoRTeXException

class _ConstituencyTreeNodeHeadFinder(object, metaclass=abc.ABCMeta):
    """ Base class for classes acting as tools to find the head child node of a ConstituencyTreeNode 
    instance.
    """
    
    @classmethod
    @abc.abstractmethod
    def get_head(cls, node):
        """ Find the grammatical head child node of the input node.
        
        Args:
            node: a ConstituencyTreeNode instance

        Returns:
            a ConstituencyTreeNode instance, or None, if could not find a suitable head
        """
        pass


class _BaseRulesConstituencyTreeNodeHeadFinder(_ConstituencyTreeNodeHeadFinder):
    """ Base class for classes acting as tools to find the head child node of a ConstituencyTreeNode 
    instance, and that use a set of rules, and an algorithm based on the application of those rules, 
    to achieve that goal.
    
    Attributes:
        RULES: a "syntactic tag value => rule" map
    """
    
    _RULES = None
    
    @property
    def RULES(self):
        return deepcopy(self._RULES)
    @RULES.setter
    def RULES(self, value):
        raise AttributeError
    
    @classmethod
    def get_head(cls, node, head_search_heuristic="leaf"):
        """ Returns the head node of the input node's children.
        
        There is at least two different heuristic to choose from when parsing the subtree 
        corresponding to the input node in search of the head node:
            - "rule": fetch the collection of rules to be applied for the input node, and then iterate 
              over it until one rule application produces a head
            - "leaf": recursively applies the "rules" heuristic, first on the input node, and then on the 
              head produced by each appication of the "rules" heuristic, until the found head node is a leaf 
              node
        
        If no head node is found, may return None.
        
        Args:
            node: a ConstituencyTreeNode instance
            head_search_heuristic: a string, among {"rule","leaf"}

        Returns:
            a ConstituencyTreeNode instance, or None
        """
        POSSIBLE_HEAD_SEARCH_HEURISTIC = set(["rule","leaf"])
        if head_search_heuristic not in POSSIBLE_HEAD_SEARCH_HEURISTIC:
            raise ValueError("ConstituencyTreeNode error: wrong head search heuristic parameter "\
                             "value ('{}'), possible values belong to {}."\
                             .format(head_search_heuristic, list(POSSIBLE_HEAD_SEARCH_HEURISTIC)))
        if head_search_heuristic == "rule":
            head = cls._get_head_rule(node)
        elif head_search_heuristic == "leaf":
            head = cls._get_head_leaf(node)
        
        return head
    
    @classmethod
    def _get_head_rule(cls, node):
        """ Returns the head of a node's children based on the use of the RULES potentially 
        associated to it.
        
        Iterates over the rules, and try to define a head from its application to the input node's 
        children, in successive order, until one is found. Return None if none is found.
        
        Args:
            node: a ConstituencyTreeNode instance

        Returns:
            a ConstituencyTreeNode instance, or None
        """
        rules = cls._RULES.get(node.syntactic_tag, None)
        if rules is not None:
            L = len(rules)
            for i, rule in enumerate(rules):
                last_resort = i == L-1 # Ask to apply default rule only if we are down to our last rule
                head = cls._get_head_from_rule(node, rule, last_resort)
                if head is not None:
                    return head
        return None

    @classmethod
    def _get_head_leaf(cls, node):
        """ Returns the head of a node's children which is a leaf of the node (potentially using 
        recursion on the children nodes).
        
        Args:
            node: a ConstituencyTreeNode instance

        Returns:
            a ConstituencyTreeNode instance, or None
        """
        head = node
        while head is not None and not head.is_leaf():
            if len(head.children) == 1 and head.children[0].is_leaf():
                head = head.children[0]
                break
            
            rules = cls._RULES.get(head.syntactic_tag, None)
            new_head = None
            if rules is not None:
                L = len(rules)
                for i,rule in enumerate(rules):
                    last_resort = i == L-1 # Ask to apply default rule in case of failure, only if we are down to our last rule
                    new_head = cls._get_head_from_rule(head, rule, last_resort)
                    if new_head is not None:
                        break
            head = new_head
        if head is None:
            #head = node # could not find head
            pass
        return head
    
    @classmethod
    @abc.abstractmethod
    def _get_head_from_rule(cls, node, rule, last_resort):
        """ Finds the head node of the input node's children, by applying the provided rule.
        
        Args:
            node: a ConstituencyTreeNode instance
            rule: the rule to use
            last_resort: boolean; if True, then if the search carried out using the input rule 
                did not turn out a result, then a final, default rule must be used; if False, then if the 
                search carried out using the input rule did not turn out a result, then None shall be returned.

        Returns:
            a ConstituencyTreeNode instance, or None
        """
        pass

# Exceptions
class ConstituencyTreeNodeHeadNotFoundError(CoRTeXException):
    """ Exception to be raised when failing to find a suitable head. """
    pass
