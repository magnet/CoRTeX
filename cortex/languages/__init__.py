# -*- coding: utf-8 -*-

"""
Defines classes and data that are specific to the different languages supported by the toolbox.

Documents are characterized by data (e.g.: tags) that may differ from one language to another.
Tools used by the toolbox may need to know the set of possible tags / data that may be encountered, 
so as to offer a proper interpretation of them.
As such, those tools that are specific to each language are defined in the languages' respective 
subpackages. 

A 'language_parameter_version' qualifies a set of tools which are using a consistent set of data 
(e.g.: tags) to qualify textual information existing in a given language.
For instance, for the English language, the 'ontonotes_v5' language parameter version prefix refers 
to tags (syntactic tags, named entity type tags...) that are used by the 'Ontonotes v5' corpus of 
English documents, as well as the models used by the current version of the Stanford Core NLP pipeline.
That means that only document qualified with such tags should be used with the English language 
utilities whose 'language_parameter_version' 's prefix is 'ontonotes_v5'.
For now, only one language parameter version exists per supported language.
"""

"""
Available subpackage(s)
-----------------------
common
    Defines utilities / data that may be shared by several languages 

english
    Defines classes and data specific to the support by the toolbox of the English language

french
    Defines classes and data specific to the support by the toolbox of the French language
"""

from cortex.utils.memoize import Memoized

from cortex.utils import CoRTeXException
from .english import get_english_object
from .french import get_french_object

IMPLEMENTED_LANGUAGES = {"en": ("english", get_english_object), 
                         "fr": ("french", get_french_object),
                         }
SUPPORTED_LANGUAGES = tuple(sorted(IMPLEMENTED_LANGUAGES.keys()))

class LanguageNotSupportedError(CoRTeXException):
    
    def __init__(self, language, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.language = language
        self.supported_languages = SUPPORTED_LANGUAGES

def check_language(language):
    """ Checks that the input string corresponds to the ident of a language that is supported by the 
    toolbox.
    
    Args:
        language: a string, a language ident
    
    Raises:
        LanguageNotSupportedError: if the input string does not correspond to that of a supported 
            language
    """
    if language not in IMPLEMENTED_LANGUAGES:
        message = "The input 'language' value '{}' is not supported; supported values are: {}."
        message = message.format(language, SUPPORTED_LANGUAGES)
        raise LanguageNotSupportedError(language, message)

@Memoized
def create_get_language_dependent_object_fct(language):
    language_object = IMPLEMENTED_LANGUAGES.get(language)
    # Check input
    check_language(language)
    # Create function
    _, get_object_type_fct = language_object #full_language
    get_language_dependent_object = Memoized(get_object_type_fct)
    return get_language_dependent_object

@Memoized
def get_languages_object(language, object_type):
    language_object = IMPLEMENTED_LANGUAGES.get(language)
    # Check input
    check_language(language)
    # Return object
    _, get_object_type_fct = language_object #full_language
    return get_object_type_fct(object_type)
