# -*- coding: utf-8 -*-

"""
Defines a class used to detect quotations in a document written in English.

Use predicate argument annotation, such as the one that is used to qualify the documents of the 
Ontonotes v5 corpus.
"""

__all__ = ["EnglishQuotationDetector",
           ]

from collections import OrderedDict

from .token_features import ENGLISH_TOKEN_FEATURES
from cortex.api.document_buffer import DocumentBuffer
TOKEN_FEATURES = ENGLISH_TOKEN_FEATURES

class EnglishQuotationDetector(object):
    """ A class defining a tool used to find the quotations of a document.
    
    Uses the POS tags and predicate arguments tags data values used to annotate the documets of the 
    'Ontonotes v5' corpus.
    
    Attributes:
        POS_tags_module: the python module defining the constituency tree tags used to characterize 
            the constituency tree of the document which the mentions originate from (the 
            "modified_CC_treebank_constituent_tags" module)
        
        pred_args_tags: the python module defining the predicate arguments tags used to characterize 
            the sentences of the document (the "ontonotes_v5_predicate_arguments_tags" module)
    """
    
    _QUOTATION_OPENING_VERBS_LEMMA = set(("say",))
    
    def __init__(self):
        from .parameters import ontonotes_v5_constituent_tags as POS_tags_module
        from .parameters import ontonotes_v5_predicate_arguments_tags as predicate_arguments_tags_module
        self.POS_tags_module = POS_tags_module
        self.pred_args_tags = predicate_arguments_tags_module
        self._document_buffer = DocumentBuffer()
    
    def detect_quotations(self, document):
        """ Parses the raw text of a Document instance in order to define the collection of potential 
        Quotation instances associated to it, and then sets the result in the Document instance.
        
        Assumes Tokens have data about their POS tags and lemma, and that they have been synchronized 
        with Sentences.
        """
        POSSIBLE_SENTENCE_POSS = set((self.POS_tags_module.SIMPLE_DECLARATIVE_CLAUSE_TAG, self.POS_tags_module.FRAGMENT_TAG)) # NOT SBAR
        # Find quote extents in raw text
        quotations_extent2data = OrderedDict()
        start = None
        end = None
        for token in document.tokens:
            # Using the quote POS tags
            if TOKEN_FEATURES.is_start_quote(token):
                start = token.extent[0] # May overwrite a start quote without end quote, but then, if it was not closed, it was not really a quotation, right?
            elif TOKEN_FEATURES.is_end_quote(token):
                if start is not None: # We create a Quotation only if we know one is currently being parsed
                    end = token.extent[1]
                    extent = (start, end)
                    ident = "{}:{}".format(*extent)
                    quotations_extent2data[extent] = {"ident": ident, "extent": extent}
                    start = end = None
            
            # Using sentences under "say" using pred-arg (without quotes symbol in CoNLL)
            if start is None and token.lemma in self._QUOTATION_OPENING_VERBS_LEMMA: # Try to find a quotation corresponding to the "..." in a "someone says ..." sentence,
                # FIXME: what to do about this legacy commented out code?
                '''
                # Find S under V
                #try:
                #    node = token.get_node()
                #    kids = node.parent.parent.kids
                #    for kid in reversed(kids):
                #        if kid.sName == "S": # token_is_verb_according_to_pa quotation
                #            s,e = kid.get_raw_extent()
                #            quote = Quotation(dict(id=str(s)+":"+str(e), document=self, extent=(s,e), text=text[s:e+1]))
                #            quotations.append(quote)
                #            print "#######", text[s:e+1]
                #            break
                #except AttributeError:
                #    pass
                #except IndexError:
                #    pass
                '''
                # Other method
                s, e = token.extent
                sentence = token.sentence
                # Find predicate arguments quotation pattern
                predicate_arguments = sentence.predicate_arguments
                if predicate_arguments is not None:
                    for predicate_argument in predicate_arguments:
                        token_is_verb_according_to_pa = False
                        for s2, e2, pa_type in predicate_argument:
                            if s == s2 and e == e2 and pa_type == self.pred_args_tags.V:
                                token_is_verb_according_to_pa = True
                                break
                        if token_is_verb_according_to_pa:
                            # Find ARG1 of V
                            for s2, e2, pa_type in predicate_argument:
                                if pa_type == self.pred_args_tags.ARG1:
                                    start_node = end_node = None
                                    for tok in sentence.tokens:
                                        s3,e3 = tok.extent
                                        if s2 == s3:
                                            start_node = tok.constituency_tree_node
                                        if e2 == e3:
                                            end_node = tok.constituency_tree_node
                                            break
                                    # Verify ARG1 is S or FRAG
                                    if start_node is not None and end_node is not None:
                                        ancestor = start_node.get_youngest_common_ancestor(end_node)
                                        if ancestor.syntactic_tag in POSSIBLE_SENTENCE_POSS:
                                            extent = (s2,e2)
                                            ident = "{}:{}".format(*extent)
                                            quotations_extent2data[extent] = {"ident": ident, "extent": extent}
                                            break
                # TODO: (legacy) skip tokens in quote / distinction of pronouns I/you and others
        
        # Find the extents corresponding to the potential speaker mention, speaker token verb, and interlocutor mention
        if quotations_extent2data:
            quotation_extents = tuple(sorted(quotations_extent2data.keys()))
            def _extent_to_potential_encompassing_quotation_data(extent):
                s, e = extent
                for quotation_extent in quotation_extents:
                    qs, qe = quotation_extent
                    if qs <= s and e <= qe:
                        return quotations_extent2data[quotation_extent]
                return None
            def _does_overlap_with_a_quotation(extent):
                s, e = extent
                for quotation_extent in quotation_extents:
                    qs, qe = quotation_extent
                    if not (e < qs or qe < s):
                        return True
                return False
            raw_text = document.raw_text
            
            # WARNING: (legacy) may be approximate
            for sentence in document.sentences:
                for predicate_argument in sentence.predicate_arguments:
                    v_extent = arg2_extent = arg1_extent = arg0_extent = None
                    for (start, end, pa_type) in predicate_argument:
                        extent = (start, end)
                        if pa_type == self.pred_args_tags.V:
                            v_extent = extent
                        elif pa_type == self.pred_args_tags.ARG2:
                            arg2_extent = extent
                            if raw_text[start:end+1].lower().startswith("to "):
                                arg2_extent = (start+3, end)
                        elif pa_type == self.pred_args_tags.ARG1:
                            arg1_extent = extent
                        elif pa_type == self.pred_args_tags.ARG0:
                            arg0_extent = extent
                    if v_extent is not None and arg1_extent is not None and arg0_extent is not None: # found a (V,arg0,arg1,...)
                        potential_quotation_data = _extent_to_potential_encompassing_quotation_data(arg1_extent)
                        is_v_out_of_a_quotation = _extent_to_potential_encompassing_quotation_data(v_extent) is None
                        is_arg0_out_of_a_quotation = _extent_to_potential_encompassing_quotation_data(arg0_extent) is None
                        is_arg2_not_overlapping_with_a_quotation = True if arg2_extent is None else (not _does_overlap_with_a_quotation(arg2_extent))
                        if is_v_out_of_a_quotation and is_arg0_out_of_a_quotation and is_arg2_not_overlapping_with_a_quotation and potential_quotation_data is not None: # v introduces a quotation
                            potential_quotation_data["speaker_mention_extent"] = arg0_extent
                            potential_quotation_data["speaker_verb_token_extent"] = v_extent
                            potential_quotation_data["interlocutor_mention_extent"] = arg2_extent
        
        # If quotation data was found, add it to the Document
        if quotations_extent2data:
            document_buffer = self._document_buffer
            document_buffer.initialize_anew(document)
            document_buffer.add_quotations(quotations_extent2data)
            strict = False
            enforce_coreference_partition_consistency = False
            document_buffer.flush(strict=strict, 
                                  enforce_coreference_partition_consistency=enforce_coreference_partition_consistency)
