# -*- coding: utf-8 -*-

"""
Provides utilities needed by the other modules of this package.
"""

__all__ = ["create_get_resource_file_path_fct", 
           "create_get_iterator_over_resource_file_fct",
           ]

import os

from cortex.parameters import ENCODING

#########
# Utils #
#########
def create_get_resource_file_path_fct(resource_type, resource_name2file_name, resource_folder_path):
    """ Creates a function used to get the path to a specific resource (such as a file) among a 
    specific collection of possible resources located in the same root folder.
    
    Args:
        resource_type: string, a name to be able to identify which kind of resource the get 
            function is about, in case that an error occurs
        resource_name2file_name: a "resource name => file name" map, where the different 
            "resource name" key values will be the acceptable inputs to the function to create
        resource_folder_path: string, the path to the root folder where all resources of the input 
            resource type are supposed to be located

    Returns:
        a callable, a function that takes a "resource name" as an input, and outputs the path to 
        the local file representing the resource
    """
    
    fct_docstring = """ Get path to the file representing the resource associated to the input resource name, 
        for resources of type '{}'.
        
        Args:
            resource_name: string, name of the resource whose path is to be to fetched

        Returns:
            string, path to the file corresponding to the resource
        """.format(resource_type)
    fct_name = "get_{}_file_path".format(resource_type)
    def _get_resource_file_path(resource_name):
        
        if resource_name not in resource_name2file_name:
            message = "Incorrect {} input '{}', possible values are: {}."
            message = message.format(resource_type, resource_name, list(resource_name2file_name.keys()))
            raise ValueError(message)
        return os.path.join(resource_folder_path, resource_name2file_name[resource_name])
    _get_resource_file_path.__doc__ = fct_docstring
    _get_resource_file_path.__name__ = fct_name
    
    return _get_resource_file_path

def create_get_iterator_over_resource_file_fct(resource_type, resource_name2file_name, resource_folder_path):
    """ Creates a function used to create an iterator over the lines of a file representing a specific 
    resource, among a specific collection of possible resources located in the same root folder.
    
    Args:
        resource_type: string, a name to be able to identify which kind of resource the get 
            function is about, in case that an error occurs
        resource_name2file_name: a "resource name => file name" map, where the different 
            "resource name" key values will be the acceptable inputs to the function to create
        resource_folder_path: string, the path to the root folder where all resources of the input 
            resource type are supposed to be located

    Returns:
        a callable, a function that takes a "resource name" as an input, and outputs an iterator 
        over the lines of the file representing the resource
    """
    
    fct_docstring = """ Get iterator over the lines of the file representing the resource associated 
        to the input resource name, for resources of type '{}'.
        
        Args:
            resource_name: a string, name of the resource whose path is to be to fetched

        Returns:
            an iterator over strings, the lines of the file corresponding to the resource
        """.format(resource_type)
    fct_name = "get_iterator_over_{}_resource".format(resource_type)
    func = create_get_resource_file_path_fct(resource_type, resource_name2file_name, resource_folder_path)
    def _get_iterator_over_resource(resource_name):
        """ Creates a generator over lines of a file corresponding to the input resource name.
        
        Args:
            resource_name: string, a possible resource name
        
        Yields:
            string, a line of the file corresponding to the input resource name, if correct
        """
        file_path = func(resource_name)
        with open(file_path, "r", encoding=ENCODING) as f:
            for line in f:
                yield line#.rstrip("\n")
    _get_iterator_over_resource.__doc__ = fct_docstring
    _get_iterator_over_resource.__name__ = fct_name
    
    return _get_iterator_over_resource
