# -*- coding: utf-8 -*-

"""
Defines data specific to the English language, and rather constant (e.g.: collection of names, etc.)
"""

"""
Available submodule(s)
-----------------------
bart_names
    Defines utilities to parse 'bart-names' data

bergsma_gender_reader
    Defines utilities to parse 'bergsma-gender' data

coref_dictionary_reader
    Defines utilities to parse 'coref-dictionary' data

gazetteers
    Defines utilities to parse 'gazetteers' data

lexicon
    Defines relevant set of English words, and association between English words

names_corpus_reader
    Defines utilities to parse 'names-corpus' data

utils
    Defines utilities needed and shared by the other modules
"""

from .gazetteers import get_gazetteers_file_path, get_iterator_over_gazetteers_file
from .names_corpus_reader import get_names_corpus_file_path, get_iterator_over_names_corpus_file
from .bart_names import get_bart_names_file_path, get_iterator_over_bart_names_file
from .bergsma_gender_reader import get_bergsma_gender_file_path, get_iterator_over_bergsma_gender_file
from .lexicon import get_lexicon_data_file_path, get_iterator_over_lexicon_data_file
from .coref_dictionary_reader import get_coref_dictionary_file_path, get_iterator_over_coref_dictionary_file