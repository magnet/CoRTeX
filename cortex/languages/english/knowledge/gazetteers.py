# -*- coding: utf-8 -*-

"""
Defines utilities to parse and use 'gazetteers' data
"""

__all__ = ["get_gazetteers_file_path", 
           "get_iterator_over_gazetteers_file",
           ]

import os

from .utils import create_get_resource_file_path_fct, create_get_iterator_over_resource_file_fct

GAZETTEERS_FOLDER_PATH = os.path.join(os.path.dirname(__file__), "gazetteers")
GAZETTEERS_FILE_NAMES = {"caprovinces": "caprovinces.txt", "countries": "countries.txt", 
                         "isocountries": "isocountries.txt", "locations": "locations.txt", 
                         "mexstates": "mexstates.txt", "nationalities": "nationalities.txt", 
                         "uscities": "uscities.txt", "usstateabbrev": "usstateabbrev.txt", 
                         "usstates": "usstates.txt"}
get_gazetteers_file_path = create_get_resource_file_path_fct("gazetteers", GAZETTEERS_FILE_NAMES, GAZETTEERS_FOLDER_PATH)
get_iterator_over_gazetteers_file = create_get_iterator_over_resource_file_fct("gazetteers", GAZETTEERS_FILE_NAMES, GAZETTEERS_FOLDER_PATH)
