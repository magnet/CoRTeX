# -*- coding: utf-8 -*-

"""
Defines relevant sets of English words, and association between English words, as well as compiled 
regexes to check whether or not a string contains a certain type of word, such as personal pronouns 
for instance.
"""

"""
Here, EXPANDED_PRONOUN means to include also other kind of words, such as demonstrative determiners, 
that can also act as a reference to a grammatical person. 
"""

__all__ = []

import os
import re

from cortex.utils.memoize import SimpleMemoized
from cortex.utils.timer import timer
from .utils import create_get_resource_file_path_fct, create_get_iterator_over_resource_file_fct

LEXICON_DATA_FOLDER_PATH = os.path.join(os.path.dirname(__file__), "lexicon_data")
LEXICON_DATA_FILE_NAMES = {'acronyms': 'acronyms.txt', 'corp_patterns': 'corp_patterns.txt', 
                           'country_to_demonym': 'country_to_demonym.txt', 
                           'female_names': 'female_names.txt', 'male_names': 'male_names.txt', 
                           'country': 'country.txt', 'verb_to_noun': 'verb_to_noun.txt', 
                           'country_to_adjective': 'country_to_adjective.txt'}
get_lexicon_data_file_path = create_get_resource_file_path_fct("lexicon_data", LEXICON_DATA_FILE_NAMES, LEXICON_DATA_FOLDER_PATH)
get_iterator_over_lexicon_data_file = create_get_iterator_over_resource_file_fct("lexicon_data", LEXICON_DATA_FILE_NAMES, LEXICON_DATA_FOLDER_PATH)

@SimpleMemoized
@timer(print, "loading English lexicon full data")
def loading_resource():
    """ Loads up the resources defined in the files corresponding to the 'lexicon_data' resources.
    
    The result is cached in memory, such that further call to the function will not imply another 
    parsing of the local resources.
    
    Returns:
        a (CORP_PATTERNS, COUNTRY, COUNTRY_TO_ADJECTIVE, COUNTRY_TO_DEMONYM, VERB_TO_NOUN, 
        MALE_NAMES, FEMALE_NAMES, ACRONYMS) tuple, where:
            - 'CORP_PATTERNS' is a collection of strings
            - 'COUNTRY' is a set of strings representing country names
            - 'COUNTRY_TO_ADJECTIVE' is a "country name => collection of adjective qualifying things or 
              inhabitant related to that country" map
            - 'COUNTRY_TO_DEMONYM' is a "country name => demonym" map
            - 'VERB_TO_NOUN' is a "verb => result of the nominalization of the verb"
            - 'MALE_NAMES' is a set of strings representing known names given to male humans
            - 'FEMALE_NAMES' is a set of strings representing known names given to female humans
            - 'ACRONYMS' is a collection of pairs of string, where the first string represents an acronym 
              of the second string 
    """
    CORP_PATTERNS = tuple(line.strip() for line in get_iterator_over_lexicon_data_file("corp_patterns"))
    COUNTRY = set(line.strip() for line in get_iterator_over_lexicon_data_file("country"))
    COUNTRY_TO_ADJECTIVE = dict(tuple((n, tuple(l.split(";")))) for n, l in \
                                (line.strip().split(":") for line in get_iterator_over_lexicon_data_file("country_to_adjective"))
                                )
    COUNTRY_TO_DEMONYM = dict(tuple((n, tuple(l.split(";")))) for n, l in \
                                (line.strip().split(":") for line in get_iterator_over_lexicon_data_file("country_to_demonym"))
                              )
    VERB_TO_NOUN = dict(tuple((n, tuple(l.split(";")))) for n, l in \
                        (line.strip().split(":") for line in get_iterator_over_lexicon_data_file("verb_to_noun"))
                        )
    ## Male first names
    MALE_NAMES = set(line.strip() for line in get_iterator_over_lexicon_data_file("male_names"))
    ### Female first names
    FEMALE_NAMES = set(line.strip() for line in get_iterator_over_lexicon_data_file("female_names"))
    # Acronyms
    ACRONYMS = tuple(tuple(line.strip().split(":")) for line in get_iterator_over_lexicon_data_file("acronyms"))
    return CORP_PATTERNS, COUNTRY, COUNTRY_TO_ADJECTIVE, COUNTRY_TO_DEMONYM, VERB_TO_NOUN, MALE_NAMES, FEMALE_NAMES, ACRONYMS

# Load the data
CORP_PATTERNS, COUNTRY, COUNTRY_TO_ADJECTIVE, COUNTRY_TO_DEMONYM, VERB_TO_NOUN, MALE_NAMES, FEMALE_NAMES, ACRONYMS = loading_resource()


######## REGEXES ########
_create_regex = lambda iterable: re.compile("^(" + "|".join(iterable) + ")", re.IGNORECASE)

## Pronoun regular expressions
dict_ = {}
EXPAND_PRO__PATTERNS = ("i", "me", "my", "myself", "you", "your", "yours", "yourself", "he", "she", "it", "him", 
                        "her", "his", "hers", "its", "himself", "herself", "itself", "we", "us", "our", "ours", 
                        "ourselves", "yourselves", "they", "their", "theirs", "them", "themselves", "this", "that", 
                        "these", "those")
dict_["EXPAND_PRO_"] = EXPAND_PRO__PATTERNS
EXPAND_PERS_PRO__PATTERNS = ("i", "me", "my", "myself", "you", "your", "yours", "yourself", "he", "she", "it", 
                            "him", "her", "his", "hers", "its", "himself", "herself", "itself", "we", "us", 
                            "our", "ours", "ourselves", "yourselves", "they", "their", "theirs", "them", "themselves")
dict_["EXPAND_PERS_PRO_"] = EXPAND_PERS_PRO__PATTERNS
EXPAND_FIRST_PERS_PRO__PATTERNS = ("i", "me", "my", "mine", "myself", "we", "us", "our", "ours", "ourselves")
dict_["EXPAND_FIRST_PERS_PRO_"] = EXPAND_FIRST_PERS_PRO__PATTERNS 
EXPAND_SECOND_PERS_PRO__PATTERNS = ("you", "your", "yours", "yourself", "yourselves")
dict_["EXPAND_SECOND_PERS_PRO_"] = EXPAND_SECOND_PERS_PRO__PATTERNS
THIRD_PERS_PRO_PATTERNS = ("he", "she", "it", "him", "her", "his", "hers", "its", "himself", "herself", "itself", 
                            "they", "them", "their", "theirs", "themselves")
dict_["EXPAND_THIRD_PERS_PRO_"] = THIRD_PERS_PRO_PATTERNS
EXPAND_SPEECH_PRO__PATTERNS = ("i", "me", "my", "", "myself", "we", "us", "our", "ours", "ourselves", "you", 
                                "your", "yours", "yourself", "yourselves") # pb with US
dict_["EXPAND_SPEECH_PRO_"] = EXPAND_SPEECH_PRO__PATTERNS
EXPAND_THIRD_SG_PRO__PATTERNS = ("he", "she", "it", "him", "her", "his", "hers", "its", "himself", "herself", "itself")
dict_["EXPAND_THIRD_SG_PRO_"] = EXPAND_THIRD_SG_PRO__PATTERNS 
EXPAND_THIRD_PL_PRO__PATTERNS = ("they", "their", "theirs", "them", "themselves")
dict_["EXPAND_THIRD_PL_PRO_"] = EXPAND_THIRD_PL_PRO__PATTERNS 
EXPAND_MASC_PRO__PATTERNS = ("he", "him", "his", "himself")
dict_["EXPAND_MASC_PRO_"] = EXPAND_MASC_PRO__PATTERNS
EXPAND_FEM_PRO__PATTERNS = ("she", "her", "hers", "herself")
dict_["EXPAND_FEM_PRO_"] = EXPAND_FEM_PRO__PATTERNS
EXPAND_NEUT_PRO__PATTERNS = ("it", "its", "itself", "this", "that", "these", "those")
dict_["EXPAND_NEUT_PRO_"] = EXPAND_NEUT_PRO__PATTERNS
EXPAND_SG_PRO__PATTERNS = ("i", "me", "my", "mine", "myself", "you", "your", "yours", "yourself", "he", "she", "it", 
                            "him", "her", "his", "hers", "its", "himself", "herself", "itself", "this", "that")
dict_["EXPAND_SG_PRO_"] = EXPAND_SG_PRO__PATTERNS
EXPAND_PL_PRO__PATTERNS = ("we", "us", "our", "ours", "they", "their", "theirs", "them", "themselves", 
                            "ourselves", "yourselves", "these", "those")
dict_["EXPAND_PL_PRO_"] = EXPAND_PL_PRO__PATTERNS 
POSS_DET_PATTERNS = ("my", "your", "his", "her", "its", "our", "their")
dict_["POSS_DET"] = POSS_DET_PATTERNS
DEM_PRO_PATTERNS = ("this", "that", "these", "those")
dict_["DEM_PRO"] = DEM_PRO_PATTERNS
REFL_PRO_PATTERNS = ("myself", "yourself", "himself", "herself", "itself", "ourselves", "yourselves", "themselves")
dict_["REFL_PRO"] = REFL_PRO_PATTERNS


## Types of determiners
SG_DET_PATTERNS = ("a", "an", "this", "that")
dict_["SG_DET"] = SG_DET_PATTERNS
PL_DET_PATTERNS = ("these", "those", "(a )?few", "several", "many", "most")
dict_["PL_DET"] = PL_DET_PATTERNS
DEF_DET_PATTERNS = ("the",)
dict_["DEF_DET"] = DEF_DET_PATTERNS
INDEF_DET_PATTERNS = ("a", "some", "(a )?few", "several", "many")
dict_["INDEF_DET"] = INDEF_DET_PATTERNS


# Person title regexes
# TITLE = re.compile("^[A-Z][a-z]+\\.$|^[A-Z][b-df-hj-np-tv-xz]+",re.IGNORECASE)
MALE_TITLE_PATTERNS = ("admiral", "archbishop", "baron", "brother", "chevalier", "count", "darth", 
                    "doctor", "dr", "dr.", "eminence", "excellency", "father", "father", "generalissimo", 
                    "her", "herr", "his", "jonkheer", "king", "lord", "marquis", "marshal", "master", 
                    "mister", "mon", "mon.", "monsieur", "mr", "mr.", "pastor", "prince", "rabbi", 
                    "raja", "rev.", "reverend", "senor", "señor", "sheik", "sheikh", "sir", "sir", 
                    "sr", "sr.", "viscount")
dict_["MALE_TITLE"] = MALE_TITLE_PATTERNS
FEMALE_TITLE_PATTERNS = ("baroness", "countess", "dame", "lady", "madame", "marchioness", "marquise", 
                        "mata", "miss", "missus", "mrs", "ms", "princess", "queen", "rani", "senorita", 
                        "señora", "señorita", "sister", "sra", "sra.", "srta", "srta.", "viscountess")
dict_["FEMALE_TITLE"] = FEMALE_TITLE_PATTERNS
UNI_TITLE_PATTERNS = ("capt.", "captain", "coach", "demiurge", "gen", "gen.", "general", "hon", 
                    "hon.", "honorable", "maj", "maj.", "major", "pres", "pres.", "president", "prof", 
                    "prof.", "professor", "rep", "rep.", "representative", "rev", "sen", "sen.", 
                    "senator", "sergeant", "sgt", "sgt.", "venerable", "vice-president")
dict_["UNI_TITLE"] = UNI_TITLE_PATTERNS


# TODO: person suffix: I, II, III, PhD, M.D., ...

# Location regexes: e.g. Austin, TX
# TODO

# Company regexes
# CORP_REGEX = re.compile("[a-z]\\.$|^[A-Z][b-df-hj-np-tv-xz]+$|^Co(rp)?$",re.IGNORECASE)
dict_["CORP"] = CORP_PATTERNS


# Finally, create the regexes
dict_2 = dict((name, _create_regex(patterns)) for name, patterns in dict_.items())
del dict_

EXPAND_PRO_REGEX = dict_2["EXPAND_PRO_"]
EXPAND_PERS_PRO_REGEX = dict_2["EXPAND_PERS_PRO_"]
EXPAND_FIRST_PERS_PRO_REGEX = dict_2["EXPAND_FIRST_PERS_PRO_"]
EXPAND_SECOND_PERS_PRO_REGEX = dict_2["EXPAND_SECOND_PERS_PRO_"]
EXPAND_THIRD_PERS_PRO_REGEX = dict_2["EXPAND_THIRD_PERS_PRO_"]
EXPAND_SPEECH_PRO_REGEX = dict_2["EXPAND_SPEECH_PRO_"]
EXPAND_THIRD_SG_PRO_REGEX = dict_2["EXPAND_THIRD_SG_PRO_"]
EXPAND_THIRD_PL_PRO_REGEX = dict_2["EXPAND_THIRD_PL_PRO_"]
EXPAND_MASC_PRO_REGEX = dict_2["EXPAND_MASC_PRO_"]
EXPAND_FEM_PRO_REGEX = dict_2["EXPAND_FEM_PRO_"]
EXPAND_NEUT_PRO_REGEX = dict_2["EXPAND_NEUT_PRO_"]
EXPAND_SG_PRO_REGEX = dict_2["EXPAND_SG_PRO_"]
EXPAND_PL_PRO_REGEX = dict_2["EXPAND_PL_PRO_"]
POSS_PRO_REGEX = dict_2["POSS_DET"]
DEM_PRO_REGEX = dict_2["DEM_PRO"]
REFL_PRO_REGEX = dict_2["REFL_PRO"]
SG_DET_REGEX = dict_2["SG_DET"]
PL_DET_REGEX = dict_2["PL_DET"]
DEF_DET_REGEX = dict_2["DEF_DET"]
INDEF_DET_REGEX = dict_2["INDEF_DET"]
MALE_TITLE_REGEX = dict_2["MALE_TITLE"]
FEMALE_TITLE_REGEX = dict_2["FEMALE_TITLE"]
UNI_TITLE_REGEX = dict_2["UNI_TITLE"]
CORP_REGEX = dict_2["CORP"]

del dict_2




######## Words enumerations ########
######## PRONOUNS ########
# PRONOUNS: GENDER
EXPANDED_FEMALE_PRONOUNS = set(["her", "hers", "herself", "she"])
EXPANDED_MALE_PRONOUNS = set(["he", "him", "himself", "his"])
EXPANDED_NEUTRAL_PRONOUNS = set(["here", "it", "its", "itself", "there", "which", "where"])



# PRONOUNS: NUMBER
EXPANDED_SINGULAR_PRONOUN_MENTIONS = set(["he", "her", "hers", "herself", "him", "himself", "his", "i", "it", "its", 
                                          "itself", "me", "mine", "my", "myself", "one", "one's", "oneself", "she", 
                                           "that", "this", "yourself"])
EXPANDED_PLURAL_PRONOUN_MENTIONS = set(["our", "ours", "ourself", "ourselves", "their", "theirs", "them", "themself", 
                                        "themselves", "these", "they", "those", "us", "we", "yourselves"])

#EXPANDED_
# PRONOUNS: PERSON 1/2/3
EXPANDED_SINGULAR_FIRST_PERSON_PERSONAL_PRONOUN_MENTIONS = set(("i", "me", "my", "mine", "myself"))
EXPANDED_SINGULAR_FIRST_PERSON_PERSONAL_PRONOUN_MENTIONS = set(("we", "us", "our", "ours", "ourselves"))
EXPANDED_SINGULAR_FIRST_PERSON_PERSONAL_PRONOUN_MENTIONS = set(("you", "your", "yours", "yourself"))
EXPANDED_FIRST_PERSON_PERSONAL_PRONOUN_MENTIONS = set(["i", "me", "mine", "my", "myself", "our", "ours", "ourself", "ourselves", "us", "we"])
EXPANDED_SECOND_PERSON_PERSONAL_PRONOUN_MENTIONS = set(["you", "your", "yours", "yourself", "yourselves"])
EXPANDED_THIRD_PERSON_PERSONAL_PRONOUN_MENTIONS = set(["'em", "he", "her", "hers", "herself", "him", "himself", "his", "it",
                                                         "its", "itself", "one", "one's", "oneself", "she", "their", "theirs", 
                                                         "them", "themself", "themselves", "they",
                                                        ])
EXPANDED_PERSONAL_PRONOUN_MENTIONS = EXPANDED_FIRST_PERSON_PERSONAL_PRONOUN_MENTIONS\
                                        .union(EXPANDED_SECOND_PERSON_PERSONAL_PRONOUN_MENTIONS)\
                                        .union(EXPANDED_THIRD_PERSON_PERSONAL_PRONOUN_MENTIONS)



# PRONOUNS: POSSESSIVE/REFLEXIVE/RELATIVE/DEMONSTRATIVE/INDEFINITE/WH--/MISC
POSSESSIVE_DETERMINERS = set(["her", "his", "its", "my", "our", "their", "whose", "your"])
EXPANDED_POSSESSIVE_PRONOUNS = set(["her", "hers", "his", "its", "mine", "my", "our", "ours", 
                                    "their", "theirs", "your", "yours"])
EXPANDED_POSSESSIVE_PRONOUN_MENTIONS = set(["her", "hers", "his", "its", "mine", "my", "our", "ours", 
                                    "their", "theirs", "your", "yours"])
REFLEXIVE_PRONOUN_MENTIONS = set(["herself", "himself", "itself", "myself", "oneself", "ourselves", 
                                "themselves", "yourself", "yourselves"])
RELATIVE_PRONOUN_MENTIONS = set(["that", "when", "where", "which", "who", "whom", "whose"])
#RELATIVE_PRONOUN_MENTIONS = set(["that", "which", "who", "whom", "whoever", "whomever", "whose", "whichever"])
INDEFINITE_PRONOUN_MENTIONS = set(["all", "another", "any", "anybody", "anyone", "anything", "both", "each", 
                        "either", "enough", "everybody", "everyone", "everything", "few", "fewer", 
                        "less", "little", "many", "more", "most", "much", "neither", "nobody", "none", 
                        "no one", "nothing", "one", "other", "others", "plenty", "several", "some", 
                        "somebody", "someone", "something", "such"])
# DEMONSTRATIVE PRONOUNS
DEMONSTRATIVE_PRONOUN_MENTIONS = set(["that","these","this","those"])
# WH-- PRONOUNS
WH_PRONOUN_MENTIONS = set(["when", "where", "which", "who", "whom", "whose"])
MISC_PRONOUN_MENTIONS = set(["it", "its", "itself", "they", "where"])



# PRONOUNS: ANIMACY
EXPANDED_ANIMATE_PRONOUN_MENTIONS = set(["'em", "he", "her", "hers", "herself", "him", "himself", "his", "i", "me", 
                                         "mine", "my", "myself", "one", "one's", "oneself", "our", "ours", "ourself", 
                                         "ourselves", "she", "their", "theirs", "them", "themself", "themselves", 
                                         "themselves", "they", "us", "we", "who", "whom", "whose", "you", "your", "yours", 
                                         "yourself", "yourselves",
                                        ])
EXPANDED_INANIMATE_PRONOUN_MENTIONS = set(["it", "its", "itself", "when", "where"])



# PRONOUNS: NE
EXPANDED_MONEY_PERCENT_NUMBER_PRONOUN_MENTIONS = set(["it", "its"])
DATE_TIME_PRONOUN_MENTIONS = set(["when"])
EXPANDED_ORGANIZATION_PRONOUN_MENTIONS = set(["it", "its", "their", "them", "they", "which"])
EXPANDED_GPE_PRONOUN_MENTIONS = set(["it", "its", "itself", "they", "where"])
EXPANDED_LOCATION_PRONOUN_MENTIONS = set(["here", "it", "its", "there", "where"])
EXPANDED_FACILITY_VEHICLE_WEAPON_PRONOUN_MENTIONS = set(["it", "its", "itself", "they", "where"])



######## QUANTIFIERS / PARTITIVES ########
QUANTIFIERS = set(["all", "any", "anything", "both", "each", "enough", "every", "everything", "few", 
                "a few", "fewer", "little", "a little", "less", "many", "more", "none", "not", 
                "nothing", "some", "a lot of"])
PARTITIVES = set(["all of", "amount of", "billion of", "billions of", "bit of", "bits of", "bunch of", 
                "dozens of", "group of", "groups of", "handful of", "handfuls of", "hundred of", 
                "hundreds of", "kind of", "kinds of", "lot of", "lots of", "million of", "millions of", 
                "number of", "numbers of", "piece of", "pieces of", "pinch of", "pounds of", 
                "quantities of", "slice of", "some of", "sort of", "sorts of", "stroke of", "tens of", 
                "thousand of", "thousands of", "total of"])



######## LOCATION MODIFIERS ########
LOCATION_MODIFIERS = set(["east", "eastern", "lower", "north", "northern", "south", "southern", "upper", "west", "western"])


######## STOP WORDS/ NON WORDS ########
NON_WORDS = set(["mm", "hmm", "ahem", "um"])


STOP_WORDS = set(["a", "an", "the", "of", "at", "on", "upon", "in", "to", "from", "out", "as", "so", 
                "such", "or", "and", "those", "this", "these", "that", "for", ",", "is", "was", "am", 
                "are", "'s", "been", "were"])


STOP_WORDS2 = set([",", "a", "am", "an", "and", "are", "as", "at", "been", "corp.", "dr.", "'em", 
                "for", "from", "he", "her", "hers", "herself", "him", "himself", "his", "i", "in", 
                "inc.", "is", "it", "its", "itself", "ltd.", "me", "mine", "miss", "mr.", "mrs.", 
                "ms.", "my", "myself", "of", "on", "one", "one's", "oneself", "or", "our", "ours", 
                "ourself", "ourselves", "out", "'s", "she", "so", "such", "that", "the", "their", 
                "theirs", "them", "themself", "themselves", "these", "the","this", "they", "this", 
                "those", "to", "upon", "us", "was", "we", "were", "when", "where", "which", "who", 
                "whom", "whose", "you", "your", "yours", "yourself", "yourselves"])


## Others
PREPOSITION = set(("abaft", "aboard", "about", "above", "absent", "across", "afore", "after", 
                    "against", "along", "alongside", "amid", "amidst", "among", "amongst", "apropos", 
                    "around", "as", "aside", "astride", "at", "athwart", "atop", "barring", "before", 
                    "behind", "below", "beneath", "beside", "besides", "between", "betwixt", "beyond", 
                    "but", "by", "circa", "concerning", "despite", "down", "during", "except", 
                    "excluding", "failing", "following", "for", "from", "given", "in", "including", 
                    "inside", "into", "lest", "like", "mid", "midst", "minus", "modulo", "near", 
                    "next", "notwithstanding", "of", "off", "on", "onto", "opposite", "out", 
                    "outside", "over", "pace", "past", "per", "plus", "pro", "qua", "regarding", 
                    "round", "sans", "save", "since", "than", "through", "throughout", "till", "times", 
                    "to", "toward", "towards", "under", "underneath", "unlike", "until", "unto", "up",
                    "upon", "versus", "via", "vice", "with", "within", "without", "worth"))



FAMILY = set(("ancestor", "aunt", "babies", "baby", "boy", "bride", "brother", "brother-in-law", 
                "brothers-in-law", "child", "children", "cousin", "dad", "daddy", "daughter", 
                "daughter-in-law", "daughters-in-law", "ex", "ex-husband", "exwife", "ex-wife", 
                "exwives", "ex-wives", "families", "family", "father", "father-in-law", "fiance", 
                "girl", "grandchild", "grandchildren", "granddad", "granddaughter", "grandfather", 
                "grandma", "grandmamma", "grandmother", "grandpa", "grandpapa", "grandparent", "mum", 
                "mummy", "nephew", "niece", "pa", "papa", "pappy", "parent", "parent-in-law", 
                "parents-in-law", "pop", "relative", "sister-in-law", "sisters-in-law", "son", 
                "son-in-law", "sons-in-law", "stepbrother", "stepfather", "stepmother", "stepsister", 
                "uncle", "wife", "wives"))


######## VERBS ########
COPULAR_VERBS = set(["is","are","were", "was","be", "been","become","became","becomes","seem",
                    "seemed","seems","remain","remains","remained"])



