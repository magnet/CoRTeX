========================================================================
README file for the coreferentially annotated test set from Techmeme

Release: 1.0 (03/23/2013)
Created: March 23, 2013
========================================================================

---- OVERVIEW ----

This data set accompanies the NAACL-2013 submission and corresponds to
the test set used in this paper:

Marta Recasens, Matthew Can, and Dan Jurafsky. 2013. Same Referent,
Different Words: Unsupervised Mining of Opaque Coreferent
Mentions. Proceedings of NAACL 2013.

It contains 24 news articles from Techmeme annotated with coreference relations.


---- FORMAT ----

Each file contains one news article delimited with a <TOPIC> and a
<DOC> tag. The <DOC> tag includes the original URL.  Each mention is
marked with an <Entity> tag, whose COREFID attribute indicates the
coreference cluster to which the mention belongs. Thus, two mentions
with the same COREFID are coreferent. E.g.,

<Entity COREFID="5">Yahoo</Entity>

<Entity COREFID="5">its</Entity>

<Entity COREFID="5">the <Entity COREFID="17">Silicon Valley</Entity>
<Entity COREFID="18">Internet</Entity> giant</Entity>

========================================================================
