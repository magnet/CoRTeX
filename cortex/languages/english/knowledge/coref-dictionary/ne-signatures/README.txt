========================================================================
README file for the NAACL-2013 signatures of NEs

Release: 1.0 (03/16/2013)
Created: March 16, 2013
========================================================================

---- OVERVIEW ----

This data set accompanies the NAACL-2013 submission and corresponds to
the list of named-entity (NE) signatures. A signature includes the NEs
that tend to co-occur with a given NE, ranked decreasingly according
to their log-likelihood ratio (LLR). It is presented in this paper:

Marta Recasens, Matthew Can, and Dan Jurafsky. 2013. Same Referent,
Different Words: Unsupervised Mining of Opaque Coreferent
Mentions. Proceedings of NAACL 2013.

Please cite this paper if you use this resource in your work.


---- DESCRIPTION ----

To construct NE signatures, we first compute the log-likelihood ratio
(LLR) statistic between NEs in our Techmeme corpus. Then, the
signature for a NE, w, is the list of k other NEs that have the
highest LLR with w. The LLR between two NEs, w1 and w2, is
-2*log(L(H1) / L(H2)), where H1 is the hypothesis that

P(w1 in sent | w2 in sent) = P(w1 in sent | w2 not in sent),

H2 is the hypothesis that

P(w1 in sent | w2 in sent) != P(w1 in sent | w2 not in sent),

and L() is the likelihood function. We assume a binomial distribution
for the likelihood.

In more detail, the computation is as follows:
c1 is the number of sentences containing w1
c2 is the number of sentences containing w2
c12 is the number of sentences containing w1 and w2
N is the number of sentences

p = c2 / N
p1 = c12 / c1
p2 = (c2 - c12) / (N - c1)

LLR = -2 * [ ((c12)*log(p) + (c1-c12)*log(1-p)) + ((c2-c12)*log(p) +
((n-c1)-(c2-c12))*log(1-p)) - ((c12)*log(p1) + (c1-c12)*log(1-p1)) -
((c2-c12)*log(p2) + ((n-c1)-(c2-c12))*log(1-p2)) ]


---- FORMAT ----

Each line in the NE-signatures file contains the signature for one NE
in a tab-separated format. The first column lists the target NE, and 
is followed by pairs of NE and LLR, corresponding to the LLR of that
NE with the target NE. More schematically:

target NE \t 
NE1 \t LLR between NE1 and target NE \t
NE2 \t LLR between NE2 and target NE \t
etc.


---- STATISTICS ----

11,968 NE signatures.


--- LICENSE ----

This resource is made available under the Public Domain Dedication and
License (PDDL) v1.0 whose full text can be found at:
http://www.opendatacommons.org/licenses/pddl/1.0/


---- CONTENTS ----

   * ne.signatures.txt
     List of NE signatures in a tab-separated format.

   * LICENSE.pdf
     The PDDL license v1.0 under which this annotation is made
     available.

   * README.txt
     This file.

========================================================================
