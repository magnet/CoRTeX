========================================================================
README file for the NAACL-2013 tech dictionary of opaque coreferent
mentions

Release: 1.0 (12/07/2012)
Created: December 7, 2012
========================================================================

---- OVERVIEW ----

This data set accompanies the NAACL-2013 submission and corresponds to
the dictionary of opaque coreferent mentions (typical of the technical
domain) presented in this paper:

Marta Recasens, Matthew Can, and Dan Jurafsky. 2013. Same Referent,
Different Words: Unsupervised Mining of Opaque Coreferent
Mentions. Proceedings of NAACL 2013.

Please cite this paper if you use this resource in your work.


---- FORMAT ----

We release four versions of the dictionary with different granularity
levels: Dict1 contains only the heads of the two mentions (e.g.,
"phone" and "device"). Dict2 contains the heads and one premodifier,
if it exists (e.g., "mobile phone" and "<organization> device").
Dict3 contains the heads and up to two premodifiers (e.g., "prototype
mobile phone" and "<organization> device"). Dict4 uses the full
mentions, including any postmodifiers (e.g., "prototype mobile phone
from <organization>" and "<organization> device").

Each dictionary is released as a tab-separated-value (TSV) file. Each
line contains a mention pair, followed by its frequency. Dict1 also
contains the normalized PMI score.  We include both directions for
each mention pair (i.e., not only mentionX--mentionY, but also
mentionY--mentionX). Pairs are sorted by their decreasing frequency.


---- STATISTICS ----

Dict1: 143,619 mention pair types.
Dict2: 206,117 mention pair types.
Dict3: 210,401 mention pair types.
Dict4: 221,419 mention pair types.


---- LICENSE ----

This resource is made available under the Public Domain Dedication and
License (PDDL) v1.0 whose full text can be found at:
http://www.opendatacommons.org/licenses/pddl/1.0/


---- CONTENTS ----

   * coref.dict1.tsv
     Pairs of mention heads, their frequency and their NPMI.

   * coref.dict2.tsv
     Pairs of premodifier+head mentions and their frequency.

   * coref.dict3.tsv    
     Pairs of premodifier+premodifier+head mentions and their
     frequency.

   * coref.dict4.tsv
     Pairs of full mentions and their frequency.

   * LICENSE.pdf 
     The PDDL license v1.0 under which this annotation is made
     available.

   * README.txt
     This file.

========================================================================
