# -*- coding: utf-8 -*-

"""
Defines utilities to parse and use 'bart-names' data
"""

__all__ = ["get_bart_names_file_path", 
           "get_iterator_over_bart_names_file",
          ]

import os

from .utils import create_get_resource_file_path_fct, create_get_iterator_over_resource_file_fct

BART_NAMES_FOLDER_PATH = os.path.join(os.path.dirname(__file__), "bart-names")
BART_NAMES_FILE_NAMES = {"adj_map": "adj_map.txt", "englishST": "englishST.txt", 
                         "gender": "gender.txt", "person_female": "person_female.lst", 
                         "person_male": "person_male.lst"}
get_bart_names_file_path = create_get_resource_file_path_fct("bart_names", BART_NAMES_FILE_NAMES, BART_NAMES_FOLDER_PATH)
get_iterator_over_bart_names_file = create_get_iterator_over_resource_file_fct("bart_names", BART_NAMES_FILE_NAMES, BART_NAMES_FOLDER_PATH)
