# -*- coding: utf-8 -*-
"""
Provides utilities to parse and use 'bergsma-gender' data

Shane Bergsma Gender Data Reader

Ref:
    Shane Bergsma and Dekang Lin, Bootstrapping Path-Based Pronoun Resolution, In Proceedings of the 
    Conference on Computational Lingustics / Association for Computational Linguistics (COLING/ACL-06), 
    Sydney, Australia, July 17-21, 2006.

FORMAT: noun phrase [TAB] Masculine_Count [SPACE] Feminine_Count [SPACE] Neutral_Count [SPACE] Plural_Count
"""

__all__ = ["GenderReader", 
           "get_bergsma_gender_file_path", 
           "get_iterator_over_bergsma_gender_file",
           ]

import os
import itertools
import numpy

from cortex.utils.memoize import SimpleMemoized
from cortex.utils.timer import timer
from .utils import create_get_resource_file_path_fct, create_get_iterator_over_resource_file_fct

BERGSMA_GENDER_FOLDER_PATH = os.path.join(os.path.dirname(__file__), "bergsma-gender")
BERGSMA_GENDER_FILE_NAMES = dict((fn, fn) for fn in ["gender.aa", "gender.ab", "gender.ac", 
                                                     "gender.ad", "gender.ae", "gender.af"]
                                 )
get_bergsma_gender_file_path = create_get_resource_file_path_fct("bergsma_gender", BERGSMA_GENDER_FILE_NAMES, BERGSMA_GENDER_FOLDER_PATH)
get_iterator_over_bergsma_gender_file = create_get_iterator_over_resource_file_fct("bergsma_gender", BERGSMA_GENDER_FILE_NAMES, BERGSMA_GENDER_FOLDER_PATH)

MASC_VALUE = "masc"
FEM_VALUE = "fem"
NEUT_VALUE = "neut"
LABEL_VALUES = (MASC_VALUE, FEM_VALUE, NEUT_VALUE)

def _create_word2counts__from_line_iterator(line_iterator):
    """ Creates a map from words to frequency counts, from the parsing of an iterator over lines of 
    a Bergsma gender resource.
    
    Args:
        line_iterator: iterator over lines of a Bergsma gender resource

    Returns:
        a "word => counts" map, where 'counts' is a (masc_ct, fem_ct , neut_ct ) tuple, 
        where:
            - 'masc_ct' is the number of occurrences of the word where the word has been associated to the 
              "masculine" gender value
            - 'fem_ct' is the number of occurrences of the word where the word has been associated to the 
              "feminine" gender value
            - 'neut_ct' is the number of occurrences of the word where the word has been associated to the 
              "neutral" gender value
    """
    word2counts = {}
    for line in line_iterator:
        word, counts_string = line.strip().split("\t")
        counts_strings = counts_string.split()
        masc_ct, fem_ct , neut_ct = map(int, counts_strings[:3])
        word2counts[word] = (masc_ct, fem_ct , neut_ct)
    return word2counts

FILES = ["gender.aa", "gender.ab", "gender.ac", "gender.ad", "gender.ae", "gender.af"]
RESOURCE_FROM_LINE_ITERATOR = itertools.chain(*list(get_iterator_over_bergsma_gender_file(fn) for fn in FILES))
@SimpleMemoized
@timer(print, "loading English GenderReader full data")
def loading_resource():
    """ Parses all the Bergsma gender resources, and return the corresponding map from words to 
    frequency counts.
    
    The result is cached in memory, such that further call to the function will not imply another 
    parsing of the local resources.
    
    Returns:
        a "word => counts" map, where 'counts' is a (masc_ct, fem_ct , neut_ct) tuple, 
        where:
            - 'masc_ct' is the number of occurrences of the word where the word has been associated to the 
              "masculine" gender value
            - 'fem_ct' is the number of occurrences of the word where the word has been associated to the 
              "feminine" gender value
            - 'neut_ct' is the number of occurrences of the word where the word has been associated to the 
              "neutral" gender value
    """
    return _create_word2counts__from_line_iterator(RESOURCE_FROM_LINE_ITERATOR)


class GenderReader(object):
    """ Class offering an interface to use the Bergsma English gender resources for gender value 
    prediction.
    
    Those resources are a compilation of words, and of counts of occurrences of the word, where it 
    was possible to associate the word to a specific English grammatical gender value, among the 
    following possible values: "masc", "fem", "neut".
    An instance of this class allows to take an input word, and output a gender suggestion for this 
    word, it the ressources know of this word: "masc", "fem", "neut"; None when a suggestion cannot 
    be made.
    
    Arguments:
        threshold: int, positive integer, minimum number of time for a word to have been counted, 
            for at least one gender value, in order for this GenderReader to be authorized to provide an 
            answer for this word
    
    Attributes:
        threshold: the value of the 'threshold' parameter used to initialize the class instance
    """
    
    def __init__(self, threshold=1):
        self.threshold = threshold
        self.__GENDER_READER_FULL_DATA = loading_resource() # The resource loading is put here so as to be carried out if and only if someone actually creates and instance of the class.
    
    def _can_answer(self, word):
        """ Checks that Bergsma gender data exists regarding the input word, and, if it does, that 
        the maximum count value associated to this word is higher or equal to the threshold value 
        associated to this instance. Return the counts data is that is the case, or None otherwise.
        
        Args:
            word: string

        Returns:
            the (masc_ct, fem_ct , neut_ct) tuple associated to this word if the condition is 
            met, or None otherwise
        """
        data = self.__GENDER_READER_FULL_DATA.get(word, None)
        if data and max(*data) >= self.threshold:
            return data
        return None
    
    def get_majority_gender(self, word):
        """ Returns the grammatical gender value, associated to the input word according to the 
        Bergsma gender resources, whose count value is the highest of the three; or None, if no 
        Bersgam gender data exists for the input word.
        
        In case of tie, the masculine grammatical gender value has priority over the female one, and 
        both male and female value have priority over the neutral value.
        
        Args:
            word: a string, word whose gender is to be determined

        Returns:
            a string, a value among ["masc", "fem", "neut"]
        """
        data = self._can_answer(word)
        if not data:
            return None
        
        a, b, c = data
        if max(a, b, c) == 0:
            return None
        
        if a >= b:
            if a >= c:
                return MASC_VALUE
            return NEUT_VALUE
        if b >= c:
            return FEM_VALUE
        return NEUT_VALUE

    def get_most_probable_gender(self, word, proba_threshold=1./3, count_threshold=10):
        """ Returns the grammatical gender value, associated to the input word according to the 
        Bergsma gender resources, whose count value is the highest of the three (ties are 
        broken at random); or None, if no Bersgam gender data exists for the input word.
        
        However, if the maximum gender count value associated to this word is strictly inferior to 
        the input threshold value, or if the computed maximum gender probability value is strictly 
        inferior to the input threshold value, then None will be returned instead.
        
        Args:
            word: a string, word whose gender is to be determined
            proba_threshold: the maximum probability for the word to be of a given gender must be 
                above this value for a gender value to be returned (so, if a gender value is returned, the 
                empirical probability that the word have this gender it higher than this threshold value).
            count_threshold: the maximum count value associated to a gender value for the input 
                word must be above this value for a gender value to be returned (so, if a gender value is 
                returned, the empirical count of occurences where this word was of this gender is higher that 
                this threshold value). In effects, it overrides the 'threshold' attribute value when filtering 
                the output.

        Returns:
            a string, a value among ["masc", "fem", "neut"]; or None
        """
        data = self._can_answer(word)
        if not data:
            return None
        
        masc_ct, fem_ct , neut_ct = data
        tot_ct = float(masc_ct + fem_ct + neut_ct)
        count_values = (masc_ct, fem_ct , neut_ct)
        proba_values = tuple(map((lambda ct: ct / tot_ct), (masc_ct, fem_ct , neut_ct)))
        max_count_value_index = numpy.argmax(proba_values)
        if proba_values[max_count_value_index] >= proba_threshold and count_values[max_count_value_index] >= count_threshold:
            return LABEL_VALUES[max_count_value_index]
        
        return None
