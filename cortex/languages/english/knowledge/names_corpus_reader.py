# -*- coding: utf-8 -*-

"""
Defines utilities to parse and use 'names-corpus' data
"""

__all__ = ["NamesCorpusReader", 
           "get_names_corpus_file_path", 
           "get_iterator_over_names_corpus_file", 
          ]

import os

from .utils import create_get_resource_file_path_fct, create_get_iterator_over_resource_file_fct
from cortex.utils.memoize import SimpleMemoized
from cortex.utils.timer import timer

NAMES_CORPUS_FOLDER_PATH = os.path.join(os.path.dirname(__file__), "names-corpus-1.3")
NAMES_CORPUS_FILE_NAMES = {"family-names-cmu": "family-names-cmu.txt", "female": "female.txt", 
                          "male": "male.txt"}
get_names_corpus_file_path = create_get_resource_file_path_fct("name_corpus", NAMES_CORPUS_FILE_NAMES, NAMES_CORPUS_FOLDER_PATH)
get_iterator_over_names_corpus_file = create_get_iterator_over_resource_file_fct("name_corpus", NAMES_CORPUS_FILE_NAMES, NAMES_CORPUS_FOLDER_PATH)

@SimpleMemoized
@timer(print, "loading English NamesCorpusReader full data")
def loading_resource():
    """ Parses all the Names Corpus resources, and return the corresponding (collection, set) pairs.
    
    The result is cached in memory, such that further call to the function will not imply another 
    parsing of the local resources.
    
    Returns:
        a (family_names, female_names, male_names) tuple, where:
        - 'family_names' is a (collection, set) pair representing the same data (strings corresponding 
          to known family names)
        - 'female_names' is a (collection, set) pair representing the same data (strings corresponding 
          to known male names)
        - 'male_names' is a (collection, set) pair representing the same data (strings corresponding to 
          known female names)
    """
    # Family names
    FAMILY_NAMES_LIST = [line.strip() for line in get_iterator_over_names_corpus_file("family-names-cmu")]
    FAMILY_NAMES_SET = set(FAMILY_NAMES_LIST)
    # Female
    FEMALE_NAMES_LIST = [line.strip() for line in get_iterator_over_names_corpus_file("female")]
    FEMALE_NAMES_SET = set(FEMALE_NAMES_LIST)
    # Male
    MALE_NAMES_LIST = [line.strip() for line in get_iterator_over_names_corpus_file("male")]
    MALE_NAMES_SET = set(MALE_NAMES_LIST)
    return (FAMILY_NAMES_LIST, FAMILY_NAMES_SET), (FEMALE_NAMES_LIST, FEMALE_NAMES_SET), (MALE_NAMES_LIST, MALE_NAMES_SET)

(_FAMILY_NAMES_LIST, _FAMILY_NAMES_SET), (_FEMALE_NAMES_LIST, _FEMALE_NAMES_SET), (_MALE_NAMES_LIST, _MALE_NAMES_SET) = loading_resource()


class NamesCorpusReader(object):
    """ Class offering an interface to use the "names corpus" data
    
    Those resources are a compilation of known family names, as well as proper names usually given 
    to male humans on the one hand, and to female humans on the other hand.
    """
    
    @classmethod
    def is_family_name(cls, name, other_set=None):
        """ Check whether or not the input name is a family name, or not, according to the 
        "names corpus" resources.
        
        Args:
            name: a string, the name to test
            other_set: another set-like objet, or None; if not None, the check will be carried 
                out w.r.t. this set-like object

        Returns:
            a boolean
        """
        if other_set:
            return name in other_set
        return name in _FAMILY_NAMES_SET
    
    @classmethod
    def is_female_name(cls, name, other_set=None):
        """ Checks whether or not the input name is a female name, or not, according to the 
        "names corpus" resources.
        
        Args:
            name: a string, the name to test
            other_set: another set-like objet, or None; if not None, the check will be carried 
                out w.r.t. this set-like object

        Returns:
            a boolean
        """
        if other_set:
            return name in other_set
        return name in _FEMALE_NAMES_SET

    @classmethod
    def is_male_name(cls, name, other_set=None):
        """ Checks whether or not the input name is a male name, or not, according to the 
        "names corpus" resources.
        
        Args:
            name: a string, the name to test
            other_set: another set-like objet, or None; if not None, the check will be carried 
                out w.r.t. this set-like object

        Returns:
            a boolean
        """
        if other_set:
            return name in other_set
        return name in _MALE_NAMES_SET

