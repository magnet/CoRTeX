# -*- coding: utf-8 -*-

"""
Defines utilities to parse and use 'coref-dictionary' data

Recasens/Can/Jurafsky coreference dictionary

Ref:
    Recasens, Marta, Matthew Can, and Dan Jurafsky. "Same referent, different words: Unsupervised mining 
    of opaque coreferent mentions." Proceedings of NAACL-HLT. 2013.


Files formats:

DICT1 FILE FORMAT: mention1 [TAB] mention2 [TAB] freq [TAB] NPMI

DICT2 FILE FORMAT: mention1 [TAB] mention2 [TAB] freq

DICT3 FILE FORMAT: mention1 [TAB] mention2 [TAB] freq

DICT4 FILE FORMAT: mention1 [TAB] mention2 [TAB] freq
"""

__all__ = ["CorefDictionaryReader", 
           "get_coref_dictionary_file_path", 
           "get_iterator_over_coref_dictionary_file",
           ]

import os

from .utils import create_get_resource_file_path_fct, create_get_iterator_over_resource_file_fct
from cortex.utils.memoize import SimpleMemoized
from cortex.utils.timer import timer

COREF_DICTIONARY_FOLDER_PATH = os.path.join(os.path.dirname(__file__), "coref-dictionary", "coref-dictionary")
COREF_DICTIONARY_FILE_NAMES = {'coref.dict1': 'coref.dict1.tsv', 'coref.dict2': 'coref.dict2.tsv', 
                               'coref.dict3': 'coref.dict3.tsv', 'coref.dict4': 'coref.dict4.tsv'}
get_coref_dictionary_file_path = create_get_resource_file_path_fct("coref_dictionary", COREF_DICTIONARY_FILE_NAMES, COREF_DICTIONARY_FOLDER_PATH)
get_iterator_over_coref_dictionary_file = create_get_iterator_over_resource_file_fct("coref_dictionary", COREF_DICTIONARY_FILE_NAMES, COREF_DICTIONARY_FOLDER_PATH)

def _dict12key_val_pair_extraction_fct(line):
    """ Parses a file's line of the first "coref dict" resource into the corresponding (key, value) pair.
    
    Args:
        line: a string, a line of the first "coref dict" resource file

    Returns:
        a (key, value) pair
    """
    l = line.strip().split("\t")
    key = (l[0],l[1])
    value = (float(l[2]), float(l[3]))
    return key, value

def _other_dict2key_val_pair_extraction_fct(line):
    """ Parses a file's line of the second, third or fourth "coref dict" resource into the 
    corresponding (key, value) pair.
    
    Args:
        line:a string, a line of the second, third or fourth "coref dict" resource file

    Returns:
        a (key, value) pair
    """
    l = line.strip().split("\t")
    key = (l[0],l[1])
    value = float(l[2])
    return key, value

dict_ident2key_val_pair_extraction_fct = {1: _dict12key_val_pair_extraction_fct, 
                                          2: _other_dict2key_val_pair_extraction_fct, 
                                          3: _other_dict2key_val_pair_extraction_fct, 
                                          4: _other_dict2key_val_pair_extraction_fct,
                                         }

def _create_dict(dict_ident):
    """ Creates the "(mention1, mention2) => data" map corresponding to the input "coref dict" ident.
    
    Args:
        dict_ident: a string, ident identifying the desired resource

    Returns:
        the corresponding "(mention1, mention2) => data" map
    """
    file_ident = "coref.dict{}".format(dict_ident)
    key_val_pair_extraction_fct = dict_ident2key_val_pair_extraction_fct[dict_ident]
    dict_ = dict()
    line_iterator = get_iterator_over_coref_dictionary_file(file_ident)
    next(line_iterator) # Header
    for line in line_iterator:
        key, value = key_val_pair_extraction_fct(line)
        dict_[key] = value
    return dict_


@SimpleMemoized
@timer(print, "loading English CorefDictionaryReader full data")
def loading_resource():
    """ Parses all the "coref dict" resources, and return the corresponding map from pairs of 
    mentions' raw texts to data.
    
    The result is cached in memory, such that further call to the function will not imply another 
    parsing of the local resources.
    
    Returns:
        a (DICT1, DICT2, DICT3, DICT4) tuple,  
        where:
            - 'DICT1' is a "(mention1, mention2) => (count, NPMI)" map corresponding to the first 
              "coref dict" data set
            - 'DICT2' is a "(mention1, mention2) => count" map corresponding to the second "coref dict" data set
            - 'DICT3' is a "(mention1, mention2) => count" map corresponding to the third "coref dict" data set
            - 'DICT4' is a "(mention1, mention2) => count" map corresponding to the fourth "coref dict" data set
    """
    DICT1 = _create_dict(1)
    DICT2 = _create_dict(2)
    DICT3 = _create_dict(3)
    DICT4 = _create_dict(4)
    return DICT1, DICT2, DICT3, DICT4


class CorefDictionaryReader(object):
    """ Class offering an interface to use the "coref-dictionary" data
    
    Those resources are a compilation of the count of occurrences of pairs of mentions, where one 
    pair of mentions encode the fact that they were coreferent in the text that they are extracted 
    from. For a part of those resources (the first "coref dict"), data about NPMI scores is also 
    available (Normalized Pointwise mutual Information).
    """
    
    def __init__(self):
        self.__DICT1, self.__DICT2, self.__DICT3, self.__DICT4 = loading_resource() # The resource loading is put here so as to be carried out if and only if someone actually creates and instance of the class.
    
    def freq_in_dict1(self, antecedent_mention_raw_text, subsequent_mention_raw_text):
        """ Returns the frequency count data associated to the pair of input mentions' respective raw 
        text, searching into the first "coref dict" resource.
        
        Args:
            antecedent_mention_raw_text: the raw text of the antecedent mention of the pair
            subsequent_mention_raw_text: the raw text of the subsequent mention of the pair

        Returns:
            a float, the frequency count value found in the resource for the input pair of 
            mentions' respective raw text; or '0.' if not found in this resource
        """
        freq, _ = self.__DICT1.get((antecedent_mention_raw_text,subsequent_mention_raw_text), (0.,0.))
        return freq

    def npmi_in_dict1(self, mention_raw_text1, mention_raw_text2):
        """ Returns the NPMI value data associated to the pair of input mentions' respective raw 
        text, searching into the first "coref dict" resource.
        
        Args:
            antecedent_mention_raw_text: the raw text of the antecedent mention of the pair
            subsequent_mention_raw_text: the raw text of the subsequent mention of the pair

        Returns:
            a float, the NPMI value found in the resource for the input pair of 
            mentions' respective raw text; or '0.' if not found in this resource
        """
        _, npmi = self.__DICT1.get((mention_raw_text1,mention_raw_text2), (0.,0.))
        return npmi
    
    def freq_in_dict2(self, mention_raw_text1, mention_raw_text2):
        """ Returns the frequency count data associated to the pair of input mentions' respective raw 
        text, searching into the second "coref dict" resource.
        
        Args:
            antecedent_mention_raw_text: the raw text of the antecedent mention of the pair
            subsequent_mention_raw_text: the raw text of the subsequent mention of the pair

        Returns:
            a float, the frequency count value found in the resource for the input pair of 
            mentions' respective raw text; or '0.' if not found in this resource
        """
        freq = self.__DICT2.get((mention_raw_text1,mention_raw_text2), 0.)
        return freq
    
    def freq_in_dict3(self, mention_raw_text1, mention_raw_text2):
        """ Returns the frequency count data associated to the pair of input mentions' respective raw 
        text, searching into the third "coref dict" resource.
        
        Args:
            antecedent_mention_raw_text: the raw text of the antecedent mention of the pair
            subsequent_mention_raw_text: the raw text of the subsequent mention of the pair

        Returns:
            a float, the frequency count value found in the resource for the input pair of 
            mentions' respective raw text; or '0.' if not found in this resource
        """
        freq = self.__DICT3.get((mention_raw_text1,mention_raw_text2), 0.)
        return freq

    def freq_in_dict4(self, mention_raw_text1, mention_raw_text2):
        """ Returns the frequency count data associated to the pair of input mentions' respective raw 
        text, searching into the fourth "coref dict" resource.
        
        Args:
            antecedent_mention_raw_text: the raw text of the antecedent mention of the pair
            subsequent_mention_raw_text: the raw text of the subsequent mention of the pair

        Returns:
            a float, the frequency count value found in the resource for the input pair of 
            mentions' respective raw text; or '0.' if not found in this resource
        """
        freq = self.__DICT4.get((mention_raw_text1,mention_raw_text2), 0.)
        return freq
