# -*- coding: utf-8 -*-

"""
Defines a class used to compute a probability value that an English document's Token instance 
representing the word 'it' is referential.

Here the library relies on an external tool, the nada software suite, which is is accessed 
through the use of the NadaWrapper class.
"""

__all__ = ["ReferentialEnglishItTokenProbaComputer",
           "CannotComputeReferentialProbaError",
           ]

from cortex.tools.wrapper.nada import NadaWrapper, UnavailableExternalRessourceError
from cortex.utils import CoRTeXException

class ReferentialEnglishItTokenProbaComputer(object):
    """ A class defining a tool used to compute a probability value that an English document's token 
    representing the word 'it' is referential.
    
    Attributes:
        POS_tags_module: the python module defining the constituency tree tags used to characterize 
            the constituency tree of the document which the mentions originate from (the 
            "modified_CC_treebank_constituent_tags" module)
        
        pred_args_tags: the python module defining the predicate arguments tags used to characterize 
            the sentences of the document (the "ontonotes_v5_predicate_arguments_tags" module)
    """
    
    def __init__(self):
        try:
            self.nada_wrapper = NadaWrapper()
        except UnavailableExternalRessourceError as e:
            msg = "Cannot compute referential probabilty values: {}"
            msg = msg.format(e.args[0] if e.args else "")
            raise CannotComputeReferentialProbaError(msg)
    
    @classmethod
    def _test_sent(cls, sentence):
        """ This function can be used to implement a filter on sentences for which to carry out the computation.
        
        For now, accepts all sentences.
        
        Args:
            sentence: a Sentence instance

        Returns:
            a boolean
        """
        return True
    
    def compute_referential_proba(self, documents):
        """ Carries out the probabilities computation for each document of the input collection.
        
        Args:
            documents: a collection of documents

        Returns:
            a collection of token_extent2non_referential_proba mappings, one per document, which 
            are "token extent => float" mapping
        """
        # For each document, write sentence only if the token 'it' belongs it?
        # Determine which sentences should be input into the computing process
        tokenized_sentences = []
        documents_sentences_indices = []
        for document in documents:
            document_sentences_indices = []
            for i, sentence in enumerate(document.sentences):
                if self._test_sent(sentence):
                    #sentence_text = sentence.raw_text
                    sentence_text = " ".join(token.raw_text for token in sentence.tokens)
                    tokenized_sentences.append(sentence_text)
                    document_sentences_indices.append(i)
            documents_sentences_indices.append(tuple(document_sentences_indices))
        
        # Carry out the computing process
        processed_sentences_data = self.nada_wrapper.process_sentences(tokenized_sentences)
        
        # Compute the mapping for each document
        mappings = []
        index = 0
        for document, document_sentences_indices in zip(documents, documents_sentences_indices):
            sentence_nb = len(document_sentences_indices)
            document_processed_sentences_data = processed_sentences_data[index:index+sentence_nb]
            document_sentences = document.sentences # FIXME: what is that doing here ? Always the first document?
            mapping = {}
            for sentence_index, sentence_data in zip(document_sentences_indices, 
                                                     document_processed_sentences_data):
                sentence = document_sentences[sentence_index]
                for token_index, non_referential_proba in sentence_data:
                    token = sentence.tokens[token_index]
                    referential_proba = 1 - non_referential_proba
                    mapping[token.extent] = referential_proba
            mappings.append(mapping)
            index += sentence_nb
        
        mappings = tuple(mappings)
        return mappings

# Exceptions
class CannotComputeReferentialProbaError(CoRTeXException):
    """ Exception to be raised when failing to be able to carry out the probabilities computation, 
    for possibly various reasons.
    """
    pass