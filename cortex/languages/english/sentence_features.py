# -*- coding: utf-8 -*-

"""
Defines a class used to compute data and values corresponding to grammatical information, also called 
grammatical features here, from instances of the Sentence class defined for document written in English.
"""
import itertools

__all__ = ["EnglishSentenceFeatures", 
           "ENGLISH_SENTENCE_FEATURES",
           ]

class EnglishSentenceFeatures(object):
    """ A class defining the grammatical features that can be interesting to know about a sentence 
    of a document written in English.
    
    Attributes:
        POS_tags_module: the python module defining the constituency tree tags used to characterize 
            the constituency tree of the document which the mentions originate from (the 
            "ontonotes_v5_constituent_tags" module)
    """
    
    def __init__(self):
        # Loading the resources to be used to define the 'feature' methods
        from .parameters import ontonotes_v5_constituent_tags
        self.POS_tags_module = ontonotes_v5_constituent_tags
    
    ## Instance methods
    def get_breadth_first_sorted_mentions(self, sentence):
        """ Returns the collection of mentions associated to a sentence, and which correspond to nodes 
        of the constituency tree associated to the sentence, nodes that are parsed in breadth first 
        order, and whose syntactic tag is that of either a noun phrase, a verb phrase, or a possessive 
        expanded pronoun. 
        
        Args:
            sentence: a Sentence instance

        Returns:
            collection of Mention instances
        

        Warning:
            This may output only a partial collection of the mentions associated to the sentence, 
            if, for instance, the constituency parse tree is incorrect.
        """
        # WARNING: This may output partial list of mentions # FIXME: insert non-matched mentions ?
        sorted_mentions = []
        # Mention extents map
        extent2mention = sentence.extent2mention
        # Sorted NP/VP breadth-first search in parse tree
        used_mention_extents = set() # Avoid multiple matches
        sentence_nodes = sentence.constituency_tree.get_tagged_nodes(self.POS_tags_module.SENTENCE_TAGS)
        if not sentence_nodes:
            sentence_nodes = sentence.constituency_tree.get_tagged_nodes(self.POS_tags_module.ROOT_TAGS)
        for sentence_node in sentence_nodes:
            for node in sentence_node.get_tagged_nodes((self.POS_tags_module.NOUN_PHRASE_TAG, 
                                                        self.POS_tags_module.VERB_PHRASE_TAG, 
                                                        self.POS_tags_module.POSSESSIVE_EXPANDED_PRONOUN_TAG)):
                if node.syntactic_tag in (self.POS_tags_module.NOUN_PHRASE_TAG, 
                                          self.POS_tags_module.POSSESSIVE_EXPANDED_PRONOUN_TAG):
                    extent = node.raw_extent
                elif node.syntactic_tag == self.POS_tags_module.VERB_PHRASE_TAG:
                    extent = node.get_leftmost_leaf().raw_extent # We want the extent of the verb token, and we assume it is the first one, in the document reading order.
                else:
                    possible_values = list(itertools.chain(self.POS_tags_module.NOUN_PHRASE_TAG, 
                                                           self.POS_tags_module.VERB_PHRASE_TAG, 
                                                           self.POS_tags_module.POSSESSIVE_EXPANDED_PRONOUN_TAG))
                    message = "Bad syntactic tag '{}' for node '{}' of document '{}': authorized values are: {}."
                    message = message.format(node.syntactic_tag, node, sentence.document, possible_values)
                    raise ValueError(message)
                    #extent = node.raw_extent
                mention = extent2mention.get(extent, None)
                if mention is not None and mention.extent not in used_mention_extents:
                    sorted_mentions.append(mention)
                    used_mention_extents.add(mention.extent)
            return sorted_mentions
    

ENGLISH_SENTENCE_FEATURES = EnglishSentenceFeatures()
