# -*- coding: utf-8 -*-

"""
Defines a class used to detect mentions in a document written in English.

Use EnglishConstituencyTreeFeature and EnglishConstituencyTreeNodeFeatures instances, which themselves 
support only documents whose constituency tree annotations use tags used to annotate the documents 
of the 'Ontonotes v5' corpus.
"""

__all__ = ["EnglishMentionDetector",
           ]

from collections import defaultdict


from cortex.utils import get_unique_elts
from cortex.languages.common.mention_detector import _MentionDetector

from cortex.languages.english.knowledge.lexicon import (QUANTIFIERS, PARTITIVES, NON_WORDS, 
                                                          VERB_TO_NOUN, STOP_WORDS)
from .knowledge.names_corpus_reader import NamesCorpusReader
from .parameters import ontonotes_v5_constituent_tags as POS_tags_module
from .parameters import ontonotes_v5_named_entities_data_tags as NE_tags
from .token_features import ENGLISH_TOKEN_FEATURES
from .constituency_tree_features import (ENGLISH_CONSTITUENCY_TREE_FEATURES, 
                                                                   ENGLISH_CONSTITUENCY_TREE_NODE_FEATURES)

NAMES_CORPUS_READER = NamesCorpusReader()
NON_MENTION_NAMED_ENTITYS = set()
for set_like in (NE_tags.PERCENT_NAMED_ENTITY_TYPE_TAGS, NE_tags.MONEY_NAMED_ENTITY_TYPE_TAGS, 
                 NE_tags.QUANTITY_NAMED_ENTITY_TYPE_TAGS, NE_tags.CARDINAL_NAMED_ENTITY_TYPE_TAGS):
    NON_MENTION_NAMED_ENTITYS.update(set_like)


class EnglishMentionDetector(_MentionDetector):
    """ Class whose role is to detect mentions in a document written in English.
    The document will have to be annotated in constituency trees using tags defined in the 
    CONLL2012 corpus' tags set.
    
    The aim is to extract mention candidates corresponding to 'noun phrases'.
    """
    _TOKEN_FEATURES = ENGLISH_TOKEN_FEATURES
    _CONSTITUENCY_TREE_FEATURES = ENGLISH_CONSTITUENCY_TREE_FEATURES
    _CONSTITUENCY_TREE_NODE_FEATURES = ENGLISH_CONSTITUENCY_TREE_NODE_FEATURES
    
    @classmethod
    def _get_NP_leaves_boundaries(cls, constituency_tree, head_search_heuristic="leaf"):
        """ Returns collection of (node leaves boundaries, node's head node leaves boundaries) pairs 
        from iterating over nodes, starting from the root of the input tree and then carrying a 
        breadth-first search on its children, which correspond to 'noun phrase'.
        
        Args:
            constituency_tree: a ConstituencyTree instance
            head_search_heuristic: string, must belong to {"rule", "leaf"}, parameter specifying 
                the heuristic to use when carrying out a search for the head node among a node's descendants

        Returns:
            a collection of (ConstituencyTreeNode instance pair, ConstituencyTreeNode instance pair) pairs
        """
        tags = [POS_tags_module.NOUN_PHRASE_TAG]
        return cls._CONSTITUENCY_TREE_FEATURES.get_tags_leaves_boundaries(constituency_tree, tags, find_head=True,
                                               head_search_heuristic=head_search_heuristic)
    
    @classmethod
    def _get_expanded_possessive_leaves_boundaries(cls, constituency_tree):
        """ Returns a collection of (node leaves boundaries, node's head node leaves boundaries) pairs 
        from iterating over the leaf nodes of the root of the input constituency tree, as long as 
        their respective corresponding token is a 'possessive_determiner'.
        
        Assumes that the input node's leaves have been synchronized with their corresponding token.
        
        Args:
            constituency_tree: a ConstituencyTree instance

        Returns:
            a collection of (ConstituencyTreeNode instance pair, ConstituencyTreeNode instance pair) pair
        """
        tags = [POS_tags_module.POSSESSIVE_EXPANDED_PRONOUN_TAG]
        return cls._CONSTITUENCY_TREE_FEATURES.get_tags_leaves_boundaries(constituency_tree, tags, find_head=False)
    
    @classmethod
    def _get_verb_leaves_boundaries(cls, constituency_tree):
        """ Return collection of (node leaves boundaries, node's head node leaves boundaries) pairs 
        from iterating over nodes, starting from the root of the input tree and then carrying a 
        breadth-first search on its children, which correspond to 'verb' words.
        
        Args:
            constituency_tree: a ConstituencyTree instance
            head_search_heuristic: string, must belong to {"rule", "leaf"}, parameter specifying 
                the heuristic to use when carrying out a search for the head node among a node's descendants

        Returns:
            a collection of (ConstituencyTreeNode instance pair, ConstituencyTreeNode instance pair) pairs
        """
        tags = POS_tags_module.VERBS_POS_TAGS
        return cls._CONSTITUENCY_TREE_FEATURES.get_tags_leaves_boundaries(constituency_tree, tags, find_head=False)
    
    @classmethod
    def _detect_mentions(cls, document, detect_verbs=False):
        """Detects mentions in the document, replace potentially already existing mentions and 
        coreference partition information.
        
        Assumes that the document has been characterized (notably, info about sentence's constituency 
        tree and tokens, with synchronization between tokens and constituency tree nodes).
        
        Args:
            detect_verb: whether or not to consider some verbal mention candidates during the search

        Returns:
            mention_head_extent2mention_extent mapping (some 'head_extent' might in fact encompass several tokens instead of one)
        """
        tokens = document.tokens
        
        # BUILD leaves_boundaries to tokens
        start_index2token = {}
        end_index2token = {}
        for i, token in enumerate(tokens):
            s,e = token.extent
            start_index2token[s] = (i, token)
            end_index2token[e] = (i, token)
        
        # DETECT MENTIONS
        # Mentions extracted from constituency tree
        tokens_leaves_boundaries_pairs = [] # Token leaves boundaries representing parts of text that are candidates to be mention (to be converted to raw leaves_boundaries)
        sentences = document.sentences
        for i, sentence in enumerate(sentences):
            constituency_tree = sentence.constituency_tree
            # DETECT Noun phrases
            tokens_leaves_boundaries_pairs.extend(cls._get_NP_leaves_boundaries(constituency_tree)) # Leaves_boundaries elements are leaves of the parse tree
            # DETECT Possessive determiner
            tokens_leaves_boundaries_pairs.extend(cls._get_expanded_possessive_leaves_boundaries(constituency_tree))
            # DETECT Verbs filtered in "verb-pronoun", "verb-verb" and "verb-noun" patterns
            if detect_verbs:
                verb2leaves_boundaries_pairs = defaultdict(list)
                kept_leaves_boundaries_pairs = []
                verb_leaves_boundaries_pairs = cls._get_verb_leaves_boundaries(constituency_tree)
                for (leaves_boundaries, head_leaves_boundaries) in verb_leaves_boundaries_pairs:
                    leftmost_leaf, rightmost_leaf = leaves_boundaries
                    v_lemma = rightmost_leaf.token.lemma.lower()
                    # Exclude some stop words
                    if v_lemma in ("be", "have", "say", "go"):
                        continue
                    
                    verb2leaves_boundaries_pairs[v_lemma].append((leaves_boundaries, head_leaves_boundaries))
                    v_end = rightmost_leaf.token.end # Raw end position
                    # Find "verb-pronoun" patterns (pronoun = it/this/that)
                    pattern_found = False
                    sentence_length = len(sentences)
                    for j in range(i, min(i+2, sentence_length)): # current and next sentence
                        s_constituency_tree = sentences[j].constituency_tree
                        for ((s_leftmost_leaf, s_rightmost_leaf), _) in cls._get_NP_leaves_boundaries(s_constituency_tree):
                            if s_leftmost_leaf == s_rightmost_leaf: # FIXME: use 'is' instead of '=='?
                                s_token = s_leftmost_leaf.token
                                t_start = s_token.start
                                if t_start > v_end and s_token.raw_text.lower() in ("it", "that", "this"):
                                    pattern_found = True
                                    break
                        if pattern_found:
                            break
                    if pattern_found:
                        kept_leaves_boundaries_pairs.append((leaves_boundaries, head_leaves_boundaries))
                # Find "verb-verb" patterns (verbs appearing several times in the document)
                for leaves_boundaries_pairs_ in verb2leaves_boundaries_pairs.values():
                    if len(leaves_boundaries_pairs_) > 1:
                        kept_leaves_boundaries_pairs.extend(leaves_boundaries_pairs_)
                # Find "verb-noun" patterns (verb and its nominalization)
                noun2verb = {}
                added = set()
                for v_lemma in verb2leaves_boundaries_pairs.keys():
                    nouns_ = VERB_TO_NOUN.get(v_lemma, tuple())
                    if nouns_:
                        for noun in nouns_:
                            noun2verb[noun] = v_lemma
                for token in tokens:
                    lemma = token.lemma.lower()
                    if lemma in noun2verb:
                        v_lemma = noun2verb[lemma]
                        if v_lemma not in added:
                            kept_leaves_boundaries_pairs.extend(verb2leaves_boundaries_pairs[v_lemma])
                            added.add(v_lemma)
                tokens_leaves_boundaries_pairs.extend(kept_leaves_boundaries_pairs)
        
        # Get raw extent data
        mention_head_extent2mention_extent = {}
        seen_mention_extents = set() # To avoid duplicate mention extent
        # Take largest extent if same head_extent
        for (leaves_boundaries, head_leaves_boundaries) in tokens_leaves_boundaries_pairs: # Leaves_boundaries in constituency tree
            leftmost_leaf, rightmost_leaf = leaves_boundaries # leftmost_leaf, rightmost_leaf are nodes in constituency tree
            head_leftmost_leaf, head_rightmost_leaf = head_leaves_boundaries # These are nodes in constituency tree
            s, e = leftmost_leaf.start, rightmost_leaf.end # Mention's extent in text
            hs, he = head_leftmost_leaf.start, head_rightmost_leaf.end # Mention's head's extent in text
            # If a potential mention with this head has already been registered, replace it the one being examined in larger / is the outermost potential mention.
            if (hs,he) in mention_head_extent2mention_extent:
                # FIXME: why delete the innermost mentions?
                s2, e2 = mention_head_extent2mention_extent[(hs,he)]
                s_min = min(s, s2)
                e_max = max(e, e2)
                mention_head_extent2mention_extent[(hs,he)] = (s_min,e_max)
                if s_min != s2 or e_max != e2:
                    seen_mention_extents.remove((s2,e2))
                    seen_mention_extents.add((s_min,e_max))
            # Else, register the potential mention being examined
            else:
                if (s,e) not in seen_mention_extents:
                    mention_head_extent2mention_extent[(hs,he)] = (s,e)
                    seen_mention_extents.add((s,e))
        
        # DETECTION add named entities: if something is a named entity, it is a mention
        # FIXME: move this upstream ? (legacy fixme) (I can understand, since it is a DETECTION phase...)
        if document.named_entities:
            for named_entity in document.named_entities:
                extent = named_entity.extent
                if extent not in mention_head_extent2mention_extent and extent not in seen_mention_extents:
                    mention_head_extent2mention_extent[extent] = extent
                ne_type = named_entity.type
                s, _ = extent
                # FIXME: do not understand: whether the condition is met or not, we already added the mention if a named entity existed for it (but this time we can bypass seen_mention_extent check... why?)
                if ne_type in NE_tags.ORGANIZATION_NAMED_ENTITY_TYPE_TAGS and "the" not in document.get_raw_text_extract((max(s-4,0),s-1)).lower(): # FIXME: find a better fix than that
                    mention_head_extent2mention_extent[extent] = extent
                # FIXME: what to do about this commented-out legacy code?
                # I think this code aims to add the would-be mention only if the would-be mention's youngest ancestor node's leaves are consistent with the tokens constituting the would-be mention
                '''
    #            s, e = extent
    #            if (s,e) not in mention_head_extent2mention_extent and (s,e) not in seen_mention_extents:
    #                # Find a head_extent
    #                ## Consider the first and last token of the would-be mention
    #                _, start_token = start_index2token[s]
    #                _, end_token = end_index2token[e]
    #                # Consider their respective constituency_tree_node
    #                start_node = start_token.constituency_tree_node
    #                end_node = end_token.constituency_tree_node
    #                # Compute their youngest ancestor node
    #                ancestor_node = start_node.get_youngest_common_ancestor(end_node)
    #                # We proceed only if the youngest ancestor node exists and spans exactly the would-be mention
    #                if ancestor_node is not None and ancestor_node.raw_extent == (s,e): # only keep span corresponding to a subtree in parse
    #                    hs,he = s,e # Default mention head extent, will be modified if head finder works
    #                    head_node = cls._CONSTITUENCY_TREE_NODE_FEATURES.get_head(ancestor_node)
    #                    if head_node is not None and head_node.is_leaf():
    #                        hs,he = head_node.token.extent
    #                        if (hs,he) not in mention_head_extent2mention_extent: # FIXME: no check with 'seen_mention_extent'?
    #                            mention_head_extent2mention_extent[(hs,he)] = (s,e)
                '''
        # REMOVE
        mention_head_extent_to_remove = []
        ## Remove unacceptable mentions
        named_entity_extent2named_entity_type = {}
        if document.named_entities:
            for named_entity in document.named_entities:
                named_entity_extent2named_entity_type[named_entity.extent] = named_entity.type
        for head_extent, mention_extent in mention_head_extent2mention_extent.items():
            m_text = document.get_raw_text_extract(mention_extent).lower().strip()
            s, e = mention_extent
            # Remove percent, money, cardinal and quantities
            if named_entity_extent2named_entity_type.get(mention_extent, None) in NON_MENTION_NAMED_ENTITYS:
                mention_head_extent_to_remove.append(head_extent)
            # Remove "you/them/us" preceded by "all of", "many of", "the CD of", etc
            if m_text in ("them", "us", "you"):
                left_text = document.get_raw_text_extract((max(s-10,0),s-1)).rstrip()
                for prefix in ("all of", "both of", "some of", "many of"):
                    if left_text.endswith(prefix):
                        mention_head_extent_to_remove.append(head_extent)
                        break
                # FIXME: code common to MentionCharacterizer, right ? Is it worth it then to build a TokenFeatures instance method ?
                left_index, _ = start_index2token[s]
                if left_index - 3 >= 0:
                    if cls._TOKEN_FEATURES.is_the_CD_of(tokens[left_index-3], tokens[left_index-2], tokens[left_index-1]):
                        mention_head_extent_to_remove.append(head_extent)
            # Remove quantified expressions
            if not (m_text.endswith("them") or m_text.endswith("us") or m_text.endswith("you")):
                # Keep expressions like "all of them", "both of you", etc.
                for quantifier in QUANTIFIERS:
                    if quantifier in ("all", "some", "many", "little"): # Some mentions of this form
                        continue
                    if m_text.startswith(quantifier):
                        mention_head_extent_to_remove.append(head_extent)
                        break
            # Remove NPs preceded by partitive (e.g. thousands of)
            prev_text = document.get_raw_text_extract((max(s-20,0),s-1)).rstrip() # Take enough text
            for partitive in PARTITIVES:
                if prev_text.endswith(partitive):
                    mention_head_extent_to_remove.append(head_extent)
                    break
            # Remove non-words
            if m_text in NON_WORDS:
                mention_head_extent_to_remove.append(head_extent)
            # Remove expressions
            if document.get_raw_text_extract((max(s-3,0),e)).strip().lower() == "in fact":
                mention_head_extent_to_remove.append(head_extent)
            # Remove some stop words
            # FIXME: code common to MentionCharacterizer, right ? Is it worth it then to build a TokenFeatures instance method ?
            if m_text in ["'s"]:
                mention_head_extent_to_remove.append(head_extent)
            # FIXME: what to do about this legacy code?
            '''
#            if m_text in STOP_WORDS:
#                mention_head_extent_to_remove.append(head_extent)
            '''
        # Remove duplicate entries
        mention_head_extent_to_remove = get_unique_elts(mention_head_extent_to_remove)
        # Execute the removal
        for head_extent in mention_head_extent_to_remove:
            mention_head_extent2mention_extent.pop(head_extent)
        '''
        # FIXME: extend this search to every document, by restricting the list of tokens that are looked up to the one who possess a 'NNP' POS_tag?
        # FIXME: move this code upstream (part of a DETECTION phase?)?
        # Add names for CoNLL.nt (no NE detection in corpus)
        if document.ident.startswith("nt_"):
            mention_extents = set(mention_head_extent2mention_extent.values())
            for token in tokens:
                extent = token.extent
                text = token.raw_text
                if NAMES_CORPUS_READER.is_male_name(text) or NAMES_CORPUS_READER.is_female_name(text):
                    if extent not in mention_extents and extent not in mention_head_extent2mention_extent: # If the extent is neither an already found mention's head extent or a mention extent, we add it
                        mention_head_extent2mention_extent[extent] = extent
        '''
        return mention_head_extent2mention_extent
