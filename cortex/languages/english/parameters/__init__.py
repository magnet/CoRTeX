# -*- coding: utf-8 -*-

"""
Defines data that specify ways to qualify text written in English (e.g: different sets of syntactic tags...)
"""

"""
Available submodules(s)
-----------------------
ontonotes_v5_constituent_tags
    Defines the syntactic tags used by the 'Ontonotes v5' corpus to qualify its English documents

ontonotes_v5_named_entities_data_tags
    Defines the named entities type used by the 'Ontonotes v5' corpus to qualify its English documents

ontonotes_v5_predicate_argument_tags
    Defines the predicate argument tags used by the 'Ontonotes v5' corpus to qualify its English documents

penn_treebank_constituent_tags
    Defines the syntactic tags used by the English Penn Treebank
"""