# -*- coding: utf-8 -*-
"""
Defines the syntactic tags used by the English Penn Treebank

That includes the Part-Of-Speech tags as well as the syntactic tags.

One source:
http://www.surdeanu.info/mihai/teaching/ista555-fall13/readings/PennTreebankConstituents.html
"""

##################
# Bracket Labels # (a.k.a. Syntactic tags)
##################
# Clause Level
SIMPLE_DECLARATIVE_CLAUSE_TAG           = "S" # Simple declarative clause. See source for more
SUBORDINATING_CONJUNCTION_CLAUSE_TAG    = "SBAR" # Clause introduced by a (possibly empty) subordinating conjunction.
DIRECT_QUESTION_TAG                     = "SBARQ"  # Direct question introduced by a wh-word or a wh-phrase. See source for more
INVERTED_DECLARATIVE_SENTENCE_TAG       = "SINV" # Inverted declarative sentence, i.e. one in which the subject follows the tensed verb or modal.
INVERTED_CLOSED_QUESTION_TAG            = "SQ" # Inverted yes/no question, or main clause of a wh-question, following the wh-phrase in SBARQ.

# Phrase Level
ADJECTIVE_PHRASE_TAG                    = "ADJP" # Adjective Phrase.
ADVERB_PHRASE_TAG                       = "ADVP" # Adverb Phrase.
CONJUNCTION_PHRASE_TAG                  = "CONJP" # Conjunction Phrase.
FRAGMENT_TAG                            = "FRAG" # Fragment.
INTERJECTION_TAG                        = "INTJ" # Interjection. Corresponds approximately to the part-of-speech tag UH.
LIST_MARKER_TAG                         = "LST" # List marker. Includes surrounding punctuation.
NOT_A_CONSTITUENT_TAG                   = "NAC" # Not a Constituent; used to show the scope of certain prenominal modifiers within an NP.
NOUN_PHRASE_TAG                         = "NP" # Noun Phrase.
COMPLEX_NP_HEAD_TAG                     = "NX" # Used within certain complex NPs to mark the head of the NP. See source for more
PREPOSITIONAL_PHRASE_TAG                = "PP" # Prepositional Phrase.
PARENTHETICAL_TAG                       = "PRN" # Parenthetical.
PARTICLE_TAG                            = "PRT" # Particle. Category for words that should be tagged RP.
QUANTIFIER_PHRASE_TAG                   = "QP" # Quantifier Phrase (i.e. complex measure/amount phrase); used within NP.
REDUCED_RELATIVE_CLAUSE_TAG             = "RRC" # Reduced Relative Clause.
UNLIKE_COORDINATED_PHRASE_TAG           = "UCP" # Unlike Coordinated Phrase.
VERB_PHRASE_TAG                         = "VP" # Verb Phrase.
WH_ADJECTIVE_PHRASE_TAG                 = "WHADJP" # Wh-adjective Phrase. Adjectival phrase containing a wh-adverb, as in how hot.
WH_ADVERB_PHRASE_TAG                    = "WHAVP" # Wh-adverb Phrase. Introduces a clause with an NP gap. See source for more
WH_NOUN_PHRASE_TAG                      = "WHNP" # Wh-noun Phrase. Introduces a clause with an NP gap. See source for more
WH_PREPOSITIONAL_PHRASE_TAG             = "WHPP" # Wh-prepositional Phrase. See source for more
UNKNOWN_TAG                             = "X" # Unknown, uncertain, or unbracketable. See source for more

# Word level, i.e. POS tags
COORDINATING_CONJUNCTION_TAG                    = "CC" # Coordinating conjunction
CARDINAL_NUMBER_TAG                             = "CD" # Cardinal number
DETERMINER_TAG                                  = "DT" # Determiner
EXISTENTIAL_THERE_TAG                           = "EX" # Existential there
FOREIGN_WORD_TAG                                = "FW" # Foreign word
PREPOSITION_OR_SUBORDINATING_CONJUNCTION_TAG    = "IN" # Preposition or subordinating conjunction
ADJECTIVE_TAG                                   = "JJ" # Adjective
ADJECTIVE_COMPARATIVE_TAG                       = "JJR" # Adjective, comparative
ADJECTIVE_SUPERLATIVE_TAG                       = "JJS" # Adjective, superlative
LIST_ITEM_MARKER_TAG                            = "LS" # List item marker
MODAL_TAG                                       = "MD" # Modal
NOUN_SINGULAR_OR_MASS_TAG                       = "NN" # Noun, singular or mass
NOUN_PLURAL_TAG                                 = "NNS" # Noun, plural
PROPER_NOUN_SINGULAR_TAG                        = "NNP" # Proper noun, singular
PROPER_NOUN_PLURAL_TAG                          = "NNPS" # Proper noun, plural
PREDETERMINER_TAG                               = "PDT" # Predeterminer
POSSESSIVE_ENDING_TAG                           = "POS" # Possessive ending
PERSONAL_PRONOUN_TAG                            = "PRP" # Personal pronoun
POSSESSIVE_PRONOUN_TAG                          = "PRP$" # Possessive pronoun (prolog version PRP-S)
ADVERB_TAG                                      = "RB" # Adverb
ADVERB_COMPARATIVE_TAG                          = "RBR" # Adverb, comparative
ADVERB_SUPERLATIVE_TAG                          = "RBS" # Adverb, superlative
PARTICLE_TAG                                    = "RP" # Particle
SYMBOL_TAG                                      = "SYM" # Symbol
TO_TAG                                          = "TO" # to
INTERJECTION_TAG                                = "UH" # Interjection
VERB_BASE_FORM_TAG                              = "VB" # Verb, base form
VERB_PAST_TENSE_TAG                             = "VBD" # Verb, past tense
VERB_GERUND_OR_PRESENT_PARTICIPLE_TAG           = "VBG" # Verb, gerund or present participle
VERB_PAST_PARTICIPLE_TAG                        = "VBN" # Verb, past participle
VERB_NON_3RD_PERSON_SINGULAR_PRESENT_TAG        = "VBP" # Verb, non-3rd person singular present
VERB_3RD_PERSON_SINGULAR_PRESENT_TAG            = "VBZ"# Verb, 3rd person singular present
WH_DETERMINER_TAG                               = "WDT" # Wh-determiner
WH_PRONOUN_TAG                                  = "WP" # Wh-pronoun
POSSESSIVE_WH_PRONOUN_TAG                       = "WP$" # Possessive wh-pronoun (prolog version WP-S)
WH_ADVERB_TAG                                   = "WRB" # Wh-adverb


# Function tags
## Form/function discrepancies
ADVERBIAL                   = "ADV" # (adverbial) - marks a constituent other than ADVP or PP when it is used adverbially (e.g. NPs or free ("headless" relatives). See source for more
NOMINAL                     = "NOM" # (nominal) - marks free ("headless") relatives and gerunds when they act nominally.

## Grammatical role
DATIVE                      = "DTV" # (dative) - marks the dative object in the unshifted form of the double object construction. See source for more
LOGICAL_SUBJECT             = "LGS" # (logical subject) - is used to mark the logical subject in passives. See source for more
PREDICATE                   = "PRD" # (predicate) - marks any predicate that is not VP. See source for more
LOCATIVE_PUT_COMPLEMENT     = "PUT" # - marks the locative complement of put.
SURFACE_SUBJECT             = "SBJ" # (surface subject) - marks the structural surface subject of both matrix and embedded clauses, including those with null subjects.
TOPICALIZED                 = "TPC" # ("topicalized") - marks elements that appear before the subject in a declarative sentence, but in two cases only: See source for more
VOCATIVE                    = "VOC" # (vocative) - marks nouns of address, regardless of their position in the sentence. See source for more

##Adverbials: Adverbials are generally VP adjuncts.
BENEFACTIVE                 = "BNF" # (benefactive) - marks the beneficiary of an action (attaches to NP or PP). See source for more
DIRECTION                   = "DIR" # (direction) - marks adverbials that answer the questions "from where?" and "to where?" See source for more
EXTENT                      = "EXT" # (extent) - marks adverbial phrases that describe the spatial extent of an activity. See source for more
LOCATIVE                    = "LOC" # (locative) - marks adverbials that indicate place/setting of the event. See source for more
MANNER                      = "MNR" # (manner) - marks adverbials that indicate manner, including instrument phrases.
PURPOSE_OR_REASON           = "PRP" # (purpose or reason) - marks purpose or reason clauses and PPs.
TEMPORAL                    = "TMP" # (temporal) - marks temporal or aspectual adverbials that answer the questions when, how often, or how long. See source for more

##Miscellaneous
CLOSELY_RELATED             = "CLR" # (closely related) - marks constituents that occupy some middle ground between arguments and adjunct of the verb phrase. See source for more
CLEFT                       = "CLF" # (cleft) - marks it-clefts ("true clefts") and may be added to the labels S, SINV, or SQ.
HEADLINE                    = "HLN" # (headline) - marks headlines and datelines.See source for more
TITLE                       = "TTL" # (title) - is attached to the top node of a title when this title appears inside running text. See source for more



######################
# Using the resource #
######################
# Creating a list to create a map to create an indexing function to reproduce the index system of the Penn TreeBank
POS_TAG_LIST = set([COORDINATING_CONJUNCTION_TAG, CARDINAL_NUMBER_TAG, DETERMINER_TAG, 
                EXISTENTIAL_THERE_TAG, FOREIGN_WORD_TAG, PREPOSITION_OR_SUBORDINATING_CONJUNCTION_TAG, 
                ADJECTIVE_TAG, ADJECTIVE_COMPARATIVE_TAG, ADJECTIVE_SUPERLATIVE_TAG, 
                LIST_ITEM_MARKER_TAG, MODAL_TAG, NOUN_SINGULAR_OR_MASS_TAG, NOUN_PLURAL_TAG, 
                PROPER_NOUN_SINGULAR_TAG, PROPER_NOUN_PLURAL_TAG, PREDETERMINER_TAG, 
                POSSESSIVE_ENDING_TAG, PERSONAL_PRONOUN_TAG, POSSESSIVE_PRONOUN_TAG, 
                ADVERB_TAG, ADVERB_COMPARATIVE_TAG, ADVERB_SUPERLATIVE_TAG, PARTICLE_TAG, SYMBOL_TAG, 
                TO_TAG, INTERJECTION_TAG, VERB_BASE_FORM_TAG, VERB_PAST_TENSE_TAG, 
                VERB_GERUND_OR_PRESENT_PARTICIPLE_TAG, VERB_PAST_PARTICIPLE_TAG, 
                VERB_NON_3RD_PERSON_SINGULAR_PRESENT_TAG, VERB_3RD_PERSON_SINGULAR_PRESENT_TAG, 
                VERB_3RD_PERSON_SINGULAR_PRESENT_TAG, WH_DETERMINER_TAG, WH_PRONOUN_TAG, 
                POSSESSIVE_WH_PRONOUN_TAG, WH_ADVERB_TAG])
_POS_TAG_TO_INDEX_MAP = {(v,i) for i,v in enumerate(POS_TAG_LIST)}
def get_penn_treebank_POS_tag_index(POS_tag):
    """ Returns the Penn treeBank POS tag index value associated to the input.
    
    Args:
        POS_tag: a string, representing a Penn TreeBank POS tag
    
    Returns:
        an int
    """
    return _POS_TAG_TO_INDEX_MAP[POS_tag] +1


# Create lists / sets to be used to define attributes of the Tokens
NN_POS_TAGS = [NOUN_SINGULAR_OR_MASS_TAG, NOUN_PLURAL_TAG]
COMMON_NOUN_POS_TAGS = [NOUN_SINGULAR_OR_MASS_TAG, NOUN_PLURAL_TAG]
NAME_POS_TAGS = [PROPER_NOUN_SINGULAR_TAG, PROPER_NOUN_PLURAL_TAG]
SINGULAR_NOUN_TAGS = [NOUN_SINGULAR_OR_MASS_TAG, PROPER_NOUN_SINGULAR_TAG]
PLURAL_NOUN_TAGS = [NOUN_PLURAL_TAG, PROPER_NOUN_PLURAL_TAG]
NOUN_POS_TAGS = COMMON_NOUN_POS_TAGS + NAME_POS_TAGS

RELATIVE_PRONOUN_POS_TAGS = set([WH_PRONOUN_TAG, POSSESSIVE_WH_PRONOUN_TAG]) # WH_DETERMINER_TAG?, WH_PRONOUN_TAG?, POSSESSIVE_WH_PRONOUN_TAG?

VERBS_POS_TAGS = set([VERB_BASE_FORM_TAG, VERB_GERUND_OR_PRESENT_PARTICIPLE_TAG, VERB_PAST_TENSE_TAG, 
                 VERB_PAST_PARTICIPLE_TAG, VERB_NON_3RD_PERSON_SINGULAR_PRESENT_TAG, 
                 VERB_3RD_PERSON_SINGULAR_PRESENT_TAG])


START_QUOTE_POS_TAG = "``"
END_QUOTE_POS_TAG = "''"
