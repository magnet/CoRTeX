# -*- coding: utf-8 -*-


"""
Defines the syntactic tags used by the 'Ontonotes v5 corpus to qualify its English documents.

Those annotations are notably documented in the 'OntoNotes Release 5.0' guidelines.

This notably means that:

  - the POS tags and parse tags used are those defined for the annotation of the English Penn Treebank, 
    as documented respectively by the 'Part-of-Speech Tagging Guidelines for the Penn treebank project (3rd Revision, 2nd printing)' 
    and the 'Bracketing Guidelines for Treebank II Style Penn Treebank Project' documents, completed by 
    the 'Addendum to the Penn Treebank II Style Bracketing Guidelines - BioMedical Treebank Annotation' 
    document
  - the Predicate Arguments tags used are those defined for the annotation of the English PropBank, as 
    documented by the 'PropBank Annotation Guidelines' document
  - the parse annotation and the predicate argument annotation had to be modified / merged so as to be 
    consistent with one another, which is documented by the 'Issues in Synchronizing the English Treebank and PropBank' 
    article, regarding technical matters, in the 'Treebank Changes Made for the Treebank/PropBank Merge - Ann Taylor - 2006-09-19' 
    document
"""

###########
# Summary #
###########
############################################
# Bracket Labels # (a.k.a. Syntactic tags) #
############################################
# Clause Level
SIMPLE_DECLARATIVE_CLAUSE_TAG           = "S" # Simple declarative clause. See source for more
SUBORDINATING_CONJUNCTION_CLAUSE_TAG    = "SBAR" # Clause introduced by a (possibly empty) subordinating conjunction.
DIRECT_QUESTION_TAG                     = "SBARQ"  # Direct question introduced by a wh-word or a wh-phrase. See source for more
INVERTED_DECLARATIVE_SENTENCE_TAG       = "SINV" # Inverted declarative sentence, i.e. one in which the subject follows the tensed verb or modal.
INVERTED_CLOSED_QUESTION_TAG            = "SQ" # Inverted yes/no question, or main clause of a wh-question, following the wh-phrase in SBARQ.

ROOT_TAG                                = "TOP" # Indicate the root of the parsing tree
NOT_PARSED_TAG                          = "NOPARSE" # No syntactic parsing could be carried out
SENTENCE_TAGS = set((SIMPLE_DECLARATIVE_CLAUSE_TAG, SUBORDINATING_CONJUNCTION_CLAUSE_TAG, DIRECT_QUESTION_TAG, INVERTED_DECLARATIVE_SENTENCE_TAG, INVERTED_CLOSED_QUESTION_TAG))
ROOT_TAGS = set((ROOT_TAG, "ROOT_TAG"))

# Phrase Level
ADJECTIVE_PHRASE_TAG                    = "ADJP" # Adjective Phrase.
ADVERB_PHRASE_TAG                       = "ADVP" # Adverb Phrase.
CONJUNCTION_PHRASE_TAG                  = "CONJP" # Conjunction Phrase.
EMBEDED_PHRASE_TAG                      = "EMBED"  # Used to qualify a word or group of words that is not part of the current sentence (for example, in the transcript of an audio conversation, the transcript may contain an interjection spoken by the listener, such as 'hum', or 'yeah', 'go on', 'ok'...)
FRAGMENT_TAG                            = "FRAG" # Fragment.
INTERJECTION_TAG                        = "INTJ" # Interjection. Corresponds approximately to the part-of-speech tag UH.
LIST_MARKER_TAG                         = "LST" # List marker. Includes surrounding punctuation.
META_PHRASE_TAG                         = "META" # A bit like EMBED, except it is about non-vocal or contextual info surrounding the speech being transcribed, such as 'reporter', or 'applause'
NOT_A_CONSTITUENT_TAG                   = "NAC" # Not a Constituent; used to show the scope of certain prenominal modifiers within an NP.
INTERNAL_NP_PHRASE_TAG                  = "NML" # Used  to capture internal NP structure, ex: (NP (NML (DT a) (NN couple)) (NN comments)); (NP (DT this) (NML (NNP al) (HYPH -) (NNP Jazeera)) (NN TV) (NN station)); (NP (DT the) (NML (CD four) (NNS islands)) (NN issue))
NOUN_PHRASE_TAG                         = "NP" # Noun Phrase.
COMPLEX_NP_HEAD_TAG                     = "NX" # Used within certain complex NPs to mark the head of the NP. See source for more
PREPOSITIONAL_PHRASE_TAG                = "PP" # Prepositional Phrase.
PARENTHETICAL_TAG                       = "PRN" # Parenthetical.
PARTICLE_TAG                            = "PRT" # Particle. Category for words that should be tagged RP.
QUANTIFIER_PHRASE_TAG                   = "QP" # Quantifier Phrase (i.e. complex measure/amount phrase); used within NP.
REDUCED_RELATIVE_CLAUSE_TAG             = "RRC" # Reduced Relative Clause.
UNLIKE_COORDINATED_PHRASE_TAG           = "UCP" # Unlike Coordinated Phrase.
VERB_PHRASE_TAG                         = "VP" # Verb Phrase.
WH_ADJECTIVE_PHRASE_TAG                 = "WHADJP" # Wh-adjective Phrase. Adjectival phrase containing a wh-adverb, as in how hot.
WH_ADVERB_PHRASE_TAG                    = "WHADVP" # Wh-adverb Phrase. Introduces a clause with an NP gap. See source for more
WH_NOUN_PHRASE_TAG                      = "WHNP" # Wh-noun Phrase. Introduces a clause with an NP gap. See source for more
WH_PREPOSITIONAL_PHRASE_TAG             = "WHPP" # Wh-prepositional Phrase. See source for more
UNKNOWN_TAG                             = "X" # Unknown, uncertain, or unbracketable. See source for more

# Word level, i.e. POS tags
CURRENCY_SYMBOL_TAG                             = "$" # Represent of currency symbol, such as $, €, ¥, £...
START_QUOTE_POS_TAG                             = "``" # START_QUOTE_POS_TAG
END_QUOTE_POS_TAG                               = "''" # END_QUOTE_POS_TAG
COMMA_TAG                                       = "," # Represent the role of introducing a slight pause in the speech, or of acting as a separator inside an enumeration
OPENING_PARENTHESIS_TAG                         = "-LRB-" # Represent the role of opening a parenthesis (or em dash — such as this one)
CLOSING_PARENTHESIS_TAG                         = "-RRB-" # Represent the role of ending a parenthesis (or em dash — such as this one)
SENTENCE_CLOSER_TAG                             = "." # Represent the role of ending a sentence
COLON_TAG                                       = ":" # Represent the role of stopping the speech in order to specify, define or give an example of what has just been said  
COORDINATING_CONJUNCTION_TAG                    = "CC" # Coordinating conjunction
CARDINAL_NUMBER_TAG                             = "CD" # Cardinal number
DETERMINER_TAG                                  = "DT" # Determiner
EXISTENTIAL_THERE_TAG                           = "EX" # Existential there
FOREIGN_WORD_TAG                                = "FW" # Foreign word
HYPHEN_TAG                                      = "HYPH" # Represent the role taken usually by a '-' character, which is to separate the parts of a multi-token word (ex: anti-abortionists)
PREPOSITION_OR_SUBORDINATING_CONJUNCTION_TAG    = "IN" # Preposition or subordinating conjunction
ADJECTIVE_TAG                                   = "JJ" # Adjective
ADJECTIVE_COMPARATIVE_TAG                       = "JJR" # Adjective, comparative
ADJECTIVE_SUPERLATIVE_TAG                       = "JJS" # Adjective, superlative
LIST_ITEM_MARKER_TAG                            = "LS" # List item marker
MODAL_TAG                                       = "MD" # Modal
ELLIPSIS_TAG                                    = "NFP" # Represent the role taken by an ellipsis, such as '...'. Also used to qualify '\*' or '\*\*' symbols? An ellipsis can also have a "." tag if it ends a sentence.
NOUN_SINGULAR_OR_MASS_TAG                       = "NN" # Noun, singular or mass
NOUN_PLURAL_TAG                                 = "NNS" # Noun, plural
PROPER_NOUN_SINGULAR_TAG                        = "NNP" # Proper noun, singular
PROPER_NOUN_PLURAL_TAG                          = "NNPS" # Proper noun, plural
PREDETERMINER_TAG                               = "PDT" # Predeterminer
POSSESSIVE_ENDING_TAG                           = "POS" # Possessive ending
PERSONAL_PRONOUN_TAG                            = "PRP" # Personal pronoun
POSSESSIVE_EXPANDED_PRONOUN_TAG                 = "PRP$" # Possessive pronoun (prolog version PRP-S) and possessive determiner
ADVERB_TAG                                      = "RB" # Adverb
ADVERB_COMPARATIVE_TAG                          = "RBR" # Adverb, comparative
ADVERB_SUPERLATIVE_TAG                          = "RBS" # Adverb, superlative
PARTICLE_TAG                                    = "RP" # Particle
SYMBOL_TAG                                      = "SYM" # Symbol
TO_TAG                                          = "TO" # to
INTERJECTION_TAG                                = "UH" # Interjection
VERB_BASE_FORM_TAG                              = "VB" # Verb, base form
VERB_PAST_TENSE_TAG                             = "VBD" # Verb, past tense
VERB_GERUND_OR_PRESENT_PARTICIPLE_TAG           = "VBG" # Verb, gerund or present participle
VERB_PAST_PARTICIPLE_TAG                        = "VBN" # Verb, past participle
VERB_NON_3RD_PERSON_SINGULAR_PRESENT_TAG        = "VBP" # Verb, non-3rd person singular present
VERB_3RD_PERSON_SINGULAR_PRESENT_TAG            = "VBZ"# Verb, 3rd person singular present
WH_DETERMINER_TAG                               = "WDT" # Wh-determiner
WH_PRONOUN_TAG                                  = "WP" # Wh-pronoun
POSSESSIVE_WH_PRONOUN_TAG                       = "WP$" # Possessive wh-pronoun (prolog version WP-S)
WH_ADVERB_TAG                                   = "WRB" # Wh-adverb
UNKNOWN_OR_TRUNCATED_WORD_TAG                   = "XX" # To signal a word that is incomplete (notably if a person is interrupted mid-word) or maybe does not exist (ex: 'to see if they could find evidence of wh-')



######################
# Using the resource #
######################
SYNTACTIC_TAGS = set((NOT_PARSED_TAG,
                      SIMPLE_DECLARATIVE_CLAUSE_TAG,
                      SUBORDINATING_CONJUNCTION_CLAUSE_TAG,
                      DIRECT_QUESTION_TAG,
                      INVERTED_DECLARATIVE_SENTENCE_TAG,
                      INVERTED_CLOSED_QUESTION_TAG,
                      ROOT_TAG,
                      ADJECTIVE_PHRASE_TAG,
                      ADVERB_PHRASE_TAG,
                      CONJUNCTION_PHRASE_TAG,
                      EMBEDED_PHRASE_TAG,
                      FRAGMENT_TAG,
                      INTERJECTION_TAG,
                      LIST_MARKER_TAG,
                      META_PHRASE_TAG,
                      NOT_A_CONSTITUENT_TAG,
                      INTERNAL_NP_PHRASE_TAG,
                      NOUN_PHRASE_TAG,
                      COMPLEX_NP_HEAD_TAG,
                      PREPOSITIONAL_PHRASE_TAG,
                      PARENTHETICAL_TAG,
                      PARTICLE_TAG,
                      QUANTIFIER_PHRASE_TAG,
                      REDUCED_RELATIVE_CLAUSE_TAG,
                      UNLIKE_COORDINATED_PHRASE_TAG,
                      VERB_PHRASE_TAG,
                      WH_ADJECTIVE_PHRASE_TAG,
                      WH_ADVERB_PHRASE_TAG,
                      WH_NOUN_PHRASE_TAG,
                      WH_PREPOSITIONAL_PHRASE_TAG,
                      UNKNOWN_TAG
                    ))

POS_TAGS = set((CURRENCY_SYMBOL_TAG,
                START_QUOTE_POS_TAG,
                END_QUOTE_POS_TAG,
                COMMA_TAG,
                OPENING_PARENTHESIS_TAG,
                CLOSING_PARENTHESIS_TAG,
                SENTENCE_CLOSER_TAG,
                COLON_TAG,
                COORDINATING_CONJUNCTION_TAG,
                CARDINAL_NUMBER_TAG,
                DETERMINER_TAG,
                EXISTENTIAL_THERE_TAG,
                FOREIGN_WORD_TAG,
                HYPHEN_TAG,
                PREPOSITION_OR_SUBORDINATING_CONJUNCTION_TAG,
                ADJECTIVE_TAG,
                ADJECTIVE_COMPARATIVE_TAG,
                ADJECTIVE_SUPERLATIVE_TAG,
                LIST_ITEM_MARKER_TAG,
                MODAL_TAG,
                ELLIPSIS_TAG,
                NOUN_SINGULAR_OR_MASS_TAG,
                NOUN_PLURAL_TAG,
                PROPER_NOUN_SINGULAR_TAG,
                PROPER_NOUN_PLURAL_TAG,
                PREDETERMINER_TAG,
                POSSESSIVE_ENDING_TAG,
                PERSONAL_PRONOUN_TAG,
                POSSESSIVE_EXPANDED_PRONOUN_TAG,
                ADVERB_TAG,
                ADVERB_COMPARATIVE_TAG,
                ADVERB_SUPERLATIVE_TAG,
                PARTICLE_TAG,
                SYMBOL_TAG,
                TO_TAG,
                INTERJECTION_TAG,
                VERB_BASE_FORM_TAG,
                VERB_PAST_TENSE_TAG,
                VERB_GERUND_OR_PRESENT_PARTICIPLE_TAG,
                VERB_PAST_PARTICIPLE_TAG,
                VERB_NON_3RD_PERSON_SINGULAR_PRESENT_TAG,
                VERB_3RD_PERSON_SINGULAR_PRESENT_TAG,
                WH_DETERMINER_TAG,
                WH_PRONOUN_TAG,
                POSSESSIVE_WH_PRONOUN_TAG,
                WH_ADVERB_TAG,
                UNKNOWN_OR_TRUNCATED_WORD_TAG
                ))

CONSTITUENT_TAGS = SYNTACTIC_TAGS.union(POS_TAGS)

# Create lists / sets to be used to define attributes of the Tokens
NN_POS_TAGS = [NOUN_SINGULAR_OR_MASS_TAG, NOUN_PLURAL_TAG]
COMMON_NOUN_POS_TAGS = [NOUN_SINGULAR_OR_MASS_TAG, NOUN_PLURAL_TAG]
NAME_POS_TAGS = [PROPER_NOUN_SINGULAR_TAG, PROPER_NOUN_PLURAL_TAG]
SINGULAR_NOUN_TAGS = [NOUN_SINGULAR_OR_MASS_TAG, PROPER_NOUN_SINGULAR_TAG]
PLURAL_NOUN_TAGS = [NOUN_PLURAL_TAG, PROPER_NOUN_PLURAL_TAG]
NOUN_POS_TAGS = COMMON_NOUN_POS_TAGS + NAME_POS_TAGS

RELATIVE_PRONOUN_POS_TAGS = set([WH_PRONOUN_TAG, POSSESSIVE_WH_PRONOUN_TAG])

VERBS_POS_TAGS = set([VERB_BASE_FORM_TAG, VERB_GERUND_OR_PRESENT_PARTICIPLE_TAG, VERB_PAST_TENSE_TAG, 
                     VERB_PAST_PARTICIPLE_TAG, VERB_NON_3RD_PERSON_SINGULAR_PRESENT_TAG, 
                     VERB_3RD_PERSON_SINGULAR_PRESENT_TAG])

PERSONAL_PRONOUN_TAGS = set([PERSONAL_PRONOUN_TAG])

NOUN_PHRASE_TAGS = set((NOUN_PHRASE_TAG,))
PREPOSITIONAL_PHRASE_TAGS = set((PREPOSITIONAL_PHRASE_TAG,))
VERB_PHRASE_TAGS = set((VERB_PHRASE_TAG,))
