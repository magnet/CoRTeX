# -*- coding: utf-8 -*- = "-"

"""
Defines the predicate argument tags used by the 'Ontonotes v5 corpus to qualify its English documents

Those annotations are notably documented in the 'OntoNotes Release 5.0' guidelines.

This notably means that:

  * the POS tags and parse tags used are those defined for the annotation of the English Penn Treebank, 
    as documented respectively by the 'Part-of-Speech Tagging Guidelines for the Penn treebank project (3rd Revision, 2nd printing)' 
    and the 'Bracketing Guidelines for Treebank II Style Penn Treebank Project' documents, completed by 
    the 'Addendum to the Penn Treebank II Style Bracketing Guidelines - BioMedical Treebank Annotation' 
    document
  * the Predicate Arguments tags used are those defined for the annotation of the English PropBank, as 
    documented by the 'PropBank Annotation Guidelines' document
  * the parse annotation and the predicate argument annotation had to be modified / merged so as to be 
    consistent with one another, which is documented by the 'Issues in Synchronizing the English Treebank and PropBank' 
    article, regarding technical matters, in the 'Treebank Changes Made for the Treebank/PropBank Merge - Ann Taylor - 2006-09-19' 
    document
"""

ARG0 = "ARG0"
ARG1 = "ARG1"
ARG2 = "ARG2"
ARG3 = "ARG3"
ARG4 = "ARG4"
ARG5 = "ARG5"
ARGA = "ARGA"
ARGM_ADJ = "ARGM-ADJ"
ARGM_ADV = "ARGM-ADV"
ARGM_CAU = "ARGM-CAU"
ARGM_COM = "ARGM-COM"
ARGM_DIR = "ARGM-DIR"
ARGM_DIS = "ARGM-DIS"
ARGM_DSP = "ARGM-DSP"
ARGM_EXT = "ARGM-EXT"
ARGM_GOL = "ARGM-GOL"
ARGM_LOC = "ARGM-LOC"
ARGM_LVB = "ARGM-LVB"
ARGM_MNR = "ARGM-MNR"
ARGM_MOD = "ARGM-MOD"
ARGM_NEG = "ARGM-NEG"
ARGM_PNC = "ARGM-PNC"
ARGM_PRD = "ARGM-PRD"
ARGM_PRP = "ARGM-PRP"
ARGM_PRR = "ARGM-PRR"
ARGM_PRX = "ARGM-PRX"
ARGM_REC = "ARGM-REC"
ARGM_TMP = "ARGM-TMP"
C_ARG0 = "C-ARG0"
C_ARG1 = "C-ARG1"
C_ARG2 = "C-ARG2"
C_ARG3 = "C-ARG3"
C_ARG4 = "C-ARG4"
C_ARGM_ADJ = "C-ARGM-ADJ"
C_ARGM_ADV = "C-ARGM-ADV"
C_ARGM_CAU = "C-ARGM-CAU"
C_ARGM_COM = "C-ARGM-COM"
C_ARGM_DIR = "C-ARGM-DIR"
C_ARGM_DIS = "C-ARGM-DIS"
C_ARGM_DSP = "C-ARGM-DSP"
C_ARGM_EXT = "C-ARGM-EXT"
C_ARGM_LOC = "C-ARGM-LOC"
C_ARGM_MNR = "C-ARGM-MNR"
C_ARGM_MOD = "C-ARGM-MOD"
C_ARGM_NEG = "C-ARGM-NEG"
C_ARGM_PNC = "C-ARGM-PNC"
C_ARGM_PRD = "C-ARGM-PRD"
C_ARGM_PRP = "C-ARGM-PRP"
C_ARGM_TMP = "C-ARGM-TMP"
R_ARG0 = "R-ARG0"
R_ARG1 = "R-ARG1"
R_ARG2 = "R-ARG2"
R_ARG3 = "R-ARG3"
R_ARG4 = "R-ARG4"
R_ARGM_ADV = "R-ARGM-ADV"
R_ARGM_CAU = "R-ARGM-CAU"
R_ARGM_COM = "R-ARGM-COM"
R_ARGM_DIR = "R-ARGM-DIR"
R_ARGM_DIS = "R-ARGM-DIS"
R_ARGM_EXT = "R-ARGM-EXT"
R_ARGM_GOL = "R-ARGM-GOL"
R_ARGM_LOC = "R-ARGM-LOC"
R_ARGM_MNR = "R-ARGM-MNR"
R_ARGM_MOD = "R-ARGM-MOD"
R_ARGM_PNC = "R-ARGM-PNC"
R_ARGM_PRD = "R-ARGM-PRD"
R_ARGM_PRP = "R-ARGM-PRP"
R_ARGM_TMP = "R-ARGM-TMP"
V = "V"

