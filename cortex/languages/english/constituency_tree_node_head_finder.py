# -*- coding: utf-8 -*-

"""
Defines utilities used to find the head child node of a ConstituencyTreeNode instance that was 
instantiated from English document data

Uses the "Ontonotes v5 modified Penna Treebank" set of constituent tags.

Head finding rules were described by Mr. Michael Collins in his Phd thesis:
"Head Driven Statistical Models for Natural Language Parsing"
"""

__all__ = ["CollinsHeadFinder",
           ]

from ..common.constituency_tree_node_head_finder import _BaseRulesConstituencyTreeNodeHeadFinder
from .parameters import ontonotes_v5_constituent_tags as POS_tags_module
# Clause and syntactic tags
NOT_PARSED_TAG                          = POS_tags_module.NOT_PARSED_TAG
SIMPLE_DECLARATIVE_CLAUSE_TAG           = POS_tags_module.SIMPLE_DECLARATIVE_CLAUSE_TAG
SUBORDINATING_CONJUNCTION_CLAUSE_TAG    = POS_tags_module.SUBORDINATING_CONJUNCTION_CLAUSE_TAG
DIRECT_QUESTION_TAG                     = POS_tags_module.DIRECT_QUESTION_TAG
INVERTED_DECLARATIVE_SENTENCE_TAG       = POS_tags_module.INVERTED_DECLARATIVE_SENTENCE_TAG
INVERTED_CLOSED_QUESTION_TAG            = POS_tags_module.INVERTED_CLOSED_QUESTION_TAG
ROOT_TAG                                = POS_tags_module.ROOT_TAG
ADJECTIVE_PHRASE_TAG                    = POS_tags_module.ADJECTIVE_PHRASE_TAG
ADVERB_PHRASE_TAG                       = POS_tags_module.ADVERB_PHRASE_TAG
CONJUNCTION_PHRASE_TAG                  = POS_tags_module.CONJUNCTION_PHRASE_TAG
EMBEDED_PHRASE_TAG                      = POS_tags_module.EMBEDED_PHRASE_TAG
FRAGMENT_TAG                            = POS_tags_module.FRAGMENT_TAG
INTERJECTION_TAG                        = POS_tags_module.INTERJECTION_TAG
LIST_MARKER_TAG                         = POS_tags_module.LIST_MARKER_TAG
META_PHRASE_TAG                         = POS_tags_module.META_PHRASE_TAG
NOT_A_CONSTITUENT_TAG                   = POS_tags_module.NOT_A_CONSTITUENT_TAG
INTERNAL_NP_PHRASE_TAG                  = POS_tags_module.INTERNAL_NP_PHRASE_TAG
NOUN_PHRASE_TAG                         = POS_tags_module.NOUN_PHRASE_TAG
COMPLEX_NP_HEAD_TAG                     = POS_tags_module.COMPLEX_NP_HEAD_TAG
PREPOSITIONAL_PHRASE_TAG                = POS_tags_module.PREPOSITIONAL_PHRASE_TAG
PARENTHETICAL_TAG                       = POS_tags_module.PARENTHETICAL_TAG
PARTICLE_TAG                            = POS_tags_module.PARTICLE_TAG
QUANTIFIER_PHRASE_TAG                   = POS_tags_module.QUANTIFIER_PHRASE_TAG
REDUCED_RELATIVE_CLAUSE_TAG             = POS_tags_module.REDUCED_RELATIVE_CLAUSE_TAG
UNLIKE_COORDINATED_PHRASE_TAG           = POS_tags_module.UNLIKE_COORDINATED_PHRASE_TAG
VERB_PHRASE_TAG                         = POS_tags_module.VERB_PHRASE_TAG
WH_ADJECTIVE_PHRASE_TAG                 = POS_tags_module.WH_ADJECTIVE_PHRASE_TAG
WH_ADVERB_PHRASE_TAG                    = POS_tags_module.WH_ADVERB_PHRASE_TAG
WH_NOUN_PHRASE_TAG                      = POS_tags_module.WH_NOUN_PHRASE_TAG
WH_PREPOSITIONAL_PHRASE_TAG             = POS_tags_module.WH_PREPOSITIONAL_PHRASE_TAG
UNKNOWN_TAG                             = POS_tags_module.UNKNOWN_TAG

# POS tags
CURRENCY_SYMBOL_TAG                             = POS_tags_module.CURRENCY_SYMBOL_TAG
START_QUOTE_POS_TAG                             = POS_tags_module.START_QUOTE_POS_TAG
END_QUOTE_POS_TAG                               = POS_tags_module.END_QUOTE_POS_TAG
COMMA_TAG                                       = POS_tags_module.COMMA_TAG
OPENING_PARENTHESIS_TAG                         = POS_tags_module.OPENING_PARENTHESIS_TAG
CLOSING_PARENTHESIS_TAG                         = POS_tags_module.CLOSING_PARENTHESIS_TAG
SENTENCE_CLOSER_TAG                             = POS_tags_module.SENTENCE_CLOSER_TAG
COLON_TAG                                       = POS_tags_module.COLON_TAG
COORDINATING_CONJUNCTION_TAG                    = POS_tags_module.COORDINATING_CONJUNCTION_TAG
CARDINAL_NUMBER_TAG                             = POS_tags_module.CARDINAL_NUMBER_TAG
DETERMINER_TAG                                  = POS_tags_module.DETERMINER_TAG
EXISTENTIAL_THERE_TAG                           = POS_tags_module.EXISTENTIAL_THERE_TAG
FOREIGN_WORD_TAG                                = POS_tags_module.FOREIGN_WORD_TAG
HYPHEN_TAG                                      = POS_tags_module.HYPHEN_TAG
PREPOSITION_OR_SUBORDINATING_CONJUNCTION_TAG    = POS_tags_module.PREPOSITION_OR_SUBORDINATING_CONJUNCTION_TAG
ADJECTIVE_TAG                                   = POS_tags_module.ADJECTIVE_TAG
ADJECTIVE_COMPARATIVE_TAG                       = POS_tags_module.ADJECTIVE_COMPARATIVE_TAG
ADJECTIVE_SUPERLATIVE_TAG                       = POS_tags_module.ADJECTIVE_SUPERLATIVE_TAG
LIST_ITEM_MARKER_TAG                            = POS_tags_module.LIST_ITEM_MARKER_TAG
MODAL_TAG                                       = POS_tags_module.MODAL_TAG
ELLIPSIS_TAG                                    = POS_tags_module.ELLIPSIS_TAG
NOUN_SINGULAR_OR_MASS_TAG                       = POS_tags_module.NOUN_SINGULAR_OR_MASS_TAG
NOUN_PLURAL_TAG                                 = POS_tags_module.NOUN_PLURAL_TAG
PROPER_NOUN_SINGULAR_TAG                        = POS_tags_module.PROPER_NOUN_SINGULAR_TAG
PROPER_NOUN_PLURAL_TAG                          = POS_tags_module.PROPER_NOUN_PLURAL_TAG
PREDETERMINER_TAG                               = POS_tags_module.PREDETERMINER_TAG
POSSESSIVE_ENDING_TAG                           = POS_tags_module.POSSESSIVE_ENDING_TAG
PERSONAL_PRONOUN_TAG                            = POS_tags_module.PERSONAL_PRONOUN_TAG
POSSESSIVE_EXPANDED_PRONOUN_TAG                 = POS_tags_module.POSSESSIVE_EXPANDED_PRONOUN_TAG
ADVERB_TAG                                      = POS_tags_module.ADVERB_TAG
ADVERB_COMPARATIVE_TAG                          = POS_tags_module.ADVERB_COMPARATIVE_TAG
ADVERB_SUPERLATIVE_TAG                          = POS_tags_module.ADVERB_SUPERLATIVE_TAG
PARTICLE_TAG                                    = POS_tags_module.PARTICLE_TAG
SYMBOL_TAG                                      = POS_tags_module.SYMBOL_TAG
TO_TAG                                          = POS_tags_module.TO_TAG
INTERJECTION_TAG                                = POS_tags_module.INTERJECTION_TAG
VERB_BASE_FORM_TAG                              = POS_tags_module.VERB_BASE_FORM_TAG
VERB_PAST_TENSE_TAG                             = POS_tags_module.VERB_PAST_TENSE_TAG
VERB_GERUND_OR_PRESENT_PARTICIPLE_TAG           = POS_tags_module.VERB_GERUND_OR_PRESENT_PARTICIPLE_TAG
VERB_PAST_PARTICIPLE_TAG                        = POS_tags_module.VERB_PAST_PARTICIPLE_TAG
VERB_NON_3RD_PERSON_SINGULAR_PRESENT_TAG        = POS_tags_module.VERB_NON_3RD_PERSON_SINGULAR_PRESENT_TAG
VERB_3RD_PERSON_SINGULAR_PRESENT_TAG            = POS_tags_module.VERB_3RD_PERSON_SINGULAR_PRESENT_TAG
WH_DETERMINER_TAG                               = POS_tags_module.WH_DETERMINER_TAG
WH_PRONOUN_TAG                                  = POS_tags_module.WH_PRONOUN_TAG
POSSESSIVE_WH_PRONOUN_TAG                       = POS_tags_module.POSSESSIVE_WH_PRONOUN_TAG
WH_ADVERB_TAG                                   = POS_tags_module.WH_ADVERB_TAG
UNKNOWN_OR_TRUNCATED_WORD_TAG                   = POS_tags_module.UNKNOWN_OR_TRUNCATED_WORD_TAG

# FIXME: is it normal to mix syntactic and POS tags in the rules values? (ex: for NOUN_PHRASE_TAG: ("rightdis", (COMPLEX_NP_HEAD_TAG, ADJECTIVE_COMPARATIVE_TAG))
# Yes, we are talking about constituency tree nodes; a head is not always a token...
POSSIBLE_PRECEDENCE_DIRECTION = set(("left", "right", "leftdis", "rightdis", "leftexcept", "rightexcept"))
COLLINS_HEAD_FINDER_RULES = {NOUN_PHRASE_TAG:  (("rightdis", (NOUN_SINGULAR_OR_MASS_TAG, 
                                                          PROPER_NOUN_SINGULAR_TAG, 
                                                          PROPER_NOUN_PLURAL_TAG, NOUN_PLURAL_TAG, 
                                                          COMPLEX_NP_HEAD_TAG, ADJECTIVE_COMPARATIVE_TAG, #INTERNAL_NP_PHRASE_TAG,
                                                          )), 
                                            ("left", (NOUN_PHRASE_TAG,)), 
                                            ("rightdis", (CURRENCY_SYMBOL_TAG, ADJECTIVE_PHRASE_TAG, 
                                                          PARENTHETICAL_TAG)), 
                                            ("right", (CARDINAL_NUMBER_TAG,)), 
                                            ("rightdis", (ADJECTIVE_TAG, ADJECTIVE_SUPERLATIVE_TAG, 
                                                          ADVERB_TAG, QUANTIFIER_PHRASE_TAG))
                                            ),
                             
                             INTERNAL_NP_PHRASE_TAG:  (("rightdis", (NOUN_SINGULAR_OR_MASS_TAG, 
                                                                 PROPER_NOUN_SINGULAR_TAG, 
                                                                 PROPER_NOUN_PLURAL_TAG, 
                                                                 NOUN_PLURAL_TAG, COMPLEX_NP_HEAD_TAG, 
                                                                 ADJECTIVE_COMPARATIVE_TAG)), 
                                                   ("left", (NOUN_PHRASE_TAG,)), 
                                                   ("rightdis", (CURRENCY_SYMBOL_TAG, ADJECTIVE_PHRASE_TAG, 
                                                                 PARENTHETICAL_TAG)), 
                                                   ("right", (CARDINAL_NUMBER_TAG,)), 
                                                   ("rightdis", (ADJECTIVE_TAG, ADJECTIVE_SUPERLATIVE_TAG, 
                                                                 ADVERB_TAG, QUANTIFIER_PHRASE_TAG))
                                                   ),
                             
                             ADJECTIVE_PHRASE_TAG: (("left", (NOUN_PLURAL_TAG, QUANTIFIER_PHRASE_TAG, 
                                                          NOUN_SINGULAR_OR_MASS_TAG, 
                                                          CURRENCY_SYMBOL_TAG, ADVERB_PHRASE_TAG, 
                                                          ADJECTIVE_TAG, VERB_PAST_PARTICIPLE_TAG, 
                                                          VERB_GERUND_OR_PRESENT_PARTICIPLE_TAG, 
                                                          ADJECTIVE_PHRASE_TAG, ADJECTIVE_COMPARATIVE_TAG, 
                                                          NOUN_PHRASE_TAG, ADJECTIVE_SUPERLATIVE_TAG, 
                                                          DETERMINER_TAG, FOREIGN_WORD_TAG, 
                                                          ADVERB_COMPARATIVE_TAG, ADVERB_SUPERLATIVE_TAG, 
                                                          SUBORDINATING_CONJUNCTION_CLAUSE_TAG, ADVERB_TAG)),),
                             
                             "JJP": (("left", (NOUN_PLURAL_TAG, QUANTIFIER_PHRASE_TAG, 
                                               NOUN_SINGULAR_OR_MASS_TAG, CURRENCY_SYMBOL_TAG, 
                                               ADVERB_PHRASE_TAG, ADJECTIVE_TAG, VERB_PAST_PARTICIPLE_TAG, 
                                               VERB_GERUND_OR_PRESENT_PARTICIPLE_TAG, ADJECTIVE_PHRASE_TAG, 
                                               ADJECTIVE_COMPARATIVE_TAG, NOUN_PHRASE_TAG, 
                                               ADJECTIVE_SUPERLATIVE_TAG, DETERMINER_TAG, 
                                               FOREIGN_WORD_TAG, ADVERB_COMPARATIVE_TAG, 
                                               ADVERB_SUPERLATIVE_TAG, SUBORDINATING_CONJUNCTION_CLAUSE_TAG, 
                                               ADVERB_TAG)),),
                             ADVERB_PHRASE_TAG: (("right", (ADVERB_TAG, ADVERB_COMPARATIVE_TAG, 
                                                        ADVERB_SUPERLATIVE_TAG, FOREIGN_WORD_TAG, 
                                                        ADVERB_PHRASE_TAG, TO_TAG, CARDINAL_NUMBER_TAG, 
                                                        ADJECTIVE_COMPARATIVE_TAG, ADJECTIVE_TAG, 
                                                        PREPOSITION_OR_SUBORDINATING_CONJUNCTION_TAG, 
                                                        NOUN_PHRASE_TAG, ADJECTIVE_SUPERLATIVE_TAG, 
                                                        NOUN_SINGULAR_OR_MASS_TAG)),),
                             CONJUNCTION_PHRASE_TAG: (("right", (COORDINATING_CONJUNCTION_TAG, ADVERB_TAG, 
                                                             PREPOSITION_OR_SUBORDINATING_CONJUNCTION_TAG)),),
                             FRAGMENT_TAG: (("right", tuple()),),
                             INTERJECTION_TAG: (("left", tuple()),),
                             LIST_MARKER_TAG: (("right", (LIST_ITEM_MARKER_TAG, COMMA_TAG)),),
                             NOT_A_CONSTITUENT_TAG: (("left", (NOUN_SINGULAR_OR_MASS_TAG, NOUN_PLURAL_TAG, 
                                                           PROPER_NOUN_SINGULAR_TAG, 
                                                           PROPER_NOUN_PLURAL_TAG, NOUN_PHRASE_TAG, 
                                                           NOT_A_CONSTITUENT_TAG, EXISTENTIAL_THERE_TAG, 
                                                           CURRENCY_SYMBOL_TAG, CARDINAL_NUMBER_TAG, 
                                                           QUANTIFIER_PHRASE_TAG, PERSONAL_PRONOUN_TAG, 
                                                           VERB_GERUND_OR_PRESENT_PARTICIPLE_TAG, 
                                                           ADJECTIVE_TAG, ADJECTIVE_SUPERLATIVE_TAG, 
                                                           ADJECTIVE_COMPARATIVE_TAG, ADJECTIVE_PHRASE_TAG, 
                                                           FOREIGN_WORD_TAG)),),
                             COMPLEX_NP_HEAD_TAG: (("left", tuple()),),
                             PREPOSITIONAL_PHRASE_TAG: (("right", (PREPOSITION_OR_SUBORDINATING_CONJUNCTION_TAG, 
                                                               TO_TAG, VERB_GERUND_OR_PRESENT_PARTICIPLE_TAG, 
                                                               VERB_PAST_PARTICIPLE_TAG, PARTICLE_TAG, 
                                                               FOREIGN_WORD_TAG)),),
                             PARENTHETICAL_TAG: (("left", tuple()),),
                             PARTICLE_TAG: (("right", (PARTICLE_TAG,)),),
                             QUANTIFIER_PHRASE_TAG: (("left", (CURRENCY_SYMBOL_TAG, 
                                                           PREPOSITION_OR_SUBORDINATING_CONJUNCTION_TAG, 
                                                           NOUN_PLURAL_TAG, NOUN_SINGULAR_OR_MASS_TAG, 
                                                           ADJECTIVE_TAG, ADVERB_TAG, DETERMINER_TAG, 
                                                           CARDINAL_NUMBER_TAG, "NCD", QUANTIFIER_PHRASE_TAG, 
                                                           ADJECTIVE_COMPARATIVE_TAG, 
                                                           ADJECTIVE_SUPERLATIVE_TAG)),),
                             REDUCED_RELATIVE_CLAUSE_TAG: (("right", (VERB_PHRASE_TAG, NOUN_PHRASE_TAG, 
                                                                  ADVERB_PHRASE_TAG, ADJECTIVE_PHRASE_TAG, 
                                                                  PREPOSITIONAL_PHRASE_TAG)),),
                             SIMPLE_DECLARATIVE_CLAUSE_TAG: (("left", (TO_TAG, PREPOSITION_OR_SUBORDINATING_CONJUNCTION_TAG, 
                                                                   VERB_PHRASE_TAG, SIMPLE_DECLARATIVE_CLAUSE_TAG, 
                                                                   SUBORDINATING_CONJUNCTION_CLAUSE_TAG, 
                                                                   ADJECTIVE_PHRASE_TAG, UNLIKE_COORDINATED_PHRASE_TAG, 
                                                                   NOUN_PHRASE_TAG)),),
                             SUBORDINATING_CONJUNCTION_CLAUSE_TAG: (("left", (WH_NOUN_PHRASE_TAG, 
                                                                          WH_PREPOSITIONAL_PHRASE_TAG, 
                                                                          WH_ADVERB_PHRASE_TAG, 
                                                                          ADJECTIVE_PHRASE_TAG, 
                                                                          PREPOSITION_OR_SUBORDINATING_CONJUNCTION_TAG, 
                                                                          DETERMINER_TAG, 
                                                                          SIMPLE_DECLARATIVE_CLAUSE_TAG, 
                                                                          INVERTED_CLOSED_QUESTION_TAG, 
                                                                          INVERTED_DECLARATIVE_SENTENCE_TAG, 
                                                                          SUBORDINATING_CONJUNCTION_CLAUSE_TAG, 
                                                                          FRAGMENT_TAG)),),
                             DIRECT_QUESTION_TAG: (("left", (INVERTED_CLOSED_QUESTION_TAG, SIMPLE_DECLARATIVE_CLAUSE_TAG, 
                                                         INVERTED_DECLARATIVE_SENTENCE_TAG, DIRECT_QUESTION_TAG, 
                                                         FRAGMENT_TAG)),),
                             INVERTED_DECLARATIVE_SENTENCE_TAG: (("left", (VERB_3RD_PERSON_SINGULAR_PRESENT_TAG, 
                                                                       VERB_PAST_TENSE_TAG, 
                                                                       VERB_NON_3RD_PERSON_SINGULAR_PRESENT_TAG, 
                                                                       VERB_BASE_FORM_TAG, MODAL_TAG, 
                                                                       VERB_PHRASE_TAG, SIMPLE_DECLARATIVE_CLAUSE_TAG, 
                                                                       INVERTED_DECLARATIVE_SENTENCE_TAG, 
                                                                       ADJECTIVE_PHRASE_TAG, NOUN_PHRASE_TAG)),),
                             INVERTED_CLOSED_QUESTION_TAG: (("left", (VERB_3RD_PERSON_SINGULAR_PRESENT_TAG, 
                                                                  VERB_PAST_TENSE_TAG, 
                                                                  VERB_NON_3RD_PERSON_SINGULAR_PRESENT_TAG, 
                                                                  VERB_BASE_FORM_TAG, MODAL_TAG, VERB_PHRASE_TAG, 
                                                                  INVERTED_CLOSED_QUESTION_TAG)),),
                             UNLIKE_COORDINATED_PHRASE_TAG: (("right", tuple()),),
                             VERB_PHRASE_TAG: (("left", (TO_TAG, VERB_PAST_TENSE_TAG, VERB_PAST_PARTICIPLE_TAG, 
                                                     MODAL_TAG, VERB_3RD_PERSON_SINGULAR_PRESENT_TAG, 
                                                     VERB_BASE_FORM_TAG, VERB_GERUND_OR_PRESENT_PARTICIPLE_TAG, 
                                                     VERB_NON_3RD_PERSON_SINGULAR_PRESENT_TAG, "AUX", "AUXG", 
                                                     VERB_PHRASE_TAG, ADJECTIVE_PHRASE_TAG, NOUN_SINGULAR_OR_MASS_TAG, 
                                                     NOUN_PLURAL_TAG, NOUN_PHRASE_TAG)),),
                             WH_ADJECTIVE_PHRASE_TAG: (("left", (COORDINATING_CONJUNCTION_TAG, WH_ADVERB_TAG, 
                                                             ADJECTIVE_TAG, ADJECTIVE_PHRASE_TAG)),),
                             WH_ADVERB_PHRASE_TAG: (("right", (COORDINATING_CONJUNCTION_TAG, WH_ADVERB_TAG)),),
                             WH_NOUN_PHRASE_TAG: (("left", (WH_DETERMINER_TAG, WH_PRONOUN_TAG, 
                                                        POSSESSIVE_WH_PRONOUN_TAG, ADJECTIVE_PHRASE_TAG, 
                                                        WH_PREPOSITIONAL_PHRASE_TAG, WH_NOUN_PHRASE_TAG)),),
                             WH_PREPOSITIONAL_PHRASE_TAG: (("right", (PREPOSITION_OR_SUBORDINATING_CONJUNCTION_TAG, 
                                                                  TO_TAG, FOREIGN_WORD_TAG)),),
                             UNKNOWN_TAG: (("right", tuple()),),
                             "TYPO": (("left", tuple()),),
                             "EDITED": (("left", tuple()),),
                             "XS": (("right", (PREPOSITION_OR_SUBORDINATING_CONJUNCTION_TAG,)),)
                             #,"NOPARSE": (("left", tuple()),) 
                             }


class CollinsHeadFinder(_BaseRulesConstituencyTreeNodeHeadFinder):
    """ Class whose instance shall be used to find the head child node of a ConstituencyTreeNode 
    instance defined on a document written in English, using  Collins' rules. 
    
    Cf this module's docstring.
    
    That means the document, whose ConstituencyTreeNode instance this class will be used for, must 
    have been annotated in constituent using the "Ontonotes v5 modified Penn Treebank" tagset.
    
    Here, a rule is a (precedence direction; rule elements) pair, 
    where:
        - 'rule elements' is a collection of rule elements, which are syntactic tag values
        - 'precedence direction' is a string, specifying the order in which to iterate over (rule element; potential head) 
          pairs, as well as how to interpret the rule, when applying a rule on a node in order to 
          determine its head child node.
          It can take the following values:
              - 'left': iterate first over the rule elements (so, slower), and then over the potential 
                head nodes (so, faster), in the nodes' reading order. The first node which matches the rule 
                element is considered to be the head node, and the search stops.
              - 'leftdis': iterate first over the potential head nodes (so, slower), and then over the 
                rule elements (so, faster), in the nodes' reading order. The first node which matches the 
                rule element is considered to be the head node, and the search stops.
              - 'right': iterate first over the rule elements (so, slower), and then over the potential 
                head nodes (so, faster), in the inverse of the nodes' reading order. The first node which 
                matches the rule element is considered to be the head node, and the search stops.
              - 'rightdis': iterate first over the potential head nodes (so, slower), and then over the 
                rule elements (so, faster), in the inverse of the nodes' reading order
              - 'leftexcept': iterate first over the potential head nodes (so, slower), and then over the 
                rule elements (so, faster), in the nodes' reading order. The first node which matches 
                absolutely no rule element is considered to be the head node, and the search stops.
              - 'rightexcept': iterate first over the potential head nodes (so, slower), and then over the 
                rule elements (so, faster), in the inverse of the nodes' reading order. The first node which 
                matches absolutely no rule element is considered to be the head node, and the search stops.
    
    Attributes:
        RULES: a "syntactic tag value => rule" map corresponding to the Collins' rules.
    """
    
    _RULES = COLLINS_HEAD_FINDER_RULES

    @classmethod
    def _get_head_from_rule(cls, node, rule, last_resort):
        """ Finds the head node of a list of children, by carrying out a linear search using the 
        provided rule.
        
        Args:
            node: a ConstituencyTreeNode instance
            rule: the rule to use
            last_resort: boolean; if True, then if the search carried out using the input rule 
                did not turn out a result, then a final, default rule will be used; if False, then if the 
                search carried out using the input rule did not turn out a result, then None will be returned.

        Returns:
            a ConstituencyTreeNode instance, or None
        """
        children = node.children
        if len(children) == 0:
            raise ValueError("The node has no children to perform the head search on.")
        
        found_head_index = 0
        found = False
        precedence_direction = rule[0]
        rule_syntactic_tags = rule[1]
        if precedence_direction not in POSSIBLE_PRECEDENCE_DIRECTION:
            raise ValueError("Incorrect precedence_direction value ('{}'), possible values are among: {}."\
                             .format(precedence_direction, POSSIBLE_PRECEDENCE_DIRECTION))
        
        if precedence_direction == "left":
            # Return the first node that respects at least one rule; reading first the RULES, then the children (from left to right)
            for rule_synt_tag in rule_syntactic_tags:
                for node_index, node in enumerate(children):
                    if rule_synt_tag == node.syntactic_tag:
                        found_head_index = node_index
                        found = True
                        break
                if found:
                    break
        
        elif precedence_direction == "leftdis":
            # Return the first node that respects at least one rule; reading first the children (from left to right), then the RULES
            for node_index, node in enumerate(children):
                syntactic_tag = node.syntactic_tag
                for rule_synt_tag in rule_syntactic_tags:
                    if rule_synt_tag == syntactic_tag:
                        found_head_index = node_index
                        found = True
                        break
                if found:
                    break
        
        elif precedence_direction == "right":
            # Return the first node that respects at least one rule; reading first the RULES, then the children (from right to left)
            for rule_synt_tag in rule_syntactic_tags:
                for node_index in range(len(children)-1, -1, -1):
                    node = children[node_index]
                    if rule_synt_tag == node.syntactic_tag:
                        found_head_index = node_index
                        found = True
                        break
                if found:
                    break
        
        elif precedence_direction == "rightdis":
            # Return the first node that respects at least one rule; reading first the children (from right to left), then the RULES
            for node_index in range(len(children)-1, -1, -1):
                node = children[node_index]
                syntactic_tag = node.syntactic_tag
                for rule_synt_tag in rule_syntactic_tags:
                    if rule_synt_tag == syntactic_tag:
                        found_head_index = node_index
                        found = True
                        break
                if found:
                    break
        
        elif precedence_direction == "leftexcept":
            for node_index, node in enumerate(children):
                syntactic_tag = node.syntactic_tag
                found = True
                for rule_synt_tag in rule_syntactic_tags:
                    if rule_synt_tag == syntactic_tag:
                        found = False
                        break
                if found:
                    found_head_index = node_index
                    break
        
        elif precedence_direction == "rightexcept":
            # Return the first node index whose node respects no RULES; reading first the heads (from right to left), then the RULES
            for node_index in range(len(children)-1, -1, -1):
                node = children[node_index]
                syntactic_tag = node.syntactic_tag
                found = True
                for rule_synt_tag in rule_syntactic_tags:
                    if rule_synt_tag == syntactic_tag:
                        found = False;
                        break
                if found:
                    found_head_index = node_index
                    break
        
        found_head = children[found_head_index]
        if not found: # What happens if our rule didn't match anything
            found_head = None
            if last_resort:
                # When parsing the sentence in the specified order, choose as head the first token whose tag does not belong to the following set.
                # Basically, anything that is not punctuation.
                tags = (END_QUOTE_POS_TAG, START_QUOTE_POS_TAG, OPENING_PARENTHESIS_TAG, 
                        CLOSING_PARENTHESIS_TAG, SENTENCE_CLOSER_TAG, COMMA_TAG, COMMA_TAG) # FIXME: why do we find the same "," tag twice?
                if str(precedence_direction).startswith("left"):
                    rule = ("leftexcept", tags)
                else:
                    rule = ("rightexcept", tags)
                found_head = cls._get_head_from_rule(node, rule, False)
        
        return found_head
