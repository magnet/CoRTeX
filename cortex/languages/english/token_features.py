# -*- coding: utf-8 -*-

"""
Defines a class used to compute data and values corresponding to grammatical information, also called 
grammatical features here, from instances of the Token class defined for document written in English.
"""

__all__ = ["EnglishTokenFeatures", 
           "ENGLISH_TOKEN_FEATURES",
           ]

from .knowledge import lexicon


class EnglishTokenFeatures(object):
    """ A class defining the grammatical features that can be interesting to know about a token 
    of a document written in English.
    
    Assumes that they are part of a document which was POS annotated with tags used to annotate the 
    documents of the 'Ontonotes v5' corpus.
    
    Attributes:
        POS_tags_module: the python module defining the constituency tree tags used to characterize 
            the constituency tree of the document which the mentions originate from (the 
            "ontonotes_v5_constituent_tags" module)
    Notes:
        No need to call 'lower' on the raw_text when trying to match regex, as the regex used here is 
        case insensitive.
    """
    
    def __init__(self):
        # Loading the resources to be used to define the 'feature' methods
        from cortex.languages.english.parameters import ontonotes_v5_constituent_tags
        self.POS_tags_module = ontonotes_v5_constituent_tags
    
    ## Instance methods
    ### Determiner
    def is_def_det(self, token): # FIXME: use lemma
        """ Determines whether ot not the input token corresponds to a definite determiner.
        
        Args:
            token: a Token instance

        Returns:
            a boolean
        """
        return token.POS_tag == self.POS_tags_module.DETERMINER_TAG \
            and lexicon.DEF_DET_REGEX.match(token.raw_text) is not None
    def is_indef_det(self, token):
        """ Determines whether ot not the input token corresponds to an indefinite determiner.
        
        Args:
            token: a Token instance

        Returns:
            a boolean
        """
        return token.POS_tag == self.POS_tags_module.DETERMINER_TAG \
            and lexicon.INDEF_DET_REGEX.match(token.raw_text) is not None
    def is_dem(self, token): # det or pro
        """ Determines whether ot not the input token corresponds to a demonstrative determiner or 
        pronoun.
        
        Args:
            token: a Token instance

        Returns:
            a boolean
        """
        return lexicon.DEM_PRO_REGEX.match(token.raw_text) is not None
    
    ### Pronoun
    def is_pers_pro(self, token):
        """ Determines whether ot not the input token corresponds to a personal pronoun.
        
        Args:
            token: a Token instance

        Returns:
            a boolean
        """
        return token.POS_tag in self.POS_tags_module.PERSONAL_PRONOUN_TAGS
    def is_expand_poss_pro(self, token):
        """ Determines whether ot not the input token corresponds to an expanded possessive pronoun.
        
        Args:
            token: a Token instance

        Returns:
            a boolean
        """
        return token.POS_tag == self.POS_tags_module.POSSESSIVE_EXPANDED_PRONOUN_TAG \
            or (token.POS_tag in self.POS_tags_module.NN_POS_TAGS \
                and token.raw_text.lower() in lexicon.EXPANDED_POSSESSIVE_PRONOUNS)
    
    ### Noun
    def is_common_noun(self, token):
        """ Determine whether ot not the input token corresponds to a common noun.
        
        Args:
            token: a Token instance

        Returns:
            a boolean
        """
        return token.POS_tag in self.POS_tags_module.COMMON_NOUN_POS_TAGS
    def is_name(self, token):
        """ Determine whether ot not the input token corresponds to a proper name (proper noun).
        
        Args:
            token: a Token instance

        Returns:
            a boolean
        """
        return token.POS_tag in self.POS_tags_module.NAME_POS_TAGS
    def is_noun(self, token):
        """ Determine whether ot not the input token corresponds to a noun (either proper or common).
        
        Args:
            token: a Token instance

        Returns:
            a boolean
        """
        return token.POS_tag in self.POS_tags_module.NOUN_POS_TAGS
    
    ### Verb
    def is_verb(self, token):
        """ Determine whether ot not the input token corresponds to a verb form.
        
        Args:
            token: a Token instance

        Returns:
            a boolean
        """
        return token.POS_tag in self.POS_tags_module.VERBS_POS_TAGS

    ### Misc.
    def is_start_quote(self, token):
        """ Determine whether ot not the input token corresponds to a starting quote.
        
        Args:
            token: a Token instance

        Returns:
            a boolean
        """
        return token.POS_tag == self.POS_tags_module.START_QUOTE_POS_TAG
    def is_end_quote(self, token):
        """ Determine whether ot not the input token corresponds to an ending quote.
        
        Args:
            token: a Token instance

        Returns:
            a boolean
        """
        return token.POS_tag == self.POS_tags_module.END_QUOTE_POS_TAG
    def is_the_CD_of(self, token1, token2, token3):
        """ Determine whether ot not the input token corresponds to a cardinal.
        
        Args:
            token: a Token instance

        Returns:
            a boolean
        """
        txt_3 = token1.raw_text.lower()
        pos_2 = token2.POS_tag
        txt_1 = token3.raw_text.lower()
        if txt_3 == "the" and pos_2 == self.POS_tags_module.CARDINAL_NUMBER_TAG and txt_1 == "of":
            return True
        return False

ENGLISH_TOKEN_FEATURES = EnglishTokenFeatures()
