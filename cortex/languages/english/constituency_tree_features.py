# -*- coding: utf-8 -*-

"""
Defines base classes and utilities used to compute data and values corresponding to grammatical 
information, also called grammatical features here, from instances of the ConstituencyTreeNode and 
of the ConstituencyTree classes, created from English document data.

Assume that the constituency tree tags used are the "Ontonotes v5 modified Penn Treebank" constituent 
tags, and the head finder used is the "Collins" rules based head finder.

Assume that trees are spread on their width rather than on their height (so no binary trees) - parse 
trees produced by the Stanford Core NLP parser should be fine.
"""

__all__ = ["EnglishConstituencyTreeNodeFeatures", 
           "EnglishConstituencyTreeFeatures",
           "ENGLISH_CONSTITUENCY_TREE_NODE_FEATURES", 
           "ENGLISH_CONSTITUENCY_TREE_FEATURES",
           "NODE_HEAD_FINDER",
           ]

from ..common.constituency_tree_features import (ConstituencyTreeNodeFeatures, 
                                                                  _ConstituencyTreeFeatures)

from .constituency_tree_node_head_finder import CollinsHeadFinder
from .parameters import ontonotes_v5_constituent_tags as POS_tags_module
ENGLISH_NODE_HEAD_FINDER = CollinsHeadFinder()

class EnglishConstituencyTreeNodeFeatures(ConstituencyTreeNodeFeatures):
    """ A class defining grammatical features that can be defined on a ConstituencyTreeNode instance, 
    created from data coming from a document written in English.
    
    Assumes that constituency trees are spread on their width rather than on their height (so no 
    "only binary trees") - parse trees produced by the Stanford Core NLP parser should be fine.
    
    Here, the constituency tree tags used are the "Ontonotes v5 modified Penn Treebank" constituent 
    tags, and the head finder used is the "Collins" rules based head finder.
    
    Attributes:
        POS_tags_module: the python module defining the constituency tree tags used to characterize 
            the constituency tree of the document which the mentions originate from (the 
            "ontonotes_v5_constituent_tags" module)
        
        node_head_finder: the _ConstituencyTreeNodeHeadFinder child class instance used under the 
            hood; here, a CollinsHeadFinder instance
    """
    
    def __init__(self):
        super().__init__(POS_tags_module, ENGLISH_NODE_HEAD_FINDER)


class EnglishConstituencyTreeFeatures(_ConstituencyTreeFeatures):
    """ A class defining grammatical features that can be defined on a ConstituencyTree instance, 
    created from data coming from a document written in English.
    
    Assumes that constituency trees are spread on their width rather than on their height (so no 
    "only binary trees") - parse trees produced by the Stanford Core NLP parser should be fine.
    
    Attributes:
        constituency_tree_node_features: the value of the ConstituencyTreeNodeFeatures instance 
        used under the hood; here, an EnglishConstituencyTreeNodeFeatures instance
    """
    
    def __init__(self):
        constituency_tree_node_features = EnglishConstituencyTreeNodeFeatures()
        super().__init__(constituency_tree_node_features)


ENGLISH_CONSTITUENCY_TREE_NODE_FEATURES = EnglishConstituencyTreeNodeFeatures()
ENGLISH_CONSTITUENCY_TREE_FEATURES = EnglishConstituencyTreeFeatures()