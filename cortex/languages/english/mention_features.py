# -*- coding: utf-8 -*-

"""
Defines a class used to compute data and values corresponding to grammatical information, also called 
grammatical features here, from instances of the Mention class, defined for document written in English.
"""

__all__ = ["EnglishMentionFeatures", 
           "ENGLISH_MENTION_FEATURES",
           ]

from collections import Counter

from cortex.languages.common.mention_features import MentionFeatures
from cortex.parameters.mention_data_tags import PLURAL_NUMBER_TAG

from .knowledge import lexicon

from .constituency_tree_features import ENGLISH_CONSTITUENCY_TREE_NODE_FEATURES
from .parameters import ontonotes_v5_constituent_tags as POS_tags_module
from .parameters import ontonotes_v5_named_entities_data_tags as NE_tags_module


class EnglishMentionFeatures(MentionFeatures):
    """ A class defining the grammatical features that can be interesting to know about a mention 
    of a document written in English.
    
    Assumes that they are part of a document whose sentences are characterized by their respective 
    constituency tree, annotated with tags used to annotate the documents of the 'Ontonotes v5' 
    corpus, and that each mention has been synchronized with its corresponding tokens.
    
    Attributes:
        POS_tags_module: the python module defining the constituency tree tags used to characterize 
            the constituency tree of the document which the mentions originate from (the 
            "ontonotes_v5_constituent_tags" module)
        
        NE_tags_module: the python module defining the named entities tags used to characterize 
            those of the document which the mentions originate from (the 
            "ontonotes_v5_named_entities_data_tags" module)
        
        lexicon: the python module defining English lexical data (ex: collection of words...) that 
            are known to be associated to definite roles, and so can be leveraged to infer grammatical 
            information.
        
        constituency_tree_node_features: an EnglishConstituencyTreeNodeFeatures instance, the one to use 
            internally, when computing features values
    """
                                
    def __init__(self):
        super().__init__(POS_tags_module, NE_tags_module, lexicon, ENGLISH_CONSTITUENCY_TREE_NODE_FEATURES)
    
    ## Instance methods
    def is_indefinite(self, mention):
        """ Determines whether the input Mention instance refers to an indefinite entity.
        
        Assumes that the Mention instance's gram type has been characterized; and, if mention is 
        nominal, assumes that mention has been synchronized with its characterized tokens, which 
        themselves have been synchronized with their respective ConstituencyTreeNode
        
        Args:
            mention: a Mention instance

        Returns:
            a boolean
        """
        if self.is_expanded_pronoun(mention):
            return mention.raw_text.lower() in self.lexicon.INDEFINITE_PRONOUN_MENTIONS
        elif self.is_nominal(mention):
            token = mention.tokens[0]  # First token
            # Test whether or not the first token is a cardinal number
            node = token.constituency_tree_node
            if token.POS_tag == self.POS_tags_module.CARDINAL_NUMBER_TAG:
                return True
            # Test whether or not the first token is an indefinite article
            if token.POS_tag == self.POS_tags_module.DETERMINER_TAG \
                 and token.raw_text.lower() in ["a", "an"]:
                return True
            # Test whether or not the first token is a plural common noun (no indefinite article for plural in English)
            if mention.number == PLURAL_NUMBER_TAG and node.parent.syntactic_tag == self.POS_tags_module.NOUN_PLURAL_TAG:
                return True
            # Test presence of a quantifier before the head
            if token.raw_text.lower() in self.lexicon.QUANTIFIERS:
                return True
            # Test whether content before the head is a partitive
            if " ".join([token.raw_text.lower(), "of"]) in self.lexicon.PARTITIVES:
                return True
        return False
    
    def is_quantified(self, mention):
        """ Determines whether the input Mention instance refers to an quantified entity.
        
        Assumes that the Mention instance's gram type has been characterized; and, if mention is 
        nominal, assume that mention has been synchronized with its characterized tokens
        
        Args:
            mention: a Mention instance

        Returns:
            a boolean
        """
        if self.is_nominal(mention):
            token = mention.tokens[0]  # First token
            if token.raw_text.lower() in self.lexicon.QUANTIFIERS:
                return True
            if " ".join((token.raw_text.lower(), "of")) in self.lexicon.PARTITIVES:
                return True
        return False
    
    def get_c_command_and_reflexive_normal_pronouns_counts(self, antecedent_mention, subsequent_mention):
        """ Computes statistical data regarding the type of constituency tree node tags of the nodes 
        making up the path between the NP dominating node of the input antecedent mention, and the 
        NP dominating node of the input subsequent mention, in the case that the input subsequent 
        mention is an extended pronoun gram type mention.
        
        Args:
            antecedent_mention: a Mention instance, located before the other input mention, in 
                reading document order
            subsequent_mention: a Mention instance, located after the other input mention, in 
                reading document order

        Returns:
            a (pronoun_type, nb_NP, nb_VP, nb_S, tot_nb) tuple, or None, if the inputs are 
            incorrect wrt to this method's functionning, where:
                - 'pronoun-type' is a string; either '_refl' if the input subsequent mention is reflexive, 
                  or '_norm' otherwise
                - 'nb_NP' is the number of constituency tree nodes, found on the path, whose syntactic tag 
                  is that of a noun phrase
                - 'nb_V' is the number of constituency tree nodes, found on the path, whose syntactic tag 
                  is that of a verb phrase
                - 'nb_S' is the number of constituency tree nodes, found on the path, whose syntactic tag 
                  is that of a simple declarative clause
                - 'tot_nb' is the number of constituency tree nodes found on the path
        """
        if self.is_expanded_pronoun(subsequent_mention):
            left_NP_node = self.get_dominating_NP_ancestor_node(antecedent_mention)
            right_NP_node = self.get_dominating_NP_ancestor_node(subsequent_mention)
            if left_NP_node is not None and right_NP_node is not None:
                c_command_path = left_NP_node.get_c_command_path(right_NP_node)
                if c_command_path is not None:
                    pronoun_type = "_refl" if self.is_reflexive_pronoun(subsequent_mention) else "_norm"
                    syntactic_tags_counter = Counter(node.syntactic_tag for node in c_command_path[1:-1]) # Assume that origin and destination are part of the path
                    nb_NP = syntactic_tags_counter.get(self.POS_tags_module.NOUN_PHRASE_TAG, 0)
                    nb_VP = syntactic_tags_counter.get(self.POS_tags_module.VERB_PHRASE_TAG, 0)
                    nb_S = syntactic_tags_counter.get(self.POS_tags_module.SIMPLE_DECLARATIVE_CLAUSE_TAG, 0)
                    tot_nb = len(c_command_path[1:-1])
                    return pronoun_type, nb_NP, nb_VP, nb_S, tot_nb
                return None
            return None
        return None
    
    def is_referential(self, mention, referential_probability_threshold):
        """ Determines whether or not the input mention is referential.
        
        Returns True if the refrential_probability attribute value of the input Mention instance is 
        strictly superior to the input threshold value, or if its value is None.
        
        Args:
            mention: a Mention instance
            referential_probability_threshold: a float, between 0. and 1.

        Returns:
            a boolean
        """
        if mention.raw_text.lower() == "there":
            if mention.head_tokens[-1].POS_tag == self.POS_tags_module.EXISTENTIAL_THERE_TAG:
                return False
            return True
        proba = mention.referential_probability
        if proba is None or proba > referential_probability_threshold:
            return True
        return False
    
ENGLISH_MENTION_FEATURES = EnglishMentionFeatures()
