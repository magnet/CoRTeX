# -*- coding: utf-8 -*-

"""
Defines utilities implementing known set(s) of rules to resolve a coreference partition for an 
English document
"""

"""
Available submodule(s)
-----------------------
sieves
    Defines the 'sieves' set of rules, and well as the corresponding 'resolve' function
"""

__all__ = ["RULES_TYPE2COREF_RESOLVE_FCT", "RULES_TYPE2EDGES_CONSTRAINTS_FCT",]

from .sieves import resolve as sieves_coref_resolve, get_sieves_pass1_edges_constraints as sieves_edges_constraints

RULES_TYPE2COREF_RESOLVE_FCT = {"english_sieves": sieves_coref_resolve}

RULES_TYPE2EDGES_CONSTRAINTS_FCT = {"english_sieves": sieves_edges_constraints}