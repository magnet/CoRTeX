# -*- coding: utf-8 -*-

"""
Defines a coreference resolution function based on the use of 'multi-sieves' set of rules.

Those rules are described in the following references:

  - K. Raghunathan, H. Lee, S. Rangarajan, N. Chambers, M. Surdeanu, D. Jurafsky, and C. Manning  2010.
    "A Multi-Pass Sieve for Coreference Resolution". In EMNLP.

  - Heeyoung Lee, Yves Peirsman, Angel Chang, Nathanael Chambers, Mihai Surdeanu, and Dan Jurafsky.  2011.
    "Stanford’s multi-pass sieve coreference resolution system at the conll-2011 shared task".
    In Proceedings of the Fifteenth Conference on Computational Natural Language Learning: Shared Task, 
    pages 28–34, Portland,  Oregon,  USA, June. Association for Computational Linguistics.


The resolution function consists in applying the distinct specific rule functions one after the other 
on inputs, of which are notably the current state coreference partition to establish, as well as the 
current state of the constraints to respect regarding coreference links between mentions. Here, a 
constraint is understood to be a (antecedent mention, subsequent mention) pair for which a coreference 
link should NOT exist. 

The functions assume that the document to process, as well as its mentions, have all been characterized.

It is assumed that the coreference partition that is passed along to the sieves 'pass' functions 
has been initialized with all the mentions that will be considered during the passage in each 'pass' 
function, so that, for each mention, there indeed exists a coreference entity to which this 
mention's extent belongs.

It is assumed that, for an input Document instance, the 'named_entities' attribute value is not None, 
but indeed a collection.
"""

__all__ = ["resolve", 
           "get_sieves_pass1_edges_constraints", 
           ]

from collections import defaultdict, OrderedDict
from itertools import product

from cortex.parameters.mention_data_tags import PLURAL_NUMBER_TAG, UNKNOWN_VALUE_TAG
from cortex.languages.english.knowledge.lexicon import (STOP_WORDS2, LOCATION_MODIFIERS, 
                                                          EXPANDED_PERSONAL_PRONOUN_MENTIONS, EXPANDED_MONEY_PERCENT_NUMBER_PRONOUN_MENTIONS, 
                                                          DATE_TIME_PRONOUN_MENTIONS, EXPANDED_ORGANIZATION_PRONOUN_MENTIONS, 
                                                          EXPANDED_GPE_PRONOUN_MENTIONS, EXPANDED_LOCATION_PRONOUN_MENTIONS, 
                                                          EXPANDED_FACILITY_VEHICLE_WEAPON_PRONOUN_MENTIONS, 
                                                          EXPANDED_ANIMATE_PRONOUN_MENTIONS, EXPANDED_INANIMATE_PRONOUN_MENTIONS, 
                                                          INDEFINITE_PRONOUN_MENTIONS, WH_PRONOUN_MENTIONS,
                                                          )
from cortex.languages.english.mention_features import ENGLISH_MENTION_FEATURES as MENTION_FEATURES
from cortex.languages.english.sentence_features import ENGLISH_SENTENCE_FEATURES as SENTENCE_FEATURES
from cortex.languages.english.parameters import ontonotes_v5_constituent_tags as POS_tags
from cortex.languages.english.parameters import ontonotes_v5_named_entities_data_tags as NE_tags
from cortex.utils.memoize import Memoized
from cortex.api.coreference_partition import CoreferencePartition



def resolve(document, sieve_filter_anaphoric=False):
    """ Predicts a coreference partition for the input document, using the algorithm consisting in 
    applying in succession the multi-sieves rules.
    
    Args:
        document: a Document instance
        sieve_filter_anaphoric: boolean, parameter for the multi-sieves rules: most rules iterate 
            over the mentions, and for each mention, try to see whether a coreference link can be found with 
            an antecedent mention. If such a link is found, then the mention under scrutiny is anaphoric. 
            If it is already known, before trying such a link, that the mention under scrutiny is anaphoric, 
            then it can be useful to skip this search. When this parameter is True, such search will be skipped.

    Returns:
        a CoreferencePartition instance
    """
    # Create mentions/coreference_partition/constraints
    mentions = document.mentions
    coreference_partition = CoreferencePartition(mention_extents=(m.extent for m in mentions))
    constraints = set()
    # Multi-sieves
    mentions, coreference_partition, constraints, _ = _sieves_pass1(document, mentions, coreference_partition, constraints, filter_anaphoric=sieve_filter_anaphoric)
    mentions, coreference_partition, constraints, _ = _sieves_pass2(document, mentions, coreference_partition, constraints, filter_anaphoric=sieve_filter_anaphoric)
    mentions, coreference_partition, constraints, _ = _sieves_pass3(document, mentions, coreference_partition, constraints, filter_anaphoric=sieve_filter_anaphoric)
    mentions, coreference_partition, constraints, _ = _sieves_pass4(document, mentions, coreference_partition, constraints, filter_anaphoric=sieve_filter_anaphoric)
    mentions, coreference_partition, constraints, _ = _sieves_pass5(document, mentions, coreference_partition, constraints, filter_anaphoric=sieve_filter_anaphoric)
    mentions, coreference_partition, constraints, _ = _sieves_pass6(document, mentions, coreference_partition, constraints, filter_anaphoric=sieve_filter_anaphoric)
    mentions, coreference_partition, constraints, _ = _sieves_pass7(document, mentions, coreference_partition, constraints, filter_anaphoric=sieve_filter_anaphoric)
    mentions, coreference_partition, constraints, _ = _sieves_pass8(document, mentions, coreference_partition, constraints, filter_anaphoric=sieve_filter_anaphoric)
    mentions, coreference_partition, constraints, _ = _sieves_pass9(document, mentions, coreference_partition, constraints, filter_anaphoric=sieve_filter_anaphoric)
    _, coreference_partition, _, _ = _sieves_pass10(document, mentions, coreference_partition, constraints, filter_anaphoric=sieve_filter_anaphoric)
    return coreference_partition


def get_sieves_pass1_edges_constraints(document, positive_edges, negative_edges, entity_head_extents=None):#mentions, 
    """ Updates the input set of edges with the result of the application of the first multi-sieves 
    rule on the input document.
    
    The positive edges set will be updated with the coreference link found during this pass
    The negative edges set will be updated with the constraints found during this pass
    
    Args:
        document: Document instance, the document on which to apply the multi-sieves rule
        positive_edges: set of (antecedent ment extent, subsequent mention extent) pairs, 
            representing the coreference link that MUST exist for the mentions of this document
        negative_edges: set of (antecedent ment extent, subsequent mention extent) pairs, 
            representing the coreference link that MUST NOT exist for the mentions of this document
        entity_head_extents: set-like, or None; not used, exist for interface compatibility reasons
    """
    mentions = document.mentions
    coreference_partition = CoreferencePartition(mention_extents=(m.extent for m in mentions))
    constraints = set()
    _, _, _, new_coreferring_mention_extent_pairs = _sieves_pass1(document, mentions, coreference_partition, 
                                                          constraints, filter_anaphoric=False, 
                                                          output_new_pairs=True) # mentions, partition, constraints
    positive_edges.update(new_coreferring_mention_extent_pairs)
    negative_edges.update(constraints)


################
#### SIEVES ####
################
def _sieves_pass1(document, mentions, coreference_partition, constraints, filter_anaphoric=False, output_new_pairs=False):
    """ Function implementing the application of the first multi-sieves rule on the input document 
    and mentions, so as to properly update the input coreference partition and constraints' respective 
    states.
    
    Args:
        document: Document instance, the document that the mentions to partition originate from
        mentions: collection of Mentions instances associated to the input document, representing 
            the universe of the partition to be built
        coreference_partition: CoreferencePartition instance, the current state of the coreference 
            partition being built by the ordered application of the multi-sieves functions. Its universe 
            should be made out of all the mention of the input collection.
        constraints: a set containing pairs of (antecedent mention extent, subsequent mention extent), 
            the current state of the constraints to be respected when trying to build the coreference partition.
        filter_anaphoric: boolean; if True, may skip some search to find coreference link between 
            mentions, if the subsequent mention of the would-be pair is already known to be anaphoric at the 
            beginning of the search.
        output_new_pairs: boolean, whether or not to fill the content of the 
            'new_coreferring_mention_extent_pairs' collection output with the pairs of 
            (antecedent mention extent, subsequent mention extent) corresponding to the pair of mentions for 
            which a coreference link was created by the processing of this function.

    Returns:
        a (mentions, coreference_partition, constraints, new_coreferring_mention_extent_pairs) 
        tuple, where:
            - 'mentions' is the input 'mentions' value
            - 'coreference_partition' is the input 'coreference_partition' value, having potentially been 
              updated by the processing of this function
            - 'constraints' is the input 'constraints' value, , having potentially been updated by the 
              processing of this function
            - 'new_coreferring_mention_extent_pairs' is a collection of 
              (antecedent mention extent, subsequent mention extent) pairs, corresponding to the pair of 
              mentions for which a coreference link was created by the processing of this function, if the 
              'output_new_pairs' parameter value was True. If it was False, then this collection will be empty.
    """
    quotations = document.quotations if document.quotations is not None else tuple()
    extent2quotation = dict(document.extent2quotation) if document.quotations is not None else dict()
    @Memoized
    def _extent_to_potential_encompassing_quotation_data(extent):
        s, e = extent
        for quotation_extent, quotation in extent2quotation.items():
            qs, qe = quotation_extent
            if qs <= s and e <= qe:
                return quotation
        return None
    def mention_is_in_quotation(mention):
        return _extent_to_potential_encompassing_quotation_data(mention.extent) is not None
    
    current_extent2mention = OrderedDict((m.extent, m) for m in mentions)
    
    # Assume mentions are already filtered
    new_coreferring_mention_extent_pairs = []
    # Speaker identification
    # Personal pronouns: data <=> (pronoun, list of values related to that pronoun)
    I_data = ("I", set(("i", "my", "me", "mine", "myself")))
    you_data = ("you", set(("you", "your", "yours", "both of you", "you all", "yourself")))
    you_pl_data = ("you_pl", set(("both of you", "you all", "yourselves")))
    he_data = ("he", set(("he", "his", "him", "himself")))
    she_data = ("she", set(("she", "her", "hers", "herself")))
    we_data = ("we", set(("we", "our", "us", "ours", "both of us", "all of us", "we all", "ourselves")))
    they_data = ("they", set(("they", "their", "them", "theirs", "all of them", "they all", "themselves")))
    personal_pronouns = [I_data, you_data, you_pl_data, he_data, she_data, we_data, they_data]
    # Speaker/pronoun dict
    speaker2mentions_per_pronoun_class = dict()
    speaker2is_in_quotation_and_pronoun_class_mentions_partition = dict()
    for sentence in document.sentences:
        speaker = sentence.speaker
        if speaker is not None:
            speaker2mentions_per_pronoun_class[speaker] = defaultdict(list)
            speaker2is_in_quotation_and_pronoun_class_mentions_partition[speaker] = {True: defaultdict(list), False: defaultdict(list)} # bool: mention in quotation
    # Find personal pronouns
    for mention in current_extent2mention.values():
        speaker = mention.sentence.speaker
        if speaker is not None and MENTION_FEATURES.is_expanded_pronoun(mention):
            text = mention.raw_text.lower() # TODO: (legacy) what to to with e.g. "all of them"
            for pronoun_class, pronoun_values in personal_pronouns:
                if text in pronoun_values:
                    speaker2mentions_per_pronoun_class[speaker][pronoun_class].append(mention)
                    speaker2is_in_quotation_and_pronoun_class_mentions_partition[speaker][mention_is_in_quotation(mention)][pronoun_class].append(mention)
    # Link 'I' of same speaker (outside quote)
    for speaker in speaker2mentions_per_pronoun_class:
        pronoun_mentions = sorted(speaker2is_in_quotation_and_pronoun_class_mentions_partition[speaker][False]["I"])
        for i in range(1, len(pronoun_mentions)):
            coreference_partition.join(pronoun_mentions[0].extent, pronoun_mentions[i].extent)
        if output_new_pairs:
            for j in range(1, len(pronoun_mentions)):
                for i in range(j):
                    new_coreferring_mention_extent_pairs.append((pronoun_mentions[i].extent, pronoun_mentions[j].extent))
    # Link 'I'/'you' in same quote
    for quotation in quotations:
        temp_pronoun_class2mentions = defaultdict(list)
        for mention in quotation.mentions:
            if mention.extent in current_extent2mention:
                text = mention.raw_text.lower() # TODO: (legacy) what to to with e.g. "all of them"
                for pronoun_class, pronoun_values in personal_pronouns:
                    if text in pronoun_values:
                        temp_pronoun_class2mentions[pronoun_class].append(mention)
        for pronoun_list in temp_pronoun_class2mentions.values():
            for i in range(1, len(pronoun_list)):
                coreference_partition.join(pronoun_list[0].extent, pronoun_list[i].extent)
            if output_new_pairs:
                for j in range(1, len(pronoun_list)):
                    for i in range(j):
                        new_coreferring_mention_extent_pairs.append((pronoun_list[i].extent,pronoun_list[j].extent))
    # Link 'I' in quotation with subject of 'say', 'answered', etc
    # Link 'you' in quotation with indirect object of 'say', 'answered', etc
    for quotation in quotations:
        I_list = []
        we_list = []
        you_list = []
        for mention in quotation.mentions:
            if mention.extent in current_extent2mention:
                text = mention.raw_text.lower()
                if text in I_data[1]:
                    I_list.append(mention)
                elif text in we_data[1]:
                    we_list.append(mention)
                elif text in you_data[1]:
                    you_list.append(mention)
        speaker_mention = quotation.speaker_mention
        #speaker_token_verb = quotation.speaker_token_verb # TODO: (legacy) restrict verbs or is it enough accurate ?
        interlocutor_mention = quotation.interlocutor_mention
        if speaker_mention is not None:
            for I_mention in I_list:
                coreference_partition.join(speaker_mention.extent, I_mention.extent)
                if output_new_pairs:
                    if speaker_mention <= I_mention:
                        pair = (speaker_mention.extent, I_mention.extent)
                    else:
                        pair = (I_mention.extent, speaker_mention.extent)
                    new_coreferring_mention_extent_pairs.append(pair)
            if speaker_mention.number == PLURAL_NUMBER_TAG:
                for we_mention in we_list:
                    coreference_partition.join(speaker_mention.extent, we_mention.extent)
                    if output_new_pairs:
                        if speaker_mention <= we_mention:
                            pair = (speaker_mention.extent, we_mention.extent)
                        else:
                            pair = (we_mention.extent, speaker_mention.extent)
                        new_coreferring_mention_extent_pairs.append(pair)
        if interlocutor_mention is not None:
            for you_mention in you_list:
                coreference_partition.join(interlocutor_mention.extent, you_mention.extent)
                if output_new_pairs:
                    if interlocutor_mention <= you_mention:
                        pair = (interlocutor_mention.extent, you_mention.extent)
                    else:
                        pair = (you_mention.extent, interlocutor_mention.extent)
                    new_coreferring_mention_extent_pairs.append(pair)
        # Constraint: 'I' and other mentions of the same speaker
        for mention in quotation.mentions:
            if mention.extent in current_extent2mention:
                if not MENTION_FEATURES.is_expanded_pronoun(mention):
                    for I_mention in I_list:
                        _add_constraint(constraints, mention, I_mention)
                # Constraint: 'you' and 'we' cannot be coreferent with nominal mentions when same speaker
                if MENTION_FEATURES.is_nominal(mention):
                    for you_mention in you_list:
                        _add_constraint(constraints, mention, you_mention)
                    for we_mention in we_list:
                        _add_constraint(constraints, mention, we_mention)
    # Constraint: 'I' and other mentions of the same speaker
    for mention in current_extent2mention.values():
        speaker = mention.sentence.speaker
        if speaker is not None:
            if mention.raw_text.lower() not in I_data: # FIXME: what are we testing here? Equality of the text with the text describing the pronoun class (==I_data[0])? Or equality of the text with one of the possible values (in I_data[1])?
                for I_mention in speaker2is_in_quotation_and_pronoun_class_mentions_partition[speaker][False]["I"]:
                    if mention.extent != I_mention.extent and not coreference_partition.are_coreferent(mention.extent, I_mention.extent):
                        _add_constraint(constraints, mention, I_mention)
            # Constraint: 'you' and 'we' cannot be coreferent with nominal mentions when same speaker
            if MENTION_FEATURES.is_nominal(mention):
                for you_mention in speaker2is_in_quotation_and_pronoun_class_mentions_partition[speaker][False]["you"]:
                    if not coreference_partition.are_coreferent(mention.extent, you_mention.extent):
                        _add_constraint(constraints, mention, you_mention)
                for we_mention in speaker2is_in_quotation_and_pronoun_class_mentions_partition[speaker][False]["we"]:
                    if not coreference_partition.are_coreferent(mention.extent, we_mention.extent):
                        _add_constraint(constraints, mention, we_mention)
    # Constraint: 'I', 'you', 'we' of different speakers
    speaker_mentions = speaker2mentions_per_pronoun_class.keys()
    for j in range(1, len(speaker_mentions)):
        spk_j = speaker_mentions[j]
        for i in range(j):
            spk_i = speaker_mentions[i]
            for pronoun_class in ("I","you","we"):
                for m_i in speaker2mentions_per_pronoun_class[spk_i][pronoun_class]:
                    for m_j in speaker2mentions_per_pronoun_class[spk_j][pronoun_class]:
                        _add_constraint(constraints, m_i, m_j)
    # Constraint: two different person pronouns by the same speaker
    for speaker in speaker2mentions_per_pronoun_class:
        for j in range(1, len(personal_pronouns)):
            prn_j = personal_pronouns[j][0]
            for i in range(j):
                prn_i = personal_pronouns[i][0]
                for m_i in speaker2mentions_per_pronoun_class[speaker][prn_i]:
                    for m_j in speaker2mentions_per_pronoun_class[speaker][prn_j]:
                        _add_constraint(constraints, m_i, m_j)
    # Link 'I'/'you' with 'you'/'I' of previous speaker in conversations (nb speaker > 1)
    # Link 'you' same speaker, same segment (i.e. link only if the speaker is not interrupted)
    if len(speaker2mentions_per_pronoun_class) > 1: # At least two speakers
        speaker_I_you = []
        for sentence in document.sentences:
            current_speaker = sentence.speaker
            if speaker is not None:
                current_I = []
                current_you = []
                for mention in sentence.mentions:
                    if mention.extent in current_extent2mention:
                        if MENTION_FEATURES.is_expanded_pronoun(mention) and not mention_is_in_quotation(mention): # Do not process in quote pronouns
                            text = mention.raw_text.lower()
                            if text in I_data[1]:
                                current_I.append(mention)
                            if text in ("you","your","yours","yourself"): # Do not use plurals
                                current_you.append(mention)
                if not speaker_I_you:
                    speaker_I_you.append((current_speaker, current_I, current_you))
                else:
                    previous_speaker, previous_I, previous_you = speaker_I_you[-1] # Last speaker
                    if previous_speaker == current_speaker:
                        speaker_I_you.pop()
                        speaker_I_you.append((previous_speaker, previous_I+current_I, previous_you+current_you))
                    else:
                        speaker_I_you.append((current_speaker, current_I, current_you))
        for j in range(1,len(speaker_I_you)):
            current_speaker, current_I, current_you = speaker_I_you[j]
            # Link current you (same speaker, same segment) together
            for l in range(1, len(current_you)):
                m_l = current_you[l]
                for k in range(l):
                    m_k = current_you[k]
                    coreference_partition.join(m_k.extent, m_l.extent)
                    if output_new_pairs:
                        new_coreferring_mention_extent_pairs.append((m_k.extent, m_l.extent))
            mem_previous_speaker = None
            for i in range(j-1,-1,-1):
                previous_speaker ,previous_I, previous_you = speaker_I_you[i]
                if mem_previous_speaker is None:
                    mem_previous_speaker = previous_speaker
                if previous_speaker == mem_previous_speaker or previous_speaker == current_speaker:
#                    # Current you with previous I # FIXME: (legacy) remove, this is not accurate enough
#                    for I_mention in previous_I:
#                        for you_mention in current_you:
#                            if not _is_constrained(constraints, coreference_partition, I_mention, you_mention):
#                                coreference_partition.join(I_mention.extent, you_mention.extent)
#                                if output_new_pairs:
#                                    new_coreferring_mention_extent_pairs.append((I_mention.extent, you_mention.extent))
                    # Current I with previous you
                    for you_mention in previous_you:
                        for I_mention in current_I:
                            if not _is_constrained(constraints, coreference_partition, you_mention, I_mention):
                                coreference_partition.join(you_mention.extent, I_mention.extent)
                                if output_new_pairs:
                                    new_coreferring_mention_extent_pairs.append((you_mention.extent, I_mention.extent))
                else:
                    break
    # 'you' of same speaker (outside quote), different segments (speaker interrupted by another speaker)
    # should be done AFTER linking 'you' and 'I' (taking constraints into account)
    for speaker in speaker2mentions_per_pronoun_class:
        pronoun_list = sorted(speaker2is_in_quotation_and_pronoun_class_mentions_partition[speaker][False]["you"])
        for j in range(1, len(pronoun_list)):
            m_j = pronoun_list[j]
            for i in range(j):
                m_i = pronoun_list[i]
                if not _is_constrained(constraints, coreference_partition, m_i, m_j):
                    coreference_partition.join(m_i.extent, m_j.extent)
                    if output_new_pairs:
                        new_coreferring_mention_extent_pairs.append((m_i.extent, m_j.extent))
    return mentions, coreference_partition, constraints, new_coreferring_mention_extent_pairs

def _sieves_pass2(document, mentions, coreference_partition, constraints, filter_anaphoric=False, output_new_pairs=False):
    """ Function implementing the application of the second multi-sieves rule on the input document 
    and mentions, so as to properly update the input coreference partition and constraints' respective 
    states.
    
    Args:
        document: Document instance, the document that the mentions to partition originate from
        mentions: collection of Mentions instances associated to the input document, representing 
            the universe of the partition to be built
        coreference_partition: CoreferencePartition instance, the current state of the coreference 
            partition being built by the ordered application of the multi-sieves functions. Its universe 
            should be made out of all the mention of the input collection.
        constraints: a set containing pairs of (antecedent mention extent, subsequent mention extent), 
            the current state of the constraints to be respected when trying to build the coreference partition.
        filter_anaphoric: boolean; if True, may skip some search to find coreference link between 
            mentions, if the subsequent mention of the would-be pair is already known to be anaphoric at the 
            beginning of the search.
        output_new_pairs: boolean, whether or not to fill the content of the 
            'new_coreferring_mention_extent_pairs' collection output with the pairs of 
            (antecedent mention extent, subsequent mention extent) corresponding to the pair of mentions for 
            which a coreference link was created by the processing of this function.

    Returns:
        a (mentions, coreference_partition, constraints, new_coreferring_mention_extent_pairs) 
        tuple, where:
            - 'mentions' is the input 'mentions' value
            - 'coreference_partition' is the input 'coreference_partition' value, having potentially been 
              updated by the processing of this function
            - 'constraints' is the input 'constraints' value, , having potentially been updated by the 
              processing of this function
            - 'new_coreferring_mention_extent_pairs' is a collection of 
              (antecedent mention extent, subsequent mention extent) pairs, corresponding to the pair of 
              mentions for which a coreference link was created by the processing of this function, if the 
              'output_new_pairs' parameter value was True. If it was False, then this collection will be empty.
    """
    new_coreferring_mention_extent_pairs = []
    # Exact match
    for j in range(1, len(mentions)):
        m_j = mentions[j]
        # Mention selection
        if filter_anaphoric and _is_anaphoric(m_j, coreference_partition):
            continue
        # Skip indefinites
        if MENTION_FEATURES.is_indefinite(m_j):
            continue
        # Do not process pronouns # FIXME: (legacy)
        if MENTION_FEATURES.is_expanded_pronoun(m_j):
            continue
        text_j = m_j.raw_text.lower().strip("\" ")
        for i in range(j):
            m_i = mentions[i]
            # Do not process pronouns # FIXME: (legacy, redundant with the same condition being applied just above?)
            if MENTION_FEATURES.is_expanded_pronoun(m_i):
                continue
            text_i = m_i.raw_text.lower().strip("\" ")
            if text_i == text_j:
                if not _is_constrained(constraints, coreference_partition, m_i, m_j):
                    coreference_partition.join(m_i.extent, m_j.extent)
                    if output_new_pairs:
                        new_coreferring_mention_extent_pairs.append((m_i.extent,m_j.extent))
    return mentions, coreference_partition, constraints, new_coreferring_mention_extent_pairs


def _sieves_pass3(document, mentions, coreference_partition, constraints, filter_anaphoric=False, output_new_pairs=False):
    """ Function implementing the application of the third multi-sieves rule on the input document 
    and mentions, so as to properly update the input coreference partition and constraints' respective 
    states.
    
    Args:
        document: Document instance, the document that the mentions to partition originate from
        mentions: collection of Mentions instances associated to the input document, representing 
            the universe of the partition to be built
        coreference_partition: CoreferencePartition instance, the current state of the coreference 
            partition being built by the ordered application of the multi-sieves functions. Its universe 
            should be made out of all the mention of the input collection.
        constraints: a set containing pairs of (antecedent mention extent, subsequent mention extent), 
            the current state of the constraints to be respected when trying to build the coreference partition.
        filter_anaphoric: boolean; if True, may skip some search to find coreference link between 
            mentions, if the subsequent mention of the would-be pair is already known to be anaphoric at the 
            beginning of the search.
        output_new_pairs: boolean, whether or not to fill the content of the 
            'new_coreferring_mention_extent_pairs' collection output with the pairs of 
            (antecedent mention extent, subsequent mention extent) corresponding to the pair of mentions for 
            which a coreference link was created by the processing of this function.

    Returns:
        a (mentions, coreference_partition, constraints, new_coreferring_mention_extent_pairs) 
        tuple, where:
            - 'mentions' is the input 'mentions' value
            - 'coreference_partition' is the input 'coreference_partition' value, having potentially been 
              updated by the processing of this function
            - 'constraints' is the input 'constraints' value, , having potentially been updated by the 
              processing of this function
            - 'new_coreferring_mention_extent_pairs' is a collection of 
              (antecedent mention extent, subsequent mention extent) pairs, corresponding to the pair of 
              mentions for which a coreference link was created by the processing of this function, if the 
              'output_new_pairs' parameter value was True. If it was False, then this collection will be empty.
    """
    def _get_relaxed_extent(mention):
        return mention.extent[0], mention.head_tokens[-1].extent[1]
    new_coreferring_mention_extent_pairs = []
    # Relaxed string match
    for j in range(1, len(mentions)):
        m_j = mentions[j]
        # Skip indefinites
        if MENTION_FEATURES.is_indefinite(m_j):
            continue
        # Do not process pronouns
        if MENTION_FEATURES.is_expanded_pronoun(m_j):
            continue
        # Mention selection
        if filter_anaphoric and _is_anaphoric(m_j, coreference_partition):
            continue
        relaxed_extent = _get_relaxed_extent(m_j) # Relaxed extent
        text_j = document.get_raw_text_extract(relaxed_extent).lower()
        for i in range(j):
            m_i = mentions[i]
            # Do not process pronouns
            if MENTION_FEATURES.is_expanded_pronoun(m_i):
                continue
            relaxed_extent_i = _get_relaxed_extent(m_i) # Relaxed extent
            text_i = document.get_raw_text_extract(relaxed_extent_i).lower()
            if text_i == text_j:
                if not _is_constrained(constraints, coreference_partition, m_i, m_j):
                    coreference_partition.join(m_i.extent, m_j.extent)
                    if output_new_pairs:
                        new_coreferring_mention_extent_pairs.append((m_i.extent,m_j.extent))
    return mentions, coreference_partition, constraints, new_coreferring_mention_extent_pairs


def _sieves_pass4(document, mentions, coreference_partition, constraints, filter_anaphoric=False, output_new_pairs=False):
    """ Function implementing the application of the fourth multi-sieves rule on the input document 
    and mentions, so as to properly update the input coreference partition and constraints' respective 
    states.
    
    Args:
        document: Document instance, the document that the mentions to partition originate from
        mentions: collection of Mentions instances associated to the input document, representing 
            the universe of the partition to be built
        coreference_partition: CoreferencePartition instance, the current state of the coreference 
            partition being built by the ordered application of the multi-sieves functions. Its universe 
            should be made out of all the mention of the input collection.
        constraints: a set containing pairs of (antecedent mention extent, subsequent mention extent), 
            the current state of the constraints to be respected when trying to build the coreference partition.
        filter_anaphoric: boolean; if True, may skip some search to find coreference link between 
            mentions, if the subsequent mention of the would-be pair is already known to be anaphoric at the 
            beginning of the search.
        output_new_pairs: boolean, whether or not to fill the content of the 
            'new_coreferring_mention_extent_pairs' collection output with the pairs of 
            (antecedent mention extent, subsequent mention extent) corresponding to the pair of mentions for 
            which a coreference link was created by the processing of this function.

    Returns:
        a (mentions, coreference_partition, constraints, new_coreferring_mention_extent_pairs) 
        tuple, where:
            - 'mentions' is the input 'mentions' value
            - 'coreference_partition' is the input 'coreference_partition' value, having potentially been 
              updated by the processing of this function
            - 'constraints' is the input 'constraints' value, , having potentially been updated by the 
              processing of this function
            - 'new_coreferring_mention_extent_pairs' is a collection of 
              (antecedent mention extent, subsequent mention extent) pairs, corresponding to the pair of 
              mentions for which a coreference link was created by the processing of this function, if the 
              'output_new_pairs' parameter value was True. If it was False, then this collection will be empty.
    """
    new_coreferring_mention_extent_pairs = []
    set_mention_extents = set(m.extent for m in mentions)
    extent2mention = document.extent2mention
    # Precise constructs
    for sentence in document.sentences:
        s_mention_extents = sorted(set_mention_extents.intersection(set(m.extent for m in sentence.mentions)))
        for j in range(1, len(s_mention_extents)):
            m_j = extent2mention[s_mention_extents[j]]
            # Mention selection
            if filter_anaphoric and _is_anaphoric(m_j, coreference_partition):
                continue
            for i in range(j):
                m_i = s_mention_extents[i]
                # Appositive
                # TODO: (legacy) (not in CoNLL)
                # Predicate nominative (copula)
                # TODO: (legacy) (not in CoNLL)
                # Role appositive
                # TODO: (legacy) (not in CoNLL)
                # Relative pronoun (not in CoNLL)
                # FIXME: what to do about this legacy commented out code?
#                    if MENTION_FEATURES.is_relative_pronoun(m_j) and m_j.extent[0] - m_i.head_extent[1] <= 3:
#                        if not _is_constrained(constraints, coreference_partition, m_i, m_j):
#                            coreference_partition.join(m_i.extent, m_j.extent)
#                            if output_new_pairs:
#                                new_coreferring_mention_extent_pairs.append((m_i.extent,m_j.extent))
    # Acronym
    for j in range(1, len(mentions)):
        m_j = mentions[j]
        # Mention selection
        if filter_anaphoric and _is_anaphoric(m_j, coreference_partition):
            continue
        for i in range(j):
            m_i = mentions[i]
            if MENTION_FEATURES.is_alias_of(m_i, m_j) or MENTION_FEATURES.is_alias_of(m_j, m_i):
                if not _is_constrained(constraints, coreference_partition, m_i, m_j):
                    coreference_partition.join(m_i.extent, m_j.extent)
                    if output_new_pairs:
                        new_coreferring_mention_extent_pairs.append((m_i.extent,m_j.extent))
    # Demonym
    # TODO: (legacy)
    return mentions, coreference_partition, constraints, new_coreferring_mention_extent_pairs


def _sieves_pass5(document, mentions, coreference_partition, constraints, filter_anaphoric=False, output_new_pairs=False, 
          constraint_words=True, constraint_modifiers=True):
    """ Function implementing the application of the fifth multi-sieves rule on the input document 
    and mentions, so as to properly update the input coreference partition and constraints' respective 
    states.
    
    Args:
        document: Document instance, the document that the mentions to partition originate from
        mentions: collection of Mentions instances associated to the input document, representing 
            the universe of the partition to be built
        coreference_partition: CoreferencePartition instance, the current state of the coreference 
            partition being built by the ordered application of the multi-sieves functions. Its universe 
            should be made out of all the mention of the input collection.
        constraints: a set containing pairs of (antecedent mention extent, subsequent mention extent), 
            the current state of the constraints to be respected when trying to build the coreference partition.
        filter_anaphoric: boolean; if True, may skip some search to find coreference link between 
            mentions, if the subsequent mention of the would-be pair is already known to be anaphoric at the 
            beginning of the search.
        output_new_pairs: boolean, whether or not to fill the content of the 
            'new_coreferring_mention_extent_pairs' collection output with the pairs of 
            (antecedent mention extent, subsequent mention extent) corresponding to the pair of mentions for 
            which a coreference link was created by the processing of this function.

    Returns:
        a (mentions, coreference_partition, constraints, new_coreferring_mention_extent_pairs) 
        tuple, where:
            - 'mentions' is the input 'mentions' value
            - 'coreference_partition' is the input 'coreference_partition' value, having potentially been 
              updated by the processing of this function
            - 'constraints' is the input 'constraints' value, , having potentially been updated by the 
              processing of this function
            - 'new_coreferring_mention_extent_pairs' is a collection of 
              (antecedent mention extent, subsequent mention extent) pairs, corresponding to the pair of 
              mentions for which a coreference link was created by the processing of this function, if the 
              'output_new_pairs' parameter value was True. If it was False, then this collection will be empty.
    """
    new_coreferring_mention_extent_pairs = []
    extent2mention = document.extent2mention
    # Strict head match
    for j in range(1, len(mentions)):
        m_j = mentions[j]
        # Do not process pronouns
        if MENTION_FEATURES.is_expanded_pronoun(m_j):
            continue
        # Mention selection
        if filter_anaphoric and _is_anaphoric(m_j, coreference_partition):
            continue
        text_j = m_j.head_raw_text.lower() # FIXME: legacy comment: "lemma ?"
        for i in range(j):
            m_i = mentions[i]
            # Do not process pronouns
            if MENTION_FEATURES.is_expanded_pronoun(m_i):
                continue
            text_i = m_i.head_raw_text.lower() # FIXME: legacy comment: "lemma ?"
            if text_i == text_j:
                if not _is_constrained(constraints, coreference_partition, m_i, m_j):
                    # Assert all words of next entity are in the previous word set
                    included_words = True
                    if constraint_words:
                        # Build word set of previous entity
                        e_i = coreference_partition.get_entity(m_i.extent)
                        word_set = set()
                        for mention_extent in e_i:
                            mention = extent2mention[mention_extent]
                            for token in mention.tokens:
                                word = token.raw_text.lower() # FIXME: legacy comment: "lemma ?"
                                if word != text_i and word not in STOP_WORDS2:
                                    word_set.add(word)
                        e_j = coreference_partition.get_entity(m_j.extent)
                        for mention_extent in e_j:
                            mention = extent2mention[mention_extent]
                            for token in mention.tokens:
                                word = token.raw_text.lower() # FIXME: legacy comment: "lemma ?"
                                if word != text_i and word not in STOP_WORDS2:
                                    if not word in word_set:
                                        included_words = False
                                    break
                            if not included_words:
                                break
                    # Assert modifiers are compatible / no location modifiers
                    included_modifiers = True
                    if constraint_modifiers:
                        e_i = coreference_partition.get_entity(m_i.extent)
                        modifier_set = set()
                        for mention_extent in e_i:
                            mention = extent2mention[mention_extent]
                            for token in mention.tokens:
                                if not token.POS_tag in (POS_tags.CARDINAL_NUMBER_TAG, POS_tags.ADJECTIVE_TAG, POS_tags.VERB_BASE_FORM_TAG, "N", "V"): # FIXME: there no "N" nor "V" POS tag in the list of tags used by CONLL!
                                    word = token.raw_text.lower() # FIXME: legacy comment: "lemma ?"
                                    if word != text_i and not word in STOP_WORDS2:
                                        modifier_set.add(word)
                        for token in m_j.tokens:
                            if not token.POS_tag in (POS_tags.CARDINAL_NUMBER_TAG, POS_tags.ADJECTIVE_TAG, POS_tags.VERB_BASE_FORM_TAG, "N", "V"): # FIXME: there no "N" nor "V" POS tag in the list of tags used by CONLL!
                                word = token.raw_text.lower() # FIXME: legacy comment: "lemma ?"
                                if not word in modifier_set or word in LOCATION_MODIFIERS:
                                    included_modifiers = False
                    # Assert not one within the other
                    not_within = not (m_i.includes(m_j) or m_j.includes(m_i))
                    if included_words and included_modifiers and not_within:
                        coreference_partition.join(m_i.extent, m_j.extent)
                        if output_new_pairs:
                            new_coreferring_mention_extent_pairs.append((m_i.extent,m_j.extent))
                        break
    return mentions, coreference_partition, constraints, new_coreferring_mention_extent_pairs


def _sieves_pass6(document, mentions, coreference_partition, constraints, filter_anaphoric=False, output_new_pairs=False):
    """ Function implementing the application of the sixth multi-sieves rule on the input document 
    and mentions, so as to properly update the input coreference partition and constraints' respective 
    states.
    
    Args:
        document: Document instance, the document that the mentions to partition originate from
        mentions: collection of Mentions instances associated to the input document, representing 
            the universe of the partition to be built
        coreference_partition: CoreferencePartition instance, the current state of the coreference 
            partition being built by the ordered application of the multi-sieves functions. Its universe 
            should be made out of all the mention of the input collection.
        constraints: a set containing pairs of (antecedent mention extent, subsequent mention extent), 
            the current state of the constraints to be respected when trying to build the coreference partition.
        filter_anaphoric: boolean; if True, may skip some search to find coreference link between 
            mentions, if the subsequent mention of the would-be pair is already known to be anaphoric at the 
            beginning of the search.
        output_new_pairs: boolean, whether or not to fill the content of the 
            'new_coreferring_mention_extent_pairs' collection output with the pairs of 
            (antecedent mention extent, subsequent mention extent) corresponding to the pair of mentions for 
            which a coreference link was created by the processing of this function.

    Returns:
        a (mentions, coreference_partition, constraints, new_coreferring_mention_extent_pairs) 
        tuple, where:
            - 'mentions' is the input 'mentions' value
            - 'coreference_partition' is the input 'coreference_partition' value, having potentially been 
              updated by the processing of this function
            - 'constraints' is the input 'constraints' value, , having potentially been updated by the 
              processing of this function
            - 'new_coreferring_mention_extent_pairs' is a collection of 
              (antecedent mention extent, subsequent mention extent) pairs, corresponding to the pair of 
              mentions for which a coreference link was created by the processing of this function, if the 
              'output_new_pairs' parameter value was True. If it was False, then this collection will be empty.
    """
    new_coreferring_mention_extent_pairs = []
    # Variant of strict head match
    # TODO: (legacy)
    return mentions, coreference_partition, constraints, new_coreferring_mention_extent_pairs


def _sieves_pass7(document, mentions, coreference_partition, constraints, filter_anaphoric=False, output_new_pairs=False):
    """ Function implementing the application of the seventh multi-sieves rule on the input document 
    and mentions, so as to properly update the input coreference partition and constraints' respective 
    states.
    
    Args:
        document: Document instance, the document that the mentions to partition originate from
        mentions: collection of Mentions instances associated to the input document, representing 
            the universe of the partition to be built
        coreference_partition: CoreferencePartition instance, the current state of the coreference 
            partition being built by the ordered application of the multi-sieves functions. Its universe 
            should be made out of all the mention of the input collection.
        constraints: a set containing pairs of (antecedent mention extent, subsequent mention extent), 
            the current state of the constraints to be respected when trying to build the coreference partition.
        filter_anaphoric: boolean; if True, may skip some search to find coreference link between 
            mentions, if the subsequent mention of the would-be pair is already known to be anaphoric at the 
            beginning of the search.
        output_new_pairs: boolean, whether or not to fill the content of the 
            'new_coreferring_mention_extent_pairs' collection output with the pairs of 
            (antecedent mention extent, subsequent mention extent) corresponding to the pair of mentions for 
            which a coreference link was created by the processing of this function.

    Returns:
        a (mentions, coreference_partition, constraints, new_coreferring_mention_extent_pairs) 
        tuple, where:
            - 'mentions' is the input 'mentions' value
            - 'coreference_partition' is the input 'coreference_partition' value, having potentially been 
              updated by the processing of this function
            - 'constraints' is the input 'constraints' value, , having potentially been updated by the 
              processing of this function
            - 'new_coreferring_mention_extent_pairs' is a collection of 
              (antecedent mention extent, subsequent mention extent) pairs, corresponding to the pair of 
              mentions for which a coreference link was created by the processing of this function, if the 
              'output_new_pairs' parameter value was True. If it was False, then this collection will be empty.
    """
    new_coreferring_mention_extent_pairs = []
    # Variant of strict head match
    # TODO: (legacy)
    return mentions, coreference_partition, constraints, new_coreferring_mention_extent_pairs


def _sieves_pass8(document, mentions, coreference_partition, constraints, filter_anaphoric=False, output_new_pairs=False):
    """ Function implementing the application of the eighth multi-sieves rule on the input document 
    and mentions, so as to properly update the input coreference partition and constraints' respective 
    states.
    
    Args:
        document: Document instance, the document that the mentions to partition originate from
        mentions: collection of Mentions instances associated to the input document, representing 
            the universe of the partition to be built
        coreference_partition: CoreferencePartition instance, the current state of the coreference 
            partition being built by the ordered application of the multi-sieves functions. Its universe 
            should be made out of all the mention of the input collection.
        constraints: a set containing pairs of (antecedent mention extent, subsequent mention extent), 
            the current state of the constraints to be respected when trying to build the coreference partition.
        filter_anaphoric: boolean; if True, may skip some search to find coreference link between 
            mentions, if the subsequent mention of the would-be pair is already known to be anaphoric at the 
            beginning of the search.
        output_new_pairs: boolean, whether or not to fill the content of the 
            'new_coreferring_mention_extent_pairs' collection output with the pairs of 
            (antecedent mention extent, subsequent mention extent) corresponding to the pair of mentions for 
            which a coreference link was created by the processing of this function.

    Returns:
        a (mentions, coreference_partition, constraints, new_coreferring_mention_extent_pairs) 
        tuple, where:
            - 'mentions' is the input 'mentions' value
            - 'coreference_partition' is the input 'coreference_partition' value, having potentially been 
              updated by the processing of this function
            - 'constraints' is the input 'constraints' value, , having potentially been updated by the 
              processing of this function
            - 'new_coreferring_mention_extent_pairs' is a collection of 
              (antecedent mention extent, subsequent mention extent) pairs, corresponding to the pair of 
              mentions for which a coreference link was created by the processing of this function, if the 
              'output_new_pairs' parameter value was True. If it was False, then this collection will be empty.
    """
    new_coreferring_mention_extent_pairs = []
    # Proper head word match
    # Strict head match
    named_entities = document.named_entities
    for j in range(1, len(mentions)):
        m_j = mentions[j]
        # Do not process pronouns
        if MENTION_FEATURES.is_expanded_pronoun(m_j):
            continue
        # Mention selection
        if filter_anaphoric and _is_anaphoric(m_j, coreference_partition):
            continue
        if not m_j.head_tokens[-1].POS_tag == POS_tags.PROPER_NOUN_SINGULAR_TAG:
            continue
        location_set_j = set()
        for named_entity in named_entities:
            ne_type = named_entity.type
            if m_j.includes(named_entity) and ne_type in NE_tags.LOCATION_NAMED_ENTITY_TYPE_TAGS or ne_type in NE_tags.GPE_NAMED_ENTITY_TYPE_TAGS:
                location_set_j.add(named_entity.raw_text.lower())
        text_j = m_j.head_raw_text.lower()
        for i in range(j):
            m_i = mentions[i]
            # Do not process pronouns
            if MENTION_FEATURES.is_expanded_pronoun(m_i):
                continue
            text_i = m_i.head_raw_text.lower() # => m_i is NNP
            if text_i == text_j:
                if not _is_constrained(constraints, coreference_partition, m_i, m_j):
                    # Assert not one within the other
                    not_within = not (m_i.includes(m_j) or m_j.includes(m_i))
                    # Assert no location mismatches
                    location_match = True
                    location_set_i = set()
                    for named_entity in named_entities:
                        if m_i.includes(named_entity) and ne_type in NE_tags.LOCATION_NAMED_ENTITY_TYPE_TAGS or ne_type in NE_tags.GPE_NAMED_ENTITY_TYPE_TAGS:
                            location_set_i.add(named_entity.raw_text.lower())
                    location_match = location_set_i == location_set_j
                    # Assert no numeric mismatches
                    # TODO: (legacy)
                    if not_within and location_match:
                        coreference_partition.join(m_i.extent, m_j.extent)
                        if output_new_pairs:
                            new_coreferring_mention_extent_pairs.append((m_i.extent,m_j.extent))
                        break
    return mentions, coreference_partition, constraints, new_coreferring_mention_extent_pairs


def _sieves_pass9(document, mentions, coreference_partition, constraints, filter_anaphoric=False, output_new_pairs=False):
    """ Function implementing the application of the ninth multi-sieves rule on the input document 
    and mentions, so as to properly update the input coreference partition and constraints' respective 
    states.
    
    Args:
        document: Document instance, the document that the mentions to partition originate from
        mentions: collection of Mentions instances associated to the input document, representing 
            the universe of the partition to be built
        coreference_partition: CoreferencePartition instance, the current state of the coreference 
            partition being built by the ordered application of the multi-sieves functions. Its universe 
            should be made out of all the mention of the input collection.
        constraints: a set containing pairs of (antecedent mention extent, subsequent mention extent), 
            the current state of the constraints to be respected when trying to build the coreference partition.
        filter_anaphoric: boolean; if True, may skip some search to find coreference link between 
            mentions, if the subsequent mention of the would-be pair is already known to be anaphoric at the 
            beginning of the search.
        output_new_pairs: boolean, whether or not to fill the content of the 
            'new_coreferring_mention_extent_pairs' collection output with the pairs of 
            (antecedent mention extent, subsequent mention extent) corresponding to the pair of mentions for 
            which a coreference link was created by the processing of this function.

    Returns:
        a (mentions, coreference_partition, constraints, new_coreferring_mention_extent_pairs) 
        tuple, where:
            - 'mentions' is the input 'mentions' value
            - 'coreference_partition' is the input 'coreference_partition' value, having potentially been 
              updated by the processing of this function
            - 'constraints' is the input 'constraints' value, , having potentially been updated by the 
              processing of this function
            - 'new_coreferring_mention_extent_pairs' is a collection of 
              (antecedent mention extent, subsequent mention extent) pairs, corresponding to the pair of 
              mentions for which a coreference link was created by the processing of this function, if the 
              'output_new_pairs' parameter value was True. If it was False, then this collection will be empty.
    """
    new_coreferring_mention_extent_pairs = []
    # Relaxed head match
    # TODO: (legacy)
    return mentions, coreference_partition, constraints, new_coreferring_mention_extent_pairs


def _sieves_pass10(document, mentions, coreference_partition, constraints, filter_anaphoric=False, output_new_pairs=False):
    """ Function implementing the application of the tenth multi-sieves rule on the input document 
    and mentions, so as to properly update the input coreference partition and constraints' respective 
    states.
    
    Args:
        document: Document instance, the document that the mentions to partition originate from
        mentions: collection of Mentions instances associated to the input document, representing 
            the universe of the partition to be built
        coreference_partition: CoreferencePartition instance, the current state of the coreference 
            partition being built by the ordered application of the multi-sieves functions. Its universe 
            should be made out of all the mention of the input collection.
        constraints: a set containing pairs of (antecedent mention extent, subsequent mention extent), 
            the current state of the constraints to be respected when trying to build the coreference partition.
        filter_anaphoric: boolean; if True, may skip some search to find coreference link between 
            mentions, if the subsequent mention of the would-be pair is already known to be anaphoric at the 
            beginning of the search.
        output_new_pairs: boolean, whether or not to fill the content of the 
            'new_coreferring_mention_extent_pairs' collection output with the pairs of 
            (antecedent mention extent, subsequent mention extent) corresponding to the pair of mentions for 
            which a coreference link was created by the processing of this function.

    Returns:
        a (mentions, coreference_partition, constraints, new_coreferring_mention_extent_pairs) 
        tuple, where:
            - 'mentions' is the input 'mentions' value
            - 'coreference_partition' is the input 'coreference_partition' value, having potentially been 
              updated by the processing of this function
            - 'constraints' is the input 'constraints' value, , having potentially been updated by the 
              processing of this function
            - 'new_coreferring_mention_extent_pairs' is a collection of 
              (antecedent mention extent, subsequent mention extent) pairs, corresponding to the pair of 
              mentions for which a coreference link was created by the processing of this function, if the 
              'output_new_pairs' parameter value was True. If it was False, then this collection will be empty.
    """
    new_coreferring_mention_extent_pairs = []
    set_mention_extents = set(mention.extent for mention in mentions)
    extent2mention = document.extent2mention
    # Pronominal coreference resolution
    sentences = document.sentences
    for j in range(0, len(sentences)):
        sent_j = sentences[j]
        for m_j in sent_j.mentions:
            # Mention selection
            if m_j.extent not in set_mention_extents:
                continue
            if _is_anaphoric(m_j, coreference_partition): # Already connected to a previous mention # Always use filter here!
                continue
            if m_j.raw_text.lower() in ("this","that"): # Difficulties with this/that
                continue
            head_text_j = m_j.head_raw_text.lower()
            if not MENTION_FEATURES.is_expanded_pronoun(m_j) or head_text_j in INDEFINITE_PRONOUN_MENTIONS or head_text_j in WH_PRONOUN_MENTIONS:
                continue
            gender_j = m_j.gender
            number_j = m_j.number
            person_j = head_text_j in EXPANDED_PERSONAL_PRONOUN_MENTIONS
            animate_j = head_text_j in EXPANDED_ANIMATE_PRONOUN_MENTIONS
            inanimate_j = head_text_j in EXPANDED_INANIMATE_PRONOUN_MENTIONS
            ne_type_j = set()
            if head_text_j in EXPANDED_MONEY_PERCENT_NUMBER_PRONOUN_MENTIONS:
                ne_type_j.add("MONEY")
                ne_type_j.add("PERCENT")
                ne_type_j.add("NUMBER")
            if head_text_j in DATE_TIME_PRONOUN_MENTIONS:
                ne_type_j.add("DATE")
                ne_type_j.add("TIME")
            if head_text_j in EXPANDED_ORGANIZATION_PRONOUN_MENTIONS:
                ne_type_j.add("ORG")
            if head_text_j in EXPANDED_GPE_PRONOUN_MENTIONS:
                ne_type_j.add("GPE")
            if head_text_j in EXPANDED_LOCATION_PRONOUN_MENTIONS:
                ne_type_j.add("LOC")
            if head_text_j in EXPANDED_FACILITY_VEHICLE_WEAPON_PRONOUN_MENTIONS:
                ne_type_j.add("FAC")
            sentence_extent2breadth_first_sorted_mentions = {}
            for i in range(j,max(-1,j-3),-1): # search in 3 previous sentences
                sent_i = sentences[i]
                found = False
                if sent_i.extent not in sentence_extent2breadth_first_sorted_mentions:
                    sentence_extent2breadth_first_sorted_mentions[sent_i.extent] = SENTENCE_FEATURES.get_breadth_first_sorted_mentions(sent_i)
                for m_i in sentence_extent2breadth_first_sorted_mentions[sent_i.extent]:
                    if m_i.extent not in set_mention_extents or m_i == m_j:
                        continue
                    e_i = coreference_partition.get_entity(m_i.extent)
                    gender_i = set()
                    number_i = set()
                    person_i = None
                    animate_i = False
                    ne_type_i = set()
                    for mention_extent in e_i:
                        mention = extent2mention[mention_extent]
                        gender_i.add(mention.gender)
                        number_i.add(mention.number)
                        if MENTION_FEATURES.is_expanded_pronoun(mention):
                            if mention.raw_text.lower() in EXPANDED_PERSONAL_PRONOUN_MENTIONS:
                                person_i = True
                            else:
                                person_i = False
                        if MENTION_FEATURES.is_animated(mention):
                            animate_i = True
                        
                        if mention.named_entity is not None:
                            ne_type_i.add(mention.named_entity.type)
                    corefer = True
                    if gender_j != UNKNOWN_VALUE_TAG and UNKNOWN_VALUE_TAG not in gender_i and gender_j not in gender_i:
                        corefer = False
                    if number_j != UNKNOWN_VALUE_TAG and UNKNOWN_VALUE_TAG not in number_i and number_j not in number_i:
                        corefer = False
                    if person_i is not None and person_i != person_j:
                        corefer = False
                    if inanimate_j and animate_i:
                        corefer = False
                    if animate_j and not animate_i:
                        corefer = False
                    if len(ne_type_j) != 0 and len(ne_type_i) != 0 and ne_type_j.isdisjoint(ne_type_i):
                        corefer = False
                    if corefer:
                        found = True
                        coreference_partition.join(m_i.extent, m_j.extent)
                        if output_new_pairs:
                            new_coreferring_mention_extent_pairs.append((m_i.extent,m_j.extent))
                        break
                if found:
                    break
    return mentions, coreference_partition, constraints, new_coreferring_mention_extent_pairs




###############
#### UTILS ####
###############
def _add_constraint(constraints, m1, m2):
    """ Adds to the input constraints set a constraint corresponding to the 
    (antecedent mention, subsequent mention) pair made of the input mentions (the antecedent mention 
    is the mention that is the first one that is met in the document's reading order).
    
    Args:
        constraints: set
        m1: Mention instance
        m2: Mention instance
    """
    # order mention pair
    if m1 <= m2:
        constraints.add((m1.extent,m2.extent))
    else:
        constraints.add((m2.extent,m1.extent))

def _is_constrained(constraints, coreference_partition, m1, m2):
    """ Determines whether or not there exist a constraint in the input set of constraint that would 
    oppose the merging of each current coreference entity respectively associated to the input mentions.
    
    Args:
        constraints: set, the constraints to consider
        coreference_partition: CoreferencePartition instance, the coreference partition where the 
            current state of the respective coreference entity associated to each input mention is defined
        m1: Mention instance
        m2: Mention instance
    """
    entity1 = coreference_partition.get_entity(m1.extent)
    entity2 = coreference_partition.get_entity(m2.extent)
    for extents in product(entity1, entity2):
        if extents in constraints:
            return True
    return False

def _is_anaphoric(m, coreference_partition):
    """ Checks whether or not a mention is anaphoric according to the state of the input coreference 
    partition object.
    
    Args:
        m: a Mention instance
        coreference_partition: a CoreferencePartition instance

    Returns:
        a boolean 
    """
    entity = coreference_partition.get_entity(m.extent)
    if sorted(entity)[0] == m.extent:
        return False
    return True
