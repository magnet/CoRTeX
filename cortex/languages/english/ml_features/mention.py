# -*- coding: utf-8 -*-

"""
Defines functions to compute values or data from a Mention instance
"""

"""
Here, a 'feature' is an instance of the 'Feature' class, defined in the 'ml_features' module.

The aim of this module is to define the base functions that will be used to define features that 
take a Mention instance as an input (i.e. 'features on mentions').

The creation of the features themselves does not occur here, because this creation can be parametrized 
by the user, and because the functions defined here can be used in other contexts, such as when 
creating features that take pairs of mentions as an input.

Most of the features that can be created use functions that can be used by other feature creation 
processes, so those functions are shared by being put in this very module.
Those functions are embedded in a tuple of data (referred later in the code as 'mention_feature_data', 
since those function are defined on a 'Mention' instance input). The first element of the tuple is 
a string defining the type of feature that this function is destined to be used by (such as 'numerical', 
'categorical' or 'multi_numeric'), the second element is the function itself, and the rest of the tuple
elements are elements that are specific to each feature instance type (such as 'column_names' for 
'multi_numeric' features, or 'possible_values' for 'categorical' features).
"""

__all_ = ["create_mention_feature_from_data",
          "get_sieve1",
          "get_sieve1_pos_neg",
          ]

from scipy import sparse
import numpy as np
import itertools

from cortex.tools.ml_features import (NumericFeature, CategoricalFeature, MultiNumericFeature, 
                                        quantize_numeric_feature, NUMERIC_FEATURE_TYPE, 
                                        MULTI_NUMERIC_FEATURE_TYPE, CATEGORICAL_FEATURE_TYPE)
from cortex.api.coreference_partition import CoreferencePartition
from cortex.languages.english.mention_features import ENGLISH_MENTION_FEATURES as MENTION_FEATURES
from cortex.languages.english.rules.sieves import _sieves_pass1 as sieve_pass1
from cortex.parameters.mention_data_tags import (FIRST_PERSON_TAG, SECOND_PERSON_TAG, 
                                                  THIRD_PERSON_TAG, SINGULAR_NUMBER_TAG, 
                                                  PLURAL_NUMBER_TAG,  UNKNOWN_VALUE_TAG,
                                                  GRAM_TYPE_TAGS, GRAM_SUBTYPE_TAGS)

from cortex.languages.english.knowledge.lexicon import (VERB_TO_NOUN,  EXPANDED_MONEY_PERCENT_NUMBER_PRONOUN_MENTIONS, 
                                                        DATE_TIME_PRONOUN_MENTIONS, EXPANDED_ORGANIZATION_PRONOUN_MENTIONS, 
                                                        EXPANDED_GPE_PRONOUN_MENTIONS, EXPANDED_LOCATION_PRONOUN_MENTIONS, 
                                                        EXPANDED_FACILITY_VEHICLE_WEAPON_PRONOUN_MENTIONS,
                                                        REFLEXIVE_PRONOUN_MENTIONS, QUANTIFIERS, POSSESSIVE_DETERMINERS, 
                                                        RELATIVE_PRONOUN_MENTIONS,
                                                        )

from cortex.languages.english.parameters.ontonotes_v5_constituent_tags import (NOUN_SINGULAR_OR_MASS_TAG, 
                                                                                 COMMON_NOUN_POS_TAGS, 
                                                                                  NOUN_PLURAL_TAG, WH_PRONOUN_TAG, 
                                                                                  POSSESSIVE_WH_PRONOUN_TAG, 
                                                                                  DETERMINER_TAG, SENTENCE_TAGS)
from cortex.languages.english.parameters.ontonotes_v5_named_entities_data_tags import (MONEY_PERCENT_NUMBER_NAMED_ENTITY_TYPE_TAGS, 
                                                                                         DATE_NAMED_ENTITY_TYPE_TAGS,
                                                                                         ORGANIZATION_NAMED_ENTITY_TYPE_TAGS, 
                                                                                         LOCATION_NAMED_ENTITY_TYPE_TAGS, 
                                                                                         GPE_NAMED_ENTITY_TYPE_TAGS, 
                                                                                         FACILITY_VEHICLE_WEAPON_NAMED_ENTITY_TYPE_TAGS,
                                                                                         PERSON_NAMED_ENTITY_TYPE_TAGS
                                                                                         )



#################
##### Utils #####
#################
def create_mention_feature_from_data(get_mention_from_sample_fct, feature_data, name, strict=False, 
                                     quantification_threshold_values=None):
    """ Creates a feature that is destined to take a sample as an input, a sample that only needs to 
    provide a mention for the feature to compute its value.
    
    Args:
        get_mention_from_sample_fct: callable, a function that defines how to extract, from a sample, 
            the Mention instance that needs to be input in the function to compute the feature value: it takes 
            a sample as an input, and outputs a Mention instance
        feature_data: tuple ('feature_type', 'function', 'other_data'), see module's documentation
        name: name to be given to the created feature
        strict: boolean, whether or not to raise an Exception when a CategoricalFeature instance 
            encounters a value that it does not know of
        quantification_threshold_values: collection of numeric value to use to define the bins to 
            use when thresholding a Numeric or MultiNumeric Feature instance (in which case the feature is 
            transformed into a CategoricalFeature instance), or None. If None, no thresholding will happen.

    Returns:
        a _Feature child class instance, the created feature
    """
    feature_type = feature_data[0]
    if feature_type == NUMERIC_FEATURE_TYPE:
        _compute_fct_, can_be_quantized = feature_data[1:]
        _compute_fct = lambda sample: _compute_fct_(get_mention_from_sample_fct(sample))
        feature = NumericFeature(name, _compute_fct)
        if can_be_quantized and quantification_threshold_values is not None:
            feature = quantize_numeric_feature(feature, quantification_threshold_values, new_name=name)
    elif feature_type == MULTI_NUMERIC_FEATURE_TYPE:
        _compute_fct_, column_names = feature_data[1:]
        _compute_fct = lambda sample, matrix=None, row_id=0, column_offset=0: _compute_fct_(get_mention_from_sample_fct(sample), matrix=matrix, row_id=row_id, column_offset=column_offset)
        feature = MultiNumericFeature(name, _compute_fct, column_names)
    elif feature_type == CATEGORICAL_FEATURE_TYPE:
        _compute_fct_, possible_values = feature_data[1:]
        _compute_fct = lambda sample: _compute_fct_(get_mention_from_sample_fct(sample))
        feature = CategoricalFeature(name, _compute_fct, possible_values, strict=strict)
    else:
        msg = "Bad feature type '{}', must be one of the following: {}."
        msg = msg.format(feature_type, (NUMERIC_FEATURE_TYPE, MULTI_NUMERIC_FEATURE_TYPE, CATEGORICAL_FEATURE_TYPE))
        raise ValueError(msg)
    return feature

# Sieve 1 constraints: functions to be used when creating a feature whose output values needs info regarding application of first sieve rule on mentions
def get_sieve1(mention):
    """ Gets positive edges set and negative edges set produced by the application of the first sieve 
    rule on the document of the input mention.
    
    Args:
        mention: the mention on whose document to apply the first sieve rule

    Returns:
        a (positive_edges, negative_edges) pair, 
        where:
            - 'positive_edges' is a set of mention extent pairs corresponding to pairs of mentions (the first 
              extent being that of the antecedent mention of the pair), representing coreference links (between 
              mentions) that must exist, according to the application of the first sieve rule
            - 'negative_edges' is a set of mention extent pairs corresponding to pairs of mentions (the first 
              extent being that of the antecedent mention of the pair), representing coreference links (between 
              mentions) that must not exist, according to the application of the first sieve rule
    """
    document = mention.document
    mentions = document.mentions
    coreference_partition = CoreferencePartition(mention_extents=(m.extent for m in mentions))
    constraints = set()
    _, _, constraints, new_coref_pairs = sieve_pass1(document, mentions, coreference_partition, 
                                                     constraints, filter_anaphoric=False, 
                                                     output_new_pairs=True) #mentions, partition
    positive_edges = set(new_coref_pairs)
    negative_edges = set(constraints)
    return (positive_edges, negative_edges)

def get_sieve1_pos_neg(focus_mention, other_mention):
    """ Gets information regarding whether or not the mention pair made out of the input mentions is 
    a positive or negative edge according to the application of the first sieve rule on the document of the 
    input 'focus' mention.
    
    Args:
        focus_mention: the mention on whose document to apply the first sieve rule
        other_mention: the candidate subsequent mention to the focus mention, used to create the 
            mention pair to fetch information about.

    Returns:
        a (pos, neg) pair, 
        where:
            - 'pos' is a boolean value specifying whether or not the mentions pair made out of the input 
              mentions is a positive edge according to the first sieve rule
            - 'neg' is a boolean value specifying whether or not the mentions pair made out of the input 
              mentions is a negative edge according to the first sieve rule
    """
    positive_edges, negative_edges = get_sieve1(focus_mention) #negative_edges
    extent_pair = (focus_mention.extent, other_mention.extent)
    pos = extent_pair in positive_edges
    neg = extent_pair in negative_edges
    return pos, neg


#############################
## Feature data definition ##
#############################
#### Word count mention features ###
can_be_quantized = True
number_words_data = (NUMERIC_FEATURE_TYPE, lambda mention: len(mention.tokens), can_be_quantized)

#### Linguistic form mention features ####
def _LinguisticForm_fct(mention, matrix=None, row_id=0, column_offset=0):
    """ Function used to vectorize linguistic information about a mention.
    The respective name of the columns in which such info is encoded are the following:
    "IsPronoun"
    "IsSpeechPronoun"
    "IsReflexivePronoun"
    "IsName"
    "IsShortName"
    "IsDefiniteDescription"
    "IsShortDefiniteDescription"
    "IsIndefiniteDescription"
    "IsQuantifiedDescription"
    "IsPossessiveDescription"
    "ContainsOf"
    "IsPossessiveCase"
    "IsBareNoun"
    "ContainsRelativePronoun"
    "ContainsWhPronoun"
    """
    if matrix is None:
        result = sparse.lil_matrix((1,15), dtype=np.int)
        _LinguisticForm_fct(mention, matrix=result, row_id=0, column_offset=0)
        return result.tocsr()
    tokens = mention.tokens
    lower_text = mention.raw_text.lower()
    # Pronoun
    if MENTION_FEATURES.is_expanded_pronoun(mention):
        matrix[row_id,column_offset+0] = 1
    # Speech pronoun
    if lower_text in ("i","we"):
        matrix[row_id,column_offset+1] = 1
    # Reflexive pronoun
    if lower_text in REFLEXIVE_PRONOUN_MENTIONS:
        matrix[row_id,column_offset+2] = 1
    # Proper name
    if MENTION_FEATURES.is_name(mention):
        matrix[row_id,column_offset+3] = 1
        # Short proper name
        if len(tokens) == 1:
            matrix[row_id,column_offset+4] = 1
    # Definite description
    if MENTION_FEATURES.is_nominal(mention) and lower_text.startswith("the"):
        matrix[row_id,column_offset+5] = 1
        # Short definite description
        nb_nouns = 0
        for token in tokens[1:]:
            if token.POS_tag in COMMON_NOUN_POS_TAGS:
                nb_nouns += 1
        if nb_nouns == 1:
            matrix[row_id,column_offset+6] = 1
    # Indefinite description
    if MENTION_FEATURES.is_nominal(mention):
        tokens = tokens
        token = tokens[0]
        if (token.POS_tag == DETERMINER_TAG and token.raw_text.lower() in ("a", "an")) or token.POS_tag == NOUN_PLURAL_TAG:
            matrix[row_id,column_offset+7] = 1
    # Quantified description
    for quant in QUANTIFIERS:
        if lower_text.startswith(quant):
            matrix[row_id,column_offset+8] = 1
            break
    # Possessive description
    if tokens[0].raw_text.lower() in POSSESSIVE_DETERMINERS:
        matrix[row_id,column_offset+9] = 1
    # Contains "of"
    for token in tokens:
        if token.raw_text.lower() in ("of",):
            matrix[row_id,column_offset+10] = 1
            break
    # Possessive case
    if any(s in lower_text for s in ("'s", "s'")):
        matrix[row_id,column_offset+11] = 1
    # Bare noun
    if MENTION_FEATURES.is_nominal(mention) and tokens[0].POS_tag == NOUN_PLURAL_TAG and len(tokens) == 1: # TODO: (legacy) make it more general (ex: 'great things')
        matrix[row_id,column_offset+12] = 1
    # Contains a relative pronoun
    for token in tokens:
        if token.raw_text.lower() in RELATIVE_PRONOUN_MENTIONS:
            matrix[row_id,column_offset+13] = 1
            break
    # Contains Wh-pronoun
    wh_pronoun_tags = (WH_PRONOUN_TAG, POSSESSIVE_WH_PRONOUN_TAG,)
    for token in tokens:
        if token.POS_tag in wh_pronoun_tags:
            matrix[row_id,column_offset+14] = 1
            break
    return matrix
column_names = ("IsPronoun", "IsSpeechPronoun", "IsReflexivePronoun", "IsName", "IsShortName", 
                  "IsDefiniteDescription", "IsShortDefiniteDescription", "IsIndefiniteDescription", 
                  "IsQuantifiedDescription", "IsPossessiveDescription", "ContainsOf", 
                  "IsPossessiveCase", "IsBareNoun", "ContainsRelativePronoun", "ContainsWhPronoun")
linguistic_form_data = (MULTI_NUMERIC_FEATURE_TYPE, _LinguisticForm_fct, column_names)





#### Position in text mention features ####
# Sentence index
def _SentenceIndex_fct(mention):
    return MENTION_FEATURES.mention_sentence_index(mention)
can_be_quantized = True
sentence_index_data = (NUMERIC_FEATURE_TYPE, _SentenceIndex_fct, can_be_quantized)
def _NormalizedSentenceIndex_fct(mention):
    return (MENTION_FEATURES.mention_sentence_index(mention)+1) / len(mention.document.sentences)
can_be_quantized = False
normalized_sentence_index_data = (NUMERIC_FEATURE_TYPE, _NormalizedSentenceIndex_fct, can_be_quantized)

# Mention rank in sentence
def _AtSentenceRank_fct(mention):
    return MENTION_FEATURES.mention_in_sentence_index(mention)
can_be_quantized = True
at_sentence_rank_data = (NUMERIC_FEATURE_TYPE, _AtSentenceRank_fct, can_be_quantized)
def _NormalizedAtSentenceRank_fct(mention):
    return (MENTION_FEATURES.mention_in_sentence_index(mention)+1) / len(mention.sentence.mentions)
can_be_quantized = False
normalized_at_sentence_rank_data = (NUMERIC_FEATURE_TYPE, _NormalizedAtSentenceRank_fct, can_be_quantized)

# Mention inverse rank in sentence
def _AtSentenceInverseRank_fct(mention):
    return len(mention.sentence.mentions) - (MENTION_FEATURES.mention_in_sentence_index(mention)+1)
can_be_quantized = True
at_sentence_inverse_rank_data = (NUMERIC_FEATURE_TYPE, _AtSentenceInverseRank_fct, can_be_quantized)
def _NormalizedAtSentenceInverseRank_fct(mention):
    return 1 - (MENTION_FEATURES.mention_in_sentence_index(mention)+1) / len(mention.sentence.mentions)
can_be_quantized = False
normalized_at_sentence_inverse_rank_data = (NUMERIC_FEATURE_TYPE, _NormalizedAtSentenceInverseRank_fct, can_be_quantized)

# Appears in which sentence
def _SentencePosition_fct(mention, matrix=None, row_id=0, column_offset=0):
    """ Function that vectorizes information about the in-document position of the input mention's 
    sentence; whether it is the first or the last sentence, or whether it is located in the second 
    or third third of the document's sentences.
    The respective name of the columns in which such info is encoded are the following:
    "SentenceFirstPosition"
    "SentenceLastPosition"
    "SentenceMiddlePosition"
    "SentenceEndPosition"
    """
    if matrix is None:
        result = sparse.lil_matrix((1,4), dtype=np.int)
        _SentencePosition_fct(mention, matrix=result, row_id=0, column_offset=0)
        return result.tocsr()
    sentences = mention.document.sentences
    index = MENTION_FEATURES.mention_sentence_index(mention)
    nb_sentences = len(sentences)
    index_first = 0
    index_last = nb_sentences-1
    index_1on3 = int(nb_sentences / 3.0) # First third
    index_2on3 = int(2.0 * nb_sentences / 3.0) # Second third
    # First position
    if index == index_first:
        matrix[row_id,column_offset+0] = 1
    # Last position
    if index == index_last:
        matrix[row_id,column_offset+1] = 1
    # Middle position (1/3 - 2/3)
    if index_1on3 <= index < index_2on3:
        matrix[row_id,column_offset+2] = 1
    # End position (2/3 - 3/3)
    if index_2on3 <= index:
        matrix[row_id,column_offset+3] = 1
column_names = ("SentenceFirstPosition", "SentenceLastPosition", "SentenceMiddlePosition", "SentenceEndPosition")
sentence_position_data = (MULTI_NUMERIC_FEATURE_TYPE, _SentencePosition_fct, column_names)

#### Relation to other mentions mention features ####
# Is embedding another mention / embedded by another mention
def _IsEmbedd_fct(mention, matrix=None, row_id=0, column_offset=0):
    """ Function that vectorizes information about whether the input mention is embedded in another 
    mention, or is itself embedding another mention.
    The respective name of the columns in which such info is encoded are the following:
    "IsEmbedding"
    "IsEmbedded"
    """
    if matrix is None:
        result = sparse.lil_matrix((1,2), dtype=np.int)
        _IsEmbedd_fct(mention, matrix=result, row_id=0, column_offset=0)
        return result.tocsr()
    s,e = mention.extent
    is_embedding = False
    is_embedded = False
    for m in mention.sentence.mentions:
        # Skip the input mention
        if m.extent == mention.extent:
            continue
        # Change boolean value if condition is met
        s1,e1 = m.extent
        if (s1 <= s and e < e1) or (s1 < s and e <= e1):
            is_embedded = True
        if (s <= s1 and e1 < e) or (s < s1 and e1 <= e):
            is_embedding = True
        # Leave loop as soon as both boolean values were changed
        if is_embedding and is_embedded:
            break
    matrix[row_id,column_offset+0:column_offset+2] = int(is_embedding), int(is_embedded)
column_names = ("IsEmbedding", "IsEmbedded")
is_embedd_data = (MULTI_NUMERIC_FEATURE_TYPE, _IsEmbedd_fct, column_names)

# Matching with previous mentions
def _StringMatchWithPrevious_fct(mention, matrix=None, row_id=0, column_offset=0):
    """ Function that vectorizes information about whether the input mention's raw text matches in 
    some way the raw text of another, previous mention.
    The respective name of the columns in which such info is encoded are the following:
    "StringMatch"
    "StringIncluded"
    "HeadStringMatch"
    """
    if matrix is None:
        result = sparse.lil_matrix((1,3), dtype=np.int)
        _StringMatchWithPrevious_fct(mention, matrix=result, row_id=0, column_offset=0)
        return result.tocsr()
    val1 = False
    val2 = False
    val3 = False
    for m in mention.document.mentions:
        # Leave the loop as soon as the input mention has been seen and do not risk overlapping with the following mentions anymore
        if m.extent == mention.extent:
            continue
        elif all(v1 < v2 for v1, v2 in zip(mention.extent, m.extent)):
            break
        # String match
        lower_text =  mention.raw_text.lower()
        lower_text1 = m.raw_text.lower()
        if lower_text == lower_text1:
            val1 = True
        if lower_text in lower_text1:
            val2 = True
        # Head match
        lower_head_text = mention.head_raw_text.lower()
        lower_head_text1 = m.head_raw_text.lower()
        if lower_head_text == lower_head_text1:
            val3 = True
    matrix[row_id, column_offset+0:column_offset+3] = int(val1), int(val2), int(val3)
column_names = ("StringMatch", "StringIncluded", "HeadStringMatch")
string_match_with_previous_data = (MULTI_NUMERIC_FEATURE_TYPE, _StringMatchWithPrevious_fct, column_names)
# Matching with other mentions
def _StringMatchOther_fct(mention, matrix=None, row_id=0, column_offset=0):
    """ Function that vectorizes information about whether the input mention's raw text matches in 
    some way the raw text of another mention.
    The respective name of the columns in which such info is encoded are the following:
    "StringMatch"
    "StringIncluded"
    "HeadStringMatch"
    """
    if matrix is None:
        result = sparse.lil_matrix((1,3), dtype=np.int)
        _StringMatchOther_fct(mention, matrix=result, row_id=0, column_offset=0)
        return result.tocsr()
    val1 = False
    val2 = False
    val3 = False
    for m in mention.document.mentions:
        # Skip the input mention
        if m.extent == mention.extent:
            continue
        # String match
        lower_text =  mention.raw_text.lower()
        lower_text1 = m.raw_text.lower()
        if lower_text == lower_text1:
            val1 = True
        if lower_text in lower_text1:
            val2 = True
        # Head match
        lower_head_text = mention.head_raw_text.lower()
        lower_head_text1 = m.head_raw_text.lower()
        if lower_head_text == lower_head_text1:
            val3 = True
    matrix[row_id, column_offset+0:column_offset+3] = int(val1), int(val2), int(val3)
column_names = ("StringMatch", "StringIncluded", "HeadStringMatch")
string_match_with_other_data = (MULTI_NUMERIC_FEATURE_TYPE, _StringMatchOther_fct, column_names)

# Apposition with other mention
def _InApposition_fct(mention):
    for m in mention.sentence.mentions:
        # Skip the input mention
        if m.extent == mention.extent:
            continue
        if (MENTION_FEATURES.is_nominal(m) or MENTION_FEATURES.is_nominal(mention)) and (MENTION_FEATURES.is_appositive(m, mention) or MENTION_FEATURES.is_appositive(mention, m)): # one nominal, at least
            return int(True)
    return int(False)
can_be_quantized = False
in_apposition_data = (NUMERIC_FEATURE_TYPE, _InApposition_fct, can_be_quantized)

# Acronym shared with a previous mention
def _AliasWithPrevious_fct(mention):
    val = False
    for m in mention.document.mentions:
        # Leave the loop as soon as the input mention has been seen and do not risk overlapping with the following mentions anymore
        if m.extent == mention.extent:
            continue
        elif all(v1 < v2 for v1, v2 in zip(mention.extent, m.extent)):
            break
        if MENTION_FEATURES.is_name(m) and MENTION_FEATURES.is_name(mention)\
         and m.raw_text != mention.raw_text and MENTION_FEATURES.abbrev(m) == MENTION_FEATURES.abbrev(mention):
            val = True
            break
    return int(val)
can_be_quantized = False
alias_with_previous_data = (NUMERIC_FEATURE_TYPE, _AliasWithPrevious_fct, can_be_quantized)
# Acronym shared with another mention
def _AliasWithOther_fct(mention):
    val = False
    for m in mention.document.mentions:
        # Skip the input mention
        if m.extent == mention.extent:
            continue
        if MENTION_FEATURES.is_name(m) and MENTION_FEATURES.is_name(mention)\
         and m.raw_text != mention.raw_text and MENTION_FEATURES.abbrev(m) == MENTION_FEATURES.abbrev(mention):
            val = True
            break
    return int(val)
can_be_quantized = False
alias_with_other_data = (NUMERIC_FEATURE_TYPE, _AliasWithOther_fct, can_be_quantized)


#### Syntactic mention features ####
# Number of NP above mention
def _NPAboveNb_fct(mention):
    return MENTION_FEATURES.count_NP_above(mention)
can_be_quantized = True
np_above_nb_data = (NUMERIC_FEATURE_TYPE, _NPAboveNb_fct, can_be_quantized)
def _NormalizedNPAboveNb_fct(mention):
    return MENTION_FEATURES.count_NP_above(mention) / MENTION_FEATURES.count_above(mention)
can_be_quantized = False
normalized_np_above_nb_data = (NUMERIC_FEATURE_TYPE, _NormalizedNPAboveNb_fct, can_be_quantized)

# Number of PP above mention
def _PPAboveNb_fct(mention):
    return MENTION_FEATURES.count_PP_above(mention)
can_be_quantized = True
pp_above_nb_data = (NUMERIC_FEATURE_TYPE, _PPAboveNb_fct, can_be_quantized)
def _NormalizedPPAboveNb_fct(mention):
    return MENTION_FEATURES.count_PP_above(mention) / MENTION_FEATURES.count_above(mention)
can_be_quantized = False
normalized_pp_above_nb_data = (NUMERIC_FEATURE_TYPE, _NormalizedPPAboveNb_fct, can_be_quantized)

# Number of VP above mention
def _VPAboveNb_fct(mention):
    return MENTION_FEATURES.count_VP_above(mention)
can_be_quantized = True
vp_above_nb_data = (NUMERIC_FEATURE_TYPE, _VPAboveNb_fct, can_be_quantized)
def _NormalizedVPAboveNb_fct(mention):
    return MENTION_FEATURES.count_VP_above(mention) / MENTION_FEATURES.count_above(mention)
can_be_quantized = False
normalized_vp_above_nb_data = (NUMERIC_FEATURE_TYPE, _NormalizedVPAboveNb_fct, can_be_quantized)







### Internal morphosyntactic features ###
# Pronoun
can_be_quantized = False
is_pronoun_data = (NUMERIC_FEATURE_TYPE, lambda mention: int(MENTION_FEATURES.is_expanded_pronoun(mention)), can_be_quantized)

# Name
can_be_quantized = False
is_name_data = (NUMERIC_FEATURE_TYPE, lambda mention: int(MENTION_FEATURES.is_name(mention)), can_be_quantized)

## Animated
possible_values = ("True", "False", UNKNOWN_VALUE_TAG)
is_animated_data = (CATEGORICAL_FEATURE_TYPE, lambda mention: str(MENTION_FEATURES.is_animated(mention)), possible_values)

## Person
def _Person_fct(mention):
    """ Return the grammatical person corresponding to the input mention, or UNKNOWN_TAG if not defined """
    person_tag = MENTION_FEATURES.get_person(mention)
    if person_tag is None:
        return UNKNOWN_VALUE_TAG
    return person_tag
possible_values = (FIRST_PERSON_TAG, SECOND_PERSON_TAG, THIRD_PERSON_TAG, UNKNOWN_VALUE_TAG)
person_data = (CATEGORICAL_FEATURE_TYPE, _Person_fct, possible_values)

## Number
possible_values = (SINGULAR_NUMBER_TAG, PLURAL_NUMBER_TAG, UNKNOWN_VALUE_TAG)
number_data = (CATEGORICAL_FEATURE_TYPE, lambda mention: mention.number, possible_values)

## Quantifier
##
can_be_quantized = False
is_indefinite_data = (NUMERIC_FEATURE_TYPE, lambda mention: int(MENTION_FEATURES.is_indefinite(mention)), can_be_quantized)
##
can_be_quantized = False
is_quantified_data = (NUMERIC_FEATURE_TYPE, lambda mention: int(MENTION_FEATURES.is_quantified(mention)), can_be_quantized)


### Grammatical role features ###
## Position
def _Position_fct(mention, matrix=None, row_id=0, column_offset=0):
    """ Function that vectorizes information about the in-document position of the input mention; 
    whether it is the first or the last mention, or whether it is located in the second 
    or third third of the document's mentions.
    The respective name of the columns in which such info is encoded are the following:
    "FirstPosition"
    "LastPosition"
    "MiddlePosition"
    "EndPosition"
    """
    if matrix is None:
        result = sparse.lil_matrix((1,4), dtype=np.int)
        _Position_fct(mention, matrix=result, row_id=0, column_offset=0)
        return result.tocsr()
    sentence = mention.sentence
    index = MENTION_FEATURES.mention_index(mention)
    nb_mentions = len(sentence.mentions)
    index_first = 0
    index_last = nb_mentions-1
    index_1on3 = int(nb_mentions / 3.0) # First third
    index_2on3 = int(2.0 * nb_mentions / 3.0) # Second third
    # First position
    if index == index_first:
        matrix[row_id,column_offset+0] = 1
    # Last position
    if index == index_last:
        matrix[row_id,column_offset+1] = 1
    # Middle position (1/3 - 2/3)
    if index_1on3 <= index < index_2on3:
        matrix[row_id,column_offset+2] = 1
    # End position (2/3 - 3/3)
    if index_2on3 <= index:
        matrix[row_id,column_offset+3] = 1
    return matrix
column_names = ("FirstPosition", "LastPosition", "MiddlePosition", "EndPosition")
position_data = (MULTI_NUMERIC_FEATURE_TYPE, _Position_fct, column_names)

## In coordination
can_be_quantized = False
in_coordination_data = (NUMERIC_FEATURE_TYPE, lambda mention: int(MENTION_FEATURES.in_coordination(mention)), can_be_quantized)

## Is coordination
can_be_qantized = False
is_coordination_data = (NUMERIC_FEATURE_TYPE, lambda mention: int(MENTION_FEATURES.is_coordination(mention)), can_be_qantized)

## Is enumeration
can_be_qantized = False
is_enumeration_data = (NUMERIC_FEATURE_TYPE, lambda mention: int(MENTION_FEATURES.is_enumeration(mention)), can_be_qantized)

### Grammatical types features ###
##
possible_values = tuple(sorted(GRAM_TYPE_TAGS))
gram_type_data = (CATEGORICAL_FEATURE_TYPE, lambda mention: mention.gram_type, possible_values)
##
possible_values = tuple(sorted(GRAM_SUBTYPE_TAGS))
gram_subtype_data = (CATEGORICAL_FEATURE_TYPE, lambda mention: mention.gram_subtype, possible_values)


### Named entity types features ###
## Named entity type
def _get_named_entity_attributes(mention):
    type_ = None
    subtype = None
    named_entity = mention.named_entity
    if named_entity is not None:
        type_ = named_entity.type
        subtype = named_entity.subtype
    return type_, subtype
POSSIBLE_NAMED_ENTITY_TYPE_CLUSTER_TAGS = ("PERS", "ORG", "TIME", "LOC", "OTHER")
def _classify_named_entity_type_tag(type_):
    if type_ in PERSON_NAMED_ENTITY_TYPE_TAGS:
        return "PERS"
    elif type_ in ORGANIZATION_NAMED_ENTITY_TYPE_TAGS:
        return "ORG"
    elif type_ in DATE_NAMED_ENTITY_TYPE_TAGS:
        return "TIME"
    elif type_ in LOCATION_NAMED_ENTITY_TYPE_TAGS:
        return "LOC"
    return "OTHER"
def _classify_ne_type_tag_if_possible(mention):
    type_, _ = _get_named_entity_attributes(mention)
    if type_ is not None:
        return _classify_named_entity_type_tag(type_)
    return "N/A"
_CLASSIFY_NE_TYPE_TAG_IF_POSSIBLE_POSSIBLE_VALUES = tuple(itertools.chain(POSSIBLE_NAMED_ENTITY_TYPE_CLUSTER_TAGS, ("N/A",)))

_compute_fct = lambda mention: _classify_ne_type_tag_if_possible(mention)
possible_values = _CLASSIFY_NE_TYPE_TAG_IF_POSSIBLE_POSSIBLE_VALUES
named_entity_type_cluster_data = (CATEGORICAL_FEATURE_TYPE, _compute_fct, possible_values)

## Named entity subtype
POSSIBLE_NAMED_ENTITY_SUBTYPE_CLUSTER_TAGS = ("OTHER",)
def _classify_named_entity_subtype_tag(subtype):
    return "OTHER"
def _classify_ne_subtype_tag_if_possible(mention):
    _, subtype = _get_named_entity_attributes(mention)
    if subtype is not None:
        return _classify_named_entity_subtype_tag(subtype)
    return "N/A"
_CLASSIFY_NE_SUBTYPE_TAG_IF_POSSIBLE_POSSIBLE_VALUES = tuple(itertools.chain(POSSIBLE_NAMED_ENTITY_SUBTYPE_CLUSTER_TAGS, ("N/A",)))

can_be_qantized = False
_compute_fct = lambda mention: int(_get_named_entity_attributes(mention)[1] is not None)
has_named_entity_subtype_data = (NUMERIC_FEATURE_TYPE, _compute_fct, can_be_qantized)

_compute_fct = lambda mention: _classify_ne_subtype_tag_if_possible(mention)
possible_values = _CLASSIFY_NE_SUBTYPE_TAG_IF_POSSIBLE_POSSIBLE_VALUES
named_entity_subtype_cluster_data = (CATEGORICAL_FEATURE_TYPE, _compute_fct, possible_values)

### Sieves features ###
def _Sieve1_fct(mention, matrix=None, row_id=0, column_offset=0):
    """ Function that vectorizes information about whether the input mention defines edges, with the 
    other mentions of its document, that are positive or negative according to the application of 
    the first sieve rule on the document.
    The respective name of the columns in which such info is encoded are the following:
    "Sieve1Pos"
    "Sieve1Neg"
    """
    if matrix is None:
        result = sparse.lil_matrix((1,2), dtype=np.int)
        _Sieve1_fct(mention, matrix=result, row_id=0, column_offset=0)
        return result.tocsr()
    document = mention.document
    sieve1_pos = False
    sieve1_neg = False
    for mention2 in document.mentions:
        pos, neg = get_sieve1_pos_neg(mention, mention2)
        sieve1_pos = sieve1_pos or pos
        sieve1_neg = sieve1_neg or neg
        if sieve1_pos and sieve1_neg:
            break
    matrix[row_id, column_offset+0:column_offset+2] = int(pos), int(neg)
column_names = ("Sieve1Pos", "Sieve1Neg")
sieve1_data = (MULTI_NUMERIC_FEATURE_TYPE, _Sieve1_fct, column_names)
