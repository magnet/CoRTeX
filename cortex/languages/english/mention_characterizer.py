# -*- coding: utf-8 -*-
"""
Defines a class used to qualify the mention of documents written in English, i.e. notably to determine 
grammatical information about them.

Uses the set of POS tags used to annotate the 'Ontonotes v5' corpus.

Here, EXPANDED_PRONOUN means to include also other kind of words, such as demonstrative determiners, 
that can also act as a reference to a grammatical person. 
"""

__all__ = ["EnglishMentionCharacterizer",]

from collections import defaultdict, OrderedDict


print("Loading external resources...")

################
# Load WordNet #
################
import nltk
import nltk.corpus.reader.wordnet as wordnet
from nltk.corpus import LazyCorpusLoader, CorpusReader
omw_corpus = LazyCorpusLoader('omw', CorpusReader, r'.*/wn-data-.*\.tab', encoding='utf8')
# Need to download the data if it does not exist
try:
    file_system_path_pointer = nltk.data.find('corpora/wordnet')
except LookupError:
    if not nltk.download("wordnet"):
        raise LookupError("Cannot find nltk 'wordnet' corpus, and cannot download it either.") from None
    file_system_path_pointer = nltk.data.find('corpora/wordnet')
WN = wordnet.WordNetCorpusReader(file_system_path_pointer, omw_corpus)

# Wordnet utils
PERSON_SYN = WN.synsets('person', lang='eng')[0] # 'person.n.01'
MALE_SYN = WN.synsets('male', lang='eng')[1] # 'male.n.02'
FEMALE_SYN = WN.synsets('female', lang='eng')[1] # 'female.n.02'
GROUP_SYN = WN.synsets('group', lang='eng')[0] # 'group.n.01'        # treated as ORG
ARTIFACT_SYN = WN.synsets('artifact', lang='eng')[0] #'artifact.n.01'
LOCATION_SYN = WN.synsets('location', lang='eng')[0] # 'location.n.01'

#####################################
# Load regexes & words enumerations #
#####################################
from .knowledge import lexicon
ACCEPTABLE_EXPANDED_PRONOUN_MENTIONS = lexicon.DEMONSTRATIVE_PRONOUN_MENTIONS.union(lexicon.EXPANDED_PERSONAL_PRONOUN_MENTIONS)


###################
# Load gazetteers #
###################
from .knowledge import names_corpus_reader
from .knowledge import get_iterator_over_gazetteers_file, get_iterator_over_bart_names_file

LAST_NAMES = set(names_corpus_reader._FAMILY_NAMES_SET)
assert len(LAST_NAMES) != 0
FEMALE_NAMES = set(names_corpus_reader._FEMALE_NAMES_SET)
FEMALE_NAMES.update(lexicon.FEMALE_NAMES) # Add names-corpus-1.3/females.txt to knowledge.lexicon.FEMALES_NAMES
assert len(FEMALE_NAMES) != 0
MALE_NAMES = set(names_corpus_reader._MALE_NAMES_SET)
MALE_NAMES.update(lexicon.MALE_NAMES) # Add names-corpus-1.3/males.txt to knowledge.lexicon.MALES_NAMES
assert len(MALE_NAMES) != 0
LOCATIONS = set(l.strip() for l in get_iterator_over_gazetteers_file("locations"))
assert len(LOCATIONS) != 0

NAT_ADJ_MAP = dict([(l.strip().split()[0], " ".join(l.strip().split()[1:])) for l in get_iterator_over_bart_names_file("adj_map")])
assert len(NAT_ADJ_MAP) != 0
NAT_NAMES = NAT_ADJ_MAP.values()
assert len(NAT_NAMES) != 0

ALIASES = defaultdict(list)
for (n1, n2) in lexicon.ACRONYMS: # Mixed bag of locations and organizations etc.
    ALIASES[n1].append(n2)
    ALIASES[n2].append(n1)
assert len(ALIASES) != 0

ORG_LIST = set(ALIASES.keys())
ORG_LIST.difference_update(LOCATIONS)
assert len(ORG_LIST) != 0

ALL_LOCATIONS = LOCATIONS.union(set(NAT_NAMES))

##############################
# Load Bergsma's gender data #
##############################
from .knowledge.bergsma_gender_reader import GenderReader
gender_reader = GenderReader(threshold=5) # Tune threshold...
BERG_PROBA = 0.5

print("Finished loading external resources.")


#################################
# Tags used to qualify mentions #
#################################
from cortex.parameters.mention_data_tags import (UNKNOWN_VALUE_TAG, 
                                                  EXPANDED_PRONOUN_GRAM_TYPE_TAG, NAME_GRAM_TYPE_TAG, 
                                                  NOMINAL_GRAM_TYPE_TAG, VERB_GRAM_TYPE_TAG,
                                                  EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS, 
                                                  NAME_GRAM_TYPE_SUBTYPE_TAGS, 
                                                  NOMINAL_GRAM_TYPE_SUBTYPE_TAGS,  
                                                  SINGULAR_NUMBER_TAG, PLURAL_NUMBER_TAG, 
                                                  MALE_GENDER_TAG, FEMALE_GENDER_TAG, 
                                                  NEUTRAL_GENDER_TAG)

################
# Other import #
################
from cortex.utils.memoize import Memoized
from cortex.api.markable import NamedEntity, Mention
from cortex.api.document_buffer import DocumentBuffer

from .referential_it_token_proba_computer import (ReferentialEnglishItTokenProbaComputer, 
                                                  CannotComputeReferentialProbaError)
from .token_features import ENGLISH_TOKEN_FEATURES
from .mention_features import ENGLISH_MENTION_FEATURES
from .parameters import ontonotes_v5_constituent_tags as POS_TAG_MODULE
from .parameters import ontonotes_v5_named_entities_data_tags as NE_tags


GAZETTEERS_STRING = "gazetteers"
REGEXES_STRING = "regexes"
WORDNET_STRING = "wn"
LAST_TOKEN_NAMED_ENTITY_STRING = "last_token_named_entity"



class EnglishMentionCharacterizer(object):
    """ Class providing a method used to enrich the representation of mentions, coming from 
    documents written in English.
    
    Does so notably by consulting various knowledge sources: lexicon, regexes, WordNet, gazetteers...
    
    Assumes that the following informations are present, for the Document 
    instance:
        - tokens
        - sentences (synchronized with tokens, themselves synchronized with the constituency tree nodes 
          of the tree associated to their respective sentence)
        - mentions extent (synchronized with tokens, but head may not be found yet)
    
    Assumes that the documents have been annotated with constituency trees, using the tags set used by 
    the annotations of the 'Ontonotes v5' corpus.
        
    Assumes for now that no specific informations about the mention is known, apart what is necessary 
    to define it (its extent, document & raw_text attributes)
    For a given Mention instance, the following attributes will be sought and set (except if the '
    keep_existing_attribute_value' init input is True, in which case, if a non None value is already 
    attributed to a mention, it will be kept):
        - head_token
        - gram_type
        - gram_subtype
        - gender
        - number
        - named_entity
        - wn_synonym
        - wn_hypernym
    
    At the end of the mention characterization process, if new NamedEntity instances were determined, 
    they will be added to the collection of NamedEntity instances associated to the Document instance.
    
    Arguments:
        keep_existing_attribute_value: boolean (default=True), whether or not to keep the original 
            attribute values of a mention, during the enrichment phase, if such a value exists
        
        raise_exception_if_referential_proba_not_computable: boolean (default = False), whether or 
            not to raise an exception if it is not possible to use a 
            ReferentialEnglishItTokenProbaComputer instance in order to estimate probabilities that 
            an 'it' token be referential or not
    
    Attributes:
        keep_existing_attribute_value: the value of the 'keep_existing_attribute_value' parameter 
            used to initialize the class instance
    """
    # The following order is important: the search for the named entity must be carried out as soon as possible, 
    # since as lot of the following attribute value search processes use its value, if it exists
    _MENTION_ATTRIBUTE_NAME_TO_ENRICH = ["head_extent", "head_tokens", "gram_type", "gram_subtype", 
                                        "named_entity", 
                                        "gender", "number", "wn_synonyms", "wn_hypernyms"]
    _POSSIBLE_GRAM_TYPE_VALUES = set([NAME_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_TAG, 
                                     NOMINAL_GRAM_TYPE_TAG, VERB_GRAM_TYPE_TAG])
    _POSSIBLE_GENDER_VALUES = [MALE_GENDER_TAG, FEMALE_GENDER_TAG, NEUTRAL_GENDER_TAG]
    _POSSIBLE_NUMBER_VALUES = [SINGULAR_NUMBER_TAG, PLURAL_NUMBER_TAG]
    _EASILY_CODED_ATTRIBUTE_ENRICHMENT = set(["gram_subtype", "named_entity", "gender", "number", 
                                             "wn_synonyms", "wn_hypernyms", "wn_antonyms"])
    
    _TOKEN_FEATURES = ENGLISH_TOKEN_FEATURES
    _MENTION_FEATURES = ENGLISH_MENTION_FEATURES
    
    def __init__(self, keep_existing_attribute_value=True, raise_exception_if_referential_proba_not_computable=False):
        # Check input
        get_extent_to_proba_mappings_fct = lambda documents: tuple(None for _ in documents) #document
        try:
            referential_english_it_token_proba_computer = ReferentialEnglishItTokenProbaComputer()
        except CannotComputeReferentialProbaError:
            if raise_exception_if_referential_proba_not_computable:
                raise
        else:
            get_extent_to_proba_mappings_fct = lambda documents: referential_english_it_token_proba_computer.compute_referential_proba(documents)
        # Set attribute values
        self.keep_existing_attribute_value = keep_existing_attribute_value
        self._document_buffer = DocumentBuffer()
        self._get_extent_to_proba_mappings_fct = get_extent_to_proba_mappings_fct
    
    # Instance methods
    def enrich_documents_mentions(self, documents):
        """ Carries out inplace mentions enrichment for each document of the input documents collection.
        
        Assumes mentions have been synchronized with their tokens, which in turn must have been 
        synchronized with their respective ConstituencyTreeNodes, and must possess and POS tag value.
        
        Args:
            documents: a collection of Document instances
        """
        extent_to_proba_mappings = self._get_extent_to_proba_mappings_fct(documents)
        for extent_to_proba_mapping, document in zip(extent_to_proba_mappings, documents):
            self._enrich_document_mentions(document, mention_extent_to_proba=extent_to_proba_mapping)
        
    def _enrich_document_mentions(self, document, mention_extent_to_proba=None):
        """ Carries out inplace mentions enrichment for the input document.
        
        Assumes mentions have been synchronized with their tokens, which in turn must have been 
        synchronized with their respective ConstituencyTreeNodes, and must possess and POS tag value.
        
        Args:
            document: a Document instance
        """
        # Enrich all the mentions
        new_named_entities = []
        if document.mentions:
            for mention in document.mentions:
                named_entity = self._enrich_mention(mention, mention_extent_to_proba=mention_extent_to_proba)
                if named_entity is not None:
                    new_named_entities.append(named_entity)
        
        # Consolidate the document to take into account the possibly newly found NamedEntity instances
        if len(new_named_entities) > 0:
            ## Create NamedEntity creation from dict data
            named_entities_data = OrderedDict()
            for named_entity in new_named_entities:
                named_entity_data = named_entity.to_dict()
                named_entities_data[named_entity_data["extent"]] = named_entity_data
            ## Use it to modify the Document instance to add the new named entities
            document_buffer = self._document_buffer
            document_buffer.initialize_anew(document)
            document_buffer.add_named_entities(named_entities_data)
            document_buffer.flush()
            document_buffer.clean()
    
    def _enrich_mention(self, mention, mention_extent_to_proba=None):
        """ Carries out inplace enrichment of the input mention.
        
        Assume the mention has been synchronized with its tokens, which in turn must have been 
        synchronized with their respective ConstituencyTreeNodes.
        
        Args:
            mention: a Mention instance

        Returns:
            a NamedEntity, if it was discovered that the mention corresponds to a previously 
            unknown named entity; or None, if that is not the case
        """
        attributes_name_to_enrich = tuple(self._MENTION_ATTRIBUTE_NAME_TO_ENRICH)
        # First do head_extent, head_tokens and gram_type, for the enriching method for all other 
        # attributes depends upon them, and so as to stop early if the mention is a verbal one
        self._enrich_head_extent(mention)
        self._enrich_head_tokens(mention)
        self._enrich_gram_type(mention)
        if mention.gram_type == VERB_GRAM_TYPE_TAG:
            return
        # Enrich with the other attributes value
        self._enrich_gram_subtype(mention)
        named_entity = self._enrich_named_entity(mention)
        attributes_name_to_enrich = attributes_name_to_enrich[5:]
        for name in attributes_name_to_enrich:
            enrich_method = getattr(self, "_enrich_{}".format(name))
            enrich_method(mention)
        # Set referential_probability value
        if mention_extent_to_proba is not None:
            proba = mention_extent_to_proba.get(mention.extent, None)
            if proba is not None:
                proba = float(proba)
                if proba < 0 or proba > 1:
                    message = "'referential_probability' value to be assigned to mention '{}' does "\
                                "not belong to the [0; 1] interval (value = {}).".format(mention, proba)
                    raise ValueError(message)
                mention.referential_probability = proba
        return named_entity
    
    ## Head token
    def _enrich_head_extent(self, mention):
        """ Finds the extent of the head of the input mention, and set the value the corresponding 
        Mention instance's attribute with it, if permitted by the 'keep_existing_attribute_value' 
        attribute and the mention's current state.
        
        Assume the mention has been synchronized with its tokens, which in turn must have been 
        synchronized with their respective ConstituencyTreeNodes.
            mention: a Mention instance

        Returns:
            the head extent value that has been found; or None, if nothing occurred because of 
        the 'keep_existing_attribute_value' parameter
        """
        if not (self.keep_existing_attribute_value and mention.head_extent is not None):
            head_extent = self._MENTION_FEATURES.find_mention_head_extent(mention)
            mention.head_extent = head_extent
            return head_extent
    
    def _enrich_head_tokens(self, mention):
        """ Synchronizes the mention's tokens corresponding to its head, so that its 'head_tokens' 
        attribute be properly set.
        
        Assumes 'head_extent' attribute has been found and set.
        
        Args:
            mention: a Mention instance
        """
        mention.synchronize_head_tokens(strict=True)
    
    ## Gram type
    def _enrich_gram_type(self, mention):
        """ Computes the gram type that corresponds to the input mention, and set the value the corresponding 
        Mention instance's attribute with it, if permitted by the 'keep_existing_attribute_value' 
        attribute and the mention's current state.
        
        Assumes 'head_tokens' attribute has been found and set.
        
        Args:
            mention: a Mention instance

        Returns:
            a string, the gram type tag that has been found; or None, if nothing occurred because of 
            the 'keep_existing_attribute_value' parameter
        """
        if not (self.keep_existing_attribute_value and mention.gram_type is not None):
            gram_type = self._find_mention_gram_type(mention)
            mention.gram_type = gram_type
            return gram_type
    
    def _find_mention_gram_type(self, mention):
        """ Compute the gram type that corresponds to the input mention.
        
        Assumes 'head_tokens' attribute has been found and set.
        Output of this method must be part of the set of possible outputs, defined in 
        _POSSIBLE_GRAM_TYPE_VALUES.
        
        Args:
             mention: a Mention instance

        Returns:
            a string, the gram type tag that has been found
        """
        # FIXME: interpretation of 'head_tokens' value changes when considering whether or not the mention is an enumeration. Do something here?
        # Moreover, assume that there is always only one head_token in the head_tokens list, if the constituency parsing of the sentence was successful (so not NOPARSE) # FIXME:
        # In the case of an enumeration, it seems the head token chosen will be the last token of the 'tokens' list constituting to the mention.
        last_token = mention.head_tokens[-1]
        # Verb
        # FIXME: what if the first condition is true, but not the second? This function will then output NOMINAL: should it really?
        if self._TOKEN_FEATURES.is_verb(last_token) and len(mention.tokens) == 1:
            return VERB_GRAM_TYPE_TAG
        # NP
        ## Pronoun
        if self._mention_is_expanded_pronoun(mention):
            return EXPANDED_PRONOUN_GRAM_TYPE_TAG
        ## Name
        elif self._TOKEN_FEATURES.is_name(last_token): # FIXME: fixme?
            return NAME_GRAM_TYPE_TAG
        ## Nominal
        else:
            # FIXME: for something to truly be nominal, check that head is common noun (NN, NNS)
            # => Check which cases would not fit those first four, to see what to qualify them with
            return NOMINAL_GRAM_TYPE_TAG
        # FIXME: add UNKNOWN value at this point
        
    def _mention_is_expanded_pronoun(self, mention):
        """ Determines whether or not the input mention can be qualified to be an expanded pronoun.
        
        Source type: knowledge.lexicon
        
        Ags:
            mention: a Mention instance

        Returns:
            a boolean
        """
        prefixes = ["all of", "both of", "some of", "many of"]
        suffixes = ["all"]
        separators = ["and", "or"]
        specials = ["'s"]
        text = mention.raw_text.lower()
        
        # Pathological cases e.g. US, eliminate them# FIXME: fixme?
        tokens = mention.tokens
        if len(tokens) == 1 and tokens[0].POS_tag == POS_TAG_MODULE.PROPER_NOUN_SINGULAR_TAG:
            return False
        # Base case
        if text in ACCEPTABLE_EXPANDED_PRONOUN_MENTIONS:
            return True
        # Special
        if text in specials:
            return True
        # Prefix
        for pref in prefixes:
            if text.startswith(pref):
                l = text.split(pref)
                if len(l) == 2 and l[1].strip() in ACCEPTABLE_EXPANDED_PRONOUN_MENTIONS:
                    return True
        # Suffix
        for suf in suffixes:
            if text.endswith(suf):
                cut_text = text[:-len(suf)].strip()
                if cut_text in ACCEPTABLE_EXPANDED_PRONOUN_MENTIONS:
                    return True
        # "the {CARDINAL_NUMBER} of you/us/them" (e.g.: the 4 of us)
        if len(tokens) == 4 and tokens[-1].raw_text.lower() in ["them", "us", "you"]:
            if self._TOKEN_FEATURES.is_the_CD_of(tokens[0], tokens[1], tokens[2]):
                return True
        # Separator
        for sep in separators:
            l = text.split(sep)
            if len(l) > 1:
                all_pronouns = True
                for e in l:
                    if not e.strip() in ACCEPTABLE_EXPANDED_PRONOUN_MENTIONS:
                        all_pronouns = False
                        break
                if all_pronouns:
                    return True
        return False
    
    ### Others
    def _enrich_attribute(self, mention, attribute_name):
        """ Calls on the input mention the enrichment method corresponding to the input attribute name, 
        if permitted by the 'keep_existing_attribute_value' attribute and the mention's current state.
        
        Assumes that mention.gram_type attribute has been found and set.
        Assumes that tokens of the mention have been synchronized with it.
        Assumes that head tokens of the mention have been synchronized with it.
        
        Args:
            mention: a Mention instance
            attribute_name: string, the name of the attribute of the mention whose value shall be 
                sought, if permitted

        Returns:
            the found value of the attribute; or None, if nothing occurred because of 
            the 'keep_existing_attribute_value' parameter
        """
        if attribute_name not in self._EASILY_CODED_ATTRIBUTE_ENRICHMENT:
            raise ValueError("Incorrect 'attribute_name' input value ('{}'), possible values are: {}."\
                             .format(attribute_name, self._EASILY_CODED_ATTRIBUTE_ENRICHMENT))
        existing_attribute_value = getattr(mention, attribute_name)
        if not (self.keep_existing_attribute_value and existing_attribute_value is not None):
            get_attribute_value_method = getattr(self, "_find_{}_{}".format(mention.gram_type, attribute_name))
            attribute_value = get_attribute_value_method(mention)
            setattr(mention, attribute_name, attribute_value)
            return attribute_value
    
    ## Gram subtype
    def _enrich_gram_subtype(self, mention):
        """ Computes the gram subtype that corresponds to the input mention, and set the value the 
        corresponding Mention instance's attribute with it, if permitted by the 
        'keep_existing_attribute_value' attribute and the mention's current state.
        
        Assumes that mention.gram_type attribute has been found and set.
        Assumes that tokens of the mention have been synchronized with it.
        
        Args:
            mention: a Mention instance

        Returns:
            a string, the gram subtype value that has been found; or None, if nothing occurred because of 
            the 'keep_existing_attribute_value' parameter
        """
        return self._enrich_attribute(mention, "gram_subtype")
    
    def _find_EXPANDED_PRONOUN_gram_subtype(self, mention):
        """ Compute the gram subtype that corresponds to the input mention, assuming the mention's 
        gram type is that of 'expanded pronouns'.
        
        Assume that tokens of the mention have been synchronized with it.
        Source type: knowledge.lexicon
        
        Args:
            mention: a Mention instance

        Returns:
            the gram subtype value that has been found (possible unknown)
        """
        prefixes = ["all of","both of","some of","many of"] # Plural
        suffixes = ["all"] # Plural
        #specials = ["'string"] # FIXME: not used? Maybe we needed to think about it because of those wrong mention using saxon genitif
        # Prepare a patch to correct those wrongly annotated mentions a loading time
        text = mention.raw_text.lower()
        # Test strings
        test_strings = [text]
        ## Prefix
        for pref in prefixes:
            if text.startswith(pref):
                l = text.split(pref)
                if len(l) == 2:
                    test_strings.append(l[-1])
        ## Suffix
        for suf in suffixes:
            if text.endswith(suf):
                test_strings.append(text[:-len(suf)].strip())
        ## Separator
        for coord in ["and","or"]:
            l = text.split(coord)
            if len(l) == 2:
                test_strings.append(l[0].strip())
                test_strings.append(l[1].strip())
        ## Last token (e.g. the CD of you/them/us)
        last_token = mention.tokens[-1]
        test_strings.append(last_token.raw_text.lower())
        ## Find a subtype
        for string in test_strings:
            # The testing order is important!
            if string in lexicon.EXPANDED_POSSESSIVE_PRONOUN_MENTIONS:
                return EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_POSS_PRO"]
            if string in lexicon.EXPANDED_PERSONAL_PRONOUN_MENTIONS:
                return EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["EXPAND_PERS_PRO"]
            if string in lexicon.DEMONSTRATIVE_PRONOUN_MENTIONS:
                return EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["DEM_PRO"]
            if string in lexicon.RELATIVE_PRONOUN_MENTIONS:
                return EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS["REL_PRO"]
        return UNKNOWN_VALUE_TAG
    
    def _find_NAME_gram_subtype(self, mention):
        """ Computes the gram subtype that corresponds to the input mention, assuming the mention's 
        gram type is that of 'name'.
        
        Assumes that tokens of the mention have been synchronized with it.
        
        Args:
            mention: a Mention instance

        Returns:
            a string, the gram subtype value that has been found
        """
        tok_ct = len(mention.tokens)
        if tok_ct <= 1:
            return NAME_GRAM_TYPE_SUBTYPE_TAGS["SHORT_NAME"]
        return NAME_GRAM_TYPE_SUBTYPE_TAGS["LONG_NAME"]

    def _find_NOMINAL_gram_subtype(self, mention):
        """ Computes the gram subtype that corresponds to the input mention, assuming the mention's 
        gram type is that of 'nominal'.
        
        Assume that tokens of the mention have been synchronized with it.
        
        Args:
            mention: a Mention instance

        Returns:
            a string, the gram subtype value that has been found
        """
        tokens = mention.tokens
        first_token = tokens[0]
        if len(tokens) <= 2:
            if (self._TOKEN_FEATURES.is_def_det(first_token) or self._TOKEN_FEATURES.is_expand_poss_pro(first_token)):
                return NOMINAL_GRAM_TYPE_SUBTYPE_TAGS["SHORT_DEF_NP"]
            if self._TOKEN_FEATURES.is_dem(first_token):
                return NOMINAL_GRAM_TYPE_SUBTYPE_TAGS["SHORT_DEM_NP"]
            if self._MENTION_FEATURES.is_indefinite(mention):
                return NOMINAL_GRAM_TYPE_SUBTYPE_TAGS["SHORT_INDEF_NP"]
            return NOMINAL_GRAM_TYPE_SUBTYPE_TAGS["SHORT_UNDET_NP"]
        
        if (self._TOKEN_FEATURES.is_def_det(first_token) or self._TOKEN_FEATURES.is_expand_poss_pro(first_token)):
            return NOMINAL_GRAM_TYPE_SUBTYPE_TAGS["LONG_DEF_NP"]
        if self._TOKEN_FEATURES.is_dem(first_token):
            return NOMINAL_GRAM_TYPE_SUBTYPE_TAGS["LONG_DEM_NP"]
        if self._MENTION_FEATURES.is_indefinite(mention):
            return NOMINAL_GRAM_TYPE_SUBTYPE_TAGS["LONG_INDEF_NP"]
        return NOMINAL_GRAM_TYPE_SUBTYPE_TAGS["LONG_UNDET_NP"]
    
    def _find_VERB_gram_subtype(self, mention):
        """ Should not be called, in principle. Exists for the sake of consistency with the way 
        higher level methods are defined, and to raise a somewhat understable exception if called.
        """
        raise NotImplemented
    
    ## NamedEntity
    def _enrich_named_entity(self, mention):
        """ Finds whether or not a named entity can be defined from the input mention, and set the 
        value the corresponding Mention instance's attribute with it, if permitted by the 
        'keep_existing_attribute_value' attribute and the mention's current state.
        
        Assumes that mention.gram_type attribute has been found and set.
        Assumes that head tokens of the mention have been synchronized with it.
        
        Args:
            mention: a Mention instance

        Returns:
            a NamedEntity instance corresponding to the named entity that could be defined 
            from studying the input mention; or None, if none was found, or if nothing occurred because 
            of the 'keep_existing_attribute_value' parameter
        """
        return self._enrich_attribute(mention, "named_entity")
    
    def _find_EXPANDED_PRONOUN_named_entity(self, mention):
        """ Finds whether or not a named entity can be defined from the input mention, assuming the 
        mention's gram type is that of 'expanded pronouns'.
        
        No particular assumption.
        Source type: lexicon
        
        Args:
            mention: a Mention instance

        Returns:
            a NamedEntity instance, or None
        """
        text = mention.raw_text.lower()
        if text.endswith(" all"):
            text = text[:-4]
        text = text.split()[-1]
        if text in lexicon.EXPANDED_ANIMATE_PRONOUN_MENTIONS:
            data = (NE_tags.PERSON_NAMED_ENTITY_TYPE_TAGS[0], UNKNOWN_VALUE_TAG, "lexicon") # FIXME: PERS or PERSON? Do we authorize variations according to original source?
            return self._create_named_entity_from_mention(mention, data)
        return None
    
    def _find_NAME_named_entity(self, mention):
        """ Finds whether or not a named entity can be defined from the input mention, assuming the 
        mention's gram type is that of 'name'.
        
        Assumes that head tokens of the mention have been synchronized with it.
        Source type(s): last token named_entity attribute; gazetteers; regexes; (regexes; gazetteers); WN
        
        Args:
            mention: a Mention instance

        Returns:
            a NamedEntity instance, or None
        """
        # FIXME: unable to determine that 'June of 2004' is a 'DATE' NamedEntity
        # See if we can attribute the NamedEntity belonging to the 'main' token
        # FIXME: special case if the mention is an enumeration (ex: group of people, group of org, group of countries, group of loc)?
        last_token = mention.head_tokens[-1]
        hd_token_ne = last_token.named_entity
        if hd_token_ne is not None and hd_token_ne.type not in NE_tags.PROBLEM_NAMED_ENTITY_TAGS: # FIXME: do not know to what those values may correspond / how they can be generated: are those possible NamedEntity.type values?
            data = (hd_token_ne.type, hd_token_ne.subtype, LAST_TOKEN_NAMED_ENTITY_STRING)
            return self._create_named_entity_from_mention(mention, data)
        
        # Easy to solve, 3 letters only for everything
        h_str = mention.head_raw_text
        # String match in regexes/gazetteers
        if last_token.raw_text in LAST_NAMES:
            data = (NE_tags.PERSON_NAMED_ENTITY_TYPE_TAGS[0], UNKNOWN_VALUE_TAG, GAZETTEERS_STRING)
            return self._create_named_entity_from_mention(mention, data)
        if last_token.raw_text in ALL_LOCATIONS:
            data = (NE_tags.LOCATION_NAMED_ENTITY_TYPE_TAGS[0], UNKNOWN_VALUE_TAG, GAZETTEERS_STRING)
            return self._create_named_entity_from_mention(mention, data)
        if h_str in ORG_LIST:
            data = (NE_tags.ORGANIZATION_NAMED_ENTITY_TYPE_TAGS[0], UNKNOWN_VALUE_TAG, GAZETTEERS_STRING)
            return self._create_named_entity_from_mention(mention, data)
        
        # Head or first token match in regexes/gazetteers
        if lexicon.CORP_REGEX.search(h_str):
            data = (NE_tags.ORGANIZATION_NAMED_ENTITY_TYPE_TAGS[0], UNKNOWN_VALUE_TAG, REGEXES_STRING)
            return self._create_named_entity_from_mention(mention, data)
        # FIXME: what if the mention is an enumeration? A 'PERS' named entity found based on the text of the first head token might not be applicable for the whole mention (ex: enumeration)...
        # Proposition: do the same as in '_find_NAME_gender': a special case for enumeration
        _, named_entity_data = self._find_possible_NAME_named_entity_and_gender(mention.raw_text, 
                                                                                mention.head_tokens[0].raw_text) #gender_data
        if named_entity_data is not None:
            return self._create_named_entity_from_mention(mention, named_entity_data)
        
        # WN
        hypernym_synsets = self._hypernyms_from_str(h_str)
        if PERSON_SYN in hypernym_synsets:
            data = (NE_tags.PERSON_NAMED_ENTITY_TYPE_TAGS[0], UNKNOWN_VALUE_TAG, WORDNET_STRING)
            return self._create_named_entity_from_mention(mention, data)
        elif LOCATION_SYN in hypernym_synsets:
            data = (NE_tags.LOCATION_NAMED_ENTITY_TYPE_TAGS[0], UNKNOWN_VALUE_TAG, WORDNET_STRING)
            return self._create_named_entity_from_mention(mention, data)
        elif GROUP_SYN in hypernym_synsets:
            data = (NE_tags.ORGANIZATION_NAMED_ENTITY_TYPE_TAGS[0], UNKNOWN_VALUE_TAG, WORDNET_STRING)
            return self._create_named_entity_from_mention(mention, data)
        elif ARTIFACT_SYN in hypernym_synsets:
            data = (NE_tags.ARTIFACT_NAMED_ENTITY_TYPE_TAGS[0], UNKNOWN_VALUE_TAG, WORDNET_STRING)
            return self._create_named_entity_from_mention(mention, data)
        
        return None

    def _find_NOMINAL_named_entity(self, mention):
        """ Finds whether or not a named entity can be defined from the input mention, assuming the 
        mention's gram type is that of 'nominal'.
        
        Assumes that head_extent has been found and set.
        Source type(s): WN
        
        Args:
            mention: a Mention instance

        Returns:
            a NamedEntity instance, or None
        """
        h_str = mention.head_raw_text
        hypernyms_synsets = self._hypernyms_from_str(h_str) # lemma? FIXME: what to do with that?
        # Use the lemma value instead of the text if it is defined
        # FIXME: see following lines
        # Faire requête dans WordNet, essayer avec des versions fléchies du mot (ex: pluriel) si on a même résultat: si non, prendre celle du texte (plus spécifique)
        # Si pas couvert, tester les deux
        if PERSON_SYN in hypernyms_synsets:
            data = (NE_tags.PERSON_NAMED_ENTITY_TYPE_TAGS[0], UNKNOWN_VALUE_TAG, WORDNET_STRING)
            return self._create_named_entity_from_mention(mention, data)
        if LOCATION_SYN in hypernyms_synsets:
            data = (NE_tags.LOCATION_NAMED_ENTITY_TYPE_TAGS[0], UNKNOWN_VALUE_TAG, WORDNET_STRING)
            return self._create_named_entity_from_mention(mention, data)
        if GROUP_SYN in hypernyms_synsets:
            data = (NE_tags.ORGANIZATION_NAMED_ENTITY_TYPE_TAGS[0], UNKNOWN_VALUE_TAG, WORDNET_STRING)
            return self._create_named_entity_from_mention(mention, data)
        if ARTIFACT_SYN in hypernyms_synsets:
            data = (NE_tags.ARTIFACT_NAMED_ENTITY_TYPE_TAGS[0], UNKNOWN_VALUE_TAG, WORDNET_STRING)
            return self._create_named_entity_from_mention(mention, data)

        return None
    
    def _find_VERB_named_entity(self, mention):
        """ Should not be called, in principle. Exists for the sake of consistency with the way 
        higher level methods are defined, and to raise a somewhat understable exception if called.
        """
        raise NotImplemented
    
    @Memoized
    def _find_possible_NAME_named_entity_and_gender(self, mention_raw_text, first_head_token_raw_text):
        """ Uses the string data of the mention and of its first head token to carry out a search (see 
        whether or not the mention refers to a person) that can yield at the same time a 
        'named_entity' and 'gender' information.
        
        Results are cached for efficient reuse of the search's results.
        Source type(s): regexes; gazetteers
        
        Args:
            mention_raw_text: raw text of the mention
            first_head_token_raw_text: raw text of the first token in the collection of head tokens 
                associated to the mention, because, in the case that the mention uses the name of a person, 
                the (first) name usually is the first token of the head

        Returns:
            a (name_gender_data, named_entity_data) tuple, where 'name_gender_data' is None or 
            a (gender_value, origin_value) tuple, and 'named_entity_data' is None or 
            a (type, subtype, origin) tuple
        """
        gender_data = None
        named_entity_data = None
        
        # Test whether titles belongs to the whole mention
        if lexicon.MALE_TITLE_REGEX.search(mention_raw_text):
            gender_data = (MALE_GENDER_TAG, REGEXES_STRING)
        elif lexicon.FEMALE_TITLE_REGEX.search(mention_raw_text):
            gender_data = (FEMALE_GENDER_TAG, REGEXES_STRING)
        # Test whether or not the first token of the mention correspond to the first name of a person
        else:
            gender_data = self._find_gender_of_name_token(first_head_token_raw_text)
        
        if gender_data is not None:
            origin = gender_data[1]
            named_entity_data = (NE_tags.PERSON_NAMED_ENTITY_TYPE_TAGS[0], UNKNOWN_VALUE_TAG, origin)
        
        return gender_data, named_entity_data
    
    ## Gender
    def _enrich_gender(self, mention):
        """ Computes the grammatical gender value that can be associated to the input mention, and 
        set the value the corresponding Mention instance's attribute with it, if permitted by the 
        'keep_existing_attribute_value' attribute and the mention's current state.
        
        Assumes that mention.gram_type attribute has been found and set.
        Assumes that head tokens of the mention have been synchronized with it.
        Assumes that mention.named_entity attribute has been found and set, where possible.
        
        Args:
            mention: a Mention instance

        Returns:
            the grammatical gender value that has been found; or None, if nothing occurred because 
            of the 'keep_existing_attribute_value' parameter
        """
        return self._enrich_attribute(mention, "gender")
    
    def _find_EXPANDED_PRONOUN_gender(self, mention):
        """ Computes the grammatical gender value that corresponds to the input mention, assuming the 
        mention's gram type is that of 'expanded pronouns'.
        
        Assumes that head_extent has been found and set.
        Source type(s): regexes
        
        Args:
            mention: a Mention instance

        Returns:
            the grammatical gender value that has been found (possibly unknown)
        """
        gender = UNKNOWN_VALUE_TAG
        h_str = mention.head_raw_text
        data = None
        if lexicon.EXPAND_MASC_PRO_REGEX.match(h_str):
            data = (MALE_GENDER_TAG, REGEXES_STRING)
        elif lexicon.EXPAND_FEM_PRO_REGEX.match(h_str):
            data = (FEMALE_GENDER_TAG, REGEXES_STRING)
        elif lexicon.EXPAND_NEUT_PRO_REGEX.match(h_str): # Doesn't include rel. pros
            data = (NEUTRAL_GENDER_TAG, REGEXES_STRING)
        # FIXME: see if there is pronoun for which we are unable to find a gender
        """
        else:
            print >> sys.stderr, "Pro w/o gender: '%s'" %h_str
        """
        if data is not None:
            gender, _ = data
        return gender
    
    def _find_NAME_gender(self, mention):
        """ Computes the grammatical gender value that corresponds to the input mention, assuming the 
        mention's gram type is that of 'name'.
        
        Assumes that head tokens of the mention have been synchronized with it.
        Assumes that mention.named_entity attribute has been found and set, where possible.
        Source type(s): named_entity attribute &: gazetters or (regexes; gazetteers); bergsma
        
        Args:
            mention: a Mention instance

        Returns:
            the grammatical gender value that has been found (possibly unknown)
        """
        gender = UNKNOWN_VALUE_TAG
        # FIXME: special case if the mention is an enumeration: look for 'GROUP OF' NamedEntities
        # If we do not have a NamedEntity info, or if we do and this info says that the entity is a person, we proceed with the search
        if mention.named_entity is None or mention.named_entity.type in NE_tags.PERSON_NAMED_ENTITY_TYPE_TAGS:
            data = None
            
            hd_tokens = mention.head_tokens
            first_head_token_str = hd_tokens[0].raw_text.lower() # First name usually is the first token of the head
            m_str = mention.raw_text.lower()
            
            enumeration = self._MENTION_FEATURES.get_enumeration(mention)
            if enumeration is not None:
                # Check to determine gender of each NP token, to see if they all have the same
                # FIXME: create a unittest for this specific part
                found_data = None
                gender_tags = set()
                origin = "combined"
                for tokens in enumeration:
                    if len(tokens) == 1:
                        token = tokens[0]
                        tmp_data = self._find_gender_of_name_token(token.raw_text)
                        if tmp_data is None:
                            break
                        elif tmp_data[0] == NEUTRAL_GENDER_TAG:
                            found_data = NEUTRAL_GENDER_TAG, origin
                            break
                        else:
                            gender_tags.add(tmp_data[0])
                    else:
                        # Consider that the group of token can be a mention itself, and then apply the enrichment method to it so that we may determine its gender
                        # No risk of infinite recursion because a text is finite, and more importantly because a priori we cannot distinguish an enumeration within an enumeration 
                        extent = (tokens[0].start, tokens[-1].end)
                        ident = "temp_{},{}".format(*extent)
                        document = mention.document
                        temp_mention = Mention(ident, extent, document)
                        temp_mention.tokens = tokens
                        self._enrich_mention(temp_mention, mention_extent_to_proba=None)
                        if temp_mention.gender == UNKNOWN_VALUE_TAG:
                            break
                        elif temp_mention.gender == NEUTRAL_GENDER_TAG:
                            found_data = NEUTRAL_GENDER_TAG, origin
                            break
                        else:
                            gender_tags.add(temp_mention.gender)
                            
                if found_data is None:
                    if len(gender_tags) == 1:
                        data = tuple(gender_tags)[0], origin
                else:
                    data = tuple(found_data)
            else:
                data, _ = self._find_possible_NAME_named_entity_and_gender(mention.raw_text, 
                                                                           mention.head_tokens[0].raw_text) # named_entity_data
            
            if data is None:
                # Try Bergsma's gender data:
                h_str = mention.head_raw_text.lower()
                strings = [m_str, m_str.replace(" - ","-"), h_str, h_str.replace(" - ","-"), 
                           first_head_token_str]
                for string in strings:
                    gdr = gender_reader.get_most_probable_gender(string, proba_threshold=BERG_PROBA, 
                                                                 count_threshold=10)
                    if gdr is not None:
                        data = (gdr, "bergsma")
                        break
            
            if data is not None:
                gender, _ = data # source
        # Else we have a mention that has a named entity that is not a person, so it must be of the neutral gender
        else:
            gender = NEUTRAL_GENDER_TAG
        
        return gender

    def _find_NOMINAL_gender(self, mention):
        """ Computes the grammatical gender value that corresponds to the input mention, assuming the 
        mention's gram type is that of 'nominal'.
        
        Assumes that head_extent has been found and set.
        Assumes that mention.named_entity attribute has been found and set, where possible.
        Source type(s): named_entity attribute &: WN; bergsma
        
        Args:
            mention: a Mention instance

        Returns:
            the grammatical gender value that has been found (possibly unknown)
        """
        gender = UNKNOWN_VALUE_TAG
        # If we do not have a NamedEntity info, or if we do and this info says that the entity is a person, we proceed with the search
        if mention.named_entity is None or mention.named_entity.type not in NE_tags.PERSON_NAMED_ENTITY_TYPE_TAGS:
            data = None
            h_str = mention.head_raw_text
            hypernym_synsets = self._hypernyms_from_str(h_str)
            # Search in WN
            if len(hypernym_synsets) > 0:
                if MALE_SYN in hypernym_synsets:
                    data = (MALE_GENDER_TAG, WORDNET_STRING)
                elif FEMALE_SYN in hypernym_synsets:
                    data = (FEMALE_GENDER_TAG, WORDNET_STRING)
                elif GROUP_SYN in hypernym_synsets or LOCATION_SYN in hypernym_synsets\
                     or ARTIFACT_SYN in hypernym_synsets:
                    data = (NEUTRAL_GENDER_TAG, WORDNET_STRING)
            # Try Bergsma's gender data
            if data is None:
                strings = [mention.raw_text, mention.head_raw_text]
                for string in strings:
                    gdr = gender_reader.get_most_probable_gender(string, proba_threshold=BERG_PROBA, 
                                                                 count_threshold=10)
                    if gdr is not None:
                        data = (gdr, "bergsma")
                        break
            if data is not None:
                gender, _ = data # source
        # Else we have a mention that has a named entity that is not a person, so it must be of the neutral gender
        else:
            gender = NEUTRAL_GENDER_TAG 
            
        return gender
    
    def _find_VERB_gender(self, mention):
        """ Should not be called, in principle. Exists for the sake of consistency with the way 
        higher level methods are defined, and to raise a somewhat understable exception if called.
        """
        raise NotImplemented
    
    ## Number
    def _enrich_number(self, mention):
        """ Computes the grammatical number value that can be associated to the input mention, and 
        set the value the corresponding Mention instance's attribute with it, if permitted by the 
        'keep_existing_attribute_value' attribute and the mention's current state.
        
        Assume that head tokens of the mention have been synchronized with it.
        
        Args:
            mention: a Mention instance

        Returns:
            the grammatical number that has been found; or None, if nothing occurred because 
        of the 'keep_existing_attribute_value' parameter
        """
        return self._enrich_attribute(mention, "number")
    
    def _find_EXPANDED_PRONOUN_number(self, mention):
        """ Computes the grammatical number value that corresponds to the input mention, assuming the 
        mention's gram type is that of 'expanded pronouns'.
        
        Assumes that tokens of the mention have been synchronized with it.
        Source type(s): knowledge.lexicon; POS_tags
        
        Args:
            mention: a Mention instance

        Returns:
            the grammatical number value that has been found (possibly unknown)
        """
        prefixes = ["all of","both of","some of","many of"] # Plural
        suffixes = ["all"] # Plural
        specials = ["'s"]
        text = mention.raw_text.lower()
        tokens = mention.tokens
        # Base case
        if text in lexicon.EXPANDED_SINGULAR_PRONOUN_MENTIONS:
            return SINGULAR_NUMBER_TAG
        elif text in lexicon.EXPANDED_PLURAL_PRONOUN_MENTIONS:
            return PLURAL_NUMBER_TAG
        # Special
        if text in specials:
            # FIXME: is this case for when the mention is an anglo-saxon possessive that has mistakenly been considered a mention / a PRONOUN mention?
            # Carry out a search to see if such a 'mention' exists in the used the currenty used CONLL2012 corpus?
            return UNKNOWN_VALUE_TAG
        # Prefix
        for pref in prefixes:
            if text.startswith(pref):
                l = text.split(pref)
                if len(l) == 2:
                    return PLURAL_NUMBER_TAG
        # Suffix
        for suf in suffixes:
            if text.endswith(suf):
                return PLURAL_NUMBER_TAG
        # "the CD of you/us/them"
        if len(tokens) == 4 and tokens[-1].raw_text.lower() in ["them", "us", "you"]:
            txt_1 = tokens[0].raw_text.lower()
            pos_2 = tokens[1].POS_tag
            txt_3 = tokens[2].raw_text.lower()
            if txt_1 == "the" and pos_2 == POS_TAG_MODULE.CARDINAL_NUMBER_TAG and txt_3 == "of":
                return PLURAL_NUMBER_TAG
        # Separator
        if "and" in text:
            return PLURAL_NUMBER_TAG
        elif "or" in text:
            l = text.split("or")
            num = UNKNOWN_VALUE_TAG
            for e in l:
                if e.strip() in lexicon.EXPANDED_PLURAL_PRONOUN_MENTIONS:
                    num = PLURAL_NUMBER_TAG
                elif e.strip() in lexicon.EXPANDED_SINGULAR_PRONOUN_MENTIONS and num != PLURAL_NUMBER_TAG:
                    num = SINGULAR_NUMBER_TAG
            return num
        return UNKNOWN_VALUE_TAG
    
    def _find_NAME_number(self, mention):
        """ Computes the grammatical number value that corresponds to the input mention, assuming the 
        mention's gram type is that of 'name'.
        
        Assume that head tokens of the mention have been synchronized with it.
        Source type(s): raw text; POS tag
        
        Args:
            mention: a Mention instance

        Returns:
            the grammatical number value that has been found (possibly unknown)
        """
        if self._MENTION_FEATURES.is_enumeration(mention):
            return PLURAL_NUMBER_TAG
        m_str = mention.raw_text
        if " and " in m_str: # FIXME: remove? should be taken into account by "is_enumeration": to deal with general refactorization of this class
            # FIXME: Carry out experiment: is there mention which are enumeration and which arrives to this point, knowing we checked for enumeration just before?
            # Rather: which are the mentions that are not enumeration, but still contain the word 'and'?
            return PLURAL_NUMBER_TAG
        hd_tag = mention.head_tokens[-1].POS_tag
        if hd_tag in POS_TAG_MODULE.SINGULAR_NOUN_TAGS:
            return SINGULAR_NUMBER_TAG
        if hd_tag in POS_TAG_MODULE.PLURAL_NOUN_TAGS:
            return PLURAL_NUMBER_TAG
        return UNKNOWN_VALUE_TAG

    def _find_NOMINAL_number(self, mention): # FIXME: replace by call to token_features methods, because everything here depends on features values of a token
        """ Computes the grammatical number value that corresponds to the input mention, assuming the 
        mention's gram type is that of 'name'.
        
        Assume that head tokens of the mention have been synchronized with it.
        Source type(s): POS_tags; regexes
        
        Args:
            mention: a Mention instance

        Returns:
            the grammatical number value that has been found (possibly unknown)
        """
        if self._MENTION_FEATURES.is_enumeration(mention):
            return PLURAL_NUMBER_TAG
        hd_tag = mention.head_tokens[-1].POS_tag
        hd_word = mention.head_tokens[-1].raw_text.lower()
        if hd_tag in POS_TAG_MODULE.SINGULAR_NOUN_TAGS:
            return SINGULAR_NUMBER_TAG
        if hd_tag in POS_TAG_MODULE.PLURAL_NOUN_TAGS:
            return PLURAL_NUMBER_TAG
        if hd_tag == POS_TAG_MODULE.CARDINAL_NUMBER_TAG:
            if hd_word == "one":
                return SINGULAR_NUMBER_TAG
            return PLURAL_NUMBER_TAG
        if hd_tag == POS_TAG_MODULE.DETERMINER_TAG:
            if hd_word in ["both", "those"]:
                return PLURAL_NUMBER_TAG
            return UNKNOWN_VALUE_TAG
        if hd_tag in POS_TAG_MODULE.PERSONAL_PRONOUN_TAGS:
            if lexicon.EXPAND_SG_PRO_REGEX.match(hd_word):
                return SINGULAR_NUMBER_TAG
            if lexicon.EXPAND_PL_PRO_REGEX.match(hd_word):
                return PLURAL_NUMBER_TAG
            return UNKNOWN_VALUE_TAG
        return UNKNOWN_VALUE_TAG
        # FIXME: what to do about this commented out legacy code?
        # To deal during class code refactorization
        """
        h_str = m.get_head_text().lower()
        if WN.morphy( _wn_str(h_str) ) == _wn_str(h_str):
            return SINGULAR_NUMBER_TAG
        else:
            return PLURAL_NUMBER_TAG
        """
    
    def _find_VERB_number(self, mention):
        """ Should not be called, in principle. Exists for the sake of consistency with the way 
        higher level methods are defined, and to raise a somewhat understable exception if called.
        """
        raise NotImplemented
    
    ## WN_synonyms
    def _enrich_wn_synonyms(self, mention):
        """ Computes the collection of WordNet synonym code values that can be associated to the input 
        mention, and set the value the corresponding Mention instance's attribute with it, if 
        permitted by the 'keep_existing_attribute_value' attribute and the mention's current state.
        
        Args:
            mention: a Mention instance

        Returns:
            the collection of WordNet synonym code values that has been found; or None, if nothing 
            occurred because of the 'keep_existing_attribute_value' parameter
        """
        return self._enrich_attribute(mention, "wn_synonyms")
    
    def _find_EXPANDED_PRONOUN_wn_synonyms(self, mention):
        """ Computes the collection of WordNet synonym code values that can be associated to the input 
        mention's raw text, assuming the mention's gram type is that of 'expanded pronouns'.
        
        Args:
            mention: a Mention instance

        Returns:
            a collection of WordNet synonym code values (possibly empty)
        """
        return []

    def _find_NAME_wn_synonyms(self, mention):
        """ Computes the collection of WordNet synonym code values that can be associated to the input 
        mention's raw text, assuming the mention's gram type is that of 'name'.
        
        Source type: WordNet
        
        Args:
            mention: a Mention instance

        Returns:
            a collection of WordNet synonym code values (possibly empty)
        """
        m_str = mention.raw_text
        synsets = WN.synsets(self._wn_str(m_str), pos=wordnet.NOUN, lang='eng') # FIXME: is it normal that wordnet.NOUN is used?
        return [s.name() for s in synsets]

    def _find_NOMINAL_wn_synonyms(self, mention):
        """ Compute the collection of WordNet synonym code values that can be associated to the input 
        mention's raw text, assuming the mention's gram type is that of 'nominal'.
        
        Source type: WordNet
        
        Args:
            mention: a Mention instance

        Returns:
            collection of WordNet synonym code values (possibly empty)
        """
        m_str = mention.raw_text
        synsets = WN.synsets(self._wn_str(m_str), pos=wordnet.NOUN, lang='eng') # FIXME: is it normal that wordnet.NOUN is used?
        return [s.name() for s in synsets]
    
    def _find_VERB_wn_synonyms(self, mention):
        """ Should not be called, in principle. Exists for the sake of consistency with the way 
        higher level methods are defined, and to raise a somewhat understable exception if called.
        """
        raise NotImplemented
    
    ## Wn_hypernyms
    def _enrich_wn_hypernyms(self, mention):
        """ Computes the collection of WordNet hypernym code values that can be associated to the input 
        mention, and set the value the corresponding Mention instance's attribute with it, if 
        permitted by the 'keep_existing_attribute_value' attribute and the mention's current state.
        
        Args:
            mention: a Mention instance

        Returns:
            the collection of WordNet hypernym code values that has been found; or None, if 
            nothing occurred because of the 'keep_existing_attribute_value' parameter
        """
        return self._enrich_attribute(mention, "wn_hypernyms")
    
    def _find_EXPANDED_PRONOUN_wn_hypernyms(self, mention):
        """ Computes the collection of WordNet hypernym code values that can be associated to the input 
        mention's raw text, assuming the mention's gram type is that of 'expanded pronouns'.
        
        Source type: WordNet
        
        Args:
            mention: a Mention instance

        Returns:
            collection of WordNet hypernym code values (possibly empty)
        """
        return []

    def _find_NAME_wn_hypernyms(self, mention):
        """ Compute the collection of WordNet hypernym code values that can be associated to the input 
        mention's raw text, assuming the mention's gram type is that of 'name'.
        
        Source type: WordNet
        
        Args:
            mention: a Mention instance

        Returns:
            a collection of WordNet hypernym code values (possibly empty)
        """
        m_str = mention.raw_text
        synsets = self._hypernyms_from_str(m_str)
        return [s.name() for s in synsets]

    def _find_NOMINAL_wn_hypernyms(self, mention):
        """ Computes the collection of WordNet hypernym code values that can be associated to the input 
        mention's raw text, assuming the mention's gram type is that of 'nominal'.
        
        Source type: WordNet
        
        Args:
            mention: a Mention instance

        Returns:
            a collection of WordNet hypernym code values (possibly empty)
        """
        m_str = mention.raw_text
        synsets = self._hypernyms_from_str(m_str)
        return [s.name() for s in synsets]
    
    def _find_VERB_wn_hypernyms(self, mention):
        """ Should not be called, in principle. Exists for the sake of consistency with the way 
        higher level methods are defined, and to raise a somewhat understable exception if called.
        """
        raise NotImplemented
    '''
    ## Wn_antonyms
    def _enrich_wn_antonyms(self, mention):
    """ Computes the collection of WordNet antonym code values that can be associated to the input 
        mention, and set the value the corresponding Mention instance's attribute with it, if 
        permitted by the 'keep_existing_attribute_value' attribute and the mention's current state.
        
        Args:
            mention: a Mention instance

        Returns:
            the collection of WordNet antonym code values that has been found; or None, if nothing 
            occurred because of the 'keep_existing_attribute_value' parameter
        """
        return self._enrich_attribute(mention, "wn_antonyms")
    
    def _find_EXPANDED_PRONOUN_wn_antonyms(self, mention):
    """ Computes the collection of WordNet antonym code values that can be associated to the input 
        mention's raw text, assuming the mention's gram type is that of 'expanded pronouns'.
        
        Source type: WordNet
        
        Args:
            mention: a Mention instance

        Returns:
            a collection of WordNet antonym code values (possibly empty)
        """
        return []

    def _find_NAME_wn_antonyms(self, mention):
    """ Computes the collection of WordNet antonym code values that can be associated to the input 
        mention's raw text, assuming the mention's gram type is that of 'name'.
        
        Args:
            mention: a Mention instance

        Returns:
            a collection of WordNet antonym code values (possibly empty)
        """
        m_str = mention.raw_text
        syns = self._antonyms_from_str(m_str)
        return [s.name for s in syns]

    def _find_NOMINAL_wn_antonyms(self, mention):
        """ Computes the collection of WordNet antonym code values that can be associated to the input 
        mention's raw text, assuming the mention's gram type is that of 'nominal'.
        
        Source type: WordNet
        
        Args:
            mention: a Mention instance

        Returns:
            a collection of WordNet antonym code values (possibly empty)
        """
        m_str = mention.raw_text
        syns = self._antonyms_from_str(m_str)
        return [s.name for s in syns]
    
    def _find_VERB_wn_antonyms(self, mention):
    """ Should not be called, in principle. Exists for the sake of consistency with the way 
        higher level methods are defined, and to raise a somewhat understable exception if called.
        """
        raise NotImplemented
    '''
    # Class methods
    @classmethod
    def _hypernyms_from_str(cls, string): # mention or head token
        """ Computes the collection of WordNet hypernym values that can be associated to the input 
        text.
        
        Source type: WordNet
        
        Args:
            string: a string, word or composed word whose hypernym list is desired

        Returns:
            a list of nltk.corpus.reader.wordnet.Synset instances
        """
        lemma = cls._wn_str(string)
        synsets = WN.synsets(lemma, pos=wordnet.NOUN, lang='eng') # list of nltk.corpus.reader.wordnet.Synset instances
        hypernyms = []
        hyp = lambda s:s.hypernyms()
        for synset in synsets[:1]: # FIXME: first sense only?
            # Tester empiriquement
            # hypernyms += hyper_closure(s,acc=[]) FIXME: what to do with that?
            hypernyms += list(synset.closure(hyp)) # list of Synset instances
        return hypernyms
    
    @classmethod
    def _antonyms_from_str(cls, string): # mention or head token
        """ Computes the collection of WordNet antonym values that can be associated to the input 
        text.
        
        Source type: WordNet
        
        Args:
            string: a string, word whose antonyms are desired

        Returns:
            an iterator over nltk.corpus.reader.wordnet.Synset instances
        """
        lemma = string
        synsets = WN.synsets(lemma, lang='eng')
        if len(synsets) > 0:
            return map(lambda x: x.synset, synsets[0].lemmas(lang='eng')[0].antonyms())
        return []
    
    # Static method
    @staticmethod
    def _wn_str(in_str):
        """ Transforms a string representing a phrase made of tokens, into a version that makes an 
        acceptable input for searches to be made with WordNet.
        
        Args:
            int_str: a string, the input phrase

        Returns:
            a string, the transformed phrase
        """
        return "_".join(in_str.split())
    
    @staticmethod
    def _find_gender_of_name_token(token_raw_text):
        """ Returns info about the grammatical gender of the input token's raw text, assuming it 
        represents a proper name, from comparison made using gazetteers data; or None if the 
        comparison was unsuccessful.
        
        But since currently no such data is available here, always returns None for now.
        
        Args:
            token_raw_text: a string, the raw text representing a token

        Returns:
            a (grammatical gender value; origin value) tuple if the comparison was successful, 
            or None if it was not
        """
        if token_raw_text in MALE_NAMES:
            return (MALE_GENDER_TAG, GAZETTEERS_STRING)
        elif token_raw_text in FEMALE_NAMES:
            return (FEMALE_GENDER_TAG, GAZETTEERS_STRING)
        return None
    
    @staticmethod
    def _create_named_entity_from_mention(mention, data):
        """ Creates a NamedEntity instance corresponding to the text of the input mention, using the 
        input named entity type data.
        
        Args:
            mention: a Mention instance, 
            data: a (type_, subtype, origin) tuple:

        Returns:
            a NamedEntity instance
        """
        type_, subtype, origin = data
        extent = mention.extent
        ident = "{},{}".format(*extent)
        dict_ = {"ident": ident, "extent": extent, "document": mention.document, "type": type_, 
                 "subtype":subtype, "origin": origin}
        return NamedEntity.from_dict(dict_)
