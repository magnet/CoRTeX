# -*- coding: utf-8 -*-

"""
CoRTeX is a toolbox made to train coreference resolution models and apply them on documents
See README file for more information.

Versioning scheme: semantic versioning (cf https://semver.org/)

CoRTeX currently support only the English and French languages.
For the specifics regarding each language, cf the corresponding subpackage of the 'cortex.languages' 
package.
"""

"""
Available subpackage(s)
-----------------------
api
    Defines base classes used for holding and representing structured data about textual documents.

io
    Defines tools to read and write textual documents data from and to hard drive.

parameters
    Defines modules defining some hyperparameters used by the toolbox.

learning
    Defines classes and structures needed to carry out machine learning.

languages
    Defines classes and data specific to the different languages supported by the toolbox.

coreference
    Defines classes and structures used to create, train and use coreference resolution models.

tools
    Defines third party modules or packages used by the toolbox, or other, 'complex' tools.

utils
    Defines basic functions used by several of cortex' subpackages.
"""


import os
with open(os.path.join(os.path.dirname(__file__), "VERSION.txt"), "r", encoding="utf-8") as f:
    __version__ = f.read().strip()

from .api.document import Document
from .api.document_buffer import DocumentBuffer
from .api.coreference_partition import CoreferencePartition
from .api.corpus import Corpus

from .io.conll2012_reader import CONLL2012DocumentReader, CONLL2012MultiDocumentReader
from .io.conll2012_writer import CONLL2012DocumentWriter
from .io.conllu_reader import CONLLUDocumentReader
from .io.pivot_reader import PivotReader
from .io.pivot_writer import PivotWriter
from .io.corpus_io import parse_corpus, parse_corpus_file, write_corpus, write_corpus_file

from .coreference.api import evaluate_resolver, evaluate_resolvers
from .coreference.resolver import load_resolver

from .tools.configuration import Mapping

import cortex.api
import cortex.io
import cortex.parameters
import cortex.learning
import cortex.languages
import cortex.coreference
import cortex.tools
import cortex.utils
