# -*- coding: utf-8 -*-'

"""
Defines simple functions or classes used by several of CoRTeX' subpackages / submodules 
"""

"""
Available submodule(s)
----------------------
display
    Defines utilities pertaining to the display of information / useful for logging tasks

io
    Defines utilities pertaining to (local) read / write operations

memoize
    Defines utilities pertaining to memoization processes

singleton
    Defines a Singleton class

timer
    Defines utilities pertaining to the measuring of a callable's execution duration

xml_utils
    Defines utilities pertaining to the processing of textual information within 
    'xml.etree.cElementTree' nodes
"""

__all__ = ["CoRTeXException", 
           "get_parameter_grids", 
           "IndexedIterator", 
           "count_iterable_content", 
           "define_check_item_name_function", 
           "_check_isinstance", 
           "compute_f1_score", 
           "normalize", 
           "get_unique_elts", 
           "are_array_equal", 
           "are_sparse_matrix_equal", 
           "write_dictionary_to_file", 
           "read_dictionary_from_file", 
           "EqualityMixIn",
           ]


from collections import OrderedDict
import numpy as np
from scipy import sparse
from sklearn.model_selection import ParameterGrid



class CoRTeXException(Exception):
    """ Parent class for all custom exception class defined in this library pertaining to errors 
    linked to the use of this library.
    """
    pass



def get_parameter_grids(parameter_name2parameter_possible_values):
    """ Creates a collection of configuration mapping from the combination of all input parameters 
    collections.
    Cf 'sklearn.model_selection.ParameterGrid.
    
    Args:
        parameter_name2parameter_possible_values: a "parameter name => collection of possible parameter values" 
            map, or a collection of such maps

    Returns:
        collection of dictionaries, each one corresponding to a specific parameters set
    """
    parameter_name2parameter_possible_values_collection = parameter_name2parameter_possible_values
    if isinstance(parameter_name2parameter_possible_values, dict):
        parameter_name2parameter_possible_values_collection = [parameter_name2parameter_possible_values]
    factory_parameters_collection = tuple(ParameterGrid(parameter_name2parameter_possible_values_collection))
    return factory_parameters_collection



class IndexedIterator(object):
    """ Class wrapping an iterator so that one may keep track of the number of items that were 
    consumed from the iterator since this class' instance has been wrapping it.
    
    Arguments:
        iterator: the iterator to wrap, and whose consumed items number should be kept track of
    
    Attributes:
        iterator: the iterator to wrap, and whose consumed items number should be kept track of
        
        iterated_items_nb: the number of items that were consumed from the iterator since this 
            class' instance has been wrapping it
    """
    def __init__(self, iterator):
        self.iterator = iterator
        self.iterated_items_nb = 0
    
    def __next__(self):
        item_ = next(self.iterator)
        self.iterated_items_nb += 1
        return item_
    
    def __iter__(self):
        return self



def count_iterable_content(iterable):
    """ Consumes an iterable, and return the number of items that were consumed.
    
    Args:
        iterable: iterable to consume

    Returns:
        number of items that were still present in the iterable before consuming it
    """
    return sum((1 for _ in iterable), 0)



def _check_isinstance(classes, instance, instance_param_name, strict=False):
    """ Checks that the input python object's class is correct with respect to a class or a 
    collection of classes.
    
    Args:
        classes: a class, or a collection of classes
        instance: the python object whose class is to be checked
        instance_param_name: the name under which the instance is supposed to be identifiable by 
            the user
        strict: boolean (default = False); whether or not the instance should be exactly an 
            instance of the provided class(es), or if it is also allowed to be an instance of a potential 
            subclass
    
    Raises:
        ValueError: if the input instance's class is not correct with respect to the input class or 
        collection of classes
    """
    try:
        len(classes)
    except (TypeError, AttributeError):
        classes = (classes,)
    if strict:
        if type(instance) not in classes:
            message = "'{}' input is not a strict instance of the following expected classes: {}; its type is '{}' instead."
            message = message.format(instance_param_name, tuple(c.__name__ for c in classes), type(instance))
            raise ValueError(message)
    else:
        if not isinstance(instance, classes):
            message = "'{}' input is not an instance of the following classes or of their potential subclasse(s): {}; its type is '{}' instead."
            message = message.format(instance_param_name, tuple(c.__name__ for c in classes), type(instance))
            raise ValueError(message)





def compute_f1_score(r, p):
    """ Computes the f1 score corresponding to the input recall and precision value.
    
    Args:
        r: float, recall value
        p: float, precision value
    
    Returns:
        float, the corresponding f1 score value
    
    Notes:
        If both input values are equal to zero, instead the result being ill-defined (ex: numpy.NaN), 
        the result will be equal to zero
    """
    f1 = 0.0
    if r+p > 0:
        f1 = (2*r*p) / (r+p)
    return f1

def normalize(sub_nb, total_nb):
    """ Carries out a division of a numerator by a denominator, but only if the denominator is 
    strictly positive; else, return numpy.NaN.
    
    Args:
        sub_nb: numerator
        total_nb: denominator

    Returns:
        the float number resulting from the division of the numerator by a float version of the 
        denominator, or numpy.NaN
    """
    if total_nb > 0:
        return sub_nb / float(total_nb)
    return np.NaN

def get_unique_elts(items_iterable):
    """ Consumes an iterable over items, and return the collection of unique items that this iterable 
    contained, in the order in which they appeared.
    
    Args:
        items: iterable of hashable objects

    Returns:
        collection of each unique item
    """
    seen = set()
    output = []
    for item in items_iterable:
        if item not in seen:
            output.append(item)
            seen.add(item)
    
    return tuple(output)

def are_array_equal(array1, array2):
    """ Tests whether two numpy arrays share the same shape and have values that are close enough  
    to be considered equals.
    
    Args:
        array1: first array of the comparison
        array2: second array of the comparison

    Returns:
        boolean
    
    Raises:
        ValueError: if one of the input is not a numpy array
    """
    if not(isinstance(array1, np.ndarray) and isinstance(array2, np.ndarray)):
        error_message = "Error: one of the input objects is not a np.ndarray."
        raise ValueError(error_message)
    if array1.shape != array2.shape:
        return False
    return np.allclose(array1, array2)
    

def are_sparse_matrix_equal(matrix1, matrix2):
    """ Test whether two scipy.sparse matrix are equal; that is whether or not they share the same 
    shape, and the difference of the two is a zero matrix.
    
    Args:
        array1: first sparse matrix of the comparison
        array2: second sparse matrix of the comparison

    Returns:
        boolean
    
    Raises:
        ValueError: if one of the input is not a scipy.sparse matri
    """
    if not (sparse.issparse(matrix1) and sparse.issparse(matrix2)):
        error_message = "Error: one of the input objects is not a scipy.sparse matrix."
        raise ValueError(error_message)
    if matrix1.shape != matrix2.shape:
        return False
    diff_matrix = matrix1-matrix2
    return np.allclose(np.sum(np.abs(diff_matrix.data), None), 0)



ENCODING = "utf-8"
def write_dictionary_to_file(d, file_path):
    """ Writes the content of a dictionary to a file, assuming all keys and values are strings/
    
    Args:
        d: dictionary
        file_path: string, path to text file where the content of the dictionary will be written
    """
    with open(file_path, "w", encoding=ENCODING) as f:
        for key, value in d.items():
            f.write("{}\t\t{}\n".format(key, value))
def read_dictionary_from_file(file_path):
    """ Parses a text file created through the use of the 'write_dictionary_to_file' function, and 
    output the corresponding dictionary.
    
    Args:
        file_path: path to text file where the data of the dictionary is written

    Returns:
        dict
    """
    with open(file_path, "r", encoding=ENCODING) as f:
        d = OrderedDict()
        for line in f:
            if line:
                key, value = line.split("\t\t")
                key = key.strip()
                value = value.strip()
                d[key] = value
    return d



class EqualityMixIn(object):
    """ MixIn class used to implement equality methods in classes that possesses several, specific 
    attributes. Also implement string representation methods useful for checking the a class instance 
    attributes' values that are used when testing for equality an instance with another.
    """
    
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple()
    
    @staticmethod
    def _compare_attribute_values(instance1, instance2, attribute_names):
        """ Tests that each of the two input python objects possess the specified attribute, and that 
        the attribute value of each of those two object is equal.
        
        If not, returns the name of the first attribute for which the value is not equal between the 
        first object and the second.
        
        Assumes that each object indeed possesses all the attribute whose name is specified in the 
        input collection.
        
        Args:
            instance1: first python object of the comparison
            instance2: second python object of the comparison
            attribute_names: collection of names of attributes whose respective value shall be 
                tested for equality, between those of the first object, and those of the second object

        Returns:
            a (boolean, string or None) pair; the second item of the pair will be None if the 
            boolean value is True (all attribute values were found to be equal), or will be the name of 
            the first attribute for which the values were found to not be equal
        """
        for attribute_name in attribute_names:
            value1 = getattr(instance1, attribute_name)
            value2 = getattr(instance2, attribute_name)
            if value1 != value2:
                return False, attribute_name
        return True, None
    
    def _inner_eq(self, other):
        """ Tests that this class instance is equal to the input object.
        
        That means 
        that:
            - this instance's class is equal or a subclass of the other instance's class
            - all the attributes whose name is present in this '_ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION' 
              class attribute are present for this instance and the other, input instance, and that their 
              values are respectively equal
        
        Args:
            other: the instance of this class for which to test for equality

        Returns:
            a (boolean, string or None) pair; the second item of the pair will be None if the 
            boolean value is True (all attribute values were found to be equal), or will be the name of 
            the first attribute for which the values were found to not be equal, or 'class' if the object 
            were found not to be equal because the first test for the classes of each instance failed
        """
        if not isinstance(other, type(self)):#type(self) != type(other):
            return False, "Class"
        return self._compare_attribute_values(self, other, self._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION)
    
    def __eq__(self, other):
        value, _ = self._inner_eq(other)
        return value
    
    def __ne__(self, other):
        return not self == other
    
    def __hash__(self):
        raise NotImplemented
    
    def __repr__(self):
        sep=", "
        string_format = "{}={}"
        strings_array = []
        for attribute_name in self._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION:
            try:
                value = getattr(self, attribute_name)
            except AttributeError:
                value = "N/A"
            string = string_format.format(attribute_name, repr(value))
            strings_array.append(string)
        return "{}({})".format(type(self).__name__, 
                               sep.join(strings_array),
                               )
    
    def __str__(self):
        sep="\n"
        string_format = "{}={}"
        strings_array = []
        for attribute_name in self._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION:
            try:
                value = getattr(self, attribute_name)
            except AttributeError:
                value = "N/A"
            string = string_format.format(attribute_name, repr(value))
            strings_array.append(string)
        return "{}{}{}".format(type(self).__name__, 
                               ":\n", sep.join(strings_array),
                               )
    