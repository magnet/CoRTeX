# -*-coding:utf-8 -*

"""
Defines utilities pertaining to the display of information / useful for logging tasks
"""

__all__ = ["get_display_progress_function", 
           "get_conditional_display_progress_function",
           ]

import numpy
import logging


def get_display_progress_function(logger, maximum_count=None, percentage_interval=5, 
                                  count_interval=1000, personalized_progress_message=None, 
                                  logging_level="debug"):
    """ Creates a 'display function' to be used to log the progress of an iteration.
    
    The aim of this function is to create a 'display function' that shall 
    be called repeatedly along an iteration, so as to log its progress. 
    This 'display function' will expect to take a non-negative integer 'k' 
    as an input, which is supposed to be the iteration number (n°0, n°1, 
    n°2, etc), for example the 'k' variable in the example below.
    This function is useful if you already know the number 'maximum_count' of step that 
    the iteration will necessitate: in this case, a percentage progress will 
    be displayed along the value of 'k', with the possibility to specify 
    the percentage interval to use (e.g.: 5% => display progress levels 
    corresponding to 0%, 5%, 10%, etc). The progress message will be sent 
    to the 'logger' specified, using the 'logging_level' log level value. 
    You have the possibility to pass along a personalized progress message 
    if you wish to. This personalized message must be a string ready to be 
    formated using the 'format' method of python 3, with the placeholders 
    such as demonstrated in the example below. 
    
    Args:
        logger: The logger object to which the progress message will be sent.
        maximum_count: int; total size of the iteration whose progress we want to monitor. Needs 
                to be a positive integer if percentage progress is to be displayed.
        step: int; if maximum_count is defined and positive, this is the percentage value whose 
                multiples will be displayed when the iteration reaches the corresponding progress stage 
        (their values will be comprised within [0; 100)). If maximum_count is negative or not defined, 
        this is the integer interval between two progress display.
        personalized_progress_message: string; personalized_progress_message to use, see example 
                below for more information.
        logging_level: string; integer or a string among 
            ["debug", "info", "warning", "error", "critical", "fatal"], the logging level to be 
            used by the logger object.
    
    Returns:
        callable, the display function
    
    Examples:
    
        >>> import logging
        >>> logging_level = logging.INFO
        >>> logger = logging.getLogger()
        >>> an_iterator = list(range(789))
        >>> maximum_count, percentage_step = len(an_iterator), 5
        >>> personalized_progress_message = "Current progress = {percentage:6.2f} ({current:d} / {total:d})."
        >>> display_function = get_display_progress_function(logger, maximum_count, percentage_step, personalized_progress_message, logging_level)
        >>> do_something = lambda x: None
        >>> for k, an_object in enumerate(an_iterator):
        >>>     display_function(k)
        >>>     do_something(an_object)
        
        or
        
        >>> import logging
        >>> logging_level = logging.INFO
        >>> logger = logging.getLogger()
        >>> an_iterator = range(789)
        >>> maximum_count = None, 
        >>> step = 100
        >>> personalized_progress_message = "Iteration n°{current:d}."
        >>> display_function = get_display_progress_function(logger, maximum_count, step, personalized_progress_message, logging_level)
        >>> do_something = lambda x: None
        >>> for k, an_object in enumerate(an_iterator):
        >>>     display_function(k)
        >>>     do_something(an_object)
    """
    logging_lvl = _format_logging_level(logging_level)
    
    if maximum_count is not None and maximum_count >= 1:
        used_percentage_interval = float(abs(percentage_interval))
        if used_percentage_interval == 0:
            used_percentage_interval = 1
        # k belongs to [0; maximum_count-1]; more interested in specifying that the progress has begun, that in specifying that it just ended.
        possible_multipliers = numpy.array([w for w in range(0, int(numpy.floor(100 / used_percentage_interval)) + 1)], dtype=numpy.int)
        threshold_count_values = numpy.array(- numpy.floor(-maximum_count * used_percentage_interval * possible_multipliers / 100), dtype=numpy.int)
        corresponding_percentage_values = used_percentage_interval * possible_multipliers
        total_number = maximum_count*numpy.ones(threshold_count_values.shape, dtype=numpy.int)
        a_dict = dict(zip(threshold_count_values, zip(corresponding_percentage_values, threshold_count_values, total_number)))
        
        l = len(str(maximum_count))
        message = "Current progress = {percentage:6.2f}% ({current:"+str(l)+"d} / {total:"+str(l)+"d})"
        if personalized_progress_message is not None:
            message = personalized_progress_message
        
        def display_message(k):
            if (k in a_dict):
                a_tuple = a_dict[k]
                logger.log(logging_lvl, message.format(percentage=a_tuple[0], current=a_tuple[1], total=a_tuple[2]))
        result = display_message
    else:
        used_count_interval = count_interval
        if used_count_interval is None or used_count_interval < 1:
            used_count_interval = 1000
        used_count_interval = int(used_count_interval)
        
        message = "Iteration n°{current:d}"
        if personalized_progress_message is not None:
            message = personalized_progress_message
        
        result = lambda k: logger.log(logging_lvl, message.format(current=k)) if k % used_count_interval == 0 else None
    
    return result

def get_conditional_display_progress_function(verbose, logger, maximum_count, percentage_interval=5, 
                                               count_interval=1000, input_message=None, 
                                               logging_level="debug"):
    """ Creates a display function using the input arguments, if the value of the 'verbose' input 
    parameter is True.
    
    See the 'get_display_progress_function' function.
    
    Args:
        verbose: boolean, whether to effectively create the display function, or just to return a 
            dummy function that actually does nothing
        logger: The logger object to which the progress message will be sent.
        maximum_count: int; total size of the iteration whose progress we want to monitor. Needs 
                to be a positive integer if percentage progress is to be displayed.
        step: int; if maximum_count is defined and positive, this is the percentage value whose 
                multiples will be displayed when the iteration reaches the corresponding progress stage 
        (their values will be comprised within [0; 100)). If maximum_count is negative or not defined, 
        this is the integer interval between two progress display.
        personalized_progress_message: string; personalized_progress_message to use, see example 
                below for more information.
        logging_level: string; integer or a string among 
            ["debug", "info", "warning", "error", "critical", "fatal"], the logging level to be 
            used by the logger object.
    
    Returns:
        callable
    """
    return (lambda k: None) if not verbose else get_display_progress_function(logger, maximum_count=maximum_count, 
                                                                              percentage_interval=percentage_interval, 
                                                                              count_interval=count_interval, 
                                                                              personalized_progress_message=input_message, 
                                                                              logging_level=logging_level)

def create_readable_table_string(header_values, data, data_format_fcts, first_column_from_left=True):
    """ Produces a readable string to represent a table of data.
    
    Args:
        header_values: the values in of the first row of the table
        data: collection of collections of values, those collections must all have the same 
            length, and their values must be consistent
        data_format_fcts: collection of function (one per column) which take as an input a value 
            of a collection of the same column, and returns the string that should represent the 
            value in the output table string
        first_column_from_left: boolean, whether or not the values of the first column should be 
            aligned to the left (the other values of the table will be aligned to the right)
    
    Returns:
        a string representing the table of the input data
    """
    # || column 1 name         || column 2 name         ||
    # ||      1st row col1 val ||      1st row col2 val || 
    columns_nb = len(data[0])
    direction_padding_symbols = ("<" if first_column_from_left else ">",) + tuple(">" for _ in range(columns_nb-1))
    
    def _format_header_line(header_values_, column_max_sizes_):
        return "||{}||".format("||".join((" {:^"+str(column_max_size)+"s} ").format(header_value)\
                                         for header_value, column_max_size in zip(header_values_, column_max_sizes_))
                                         )
    
    def _format_line(data_row_string_values_, column_max_sizes_):
        return "||{}||".format("||".join((" {:"+direction_padding_symbol+str(column_max_size)+"s} ").format(data_row_string_value)\
                                         for data_row_string_value, direction_padding_symbol, column_max_size in zip(data_row_string_values_, 
                                                                                                                     direction_padding_symbols, 
                                                                                                                     column_max_sizes_))
                                         )
    
    header_string_values = header_values
    data_rows_string_values = []
    for data_values in data:
        l = []
        for data_format_fct, data_value in zip(data_format_fcts, data_values):
            s = data_format_fct(data_value)
            l.append(s)
        data_rows_string_values.append(l)
    
    data_rows_string_values = np.array(data_rows_string_values)
    string_lengths_fct = np.frompyfunc((lambda s: len(s)), 1, 1)
    string_lengths = string_lengths_fct(np.vstack((header_string_values, data_rows_string_values)))
    column_max_sizes = np.max(string_lengths, axis=0)
    
    strings_array = []
    header_line = _format_header_line(header_string_values, column_max_sizes)
    dash_line = "-"*len(header_line)
    strings_array.append(dash_line)
    strings_array.append(header_line)
    strings_array.append(dash_line)
    strings_array.append(dash_line)
    for data_row_string_values in data_rows_string_values:
        line = _format_line(data_row_string_values, column_max_sizes)
        strings_array.append(line)
        strings_array.append(dash_line)
    
    report = "{}\n".format("\n".join(strings_array))
    return report


# Utilities
_LOGGING_LEVELS = {"debug": logging.DEBUG, "info": logging.INFO, "error": logging.ERROR, 
                  "warn": logging.WARN, "warning": logging.WARNING, "error": logging.ERROR, 
                  "critical": logging.CRITICAL, "fatal": logging.FATAL}
_POSSIBLE_LOGGING_LEVELS_KEYS =  str(sorted(list(_LOGGING_LEVELS.keys())))

def _format_logging_level(logging_level):
    """ Checks the input value to make sure that it is a suitable value to act as a 'logging level' 
    value.
    
    That is, it must be either a string among the following: ["debug", "info", "warning", "warn", 
    "error", "critical", "fatal"]; or an int.
    If the value is a string, and correct (not taking case into account), then the corresponding, 
    standard value will be returned.
    Else the input value will be returned.
    
    Args:
        logging_level: string, or other python object

    Returns:
        string, or other python object
    
    Raises:
        ValueError: if the input valie is a string that is incorrect
    """
    if isinstance(logging_level, str):
        logging_lvl = logging_level.lower()
        if logging_lvl in _LOGGING_LEVELS:
            logging_lvl = _LOGGING_LEVELS[logging_lvl]
        else:
            error_message = "'{:s}' is not a correct logging level name (possible names are '{:s}')"
            error_message = error_message.format(logging_level, _POSSIBLE_LOGGING_LEVELS_KEYS)
            raise ValueError(error_message)
    else:
        logging_lvl = logging_level
    return logging_lvl
