# -*- coding: utf-8 -*-

"""
Defines utilities pertaining to the processing of textual information within 
'xml.etree.cElementTree' nodes
"""

__all__ = ["xml_escape", 
           "xml_unescape", 
           "normalize_spaces", 
           "indent", 
           "add_unique_field_to_dict_from_subelt",
           "IncorrectXMLElementError",
           ]

def xml_escape(str_):
    """ Escapes specific xml characters that might exist within the input string.
    
    Notably:
    
        * '&' => '&amp;'
        * '<' => '&lt;'
        * '>' => '&gt;'
    
    Args:
        str_: string

    Returns:
        string
    """
    # You could also use: 'from xml.sax.saxutils import escape'
    # Caution: you have to escape '&' first!
    return str_.replace(u'&',u'&amp;').replace(u'<',u'&lt;').replace(u'>',u'&gt;')

def xml_unescape(str_):
    """ Unescapes specific escaped xml characters that might exist within the input string.
    
    Notably:
    
        * '&amp;' => '&'
        * '&lt;' => '<'
        * '&gt;' => '>'
    
    Args:
        str_: string

    Returns:
        string
    """
    return str_.replace(u'&amp;',u'&').replace(u'&lt;',u'<').replace(u'&gt;',u'>')


def normalize_spaces(str_):
    """ Replaces carriage returns by simple white spaces -- does not change offsets.
    
    Args:
        str_: string

    Returns:
        string
    """
    #str_ = re.sub(r'[\n\r]', r' ', _str)
    return str_.replace(u'\n',u' ').replace(u'\r',u' ').replace(u'^M',u' ')

def indent(elem, level=0):
    """ Indents the string content of a xml.etree.cElementTree.Element for xml pretty print / writing 
    in xml file
    
    Args:
        elem: a xml.etree.cElementTree.Element instance
        level: non-negative int, the indentation level to use, i.e. the number of indentation steps 
            to prefix each line with
    """
    str_ = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = str_ + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = str_
        for elem in elem:
            indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = str_
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = str_

def add_unique_field_to_dict_from_subelt(elt, subelt_name_field_name_pairs, dict_, fct_, enforce_presence=False):
    """ Parses a xml.etree.cElementTree.Element instance looking for one potential sub-elements going 
    by a specific name, and update the input dict by setting a (key; value) pairs, with the key being 
    provided by the user, and the value being equal to the result of the value of the 'text' attribute, of the 
    potentially found sub-element, being processed by the input function.
    
    Args:
        elt: a xml.etree.cElementTree.Element instance
        subelt_name_field_name_pairs: pair of string, where the first is the type of the potential 
            sub-element to look for, and the second is the value of the key to use when updating the input dict. 
        dict_: dictionary instance, to update with the value of the 'text' attribute of the
        fct_: a callable that must take a string as an input, will be called using the 'text' 
            attribute value of the potentially found sub-element as an input; the resulting value is the one 
            that will be used to update the dictionary instance
        enforce_presence: boolean; whether or not to raise an Exception if no field by the 
            provided name was found as a sub-element of the input element
    """
    subelt_name, field_name = subelt_name_field_name_pairs
    subelts = elt.findall(subelt_name)
    L = len(subelts)
    if L == 0:
        if enforce_presence:
            raise IncorrectXMLElementError("The input xml element contains no '{}' element.".format(subelt_name))
    else:
        if L > 1:
            raise IncorrectXMLElementError("The input xml element contains more than one '{}' element.".format(subelt_name))    
        elt = subelts[0]
        value = elt.text
        if value is not None:
            dict_[field_name] = fct_(xml_unescape(value))


# Exceptions
class IncorrectXMLElementError(Exception):
    """ Exception to be raised when parsing an xml elements that lacks needed data. """
    pass
