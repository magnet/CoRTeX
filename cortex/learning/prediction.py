# -*- coding: utf-8 -*-

"""
Defines classes used to represent the result of a ML model prediction regarding a Sample child 
class instance
"""

import abc
from operator import itemgetter

__all__ = ["ClassificationPrediction",
           "_Prediction", 
           ]


class _Prediction(object, metaclass=abc.ABCMeta):
    """ Base class for classes representing a prediction made for a ml sample. """
    pass


class ClassificationPrediction(_Prediction):
    """ Class used to represent the result of a classification prediction. Assume that each 
    classification class label is associated a score value, and assume that the predicted class label 
    is the one with the highest score.
    
    Arguments:
        label_score_pairs: a collection of (label, score) tuples, where 'score' is a float, and each 
            'label' values are supposed to be hashable and distinct one from another
    """
    
    def __init__(self, label_score_pairs):
        self._label_score_pairs = sorted(label_score_pairs, key=itemgetter(1)) # sort by scores
        self._label2score = {label: score for label, score in self._label_score_pairs}
        self._labels, self._scores = map(tuple, zip(*self._label_score_pairs))

    def get_labels(self):
        """ Returns the collection of labels used to define this instance.
        
        Returns:
            collection of labels (cf the class docstring)
        """
        return self._labels
    
    def get_max_label(self):
        """ Returns the label value associated to the highest score value. In case of tie, will be 
        the last value with this score present in the collection input during the creation of this 
        instance.
        
        Returns:
            label
        """
        return self._labels[-1] # Because pairs (label; score) were sorted by increasing score value at init time

    def get_max_score(self):
        """ Return the maximum score associated to a label.
        
        Returns:
            float
        """
        return self._scores[-1] # Because pairs (label; score) were sorted by increasing score value at init time
    
    def get_max_pred(self):
        """ Returns the (label; score) pair associated to the highest score value. In case of tie, 
        will be the last pair with this score present in the collection input during the creation of 
        this instance.
        
        Returns:
            a (label, float) pair
        """
        return self._label_score_pairs[-1] # Because pairs (label; score) were sorted by increasing score value at init time
    
    def get_label_score(self, label):
        """ Returns the score value associated to a given label.
        
        Args:
            label: label
        
        Returns:
            float
        """
        try:
            score = self._label2score[label]
        except IndexError:
            msg = "Incorrect label value '{}', possible values are: {}."
            msg = msg.format(label, tuple(self._label2score.keys()))
            raise ValueError(msg)
        return score
    
    def get_label(self):
        """ Returns the label to be predicted according to this instance, which is the label value 
        associated to the highest score value. In case of tie, will be the last value with this score 
        present in the collection that was input during the creation of this instance.
        
        Returns:
            label
        """
        return self.get_max_label()

    def get_score(self):
        """ Returns the score of the label to be predicted according to this instance, which is the 
        score to the highest score value.
        
        Returns:
            float
        """
        return self.get_max_score()
    
    def get_pred(self):
        """ Returns the (label; score) associated to the label to be predicted according to this 
        instance, which is the label value associated to the highest score value. In case of tie, 
        will be the last value with this score present in the collection input during the creation 
        of this instance.
        
        Returns:
            a (label, float) pair
        """
        return self.get_max_pred()
    
    ##
    def __repr__(self):
        return "ClassificationPrediction({})".format(self._label_score_pairs)
    
    def __str__(self):
        return str(tuple(sorted(zip(self._labels,self._scores))))
    
    def __eq__(self, other):
        return self._label_score_pairs == other._label_score_pairs
    
    def __ne__(self, other):
        return not self == other
    
    def __hash__(self):
        return hash(self._label_score_pairs)
