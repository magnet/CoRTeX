# -*- coding: utf-8 -*-

"""
Defines parameters pertaining to the translation of tasks such as coreference partition resolution 
to ML tasks, such as binary classification
"""

# Binary classification classes label
POS_CLASS = "1"
NEG_CLASS = "-1"
