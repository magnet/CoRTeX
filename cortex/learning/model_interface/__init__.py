# -*- coding: utf-8 -*-'

"""
Defines classes used to interface between ML models and Sample child classes instances, and 
translate instruction regarding a set of such instances into instructions for the underlying 
ML model
"""

"""
Available submodule(s)
----------------------
base
    Defines base model interface classes

online
    Defines model interface class made to interface with online binary classifier classes

sklearn
    Defines model interface class made to interface with sklearn binary classifier classes
"""

__all__ = ["SKLEARNInterface", "OnlineInterface",
           "model_interface_factory",
           ]

import itertools
from collections import OrderedDict

from .online import OnlineInterface
from .sklearn import SKLEARNInterface
from cortex.tools import (define_check_item_name_function, define_create_function, 
                          define_generate_factory_configuration_function, 
                          define_create_from_factory_configuration_function,
                          )


model_interface_names_collection_and_filter_class_pairs = ((("online", OnlineInterface.__name__), OnlineInterface),
                                                           (("sklearn", SKLEARNInterface.__name__), SKLEARNInterface),
                                                           )
model_interface_name2model_interface_class = OrderedDict((name, class_) for names, class_ in model_interface_names_collection_and_filter_class_pairs for name in names)


# High level functions
_check_model_interface_name = define_check_item_name_function(model_interface_name2model_interface_class, "model_interface")

create_model_interface = define_create_function(model_interface_name2model_interface_class, "model_interface")

create_model_interface_from_factory_configuration =\
 define_create_from_factory_configuration_function(model_interface_name2model_interface_class, "model_interface")

generate_model_interface_factory_configuration =\
 define_generate_factory_configuration_function(model_interface_name2model_interface_class, "model_interface")

