# -*- coding: utf-8 -*-

"""
Defines model interface class made to interface with sklearn binary classifier classes
"""

__all__ = ["SKLEARNInterface",
           "_BaseSKLEARNInterface",
           ]

from .base import _BaseClassifierInterface
from cortex.learning.ml_model.classifier.sklearn_binary_classifier import (_SKLEARNBinaryClassifier, 
                                                                           classifier_name2classifier_class)

class _BaseSKLEARNInterface(_BaseClassifierInterface):
    """ Base class for classes whose instances act as interfaces between sklearn classification 
    machine learning model instances, and those models being used as an underlying layer by resolvers.
    
    Arguments:
        sample_features_vectorizer: a _SampleFeatureVectorizer child class instance, to be used to 
            vectorize ML samples
        
        classifier_factory_configuration: a Mapping instance; configuration specifying a supported 
            online classification machine learning model along with its hyperparameters (cf child classes 
            of the _SKLEARNBinaryClassifier class, in the 'ml_model.classifier.sklearn' module).
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SCORES_ARE_PROBA: boolean; whether or not the score values associated to the prediction made 
            by the underlying model should be interpreted as probability values or not
        
        document_hash2cache: the "document hash key => cache" map currently used
        
        iteration_nb: an integer value corresponding to number of time a learning step has been 
            carried out on the underlying sklearn classification ML model, if applicable for this model.
            If not applicable, an AttributeError will be raised when trying to access it.
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def __init__(self, sample_features_vectorizer, classifier_factory_configuration, configuration=None):
        # Check input value
        # Initialize using parent class
        super().__init__(sample_features_vectorizer, classifier_factory_configuration, configuration=configuration)
        # Set attribute values
        # All value that can be input into the '__init__' function must be recorded in the configuration
        # Final check
    
    @property
    def iteration_nb(self):
        try:
            iter_nb = self._classifier.n_iter_
        except AttributeError:
            msg = "'iteration_nb': the underlying SKLEARN's classifier model ('{}') does not possess "\
                  "something akin to an 'iteration nb' attribute, cannot proceed."
            msg = msg.format(type(self._classifier))
            raise AttributeError(msg)
        return iter_nb
    @iteration_nb.setter
    def iteration_nb(self, value):
        raise AttributeError
    
    def _learn_with_classifier(self, data, coded_labels):
        """ Carries out a learning step for the underlying online classification ML model
        
        Args:
            data: a (samples_nb; features_nb)-shaped scipy sparse matrix or numpy array, 
                corresponding to the vectorized samples
            coded_labels: a (samples_nb,)-shaped numpy array, corresponding to the class label 
                associated to each sample
        """
        self._classifier.fit(data, coded_labels)


class SKLEARNInterface(_BaseSKLEARNInterface):
    """ Class whose instances act as interfaces between sklearn classification machine learning model 
    instances, and those models being used as an underlying layer by resolvers.
    
    Arguments:
        sample_features_vectorizer: a _SampleFeatureVectorizer child class instance, to be used to 
            vectorize ML samples
        
        classifier_factory_configuration: a Mapping instance; configuration specifying a supported 
            online classification machine learning model along with its hyperparameters (cf child classes 
            of the _SKLEARNBinaryClassifier class, in the 'ml_model.classifier.sklearn' module).
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SCORES_ARE_PROBA: boolean; whether or not the score values associated to the prediction made 
            by the underlying model should be interpreted as probability values or not
        
        document_hash2cache: the "document hash key => cache" map currently used
        
        iteration_nb: an integer value corresponding to number of time a learning step has been 
            carried out on the underlying sklearn classification ML model, if applicable for this model.
            If not applicable, an AttributeError will be raised when trying to access it.
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def __init__(self, sample_features_vectorizer, classifier_factory_configuration, configuration=None):
        # Check input value
        # Initialize using parent class
        _BaseSKLEARNInterface.__init__(self, sample_features_vectorizer, classifier_factory_configuration, 
                                      configuration=configuration)
        # Set attribute values
        # All value that can be input into the '__init__' function must be recorded in the configuration; here there is None
        # Final check
        if not isinstance(self._classifier, _SKLEARNBinaryClassifier):
            message = "'{}' input is not a configuration for a {} class descendant classifier; the "\
                      "type of the classifier defined with it is '{}' instead."
            message = message.format(classifier_factory_configuration, _SKLEARNBinaryClassifier, type(self._classifier))
            raise ValueError(message)
    
    @classmethod
    def define_configuration(cls, sample_features_vectorizer_factory_configuration, 
                             classifier_factory_configuration):
        """ Create a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Cf the documentation of the class of know which argument(s) to use.
        
        Returns:
            a Mapping instance
        """
        if classifier_factory_configuration["name"] not in classifier_name2classifier_class:
            msg = "Incorrect input classifier_factory_configurations name '{}': must belong to '{}'."
            msg = msg.format(classifier_factory_configuration["name"], 
                             set(classifier_name2classifier_class.keys()))
            raise ValueError(msg)
        return super().define_configuration(sample_features_vectorizer_factory_configuration, 
                                            classifier_factory_configuration)

