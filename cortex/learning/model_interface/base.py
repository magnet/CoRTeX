# -*- coding: utf-8 -*-

"""
Defines base model interface classes
"""

__all__ = ["_ModelInterface", 
           "_BaseClassifierInterface", 
           "UnauthorizedOperationError",
           ]

import abc
import logging
import os
from collections import OrderedDict

from cortex.utils import EqualityMixIn, _check_isinstance, CoRTeXException
from cortex.tools import ConfigurationMixIn, _create_attribute_data, Mapping
from ..parameters import POS_CLASS, NEG_CLASS
from ..prediction import ClassificationPrediction
from ..classification_sample_converter import ClassificationSampleConverter
from ..sample_features_vectorizer import (create_sample_features_vectorizer_from_factory_configuration, 
                                          _SampleFeaturesVectorizer)
from ..ml_model.classifier import create_classifier_from_factory_configuration
from cortex.parameters import ENCODING




_set_parameter_value_in_configuration_fct = lambda instance: setattr(instance.configuration, "sample_features_vectorizer", Mapping([("name", instance._classification_sample_converter._sample_features_vectorizer.NAME), 
                                                                                                                                   ("config", instance._classification_sample_converter._sample_features_vectorizer.configuration),
                                                                                                                                   ]))
sample_features_vectorizer_parameter_data = _create_attribute_data("sample_features_vectorizer", 
                                                                   postprocess_fct=create_sample_features_vectorizer_from_factory_configuration, 
                                                                   set_parameter_value_in_configuration_fct=_set_parameter_value_in_configuration_fct)
class _ModelInterface(ConfigurationMixIn, EqualityMixIn, metaclass=abc.ABCMeta):
    """ Base class for classes whose instances act as interfaces between general machine learning 
    model instances, and those models being used as an underlying layer by resolvers. The role of 
    such classes is to convert resolver's needs into actions to be carried out on underlying machine 
    learning models.
    
    Argument
        sample_features_vectorizer: a _SampleFeatureVectorizer child class instance, to be used to 
            vectorize ML samples
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attribute
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        logger: logger instance associated to the class instance
        
        SCORES_ARE_PROBA: boolean; whether or not the score values associated to the prediction made 
            by the underlying model should be interpreted as probability values or not
        
        document_hash2cache: the "document hash key => cache" map currently used
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    # TODO: Make it so this base class makes no reference to the fact that, currently, it is used 
    # only with classification ML models.
    # Some of its architecture makes the implicit hypothesis that the ML models are classification 
    # models. That should be changed.
    
    __INIT_PARAMETERS = OrderedDict((sample_features_vectorizer_parameter_data,))
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = ("_classification_sample_converter",) + ("configuration",)
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = ("_classification_sample_converter",)
    _NEG_LABEL = NEG_CLASS
    _POS_LABEL = POS_CLASS
    _LABELS = (_NEG_LABEL, _POS_LABEL)
    SCORES_ARE_PROBA = None
    _FEATURES_NAMES_FILE_NAME = "features_names.txt"
    
    def __init__(self, sample_features_vectorizer, configuration=None):
        # Check input values
        _check_isinstance(_SampleFeaturesVectorizer, sample_features_vectorizer, "sample_features_vectorizer", strict=False)
        # Initialize using parent class
        ConfigurationMixIn.__init__(self, configuration=configuration)
        # Set attribute values
        self._classification_sample_converter = ClassificationSampleConverter(self._LABELS, sample_features_vectorizer)
        self._logger = logging.getLogger(self.NAME)
        self.toggle_memoization(document_hash2cache=None)
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    # Methods
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        sample_features_vectorizer, = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        args = args + (sample_features_vectorizer,)
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, sample_features_vectorizer_factory_configuration):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Cf the documentation of the class of know which argument(s) to use.
        
        Returns:
            a Mapping instance
        """
        configuration = super().define_configuration()
        configuration["sample_features_vectorizer"] = sample_features_vectorizer_factory_configuration
        return configuration
    
    @abc.abstractproperty
    def SCORES_ARE_PROBA(self):
        return None
    @SCORES_ARE_PROBA.setter
    def SCORES_ARE_PROBA(self, value):
        raise AttributeError
    
    @property
    def document_hash2cache(self):
        return self._classification_sample_converter.document_hash2cache
    @document_hash2cache.setter
    def document_hash2cache(self, value):
        self._classification_sample_converter.document_hash2cache = value
    
    def toggle_memoization(self, document_hash2cache=None):
        """ Activates the use of the input caching system by this class instance.
        
        Args:
            document_hash2cache: the "document hash key => cache" map to use, or None; if None, 
                deactivate the use of the possible previous map

        Returns:
            the previously used value of the "document_hash2cache" attribute
        """
        previous_document_hash2cache = self.document_hash2cache
        if document_hash2cache is None:
            self._convert_fct = lambda *args, **kwargs: self._classification_sample_converter.convert(*args, **kwargs)
            self.document_hash2cache = None
            self._vectorize_fct = lambda *args, **kwargs: self._classification_sample_converter.vectorize(*args, **kwargs)
        else:
            self.document_hash2cache = document_hash2cache
            self._convert_fct = lambda *args, **kwargs: self._classification_sample_converter.convert_with_memoization(*args, **kwargs)
            self._vectorize_fct = lambda *args, **kwargs: self._classification_sample_converter.vectorize_with_memoization(*args, **kwargs)
        return previous_document_hash2cache
    
    def _convert(self, samples):
        """ Converts the input samples by applying the function corresponding to the value of the 
        '_convert_fct' attribute value.
        
        Args:
            samples: a collection of instances of a class used to represent a machine-learning sample 
                that can be used with the specific model interface used
        
        Returns:
            the output of the current '_convert_fct' attribute value when applied to the input
        """
        return self._convert_fct(samples)
    
    def _vectorize(self, samples):
        """ Vectorizes the input samples by applying the function corresponding to the value of the 
        '_vectorize_fct' attribute.
        
        Args:
            samples: a collection of instances of a class used to represent a machine-learning sample 
                that can be used with the specific model interface used
        
        Returns:
            the output of the current '_vectorize_fct' attribute value when applied to the input
        """
        return self._vectorize_fct(samples)
    
    # Abstract methods
    @abc.abstractmethod
    def learn(self, samples_iterable):
        """ Carries out one learning step, using the input samples as training data.
        
        Args:
            sample_iterable: an iterable over instances of a class used to represent a 
                machine-learning sample that can be used with the specific model interface used; if the 
                model interface represent a supervised machine-learning task, then each sample must be 
                properly labelled
        """
        pass
    
    @abc.abstractmethod
    def predict(self, samples_iterable):
        """ Carries out one prediction step, for the input samples.
        
        Args:
            sample_iterable: an iterable over instances of a class used to represent a 
                machine-learning sample that can be used with the specific model interface used

        Returns:
            an iterable over instances of a ML prediction class, whose order is such that each 
            can be associated to the corresponding ML sample in the input iterable
        """
        pass
    
    @abc.abstractmethod
    def _can_predict(self):
        """ Checks whether or not the model interface's different attributes are ready or consistent 
        enough to carry out prediction
    
        Returns:
            a (boolean, 'reason') pair, 'reason' being a small string hinting at what the 
            reason is in case the boolean is False
        """
        pass
    
    @abc.abstractmethod
    def save_model(self, folder_path):
        """ Non abstract version of this method should implement the persistence of the data of the 
        underlying ML model. This version persists data regarding the vector space that the model 
        will have been trained on, in a local folder. Call to this specific method can be made either 
        after or before persistence of data related to the model, a priori.
        
        Args:
            folder_path: string, path to the local folder where the data shall be persisted
        """
        features_names_file_path = os.path.join(folder_path, self._FEATURES_NAMES_FILE_NAME)
        features_names = self._classification_sample_converter.features_names
        with open(features_names_file_path, "w", encoding=ENCODING) as f:
            f.write("\n".join(features_names))
    
    @abc.abstractmethod
    def load_model(self, folder_path):
        """ Non abstract version of this function should use previously locally persisted data to 
        initialize the internal state of the ML model. This version loads up persisted data about 
        the vector space used in conjunction with a ML model, so as to check that the vector space 
        currently used indeed corresponds to the one a persisted ML model had been trained on. Call 
        to this specific method must be made after loading up the data of a previous model, as a 
        'fit for prediction' test will be carried out, and it will surely fail if the ML model has 
        just been recreated and not initialized / loaded up yet..
        
        Args:
            folder_path: string, path to the local folder from which that data shall be loaded
        """
        # Check that the loaded is such that it can indeed be used in conjunction with this interface's current classification encoder to carry out prediction tasks
        result, reason_message = self._can_predict()
        if not result:
            self._logger.warning("The loaded model is unfit for prediction ('{}').".format(reason_message))
        else:
            # Check that the features that were used to train the loaded model are the same than the ones currently used by the interface
            features_names_file_path = os.path.join(folder_path, self._FEATURES_NAMES_FILE_NAME)
            current_features_names = self._classification_sample_converter.features_names
            with open(features_names_file_path, "r", encoding=ENCODING) as f:
                read_features_names = tuple(s.strip() for s in f.read().strip().split("\n"))
            if current_features_names != read_features_names:
                msg = "The loaded model's features names are different from the features names' of the "\
                "features used by this instance's classification encoder.\nPrediction can be carried out,"\
                " but doing so may be nonsensical wrt a prediction task using this model."
                self._logger.warning(msg)
    
    def can_predict(self):
        """ Checks whether or not the model interface's different attributes are ready or consistent 
        enough to carry out prediction

        Returns:
            boolean
        """
        result, _ = self._can_predict() #reason_message
        return result
    
    def _check_can_predict(self): #FIXME: remove it, replace its use with appropriate try except
        result, reason_message = self._can_predict()
        if not result:
            raise UnauthorizedOperationError(reason_message)



_set_parameter_value_in_configuration_fct = lambda instance: setattr(instance.configuration, "classifier", Mapping([("name", instance._classifier.NAME), 
                                                                                                                   ("config", instance._classifier.configuration),
                                                                                                                   ]))
classifier_configuration_parameter_data = _create_attribute_data("classifier", 
                                                                 set_parameter_value_in_configuration_fct=_set_parameter_value_in_configuration_fct)
class _BaseClassifierInterface(_ModelInterface):
    """ Base class for classes whose instances act as interfaces between classification machine 
    learning model instances, and those models being used as an underlying layer by resolvers.
    
    Argument
        sample_features_vectorizer: a _SampleFeatureVectorizer child class instance, to be used to 
            vectorize ML samples
        
        classifier_factory_configuration: a Mapping instance; configuration specifying a supported 
            classification machine learning model along with its hyperparameters (cf child classes of the 
            _Classifier class, in the 'ml_model.classifier' package)
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attribute
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SCORES_ARE_PROBA: boolean; whether or not the score values associated to the prediction made 
            by the underlying model should be interpreted as probability values or not
        
        document_hash2cache: the "document hash key => cache" map currently used
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    __INIT_PARAMETERS = OrderedDict((classifier_configuration_parameter_data,))
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = ("_classifier",) \
                                                + _ModelInterface._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = _ModelInterface._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION+\
                                            ("_classifier",)
    
    def __init__(self, sample_features_vectorizer, classifier_factory_configuration, configuration=None):
        # Check input value
        _check_isinstance(Mapping, classifier_factory_configuration, "classifier_factory_configuration")
        # Initialize using parent class
        super().__init__(sample_features_vectorizer, configuration=configuration)
        # Set attribute values
        classifier_factory_configuration["config"].positive_class_label = self._classification_sample_converter.encode_label(self._POS_LABEL)
        classifier_factory_configuration["config"].negative_class_label = self._classification_sample_converter.encode_label(self._NEG_LABEL)
        _classifier = create_classifier_from_factory_configuration(classifier_factory_configuration)
        self._classifier = _classifier
        del self._classifier.configuration["positive_class_label"] # We do not want this to stay in the configuration if the configuration is to be part of the configuration for the OnlineInterface instance.
        del self._classifier.configuration["negative_class_label"] # We do not want this to stay in the configuration if the configuration is to be part of the configuration for the OnlineInterface instance.
        # All value that can be input into the '__init__' function must be recorded in the configuration; here there is None
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        classifier_configuration, = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        args = args + (classifier_configuration,)
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, sample_features_vectorizer_factory_configuration, 
                             classifier_factory_configuration):
        """ Create a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Cf the documentation of the class of know which argument(s) to use.
        
        Returns:
            a Mapping instance
        """
        configuration = super().define_configuration(sample_features_vectorizer_factory_configuration)
        configuration["classifier"] = classifier_factory_configuration
        return configuration
    
    @property
    def SCORES_ARE_PROBA(self):
        return self._classifier.SCORES_ARE_PROBA
    @SCORES_ARE_PROBA.setter
    def SCORES_ARE_PROBA(self, value):
        raise AttributeError
    
    @abc.abstractmethod
    def _learn_with_classifier(self, data, coded_labels):
        """ Carries out a learning step for the underlying ML model
        
        Args:
            data: a (samples_nb; features_nb)-shaped scipy sparse matrix or numpy array, 
                corresponding to the vectorized samples
            coded_labels: a (samples_nb,)-shaped numpy array, corresponding to the class label 
                associated to each sample
        """
        pass
    
    def learn(self, classification_samples):
        """ Carries out a learning step using the input samples
        
        Args:
            classification_samples: collection of labelled classification samples
        """
        if not len(classification_samples):
            msg = "The input classification samples collection is empty, cannot carry out the learning of a model."
            raise ValueError(msg)
        
        # Vectorize and create learning data
        self._logger.debug("Vectorizing {} sample(s)...".format(len(classification_samples)))
        data, coded_labels = self._convert(classification_samples)
        
        # Training the model (potentially continuing the training)
        self._logger.debug("Training {}'s model's content...".format(self.NAME))
        self._learn_with_classifier(data, coded_labels)
    
    def predict(self, classification_samples):
        """ Uses the underlying ML model to predict a class label for each sample of the input collection.
        
        Args:
            classification_samples: collection of classification samples

        Returns:
            a collection of ClassificationPrediction instances, whose order is such that each 
            can be associated to the corresponding ClassificationSample instance in the input collection
        """
        self._logger.debug("Predicting with {}'s model for {} sample(s)...".format(self.NAME, len(classification_samples)))
        #self._check_can_predict() #FIXME: remove it, to replace with appropriate try except
        
        # Predicting
        if not len(classification_samples):
            return tuple()
        
        ## Encode data
        self._logger.debug("Vectorizing {} sample(s)...".format(len(classification_samples)))
        data = self._vectorize(classification_samples)
        self._logger.debug("Finished vectorizing {} sample(s).".format(len(classification_samples)))
        ## Predict with model
        self._logger.debug("Predicting with {}'s model...".format(self.NAME))
        ## Predict class scores
        predicted_scores = self._classifier.predict_class_scores(data)
        ## Decode prediction results into classification instances
        predictions = self._decode_class_scores(predicted_scores)
        self._logger.debug("Finished predicting with {}'s model.".format(self.NAME))
        return predictions
    
    def _convert_class_scores_to_label_score_pairs(self, class_scores):
        """ Converts a given collection of scores associated to each possible coded class label that the 
        underlying classification ML model has been initialized with, into the collection of 
        (corresponding decoded class label; score) pairs.
        
        Such an input is for instance produced as a prediction made by the underlying classification 
        ML model for a given sample.
        
        Args:
            class_scores: collection of float, of length equal to number of classes

        Returns:
            collection of (decoded class label; score) pairs
        """
        return tuple((self._classification_sample_converter.decode_label(coded_class_label), score)\
                     for coded_class_label, score in zip(self._classifier.class_labels, class_scores)
                     )
    
    def _decode_class_scores(self, class_scores_iterable):
        """ Iterates over prediction scores to produce the corresponding ClassfificationPrediction 
        instances.
        
        It is assumed 'class_scores_iterable' is an iterable over collection of score values 
        representing confidence in the classification of a sample into the corresponding classes: 
        the class that should predicted is the one whose score is the highest.
        
        Also, we assume the underlying model has already been trained.
        
        Args:
            class_scores_iterable: an iterable over 1D array-likes of score values

        Returns:
            ClassificationPrediction instances collection
        """
        predictions = tuple(ClassificationPrediction(self._convert_class_scores_to_label_score_pairs(scores))\
                             for scores in class_scores_iterable
                            )
        return predictions
    
    def save_model(self, folder_path):
        """ Persists the data of the underlying ML model, as well as data regarding the vector space 
        that the model has been trained on, in a local folder.
        
        Args:
            folder_path: string, path to the local folder where the data shall be persisted
        """
        # Save classifier's model
        self._logger.debug("Saving {}'s classifier model in {}...".format(self.NAME, folder_path))
        self._classifier.save_model(folder_path)
        self._logger.debug("Finished saving {}'s classifier model in {}.".format(self.NAME, folder_path))
        # Save data about features names, can be used as interpretation help and checking tool when loading the model data back
        super().save_model(folder_path)
    
    def load_model(self, folder_path):
        """ Uses previously locally persisted data to initialize the internal state of the ML model, 
        as well as to check that the vector space currently used indeed corresponds to the one the ML 
        model had been trained on.
        
        Args:
            folder_path: string, path to the local folder from which that data shall be loaded
        """
        # Load classifier's model
        self._logger.debug("Loading {}'s classifier model's content from {}...".format(self.NAME, folder_path))
        self._classifier.load_model(folder_path)
        self._logger.debug("Finished loading {}'s classifier model's content from {}.".format(self.NAME, folder_path))
        # Carry out some checks, notably using the saved features names data
        super().load_model(folder_path)
    
    def _can_predict(self):
        """ Checks whether or not the model interface's different attributes are ready or consistent 
        enough to carry out prediction.

        Returns:
            a (boolean, 'reason') pair, 'reason' being a small string hinting at what the 
            reason is in case the boolean is False
        """
        # First, check classifier model's existence
        if not self._classifier.can_predict():
            return False, "The interface's classifier model has not been learnt, cannot perform a prediction."
        # Second, check consistency between existing model and classification sample encoder
        ## Regarding the features to use
        model_features_nb = self._classifier.coef_.shape[1]
        features_nb = self._classification_sample_converter.features_nb
        if features_nb != model_features_nb:
            message = "The trained classifier model uses a different number of features ('{}') than "\
                      "what is currently defined for this interface ('{}'), cannot perform a prediction."
            message = message.format(model_features_nb, features_nb)
            return False, message
        return True, None


# Exceptions
class UnauthorizedOperationError(CoRTeXException):
    """ Exception to be raised when failing the 'fit for prediction test? """
    pass
