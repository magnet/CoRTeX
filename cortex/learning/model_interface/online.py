# -*- coding: utf-8 -*-

"""
Defines model interface class made to interface with online binary classifier classes
"""

__all__ = ["OnlineInterface",
           "_BaseOnlineClassifierInterface",
           ]

import abc

from .base import _BaseClassifierInterface
from ..ml_model.classifier.online_binary_classifier import (_OnlineBinaryClassifier, 
                                                            classifier_name2classifier_class)


class _BaseOnlineClassifierInterface(_BaseClassifierInterface):
    """ Base class for classes whose instances act as interfaces between online classification 
    machine learning model instances, and those models being used as an underlying layer by resolvers.
    
    Arguments:
        sample_features_vectorizer: a _SampleFeatureVectorizer child class instance, to be used to 
            vectorize ML samples
        
        classifier_factory_configuration: a Mapping instance; configuration specifying a supported 
            online classification machine learning model along with its hyperparameters (cf child classes 
            of the _OnlineBinaryClassifier class, in the 'ml_model.classifier.online' module). 
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SCORES_ARE_PROBA: boolean; whether or not the score values associated to the prediction made 
            by the underlying model should be interpreted as probability values or not
        
        document_hash2cache: the "document hash key => cache" map currently used
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    @classmethod
    def define_configuration(cls, sample_features_vectorizer_factory_configuration, 
                             classifier_factory_configuration):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Cf the documentation of the class of know which argument(s) to use.
        
        Returns:
            a Mapping instance
        """
        if classifier_factory_configuration["name"] not in classifier_name2classifier_class:
            msg = "Incorrect input classifier_configurations name '{}': must belong to '{}'."
            msg = msg.format(classifier_factory_configuration["name"], set(classifier_name2classifier_class.keys()))
            raise ValueError(msg)
        return super().define_configuration(sample_features_vectorizer_factory_configuration, 
                                               classifier_factory_configuration)
    
    @abc.abstractmethod
    def initialize_model(self):
        """ Sets the underlying online classification ML model instance attributes with correct initial values. """
        pass
    
    def learn_sum(self, positive_class_samples, negative_class_samples, loss):
        """ Updates the model with the numeric vector corresponding to the sum of numeric vectors 
        corresponding to the samples of the 'positive class samples' collection, minus the sum of 
        numeric vectors corresponding to the samples of the 'negative class samples' collection. 
        
        The update made using the resulting vector as base material, and guided using the input 
        'loss' value;
        
        Args:
            positive_class_samples: collection of samples which are associated to the positive class
            negative_class_samples: collection of samples which are associated to the negative class
            loss: float, loss corresponding to the learning of the sum of positive and negative samples
        """
        self._logger.debug("Carrying a 'learn_sum' step with {} positive sample(s) and {} negative sample(s)...".format(len(positive_class_samples), len(negative_class_samples)))
        if positive_class_samples or negative_class_samples:
            positive_class_data = self._vectorize(positive_class_samples)
            negative_class_data = self._vectorize(negative_class_samples)
            self._classifier.update_sum(positive_class_data, negative_class_data, loss)


class OnlineInterface(_BaseOnlineClassifierInterface):
    """ Class whose instances act as interfaces between online classification machine learning 
    model instances, and those models being used as an underlying layer by resolvers.
    Instances of underlying online classification ML model classes must posses an 'iteration_nb' 
    attribute and a 'train_mode' attribute.
    
    Arguments:
        sample_features_vectorizer: a _SampleFeatureVectorizer child class instance, to be used to 
            vectorize ML samples
        
        classifier_factory_configuration: a Mapping instance; configuration specifying a supported 
            online classification machine learning model along with its hyperparameters (cf child classes 
            of the _OnlineBinaryClassifier class, in the 'ml_model.classifier.online' module). 
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SCORES_ARE_PROBA: boolean; whether or not the score values associated to the prediction made 
            by the underlying model should be interpreted as probability values or not
        
        document_hash2cache: the "document hash key => cache" map currently used
        
        iteration_nb: the 'iteration_nb' attribute value of the underlying online classification ML model
        
        train_mode: boolean; allows to read and set the 'train_mode' attribute value of the 
            underlying online classification ML model
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def __init__(self, sample_features_vectorizer, classifier_factory_configuration, configuration=None):
        # Check input value
        # Initialize using parent class
        super().__init__(sample_features_vectorizer, classifier_factory_configuration, configuration=configuration)
        # Set attribute values
        # All value that can be input into the '__init__' function must be recorded in the configuration; here there is None
        # Final check
        if not isinstance(self._classifier, _OnlineBinaryClassifier):
            message = "'{}' input is not a configuration for a {} class descendant classifier; the "\
                      "type of the classifier defined with it is '{}' instead."
            message = message.format(classifier_factory_configuration, _OnlineBinaryClassifier, type(self._classifier))
            raise ValueError(message)
    
    @property
    def iteration_nb(self):
        return self._classifier.iteration_nb
    @iteration_nb.setter
    def iteration_nb(self, value):
        raise AttributeError
    
    @property
    def train_mode(self):
        return self._classifier.train_mode
    @train_mode.setter
    def train_mode(self, value):
        self._classifier.train_mode = value
    
    def initialize_model(self):
        """ Sets the underlying online classification ML model instance attributes with correct initial values. """
        if not (self._classifier.warm_start and self._classifier.can_predict()):
            self._classifier.init_model(self._classification_sample_converter.features_nb)
    
    def set_avg(self, avg):
        """ Sets the 'avg' attribute value of the underlying online classification ML model instance 
        to the input value.
        
        Args:
            avg: boolean; the value to set the 'avg' attribute of the underlying online 
                classification ML model instance with
        """
        self._classifier.avg = avg
    
    def _learn_with_classifier(self, data, coded_labels):
        """ Carries out a learning step for the underlying online classification ML model
        
        Args:
            data: a (samples_nb; features_nb)-shaped scipy sparse matrix or numpy array, 
                corresponding to the vectorized samples
            coded_labels: a (samples_nb,)-shaped numpy array, corresponding to the class label 
                associated to each sample
        """
        self._classifier.train_mode = True
        self._classifier.learn(data, coded_labels)
        self._classifier.train_mode = False
