# -*- coding: utf-8 -*-

"""
Defines utilities to create a 'cortex.tools.ml_features.SampleVectorizer' instance that can be used to 
vectorize AnaphoricitySample instances
"""

__all__ = ["_create_anaphoricity_mention_sample_feature_vectorizer",
           ]

import itertools
from collections import OrderedDict

from .hierarchy.mentions import _MentionHierarchy
from cortex.tools.ml_features import (CategoricalFeature, create_features_product, SampleVectorizer, 
                                      create_product_features_from_groups_definition)

from cortex.config import language
from cortex.languages import create_get_language_dependent_object_fct
get_language_dependent_object = create_get_language_dependent_object_fct(language)

prepare_features_creation_fct = get_language_dependent_object("mention_anaphoricity_features_prepare_creation_fct")

def _create_anaphoricity_mention_sample_feature_vectorizer(mention_hierarchy, quantize, strict=False):
    """ Creates a SampleVectoriser instance, using utilities defined for the current working languages.
    The created sample vectorizer allows to encode AnaphoricitySample instances into a feature space.
    
    Args:
        mention_hierarchy: a _MentionHierarchy child class instance; used to potentially extend 
            the feature space dedicated to encode single mention based samples, by concatenating the same 
            feature subspace several time, in function of categorical values that are defined on single 
            mentions, and computed using the mention hierarchy object. 
            Cf cortex.learning.ml_features.hierarchy.
        quantize: boolean; whether or not quantize some features that can be quantized, when 
            defining the SampleVectorizer instance
        strict: boolean; to be passed to CategoricalFeature instances when initializing them.
            Cf the documentation of the CategoricalFeature class

    Returns:
        a SampleVectorizer instance
    """
    
    # Check input
    if not isinstance(mention_hierarchy, _MentionHierarchy):
        msg = "Wrong class '{}', must be an instance of child class of _MentionHierarchy"
        msg = msg.format(type(mention_hierarchy))
        raise TypeError(msg)
    
    # Fetch needed data to create the features
    create_functions_collection, features_groups_features_names = prepare_features_creation_fct()
    
    # Initialize data
    feature_name2feature = OrderedDict()
    group_name2feature_names = {}
    
    # Create features
    for create_features_fct in create_functions_collection:
        create_features_fct(feature_name2feature, group_name2feature_names, quantize=quantize, strict=strict)
    
    # Carry out group products
    features = tuple(feature_name2feature.values())
    features_groups = []
    for group_name1, group_name2 in features_groups_features_names:
        features_groups.append((group_name2feature_names[group_name1], group_name2feature_names[group_name2]))
    
    # Create product features
    group_features = create_product_features_from_groups_definition(features_groups, features, feature_name2feature=feature_name2feature)
    
    # Join features and group product features
    features = tuple(itertools.chain(features, group_features))
    
    # Create more feature if needed because of node id instance
    if mention_hierarchy.POSSIBLE_VALUES:
        current_features = features
        new_features = []
        
        def _create_compute_fct(ident):
            return lambda anaphoricity_sample: mention_hierarchy.node_id(anaphoricity_sample.mention)[ident]
        compute_fcts = tuple(map(_create_compute_fct, range(len(mention_hierarchy.POSSIBLE_VALUES))))
        
        for i, (compute_fct, possible_values_) in enumerate(zip(compute_fcts, mention_hierarchy.POSSIBLE_VALUES)):
            name = "'{}'_node_id_fct_n°{}".format(type(mention_hierarchy).__name__, i)
            node_id_feature = CategoricalFeature(name, compute_fct, possible_values_)
            new_features.extend(create_features_product(node_id_feature, feature) for feature in current_features)
        features = new_features
    
    # Create the sample vectorizer
    sample_vectorizer = SampleVectorizer(features)
    
    return sample_vectorizer
