# -*- coding: utf-8 -*-

"""
Defines utilities to create a 'cortex.tools.ml_features.Vectorizer' instance that can be used to 
vectorize Sample child classes instances
"""

"""
Available subpackage(s)
----------------------
hierarchy
    Defines utilities to create a hierarchy between instances of objects, which can then be used to 
    introduce a hierarchy within the vectorization of samples based on those objects 
"""

"""
Available submodule(s)
----------------------
singleton_mention_sample_features_vectorizer
    Defines utilities to create a 'cortex.tools.ml_features.Vectorizer' instance that can be used to 
    vectorize SingletonSample instances

anaphoricity_mention_sample_features_vectorizer
    Defines utilities to create a 'cortex.tools.ml_features.Vectorizer' instance that can be used to 
    vectorize AnaphoricitySample instances

mention_pair_sample_features_vectorizer
    Defines utilities to create a 'cortex.tools.ml_features.Vectorizer' instance that can be used to 
    vectorize MentionPairSample instances

extended_mention_pair_sample_features_vectorizer
    Defines utilities to create a 'cortex.tools.ml_features.Vectorizer' instance that can be used to 
    vectorize ExtendedMentionPairSample instances

super_extended_mention_pair_sample_features_vectorizer
    Defines utilities to create a 'cortex.tools.ml_features.Vectorizer' instance that can be used to 
    vectorize ExtendedMentionPairSample instances in another way
"""