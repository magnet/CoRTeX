# -*- coding: utf-8 -*-

"""
Defines utilities to create a 'cortex.tools.ml_features.Vectorizer' instance that can be used to 
vectorize ExtendedMentionPairSample instances
"""

__all__ = ["_create_extended_mention_pair_sample_feature_vectorizer",
           ]

import numpy as np
from scipy import sparse

from .anaphoricity_mention_sample_features_vectorizer import _create_anaphoricity_mention_sample_feature_vectorizer
from .mention_pair_sample_features_vectorizer import _create_mention_pair_sample_feature_vectorizer
from cortex.tools.ml_features import MultiNumericFeature, SampleVectorizer


def _create_extended_mention_pair_sample_feature_vectorizer(pair_hierarchy, mention_hierarchy, 
                                                            with_singleton_features, with_anaphoricity_features, 
                                                            quantize, strict):
    """ Creates a SampleVectoriser instance, using utilities defined for the current working languages.
    
    The created sample vectorizer allows to encode into a feature space MentionPairSample instances 
    whose first mention can be either a true Mention instance, or the NULL_MENTION. In order to do 
    this, a SampleVectorizer dedicated to encode data about a single mention into a feature space for 
    anaphoricity detection is used to encode data about the second mention of the pair in the case 
    that the first mention of the pair is the NULL_MENTION.
    
    Args:
        pair_hierarchy: a _PairHierarchy child class instance, or None; used to potentially extend 
            the feature space by concatenating the same feature space several time, in function of 
            categorical values that are defined on pairs of mentions, and computed using the hierarchy object. 
            Cf cortex.learning.ml_features.hierarchy.
        mention_hierarchy: a _MentionHierarchy child class instance; used to potentially extend 
            the feature space dedicated to encode single mention based samples, by concatenating the same 
            feature subspace several time, in function of categorical values that are defined on single 
            mentions, and computed using the mention hierarchy object. 
            Cf cortex.learning.ml_features.hierarchy.
        with_singleton_features: boolean; when defining the vectorizer, specifies whether or not 
            to include specific 'singleton' features when vectorizing info related to a pair of true mentions 
        with_anaphoricity_features: boolean; when defining the vectorizer, specifies whether or not 
            to include specific 'anaphoricity' features when vectorizing info related to a pair of true mentions
        quantize: boolean; whether or not quantize some features that can be quantized, when 
            defining the SampleVectorizer instance
        strict: boolean; to be passed to CategoricalFeature instances when initializing them.
            Cf the documentation of the CategoricalFeature class

    Returns:
        a SampleVectorizer instance
    """
    
    mention_pair_sample_vectorizer = _create_mention_pair_sample_feature_vectorizer(pair_hierarchy, 
                                                                              with_singleton_features, 
                                                                              with_anaphoricity_features, 
                                                                              quantize, strict=strict)
    anaphoricity_sample_vectorizer = _create_anaphoricity_mention_sample_feature_vectorizer(mention_hierarchy, 
                                                                                      quantize, 
                                                                                      strict=strict)
    
    column_nb1 = mention_pair_sample_vectorizer.numeric_column_nb
    column_nb2 = anaphoricity_sample_vectorizer.numeric_column_nb
    features_nb = column_nb1 + 1 + column_nb2
    column_names1 = mention_pair_sample_vectorizer.numeric_column_names
    column_names2 = anaphoricity_sample_vectorizer.numeric_column_names
    column_names = column_names1 + ("IsLeftNullMention",) + column_names2
    
    name = "features_combination"
    def combine_features_fct(extended_mention_pair_sample, matrix=None, row_id=0, column_offset=0):
        if matrix is None:
            result = sparse.lil_matrix((1,features_nb), dtype=np.int)
            combine_features_fct(extended_mention_pair_sample, matrix=result, row_id=0, column_offset=0)
            return result.tocsr()
        anaphoricity_sample = extended_mention_pair_sample.anaphoricity_sample
        if anaphoricity_sample is None:
            mention_pair_sample_vectorizer.fill_matrix(extended_mention_pair_sample, matrix, 
                                                    row_id=row_id, column_offset=column_offset)
        else:
            matrix[row_id,column_offset+column_nb1] = 1 # Specify that the left mention is the NULL_MENTION
            anaphoricity_sample_vectorizer.fill_matrix(anaphoricity_sample, matrix, row_id=row_id, 
                                                    column_offset=column_offset+column_nb1+1)
        return matrix
    compute_fct = combine_features_fct
    feature = MultiNumericFeature(name, compute_fct, column_names)
    
    # Create the sample vectorizer
    sample_vectorizer = SampleVectorizer((feature,))
    
    return sample_vectorizer
