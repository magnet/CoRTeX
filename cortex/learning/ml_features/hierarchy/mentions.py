# -*- coding: utf-8 -*-'

"""
Defines hierarchy classes that expect Mention instances as inputs
"""

__all__ = ["MentionNoneHierarchy", 
           "MentionGramTypeHierarchy",
           "_MentionHierarchy", 
           ]

import abc

from .base import _Hierarchy
from cortex.parameters.mention_data_tags import (NAME_GRAM_TYPE_TAG, EXPANDED_PRONOUN_GRAM_TYPE_TAG, 
                                                   NOMINAL_GRAM_TYPE_TAG, VERB_GRAM_TYPE_TAG)


class _MentionHierarchy(_Hierarchy):
    """ Hierarchy base class for classes whose instances act on objects that are Mention instances.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        POSSIBLE_VALUES: the collection of legitimate values that can be output by the instance
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    @abc.abstractmethod
    def node_id(self, mention):
        pass


class MentionNoneHierarchy(_MentionHierarchy):
    """ Mention hierarchy class that has no effect when used other structures. That is, when used 
    in order to create a vectorizer, the resulting feature space will be the same as the input 
    feature space.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        POSSIBLE_VALUES: the collection of legitimate values that can be output by the instance
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    @property
    def POSSIBLE_VALUES(self):
        return tuple()
    @POSSIBLE_VALUES.setter
    def POSSIBLE_VALUES(self, value):
        raise AttributeError
    
    def node_id(self, mention):
        """ Return the categorical value associated to the input mention.
        
        Args:
            mention: a Mention instance

        Returns:
            None
        """
        return None


class MentionGramTypeHierarchy(_MentionHierarchy):
    """ Mention hierarchy class whose set of possible values is the set of possible mention gram 
    type values. When used in order to create a vectorizer, the resulting feature space dedicated to 
    encode data about a single mention will be made from the concatenation the original feature space 
    dedicated to this, concatenated as many time as there is possible gram type values, including the 
    'None' value for cases where the gram type could not be found.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        POSSIBLE_VALUES: the collection of legitimate values that can be output by the instance
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    _LABELS = {NAME_GRAM_TYPE_TAG:"0", NOMINAL_GRAM_TYPE_TAG:"1", 
               EXPANDED_PRONOUN_GRAM_TYPE_TAG:"2", VERB_GRAM_TYPE_TAG:"3", None:"4"} # 'None' for NULLMENTION
    
    @property
    def POSSIBLE_VALUES(self):
        return (tuple(sorted(self._LABELS.values())),)
    @POSSIBLE_VALUES.setter
    def POSSIBLE_VALUES(self, value):
        raise AttributeError
    
    def node_id(self, mention):
        """ Returns the categorical value associated to the input mention.
        
        Args:
            mention: a Mention instance

        Returns:
            a tuple containing the single, coded value associated to the gram type attribute value 
            of the input mention 
        """
        coded_gram_type = self._LABELS[mention.gram_type]
        return  (coded_gram_type,)

