# -*- coding: utf-8 -*-'

"""
Defines utilities to create a hierarchy between instances of objects, which can then be used to 
introduce a hierarchy within the vectorization of samples based on those objects
"""

"""
Available submodule(s)
----------------------
base
    Defines base hierarchy class

mentions
    Defines hierarchy classes that expect Mention instances as inputs

pairs
    Defines hierarchy classes that expect MentionPairSample instances as inputs
"""

__all__ = ["hierarchy_factory",
           "MentionNoneHierarchy", "MentionGramTypeHierarchy", 
           "PairNoneHierarchy", "PairGramTypeHierarchy", "OverlapPairGramTypeHierarchy",
           ]

from collections import OrderedDict

from .mentions import MentionNoneHierarchy, MentionGramTypeHierarchy
from .pairs import PairNoneHierarchy, PairGramTypeHierarchy#, OverlapPairGramTypeHierarchy

from cortex.tools import (define_check_item_name_function, define_create_function, 
                          define_generate_factory_configuration_function, 
                          define_create_from_factory_configuration_function,
                          )

hierarchy_names_collection_and_filter_class_pairs = ((("pairnone", PairNoneHierarchy.__name__), PairNoneHierarchy),
                                                     (("pairgramtype", PairGramTypeHierarchy.__name__), PairGramTypeHierarchy),
                                                     #(("overlappairgramtype", OverlapPairGramTypeHierarchy.__name__), OverlapPairGramTypeHierarchy),
                                                     (("mentionnone", MentionNoneHierarchy.__name__), MentionNoneHierarchy),
                                                     (("mentiongramtype", MentionGramTypeHierarchy.__name__), MentionGramTypeHierarchy),
                                                     )
hierarchy_name2hierarchy_class = OrderedDict((name, class_) for names, class_ in hierarchy_names_collection_and_filter_class_pairs for name in names)


# High level functions
_check_hierarchy_name = define_check_item_name_function(hierarchy_name2hierarchy_class, "hierarchy")

create_hierarchy = define_create_function(hierarchy_name2hierarchy_class, "hierarchy")

create_hierarchy_from_factory_configuration =\
 define_create_from_factory_configuration_function(hierarchy_name2hierarchy_class, "hierarchy")
 
generate_hierarchy_factory_configuration =\
 define_generate_factory_configuration_function(hierarchy_name2hierarchy_class, "hierarchy")