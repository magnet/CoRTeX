# -*- coding: utf-8 -*-'

"""
Defines base hierarchy class.
"""

__all__ = ["_Hierarchy",
           ]

import abc

from cortex.utils import EqualityMixIn
from cortex.tools import ConfigurationMixIn

class _Hierarchy(ConfigurationMixIn, EqualityMixIn, metaclass=abc.ABCMeta):
    """ Hierarchy base class
    
    A hierarchy class instance is an object that acts similarly to a categorical feature on a 
    specific kind of objects: it possesses a 'node_id' method that takes such an object as an input, 
    and outputs one value among a set of possibles values.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        POSSIBLE_VALUES: the collection of legitimate values that can be output by the instance
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = ("configuration", )
    
    def __init__(self, configuration=None):
        # Initialize using parent class
        ConfigurationMixIn.__init__(self, configuration=configuration)
    
    @abc.abstractproperty
    def POSSIBLE_VALUES(self):
        return tuple()
    
    @abc.abstractmethod
    def node_id(self, input_):
        """ Returns the categorical value associated to the input value.
        
        Args:
            input_: a python object

        Returns:
            a tuple of strings
        """
        pass
