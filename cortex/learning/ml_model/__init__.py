# -*- coding: utf-8 -*-

"""
Defines classes representing machine learning models that can be trained and then used for prediction
"""

"""
Available subpackage(s)
----------------------
classifier
    Defines classes representing classification machine learning models that can be trained and then 
    used for prediction
"""