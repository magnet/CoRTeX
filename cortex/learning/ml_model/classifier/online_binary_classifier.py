# -*- coding: utf-8 -*-

"""
Define custom classifier classes specifically made for online training
"""

__all__ = ["PerceptronOnlineBinaryClassifier", 
           "PAOnlineBinaryClassifier",
           "CWFullOnlineBinaryClassifier", 
           "CWDiagOnlineBinaryClassifier",
           "AROWFullOnlineBinaryClassifier", 
           "AROWDiagOnlineBinaryClassifier",
           "_BaseLinearOnlineBinaryClassifier", 
           "NotInitializedModelError", 
           ]

import abc
import warnings
import numpy
numpy.seterr(all='warn')
from scipy import sparse
import math
import os
from collections import OrderedDict

from .base import _BinaryClassifier
from cortex.tools import (_create_simple_get_parameter_value_from_configuration, 
                            _create_simple_set_parameter_value_in_configuration)
from cortex.tools.ltqnorm import ltqnorm
from cortex.utils import are_array_equal, CoRTeXException



ITERATION_NB_DEFAULT_VALUE = 5
iteration_nb_attribute_data = ("iteration_nb",
                               (_create_simple_get_parameter_value_from_configuration("iteration_nb", 
                                                                                      default=ITERATION_NB_DEFAULT_VALUE, 
                                                                                      postprocess_fct=int),
                                _create_simple_set_parameter_value_in_configuration("iteration_nb"))
                               )
class _OnlineBinaryClassifier(_BinaryClassifier):
    """ Base class for classes whose instances act as online binary classification ML models.
    
    So as to be compatible with some use cases in CoRTeX' resolver class instances that use such 
    models, it is for now necessary to specify the role of the two class labels; that is, it is 
    necessary to explicitly say which class label is associated to the "positive class", and which 
    class label is associated to the "negative class". 
    
    When carrying out a prediction for a given sample, a positive score corresponds to prediction 
    the positive class, and a negative score corresponds to predicting the negative class.
    
    While it is possible to carry out online learning, it is just as well possible to carry out batch 
    learning, by supplying all at once a matrix of sample vectors. In that case, an 'iteration_nb' 
    parameters allows to specify how many time the model should iterate over this training set. 
    
    Arguments:
        positive_class_label: class label associated to the class which has the role of positive class
        
        negative_class_label: class label associated to the class which has the role of negative class
        
        iteration_nb: the number of iteration to carry out over a specified training set when 
            deciding to carry out batch training
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SCORES_ARE_PROBA: boolean; whether or not the score values associated to the prediction made 
            by the underlying model should be interpreted as probability values or not
        
        class_labels: collection of unique class labels representing the class nomenclature the model 
            expects to work with
        
        positive_class_label: the value of the 'positive_class_label' parameter used to initialize 
            the model class instance
        
        negative_class_label: the value of the 'negative_class_label' parameter used to initialize 
            the model class instance
        
        iteration_nb: the value of the 'iteration_nb' parameter used to initialize the model class instance
        
        train_mode: boolean; whether the model class instance's expected next action will be related 
            to training, or to carrying out predictions, since the functioning of the model may vary a 
            bit between the two
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    __INIT_PARAMETERS = OrderedDict((iteration_nb_attribute_data,))
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple(__INIT_PARAMETERS.keys()) + ("configuration",)
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = _BinaryClassifier._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION+\
                                            tuple(__INIT_PARAMETERS.keys())
    SCORES_ARE_PROBA = None
    
    def __init__(self, positive_class_label, negative_class_label, 
                 iteration_nb=ITERATION_NB_DEFAULT_VALUE, configuration=None):
        # Check input values
        if iteration_nb < 1:
            iteration_nb = ITERATION_NB_DEFAULT_VALUE
        # Initialize using parent class
        super().__init__(positive_class_label, negative_class_label, configuration=configuration)
        # Set attribute values
        self.iteration_nb = iteration_nb
        self._label2coef = {self.positive_class_label: 1.0, self.negative_class_label: -1.0}
        self._train_mode = None
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        iteration_nb, = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        kwargs["iteration_nb"] = iteration_nb
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, positive_class_label, negative_class_label, 
                             iteration_nb=ITERATION_NB_DEFAULT_VALUE):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Cf the documentation of the class of know which argument(s) to use.
        
        Returns:
            a Mapping instance
        """
        configuration = super().define_configuration(positive_class_label, negative_class_label)
        configuration["iteration_nb"] = iteration_nb
        return configuration
    
    @property
    def class_labels(self):
        return (self.negative_class_label, self.positive_class_label)
    @class_labels.setter
    def class_labels(self, value):
        raise AttributeError
    
    @abc.abstractproperty
    def train_mode(self):
        return self._train_mode
    @train_mode.setter
    def train_mode(self, value):
        raise AttributeError
    
    def _label2sign(self, label):
        """ Fetches the +1 or -1 float sign coefficient associated to either the positive class label 
        or the negative class label respectively, and raise an exception if the input class label is 
        incorrect.
        
        Args:
            label: the class label whose sign coefficient is wanted
        """
        try:
            sign_coef = self._label2coef[label]
        except (KeyError, IndexError):
            msg = "Incorrect label value ('{}'), must be either '{}' (positive class) or '{}' (negative class)."
            msg = msg.format(self.positive_class_label, self.negative_class_label)
            raise ValueError(msg) from None
        return sign_coef
    
    @abc.abstractmethod
    def init_model(self):
        """ Carries out the actions needed (such as setting attributes values) to initialize the model. """
        pass
    
    @abc.abstractmethod
    def learn(self, X, Y):
        """ Trains the model using the input as the training set.
        
        Args:
            X: a (samples_nb; features_nb)-shaped numpy array or scipy.sparse matrix, the training 
                set samples vectors
            y: a (samples_nb,)-shaped numpy array, the training set ground truth annotation
        """
        pass
    
    @abc.abstractmethod
    def update_sum(self, positive_class_encoded_samples, negative_class_encoded_samples, loss):
        """ Updates the model, using vectorized samples associated to the positive class, and to the 
        negative class, as well as a loss value associated to the sum of them weighted by +1 or -1 
        according the whether they are associated to the positive or the negative class.
        
        Args:
            positive_class_encoded_samples: collection of vectors associated to samples associated 
                to the positive class
            negative_class_encoded_samples: collection of vectors associated to samples associated 
                to the negative class
            loss: float (non-nagetive?)
        """
        # TODO: is it necessary for the loss value to be non-negative?
        pass
    
    @abc.abstractmethod
    def classify(self, X):
        """ Predicts class labels corresponding to input data matrix (one row corresponding to one 
        sample).
        
        Args:
            X: a (samples_nb; features_nb)-shaped numpy array or scipy.sparse matrix

        Returns:
            a (samples_nb,)-shaped numpy array containing the class label values associated to 
        each row of the input matrix
        """
        pass
    
    @abc.abstractmethod
    def can_predict(self):
        """ Checks whether or not this model's class instance possesses all the the attribute it needs 
        to carry out a prediction.
        
        Returns:
            a boolean; True if it does, False if it does not
        """
        pass
    
    @abc.abstractmethod
    def save_model(self, folder_path):
        """ Persists as files in a local folder the data corresponding to the the current state of the 
        model, and which is necessary to initialize another model of the same kind so that it be able 
        to carry out predictions. 
        
        Args:
            folder_path: path to the local folder where the data shall be persisted
        """
        pass
    
    @abc.abstractmethod
    def load_model(self, folder_path):
        """ Uses previously locally persisted data to initialize the internal state of the model, so 
        that it be able to carry out predictions. 
        
        Args:
            folder_path: string, path to the local folder from which that data shall be loaded
        """
        pass




AVG_DEFAULT_VALUE = False
avg_attribute_data = ("avg",
                      (_create_simple_get_parameter_value_from_configuration("avg", 
                                                                             default=AVG_DEFAULT_VALUE, 
                                                                             postprocess_fct=bool),
                       _create_simple_set_parameter_value_in_configuration("avg"))
                      )
WARM_START_DEFAULT_VALUE = True
warm_start_attribute_data = ("warm_start",
                             (_create_simple_get_parameter_value_from_configuration("warm_start", 
                                                                                    default=WARM_START_DEFAULT_VALUE, 
                                                                                    postprocess_fct=bool),
                              _create_simple_set_parameter_value_in_configuration("warm_start"))
                             )
class _BaseLinearOnlineBinaryClassifier(_OnlineBinaryClassifier):
    """ Base class for classes whose instances act as linear online binary classification ML models.
    
    So as to be compatible with some use cases in CoRTeX' resolver class instances that use such 
    models, it is for now necessary to specify the role of the two class labels; that is, it is 
    necessary to explicitly say which class label is associated to the "positive class", and which 
    class label is associated to the "negative class". 
    
    When carrying out a prediction for a given sample, a positive score corresponds to prediction 
    the positive class, and a negative score corresponds to predicting the negative class.
    
    While it is possible to carry out online learning, it is just as well possible to carry out batch 
    learning, by supplying all at once a matrix of sample vectors. In that case, an 'iteration_nb' 
    parameters allows to specify how many time the model should iterate over this training set. 
    
    Arguments:
        positive_class_label: class label associated to the class which has the role of positive class
        
        negative_class_label: class label associated to the class which has the role of negative class
        
        iteration_nb: the number of iteration to carry out over a specified training set when 
            deciding to carry out batch training
        
        avg: boolean; whether or not to use an average of all the versions of the updated weights 
            vectors computed over time when performing predictions
        
        warm_start: boolean, whether or not to continue training on potentially existing weights (True), 
            or initializing the model anew, when carrying out a call to the 'learn' method
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SCORES_ARE_PROBA: boolean; whether or not the score values associated to the prediction made 
            by the underlying model should be interpreted as probability values or not
        
        class_labels: collection of unique class labels representing the class nomenclature the model 
            expects to work with
        
        positive_class_label: the value of the 'positive_class_label' parameter used to initialize 
            the model class instance
        
        negative_class_label: the value of the 'negative_class_label' parameter used to initialize 
            the model class instance
        
        iteration_nb: the value of the 'iteration_nb' parameter used to initialize the model class instance
        
        train_mode: boolean; whether the model class instance's expected next action will be related 
            to training, or to carrying out predictions, since the functioning of the model may vary a 
            bit between the two. For this class' child class instances, it specifies whether the 'average' 
            update weight vector shall be used (if we want to carry out a prediction, and if we allowed 
            for it when initializing the model), or the last updated version of the weight vector; 
            whereas, during a training phase, it is always the the last updated version of the weight vector 
            that is being used
        
        avg: boolean; whether or not to use an average of all the versions of the updated weights 
            vectors computed over time when performing predictions
        
        warm_start: the value of the 'warm_start' parameter used to initialize the model class instance
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    # TODO: check whether or not a 'train_mode' is really necessary
    
    __INIT_PARAMETERS = OrderedDict((avg_attribute_data, warm_start_attribute_data))
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple(__INIT_PARAMETERS.keys()) +\
                                                 _OnlineBinaryClassifier._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = _OnlineBinaryClassifier._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION+\
                                            tuple(__INIT_PARAMETERS.keys())
    _MODEL_FILE_NAME = None
    _MODEL_DATA_ATTRIBUTES_NAMES = ("coef_",) # To update if things are added, such as '_cumulative_coef_'
    SCORES_ARE_PROBA = None
    
    def __init__(self, positive_class_label, negative_class_label, iteration_nb=ITERATION_NB_DEFAULT_VALUE, 
                 avg=AVG_DEFAULT_VALUE, warm_start=WARM_START_DEFAULT_VALUE, configuration=None):
        # Check input values
        if positive_class_label == negative_class_label:
            msg = "The input positive class label value is equal to that of the negative class ('{}')"\
                  ", they must be distinct wrt the '==' operation."
            msg = msg.format(positive_class_label)
            raise ValueError(msg)
        # Initialize using parent class
        super().__init__(positive_class_label, negative_class_label, iteration_nb=iteration_nb, 
                         configuration=configuration)
        # Set attribute values
        self.avg = avg # Can be modified when doing structured learning (decoding during training)
        self.warm_start = warm_start
        self._original_avg = avg
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        avg, warm_start = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        kwargs["avg"] = avg
        kwargs["warm_start"] = warm_start
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, positive_class_label, negative_class_label, iteration_nb=ITERATION_NB_DEFAULT_VALUE,
                              avg=AVG_DEFAULT_VALUE, warm_start=WARM_START_DEFAULT_VALUE):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Cf the documentation of the class of know which argument(s) to use.
        
        Returns:
            a Mapping instance
        """
        configuration = super().define_configuration(positive_class_label, negative_class_label)
        configuration["iteration_nb"] = iteration_nb
        configuration["avg"] = avg
        configuration["warm_start"] = warm_start
        return configuration
    
    @property
    def train_mode(self):
        return self._train_mode
    @train_mode.setter
    def train_mode(self, value):
        """ Sets the train mode boolean value.
        
        This has an effect only if the initialization input 'avg' value was True. If it was False, 
        then the 'avg' attribute value is always False. 
        If True, the 'avg' attribute value is changed to False.
        If False, the 'avg' attribute value is changed to True.
        
        Args:
            value: boolean
        """
        value = bool(value)
        self.avg = not value and self._original_avg
        self._train_mode = value
    
    def init_model(self, features_nb): # TODO: change 'features_nb' to 'dimensionality' there and elsewhere
        """ Carries out the actions needed (such as setting attributes values) to initialize the model.
        
        Args:
            features_nb: number of numeric features used to encode a sample into a vector (that is, 
                the dimensionality of the vector space where and sample is vectorized), regarding the vectors 
                to be used by the model
        """
        self.coef_ = numpy.zeros((1,features_nb), dtype=numpy.float)
        self._cumulative_coef_ = numpy.zeros((1, features_nb), dtype=numpy.float)
        
    def learn(self, X, Y):
        """ Trains the model using the inputs as the training set.
        
        Args:
            X: a (samples_nb; features_nb)-shaped numpy array or scipy.sparse matrix, the training 
                set samples vectors
            y: a (samples_nb,)-shaped numpy array, the training set ground truth annotation
        """
        if X.shape[0] != Y.shape[0]:
            msg = "Incorrect input, number of samples ({}) is different from number of class labels ({})."
            msg = msg.format(X.shape[0], Y.shape[0])
            raise ValueError(msg)
        
        if not (self.warm_start and self.can_predict()):
            self.init_model(X.shape[1])
        
        if sparse.issparse(X):
            X = X.todense().A
        for _ in range(self.iteration_nb):
            scores = self._predict_score(X, avg=False)
            predicted_labels = self.decode_score(scores)
            for x, true_label, prediction in zip(X, Y, zip(predicted_labels, scores)):
                self._update(x, true_label, prediction)
    
    def update_sum(self, positive_class_encoded_samples, negative_class_encoded_samples, loss):
        """ Carries out an update of the model using a loss value, associated to a collection of 
        positive class sample vectors, and a collection of negative class sample vectors, as inputs.
        The model must have been initialized first. At least one of the two collection must be non-empty
        Each collection must be a collection of vectors, either as numpy 1D array of '(features_nb,)' 
        shape, or as scipy.sparse matrix of (1,features_nb) shape.
        
        Args:
        :param positive_class_encoded_samples:
        :param negative_class_encoded_samples:
            loss: non-negative float
        """
        # TODO: is it necessary for the loss value to be non-negative?
        positive_class_coef = self._label2coef[self.positive_class_label]
        negative_clas_coef = self._label2coef[self.negative_class_label]
        x = positive_class_coef*sum(positive_class_encoded_samples,0) + negative_clas_coef*sum(negative_class_encoded_samples,0)
        if sparse.issparse(x):
            x = x.todense().A[0]
        self._update_sum(x, loss)
    
    def predict_class_scores(self, X):
        """ Carries out a prediction step on the input samples data matrix, and output an iterable 
        over collection of scores such that each score in a given collection is associated to the 
        corresponding class label in the collection of class labels associated to this model instance.
        
        Args:
            X: numpy 2D array or scipy.sparse matrix of shape (samples_nb, features_nb)

        Returns:
            a (samples_nb; classes_nb)-shaped numpy array
        """
        positive_class_scores = self._predict_score(X, avg=self.avg).reshape((-1,1))
        negative_class_scores = -positive_class_scores
        scores = numpy.hstack((negative_class_scores, positive_class_scores)) # The same order as the one defined by 'class_labels'
        return scores
    
    def _predict_score(self, X, avg=False):
        """ Carries out a prediction step on the input samples data matrix, and output the 
        corresponding collection of binary classification scores, with one score associated to one 
        sample.
        
        Args:
            X: (samples_nb, features_nb)-shaped numpy 2D array or scipy.sparse matrix
            avg: boolean; whether or not to use the average of the updated weights (True), or the 
                last updated weights (False), when computing the prediction scores

        Returns:
            numpy 1D array containing the binary classification score values corresponding to 
            each sample / each row of the input 2D array-like
        """
        ws = self._get_weights(avg=avg)
        scores = X.dot(ws.T)[:,0]
        return scores
    
    def predict_decision_scores(self, X):
        """ Carry out a prediction step on the input samples data matrix, and outputs the 
        corresponding collection of binary classification scores, with one score associated to one 
        sample.
        
        Args:
            X: numpy 2D array or scipy.sparse matrix of shape (samples_nb, features_nb)

        Returns:
            numpy 1D array containing the score value corresponding to each sample / each row 
            of the input 2D array-like
        """
        return self._predict_score(X, avg=self.avg)
    
    def decode_score(self, scores):
        """ Interprets score into class labels
        
        Args:
            scores: collection of scores (float), one score corresponds to one sample

        Returns:
            a (samples_nb,)-shaped numpy array containing the class label values associated to 
            each score value of the input
        """
        return numpy.array(tuple(self.positive_class_label if score > 0. else self.negative_class_label for score in scores))
    
    def classify(self, X):
        """ Predicts class labels corresponding to input data matrix (one row corresponding to one 
        sample).
        
        Args:
            X: a (samples_nb; features_nb)-shaped numpy array or scipy.sparse matrix

        Returns:
            a (samples_nb,)-shaped numpy array containing the class label values associated to 
            each row of the input matrix
        """
        return self.decode_score(self.predict_decision_scores(X))
    
    def get_weights(self):
        """ Return the model's weights' current state. (the last updated weight vector).
        

        Returns:
            a (weights_nb,)-shaped numpy array; or None, if the model has not been initialized 
        or trained yet
        """
        if not self.can_predict():
            return None
        return self._get_weights(avg=False)

    def get_cumulative_weights(self): #TODO: 'average' or 'cumulative'?
        """ Return the model's averaged weights' current state.
        
        Returns:
            a (weights_nb,)-shaped numpy array; or None, if the model has not been initialized 
            or trained yet
        """
        if not self.can_predict():
            return None
        return self._get_weights(avg=True)
    
    def _get_weights(self, avg=False):
        """ Returnq the model's weights or averaged weights' current state.
        
        Args:
            avg: boolean; whether or not to return the average of the updated weights (True), or 
                the last updated weights (False)

        Returns:
            a (weights_nb,)-shaped numpy array
        
        Raises:
            AttributeError, if the model has not been initialized yet
        """
        try:
            weights = self.coef_
            if avg:
                weights =  self._cumulative_coef_
        except AttributeError:
            msg = "Model's coefficient are missing: has the model instance been initialized?"
            raise NotInitializedModelError(msg) from None
        return weights
    
    def can_predict(self):
        """ Checks whether or not this model's class instance possesses all the the attribute it needs 
        to carry out a prediction.
        
        Returns:
            boolean; True if it does, False if it does not
        """
        for attribute_name in self._MODEL_DATA_ATTRIBUTES_NAMES:
            if not hasattr(self, attribute_name):
                return False
        return True
    
    def save_model(self, folder_path):
        """ Persists as files in a local folder the data corresponding to the the current state of the 
        model, and which is necessary to initialize another model of the same kind so that it be able 
        to carry out the same predictions. 
        
        Args:
            folder_path: string, path to the local folder where the data shall be persisted
        """
        # Save model'data
        if not self.can_predict():
            msg = "Nothing to save for this model, nothing will be saved in the folder '{}'.".format(folder_path)
            raise ValueError(msg) # FIXME: replace this by a warning log?
        model_file_path = os.path.join(folder_path, self._MODEL_FILE_NAME)
        d = {attribute_name: getattr(self, attribute_name) for attribute_name in self._MODEL_DATA_ATTRIBUTES_NAMES}
        with open(model_file_path, "wb") as f:
            numpy.savez(f, **d)
    
    def load_model(self, folder_path):
        """ Uses previously locally persisted data to initialize the internal state of the model, so 
        that it be able to carry out the same predictions as the one made by the model whose data were 
        persisted. 
        
        Args:
            folder_path: path to the local folder from which that data shall be loaded
        
        Raises:
            ValueError: if the data loaded does not allow to initialize the model for prediction
        """
        # Load & parse data
        model_file_path = os.path.join(folder_path, self._MODEL_FILE_NAME)
        with open(model_file_path, "rb") as f:
            # Load data
            npzfiles = numpy.load(f)
            # Parse loaded data
            attribute_name_value_pairs = []
            for attribute_name in self._MODEL_DATA_ATTRIBUTES_NAMES:
                try:
                    value = npzfiles[attribute_name]
                except KeyError:
                    pass
                else:
                    attribute_name_value_pairs.append((attribute_name, value))
        # Check parsed loaded data
        if attribute_name_value_pairs:
            if len(attribute_name_value_pairs) < len(self._MODEL_DATA_ATTRIBUTES_NAMES):
                msg = "Incomplete data has been loaded from '{}': expected the following list of data '{}', only got '{}'"
                msg = msg.format(model_file_path, tuple(self._MODEL_DATA_ATTRIBUTES_NAMES), tuple(attr_name for attr_name, _ in attribute_name_value_pairs))
                raise ValueError(msg)
            for attribute_name, value in attribute_name_value_pairs:
                setattr(self, attribute_name, value)
        else:
            msg = "Tried to load data for a '{}' instance, located at '{}', but the content of "\
                  "the data is empty. This can happen the ml model whose data one wanted to save "\
                  "there had not been trained first."
            msg = msg.format(self.NAME)
            raise ValueError(msg)
            #self.logger.warning(msg)
    
    @abc.abstractmethod
    def _update(self, x, true_label, prediction):
        """ Carries out an update of the online-learning model.
        
        Args:
            x: 1D-shaped numpy array, the vectorized sample for which a prediction was made using 
                the model in its current state
            true_label: the correct label value associated to the sample
            prediction: a (predicted_label, score) pair resulting from carrying out a prediction 
                using the model in its current state
        """
        raise NotImplementedError
    
    @abc.abstractmethod
    def _update_sum(self, x, loss):
        """ Carries out an update of the online-learning model, using directly a provided loss value 
        as input, rather than a comparison between true label and predicted label.
        
        Args: 
            x: 1D-shaped numpy array
            loss: non-negative float
        """
        raise NotImplementedError
        
    def _inner_eq(self, other):
        """ Tests that this ML model class instance is equal to the input object.
        
        Being equal means 
        that:
            - this instance's class is equal or a subclass of the other instance's class
            - all the attributes whose name is present in this '_ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION' 
              class attribute are present for this instance and the other, input instance, and that their 
              values are respectively equal
            - all the attributes whose name is present in this '_MODEL_DATA_ATTRIBUTES_NAMES' class 
              attribute are present for this instance and the other, input instance, and that their values 
              are respectively equal 
        
        Args:
            other: the instance of this ML model class for which to test for equality

        Returns:
            a (boolean, string or None) pair; the second item of the pair will be None if the 
            boolean value is True (all attribute values were found to be equal), or will be the name of 
            the first attribute for which the values were found to not be equal, or 'class' if the object 
            were found not to be equal because the first test for the classes of each instance failed
        """
        result, message = super()._inner_eq(other)
        if not result:
            return result, message
        for attribute_name in self._MODEL_DATA_ATTRIBUTES_NAMES:
            hasattr1 = hasattr(self, attribute_name)
            hasattr2 = hasattr(other, attribute_name)
            if (hasattr1 and not hasattr2) or (not hasattr1 and hasattr2):
                return False, "One of the compared element is lacking the '{}' attribute".format(attribute_name)
            elif hasattr1 and hasattr2:
                if not are_array_equal(getattr(self, attribute_name), getattr(other, attribute_name)):
                    return False, attribute_name
        return True, None


ALPHA_DEFAULT_VALUE = 0.0001
alpha_attribute_data = ("alpha",
                        (_create_simple_get_parameter_value_from_configuration("alpha", 
                                                                               default=ALPHA_DEFAULT_VALUE, 
                                                                               postprocess_fct=float),
                         _create_simple_set_parameter_value_in_configuration("alpha")
                         )
                        )
class PerceptronOnlineBinaryClassifier(_BaseLinearOnlineBinaryClassifier):
    """ Linear classifier with online learning and perceptron update (in primal form).
    
    Perceptron update rule:
    w = w + alpha * (y - y^) * x
    
    Arguments:
        positive_class_label: class label associated to the class which has the role of positive class
        
        negative_class_label: class label associated to the class which has the role of negative class
        
        iteration_nb: the number of iteration to carry out over a specified training set when 
            deciding to carry out batch training
        
        alpha: positive float, learning coefficient used in the perceptron rule
        
        avg: boolean; whether or not to use an average of all the versions of the updated weights 
            vectors computed over time when performing predictions
        
        warm_start: boolean, whether or not to continue training on potentially existing weights (True), 
            or initializing the model anew, when carrying out a call to the 'learn' method
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SCORES_ARE_PROBA: boolean; whether or not the score values associated to the prediction made 
            by the underlying model should be interpreted as probability values or not
        
        class_labels: collection of unique class labels representing the class nomenclature the model 
            expects to work with
        
        positive_class_label: the value of the 'positive_class_label' parameter used to initialize 
            the model class instance
        
        negative_class_label: the value of the 'negative_class_label' parameter used to initialize 
            the model class instance
        
        iteration_nb: the value of the 'iteration_nb' parameter used to initialize the model class instance
        
        train_mode: boolean; whether the model class instance's expected next action will be related 
            to training, or to carrying out predictions, since the functioning of the model may vary a 
            bit between the two. For this class' child class instances, it specifies whether the 'average' 
            update weight vector shall be used (if we want to carry out a prediction, and if we allowed 
            for it when initializing the model), or the last updated version of the weight vector; 
            whereas, during a training phase, it is always the the last updated version of the weight vector 
            that is being used
        
        alpha: positive float, learning coefficient used in the perceptron rule
        
        avg: boolean; whether or not to use an average of all the versions of the updated weights 
            vectors computed over time when performing predictions. Its value is modified by using the 
            'train_mode' attribute.
        
        warm_start: the value of the 'warm_start' parameter used to initialize the model class instance
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    __INIT_PARAMETERS = OrderedDict((alpha_attribute_data,))
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple(__INIT_PARAMETERS.keys()) +\
                                                 _BaseLinearOnlineBinaryClassifier._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = _BaseLinearOnlineBinaryClassifier._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION+\
                                            tuple(__INIT_PARAMETERS.keys())
    _MODEL_DATA_ATTRIBUTES_NAMES = ("_cumulative_coef_",) + _BaseLinearOnlineBinaryClassifier._MODEL_DATA_ATTRIBUTES_NAMES
    _MODEL_FILE_NAME = "perceptron_online_binary_classifier.cortex"
    SCORES_ARE_PROBA = False
    
    def __init__(self, positive_class_label, negative_class_label, iteration_nb=ITERATION_NB_DEFAULT_VALUE, 
                 alpha=ALPHA_DEFAULT_VALUE, avg=AVG_DEFAULT_VALUE, warm_start=WARM_START_DEFAULT_VALUE, 
                 configuration=None):
        # Initialize using parent class
        super().__init__(positive_class_label, negative_class_label, iteration_nb=iteration_nb, 
                         avg=avg, warm_start=warm_start, configuration=configuration)
        # Set attribute values
        self.alpha = alpha
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        alpha, = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        kwargs["alpha"] = alpha
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, positive_class_label, negative_class_label, iteration_nb=ITERATION_NB_DEFAULT_VALUE,
                              alpha=ALPHA_DEFAULT_VALUE, avg=AVG_DEFAULT_VALUE, warm_start=WARM_START_DEFAULT_VALUE):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Cf the documentation of the class of know which argument(s) to use.
        
        Returns:
            a Mapping instance
        """
        configuration = super().define_configuration(positive_class_label, negative_class_label, 
                                                     iteration_nb=iteration_nb, avg=avg, warm_start=warm_start)
        configuration["alpha"] = alpha
        return configuration
    
    def _update(self, x, true_label, prediction):
        """ Updates the model's data by applying the perceptron update rule:
        
        w = w + alpha * (y - y^) * x
        
        Args:
            x: sample vector
            true_label: ground truth class label value associated to the input sample vector
            prediction: (label; score) pair, corresponding to a prediction made by the model for 
                the input sample vector

        Returns:
            float, the loss value resulting from the comparison between ground truth and prediction.
            Here, 0. if ground truth class == predicted class, 1. otherwise
        """
        alpha = self.alpha
        predicted_label, _ = prediction #score
        t_lab = self._label2sign(true_label)
        p_lab = self._label2sign(predicted_label)
        tau = (t_lab - p_lab) / 2
        loss = float(abs(tau))
        self.coef_ += tau * alpha * x
        self._cumulative_coef_ += self.coef_
        return loss
    
    def _update_sum(self, x, loss):
        """ Update the model's data by applying the modified perceptron update rule:
        
        w = w + alpha * 1 * x
        
        Args:
            x: sample vector, directly used to update the weight vector
            loss: non-negative float

        Returns:
            float, the input loss value

        Warning:
            loss value does not play a role here, the parameter exists only for API compatibility reason
        """
        # TODO: is it necessary for the loss value to be non-negative?
        alpha = self.alpha
        tau = 1
        self.coef_ += tau * alpha * x
        self._cumulative_coef_ += self.coef_
        return loss



C_DEFAULT_VALUE = 1.0#numpy.inf # Cannot use numpy.inf in conjunction with configuration saving in file and then loading back
C_attribute_data = ("C",
                    (_create_simple_get_parameter_value_from_configuration("C", default=C_DEFAULT_VALUE, 
                                                                           postprocess_fct=float),
                     _create_simple_set_parameter_value_in_configuration("C"))
                    )
class PAOnlineBinaryClassifier(_BaseLinearOnlineBinaryClassifier):
    """ Passive Aggressive (PA) classifier in primal form.
    
    PA has a margin-based update rule: each update yields at least a margin of one.
    Specifically, we implement PA-I rule for the binary setting.
    See "Online Passive-Aggressive Algorithms", Crammer et. al, 2006.
    
    Arguments:
        positive_class_label: class label associated to the class which has the role of positive class
        
        negative_class_label: class label associated to the class which has the role of negative class
        
        iteration_nb: the number of iteration to carry out over a specified training set when 
            deciding to carry out batch training
        
        C: positive float, aggressiveness value of the model
        
        avg: boolean; whether or not to use an average of all the versions of the updated weights 
            vectors computed over time when performing predictions
        
        warm_start: boolean, whether or not to continue training on potentially existing weights (True), 
            or initializing the model anew, when carrying out a call to the 'learn' method
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SCORES_ARE_PROBA: boolean; whether or not the score values associated to the prediction made 
            by the underlying model should be interpreted as probability values or not
        
        class_labels: collection of unique class labels representing the class nomenclature the model 
            expects to work with
        
        positive_class_label: the value of the 'positive_class_label' parameter used to initialize 
            the model class instance
        
        negative_class_label: the value of the 'negative_class_label' parameter used to initialize 
            the model class instance
        
        iteration_nb: the value of the 'iteration_nb' parameter used to initialize the model class 
            instance
        
        train_mode: boolean; whether the model class instance's expected next action will be related 
            to training, or to carrying out predictions, since the functioning of the model may vary a 
            bit between the two. For this class' child class instances, it specifies whether the 'average' 
            update weight vector shall be used (if we want to carry out a prediction, and if we allowed 
            for it when initializing the model), or the last updated version of the weight vector; 
            whereas, during a training phase, it is always the the last updated version of the weight vector 
            that is being used
        
        C: positive float, aggressiveness value of the model
        
        avg: boolean; whether or not to use an average of all the versions of the updated weights 
            vectors computed over time when performing predictions. Its value is modified by using the 
            'train_mode' attribute.
        
        warm_start: the value of the 'warm_start' parameter used to initialize the model class instance
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    __INIT_PARAMETERS = OrderedDict((C_attribute_data,))
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple(__INIT_PARAMETERS.keys()) +\
                                                 _BaseLinearOnlineBinaryClassifier._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = _BaseLinearOnlineBinaryClassifier._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION+\
                                            tuple(__INIT_PARAMETERS.keys())
    _MODEL_DATA_ATTRIBUTES_NAMES = ("_cumulative_coef_",) + _BaseLinearOnlineBinaryClassifier._MODEL_DATA_ATTRIBUTES_NAMES
    _MODEL_FILE_NAME = "pa_online_binary_classifier.cortex"
    SCORES_ARE_PROBA = False
    
    def __init__(self, positive_class_label, negative_class_label, iteration_nb=ITERATION_NB_DEFAULT_VALUE, 
                 C=C_DEFAULT_VALUE, avg=AVG_DEFAULT_VALUE, warm_start=WARM_START_DEFAULT_VALUE, 
                 configuration=None):
        # Check input value
        if C < 0:
            msg = "Incorrect aggressiveness value '{}', must be a non-negative value".format(C)
            raise ValueError(msg)
        # Initialize using parent class
        super().__init__(positive_class_label, negative_class_label, iteration_nb=iteration_nb, 
                         avg=avg, warm_start=warm_start, configuration=configuration)
        # Set attribute values
        self.C = C # Aggressiveness parameter
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        C, = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        kwargs["C"] = C
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, positive_class_label, negative_class_label, 
                             iteration_nb=ITERATION_NB_DEFAULT_VALUE, C=C_DEFAULT_VALUE, 
                             avg=AVG_DEFAULT_VALUE, warm_start=WARM_START_DEFAULT_VALUE):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Cf the documentation of the class of know which argument(s) to use.
        
        Returns:
            a Mapping instance
        """
        configuration = super().define_configuration(positive_class_label, negative_class_label, 
                                                     iteration_nb=iteration_nb, avg=avg, warm_start=warm_start)
        configuration["C"] = C
        return configuration
    
    def _update(self, x, true_label, prediction):
        """ Updates the model's data by applying the PA-I update rule:
        
        w = w + t * y * x
        where: t = min {C, loss / ||x||**2}
             loss = 0  if margin >= 1.0
               1.0 - margin  o.w.
               margin =  y (w . x)
        
        Args:
            x: sample vector
            true_label: ground truth class label value associated to the input sample vector
            prediction: (label; score) pair, corresponding to a prediction made by the model for 
                the input sample vector

        Returns:
            float, the hinge loss value resulting from the comparison between ground truth and 
            prediction.
        """
        _, score = prediction
        sign_ = self._label2sign(true_label)
        # Compute hinge loss
        margin = sign_ * score
        loss = 1.0 - margin if margin < 1.0 else 0.0
        # Compute tau
        norm_x = numpy.linalg.norm(x)
        #tau = loss / (norm_x**2)
        tau = min(self.C, loss / (norm_x**2)) if norm_x > 0 else self.C# FIXME: must we add a 'max(0, loss)' here? # If norm_x is equal to 0, then x is equal to the null vector, so the update will be null too, so we do not care about the value of tau, in the end
        # Update weights
        self.coef_ += tau * sign_ * x
        self._cumulative_coef_ += self.coef_
        return loss
    
    def _update_sum(self, x, loss):
        """ Updates the model's data by applying the PA-I update rule with user defined loss value:
        
        w = w + t * 1 * x
        where: t = min {C, loss / ||x||**2}
        
        Args:
            x: sample vector, directly used to update the weight vector
            loss: non-negative float

        Returns:
            float, the input loss value

        Warning:
            assume loss value is positive, and that the learning mechanism is based on the 
            minimization toward 0 of this loss value
        """
        sign_ = 1
        # Compute tau
        norm_x = numpy.linalg.norm(x)
        #tau = loss / (norm_x**2) # FIXME: must we add a 'max(0, loss)' here?
        tau = min(self.C, loss / (norm_x**2)) if norm_x > 0 else self.C# FIXME: must we add a 'max(0, loss)' here?
        # Update weights
        self.coef_ += tau * sign_ * x
        self._cumulative_coef_ += self.coef_
        return loss



ETA_DEFAULT_VALUE = 0.8
eta_attribute_data = ("eta",
                      (_create_simple_get_parameter_value_from_configuration("eta", default=ETA_DEFAULT_VALUE, 
                                                                             postprocess_fct=float),
                       _create_simple_set_parameter_value_in_configuration("eta"))
                      )
LINEARIZATION_DEFAULT_VALUE = False
linearization_attribute_data = ("linearization",
                                (_create_simple_get_parameter_value_from_configuration("linearization", 
                                                                                       default=LINEARIZATION_DEFAULT_VALUE, 
                                                                                       postprocess_fct=bool),
                                 _create_simple_set_parameter_value_in_configuration("linearization"))
                                )
class _BaseCWOnlineBinaryClassifier(_BaseLinearOnlineBinaryClassifier):
    """ Base class for Confidence-Weighted algorithms online classifiers.
    
    See "Exact Convex Confidence-Weighted Learning", Crammer et al., 2008.
    
    The paper, notably, introduces an algorithm used to the solve the 'exact convex' optimization 
    problem it refers to, as the 'CW-Stdev algorithm', by opposition to an algorithm introduced by 
    previous work (Dredze et al., cf the paper), which is dubbed here the 'CW-Var algorithm'.
    
    Arguments:
        positive_class_label: class label associated to the class which has the role of positive class
        
        negative_class_label: class label associated to the class which has the role of negative class
        
        iteration_nb: the number of iteration to carry out over a specified training set when 
            deciding to carry out batch training
        
        eta: float comprised between 0 and 1, confidence hyperparameter, that is, the minimum 
            probability that the model makes a correct prediction on a given training sample, during the 
            training phase
        
        linearization: boolean; if True, uses the 'CW-Stdev' algorithm, else uses the 'CW-Var' algorithm
        
        warm_start: boolean, whether or not to continue training on potentially existing weights (True), 
            or initializing the model anew, when carrying out a call to the 'learn' method
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SCORES_ARE_PROBA: boolean; whether or not the score values associated to the prediction made 
            by the underlying model should be interpreted as probability values or not
        
        class_labels: collection of unique class labels representing the class nomenclature the model 
            expects to work with
        
        positive_class_label: the value of the 'positive_class_label' parameter used to initialize 
            the model class instance
        
        negative_class_label: the value of the 'negative_class_label' parameter used to initialize 
            the model class instance
        
        iteration_nb: the value of the 'iteration_nb' parameter used to initialize the model class 
            instance
        
        train_mode: boolean; whether the model class instance's expected next action will be related 
            to training, or to carrying out predictions, since the functioning of the model may vary a 
            bit between the two. For this class' child class instances, it specifies whether the 'average' 
            update weight vector shall be used (if we want to carry out a prediction, and if we allowed 
            for it when initializing the model), or the last updated version of the weight vector; 
            whereas, during a training phase, it is always the the last updated version of the weight vector 
            that is being used
        
        eta: the value of the 'eta' parameter used to initialize the model class instance
        
        linearization: the value of the 'lineariation' parameter used to initialize the model class 
            instance
        
        warm_start: the value of the 'warm_start' parameter used to initialize the model class instance
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    __INIT_PARAMETERS = OrderedDict((eta_attribute_data, linearization_attribute_data))
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple(__INIT_PARAMETERS.keys()) + ("_phi", "_phi_1", "_phi_2") +\
                                                 _BaseLinearOnlineBinaryClassifier._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = _BaseLinearOnlineBinaryClassifier._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION+\
                                            tuple(k for k in __INIT_PARAMETERS.keys() if k!= "avg")
    _MODEL_DATA_ATTRIBUTES_NAMES = ("_sigma",) + _BaseLinearOnlineBinaryClassifier._MODEL_DATA_ATTRIBUTES_NAMES
    SCORES_ARE_PROBA = False
    
    def __init__(self, positive_class_label, negative_class_label, 
                 iteration_nb=ITERATION_NB_DEFAULT_VALUE, 
                 eta=ETA_DEFAULT_VALUE, linearization=LINEARIZATION_DEFAULT_VALUE, 
                 warm_start=WARM_START_DEFAULT_VALUE, configuration=None):
        # Check inputs
        if not 0.5 <= eta <= 1.0:
            msg = "CWFull error: incorrect parameter 'eta' value ('{}'), must be comprised between 0.5 and 1.0."
            msg = msg.format(eta)
            raise ValueError(eta)
        # Initialize using parent class
        super().__init__(positive_class_label, negative_class_label, iteration_nb=iteration_nb, 
                         avg=False, warm_start=warm_start, configuration=configuration)
        # Set attribute values
        self.linearization = linearization # if False, "change of variable" is used to compute exact constraint
        self.eta = eta # minimum probability of correct classification # TODO: check that value is between 0 and 1!
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
        #
        phi = ltqnorm(self.eta) # Inverse normal c.d.f # FIXME: do we still use this function instead of a potentially already existing one in a common package such as scipy?
        self._phi = phi
        self._phi_1 = 1 + phi*phi*0.5
        self._phi_2 = 1 + phi*phi
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        eta, linearization = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        del kwargs["avg"]
        kwargs["eta"] = eta
        kwargs["linearization"] = linearization
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, positive_class_label, negative_class_label, iteration_nb=ITERATION_NB_DEFAULT_VALUE,
                              eta=ETA_DEFAULT_VALUE, linearization=LINEARIZATION_DEFAULT_VALUE, 
                              warm_start=WARM_START_DEFAULT_VALUE):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Cf the documentation of the class of know which argument(s) to use.
        
        Returns:
            a Mapping instance
        """
        configuration = super().define_configuration(positive_class_label, negative_class_label, 
                                                     iteration_nb=iteration_nb, warm_start=warm_start)
        del configuration["avg"]
        configuration["eta"] = eta
        configuration["linearization"] = linearization
        return configuration
    
    @abc.abstractmethod
    def init_model(self, features_nb):
        """ Carries out the actions needed (such as setting attributes values) to initialize the model.
        
        Args:
            features_nb: number of numeric features used to encode a sample into a vector (that is, 
                the dimensionality of the vector space where and sample is vectorized), regarding the vectors 
                to be used by the model
        """
        self.coef_ = numpy.zeros((1, features_nb), dtype=numpy.float) # W ~ Normal(coef_, sigma)

    def _get_weights(self, avg=False):
        """ Returns the model's weights vector's current state.
        
        The 'avg' parameter is kept only for compatibility reasons, its value does not influence the 
        result.
        
        Args:
            avg: boolean; whether or not to return the average of the updated weights (True), or 
                the last updated weights (False)

        Returns:
            a (weights_nb,)-shaped numpy array
        Raises:
            AttributeError, if the model has not been initialized yet
        """
        return super()._get_weights(avg=False)




class CWFullOnlineBinaryClassifier(_BaseCWOnlineBinaryClassifier):
    """ Confidence-Weighted algorithms online classifier (full covariance matrix version).
    
    See "Exact Convex Confidence-Weighted Learning", Crammer et al., 2008.
    
    The paper, notably, introduces an algorithm used to the solve the 'exact convex' optimization 
    problem it refers to, as the 'CW-Stdev algorithm', by opposition to an algorithm introduced by 
    previous work (Dredze et al., cf the paper), which is dubbed here the 'CW-Var algorithm'.
    
    In this version, a (N,N)-shaped confidence matrix (dense) must be created and updated, where N 
    is the dimensionality of the vector space. It can grow very heavy for large vector space.
    If you encounter issues related to this, try using the 'diagonal approximation of covariance matrix' 
    version instead
    
    Arguments:
        positive_class_label: class label associated to the class which has the role of positive class
        
        negative_class_label: class label associated to the class which has the role of negative class
        
        iteration_nb: the number of iteration to carry out over a specified training set when 
            deciding to carry out batch training
        
        eta: float comprised between 0 and 1, confidence hyperparameter, that is, the minimum 
            probability that the model makes a correct prediction on a given training sample, during the 
            training phase
        
        linearization: boolean; if True, uses the 'CW-Stdev' algorithm, else uses the 'CW-Var' algorithm
        
        warm_start: boolean, whether or not to continue training on potentially existing weights (True), 
            or initializing the model anew, when carrying out a call to the 'learn' method
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SCORES_ARE_PROBA: boolean; whether or not the score values associated to the prediction made 
            by the underlying model should be interpreted as probability values or not
        
        class_labels: collection of unique class labels representing the class nomenclature the model 
            expects to work with
        
        positive_class_label: the value of the 'positive_class_label' parameter used to initialize 
            the model class instance
        
        negative_class_label: the value of the 'negative_class_label' parameter used to initialize 
            the model class instance
        
        iteration_nb: the value of the 'iteration_nb' parameter used to initialize the model class 
            instance
        
        train_mode: boolean; whether the model class instance's expected next action will be related 
            to training, or to carrying out predictions, since the functioning of the model may vary a 
            bit between the two. For this class' child class instances, it specifies whether the 'average' 
            update weight vector shall be used (if we want to carry out a prediction, and if we allowed 
            for it when initializing the model), or the last updated version of the weight vector; 
            whereas, during a training phase, it is always the the last updated version of the weight vector 
            that is being used
        
        eta: the value of the 'eta' parameter used to initialize the model class instance
        
        linearization: the value of the 'lineariation' parameter used to initialize the model class 
            instance
        
        warm_start: the value of the 'warm_start' parameter used to initialize the model class instance
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    _MODEL_FILE_NAME = "cwfull_online_binary_classifier.cortex"
    
    def init_model(self, features_nb):
        """ Carries out the actions needed (such as setting attributes values) to initialize the model.
        
        Args:
            features_nb: number of numeric features used to encode a sample into a vector (that is, 
                the dimensionality of the vector space where and sample is vectorized), regarding the vectors 
                to be used by the model
        """
        super().init_model(features_nb)
        self._sigma = numpy.identity(features_nb)
    
    def _update(self, x, true_label, prediction):
        """ Updates the model's data by applying the model' algorithm.
        
        Args:
            x: sample vector
            true_label: ground truth class label value associated to the input sample vector
            prediction: (label; score) pair, corresponding to a prediction made by the model for 
                the input sample vector

        Returns:
            float, the loss value resulting from the comparison between ground truth and prediction.
            Here, 0. if ground truth class == predicted class, 1. otherwise
        """
        # variables
        phi = self._phi
        phi_1 = self._phi_1
        phi_2 = self._phi_2
        w = self.coef_ # w is in fact E[w] in the Gaussian distribution
        sigma = self._sigma
        with warnings.catch_warnings():
            warnings.filterwarnings('error')
            try:
                # new instance and score
                _, score = prediction #predicted_label
                sign_ = numpy.sign(self._label2coef[true_label])
                margin = sign_ * score
                loss = 1. if margin <= 0. else 0.
                # compute v / m
                v = numpy.dot(numpy.dot(x,sigma), x.T) # v is supposed to be a variance term, a scalar; use x * SIGMA * x^T if if we suppose that x is a (1, features_nb) 2D array
                m = margin # m is supposed to be a scalar
                if self.linearization:
                    alpha = max(0, ( -(1 + 2*phi*m) + math.sqrt((1 + 2*phi*m)**2 - 8*phi*(m - phi*v)) ) / ( 4*phi*v ) )
                    beta = 2*alpha*phi / ( 1 + 2*alpha*phi*v )
                else:
                    alpha = max(0, (-m*phi_1 + math.sqrt(m*m * 0.25*phi**4 + v*phi*phi*phi_2)) / (v*phi_2) )
                    v_plus = ( (-alpha*v*phi + math.sqrt(alpha*alpha * v*v * phi*phi + 4*v)) / 2 )**2
                    beta = alpha*phi / (math.sqrt(v_plus) + v*alpha*phi)
                # Define update vectors
                update_w = alpha * sign_ * numpy.dot(sigma, x)
                update_sigma = beta * numpy.dot(sigma, x.reshape(-1, 1)) * numpy.dot(x, sigma)
            except (Warning, ZeroDivisionError):
                pass
            else:
                # update weights & confidence
                w += update_w
                sigma -= update_sigma
            return loss
    
    def _update_sum(self, x, loss): # TODO: make it so there is no need to have a parameter not used just for API compatibility reasons?
        """ Updates the model's data by applying the model' algorithm.
        
        Args:
            x: sample vector, directly used to update the weight vector
            loss: non-negative float

        Returns:
            float, the input loss value

        Warning:
            loss value does not play a role here, the parameter exists only for API compatibility reason
        """
        # Variables
        phi = self._phi
        phi_1 = self._phi_1
        phi_2 = self._phi_2
        w = self.coef_ # w is in fact E[w] in the Gaussian distribution
        sigma = self._sigma
        # New instance and score
        sign_ = 1
        score = numpy.dot(w, x)
        margin = sign_ * score
        with warnings.catch_warnings():
            warnings.filterwarnings('error')
            try:
                # compute v / m
                v = numpy.dot(numpy.dot(x,sigma), x.T) # v is supposed to be a variance term, a scalar; use x * SIGMA * x^T if if we suppose that x is a (1, features_nb) 2D array
                m = margin # m is supposed to be a scalar
                # compute alpha / beta
                alpha = max(0, (-m*phi_1 + math.sqrt(m*m * 0.25*phi**4 + v*phi*phi*phi_2)) / (v*phi_2) )
                v_plus = ( (-alpha*v*phi + math.sqrt(alpha*alpha * v*v * phi*phi + 4*v)) / 2 )**2
                beta = alpha*phi / math.sqrt(v_plus)
                # Define update vectors
                update_w = alpha * sign_ * numpy.dot(sigma, x)
                update_sigma = numpy.dot(sigma, x.reshape(-1, 1)) * numpy.dot(x, sigma) / (1/beta + v)
            except (Warning, ZeroDivisionError):
                pass
            else:
                # update weights & confidence
                w += update_w
                sigma -= update_sigma
        return loss



class CWDiagOnlineBinaryClassifier(_BaseCWOnlineBinaryClassifier):
    """ Confidence-Weighted algorithm (diagonal approximation of covariance matrix).
    
    See "Exact Convex Confidence-Weighted Learning", Crammer et al., 2008.
    
    The paper, notably, introduces an algorithm used to the solve the 'exact convex' optimization 
    problem it refers to, as the 'CW-Stdev algorithm', by opposition to an algorithm introduced by 
    previous work (Dredze et al., cf the paper), which is dubbed here the 'CW-Var algorithm'.
    
    In this version, the (N,N)-shaped confidence dense matrix, where N is the dimensionality of the 
    vector space, is approximated by a diagonal matrix. This allows to save memory by requiring to 
    be able to keep in memory only N float numbers.
    
    Arguments:
        positive_class_label: class label associated to the class which has the role of positive class
        
        negative_class_label: class label associated to the class which has the role of negative class
        
        iteration_nb: the number of iteration to carry out over a specified training set when 
            deciding to carry out batch training
        
        eta: float comprised between 0 and 1, confidence hyperparameter, that is, the minimum 
            probability that the model makes a correct prediction on a given training sample, during the 
            training phase
        
        linearization: boolean; if True, uses the 'CW-Stdev' algorithm, else uses the 'CW-Var' algorithm
        
        warm_start: boolean, whether or not to continue training on potentially existing weights (True), 
            or initializing the model anew, when carrying out a call to the 'learn' method
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SCORES_ARE_PROBA: boolean; whether or not the score values associated to the prediction made 
            by the underlying model should be interpreted as probability values or not
        
        class_labels: collection of unique class labels representing the class nomenclature the model 
            expects to work with
        
        positive_class_label: the value of the 'positive_class_label' parameter used to initialize 
            the model class instance
        
        negative_class_label: the value of the 'negative_class_label' parameter used to initialize 
            the model class instance
        
        iteration_nb: the value of the 'iteration_nb' parameter used to initialize the model class 
            instance
        
        train_mode: boolean; whether the model class instance's expected next action will be related 
            to training, or to carrying out predictions, since the functioning of the model may vary a 
            bit between the two. For this class' child class instances, it specifies whether the 'average' 
            update weight vector shall be used (if we want to carry out a prediction, and if we allowed 
            for it when initializing the model), or the last updated version of the weight vector; 
            whereas, during a training phase, it is always the the last updated version of the weight vector 
            that is being used
        
        eta: the value of the 'eta' parameter used to initialize the model class instance
        
        linearization: the value of the 'lineariation' parameter used to initialize the model class 
            instance
        
        warm_start: the value of the 'warm_start' parameter used to initialize the model class instance
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    _MODEL_FILE_NAME = "cwdiag_online_binary_classifier.cortex"
    
    def init_model(self, features_nb):
        """ Carries out the actions needed (such as setting attributes values) to initialize the model.
        
        Args:
            features_nb: number of numeric features used to encode a sample into a vector (that is, 
                the dimensionality of the vector space where and sample is vectorized), regarding the vectors 
                to be used by the model
        """
        super().init_model(features_nb)
        self._sigma = numpy.ones((features_nb,), dtype=numpy.float)
    
    def _update(self, x, true_label, prediction):
        """ Updates the model's data by applying the model' algorithm.
        
        Args:
            x: sample vector
            true_label: ground truth class label value associated to the input sample vector
            prediction: (label; score) pair, corresponding to a prediction made by the model for 
                the input sample vector

        Returns:
            float, the loss value resulting from the comparison between ground truth and prediction.
            Here, 0. if ground truth class == predicted class, 1. otherwise
        """
        # Variables
        phi = self._phi
        phi_1 = self._phi_1
        phi_2 = self._phi_2
        w = self.coef_ # w is in fact E[w] in the Gaussian distribution
        sigma = self._sigma
        with warnings.catch_warnings():
            warnings.filterwarnings('error')
            try:
                # new instance and score
                _, score = prediction #predicted_label
                sign_ = self._label2coef[true_label]
                margin = sign_ * score
                loss = 1.0 if margin <= 0.0 else 0.0
                # compute v / m
                v = numpy.dot(x, (sigma*x).T) # v is supposed to be a variance term, a scalar; use x * SIGMA * x^T if if we suppose that x is a (1, features_nb) 2D array
                m = margin # m is supposed to be a scalar
                # compute alpha / beta
                if self.linearization:
                    alpha = max(0, ( -(1 + 2*phi*m) + numpy.sqrt((1 + 2*phi*m)**2 - 8*phi*(m - phi*v)) ) / ( 4*phi*v ) )
                    beta = 2*alpha*phi / ( 1 + 2*alpha*phi*v )
                else:
                    alpha = max(0, (-m*phi_1 + numpy.sqrt(m*m * 0.25*phi**4 + v*phi*phi*phi_2)) / (v*phi_2) )
                    v_plus = ( (-alpha*v*phi + numpy.sqrt(alpha*alpha * v*v * phi*phi + 4*v)) / 2 )**2
                    beta = alpha*phi / (numpy.sqrt(v_plus) + v*alpha*phi)
                # Define update vectors
                sigma_x = sigma * x # vector of size n
                update_w = alpha * sign_ * sigma_x
                update_sigma = beta * (sigma_x * sigma_x)
            except (Warning, ZeroDivisionError):
                pass
            else:
                # update weights & confidence
                w += update_w
                sigma -= update_sigma
        return loss
    
    def _update_sum(self, x, loss):
        """ Updates the model's data by applying the model' algorithm.
        
        Args:
            x: sample vector, directly used to update the weight vector
            loss: non-negative float

        Returns:
            float, the input loss value

        Warning:
            loss value does not play a role here, the parameter exists only for API compatibility reason
        """
        # FIXME: problem if x is null: will cause problem in division...
        # variables
        phi = self._phi
        phi_1 = self._phi_1
        phi_2 = self._phi_2
        w = self.coef_ # w is in fact E[w] in the Gaussian distribution
        sigma = self._sigma
        with warnings.catch_warnings():
            warnings.filterwarnings('error')
            try:
                # new instance and score
                score = numpy.dot(w, x)
                sign_ = 1 # Assume that input is linked to positive class?
                margin = sign_ * score
                # compute v / m
                v = numpy.dot(x, (sigma*x).T) # v is supposed to be a variance term, a scalar; use x * SIGMA * x^T if if we suppose that x is a (1, features_nb) 2D array
                m = margin # m is supposed to be a scalar
                # compute alpha / beta
                alpha = max(0, (-m*phi_1 + numpy.sqrt(m*m * 0.25*phi**4 + v*phi*phi*phi_2)) / (v*phi_2) ) # FIXME: what if 'v' is null?
                v_plus = ( (-alpha*v*phi + numpy.sqrt(alpha*alpha * v*v * phi*phi + 4*v)) / 2 )**2
                beta = alpha*phi / numpy.sqrt(v_plus) # FIXME: what if 'v_plus' is null?
                # Define update vectors
                sigma_x = sigma * x # vector of size n
                update_w = alpha * sign_ * sigma_x
                update_sigma = (sigma_x * sigma_x) / (1/beta + v) # FIXME: what if beta is null <=> What if alpha is null?
            except (Warning, ZeroDivisionError):
                pass
            else:
                # update weights & confidence
                w += update_w
                sigma -= update_sigma
        return loss



R_DEFAULT_VALUE = 1.0 # Must be a positive value
r_attribute_data = ("r",
                    (_create_simple_get_parameter_value_from_configuration("r", default=R_DEFAULT_VALUE, 
                                                                           postprocess_fct=float),
                     _create_simple_set_parameter_value_in_configuration("r"))
                    )
class _BaseAROWOnlineBinaryClassifier(_BaseLinearOnlineBinaryClassifier):
    """ Base class for Adaptive Regularization of Weight Vectors algorithms online classifiers.
    
    See "Adaptive Regularization of Weight Vectors" , Crammer et al., 2009.
    Among the described algorithm's cons 
    are:
        - large margin training
        - confidence weighting
        - handling non-separable data
    
    Arguments:
        positive_class_label: class label associated to the class which has the role of positive class
        
        negative_class_label: class label associated to the class which has the role of negative class
        
        iteration_nb: the number of iteration to carry out over a specified training set when 
            deciding to carry out batch training
        
        r: positive float, tolerance hyperparameter, used to compute penalization terms used in the 
            definition of the optimization problem solved by the model's algorithm: the more important 
            the penalization termes, the more important the contribution of losses in the computation of 
            the quantity to minimize. This, in turns, limit the scope of an update of the model. 
            The penalization terms' respective values are inversely proportional to this tolerance parameter.
            As such, the higher this parameter value is, the broader the scope of updates.
            Cf the original paper. 
        
        warm_start: boolean, whether or not to continue training on potentially existing weights (True), 
            or initializing the model anew, when carrying out a call to the 'learn' method
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SCORES_ARE_PROBA: boolean; whether or not the score values associated to the prediction made 
            by the underlying model should be interpreted as probability values or not
        
        class_labels: collection of unique class labels representing the class nomenclature the model 
            expects to work with
        
        positive_class_label: the value of the 'positive_class_label' parameter used to initialize 
            the model class instance
        
        negative_class_label: the value of the 'negative_class_label' parameter used to initialize 
            the model class instance
        
        iteration_nb: the value of the 'iteration_nb' parameter used to initialize the model class 
            instance
        
        train_mode: boolean; whether the model class instance's expected next action will be related 
            to training, or to carrying out predictions, since the functioning of the model may vary a 
            bit between the two. For this class' child class instances, it specifies whether the 'average' 
            update weight vector shall be used (if we want to carry out a prediction, and if we allowed 
            for it when initializing the model), or the last updated version of the weight vector; 
            whereas, during a training phase, it is always the the last updated version of the weight vector 
            that is being used
        
        r: the value of the 'r' parameter used to initialize the model class instance
        
        warm_start: the value of the 'warm_start' parameter used to initialize the model class instance
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    __INIT_PARAMETERS = OrderedDict((r_attribute_data,))
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple(__INIT_PARAMETERS.keys()) +\
                                                 _BaseLinearOnlineBinaryClassifier._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = _BaseLinearOnlineBinaryClassifier._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION+\
                                            tuple(k for k in __INIT_PARAMETERS.keys() if k!= "avg")
    _MODEL_DATA_ATTRIBUTES_NAMES = ("_sigma",) + _BaseLinearOnlineBinaryClassifier._MODEL_DATA_ATTRIBUTES_NAMES
    
    SCORES_ARE_PROBA = False
    
    def __init__(self, positive_class_label, negative_class_label, iteration_nb=ITERATION_NB_DEFAULT_VALUE, 
                 r=R_DEFAULT_VALUE, warm_start=WARM_START_DEFAULT_VALUE, configuration=None):
        # Initialize using parent class
        super().__init__(positive_class_label, negative_class_label, iteration_nb=iteration_nb, 
                         avg=False, warm_start=warm_start, configuration=configuration)
        # Set attribute values
        self.r = r # hyperparameter for penalization
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        r, = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        del kwargs["avg"]
        kwargs["r"] = r
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, positive_class_label, negative_class_label, 
                             iteration_nb=ITERATION_NB_DEFAULT_VALUE,
                             r=R_DEFAULT_VALUE, warm_start=WARM_START_DEFAULT_VALUE):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Cf the documentation of the class of know which argument(s) to use.
        
        Returns:
            a Mapping instance
        """
        configuration = super().define_configuration(positive_class_label, negative_class_label, 
                                                     iteration_nb=iteration_nb, warm_start=warm_start)
        del configuration["avg"]
        configuration["r"] = r
        return configuration
    
    @abc.abstractmethod
    def init_model(self, features_nb):
        """ Carries out the actions needed (such as setting attributes values) to initialize the model.
        
        Args:
            features_nb: number of numeric features used to encode a sample into a vector (that is, 
                the dimensionality of the vector space where and sample is vectorized), regarding the vectors 
                to be used by the model
        """
        self.coef_ = numpy.zeros((1, features_nb), dtype=numpy.float) # W ~ Normal(coef_, sigma)

    def _get_weights(self, avg=False):
        """ Returns the model's weights vector's current state.
        
        The 'avg' parameter is kept only for compatibility reasons, its value does not influence the 
        result.
        
        Args:
            avg: boolean; whether or not to return the average of the updated weights (True), or 
                the last updated weights (False)

        Returns:
            a (weights_nb,)-shaped numpy array
        
        Raises:
            AttributeError, if the model has not been initialized yet
        """
        return super()._get_weights(avg=False)
    
    
    
class AROWFullOnlineBinaryClassifier(_BaseAROWOnlineBinaryClassifier):
    """ Adaptive Regularization of Weight Vectors algorithms online classifiers (Full matrix version).
    
    See "Adaptive Regularization of Weight Vectors" , Crammer et al., 2009.
    Among the described algorithm's cons 
    are:
        - large margin training
        - confidence weighting
        - handling non-separable data
    
    In this version, a (N,N)-shaped confidence matrix (dense) must be created and updated, where N 
    is the dimensionality of the vector space. It can grow very heavy for large vector space.
    If you encounter issues related to this, try using the 'diagonal matrix approximation' 
    version instead
    
    Arguments:
        positive_class_label: class label associated to the class which has the role of positive class
        
        negative_class_label: class label associated to the class which has the role of negative class
        
        iteration_nb: the number of iteration to carry out over a specified training set when 
            deciding to carry out batch training
        
        r: positive float, tolerance hyperparameter, used to compute penalization terms used in the 
            definition of the optimization problem solved by the model's algorithm: the more important 
            the penalization termes, the more important the contribution of losses in the computation of 
            the quantity to minimize. This, in turns, limit the scope of an update of the model. 
            The penalization terms' respective values are inversely proportional to this tolerance parameter.
            As such, the higher this parameter value is, the broader the scope of updates.
            Cf the original paper. 
        
        warm_start: boolean, whether or not to continue training on potentially existing weights (True), 
            or initializing the model anew, when carrying out a call to the 'learn' method
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SCORES_ARE_PROBA: boolean; whether or not the score values associated to the prediction made 
            by the underlying model should be interpreted as probability values or not
        
        class_labels: collection of unique class labels representing the class nomenclature the model 
            expects to work with
        
        positive_class_label: the value of the 'positive_class_label' parameter used to initialize 
            the model class instance
        
        negative_class_label: the value of the 'negative_class_label' parameter used to initialize 
            the model class instance
        
        iteration_nb: the value of the 'iteration_nb' parameter used to initialize the model class 
            instance
        
        train_mode: boolean; whether the model class instance's expected next action will be related 
            to training, or to carrying out predictions, since the functioning of the model may vary a 
            bit between the two. For this class' child class instances, it specifies whether the 'average' 
            update weight vector shall be used (if we want to carry out a prediction, and if we allowed 
            for it when initializing the model), or the last updated version of the weight vector; 
            whereas, during a training phase, it is always the the last updated version of the weight vector 
            that is being used
        
        r: the value of the 'r' parameter used to initialize the model class instance
        
        warm_start: the value of the 'warm_start' parameter used to initialize the model class instance
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    _MODEL_FILE_NAME = "arowfull_online_binary_classifier.cortex"
    
    def init_model(self, features_nb):
        """ Carries out the actions needed (such as setting attributes values) to initialize the model.
        
        Args:
            features_nb: number of numeric features used to encode a sample into a vector (that is, 
                the dimensionality of the vector space where and sample is vectorized), regarding the vectors 
                to be used by the model
        """
        super().init_model(features_nb)
        self._sigma = numpy.identity(features_nb)
    
    def _update(self, x, true_label, prediction):
        """ Updates the model's data by applying the model' algorithm.
        
        Args:
            x: sample vector
            true_label: ground truth class label value associated to the input sample vector
            prediction: (label; score) pair, corresponding to a prediction made by the model for 
                the input sample vector

        Returns:
            float, the hinge loss value resulting from the comparison between ground truth and 
            prediction.
        """
        # Variables
        r = self.r
        w = self.coef_ # w is in fact E[w] in the Gaussian distribution
        sigma = self._sigma
        # new instance and score
        _, score = prediction #predicted_label
        sign_ = self._label2coef[true_label]
        margin = sign_ * score
        loss = max(0.0, 1.0 - margin)
        # update
        if margin * sign_ < 1.0:
            with warnings.catch_warnings():
                warnings.filterwarnings('error')
                try:
                    # compute alpha / beta
                    v = numpy.dot(numpy.dot(x,sigma), x.T) # v is supposed to be a variance term, a scalar; use x * SIGMA * x^T if if we suppose that x is a (1, features_nb) 2D array
                    beta = 1.0 / (v + r)
                    alpha = loss * beta
                    # Define update vectors
                    update_w = alpha * sign_ * numpy.dot(sigma, x)
                    update_sigma = beta * numpy.dot(sigma, x.reshape(-1, 1)) * numpy.dot(x, sigma)
                except (Warning, ZeroDivisionError):
                    pass
                else:
                    # update weights & confidence
                    w += update_w
                    sigma -= update_sigma
        return loss
    
    def _update_sum(self, x, loss):
        """ Updates the model's data by applying the model' algorithm.
        
        Args:
            x: sample vector, directly used to update the weight vector
            loss: non-negative float

        Returns:
            float, the input loss value

        Warning:
            Assumes that loss if built from sum of scores associated to sample (+ potential offset)?
        """
        # TODO: check that the hypothesis made described in the docstring is relevant to the algorithm implementation
        # Variables
        r = self.r
        w = self.coef_ # w is in fact E[w] in the Gaussian distribution
        sigma = self._sigma
        # new instance and score
        sign_ = 1
        score = numpy.dot(w, x)
        with warnings.catch_warnings():
            warnings.filterwarnings('error')
            try:
                # compute alpha / beta
                v = numpy.dot(numpy.dot(x,sigma), x.T) # v is supposed to be a variance term, a scalar; use x * SIGMA * x^T if if we suppose that x is a (1, features_nb) 2D array
                beta = 1. / r
                alpha = max(0, loss - score) / (r + v) # FIXME: assume that the loss value is computed through a sum of score values?
                # Define update vectors
                update_w = alpha * sign_ * numpy.dot(sigma, x)
                update_sigma = numpy.dot(sigma, x.reshape(-1, 1)) * numpy.dot(x, sigma) / (1/beta + v)
            except (Warning, ZeroDivisionError):
                pass
            else:
                # update weights & confidence
                w += update_w
                sigma -= update_sigma
        return loss



class AROWDiagOnlineBinaryClassifier(_BaseAROWOnlineBinaryClassifier):
    """ Adaptives Regularization of Weight Vectors algorithms online classifiers (Diagonal matrix approximation version).
    
    See "Adaptive Regularization of Weight Vectors" , Crammer et al., 2009.
    Among the described algorithm's cons 
    are:
        - large margin training
        - confidence weighting
        - handling non-separable data
    
    In this version, the (N,N)-shaped confidence dense matrix, where N is the dimensionality of the 
    vector space, is approximated by a diagonal matrix. This allows to save memory by requiring to 
    be able to keep in memory only N float numbers.
    
    Arguments:
        positive_class_label: class label associated to the class which has the role of positive class
        
        negative_class_label: class label associated to the class which has the role of negative class
        
        iteration_nb: the number of iteration to carry out over a specified training set when 
            deciding to carry out batch training
        
        r: positive float, tolerance hyperparameter, used to compute penalization terms used in the 
            definition of the optimization problem solved by the model's algorithm: the more important 
            the penalization termes, the more important the contribution of losses in the computation of 
            the quantity to minimize. This, in turns, limit the scope of an update of the model. 
            The penalization terms' respective values are inversely proportional to this tolerance parameter.
            As such, the higher this parameter value is, the broader the scope of updates.
            Cf the original paper.
        
        warm_start: boolean, whether or not to continue training on potentially existing weights (True), 
            or initializing the model anew, when carrying out a call to the 'learn' method
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SCORES_ARE_PROBA: boolean; whether or not the score values associated to the prediction made 
            by the underlying model should be interpreted as probability values or not
        
        class_labels: collection of unique class labels representing the class nomenclature the model 
            expects to work with
        
        positive_class_label: the value of the 'positive_class_label' parameter used to initialize 
            the model class instance
        
        negative_class_label: the value of the 'negative_class_label' parameter used to initialize 
            the model class instance
        
        iteration_nb: the value of the 'iteration_nb' parameter used to initialize the model class 
            instance
        
        train_mode: boolean; whether the model class instance's expected next action will be related 
            to training, or to carrying out predictions, since the functioning of the model may vary a 
            bit between the two. For this class' child class instances, it specifies whether the 'average' 
            update weight vector shall be used (if we want to carry out a prediction, and if we allowed 
            for it when initializing the model), or the last updated version of the weight vector; 
            whereas, during a training phase, it is always the the last updated version of the weight vector 
            that is being used
        
        r: the value of the 'r' parameter used to initialize the model class instance
        
        warm_start: the value of the 'warm_start' parameter used to initialize the model class instance
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    _MODEL_FILE_NAME = "arowdiag_online_binary_classifier.cortex"
    
    def init_model(self, features_nb):
        """ Carries out the actions needed (such as setting attributes values) to initialize the model.
        
        Args:
            features_nb: number of numeric features used to encode a sample into a vector (that is, 
                the dimensionality of the vector space where and sample is vectorized), regarding the vectors 
                to be used by the model
        """
        super().init_model(features_nb)
        self._sigma = numpy.ones((features_nb,), dtype=numpy.float)
    
    def _update(self, x, true_label, prediction):
        """ Updates the model's data by applying the model' algorithm.
        
        Args:
            x: sample vector
            true_label: ground truth class label value associated to the input sample vector
            prediction: (label; score) pair, corresponding to a prediction made by the model for 
                the input sample vector

        Returns:
            float, the hinge loss value resulting from the comparison between ground truth and 
            prediction.
        """
        # Variables
        r = self.r
        w = self.coef_ # w is in fact E[w] in the Gaussian distribution
        sigma = self._sigma
        # new instance and score
        _, score = prediction #predicted_label
        t_lab = self._label2coef[true_label]
        margin = t_lab * score
        loss = max(0., 1.-margin)
        # update
        if margin * t_lab < 1:
            with warnings.catch_warnings():
                warnings.filterwarnings('error')
                try:
                    # compute alpha / beta
                    v = numpy.dot(x, (sigma*x).T) # v is supposed to be a variance term, a scalar; use x * SIGMA * x^T if if we suppose that x is a (1, features_nb) 2D array
                    beta = 1. / (v + r)
                    alpha = loss * beta
                    # Define update vectors
                    sigmax = sigma * x # vector of size n
                    update_w = alpha * t_lab * (sigma * x)
                    update_sigma = beta * (sigmax * sigmax)
                except (Warning, ZeroDivisionError):
                    pass
                else:
                    # update weights & confidence
                    w += update_w
                    sigma -= update_sigma
        return loss
    
    def _update_sum(self, x, loss):
        """ Updates the model's data by applying the model' algorithm.
        
        Args:
            x: sample vector, directly used to update the weight vector
            loss: non-negative float

        Returns:
            float, the input loss value

        Warning:
            Assumes that loss if built from sum of scores associated to sample (+ potential offset)?
        """
        # Variables
        r = self.r
        w = self.coef_ # w is in fact E[w] in the Gaussian distribution
        sigma = self._sigma
        # new instance and score
        sign_ = 1
        score = numpy.dot(w, x)
        with warnings.catch_warnings():
            warnings.filterwarnings('error')
            try:
                # compute alpha / beta
                v = numpy.dot(x, (sigma*x).T) # v is supposed to be a variance term, a scalar; use x * SIGMA * x^T if if we suppose that x is a (1, features_nb) 2D array
                beta = 1. / r
                alpha = max(0, loss - score) / (r + v) # FIXME: assume that the loss value is computed through a sum of score values?
                # Define update vectors
                sigmax = sigma * x # vector of size n
                update_w = alpha * sign_ * sigmax
                update_sigma = (sigmax * sigmax) / (1/beta + v)
            except (Warning, ZeroDivisionError):
                pass
            else:
                # update weights & confidence
                w += update_w
                sigma -= update_sigma
        return loss



classifier_names_collection_and_classifier_class_pairs = ((("perceptrononlinebinary", PerceptronOnlineBinaryClassifier.__name__), PerceptronOnlineBinaryClassifier),
                                                          (("paonlinebinary", PAOnlineBinaryClassifier.__name__), PAOnlineBinaryClassifier),
                                                          (("cwfullonlinebinary", CWFullOnlineBinaryClassifier.__name__), CWFullOnlineBinaryClassifier),
                                                          (("cwdiagonlinebinary", CWDiagOnlineBinaryClassifier.__name__), CWDiagOnlineBinaryClassifier),
                                                          (("arowfullonlinebinary", AROWFullOnlineBinaryClassifier.__name__), AROWFullOnlineBinaryClassifier),
                                                          (("arowdiagonlinebinary", AROWDiagOnlineBinaryClassifier.__name__), AROWDiagOnlineBinaryClassifier),
                                                          #(("", .__name__), ),
                                                        )
classifier_name2classifier_class = OrderedDict((name, class_) for names, class_ in classifier_names_collection_and_classifier_class_pairs for name in names)


# Exceptions
class NotInitializedModelError(CoRTeXException):
    """ Exception to be raised when failing to fetch a linear model's weights vector. """
    pass
