# -*- coding: utf-8 -*-

"""
Defines classifier classes based upon some of the sklearn python package's classifier classes
"""

__all__ = ["LogisticRegressionSKLEARNBinaryClassifier",
           "PerceptronSKLEARNBinaryClassifier",
           "PASKLEARNBinaryClassifier",
           "SVSKLEARNBinaryClassifier",
           "_SKLEARNLinearBinaryClassifier", 
           "_SKLEARNBinaryClassifier",
           ]

import abc
import os
from collections import OrderedDict
from sklearn.linear_model import (LogisticRegression, #, LogisticRegressionCV
                                  Perceptron, PassiveAggressiveClassifier, 
                                  )
from sklearn.svm import SVC
import numpy

from .base import _BinaryClassifier
from cortex.tools import  _create_attribute_data
from cortex.utils import are_array_equal


labels_attribute_data = _create_attribute_data("labels", postprocess_fct=tuple)
class _SKLEARNBinaryClassifier(_BinaryClassifier):
    """ Base class for SKLEARN's ML model based classifiers.
    
    That is, the child classes inherits from one of sklearn's classifier ML model classes.
    
    So as to be compatible with some use cases in CoRTeX' resolver class instances that use such 
    models, it is for now necessary to specify the role of the two class labels; that is, it is 
    necessary to explicitly say which class label is associated to the "positive class", and which 
    class label is associated to the "negative class". 
    When carrying out a prediction for a given sample, a positive score corresponds to prediction 
    the positive class, and a negative score corresponds to predicting the negative class.
    
    For those classes, all training shall be made by providing all the training data all at once 
    ('batch training').
    
    Arguments:
        positive_class_label: class label associated to the class which has the role of positive class
        
        negative_class_label: class label associated to the class which has the role of negative class
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SCORES_ARE_PROBA: boolean; whether or not the score values associated to the prediction made 
            by the underlying model should be interpreted as probability values or not
        
        class_labels: collection of unique class labels representing the class nomenclature the model 
            expects to work with
        
        positive_class_label: the value of the 'positive_class_label' parameter used to initialize 
            the model class instance
        
        negative_class_label: the value of the 'negative_class_label' parameter used to initialize 
            the model class instance
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    @property
    def class_labels(self):
        return self.classes_
    @class_labels.setter
    def class_labels(self, value):
        raise AttributeError
    
    def predict_decision_function_class_scores(self, data):
        """ Carries out a prediction step on the input samples data matrix, and output an iterable 
        over collection of scores such that each score in a given collection is associated to the 
        corresponding class label in the collection of class labels associated to this model instance.
        
        Args:
            data: numpy 2D array or scipy.sparse matrix of shape (samples_nb, features_nb)

        Returns:
            a (samples_nb, classes_nb)-shaped numpy array
        """
        class_scores = self.decision_function(data)
        if len(self.classes_) == 2:
            second_class_scores = class_scores.reshape((-1,1))
            first_class_scores = -second_class_scores
            class_scores = numpy.hstack((first_class_scores, second_class_scores)) # TODO: check that this is because we will always use '-1' for the negative class, and '1' for the positive class ?
            # Would be easier if if we could check which class the sklearn models associate with which score vector column...
        return class_scores
    
    @abc.abstractmethod
    def can_predict(self):
        """ Checks whether or not this model's class instance possesses all the the attribute it needs 
        to carry out a prediction.
    
        Returns:
            a boolean; True if it does, False if it does not
        """
        pass
    
    def save_model(self, folder_path):
        """ Persists as files in a local folder the data corresponding to the the current state of the 
        model, and which is necessary to initialize another model of the same kind so that it be able 
        to carry out predictions. 
        
        Args:
            folder_path: path to the local folder where the data shall be persisted
        """
        # Save model'data
        if not self.can_predict():
            msg = "Nothing to save for this model, nothing will be saved in the folder '{}'.".format(folder_path)
            raise ValueError(msg) # FIXME: replace this by a warning log?
        model_file_path = os.path.join(folder_path, self._MODEL_FILE_NAME)
        d = {attribute_name: getattr(self, attribute_name) for attribute_name in self._MODEL_DATA_ATTRIBUTES_NAMES}
        with open(model_file_path, "wb") as f:
            numpy.savez(f, **d)
    
    def load_model(self, folder_path):
        """ Uses previously locally persisted data to initialize the internal state of the model, so 
        that it be able to carry out the same predictions as the one made by the model whose data were 
        persisted. 
        
        Args:
            folder_path: a string, path to the local folder from which that data shall be loaded
        
        Raises:
            ValueError: if the data loaded does not allow to initialize the model for prediction
        """
        # Load & parse data
        model_file_path = os.path.join(folder_path, self._MODEL_FILE_NAME)
        with open(model_file_path, "rb") as f:
            # Load data
            npzfiles = numpy.load(f)
            # Parse loaded data
            attribute_name_value_pairs = []
            for attribute_name in self._MODEL_DATA_ATTRIBUTES_NAMES:
                try:
                    value = npzfiles[attribute_name]
                except KeyError:
                    pass
                else:
                    attribute_name_value_pairs.append((attribute_name, value))
        # Check parsed loaded data
        if attribute_name_value_pairs:
            if len(attribute_name_value_pairs) < len(self._MODEL_DATA_ATTRIBUTES_NAMES):
                msg = "Incomplete data has been loaded from '{}': expected the following list of data '{}', only got '{}'."
                msg = msg.format(model_file_path, tuple(self._MODEL_DATA_ATTRIBUTES_NAMES), tuple(attr_name for attr_name, _ in attribute_name_value_pairs))
                raise ValueError(msg)
            for attribute_name, value in attribute_name_value_pairs:
                setattr(self, attribute_name, value)
        else:
            msg = "Tried to load data for a '{}' instance, located at '{}', but the content of "\
                  "the data is empty. This can happen if the ml model whose data one wanted to save "\
                  "there had not been trained first."
            msg = msg.format(self.NAME)
            raise ValueError(msg)
            #self.logger.warning(msg)
    
    def _inner_eq(self, other):
        """ Tests that this ML model class instance is equal to the input object.
        
        Being equal means 
        that:
            - this instance's class is equal or a subclass of the other instance's class
            - all the attributes whose name is present in this '_ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION' 
              class attribute are present for this instance and the other, input instance, and that their 
              values are respectively equal
            - all the attributes whose name is present in this '_MODEL_DATA_ATTRIBUTES_NAMES' class 
              attribute are present for this instance and the other, input instance, and that their values 
              are respectively equal 
        
        Args:
            other: a python object, supposedly the _SKLEARNBinaryClassifier child class instance for 
                which to test for equality

        Returns:
            a (boolean, string or None) pair; the second item of the pair will be None if the 
            boolean value is True (all attribute values were found to be equal), or will be the name of 
            the first attribute for which the values were found to not be equal, or 'class' if the object 
            were found not to be equal because the first test for the classes of each instance failed
        """
        result, message = super()._inner_eq(other)
        if not result:
            return result, message
        for attribute_name in self._MODEL_DATA_ATTRIBUTES_NAMES:
            hasattr1 = hasattr(self, attribute_name)
            hasattr2 = hasattr(other, attribute_name)
            if (hasattr1 and not hasattr2) or (not hasattr1 and hasattr2):
                return False, "One of the compared element is lacking the '{}' attribute".format(attribute_name)
            elif hasattr1 and hasattr2:
                val1 = getattr(self, attribute_name)
                val2 = getattr(other, attribute_name)
                if (val1 is None and val2 is not None) or (val1 is not None and val2 is None):
                    return False, attribute_name
                try:
                    test = are_array_equal(val1, val2)
                except TypeError:
                    test = val1 == val2
                if not test:
                    return False, attribute_name
        return True, None




class _SKLEARNLinearBinaryClassifier(_SKLEARNBinaryClassifier):
    """ Base class for linear SKLEARN's ML model based classifiers.
    
    That is, the child classes inherit from one of sklearn's classifier ML model classes.
    
    So as to be compatible with some use cases in CoRTeX' resolver class instances that use such 
    models, it is for now necessary to specify the role of the two class labels; that is, it is 
    necessary to explicitly say which class label is associated to the "positive class", and which 
    class label is associated to the "negative class". 
    When carrying out a prediction for a given sample, a positive score corresponds to prediction 
    the positive class, and a negative score corresponds to predicting the negative class.
    
    For those classes, all training shall be made by providing all the training data all at once 
    ('batch training').
    
    Arguments:
        positive_class_label: class label associated to the class which has the role of positive class
        
        negative_class_label: class label associated to the class which has the role of negative class
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SCORES_ARE_PROBA: boolean; whether or not the score values associated to the prediction made 
            by the underlying model should be interpreted as probability values or not
        
        class_labels: collection of unique class labels representing the class nomenclature the model 
            expects to work with
        
        positive_class_label: the value of the 'positive_class_label' parameter used to initialize 
            the model class instance
        
        negative_class_label: the value of the 'negative_class_label' parameter used to initialize 
            the model class instance
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    _MODEL_DATA_ATTRIBUTES_NAMES = ("classes_", "coef_", "intercept_", "n_iter_",)
    
    def can_predict(self):
        coef_ = getattr(self, "coef_", None)
        intercept_ = getattr(self, "intercept_", None)
        return coef_ is not None and intercept_ is not None and coef_.shape[0] == intercept_.shape[0]



# Define attribute common to a lot of model classes
FIT_INTERCEPT_DEFAULT_VALUE = True
fit_intercept_attribute_data = _create_attribute_data("fit_intercept", default_value=FIT_INTERCEPT_DEFAULT_VALUE)
CLASS_WEIGHT_DEFAULT_VALUE = None
class_weight_attribute_data = _create_attribute_data("class_weight", default_value=CLASS_WEIGHT_DEFAULT_VALUE) 
RANDOM_STATE_DEFAULT_VALUE = None
random_state_attribute_data = _create_attribute_data("random_state", default_value=RANDOM_STATE_DEFAULT_VALUE)
SHUFFLE_DEFAULT_VALUE = True
shuffle_attribute_data = _create_attribute_data("shuffle", default_value=SHUFFLE_DEFAULT_VALUE)
#WARM_START_DEFAULT_VALUE = False
#warm_start_attribute_data = _create_attribute_data("warm_start", default_value=WARM_START_DEFAULT_VALUE)
#N_JOB_DEFAULT_VALUE = False
#n_job_attribute_data = _create_attribute_data("n_job", default_value=N_JOB_DEFAULT_VALUE)
#VERBOSE_DEFAULT_VALUE = 0
#verbose_attribute_data = _create_attribute_data("verbose", default_value=VERBOSE_DEFAULT_VALUE)





LR_PENALTY_DEFAULT_VALUE = "l2"
lr_penalty_attribute_data = _create_attribute_data("penalty", default_value=LR_PENALTY_DEFAULT_VALUE)
LR_DUAL_DEFAULT_VALUE = False
lr_dual_attribute_data = _create_attribute_data("dual", default_value=LR_DUAL_DEFAULT_VALUE)
LR_TOL_DEFAULT_VALUE = 1e-4
lr_tol_attribute_data = _create_attribute_data("tol", default_value=LR_TOL_DEFAULT_VALUE)
LR_C_DEFAULT_VALUE = 1.0
lr_C_attribute_data = _create_attribute_data("C", default_value=LR_C_DEFAULT_VALUE)
LR_INTERCEPT_SCALING_DEFAULT_VALUE = 1
lr_intercept_scaling_attribute_data = _create_attribute_data("intercept_scaling", default_value=LR_INTERCEPT_SCALING_DEFAULT_VALUE)
LR_SOLVER_DEFAULT_VALUE = "liblinear"
lr_solver_attribute_data = _create_attribute_data("solver", default_value=LR_SOLVER_DEFAULT_VALUE)
LR_MAX_ITER_DEFAULT_VALUE = 100
lr_max_iter_attribute_data = _create_attribute_data("max_iter", default_value=LR_MAX_ITER_DEFAULT_VALUE)
LR_MULTI_CLASS_DEFAULT_VALUE = "ovr"
lr_multi_class_attribute_data = _create_attribute_data("multi_class", default_value=LR_MULTI_CLASS_DEFAULT_VALUE)
class LogisticRegressionSKLEARNBinaryClassifier(_SKLEARNLinearBinaryClassifier, LogisticRegression):
    """ SKLEARN's LogisticRegression based classifier class.
    
    That is, this class inherits from sklearn's LogisticRegression class.
    
    So as to be compatible with some use cases in CoRTeX' resolver class instances that use such 
    models, it is for now necessary to specify the role of the two class labels; that is, it is 
    necessary to explicitly say which class label is associated to the "positive class", and which 
    class label is associated to the "negative class". 
    When carrying out a prediction for a given sample, a positive score corresponds to prediction 
    the positive class, and a negative score corresponds to predicting the negative class.
    
    Training shall be made by providing all the training data all at once ('batch training').
    
    A lot of the possible arguments, as well as the instance's attributes, are the same than those of 
    sklearn's LogisticRegression. Part of its class docstring is copied here for practical purposes.
    
    Arguments:
        positive_class_label: class label associated to the class which has the role of positive class
        
        negative_class_label: class label associated to the class which has the role of negative class
        
        penalty: str, 'l1' or 'l2'
            Used to specify the norm used in the penalization. The 'newton-cg',
            'sag' and 'lbfgs' solvers support only l2 penalties.
    
        dual: boolean
            Dual or primal formulation. Dual formulation is only implemented for
            l2 penalty with liblinear solver. Prefer dual=False when
            n_samples > n_features.
    
        tol: float, default = 1e-4
            Tolerance for stopping criteria.
    
        C: float, default = 1.0
            Inverse of regularization strength; must be a positive float.
            Like in support vector machines, smaller values specify stronger
            regularization.
    
        fit_intercept: boolean, default = True
            Specifies if a constant (a.k.a. bias or intercept) should be
            added to the decision function.
    
        intercept_scaling: float, default 1.
            Useful only when the solver 'liblinear' is used
            and self.fit_intercept is set to True. In this case, x becomes
            [x, self.intercept_scaling],
            i.e. a "synthetic" feature with constant value equal to
            intercept_scaling is appended to the instance vector.
            The intercept becomes ``intercept_scaling * synthetic_feature_weight``.
    
            Note! the synthetic feature weight is subject to l1/l2 regularization
            as all other features.
            To lessen the effect of regularization on synthetic feature weight
            (and therefore on the intercept) intercept_scaling has to be increased.
    
        class_weight: dict or 'balanced', default = None
            Weights associated with classes in the form ``{class_label: weight}``.
            If not given, all classes are supposed to have weight one.
    
            The "balanced" mode uses the values of y to automatically adjust
            weights inversely proportional to class frequencies in the input data
            as ``n_samples / (n_classes * np.bincount(y))``.
    
            Note that these weights will be multiplied with sample_weight (passed
            through the fit method) if sample_weight is specified.
    
        random_state: int, RandomState instance or None, optional, default = None
            The seed of the pseudo random number generator to use when shuffling
            the data.  If int, random_state is the seed used by the random number
            generator; If RandomState instance, random_state is the random number
            generator; If None, the random number generator is the RandomState
            instance used by `np.random`. Used when ``solver`` == 'sag' or
            'liblinear'.
    
        solver: {'newton-cg', 'lbfgs', 'liblinear', 'sag', 'saga'},
            default = 'liblinear'
            Algorithm to use in the optimization problem.
    
            - For small datasets, 'liblinear' is a good choice, whereas 'sag' and 'saga' are faster 
              for large ones.
            - For multiclass problems, only 'newton-cg', 'sag', 'saga' and 'lbfgs'
              handle multinomial loss; 'liblinear' is limited to one-versus-rest
              schemes.
            - 'newton-cg', 'lbfgs' and 'sag' only handle L2 penalty, whereas'liblinear' and 'saga' 
              handle L1 penalty.
    
            Note that 'sag' and 'saga' fast convergence is only guaranteed on
            features with approximately the same scale. You can
            preprocess the data with a scaler from sklearn.preprocessing.
    
        max_iter: int, default = 100
            Useful only for the newton-cg, sag and lbfgs solvers.
            Maximum number of iterations taken for the solvers to converge.
    
        multi_class: str, {'ovr', 'multinomial'}, default = 'ovr'
            Multiclass option can be either 'ovr' or 'multinomial'. If the option
            chosen is 'ovr', then a binary problem is fit for each label. Else
            the loss minimised is the multinomial loss fit across
            the entire probability distribution. Does not work for liblinear
            solver.
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SCORES_ARE_PROBA: boolean; whether or not the score values associated to the prediction made 
            by the underlying model should be interpreted as probability values or not
        
        class_labels: collection of unique class labels representing the class nomenclature the model 
            expects to work with
        
        coef_: array, shape (1, n_features) or (n_classes, n_features)
            Coefficient of the features in the decision function.
    
            `coef_` is of shape (1, n_features) when the given problem
            is binary.
    
        intercept_: array, shape (1,) or (n_classes,)
            Intercept (a.k.a. bias) added to the decision function.
    
            If `fit_intercept` is set to False, the intercept is set to zero.
            `intercept_` is of shape(1,) when the problem is binary.
    
        n_iter_: array, shape (n_classes,) or (1, )
            Actual number of iterations for all classes. If binary or multinomial,
            it returns only 1 element. For liblinear solver, only the maximum
            number of iteration across all classes is given.
        
        positive_class_label: the value of the 'positive_class_label' parameter used to initialize 
            the model class instance
        
        negative_class_label: the value of the 'negative_class_label' parameter used to initialize 
            the model class instance
        
        penalty: the value of the 'penalty' parameter used to initialize the model class instance
    
        dual: the value of the 'dual' parameter used to initialize the model class instance
    
        tol: the value of the 'tol' parameter used to initialize the model class instance
    
        C: the value of the 'C' parameter used to initialize the model class instance
    
        fit_intercept: the value of the 'fit_intercept' parameter used to initialize the model class 
            instance
    
        intercept_scaling: the value of the 'intercept_scaling' parameter used to initialize the 
            model class instance
    
        class_weight: the value of the 'class_weight' parameter used to initialize the model class 
            instance
    
        random_state: the value of the 'random_state' parameter used to initialize the model class 
            instance
    
        solver: the value of the 'solver' parameter used to initialize the model class instance
    
        max_iter: the value of the 'max_iter' parameter used to initialize the model class instance
    
        multi_class: the value of the 'multi_class' parameter used to initialize the model class instance
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    __INIT_PARAMETERS = OrderedDict((lr_penalty_attribute_data, lr_dual_attribute_data, 
                                     lr_tol_attribute_data, lr_C_attribute_data, 
                                     fit_intercept_attribute_data, 
                                     lr_intercept_scaling_attribute_data, class_weight_attribute_data, 
                                     random_state_attribute_data, lr_solver_attribute_data, 
                                     lr_max_iter_attribute_data, lr_multi_class_attribute_data, 
                                     #warm_start_attribute_data, n_job_attribute_data, verbose_attribute_data, 
                                     )
                                    )
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple(__INIT_PARAMETERS.keys()) + ("configuration",)
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = _SKLEARNLinearBinaryClassifier._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION+\
                                            tuple(__INIT_PARAMETERS.keys())
    _MODEL_FILE_NAME = "lr_sklearn_binary_classifier.cortex"
    SCORES_ARE_PROBA = True
    
    def __init__(self, positive_class_label, negative_class_label, 
                 penalty=LR_PENALTY_DEFAULT_VALUE, dual=LR_DUAL_DEFAULT_VALUE, tol=LR_TOL_DEFAULT_VALUE, C=LR_C_DEFAULT_VALUE, 
                 fit_intercept=FIT_INTERCEPT_DEFAULT_VALUE, intercept_scaling=LR_INTERCEPT_SCALING_DEFAULT_VALUE, 
                 class_weight=CLASS_WEIGHT_DEFAULT_VALUE, random_state=RANDOM_STATE_DEFAULT_VALUE, 
                 solver=LR_SOLVER_DEFAULT_VALUE, max_iter=LR_MAX_ITER_DEFAULT_VALUE, multi_class=LR_MULTI_CLASS_DEFAULT_VALUE, 
                 #warm_start=WARM_START_DEFAULT_VALUE, n_jobs=N_JOB_DEFAULT_VALUE, verbose=VERBOSE_DEFAULT_VALUE, 
                 configuration=None):
        # Check input values
        # Initialize using parent class
        _SKLEARNBinaryClassifier.__init__(self, positive_class_label, negative_class_label, configuration=configuration)
        LogisticRegression.__init__(self, penalty=penalty, dual=dual, tol=tol, C=C, 
                                    fit_intercept=fit_intercept, intercept_scaling=intercept_scaling, 
                                    class_weight=class_weight, random_state=random_state, 
                                    solver=solver, max_iter=max_iter, multi_class=multi_class,
                                    #warm_start=warm_start, n_job=n_job, verbose=verbose, 
                                    )
        # Set attribute values
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        (penalty, dual, tol, C, fit_intercept, intercept_scaling, class_weight, random_state, solver, 
         max_iter, multi_class, 
         #warm_start, n_job, verbose
         ) = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        kwargs["penalty"] = penalty
        kwargs["dual"] = dual
        kwargs["tol"] = tol
        kwargs["C"] = C
        kwargs["fit_intercept"] = fit_intercept
        kwargs["intercept_scaling"] = intercept_scaling
        kwargs["class_weight"] = class_weight
        kwargs["random_state"] = random_state
        kwargs["solver"] = solver
        kwargs["max_iter"] = max_iter
        kwargs["multi_class"] = multi_class
        #kwargs["warm_start"] = warm_start
        #kwargs["n_job"] = n_job
        #kwargs["verbose"] = verbose
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, positive_class_label, negative_class_label, 
                              penalty=LR_PENALTY_DEFAULT_VALUE, dual=LR_DUAL_DEFAULT_VALUE, 
                              tol=LR_TOL_DEFAULT_VALUE, C=LR_C_DEFAULT_VALUE, 
                              fit_intercept=FIT_INTERCEPT_DEFAULT_VALUE, 
                              intercept_scaling=LR_INTERCEPT_SCALING_DEFAULT_VALUE, 
                              class_weight=CLASS_WEIGHT_DEFAULT_VALUE, 
                              random_state=RANDOM_STATE_DEFAULT_VALUE, 
                              solver=LR_SOLVER_DEFAULT_VALUE, max_iter=LR_MAX_ITER_DEFAULT_VALUE, 
                              multi_class=LR_MULTI_CLASS_DEFAULT_VALUE, 
                              #warm_start=WARM_START_DEFAULT_VALUE, n_jobs=N_JOB_DEFAULT_VALUE, verbose=VERBOSE_DEFAULT_VALUE,
                              ):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Cf the documentation of the class of know which argument(s) to use.
        
        Returns:
            a Mapping instance
        """
        configuration = super().define_configuration(positive_class_label, negative_class_label)
        configuration["penalty"] = penalty 
        configuration["dual"] = dual 
        configuration["tol"] = tol 
        configuration["C"] = C 
        configuration["fit_intercept"] = fit_intercept 
        configuration["intercept_scaling"] = intercept_scaling 
        configuration["class_weight"] = class_weight
        configuration["random_state"] = random_state 
        configuration["solver"] = solver 
        configuration["max_iter"] = max_iter 
        configuration["multi_class"] = multi_class 
        #configuration["warm_start"] = warm_start 
        #configuration["n_jobs"] = n_jobs 
        #configuration["verbose"] = verbose 
        return configuration
    
    def predict_class_scores(self, data):
        """ Carries out a prediction step on the input samples data matrix, and output an iterable 
        over collection of scores such that each score in a given collection is associated to the 
        corresponding class label in the collection of class labels associated to this model instance.
        
        Args:
            X: numpy 2D array or scipy.sparse matrix of shape (samples_nb, features_nb)

        Returns:
            a (samples_nb; classes_nb)-shaped numpy array
        """
        return self.predict_proba(data)
#LogisticRegressionSKLEARNBinaryClassifier.__doc__ = LogisticRegressionSKLEARNBinaryClassifier.__doc__ + LogisticRegression.__doc__




PERC_PENALTY_DEFAULT_VALUE = None
perc_penalty_attribute_data = _create_attribute_data("penalty", default_value=PERC_PENALTY_DEFAULT_VALUE)
PERC_ALPHA_DEFAULT_VALUE = 0.0001
perc_alpha_attribute_data = _create_attribute_data("alpha", default_value=PERC_ALPHA_DEFAULT_VALUE)
PERC_MAX_ITER_DEFAULT_VALUE = None
perc_max_iter_attribute_data = _create_attribute_data("max_iter", default_value=PERC_MAX_ITER_DEFAULT_VALUE)
PERC_TOL_DEFAULT_VALUE = None
perc_tol_attribute_data = _create_attribute_data("tol", default_value=PERC_TOL_DEFAULT_VALUE)
PERC_ETA0_DEFAULT_VALUE = 1.0
perc_eta0_attribute_data = _create_attribute_data("eta0", default_value=PERC_ETA0_DEFAULT_VALUE)
class PerceptronSKLEARNBinaryClassifier(_SKLEARNLinearBinaryClassifier, Perceptron):
    """ SKLEARN's Perceptron based classifier class.
    
    That is, this class inherits from sklearn's Perceptron class.
    
    So as to be compatible with some use cases in CoRTeX' resolver class instances that use such 
    models, it is for now necessary to specify the role of the two class labels; that is, it is 
    necessary to explicitly say which class label is associated to the "positive class", and which 
    class label is associated to the "negative class". 
    When carrying out a prediction for a given sample, a positive score corresponds to prediction 
    the positive class, and a negative score corresponds to predicting the negative class.
    
    Training shall be made by providing all the training data all at once ('batch training').
    
    A lot of the possible arguments, as well as the instance's attributes, are the same than those of 
    sklearn's Perceptron. Part of its class docstring is copied here for practical purposes.
    
    Arguments:
        positive_class_label: class label associated to the class which has the role of positive class
        
        negative_class_label: class label associated to the class which has the role of negative class
        
        penalty: None, 'l2' or 'l1' or 'elasticnet'
            The penalty (aka regularization term) to be used. Defaults to None.
    
        alpha: float
            Constant that multiplies the regularization term if regularization is
            used. Defaults to 0.0001
    
        max_iter: int, optional
            The maximum number of passes over the training data (aka epochs).
            It only impacts the behavior in the ``fit`` method, and not the
            `partial_fit`.
            Defaults to 5. Defaults to 1000 from 0.21, or if tol is not None.
    
        tol: float or None, optional
            The stopping criterion. If it is not None, the iterations will stop
            when (loss > previous_loss - tol). Defaults to None.
            Defaults to 1e-3 from 0.21.
    
        fit_intercept: boolean
            Whether the intercept should be estimated or not. If False, the
            data is assumed to be already centered. Defaults to True.
    
        eta0: double
            Constant by which the updates are multiplied. Defaults to 1.
    
        random_state: int, RandomState instance or None, optional, default None
            The seed of the pseudo random number generator to use when shuffling
            the data.  If int, random_state is the seed used by the random number
            generator; If RandomState instance, random_state is the random number
            generator; If None, the random number generator is the RandomState
            instance used by `np.random`.
    
        class_weight: dict, {class_label: weight} or "balanced" or None, optional
            Preset for the class_weight fit parameter.
    
            Weights associated with classes. If not given, all classes
            are supposed to have weight one.
    
            The "balanced" mode uses the values of y to automatically adjust
            weights inversely proportional to class frequencies in the input data
            as ``n_samples / (n_classes * np.bincount(y))``
    
        shuffle: boolean, optional, default True
            Whether or not the training data should be shuffled after each epoch.
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SCORES_ARE_PROBA: boolean; whether or not the score values associated to the prediction made 
            by the underlying model should be interpreted as probability values or not
        
        class_labels: collection of unique class labels representing the class nomenclature the model 
            expects to work with
        
        coef_: array, shape = [1, n_features] if n_classes == 2 else [n_classes, n_features]
            Weights assigned to the features.
    
        intercept_: array, shape = [1] if n_classes == 2 else [n_classes]
            Constants in decision function.
    
        n_iter_: int
            The actual number of iterations to reach the stopping criterion.
            For multiclass fits, it is the maximum over every binary fit.
        
        positive_class_label: the value of the 'positive_class_label' parameter used to initialize 
            the model class instance
        
        negative_class_label: the value of the 'negative_class_label' parameter used to initialize 
            the model class instance
        
        penalty: the value of the 'penalty' parameter used to initialize the model class instance
    
        alpha: the value of the 'alpha' parameter used to initialize the model class instance
    
        max_iter: the value of the 'max_iter' parameter used to initialize the model class instance
    
        tol: the value of the 'tol' parameter used to initialize the model class instance
    
        fit_intercept: the value of the 'fit_intercept' parameter used to initialize the model class instance
    
        eta0: the value of the 'eta0' parameter used to initialize the model class instance
    
        random_state: the value of the 'random_state' parameter used to initialize the model class instance
    
        class_weight: the value of the 'class_weight' parameter used to initialize the model class instance
    
        shuffle: the value of the 'shuffle' parameter used to initialize the model class instance
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    __INIT_PARAMETERS = OrderedDict((perc_penalty_attribute_data, perc_alpha_attribute_data, 
                                     perc_max_iter_attribute_data, perc_tol_attribute_data, 
                                     fit_intercept_attribute_data, 
                                     perc_eta0_attribute_data, random_state_attribute_data, 
                                     class_weight_attribute_data, shuffle_attribute_data, 
                                     #warm_start_attribute_data, n_job_attribute_data, verbose_attribute_data, 
                                     )
                                    )
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple(__INIT_PARAMETERS.keys()) + ("configuration",)
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = _SKLEARNLinearBinaryClassifier._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION+\
                                            tuple(__INIT_PARAMETERS.keys())
    _MODEL_FILE_NAME = "perceptron_sklearn_binary_classifier.cortex"
    SCORES_ARE_PROBA = False
    
    def __init__(self, positive_class_label, negative_class_label, 
                 penalty=PERC_PENALTY_DEFAULT_VALUE, alpha=PERC_ALPHA_DEFAULT_VALUE, 
                 max_iter=PERC_MAX_ITER_DEFAULT_VALUE, tol=PERC_TOL_DEFAULT_VALUE, 
                 fit_intercept=FIT_INTERCEPT_DEFAULT_VALUE, 
                 eta0=PERC_ETA0_DEFAULT_VALUE, random_state=RANDOM_STATE_DEFAULT_VALUE, 
                 class_weight=CLASS_WEIGHT_DEFAULT_VALUE, shuffle=SHUFFLE_DEFAULT_VALUE, 
                 #warm_start=WARM_START_DEFAULT_VALUE, n_jobs=N_JOB_DEFAULT_VALUE, verbose=VERBOSE_DEFAULT_VALUE, 
                 configuration=None):
        # Check input values
        # Initialize using parent class
        _SKLEARNBinaryClassifier.__init__(self, positive_class_label, negative_class_label, configuration=configuration)
        Perceptron.__init__(self, penalty=penalty, alpha=alpha, max_iter=max_iter, tol=tol, 
                            fit_intercept=fit_intercept, 
                            eta0=eta0, class_weight=class_weight, random_state=random_state, 
                            shuffle=shuffle, 
                            #warm_start=warm_start, n_job=n_job, verbose=verbose, 
                            )
        # Set attribute values
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        (penalty, alpha, max_iter, tol, fit_intercept, eta0, random_state, class_weight, shuffle, 
         #warm_start, n_job, verbose
         ) = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        kwargs["penalty"] = penalty
        kwargs["alpha"] = alpha
        kwargs["max_iter"] = max_iter
        kwargs["tol"] = tol
        kwargs["fit_intercept"] = fit_intercept
        kwargs["eta0"] = eta0
        kwargs["random_state"] = random_state
        kwargs["class_weight"] = class_weight
        kwargs["shuffle"] = shuffle
        #kwargs["warm_start"] = warm_start
        #kwargs["n_job"] = n_job
        #kwargs["verbose"] = verbose
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, positive_class_label, negative_class_label, 
                              penalty=PERC_PENALTY_DEFAULT_VALUE, alpha=PERC_ALPHA_DEFAULT_VALUE, 
                              max_iter=PERC_MAX_ITER_DEFAULT_VALUE, tol=PERC_TOL_DEFAULT_VALUE, 
                              fit_intercept=FIT_INTERCEPT_DEFAULT_VALUE, 
                              eta0=PERC_ETA0_DEFAULT_VALUE, random_state=RANDOM_STATE_DEFAULT_VALUE, 
                              class_weight=CLASS_WEIGHT_DEFAULT_VALUE, shuffle=SHUFFLE_DEFAULT_VALUE, 
                              #warm_start=WARM_START_DEFAULT_VALUE, n_jobs=N_JOB_DEFAULT_VALUE, verbose=VERBOSE_DEFAULT_VALUE,
                              ):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Cf the documentation of the class of know which argument(s) to use.
        
        Returns:
            a Mapping instance
        """
        configuration = super().define_configuration(positive_class_label, negative_class_label)
        configuration["penalty"] = penalty 
        configuration["alpha"] = alpha 
        configuration["max_iter"] = max_iter 
        configuration["tol"] = tol 
        configuration["fit_intercept"] = fit_intercept 
        configuration["eta0"] = eta0 
        configuration["random_state"] = random_state 
        configuration["class_weight"] = class_weight 
        configuration["shuffle"] = shuffle 
        #configuration["warm_start"] = warm_start 
        #configuration["n_jobs"] = n_jobs 
        #configuration["verbose"] = verbose 
        return configuration
    
    def predict_class_scores(self, data):
        """ Carries out a prediction step on the input samples data matrix, and output an iterable 
        over collection of scores such that each score in a given collection is associated to the 
        corresponding class label in the collection of class labels associated to this model instance.
        
        Args:
            X: numpy 2D array or scipy.sparse matrix of shape (samples_nb, features_nb)

        Returns:
            a (samples_nb; classes_nb)-shaped numpy array
        """
        return self.predict_decision_function_class_scores(data)
#PerceptronSKLEARNBinaryClassifier.__doc__ = PerceptronSKLEARNBinaryClassifier.__doc__ + Perceptron.__doc__



PA_C_DEFAULT_VALUE = 1.0
pa_C_attribute_data = _create_attribute_data("C", default_value=PA_C_DEFAULT_VALUE)
PA_MAX_ITER_DEFAULT_VALUE = 5
pa_max_iter_attribute_data = _create_attribute_data("max_iter", default_value=PA_MAX_ITER_DEFAULT_VALUE)
PA_TOL_DEFAULT_VALUE = None
pa_tol_attribute_data = _create_attribute_data("tol", default_value=PA_TOL_DEFAULT_VALUE)
PA_LOSS_DEFAULT_VALUE = "hinge"
pa_loss_attribute_data = _create_attribute_data("loss", default_value=PA_LOSS_DEFAULT_VALUE)
PA_AVERAGE_DEFAULT_VALUE = False
pa_average_attribute_data = _create_attribute_data("average", default_value=PA_AVERAGE_DEFAULT_VALUE, postprocess_fct=lambda x: x) # Can be either boolean or int, so must provide this postprocess function
class PassiveAggressiveSKLEARNBinaryClassifier(_SKLEARNLinearBinaryClassifier, PassiveAggressiveClassifier):
    """ SKLEARN's PassiveAggressiveClassifier based classifier class.
    
    That is, this class inherits from sklearn's PassiveAggressiveClassifier class.
    
    So as to be compatible with some use cases in CoRTeX' resolver class instances that use such 
    models, it is for now necessary to specify the role of the two class labels; that is, it is 
    necessary to explicitly say which class label is associated to the "positive class", and which 
    class label is associated to the "negative class". 
    When carrying out a prediction for a given sample, a positive score corresponds to prediction 
    the positive class, and a negative score corresponds to predicting the negative class.
    
    Training shall be made by providing all the training data all at once ('batch training').
    
    A lot of the possible arguments, as well as the instance's attributes, are the same than those of 
    sklearn's PassiveAggressiveClassifier. Part of its class docstring is copied here for practical 
    purposes.
    
    Arguments:
        positive_class_label: class label associated to the class which has the role of positive class
        
        negative_class_label: class label associated to the class which has the role of negative class
        
        C: float
            Maximum step size (regularization). Defaults to 1.0.
    
        fit_intercept: boolean, default=False
            Whether the intercept should be estimated or not. If False, the
            data is assumed to be already centered.
    
        max_iter: int, optional
            The maximum number of passes over the training data (aka epochs).
            It only impacts the behavior in the ``fit`` method, and not the
            `partial_fit`.
            Defaults to 5. Defaults to 1000 from 0.21, or if tol is not None.
    
        tol: float or None, optional
            The stopping criterion. If it is not None, the iterations will stop
            when (loss > previous_loss - tol). Defaults to None.
            Defaults to 1e-3 from 0.21.
    
        loss: string, optional
            The loss function to be used:
            hinge: equivalent to PA-I in the reference paper.
            squared_hinge: equivalent to PA-II in the reference paper.
    
        average: boolean or int, optional
            When set to True, computes the averaged SGD weights and stores the
            result in the ``coef_`` attribute. If set to an int greater than 1,
            averaging will begin once the total number of samples seen reaches
            average. So average=10 will begin averaging after seeing 10 samples.
    
        random_state: int, RandomState instance or None, optional, default=None
            The seed of the pseudo random number generator to use when shuffling
            the data.  If int, random_state is the seed used by the random number
            generator; If RandomState instance, random_state is the random number
            generator; If None, the random number generator is the RandomState
            instance used by `np.random`.
    
        class_weight: dict, {class_label: weight} or "balanced" or None, optional
            Preset for the class_weight fit parameter.
    
            Weights associated with classes. If not given, all classes
            are supposed to have weight one.
    
            The "balanced" mode uses the values of y to automatically adjust
            weights inversely proportional to class frequencies in the input data
            as ``n_samples / (n_classes * np.bincount(y))``
    
        shuffle: boolean, default=True
            Whether or not the training data should be shuffled after each epoch.
    
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SCORES_ARE_PROBA: boolean; whether or not the score values associated to the prediction made 
            by the underlying model should be interpreted as probability values or not
        
        class_labels: collection of unique class labels representing the class nomenclature the model 
            expects to work with
        
        coef_: array, shape = [1, n_features] if n_classes == 2 else [n_classes,n_features]
            Weights assigned to the features.
    
        intercept_: array, shape = [1] if n_classes == 2 else [n_classes]
            Constants in decision function.
    
        n_iter_: int
            The actual number of iterations to reach the stopping criterion.
            For multiclass fits, it is the maximum over every binary fit.
        
        positive_class_label: the value of the 'positive_class_label' parameter used to initialize 
            the model class instance
        
        negative_class_label: the value of the 'negative_class_label' parameter used to initialize 
            the model class instance
        
        C: the value of the 'C' parameter used to initialize the model class instance
    
        fit_intercept: the value of the 'fit_intercept' parameter used to initialize the model class instance
    
        max_iter: the value of the 'max_iter' parameter used to initialize the model class instance
    
        tol: the value of the 'tol' parameter used to initialize the model class instance
    
        loss: the value of the 'loss' parameter used to initialize the model class instance
    
        average: the value of the 'average' parameter used to initialize the model class instance
    
        random_state: the value of the 'random_state' parameter used to initialize the model class instance
    
        class_weight: the value of the 'class_weight' parameter used to initialize the model class instance
    
        shuffle: the value of the 'shuffle' parameter used to initialize the model class instance
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    __INIT_PARAMETERS = OrderedDict((pa_C_attribute_data, fit_intercept_attribute_data, 
                                     pa_max_iter_attribute_data, pa_tol_attribute_data, 
                                     pa_loss_attribute_data, pa_average_attribute_data, 
                                     random_state_attribute_data, 
                                     class_weight_attribute_data, shuffle_attribute_data, 
                                     #warm_start_attribute_data, n_job_attribute_data, verbose_attribute_data, 
                                     )
                                    )
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple(__INIT_PARAMETERS.keys()) + ("configuration",)
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = _SKLEARNLinearBinaryClassifier._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION+\
                                            tuple(__INIT_PARAMETERS.keys())
    _MODEL_FILE_NAME = "pa_sklearn_binary_classifier.cortex"
    SCORES_ARE_PROBA = False
    
    def __init__(self, positive_class_label, negative_class_label, 
                 C=PA_C_DEFAULT_VALUE, fit_intercept=FIT_INTERCEPT_DEFAULT_VALUE, 
                 max_iter=PA_MAX_ITER_DEFAULT_VALUE, tol=PA_TOL_DEFAULT_VALUE, 
                 loss=PA_LOSS_DEFAULT_VALUE, average=PA_AVERAGE_DEFAULT_VALUE, 
                 random_state=RANDOM_STATE_DEFAULT_VALUE, 
                 class_weight=CLASS_WEIGHT_DEFAULT_VALUE, shuffle=SHUFFLE_DEFAULT_VALUE, 
                 #warm_start=WARM_START_DEFAULT_VALUE, n_jobs=N_JOB_DEFAULT_VALUE, verbose=VERBOSE_DEFAULT_VALUE, 
                 configuration=None):
        # Check input values
        # Initialize using parent class
        _SKLEARNBinaryClassifier.__init__(self, positive_class_label, negative_class_label, configuration=configuration)
        PassiveAggressiveClassifier.__init__(self, C=C, fit_intercept=fit_intercept, max_iter=max_iter, tol=tol, 
                                             loss=loss, average=average, 
                                             class_weight=class_weight, random_state=random_state, 
                                             shuffle=shuffle, 
                                             #warm_start=warm_start, n_job=n_job, verbose=verbose, 
                                            )
        # Set attribute values
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        (C, fit_intercept, max_iter, tol, loss, average, random_state, class_weight, shuffle, 
         #warm_start, n_job, verbose
         ) = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        kwargs["C"] = C
        kwargs["fit_intercept"] = fit_intercept
        kwargs["max_iter"] = max_iter
        kwargs["tol"] = tol
        kwargs["loss"] = loss
        kwargs["average"] = average
        kwargs["random_state"] = random_state
        kwargs["class_weight"] = class_weight
        kwargs["shuffle"] = shuffle
        #kwargs["warm_start"] = warm_start
        #kwargs["n_job"] = n_job
        #kwargs["verbose"] = verbose
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, positive_class_label, negative_class_label, 
                              C=PA_C_DEFAULT_VALUE, fit_intercept=FIT_INTERCEPT_DEFAULT_VALUE, 
                              max_iter=PA_MAX_ITER_DEFAULT_VALUE, tol=PA_TOL_DEFAULT_VALUE, 
                              loss=PA_LOSS_DEFAULT_VALUE, average=PA_AVERAGE_DEFAULT_VALUE, 
                              random_state=RANDOM_STATE_DEFAULT_VALUE, 
                              class_weight=CLASS_WEIGHT_DEFAULT_VALUE, shuffle=SHUFFLE_DEFAULT_VALUE, 
                              #warm_start=WARM_START_DEFAULT_VALUE, n_jobs=N_JOB_DEFAULT_VALUE, verbose=VERBOSE_DEFAULT_VALUE,
                              ):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Cf the documentation of the class of know which argument(s) to use.
        
        Returns:
            a Mapping instance
        """
        configuration = super().define_configuration(positive_class_label, negative_class_label)
        configuration["C"] = C 
        configuration["fit_intercept"] = fit_intercept 
        configuration["max_iter"] = max_iter 
        configuration["tol"] = tol 
        configuration["loss"] = loss 
        configuration["average"] = average 
        configuration["random_state"] = random_state 
        configuration["class_weight"] = class_weight 
        configuration["shuffle"] = shuffle 
        #configuration["warm_start"] = warm_start 
        #configuration["n_jobs"] = n_jobs 
        #configuration["verbose"] = verbose 
        return configuration
    
    def predict_class_scores(self, data):
        """ Carries out a prediction step on the input samples data matrix, and output an iterable 
        over collection of scores such that each score in a given collection is associated to the 
        corresponding class label in the collection of class labels associated to this model instance.
        
        Args:
            X: numpy 2D array or scipy.sparse matrix of shape (samples_nb, features_nb)

        Returns:
            a (samples_nb; classes_nb)-shaped numpy array
        """
        return self.predict_decision_function_class_scores(data)
#PassiveAggressiveSKLEARNBinaryClassifier.__doc__ = PassiveAggressiveSKLEARNBinaryClassifier.__doc__ + PassiveAggressiveClassifier.__doc__






SV_C_DEFAULT_VALUE = 1.0
sv_C_attribute_data = _create_attribute_data("C", default_value=SV_C_DEFAULT_VALUE)
SV_KERNEL_DEFAULT_VALUE = "rbf"
sv_kernel_attribute_data = _create_attribute_data("kernel", default_value=SV_KERNEL_DEFAULT_VALUE, postprocess_fct=lambda x: x) # Can be a callable
SV_DEGREE_DEFAULT_VALUE = 3
sv_degree_attribute_data = _create_attribute_data("degree", default_value=SV_DEGREE_DEFAULT_VALUE)
SV_GAMMA_DEFAULT_VALUE = "auto"
sv_gamma_attribute_data = _create_attribute_data("gamma", default_value=SV_GAMMA_DEFAULT_VALUE, postprocess_fct=lambda x: x) # Can be a float
SV_COEF0_DEFAULT_VALUE = 0.0
sv_coef0_attribute_data = _create_attribute_data("coef0", default_value=SV_COEF0_DEFAULT_VALUE)
SV_PROBABILITY_DEFAULT_VALUE = False
sv_probability_attribute_data = _create_attribute_data("probability", default_value=SV_PROBABILITY_DEFAULT_VALUE)
SV_SHRINKING_DEFAULT_VALUE = True
sv_shrinking_attribute_data = _create_attribute_data("shrinking", default_value=SV_SHRINKING_DEFAULT_VALUE)
SV_CACHE_SIZE_DEFAULT_VALUE = 200
sv_cache_size_attribute_data = _create_attribute_data("cache_size", default_value=SV_CACHE_SIZE_DEFAULT_VALUE)
SV_MAX_ITER_DEFAULT_VALUE = -1
sv_max_iter_attribute_data = _create_attribute_data("max_iter", default_value=SV_MAX_ITER_DEFAULT_VALUE)
SV_TOL_DEFAULT_VALUE = 1e-3
sv_tol_attribute_data = _create_attribute_data("tol", default_value=SV_TOL_DEFAULT_VALUE)
class SVSKLEARNBinaryClassifier(_SKLEARNBinaryClassifier, SVC):
    """ SKLEARN's PassiveAggressiveClassifier based classifier class.
    
    That is, this class inherits from sklearn's PassiveAggressiveClassifier class.
    
    So as to be compatible with some use cases in CoRTeX' resolver class instances that use such 
    models, it is for now necessary to specify the role of the two class labels; that is, it is 
    necessary to explicitly say which class label is associated to the "positive class", and which 
    class label is associated to the "negative class". 
    When carrying out a prediction for a given sample, a positive score corresponds to prediction 
    the positive class, and a negative score corresponds to predicting the negative class.
    
    Training shall be made by providing all the training data all at once ('batch training').
    
    A lot of the possible arguments, as well as the instance's attributes, are the same than those of 
    sklearn's PassiveAggressiveClassifier. Part of its class docstring is copied here for practical 
    purposes.
    
    Arguments:
        positive_class_label: class label associated to the class which has the role of positive class
        
        negative_class_label: class label associated to the class which has the role of negative class
        
        C: float, optional (default=1.0)
            Penalty parameter C of the error term.
    
        kernel: string, optional (default='rbf')
             Specifies the kernel type to be used in the algorithm.
             It must be one of 'linear', 'poly', 'rbf', 'sigmoid', 'precomputed' or a callable.
             If none is given, 'rbf' will be used. If a callable is given it is
             used to pre-compute the kernel matrix from data matrices; that matrix
             should be an array of shape ``(n_samples, n_samples)``.
    
        degree: int, optional (default=3)
            Degree of the polynomial kernel function ('poly').
            Ignored by all other kernels.
    
        gamma: float, optional (default='auto')
            Kernel coefficient for 'rbf', 'poly' and 'sigmoid'.
            If gamma is 'auto' then 1/n_features will be used instead.
    
        coef0: float, optional (default=0.0)
            Independent term in kernel function.
            It is only significant in 'poly' and 'sigmoid'.
    
        probability: boolean, optional (default=False)
            Whether to enable probability estimates. This must be enabled prior
            to calling `fit`, and will slow down that method.
    
        shrinking: boolean, optional (default=True)
            Whether to use the shrinking heuristic.
    
        cache_size: float, optional
            Specify the size of the kernel cache (in MB).
    
        tol: float, optional (default=1e-3)
            Tolerance for stopping criterion.
    
        random_state: int, RandomState instance or None, optional (default=None)
            The seed of the pseudo random number generator to use when shuffling
            the data.  If int, random_state is the seed used by the random number
            generator; If RandomState instance, random_state is the random number
            generator; If None, the random number generator is the RandomState
            instance used by `np.random`.
    
        class_weight: {dict, 'balanced'}, optional
            Set the parameter C of class i to class_weight[i]*C for
            SVC. If not given, all classes are supposed to have
            weight one.
            The "balanced" mode uses the values of y to automatically adjust
            weights inversely proportional to class frequencies in the input data
            as ``n_samples / (n_classes * np.bincount(y))``
    
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SCORES_ARE_PROBA: boolean; whether or not the score values associated to the prediction made 
            by the underlying model should be interpreted as probability values or not
        
        class_labels: collection of unique class labels representing the class nomenclature the model 
            expects to work with
        
        support_: array-like, shape = [n_SV]
            Indices of support vectors.
    
        support_vectors_: array-like, shape = [n_SV, n_features]
            Support vectors.
    
        n_support_: array-like, dtype=int32, shape = [n_class]
            Number of support vectors for each class.
    
        dual_coef_: array, shape = [n_class-1, n_SV]
            Coefficients of the support vector in the decision function.
            For multiclass, coefficient for all 1-vs-1 classifiers.
            The layout of the coefficients in the multiclass case is somewhat
            non-trivial. See the section about multi-class classification in the
            SVM section of the User Guide for details.
    
        coef_: array, shape = [n_class-1, n_features]
            Weights assigned to the features (coefficients in the primal
            problem). This is only available in the case of a linear kernel.
    
            `coef_` is a readonly property derived from `dual_coef_` and
            `support_vectors_`.
    
        intercept_: array, shape = [n_class * (n_class-1) / 2]
            Constants in decision function.
        
        positive_class_label: the value of the 'positive_class_label' parameter used to initialize 
            the model class instance
        
        negative_class_label: the value of the 'negative_class_label' parameter used to initialize 
            the model class instance
        
        C: the value of the 'C' parameter used to initialize the model class instance
    
        kernel: the value of the 'kernel' parameter used to initialize the model class instance
    
        degree: the value of the 'degree' parameter used to initialize the model class instance
    
        gamma: the value of the 'gamma' parameter used to initialize the model class instance
    
        coef0: the value of the 'coef0' parameter used to initialize the model class instance
    
        probability: the value of the 'probability' parameter used to initialize the model class instance
    
        shrinking: the value of the 'shrinking' parameter used to initialize the model class instance
    
        cache_size: the value of the 'cache_size' parameter used to initialize the model class instance
    
        tol: the value of the 'tol' parameter used to initialize the model class instance
    
        random_state: the value of the 'random_state' parameter used to initialize the model class 
            instance
    
        class_weight: the value of the 'class_weight' parameter used to initialize the model class 
            instance
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    __INIT_PARAMETERS = OrderedDict((sv_C_attribute_data, sv_kernel_attribute_data, 
                                     sv_degree_attribute_data, sv_gamma_attribute_data, 
                                     sv_coef0_attribute_data, sv_probability_attribute_data, 
                                     sv_shrinking_attribute_data, sv_cache_size_attribute_data, 
                                     sv_max_iter_attribute_data, sv_tol_attribute_data,  
                                     random_state_attribute_data, 
                                     class_weight_attribute_data, 
                                     #verbose_attribute_data, 
                                     )
                                    )
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple(__INIT_PARAMETERS.keys()) + ("configuration",)
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = _SKLEARNLinearBinaryClassifier._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION+\
                                            tuple(__INIT_PARAMETERS.keys())
    _MODEL_DATA_ATTRIBUTES_NAMES = ("_dual_coef_", "_gamma", "_intercept_", "_sparse", "class_weight_", "classes_", 
                                   "dual_coef_", "fit_status_", "intercept_", "n_support_", "probA_", "probB_", 
                                   "shape_fit_", "support_", "support_vectors_",
                                   )
    
    _MODEL_FILE_NAME = "sv_sklearn_binary_classifier.cortex"
    SCORES_ARE_PROBA = False
    
    def __init__(self, positive_class_label, negative_class_label, 
                 C=SV_C_DEFAULT_VALUE, kernel=SV_KERNEL_DEFAULT_VALUE, degree=SV_DEGREE_DEFAULT_VALUE, 
                 gamma=SV_GAMMA_DEFAULT_VALUE, coef0=SV_COEF0_DEFAULT_VALUE, 
                 probability=SV_PROBABILITY_DEFAULT_VALUE, shrinking=SV_SHRINKING_DEFAULT_VALUE, 
                 cache_size=SV_CACHE_SIZE_DEFAULT_VALUE, 
                 max_iter=SV_MAX_ITER_DEFAULT_VALUE, tol=SV_TOL_DEFAULT_VALUE, 
                 random_state=RANDOM_STATE_DEFAULT_VALUE, 
                 class_weight=CLASS_WEIGHT_DEFAULT_VALUE, 
                 #verbose=VERBOSE_DEFAULT_VALUE, 
                 configuration=None):
        # Check input values
        # Initialize using parent class
        _SKLEARNBinaryClassifier.__init__(self, positive_class_label, negative_class_label, configuration=configuration)
        SVC.__init__(self, C=C, kernel=kernel, degree=degree, gamma=gamma, coef0=coef0, probability=probability, 
                     shrinking=shrinking, cache_size=cache_size, decision_function_shape="ovr", 
                     max_iter=max_iter, tol=tol, class_weight=class_weight, random_state=random_state, 
                     #verbose=verbose,
                    )
        # Set attribute values
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        (C, kernel, degree, gamma, coef0, probability, shrinking, cache_size, 
         max_iter, tol, random_state, class_weight, 
         #verbose
         ) = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        kwargs["C"] = C
        kwargs["kernel"] = kernel
        kwargs["degree"] = degree 
        kwargs["gamma"] = gamma
        kwargs["coef0"] = coef0 
        kwargs["probability"] = probability 
        kwargs["shrinking"] = shrinking 
        kwargs["cache_size"] = cache_size
        kwargs["max_iter"] = max_iter
        kwargs["tol"] = tol
        kwargs["random_state"] = random_state
        kwargs["class_weight"] = class_weight
        #kwargs["verbose"] = verbose
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, positive_class_label, negative_class_label, 
                              C=SV_C_DEFAULT_VALUE, kernel=SV_KERNEL_DEFAULT_VALUE, degree=SV_DEGREE_DEFAULT_VALUE, 
                              gamma=SV_GAMMA_DEFAULT_VALUE, coef0=SV_COEF0_DEFAULT_VALUE, 
                              probability=SV_PROBABILITY_DEFAULT_VALUE, shrinking=SV_SHRINKING_DEFAULT_VALUE, 
                              cache_size=SV_CACHE_SIZE_DEFAULT_VALUE, 
                              max_iter=SV_MAX_ITER_DEFAULT_VALUE, tol=SV_TOL_DEFAULT_VALUE, 
                              random_state=RANDOM_STATE_DEFAULT_VALUE, 
                              class_weight=CLASS_WEIGHT_DEFAULT_VALUE, 
                              #verbose=VERBOSE_DEFAULT_VALUE,
                              ):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Cf the documentation of the class of know which argument(s) to use.
        
        Returns:
            a Mapping instance
        """
        configuration = super().define_configuration(positive_class_label, negative_class_label)
        configuration["C"] = C 
        configuration["kernel"] = kernel 
        configuration["degree"] = degree
        configuration["gamma"] = gamma 
        configuration["coef0"] = coef0 
        configuration["probability"] = probability 
        configuration["shrinking"] = shrinking 
        configuration["cache_size"] = cache_size 
        configuration["max_iter"] = max_iter 
        configuration["tol"] = tol 
        configuration["random_state"] = random_state
        configuration["class_weight"] = class_weight 
        #configuration["verbose"] = verbose 
        return configuration
    
    def can_predict(self):
        """ Checks whether or not this model's class instance possesses all the the attribute it needs 
        to carry out a prediction.
        
        Returns:
            boolean; True if it does, False if it does not
        """
        return all(getattr(self, attribute_name, None) is not None for attribute_name in self._MODEL_DATA_ATTRIBUTES_NAMES)
    
    def predict_class_scores(self, data):
        """ Carries out a prediction step on the input samples data matrix, and output an iterable 
        over collection of scores such that each score in a given collection is associated to the 
        corresponding class label in the collection of class labels associated to this model instance.
        
        Args:
            X: numpy 2D array or scipy.sparse matrix of shape (samples_nb, features_nb)

        Returns:
            a (samples_nb; classes_nb)-shaped numpy array
        """
        return self.predict_decision_function_class_scores(data)
#SVSKLEARNBinaryClassifier.__doc__ = SVSKLEARNBinaryClassifier.__doc__ + SVC.__doc__


classifier_names_collection_and_classifier_class_pairs = ((("logisticregressionsklearnbinary", LogisticRegressionSKLEARNBinaryClassifier.__name__), LogisticRegressionSKLEARNBinaryClassifier),
                                                          (("perceptronsklearnbinary", PerceptronSKLEARNBinaryClassifier.__name__), PerceptronSKLEARNBinaryClassifier),
                                                          (("pasklearnbinary", PassiveAggressiveSKLEARNBinaryClassifier.__name__), PassiveAggressiveSKLEARNBinaryClassifier),
                                                          (("svsklearnbinary", SVSKLEARNBinaryClassifier.__name__), SVSKLEARNBinaryClassifier),
                                                          #(("", .__name__), ),
                                                          )
classifier_name2classifier_class = OrderedDict((name, class_) for names, class_ in classifier_names_collection_and_classifier_class_pairs for name in names)
