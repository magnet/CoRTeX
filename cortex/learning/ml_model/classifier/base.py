# -*- coding: utf-8 -*-

"""
Defines base classifier classes
"""

__all__ = ["_Classifier", 
           "_BinaryClassifier",
           ]

import abc
from collections import OrderedDict

from cortex.utils import EqualityMixIn, get_unique_elts
from cortex.tools import (ConfigurationMixIn, _create_simple_get_parameter_value_from_configuration,
                            _create_simple_set_parameter_value_in_configuration)


labels_attribute_data = ("labels", 
                         (_create_simple_get_parameter_value_from_configuration("labels", postprocess_fct=tuple), 
                          _create_simple_set_parameter_value_in_configuration("labels")
                          )
                         )
class _Classifier(ConfigurationMixIn, EqualityMixIn, metaclass=abc.ABCMeta):
    """ Base class for classes whose instances act as classification ML models.
    As far as prediction is concerned, assume that the predicted class is the class associated to 
    the highest score value.
    
    Here the choice has been made to ask the user to specify the collection of possible classification 
    labels, since the learning may be made online rather than in one block, and so, in such a case it 
    is not possible to consider all the samples used for learning prior to the learning so as to 
    estimate the collection of possible labels.
    
    Arguments:
        labels: collection of label values, constituting the classification nomenclature, that the 
        model is expected to work with. Must contain at least two different values.
        
        configuration: a Mapping instance; or None; info about the class instance that 
        the user wants to keep around; values of fields corresponding to the attributes of the instance 
        that can be parametrized through the '__init__' method will be replaced by the value needed 
        to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SCORES_ARE_PROBA: boolean; whether or not the score values associated to the prediction made 
        by the underlying model should be interpreted as probability values or not
        
        class_labels: collection of get_unique_elts class labels representing the class nomenclature the model 
        expects to work with
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
        to the values of the corresponding attributes of the instance, and generally correspond to 
        parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    __INIT_PARAMETERS = OrderedDict((labels_attribute_data, ))
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple(__INIT_PARAMETERS.keys()) + ("configuration",)
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = tuple(__INIT_PARAMETERS.keys())
    SCORES_ARE_PROBA = None
    
    def __init__(self, labels, configuration=None):
        # Check input values
        labels = tuple(get_unique_elts(labels))
        if len(labels) < 2:
            msg = "The input labels collection '{}' contains less than two different label value."
            msg = msg.format(labels) 
            raise ValueError(labels)
        # Initialize using parent class
        ConfigurationMixIn.__init__(self, configuration=configuration)
        # Set attribute values
        self.labels = labels # TODO: make it so this become a hidden attribute, if the 'class_labels' property is used instead
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extract *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        labels = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        args = args + (labels,)
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, labels):
        """ Create a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        Cf the documentation of the class of know which argument(s) to use.
        

        Returns:
            a Mapping instance
        """
        configuration = super().define_configuration()
        configuration["labels"] = labels
        return configuration
    
    @abc.abstractproperty
    def class_labels(self):
        """ Must return the collection of class labels used as the class nomenclature by the classifier. """
        pass
    @class_labels.setter
    def class_labels(self, value):
        raise AttributeError
    
    @abc.abstractmethod
    def predict_class_scores(self, samples):
        """ Must return an iterable over class scores predicted for each sample in the same order.
        One 'classes scores' item is a collection of scores associated to each class, in the same 
        order than the collection of classes defined by 'class_labels', and such that predicted 
        class for a given sample should be the class whose score is the highest.
        """
        pass
    
    @abc.abstractmethod
    def save_model(self, folder_path):
        """ Must persist as files in a local folder the data corresponding to the the current state of the 
        model, and which is necessary to initialize another model of the same kind so that it be able 
        to carry out predictions. 
        
        Args:
            folder_path: path to the local folder where the data shall be persisted
        """
        pass
    
    @abc.abstractmethod
    def load_model(self, folder_path):
        """ Must use previously locally persisted data to initialize the internal state of the model, 
        so that it be able to carry out predictions. 
        
        Args:
            folder_path: path to the local folder from which that data shall be loaded
        """
        pass


positive_class_label_attribute_data = ("positive_class_label", 
                                       (_create_simple_get_parameter_value_from_configuration("positive_class_label"),
                                        _create_simple_set_parameter_value_in_configuration("positive_class_label"))
                                       )
negative_class_label_attribute_data = ("negative_class_label", 
                                       (_create_simple_get_parameter_value_from_configuration("negative_class_label"),
                                        _create_simple_set_parameter_value_in_configuration("negative_class_label"))
                                       )
class _BinaryClassifier(_Classifier):
    """ Base class for classes whose instances act as binary classification ML models.
    
    So as to be compatible with some use cases in CoRTeX' resolver class instances that use such 
    models, it is for now necessary to specify the role of the two class labels; that is, it is 
    necessary to explicitly say which class label is associated to the "positive class", and which 
    class label is associated to the "negative class". 
    When carrying out a prediction for a given sample, a positive score corresponds to prediction 
    the positive class, and a negative score corresponds to predicting the negative class.
    
    Arguments:
        positive_class_label: class label associated to the class which has the role of positive class
        
        negative_class_label: the value of the 'negative_class_label' parameter used to initialize the model class instance
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SCORES_ARE_PROBA: boolean; whether or not the score values associated to the prediction made 
            by the underlying model should be interpreted as probability values or not
        
        class_labels: collection of get_unique_elts class labels representing the class nomenclature the model 
            expects to work with
        
        positive_class_label: the value of the 'positive_class_label' parameter used to initialize 
            the model class instance
        
        negative_class_label: the value of the 'negative_class_label' parameter used to initialize 
            the model class instance
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    __INIT_PARAMETERS = OrderedDict((positive_class_label_attribute_data, negative_class_label_attribute_data))
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple(__INIT_PARAMETERS.keys()) + _Classifier._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = tuple(__INIT_PARAMETERS.keys())
    SCORES_ARE_PROBA = None
    
    def __init__(self, positive_class_label, negative_class_label, configuration=None):
        # Check input values
        if positive_class_label == negative_class_label:
            msg = "The input positive class label value is equal to that of the negative class ('{}')"\
                  ", they must be distinct wrt the '==' operation."
            msg = msg.format(positive_class_label)
            raise ValueError(msg)
        # Initialize using parent class
        labels = (positive_class_label, negative_class_label) # TODO: reverse the order, so that there is no need to implement anew the 'class_labels' property for child classes?
        super().__init__(labels, configuration=configuration)
        del self.configuration["labels"] # We do not want this to stay in the configuration, what needs to stay, now, is 'positive_class_label' and 'negative_class_label' instead
        # Set attribute values
        self.positive_class_label = positive_class_label
        self.negative_class_label = negative_class_label
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extract *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        configuration["labels"] = (None,None)
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        positive_class_label, negative_class_label = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        args = args[:-1] + (positive_class_label, negative_class_label)
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, positive_class_label, negative_class_label):
        """ Create a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Cf the documentation of the class of know which argument(s) to use.
        
        Returns:
            a Mapping instance
        """
        configuration = super().define_configuration((positive_class_label, negative_class_label))
        del configuration["labels"]
        configuration["positive_class_label"] = positive_class_label
        configuration["negative_class_label"] = negative_class_label 
        return configuration


