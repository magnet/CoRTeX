# -*- coding: utf-8 -*-

"""
Defines classes representing classification machine learning models that can be trained and then 
used for prediction
"""

"""
Available submodule(s)
----------------------
base
    Defines base classifier classes

online_binary_classifier
    Defines custom classifier classes specifically made for online training

sklearn_binary_classifier
    Defines classifier classes based upon some of the sklearn python package's classifier classes
"""

__all__ = ["PerceptronOnlineBinaryClassifier", 
           "PAOnlineBinaryClassifier", 
           "CWFullOnlineBinaryClassifier", 
           "CWDiagOnlineBinaryClassifier", 
           "AROWFullOnlineBinaryClassifier", 
           "AROWDiagOnlineBinaryClassifier",
           "LogisticRegressionSKLEARNBinaryClassifier", 
           "PerceptronSKLEARNBinaryClassifier",
           "PassiveAggressiveSKLEARNBinaryClassifier", 
           "SVSKLEARNBinaryClassifier",  
           "create_classifier",
           "create_classifier_from_factory_configuration",
           "generate_classifier_factory_configuration",
           ]

import itertools
from collections import OrderedDict

from .online_binary_classifier import (classifier_name2classifier_class as online_classifiers_map,
                                       PerceptronOnlineBinaryClassifier, 
                                       PAOnlineBinaryClassifier, 
                                       CWFullOnlineBinaryClassifier, 
                                       CWDiagOnlineBinaryClassifier, 
                                       AROWFullOnlineBinaryClassifier, 
                                       AROWDiagOnlineBinaryClassifier,
                                       )
from .sklearn_binary_classifier import (classifier_name2classifier_class as sklearn_classifiers_map,
                                        LogisticRegressionSKLEARNBinaryClassifier, 
                                        PerceptronSKLEARNBinaryClassifier,
                                        PassiveAggressiveSKLEARNBinaryClassifier, 
                                        SVSKLEARNBinaryClassifier,
                                        )
from cortex.tools import (define_check_item_name_function, define_create_function, 
                          define_generate_factory_configuration_function, 
                          define_create_from_factory_configuration_function,
                          )



classifier_name2classifier_class = OrderedDict(itertools.chain(online_classifiers_map.items(), 
                                                               sklearn_classifiers_map.items(),
                                                               )
                                               )

# High level functions
_check_classifier_name = define_check_item_name_function(classifier_name2classifier_class, "classifier")

create_classifier = define_create_function(classifier_name2classifier_class, "classifier")

create_classifier_from_factory_configuration =\
 define_create_from_factory_configuration_function(classifier_name2classifier_class, "classifier")
 
generate_classifier_factory_configuration =\
 define_generate_factory_configuration_function(classifier_name2classifier_class, "classifier")
