# -*- coding: utf-8 -*-

"""
Defines a class used to translate collections of Sample child classes instances into sample data matrices 
and labels vectors
"""

__all__ = ["ClassificationSampleConverter",
           ]

import numpy
from scipy import sparse
from collections import OrderedDict, defaultdict

from cortex.parameters import ENCODING, LINE_SEPARATOR
from cortex.utils import EqualityMixIn, get_unique_elts



class ClassificationSampleConverter(EqualityMixIn):
    """ The aim of this class is to provide a tool used to convert a _Sample child class 
    instance to a (label; vector) pair that can be used by a ML-model learning object.
    
    It must also be able to convert from a value predicted by the learnt ML-model object to a label 
    value used by CorTeX in the Sample child class instances.
    
    An instance of this class is able to cache the results of the vectorization processes carried out 
    on classification samples. A cache is a "sample_hash_string => vector" map. The vectorized values 
    are kept in a cache specific to the document their corresponding classification samples were 
    defined from. 
    Cf the 'vectorize_with_memoization' method.
    
    Arguments:
        labels: collection of strings; assume that the first element corresponds to a class label 
            to be assigned to a sample if a sample's label is unknown when trying to vectorize this sample
        
        sample_features_vectorizer: a _BaseMentionPairSampleFeaturesVectorizer child class instance, 
            used to vectorize a sample
    
    Attributes:
        labels: collection of strings; assume that the first element corresponds to a class label 
            to be assigned to a sample if a sample's label is unknown when trying to vectorize this sample
        
        features_names: collection of the name associated to the columns of the vector representation 
            of a sample
        
        features_nb: number of columns of the vector representation of a sample; that is, the length 
            of such a vector
        
        document_hash2cache: current state of the "document hash => cache of vector values" associated 
            to the instance
    """
    
    _CSV_SEPARATOR = "\t"
    _FEATURE_VALUE_POSSIBLE_TYPES = (int, float)
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = ("labels", "_sample_features_vectorizer")
    
    def __init__(self, labels, sample_features_vectorizer):
        if not labels:
            raise ValueError("Input 'labels' parameter is empty, must contain at least one value.")
        if not sample_features_vectorizer.numeric_column_names:
            raise ValueError("Input 'sample_features_vectorizer' is incorrect, must possess at least one numeric feature.")
        self._initialize_label_values(labels)
        self._sample_features_vectorizer = sample_features_vectorizer
        self._feature_name2feature_index = OrderedDict((name, index+1) for index, name in enumerate(self._sample_features_vectorizer.numeric_column_names))
        self._feature_index2feature_name = lambda index: self._sample_features_vectorizer.numeric_column_names[index]
        self._document_hash2cache = defaultdict(dict)

    def _initialize_label_values(self, labels):
        """ Sets the input labels collection to be the one supported by this instance.
        
        Args:
            labels: collection of strings
        """
        labels = get_unique_elts(labels)
        coded_label2label = labels
        label2coded_label = OrderedDict((label,coded_label) for (coded_label,label) in enumerate(coded_label2label))
        self._coded_label2label = coded_label2label
        self._label2coded_label = label2coded_label
        self.labels = labels
        
    @property
    def features_names(self):
        return self._sample_features_vectorizer.numeric_column_names
    @features_names.setter
    def features_names(self, value):
        raise AttributeError
        
    @property
    def features_nb(self):
        return self._sample_features_vectorizer.numeric_column_nb
    @features_nb.setter
    def features_nb(self, value):
        raise AttributeError
    
    @property
    def document_hash2cache(self):
        return self._document_hash2cache
    @document_hash2cache.setter
    def document_hash2cache(self, value):
        self._document_hash2cache = value
    
    # Instance methods
    def get_feature_name_coded_feature_pairs_iterator(self):
        """ Returns an iterator over (feature_name; coded_feature_value) pairs associated to the 
        feature names defined by the instance's sample vectorizer.
        
        Yields:
            a (feature_name, coded_feature_value) pair
        """
        return self._feature_name2feature_index.items()
    
    ## Labels related methods
    def encode_label(self, label):
        """ Returns the code corresponding to the input label value, if it is a correct label value; 
        if it is not, then return -1.
        
        Args:
            label: a legitimate label value

        Returns:
            int
        """
        try:
            return self._label2coded_label[label]
        except KeyError:
            return -1
    
    def decode_label(self, index):
        """ Returns the label associated to the input label index.
        
        Args:
            index: int

        Returns:
            the corresponding label
        """
        try:
            return self._coded_label2label[index]
        except TypeError or IndexError:
            message = "Incorrect 'index' input value; possible values are: {}."
            message = message.format(tuple(range(len(self._coded_label2label))))
            raise ValueError(message) from None
    
    ## Methods to encode data
    def vectorize(self, classification_samples):
        """ Returns the sparse matrix whose rows are the vectorized versions of the input 
        classification sample collection.
        
        The order of the rows is the same as the one in which the samples are defined within the 
        input collection.
        
        Args:
            classification_samples: collection of classification samples

        Returns:
            a ((samples_nb; features_nb)-shaped sparse.csr_matrix instance
        """
        return self._sample_features_vectorizer.vectorize(classification_samples, output_format="sparse")
    
    def vectorize_with_memoization(self, classification_samples):
        """ Returns the sparse matrix whose rows are the vectorized versions of the input 
        classification sample collection, by potentially using cached values from a precedent 
        computation. If some values were not computed before, then compute them now, and add them 
        to the cache.
        
        The order of the rows is the same as the one in which the samples are defined within the 
        input collection.
        
        Args:
            classification_samples: collection of classification samples

        Returns:
            a ((samples_nb; features_nb)-shaped sparse.csr_matrix instance
        """
        # Setup caching and collect samples whose vectorization has been cached yet
        document_hash2cache = self._document_hash2cache
        document_id2hash_string = {}
        cached = []
        classification_samples_to_vectorize = []
        keys_pairs_to_vectorize = []
        collec_index_pairs = [] # List of (collection; index) pairs, will be used later to fetch collection[index] values in the order in which the pair were added to this list
        rt = _CacheWrappedCollection([], keys_pairs_to_vectorize, document_hash2cache)
        for sample in classification_samples:
            # Get the ml sample's key
            sample_key = sample.to_hash_string()
            # Get the ml sample's document's key
            document = sample.document
            document_id = id(document)
            document_key = document_id2hash_string.get(document_id)
            if document_key is None:
                document_key = sample.document.hash_string
                document_id2hash_string[document_id] = document_key
            # Process cache data, or prepare computation and caching
            cached_value = document_hash2cache[document_key].get(sample_key)
            if cached_value is None:
                classification_samples_to_vectorize.append(sample)
                keys_pairs_to_vectorize.append((document_key,sample_key))
                collec_index_pairs.append((rt, len(classification_samples_to_vectorize)-1))
            else:
                cached.append(cached_value)
                collec_index_pairs.append((cached, len(cached)-1))
        # Vectorize the samples that had not been cached up until now
        if classification_samples_to_vectorize:
            vectorized_data = self.vectorize(classification_samples_to_vectorize)
            rt.samples_vectors = tuple(vectorized_data)
        # Fetch all vectorized data in the correct order, while caching newly computed data
        _matrixes = tuple(collec[index] for collec, index in collec_index_pairs)
        features_matrix = sparse.vstack(_matrixes) if _matrixes else sparse.csr_matrix((0,self.features_nb))
        
        return features_matrix
    
    def convert(self, classification_samples):
        """ Returns the (sample features matrix; sample label values array) associated to the input 
        classification samples collection.
        
        Args:
            classification_samples: collection of classification samples

        Returns:
            a ((samples_nb; features_nb)-shaped sparse.csr_matrix; numpy 1D array) pair, 
            respectively corresponding to the feature matrix and label values array
        """
        # Compute the label values vector
        encoded_labels = numpy.array(tuple(self.encode_label(sample.label) for sample in classification_samples))
        # Compute the vectorized sample data matrix
        data = self.vectorize(classification_samples)
        return data, encoded_labels
    
    def convert_with_memoization(self, classification_samples):
        """ Returns the (sample features matrix; sample label values array) associated to the input 
        classification samples collection, while using the caching of vector values.
        
        Args:
            classification_samples: collection of classification samples

        Returns:
            ((samples_nb; features_nb)-shaped sparse.csr_matrix; numpy 1D array) pair, 
            respectively corresponding to the feature matrix and label values array
        """
        # Compute the label values vector
        encoded_labels = numpy.array(tuple(self.encode_label(sample.label) for sample in classification_samples))
        # Compute the vectorized sample data matrix
        data = self.vectorize_with_memoization(classification_samples)
        return data, encoded_labels
    
    def convert_to_svmlight_file(self, classification_samples, file_path):
        """ Writes the encoded version of the samples of the input classification into the designated 
        file, under the 'svmlight' format.
        
        Args:
            classification_samples: collection of classification samples
            file_path: path where the 'svmligh' formatted file shall be written
        """
        # Encode samples
        data, encoded_labels = self.convert(classification_samples)
        # Write classification_samples using a specific format
        with open(file_path, "w", encoding=ENCODING) as f:
            f.writelines(self.convert_to_svmlight_file_format(encoded_sample, encoded_label)+LINE_SEPARATOR \
                         for encoded_sample, encoded_label in zip(data, encoded_labels))
    
    
    # Static methods
    @staticmethod
    def convert_to_svmlight_file_format(vector, encoded_label):
        """ Returns a string representing a vectorized sample using the 'SVMLight' sparse data file format.
        
        Args:
            vector: a numpy (1,N)-shaped array, or a (1,N)-shaped scipy.sparse.sparse_matrix
            encoded_label: int

        Returns:
            string, the 'svmlight' formatted line corresponding to the inputs
        
        Examples:
            
            .. code:: python
                
                >>> vector = numpy.array([2.,0.,0.,-9.,0,45.63]).reshape((1,-1)
                >>> encoded_label = 1
                >>> converter.convert_to_svmlight_file_format(vector, encoded_label)
                '1 0:2 3:-9 5:45.63'
            
            
            or
            
            .. code:: python
                
                >>> vector = numpy.array([1.,0.,0.6,-5.,0,12.65,0,0,42.]).reshape((1,-1)
                >>> encoded_label = 0
                >>> converter.convert_to_svmlight_file_format(vector, encoded_label)
                '0 0:1 2:0.6 3:-5 5:12.65 8:42'
        """
        features_string = " ".join("{}:{}".format(col_id+1, vector[0,col_id]) for col_id in vector.nonzero()[1])
        return "{} {}".format(encoded_label, features_string)


# Utilities
class _CacheWrappedCollection(object):
    """ A class whose role is to link a list of samples' respective vector values with a cache 
    (indexed by the document that a given sample refers to) for those vector values, 
    so that when a vector value is accessed using an index (just like one would do with a plain list), 
    its value is added to the correct cache.
    
    Arguments:
        samples_vectors: collection of sample vector values that the class instance shall wrap around
        
        document_hash_and_vector_key_pairs: collection of (document_hash; vector_key) pairs 
            corresponding term to term with the 'samples_vectors' input: the pair located at index i 
            corresponds to the hash of the document that the vector located at index i in the 'samples_vectors' 
            input refers to, and shall be used to fetch the cache to use, from the input 'document_hash2cache' 
            value; and the 'vector_key' is the key to which the vector value shall be associated within 
            the cache where it shall be stored
        
        document_hash2cache: a "document_hash => cache" map to fill when fetching a vector value by 
            its index in the wrapped collection
        
    
    Attributes:
        samples_vectors: collection of sample vector values that the class instance wraps around
        
        document_hash_and_vector_key_pairs: the collection of (document_hash; vector_key) pairs 
            corresponding term to term with the 'samples_vectors' input: the pair located at index i 
            corresponds to the hash of the document that the vector located at index i in the 'samples_vectors' 
            attribute refers to, and shall be used to fetch the cache to use, from the  'document_hash2cache' 
            attribute value; and the 'vector_key' is the key to which the vector value shall be associated 
            within the cache where it shall be stored
        
        document_hash2cache: the "document_hash => cache" map to fill when fetching a vector value by 
            its index in the wrapped collection
        
    """
    def __init__(self, samples_vectors, document_hash_and_vector_key_pairs, document_hash2cache):
        self.samples_vectors = samples_vectors
        self.document_hash_and_vector_key_pairs = document_hash_and_vector_key_pairs
        self.document_hash2cache = document_hash2cache
    def __getitem__(self, index):
        value = self.samples_vectors[index]
        document_hash, item_key = self.document_hash_and_vector_key_pairs[index]
        cache = self.document_hash2cache[document_hash]
        cache[item_key] = value
        return value
    def __len__(self):
        return len(self.samples_vectors)

