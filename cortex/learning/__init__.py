# -*- coding: utf-8 -*-

"""
Defines classes and structures needed to carry out machine learning, and to interface with ML interpretation 
of the coreference partition task
"""

"""
Available subpackage(s)
----------------------
filter
    Defines utilities to filter a set of specific class instances, possibly based on textual data 
    they may hold

ml_features
    Defines utilities to create a 'cortex.tools.ml_features.Vectorizer' instance that can be used to 
    vectorize Sample child classes instances

ml_model
    Defines classes representing machine learning models that can be trained and then used for prediction

model_interface
    Defines classes used to interface between ML models and Sample child classes instances, and 
    translate instruction regarding a set of such instances into instructions for the underlying 
    ML model

samples_features_vectorizer
    Defines classes representing a Vectorizer, but whose instantiation operation can be configured 
"""

"""
Available submodule(s)
----------------------
classification_sample_converter
    Defines a class used to translate collections of Sample child classes instances into sample data 
    matrices and labels vectors

parameters
    Defines parameters pertaining to the translation of tasks such as coreference partition resolution 
    to ML tasks, such as binary classification

prediction
    Defines classes used to represent the result of a ML model prediction regarding a Sample child 
    class instance

samples
    Defines classes used to represent a sample that can be used in the context of a ML task
"""