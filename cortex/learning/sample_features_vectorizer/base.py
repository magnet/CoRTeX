# -*- coding: utf-8 -*-

"""
Defines base sample features vectorizer classes
"""

__all__ = ["_SampleFeaturesVectorizer",
           ]

import abc
from collections import OrderedDict

from cortex.utils import EqualityMixIn
from cortex.tools import ConfigurationMixIn



class _SampleFeaturesVectorizer(ConfigurationMixIn, EqualityMixIn, metaclass=abc.ABCMeta):
    """ Base class for classes whose instances shall wrap a SampleVectorizer instance: if an attribute 
    is not found in a given instance of such a class, then it is assumed that the attribute value that 
    is looked for corresponds to an attribute of the wrapped SampleVectotizer instance. This allows 
    to use the methods and attributes of a SampleVectorizer instance directly from an instance of 
    this class.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    _INIT_PARAMETERS = OrderedDict()
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = ("configuration",)
    
    @abc.abstractmethod
    def __init__(self, configuration=None):
        # Check input values
        # Initialize using parent class
        ConfigurationMixIn.__init__(self, configuration=configuration)
        # Set attribute values
        self._features_vectorizer = None # To ensure correct functioning this class' '__getattr__' method
        # All value that can be input into the '__init__' function must be recorded in the configuration
    
    # Instance method
    def __getattr__(self, attribute_name):
        """ This method is called after trying to fetch the value of the potential attribute of this 
        instance, whose name is input, failed. In that case, assumes that the value that is searched 
        for one of the attribute of the wrapped SampleVectorizer instance, and so tries to fetch it 
        instead. Also works for calling methods of the wrapped SampleVectorizer instance.
        
        Args:
            attribute_name: string, name of the attribute that one seeks to access

        Returns:
            the value of the corresponding attribute existing for the wrapped SampleVectorizer instance
        
        Raise:
            AttributeError: if the attribute does not exist either in the wrapped Sample Vectorizer 
            instance.
        """
        try:
            attribute_value =  getattr(self._features_vectorizer, attribute_name)
        except AttributeError:
            msg = "AttributeError: '{}' object has no attribute '{}'".format(type(self).__name__, attribute_name)
            raise AttributeError(msg) from None
        return attribute_value
    
    def _set_configuration_values(self):
        """ Makes it so the configuration associated to this instance is filled with the attribute 
        values of this instance.
        """
        self._set_configuration_values_from_instance(self._INIT_PARAMETERS, self)