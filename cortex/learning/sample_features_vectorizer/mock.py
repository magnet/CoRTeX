# -*- coding: utf-8 -*-

"""
Defines simple sample features vectorizer class useful for mocking in tests
"""

__all__ = ["MockSampleFeaturesVectorizer", 
           "get_simple_features_vectorizer", 
           "get_simple_mention_pair_features_vectorizer",
           ]

from collections import OrderedDict
import numpy

from .base import _SampleFeaturesVectorizer
from cortex.tools.ml_features import (NumericFeature, MultiNumericFeature,CategoricalFeature, 
                                        SampleVectorizer, 
                                        create_product_features_from_groups_definition)
from cortex.tools import (_create_simple_set_parameter_value_in_configuration, 
                          _create_simple_get_parameter_value_from_configuration)

INVERSE_ORDER_DEFAULT_VALUE = False
inverse_order_attribute_data = ("inverse_order",
                                (_create_simple_get_parameter_value_from_configuration("inverse_order", 
                                                                                       default=INVERSE_ORDER_DEFAULT_VALUE, 
                                                                                       postprocess_fct=bool), 
                                 _create_simple_set_parameter_value_in_configuration("inverse_order"))
                                )
FOR_MENTION_PAIR_SAMPLE = False
for_mention_pair_sample_attribute_data = ("for_mention_pair_sample",
                                        (_create_simple_get_parameter_value_from_configuration("for_mention_pair_sample", 
                                                                                               default=FOR_MENTION_PAIR_SAMPLE, 
                                                                                               postprocess_fct=bool), 
                                         _create_simple_set_parameter_value_in_configuration("for_mention_pair_sample"))
                                        )
class MockSampleFeaturesVectorizer(_SampleFeaturesVectorizer):
    """ Class whose instances' role is to be easily testable, notably with regards to it wrapping 
    around a SampleVectorizer instance.
    
    Arguments:
        inverse_order: boolean, whether to reverse the order of the Feature instances used to 
            define the SampleVectorizer instance to be wrapped around
        
        for_mention_pair_sample: boolean, whether the inputs of the SampleVectorizer instance to 
            be wrapped around will be MentionPairSample instances, or will be plain numbers
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        inverse_order: boolean, the 'inverse_order' value that the instance has been initialized with
        
        for_mention_pair_sample: boolean, the 'for_mention_pair_sample' value that the instance has 
            been initialized with
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    __INIT_PARAMETERS = OrderedDict((inverse_order_attribute_data, for_mention_pair_sample_attribute_data))
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple(__INIT_PARAMETERS.keys()) + ("_features_vectorizer",) +\
                                                 _SampleFeaturesVectorizer._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = _SampleFeaturesVectorizer._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION+\
                                            tuple(__INIT_PARAMETERS.keys())
    
    def __init__(self, inverse_order=INVERSE_ORDER_DEFAULT_VALUE, for_mention_pair_sample=FOR_MENTION_PAIR_SAMPLE, 
                 configuration=None):
        # Check input values
        # Init parent
        super().__init__(configuration=configuration)
        # Check input values
        # Assign attribute values
        self.inverse_order = bool(inverse_order)
        self.for_mention_pair_sample = bool(for_mention_pair_sample)
        if for_mention_pair_sample:
            features_vectorizer = get_simple_mention_pair_features_vectorizer()
        else:
            features_vectorizer = get_simple_features_vectorizer(sample=True, inverse_order=self.inverse_order)
        self._features_vectorizer = features_vectorizer
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        inverse_order, for_mention_pair_sample = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        kwargs["inverse_order"] = inverse_order
        kwargs["for_mention_pair_sample"] = for_mention_pair_sample
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, inverse_order=INVERSE_ORDER_DEFAULT_VALUE, 
                             for_mention_pair_sample=FOR_MENTION_PAIR_SAMPLE):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        
        this class, using the 'from_configuration' class method.
        Cf the documentation of the class of know which argument(s) to use.
        
        Returns:
            a Mapping instance
        """
        configuration = super().define_configuration()
        configuration["inverse_order"] = inverse_order
        configuration["for_mention_pair_sample"] = for_mention_pair_sample
        return configuration


def get_simple_features_vectorizer(sample=False, inverse_order=False):
    """ Creates a SampleVectorizer instance that takes numbers as inputs.
    
    Args:
        sample: boolean, whether the inputs of the SampleVectorizer instance to be created will 
            be python objects possessing an 'x' attribute (whose value will be used to carry out vectorization 
            operations), or will directly be numbers
        inverse_order: boolean, whether to reverse the order of the Feature instances used to 
            define the SampleVectorizer instance

    Returns:
        a SampleVectorizer instance
    """
    
    features = []
    ## Numeric feature
    compute_fct = lambda x: -x**2
    if sample:
        compute_fct = lambda sample: -(sample.x)**2
    feature = NumericFeature("test_numeric_feature", compute_fct)
    features.append(feature)
    ## Categorical feature 1
    compute_fct = lambda x: "yes" if -x**2 < -26 else "no"
    if sample:
        compute_fct = lambda sample: "yes" if -(sample.x)**2 < -26 else "no"
    possible_values = ("yes", "no")
    strict = True
    feature = CategoricalFeature("test_categorical_feature1", compute_fct, possible_values, strict=strict)
    features.append(feature)
    ## Categorical feature 2
    compute_fct = lambda x: "yep" if x < 0 else "nope"
    if sample:
        compute_fct = lambda sample: "yep" if sample.x < 0 else "nope"
    possible_values = ("yep", "nope")
    strict = True
    feature = CategoricalFeature("test_categorical_feature2", compute_fct, possible_values, strict=strict)
    features.append(feature)
    ## Group features
    first_group = (("test_categorical_feature1",), ("test_categorical_feature2",))
    second_group = (("test_categorical_feature2",), ("test_categorical_feature1",))
    features_groups = (first_group, second_group, )
    group_features = create_product_features_from_groups_definition(features_groups, features)
    ## Finally, the FeatureVectorizer instance
    if inverse_order:
        features = features[::-1]
    features = features + group_features
    features_vectorizer = SampleVectorizer(features)
    
    return features_vectorizer


sample_features_vectorizer_names_collection_and_sample_features_vectorizer_class_pairs = ((("mock", MockSampleFeaturesVectorizer.__name__), MockSampleFeaturesVectorizer),
                                                                                          )
sample_features_vectorizer_name2sample_features_vectorizer_class = OrderedDict((name, class_) for names, class_ in sample_features_vectorizer_names_collection_and_sample_features_vectorizer_class_pairs for name in names)


def get_simple_mention_pair_features_vectorizer():
    """ Creates a SampleVectorizer instance that takes MentionPairSample instances as inputs.
    
    Returns:
        a SampleVectorizer instance
    """
    features = []
    
    ## MultiNumeric feature
    def _multinumeric_feature_mention_pair_sample(mention_pair_sample, matrix=None, row_id=0, column_offset=0):
        if matrix is None:
            result = numpy.matrix((1,6), dtype=numpy.int)
            _multinumeric_feature_mention_pair_sample(mention_pair_sample, matrix=result, row_id=0, column_offset=0)
            return result
        if hasattr(mention_pair_sample, "mention") and mention_pair_sample.mention is not None:
            extent = mention_pair_sample.mention.extent
            s = sum(extent)
            v1 = numpy.sign(float(s % 2)-0.5)
            v2 = s
            matrix[row_id,column_offset+2:column_offset+4] = v1, v2
        else:
            s1, s2 = tuple(map(sum, zip(mention_pair_sample.left.extent, mention_pair_sample.right.extent)))
            v1 = numpy.sign(float(s1 % 2)-0.5) * s1
            v2 = numpy.sign(float(s1 % 2)-0.5) * s2
            matrix[row_id,column_offset+0:column_offset+2] = v1, v2
        return matrix
    column_names = ("signed_begin_sum", "signed_end_sum", "extent_sum", "sign_extent_sum")
    feature = MultiNumericFeature("test_multinumeric_feature", _multinumeric_feature_mention_pair_sample, column_names)
    features.append(feature)

    ## Finally, the FeatureVectorizer instance
    features_vectorizer = SampleVectorizer(features)
    
    return features_vectorizer
