# -*- coding: utf-8 -*-

"""
Defines sample features vectorizer classes where samples are instances of MentionPair Samples classes
"""

__all__ = ["MentionPairSampleFeaturesVectorizer",
           "ExtendedMentionPairSampleFeaturesVectorizer", 
           "SuperExtendedMentionPairSampleFeaturesVectorizer",
           "_BaseMentionPairSampleFeaturesVectorizer", 
           "_BaseExtendedMentionPairSampleFeaturesVectorizer", 
           ]

import abc
from collections import OrderedDict
import itertools

from .base import _SampleFeaturesVectorizer
from ..ml_features.hierarchy import create_hierarchy_from_factory_configuration
from cortex.tools import (_create_simple_get_parameter_value_from_configuration, 
                          _create_simple_set_parameter_value_in_configuration, 
                          Mapping, deepcopy_mapping)


WITH_SINGLETON_FEATURES_DEFAULT_VALUE = False
with_singleton_features_attribute_data = ("with_singleton_features",
                                          (_create_simple_get_parameter_value_from_configuration("with_singleton_features", 
                                                                                                 default=WITH_SINGLETON_FEATURES_DEFAULT_VALUE, 
                                                                                                 postprocess_fct=bool),
                                           _create_simple_set_parameter_value_in_configuration("with_singleton_features"))
                                          )
WITH_ANAPHORICITY_FEATURES_DEFAULT_VALUE = False
with_anaphoricity_features_attribute_data = ("with_anaphoricity_features",
                                             (_create_simple_get_parameter_value_from_configuration("with_anaphoricity_features", 
                                                                                                    default=WITH_ANAPHORICITY_FEATURES_DEFAULT_VALUE, 
                                                                                                    postprocess_fct=bool),
                                              _create_simple_set_parameter_value_in_configuration("with_anaphoricity_features"))
                                             )
QUANTIZE_DEFAULT_VALUE = False
quantize_attribute_data = ("quantize",
                         (_create_simple_get_parameter_value_from_configuration("quantize", 
                                                                                 default=QUANTIZE_DEFAULT_VALUE, 
                                                                                 postprocess_fct=bool),
                          _create_simple_set_parameter_value_in_configuration("quantize"))
                         )
'''
STRICT_DEFAULT_VALUE = False
strict_attribute_data = ("strict",
                         (_create_simple_get_parameter_value_from_configuration("strict", 
                                                                                 default=STRICT_DEFAULT_VALUE, 
                                                                                 postprocess_fct=bool),
                          _create_simple_set_parameter_value_in_configuration("strict"))
                         )
'''                               
class _BaseMentionPairSampleFeaturesVectorizer(_SampleFeaturesVectorizer):
    """ Base class for classes whose instances' role is to be able to vectorize MentionPairSample 
    instances, and whose instances must be able to be parametrized during their initialization.
    
    Arguments:
        with_singleton_features: boolean, specifies whether or not to include specific 'singleton' features 
            when defining the vectorizer
        
        with_anaphoricity_features: boolean, specifies whether or not to include specific 'anaphoricity' features 
            when defining the vectorizer
        
        quantize: boolean; whether or not quantize some features that can be quantized, when defining 
            the vectorizer
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        with_singleton_features: boolean, the 'with_singleton_features' value that the instance has 
            been initialized with
        
        with_anaphoricity_features: boolean, the 'with_anaphoricity_features' value that the instance 
            has been initialized with
        
        quantize: boolean; the 'quantize' value that the instance has been initialized with
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    __INIT_PARAMETERS = OrderedDict((with_singleton_features_attribute_data, with_anaphoricity_features_attribute_data, 
                                    quantize_attribute_data, 
                                    #strict_attribute_data,
                                    ))
    _INIT_PARAMETERS = OrderedDict(itertools.chain(_SampleFeaturesVectorizer._INIT_PARAMETERS.items(), __INIT_PARAMETERS.items()))
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple(__INIT_PARAMETERS.keys()) + ("_features_vectorizer",)\
                                                 + _SampleFeaturesVectorizer._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = _SampleFeaturesVectorizer._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION+\
                                            tuple(__INIT_PARAMETERS.keys())
    
    @abc.abstractmethod
    def __init__(self, with_singleton_features=WITH_SINGLETON_FEATURES_DEFAULT_VALUE, 
                 with_anaphoricity_features=WITH_ANAPHORICITY_FEATURES_DEFAULT_VALUE, 
                 quantize=QUANTIZE_DEFAULT_VALUE, 
                 #strict=STRICT_DEFAULT_VALUE, 
                 configuration=None):
        # Check input values
        # Init parent
        super().__init__(configuration=configuration)
        # Process input values
        # Assign attribute values
        self.with_singleton_features = with_singleton_features
        self.with_anaphoricity_features = with_anaphoricity_features
        self.quantize = quantize
        #self.strict = strict
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        (with_singleton_features, with_anaphoricity_features, 
         quantize, #strict
         ) = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        kwargs["with_singleton_features"] = with_singleton_features
        kwargs["with_anaphoricity_features"] = with_anaphoricity_features
        kwargs["quantize"] = quantize
        #kwargs["strict"] = strict
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, with_singleton_features=WITH_SINGLETON_FEATURES_DEFAULT_VALUE, 
                              with_anaphoricity_features=WITH_ANAPHORICITY_FEATURES_DEFAULT_VALUE, 
                              quantize=QUANTIZE_DEFAULT_VALUE, 
                              #strict=STRICT_DEFAULT_VALUE,
                              ):
        """ Create a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Cf the documentation of the class of know which argument(s) to use.
        
        Returns:
            a Mapping instance
        """
        configuration = super().define_configuration()
        configuration["with_singleton_features"] = with_singleton_features
        configuration["with_anaphoricity_features"] = with_anaphoricity_features
        configuration["quantize"] = quantize
        #configuration["strict"] = strict
        return configuration



PAIR_HIERARCHY_DEFAULT_VALUE_FACTORY_CONFIGURATION = Mapping({"name": "pairnone"})
pair_hierarchy_attribute_data = ("pair_hierarchy",
                                 (_create_simple_get_parameter_value_from_configuration("pair_hierarchy", 
                                                                                        default=lambda: deepcopy_mapping(PAIR_HIERARCHY_DEFAULT_VALUE_FACTORY_CONFIGURATION), 
                                                                                        postprocess_fct=create_hierarchy_from_factory_configuration), 
                                  lambda instance: setattr(instance.configuration, "pair_hierarchy", instance.pair_hierarchy.factory_configuration))
                                 )
class MentionPairSampleFeaturesVectorizer(_BaseMentionPairSampleFeaturesVectorizer):
    """ Class whose instances' role is to be able to vectorize MentionPairSample instances corresponding 
    to a pair of true mentions (as opposed to a pair made of the NULL_MENTION and a true mention), 
    and whose instances must be able to be parametrized during their initialization.
    
    Arguments:
        pair_hierarchy: a _PairHierarchy child class instance, or None; used to potentially extend 
            the feature space by concatenating the same feature space several time, in function of 
            categorical values that are defined on pairs of mentions, and computed using the pair hierarchy 
            object. Cf cortex.learning.ml_features.hierarchy. If None, a PairNoneHierarchy class 
            instance will be used.
    
        with_singleton_features: boolean, specifies whether or not to include specific 'singleton' features 
            when defining the vectorizer
        
        with_anaphoricity_features: boolean, specifies whether or not to include specific 'anaphoricity' features 
            when defining the vectorizer
        
        quantize: boolean; whether or not quantize some features that can be quantized, when defining 
            the vectorizer
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        pair_hierarchy: the _PairHierarchy child class instance used by this instance
        
        with_singleton_features: boolean, the 'with_singleton_features' value that the instance has 
            been initialized with
        
        with_anaphoricity_features: boolean, the 'with_anaphoricity_features' value that the instance 
            has been initialized with
        
        quantize: boolean; the 'quantize' value that the instance has been initialized with
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    __INIT_PARAMETERS = OrderedDict((pair_hierarchy_attribute_data,))
    _INIT_PARAMETERS = OrderedDict(itertools.chain(_BaseMentionPairSampleFeaturesVectorizer._INIT_PARAMETERS.items(), __INIT_PARAMETERS.items()))
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple(__INIT_PARAMETERS.keys())\
                                                 + _BaseMentionPairSampleFeaturesVectorizer._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = _BaseMentionPairSampleFeaturesVectorizer._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION+\
                                            tuple(__INIT_PARAMETERS.keys())
    
    def __init__(self, pair_hierarchy=None, with_singleton_features=WITH_SINGLETON_FEATURES_DEFAULT_VALUE, 
                 with_anaphoricity_features=WITH_ANAPHORICITY_FEATURES_DEFAULT_VALUE, 
                 quantize=QUANTIZE_DEFAULT_VALUE, 
                 #strict=STRICT_DEFAULT_VALUE, 
                 configuration=None):
        # Check input values
        # Init parent
        super().__init__(with_singleton_features=with_singleton_features, 
                         with_anaphoricity_features=with_anaphoricity_features, 
                         quantize=quantize, #strict=strict, 
                         configuration=configuration)
        # Process input values
        if pair_hierarchy is None:
            pair_hierarchy = create_hierarchy_from_factory_configuration(deepcopy_mapping(PAIR_HIERARCHY_DEFAULT_VALUE_FACTORY_CONFIGURATION))
        from cortex.learning.ml_features.mention_pair_sample_features_vectorizer import _create_mention_pair_sample_feature_vectorizer
        create_mention_pair_sample_feature_vectorizer_fct = _create_mention_pair_sample_feature_vectorizer
        
        # Assign attribute values
        self.pair_hierarchy = pair_hierarchy
        self._features_vectorizer = create_mention_pair_sample_feature_vectorizer_fct(self.pair_hierarchy, 
                                                                                self.with_singleton_features, 
                                                                                self.with_anaphoricity_features, 
                                                                                self.quantize,
                                                                                #self.strict
                                                                                True
                                                                                )
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        pair_hierarchy, = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        kwargs["pair_hierarchy"] = pair_hierarchy
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, pair_hierarchy_factory_configuration=None, 
                              with_singleton_features=WITH_SINGLETON_FEATURES_DEFAULT_VALUE, 
                              with_anaphoricity_features=WITH_ANAPHORICITY_FEATURES_DEFAULT_VALUE, 
                              quantize=QUANTIZE_DEFAULT_VALUE, 
                              #strict=STRICT_DEFAULT_VALUE,
                              ):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Cf the documentation of the class of know which argument(s) to use.
        
        Returns:
            a Mapping instance
        """
        if pair_hierarchy_factory_configuration is None:
            pair_hierarchy_factory_configuration = deepcopy_mapping(PAIR_HIERARCHY_DEFAULT_VALUE_FACTORY_CONFIGURATION)
        configuration = super().define_configuration()
        configuration["pair_hierarchy"] = pair_hierarchy_factory_configuration
        configuration["with_singleton_features"] = with_singleton_features
        configuration["with_anaphoricity_features"] = with_anaphoricity_features
        configuration["quantize"] = quantize
        #configuration["strict"] = strict
        return configuration


MENTION_HIERARCHY_DEFAULT_VALUE_FACTORY_CONFIGURATION = Mapping({"name": "mentionnone"})
mention_hierarchy_attribute_data = ("mention_hierarchy",
                                     (_create_simple_get_parameter_value_from_configuration("mention_hierarchy", default=lambda: deepcopy_mapping(MENTION_HIERARCHY_DEFAULT_VALUE_FACTORY_CONFIGURATION), 
                                                                                            postprocess_fct=create_hierarchy_from_factory_configuration),
                                      lambda instance: setattr(instance.configuration, "mention_hierarchy", instance.mention_hierarchy.factory_configuration))
                                     )
class _BaseExtendedMentionPairSampleFeaturesVectorizer(_BaseMentionPairSampleFeaturesVectorizer):
    """ Base class for classes whose instances' role is to be able to vectorize MentionPairSample 
    instances corresponding to pairs of mentions where the first mention of the pair can be either 
    a true mention, or the NULL_MENTION, 
    and whose instances must be able to be parametrized during their initialization.
    
    Arguments:
        pair_hierarchy: a _PairHierarchy child class instance, or None; used to potentially extend 
            the feature space by concatenating the same feature space several time, in function of 
            categorical values that are defined on pairs of mentions, and computed using the hierarchy object. 
            Cf cortex.learning.ml_features.hierarchy. If None, a PairNoneHierarchy class instance will be used.
        
        mention_hierarchy: a _MentionHierarchy child class instance, or None; used to potentially extend 
            the feature space dedicated to encode single mention based samples, by concatenating the same 
            feature subspace several time, in function of categorical values that are defined on single 
            mentions, and computed using the mention hierarchy object. 
            Cf cortex.learning.ml_features.hierarchy. If None, a MentionNoneHierarchy class instance will be used.
        
        with_singleton_features: boolean, specifies whether or not to include specific 'singleton' features 
            when defining the vectorizer
        
        with_anaphoricity_features: boolean, specifies whether or not to include specific 'anaphoricity' features 
            when defining the vectorizer
        
        quantize: boolean; whether or not quantize some features that can be quantized, when defining 
            the vectorizer
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        pair_hierarchy: the _PairHierarchy child class instance used by this instance
        
        mention_hierarchy: the _MentionHierarchy child class instance used by this instance
        
        with_singleton_features: boolean, the 'with_singleton_features' value that the instance has 
            been initialized with
        
        with_anaphoricity_features: boolean, the 'with_anaphoricity_features' value that the instance 
            has been initialized with
        
        quantize: boolean; the 'quantize' value that the instance has been initialized with
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    __INIT_PARAMETERS = OrderedDict((pair_hierarchy_attribute_data, mention_hierarchy_attribute_data))
    _INIT_PARAMETERS = OrderedDict(itertools.chain(_BaseMentionPairSampleFeaturesVectorizer._INIT_PARAMETERS.items(), 
                                                   __INIT_PARAMETERS.items()
                                                   )
                                   )
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple(__INIT_PARAMETERS.keys())\
                                                 + _BaseMentionPairSampleFeaturesVectorizer._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = _BaseMentionPairSampleFeaturesVectorizer._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION+\
                                            tuple(__INIT_PARAMETERS.keys())
    
    def __init__(self, pair_hierarchy=None, mention_hierarchy=None, 
                 with_singleton_features=WITH_SINGLETON_FEATURES_DEFAULT_VALUE, 
                 with_anaphoricity_features=WITH_ANAPHORICITY_FEATURES_DEFAULT_VALUE, 
                 quantize=QUANTIZE_DEFAULT_VALUE, 
                 #strict=STRICT_DEFAULT_VALUE, 
                 configuration=None):
        # Check input values
        # Init parent
        super().__init__(with_singleton_features=with_singleton_features, 
                         with_anaphoricity_features=with_anaphoricity_features, 
                         quantize=quantize, #strict=strict, 
                         configuration=configuration)
        # Process input values
        if pair_hierarchy is None:
            pair_hierarchy = create_hierarchy_from_factory_configuration(deepcopy_mapping(PAIR_HIERARCHY_DEFAULT_VALUE_FACTORY_CONFIGURATION))
        if mention_hierarchy is None:
            mention_hierarchy = create_hierarchy_from_factory_configuration(deepcopy_mapping(MENTION_HIERARCHY_DEFAULT_VALUE_FACTORY_CONFIGURATION))
        create_extended_mention_pair_sample_feature_vectorizer_fct = self.get_sample_feature_vectorizer_creation_fct()
        # Assign attribute values
        self.pair_hierarchy = pair_hierarchy
        self.mention_hierarchy = mention_hierarchy
        self._features_vectorizer = create_extended_mention_pair_sample_feature_vectorizer_fct(self.pair_hierarchy, 
                                                                                         self.mention_hierarchy, 
                                                                                         self.with_singleton_features, 
                                                                                         self.with_anaphoricity_features, 
                                                                                         self.quantize,
                                                                                         #self.strict
                                                                                         True
                                                                                         )
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        pair_hierarchy, mention_hierarchy = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        kwargs["pair_hierarchy"] = pair_hierarchy
        kwargs["mention_hierarchy"] = mention_hierarchy
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, pair_hierarchy_factory_configuration=None, mention_hierarchy_factory_configuration=None, 
                              with_singleton_features=WITH_SINGLETON_FEATURES_DEFAULT_VALUE, 
                              with_anaphoricity_features=WITH_ANAPHORICITY_FEATURES_DEFAULT_VALUE, 
                              quantize=QUANTIZE_DEFAULT_VALUE, 
                              #strict=STRICT_DEFAULT_VALUE,
                              ):
        """ Create a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Cf the documentation of the class of know which argument(s) to use.
        
        Returns:
            a Mapping instance
        """
        if pair_hierarchy_factory_configuration is None:
            pair_hierarchy_factory_configuration = deepcopy_mapping(PAIR_HIERARCHY_DEFAULT_VALUE_FACTORY_CONFIGURATION)
        if mention_hierarchy_factory_configuration is None:
            mention_hierarchy_factory_configuration = deepcopy_mapping(MENTION_HIERARCHY_DEFAULT_VALUE_FACTORY_CONFIGURATION)
        configuration = super().define_configuration()
        configuration["pair_hierarchy"] = pair_hierarchy_factory_configuration
        configuration["mention_hierarchy"] = mention_hierarchy_factory_configuration
        configuration["with_singleton_features"] = with_singleton_features
        configuration["with_anaphoricity_features"] = with_anaphoricity_features
        configuration["quantize"] = quantize
        #configuration["strict"] = strict
        return configuration
    
    @classmethod
    @abc.abstractmethod
    def get_sample_feature_vectorizer_creation_fct(cls):
        pass


class ExtendedMentionPairSampleFeaturesVectorizer(_BaseExtendedMentionPairSampleFeaturesVectorizer):
    """ Class whose instances' role is to be able to vectorize MentionPairSample 
    instances corresponding to pairs of mentions where the first mention of the pair can be either 
    a true mention, or the NULL_MENTION, and whose instances must be able to be parametrized during 
    their initialization.
    
    Uses the plain 'extended mention pair features' version (Cf 
    'cortex.learning.ml_features.extended_mention_pair_sample_features_vectorizer').
    
    Arguments:
        pair_hierarchy: a _PairHierarchy child class instance, or None; used to potentially extend 
            the feature space by concatenating the same feature space several time, in function of 
            categorical values that are defined on pairs of mentions, and computed using the hierarchy object. 
            Cf cortex.learning.ml_features.hierarchy. If None, a PairNoneHierarchy class instance will be used.
        
        mention_hierarchy: a _MentionHierarchy child class instance, or None; used to potentially extend 
            the feature space dedicated to encode single mention based samples, by concatenating the same 
            feature subspace several time, in function of categorical values that are defined on single 
            mentions, and computed using the mention hierarchy object. Cf cortex.learning.ml_features.hierarchy. 
            If None, a MentionNoneHierarchy class instance will be used.
        
        with_singleton_features: boolean, specifies whether or not to include specific 'singleton' features 
            when defining the vectorizer
        
        with_anaphoricity_features: boolean, specifies whether or not to include specific 'anaphoricity' features 
            when defining the vectorizer
        
        quantize: boolean; whether or not quantize some features that can be quantized, when defining 
            the vectorizer
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        pair_hierarchy: the _PairHierarchy child class instance used by this instance
        
        mention_hierarchy: the _MentionHierarchy child class instance used by this instance
        
        with_singleton_features: boolean, the 'with_singleton_features' value that the instance has 
            been initialized with
        
        with_anaphoricity_features: boolean, the 'with_anaphoricity_features' value that the instance 
            has been initialized with
        
        quantize: boolean; the 'quantize' value that the instance has been initialized with
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    @classmethod
    def get_sample_feature_vectorizer_creation_fct(cls):
        """ Fetches the function needed to create the parametrized SampleVectorizer instance that this 
        class' instances will wrap around.
        """
        from cortex.learning.ml_features.extended_mention_pair_sample_features_vectorizer import _create_extended_mention_pair_sample_feature_vectorizer
        return _create_extended_mention_pair_sample_feature_vectorizer


class SuperExtendedMentionPairSampleFeaturesVectorizer(_BaseExtendedMentionPairSampleFeaturesVectorizer):
    """ Class whose instances' role is to be able to vectorize MentionPairSample 
    instances corresponding to pairs of mentions where the first mention of the pair can be either 
    a true mention, or the NULL_MENTION, and whose instances must be able to be parametrized during 
    their initialization.
    
    Uses the plain 'super extended mention pair features' version (Cf 
    'cortex.learning.ml_features.super_extended_mention_pair_sample_features_vectorizer').
    
    Arguments:
        pair_hierarchy: a _PairHierarchy child class instance, or None; used to potentially extend 
            the feature space by concatenating the same feature space several time, in function of 
            categorical values that are defined on pairs of mentions, and computed using the hierarchy object. 
            Cf cortex.learning.ml_features.hierarchy. If None, a PairNoneHierarchy class instance will be used.
        
        mention_hierarchy: a _MentionHierarchy child class instance, or None; used to potentially extend 
            the feature space dedicated to encode single mention based samples, by concatenating the same 
            feature subspace several time, in function of categorical values that are defined on single 
            mentions, and computed using the mention hierarchy object. 
            Cf cortex.learning.ml_features.hierarchy. If None, a MentionNoneHierarchy class instance will be used.
        
        with_singleton_features: boolean, specifies whether or not to include specific 'singleton' features 
            when defining the vectorizer
        
        with_anaphoricity_features: boolean, specifies whether or not to include specific 'anaphoricity' features 
            when defining the vectorizer
        
        quantize: boolean; whether or not quantize some features that can be quantized, when defining 
            the vectorizer
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        pair_hierarchy: the _PairHierarchy child class instance used by this instance
        
        mention_hierarchy: the _MentionHierarchy child class instance used by this instance
        
        with_singleton_features: boolean, the 'with_singleton_features' value that the instance has 
            been initialized with
        
        with_anaphoricity_features: boolean, the 'with_anaphoricity_features' value that the instance 
            has been initialized with
        
        quantize: boolean; the 'quantize' value that the instance has been initialized with
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    @classmethod
    def get_sample_feature_vectorizer_creation_fct(cls):
        """ Fetches the function needed to create the parametrized SampleVectorizer instance that this 
        class' instances will wrap around.
        """
        from cortex.learning.ml_features.super_extended_mention_pair_sample_features_vectorizer import _create_super_extended_mention_pair_sample_feature_vectorizer
        return _create_super_extended_mention_pair_sample_feature_vectorizer


sample_features_vectorizer_names_collection_and_sample_features_vectorizer_class_pairs = ((("mentionpair", MentionPairSampleFeaturesVectorizer.__name__), MentionPairSampleFeaturesVectorizer),
                                                                                          (("extendedmentionpair", ExtendedMentionPairSampleFeaturesVectorizer.__name__), ExtendedMentionPairSampleFeaturesVectorizer),
                                                                                          (("superextendedmentionpair", SuperExtendedMentionPairSampleFeaturesVectorizer.__name__), SuperExtendedMentionPairSampleFeaturesVectorizer),
                                                                                          )
sample_features_vectorizer_name2sample_features_vectorizer_class = OrderedDict((name, class_) for names, class_ in sample_features_vectorizer_names_collection_and_sample_features_vectorizer_class_pairs for name in names)
