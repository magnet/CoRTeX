# -*- coding: utf-8 -*-

"""
Defines classes representing a Vectorizer, but whose instantiation operation can be configured
"""

"""
Available submodule(s)
----------------------
base
    Defines base sample features vectorizer classes

pairs
    Defines sample features vectorizer classes where samples are instances of MentionPair Samples classes

mock
    Defines simple sample features vectorizer class useful for mocking in tests
"""

__all__ = ["MentionPairSampleFeaturesVectorizer", 
           "ExtendedMentionPairSampleFeaturesVectorizer", 
           "SuperExtendedMentionPairSampleFeaturesVectorizer",
           "MockSampleFeaturesVectorizer",
           "create_sample_features_vectorizer", 
           "create_sample_features_vectorizer_from_factory_configuration", 
           "generate_sample_features_vectorizer_factory_configuration", 
           "get_normalized_vectorizer_factory_configuration",
           ]



from collections import OrderedDict
import itertools

from .pairs import (sample_features_vectorizer_name2sample_features_vectorizer_class as coref_sample_features_vectorizer_map,
                    _SampleFeaturesVectorizer, 
                     MentionPairSampleFeaturesVectorizer, 
                     ExtendedMentionPairSampleFeaturesVectorizer, 
                     SuperExtendedMentionPairSampleFeaturesVectorizer,
                     )
from .mock import (sample_features_vectorizer_name2sample_features_vectorizer_class as mock_sample_features_vectorizer_map,
                   MockSampleFeaturesVectorizer,
                   )
from cortex.tools import (define_check_item_name_function, define_create_function, 
                          define_generate_factory_configuration_function, 
                          define_create_from_factory_configuration_function,
                          Mapping,
                          )


sample_features_vectorizer_name2sample_features_vectorizer_class = OrderedDict(itertools.chain(coref_sample_features_vectorizer_map.items(), 
                                                                                               mock_sample_features_vectorizer_map.items(),
                                                                                               )
                                                                               )

# High level functions
_check_sample_features_vectorizer_name =\
 define_check_item_name_function(sample_features_vectorizer_name2sample_features_vectorizer_class, 
                                 "sample_features_vectorizer")
 
create_sample_features_vectorizer =\
 define_create_function(sample_features_vectorizer_name2sample_features_vectorizer_class, 
                        "sample_features_vectorizer")
 
create_sample_features_vectorizer_from_factory_configuration =\
 define_create_from_factory_configuration_function(sample_features_vectorizer_name2sample_features_vectorizer_class, 
                                                   "sample_features_vectorizer")
 
generate_sample_features_vectorizer_factory_configuration =\
 define_generate_factory_configuration_function(sample_features_vectorizer_name2sample_features_vectorizer_class, 
                                                "sample_features_vectorizer")


def get_normalized_vectorizer_factory_configuration(vectorizer_factory_configuration):
    """ Creates a normalized version of a vectorizer factory configuration; that is to say, a factory 
    configuration that unequivocally defines a specific resolver instance, and that can be considered 
    constant, not polluted by potential unwelcome, useless other (field; value) pairs that play no role 
    in specifying the vectorizer instance.
    
    Args:
        vectorizer_factory_configuration: mapping

    Returns:
        a Mapping instance, the normalized version of the input factory configuration
    """
    vectorizer = create_sample_features_vectorizer_from_factory_configuration(vectorizer_factory_configuration)
    vectorizer.configuration = Mapping()
    vectorizer._set_configuration_values()
    normalized_configuration = vectorizer.factory_configuration
    return normalized_configuration
