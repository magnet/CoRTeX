# -*- coding: utf-8 -*-

"""
Defines filters that combine the effect of several filters.
"""

__all__ = ["FiltersCombination",
           ]

from cortex.utils import EqualityMixIn
from cortex.tools import ConfigurationMixIn

class FiltersCombination(ConfigurationMixIn, EqualityMixIn):
    """ Class implementing a filter made from the successive use of several other filter class instances.
    As such, the order in which the filters are specified is important.
    
    Arguments:
        filters: the collection of filters to apply in succession. The order in which they are 
            specified in this collection is the order in which they will be applied.
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        filters: the collection of filters to apply in successive order
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = ("filters", "configuration",)
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = ("filters",)
    
    def __init__(self, filters, configuration=None):
        # Check input values
        if not filters:
            raise ValueError("Input filter collection is empty, need at least one filter.")
        # Initialize using parent class
        ConfigurationMixIn.__init__(self, configuration=configuration)
        # Assign attribute values
        self.filters = tuple(filters)
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self._INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        filters, = cls._get_initialization_parameters_values_from_configuration(cls._INIT_PARAMETERS, configuration)
        args = args + (filters,)
        kwargs["configuration"] = configuration
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, filter_configurations):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Args:
            filter_configurations: collection of configurations of filter

        Returns:
            a Mapping instance
        """
        configuration = super().define_configuration()
        configuration["filters"] = filter_configurations
        return configuration
    
    def accept(self, *args):
        """ Returns whether or not the input argument(s) shall be accepted / pass through the filter, 
        or not.
        
        Args:
            mention: Mention instance

        Returns:
            a boolean
        """
        # Accept iff all filters accept
        for filter_ in self.filters:
            if filter_.refuse(*args):
                return False
        return True

    def refuse(self, *args):
        """ Returns whether or not the input mention shall be refused / not pass through the filter, 
        or if it can.
        
        Args:
            mention: a Mention instance

        Returns:
            a boolean
        """
        return not self.accept(*args)
