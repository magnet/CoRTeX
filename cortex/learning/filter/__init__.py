# -*- coding: utf-8 -*-

"""
Defines utilities to filter a set of specific class instances, possibly based on textual data they 
may hold
"""

"""
Available submodule(s)
----------------------
mentions
    Defines filters that operates on Mention instances

pairs
    Defines filters that operates on Pairs instances

combination
    Defines filters that combine the effect of several filters
"""