# -*- coding: utf-8 -*-

"""
Defines filters that operates on Pairs instances.
"""

__all__ = ["AcceptAllPairFilter", 
           "RecallPairFilter", 
           "DistancePairFilter", 
           "SameEntityTypePairFilter", 
           "SameGenderPairFilter", 
           "SameNumberPairFilter",
           "PairFiltersCombination",
           "_PairFilter", 
           "_MaxMentionDistancePairFilter",  
           "create_pair_filter",
           "create_pair_filter_from_factory_configuration",
           "generate_pair_filter_factory_configuration",
           ]

import abc
from collections import OrderedDict

from .combination import FiltersCombination
from cortex.api.markable import NULL_MENTION
from cortex.parameters.mention_data_tags import UNKNOWN_VALUE_TAG
from cortex.utils import EqualityMixIn
from cortex.tools import (ConfigurationMixIn, _create_simple_get_parameter_value_from_configuration, 
                            _create_simple_set_parameter_value_in_configuration)
from cortex.tools import (define_check_item_name_function, 
                          define_create_function, 
                          define_generate_factory_configuration_function, 
                          define_create_from_factory_configuration_function,
                          )

from cortex.config import language
from cortex.languages import create_get_language_dependent_object_fct
get_language_dependent_object = create_get_language_dependent_object_fct(language)

MENTION_FEATURES = get_language_dependent_object("mention_features")


class _PairFilter(ConfigurationMixIn, EqualityMixIn, metaclass=abc.ABCMeta):
    """ Base class for filter classes that filter pairs of Mention instances.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = ("configuration",)
    
    def __init__(self, configuration=None):
        # Initialize using parent class
        ConfigurationMixIn.__init__(self, configuration=configuration)
    
    @abc.abstractmethod
    def accept(self, antecedent_mention, subsequent_mention):
        """ Specifies whether or not the input mention pair shall be accepted / pass through the filter, 
        or not.
        
        Args:
            antecedent_mention: a Mention instance, the first mention of the pair in reading order
            subsequent_mention: a Mention instance, the second mention of the pair in reading order

        Returns:
            a boolean
        """
        pass

    def refuse(self, antecedent_mention, subsequent_mention):
        """ Specifies whether or not the input mention pair shall be refused / not pass through the filter, 
        or if it can.
        
        Args:
            antecedent_mention: a Mention instance, the first mention of the pair in reading order
            subsequent_mention: a Mention instance, the second mention of the pair in reading order

        Returns:
            boolean
        """
        return not self.accept(antecedent_mention, subsequent_mention)

    def filter_pairs(self, pairs):
        """ Creates a generator over filtered mention pairs from the input collection.
        
        Args:
            mention: a collection of Mention instances pairs

        Returns:
            Mention instance collection
        """
        for pair in pairs:
            antecedent_mention, subsequent_mention = pair
            if self.accept(antecedent_mention, subsequent_mention):
                yield pair


class AcceptAllPairFilter(_PairFilter):
    """ Class representing an accept-all mention pairs filter.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def accept(self, antecedent_mention, subsequent_mention):
        """ Specifies whether or not the input mention pair shall be accepted / pass through the filter, 
        or not.
        
        Args:
            antecedent_mention: a Mention instance, the first mention of the pair in reading order
            subsequent_mention: a Mention instance, the second mention of the pair in reading order

        Returns:
            a boolean
        """
        return True


max_mention_distance_attribute_data = ("max_mention_distance",
                                       (_create_simple_get_parameter_value_from_configuration("max_mention_distance", postprocess_fct=int),
                                        _create_simple_set_parameter_value_in_configuration("max_mention_distance"))
                                       )
class _MaxMentionDistancePairFilter(_PairFilter):
    """ Base class for classes representing filters that reject mention pairs notably if the two 
    mentions are too far apart in the document they originate from.
    
    The distance between two mentions is defined as the number of mentions between them, them 
    excluded, minus one.
    
    Arguments:
        max_mention_distance: an int, mention distance value above which a pair of mentions will be 
            rejected by the filter
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        max_mention_distance: int, the 'max_mention_distance' value that the instance has been 
            initialized with
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    __INIT_PARAMETERS = OrderedDict((max_mention_distance_attribute_data,))
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple(__INIT_PARAMETERS.keys()) +\
                                                 _PairFilter._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = _PairFilter._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION +\
                                            tuple(__INIT_PARAMETERS.keys())
    
    def __init__(self, max_mention_distance, configuration=None):
        super().__init__(configuration=configuration)
        # Check input values
        if not max_mention_distance:
            message = "Input 'max_mention_distance' parameter's value is incorrect, need a non-zero integer."
            raise ValueError(message)
        # Assign attribute values
        self.max_mention_distance = max_mention_distance
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extract *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        max_mention_distance, = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        args = args + (max_mention_distance,)
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, max_mention_distance):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Cf the documentation of the class of know which argument(s) to use.
        
        Returns:
            a Mapping instance
        """
        configuration = super().define_configuration()
        configuration["max_mention_distance"] = max_mention_distance
        return configuration



class RecallPairFilter(_MaxMentionDistancePairFilter):
    """ Class representing filters that reject pairs of mentions whose mentions meet all of a 
    collection of criteria.
    
    This collection of criteria is the 
    following: 
        - the antecedent mention is not the NULL MENTION
        - the mentions are located too far apart in the text
        - the antecedent mention is not an alias of the subsequent mention
        - the raw textual content of each mention's head is different, or at least one of them is None
        - the antecedent mention is not a pronoun, or there no morphosyntactic agreement between the 
          two mentions
        - the antecedent mention is neither a pronoun or a name, and the subsequent mention is not a 
          pronoun and there is no morphosyntactic agreement between the two mentions
        - there is no morphosyntactic agreement between the two mentions
        
    Another way to look at it is to say that such a filter accepts pairs of mentions whose mentions 
    meet at least one of the following criteria:
        - the antecedent mention is the NULL MENTION
        - the mentions are located close enough in the text
        - the antecedent mention is an alias of the subsequent mention
        - the raw textual content of each mention's head is equal, is not None 
        - the antecedent mention is a pronoun and there is a morphosyntactic agreement between the 
          two mentions
        - the antecedent mention is either a pronoun or a name, and the subsequent mention is a 
          pronoun and there is a morphosyntactic agreement between the two mentions
        - there is a morphosyntactic agreement between the two mentions
    
    The distance between two mentions is defined as the number of mentions between them, them 
    excluded, minus one.
    
    Arguments:
        max_mention_distance: int, mention distance value above which a pair of mentions will be 
            rejected by the filter
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        max_mention_distance: int, the 'max_mention_distance' value that the instance has been 
            initialized with
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def accept(self, antecedent_mention, subsequent_mention):
        """ Specifies whether or not the input mention pair shall be accepted / pass through the filter, 
        or not.
        
        Args:
            antecedent_mention: a Mention instance, the first mention of the pair in reading order
            subsequent_mention: a Mention instance, the second mention of the pair in reading order

        Returns:
            a boolean
        """
        # FIXME: what to do about all those legacy comments?
        if antecedent_mention is NULL_MENTION: # supports extended pair generators
            return True # TODO find nice predicates
        if MENTION_FEATURES.mention_distance(antecedent_mention, subsequent_mention) <= self.max_mention_distance:
            return True
        if MENTION_FEATURES.is_alias_of(antecedent_mention, subsequent_mention):
            return True
        if antecedent_mention.head_raw_text == subsequent_mention.head_raw_text is not None: #FIXME: what.
            return True
        # Is the following not redundant with the condition posed with the last if?
        if MENTION_FEATURES.is_expanded_pronoun(subsequent_mention) and MENTION_FEATURES.morph_agr(antecedent_mention, subsequent_mention): # add anymacy and speaker
            return True
        if (MENTION_FEATURES.is_expanded_pronoun(antecedent_mention) or MENTION_FEATURES.is_name(antecedent_mention))\
         and (MENTION_FEATURES.is_expanded_pronoun(subsequent_mention)) and MENTION_FEATURES.morph_agr(antecedent_mention, subsequent_mention):
            return True
        if MENTION_FEATURES.morph_agr(antecedent_mention, subsequent_mention):
            return True
        # add match head word
        # shallow discourse attribute match
        # If we did not return True up until now, since we are about to return False, if the mention still corefers, then we want to know which conditions we overlooked:
#        if antecedent_mention.corefers_with(subsequent_mention):
#            print "#####################################", antecedent_mention.get_document().get_id()
#            print "antecedent_mention text =", antecedent_mention.get_text(), "         head =", antecedent_mention.get_head_text(), "    gender =", antecedent_mention.get_gender(), "    number =", antecedent_mention.get_number()
#            print "subsequent_mention text =", subsequent_mention.get_text(), "         head =", subsequent_mention.get_head_text(), "    gender =", subsequent_mention.get_gender(), "    number =", subsequent_mention.get_number()
#            print "distance =", abs(antecedent_mention.get_index() - subsequent_mention.get_index())
#            print "antecedent_mention is pronoun =", antecedent_mention.is_expanded_pronoun()
#            print "antecedent_mention is name =", antecedent_mention.is_name()
#            print "subsequent_mention is pronoun =", subsequent_mention.is_expanded_pronoun()
#            print "alias =", antecedent_mention.is_alias_of(subsequent_mention)
#            print "morph agree =", antecedent_mention.morph_agr(subsequent_mention)
#            print "#####################################"
        return False


class DistancePairFilter(_MaxMentionDistancePairFilter):
    """ Class representing filters that reject pairs of mentions whose mentions are too far apart 
    in the text.
    
    The distance between two mentions is defined as the number of mentions between them, them 
    excluded, minus one.
    
    Arguments:
        max_mention_distance: int, mention distance value above which a pair of mentions will be 
            rejected by the filter
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        max_mention_distance: int, the 'max_mention_distance' value that the instance has been 
            initialized with
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """

    def accept(self, antecedent_mention, subsequent_mention):
        """ Specifies whether or not the input mention pair shall be accepted / pass through the filter, 
        or not.
        
        Args:
            antecedent_mention: a Mention instance, the first mention of the pair in reading order
            subsequent_mention: a Mention instance, the second mention of the pair in reading order
        
        Returns:
            a boolean
        """
        if antecedent_mention is NULL_MENTION: # supports extended pair generators
            return True
        if MENTION_FEATURES.mention_distance(antecedent_mention, subsequent_mention) <= self.max_mention_distance:
            return True
        return False


class SameEntityTypePairFilter(_PairFilter):
    """ Class representing filters that reject pairs of mentions whose mentions are both 
    characterized by a named entity, but of different respective types.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def accept(self, antecedent_mention, subsequent_mention):
        """ Specifies whether or not the input mention pair shall be accepted / pass through the filter, 
        or not.
        
        Args:
            antecedent_mention: Mention instance, the first mention of the pair in reading order
            subsequent_mention: Mention instance, the second mention of the pair in reading order

        Returns:
            boolean
        """
        if antecedent_mention is NULL_MENTION: # supports extended pair generators
            return True
        ne1 = antecedent_mention.named_entity
        ne2 = subsequent_mention.named_entity
        if ne1 is None or ne2 is None or ne1.type == ne2.type:
            return True
        return False


class SameGenderPairFilter(_PairFilter):
    """ Class representing filters that reject pairs of mentions whose mentions' gender's value are 
    known and distinct.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def accept(self, antecedent_mention, subsequent_mention):
        """ Specifies whether or not the input mention pair shall be accepted / pass through the filter, 
        or not.
        
        Args:
            antecedent_mention: a Mention instance, the first mention of the pair in reading order
            subsequent_mention: a Mention instance, the second mention of the pair in reading order

        Returns:
            a boolean
        """
        if antecedent_mention is NULL_MENTION: # supports extended pair generators
            return True
        gender1 = antecedent_mention.gender
        gender2 = subsequent_mention.gender
        if gender1 == UNKNOWN_VALUE_TAG or gender2 == UNKNOWN_VALUE_TAG or gender1 == gender2:
            return True
        return False


class SameNumberPairFilter(_PairFilter):
    """ Class representing filters that reject pairs of mentions whose mentions' grammatical number's 
    value are known and distinct
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def accept(self, antecedent_mention, subsequent_mention):
        """ Specifies whether or not the input mention pair shall be accepted / pass through the filter, 
        or not.
        
        Args:
            antecedent_mention: a Mention instance, the first mention of the pair in reading order
            subsequent_mention: a Mention instance, the second mention of the pair in reading order

        Returns:
            a boolean
        """
        if antecedent_mention is NULL_MENTION: # supports extended pair generators
            return True
        number1 = antecedent_mention.number
        number2 = subsequent_mention.number
        if number1 == UNKNOWN_VALUE_TAG or number2 == UNKNOWN_VALUE_TAG or number1 == number2:
            return True
        return False


class NotCopulaPairFilter(_PairFilter):
    """ Class representing filters that reject pairs of mentions whose antecedent mention is in a 
    copula with the subsequent mention
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
    """
    
    def accept(self, antecedent_mention, subsequent_mention):
        """ Specifies whether or not the input mention pair shall be accepted / pass through the filter, 
        or not.
        
        Args:
            antecedent_mention: a Mention instance, the first mention of the pair in reading order
            subsequent_mention: a Mention instance, the second mention of the pair in reading order

        Returns:
            a boolean
        """
        return not MENTION_FEATURES.in_copula_with(antecedent_mention, subsequent_mention)


''''
class StoyanovDistancePairFilter(_PairFilter):
    
    # TODO: this is the original version. To be improved.
    def accept(self, antecedent_mention, subsequent_mention):
        """ Specifies whether or not the input mention pair shall be accepted / pass through the filter, 
        or not.
        
        Args:
            antecedent_mention: Mention instance, the first mention of the pair in reading order
            subsequent_mention: Mention instance, the second mention of the pair in reading order

        Returns:
            boolean
        """
        if antecedent_mention is NULL_MENTION:
            return True
        l_text = antecedent_mention.raw_text.lower()
        r_text = subsequent_mention.raw_text.lower()
        l_head_text = antecedent_mention.head_raw_text.lower()
        r_head_text = subsequent_mention.head_raw_text.lower()
        sentence_distance = MENTION_FEATURES.sentence_distance(antecedent_mention, subsequent_mention)
        accept = False
        # proper name sentence_distance
        if MENTION_FEATURES.is_name(antecedent_mention) and MENTION_FEATURES.is_name(subsequent_mention) and sentence_distance <= 20:
            accept = True
        # definite noun phrase
        if r_text.startswith("the") and sentence_distance <= 6:
            accept = True
        # common noun phrase
        if MENTION_FEATURES.is_nominal(subsequent_mention):
            if MENTION_FEATURES.is_nominal(antecedent_mention):
                if l_head_text == r_head_text:
                    accept = True
            elif sentence_distance <= 2:
                accept = True
        # pronouns
        if MENTION_FEATURES.is_expanded_pronoun(subsequent_mention):
            if r_text in FIRST_PERSON_PRONOUNS:
                if MENTION_FEATURES.is_expanded_pronoun(antecedent_mention) and l_text in FIRST_PERSON_PRONOUNS and sentence_distance <= 20:
                    accept = True
            elif sentence_distance <= 2:
                accept = True
        # verbs
        if MENTION_FEATURES.is_verb(antecedent_mention):
            if r_text in ["it","this","that"] and sentence_distance <= 2:
                accept = True
        # sentence_distance
        if sentence_distance <= 10:
            accept = True
        return accept
'''




_pair_filter_names_collection_and_filter_class_pairs = ((("acceptall", AcceptAllPairFilter.__name__), AcceptAllPairFilter),
                                                       (("recall", RecallPairFilter.__name__), RecallPairFilter),
                                                       (("distance", DistancePairFilter.__name__), DistancePairFilter),
                                                       (("sameentitytype", SameEntityTypePairFilter.__name__), SameEntityTypePairFilter),
                                                       (("samegender", SameGenderPairFilter.__name__), SameGenderPairFilter),
                                                       (("samenumber", SameNumberPairFilter.__name__), SameNumberPairFilter),
                                                       (("notcopula", NotCopulaPairFilter.__name__), NotCopulaPairFilter),
                                                       )
_pair_filter_name2pair_filter_class = OrderedDict((name, class_) for names, class_ in _pair_filter_names_collection_and_filter_class_pairs for name in names)

_create_pair_filter_from_factory_configuration =\
 define_create_from_factory_configuration_function(_pair_filter_name2pair_filter_class, "pair_filter")
filters_attribute_data = ("filters",
                              (_create_simple_get_parameter_value_from_configuration("filters", 
                                                                                     postprocess_fct=lambda confs: tuple(_create_pair_filter_from_factory_configuration(conf) for conf in confs)),
                               lambda instance: setattr(instance.configuration, "filters", tuple(filter_.factory_configuration for filter_ in instance.filters))
                               )
                              )
class PairFiltersCombination(FiltersCombination):
    """ Class implementing a mention pair filter made from the successive use of several other 
    mention pair filter class instances.
    
    As such, the order in which the filters are specified is important.
    
    Arguments:
        filters: the collection of filters to apply in succession. The order in which they are 
            specified in this collection is the order in which they will be applied.
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        filters: the collection of filters to apply in successive order
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    _INIT_PARAMETERS = OrderedDict((filters_attribute_data,))
    
    @classmethod
    def define_configuration(cls, pair_filter_factory_configurations=None):
        """ Create a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Args:
            pair_filter_factory_configurations: collection of factory configurations of 
                mention pair filters

        Returns:
            a Mapping instance
        """
        if pair_filter_factory_configurations is None:
            pair_filter_factory_configurations = [AcceptAllPairFilter.define_factory_configuration()]
        return super().define_configuration(pair_filter_factory_configurations)
    
    def filter(self, pairs):
        """ Returns the filtered input collection of mention pairs, that is, the collection made from 
        the mention pairs that were accepted by the filter
        
        Args:
            pairs: a collection of Mention instance pairs

        Returns:
            a collection of Mention instance pairs
        """
        return tuple(pair for pair in pairs if self.accept(*pairs))


pair_filter_names_collection_and_filter_class_pairs = _pair_filter_names_collection_and_filter_class_pairs +\
                                                        ((("combination", PairFiltersCombination.__name__), PairFiltersCombination),
                                                         )
pair_filter_name2pair_filter_class = OrderedDict((name, class_) for names, class_ in pair_filter_names_collection_and_filter_class_pairs for name in names)


# High level functions
_check_pair_filter_name = define_check_item_name_function(pair_filter_name2pair_filter_class, "pair_filter")

create_pair_filter = define_create_function(pair_filter_name2pair_filter_class, "pair_filter")

create_pair_filter_from_factory_configuration =\
 define_create_from_factory_configuration_function(pair_filter_name2pair_filter_class, "pair_filter")
 
generate_pair_filter_factory_configuration =\
 define_generate_factory_configuration_function(pair_filter_name2pair_filter_class, "pair_filter")
