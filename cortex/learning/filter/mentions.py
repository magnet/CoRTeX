# -*- coding: utf-8 -*-

"""
Defines filters that operates on Mention instances
"""

__all__ = ["AcceptAllMentionFilter", 
           "NoVerbGramTypeMentionFilter", 
           "NonReferentialMentionFilter", 
           "MentionFiltersCombination",
           "_MentionFilter",
           "create_mention_filter",
           "create_mention_filter_from_factory_configuration",
           "generate_mention_filter_factory_configuration",
           ]


import abc
from collections import OrderedDict

from .combination import FiltersCombination
from cortex.api.document_buffer import DocumentBuffer
from cortex.parameters.mention_data_tags import VERB_GRAM_TYPE_TAG
from cortex.tools import (ConfigurationMixIn, _create_simple_get_parameter_value_from_configuration, 
                            _create_simple_set_parameter_value_in_configuration)
from cortex.tools import (define_check_item_name_function, define_create_function, 
                          define_generate_factory_configuration_function, 
                          define_create_from_factory_configuration_function,
                          )
from cortex.utils import EqualityMixIn

from cortex.config import language
from cortex.languages import create_get_language_dependent_object_fct
get_language_dependent_object = create_get_language_dependent_object_fct(language)

MENTION_FEATURES = get_language_dependent_object("mention_features")


class _MentionFilter(ConfigurationMixIn, EqualityMixIn, metaclass=abc.ABCMeta):
    """ Base class for filter classes that filter Mention instances.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = ("configuration", )
    
    def __init__(self, configuration=None):
        # Initialize using parent class
        ConfigurationMixIn.__init__(self, configuration=configuration)
    
    @abc.abstractmethod
    def accept(self, mention):
        """ Specifies whether or not the input mention shall be accepted / pass through the filter, 
        or not.
        
        Args:
            mention: a Mention instance

        Returns:
            a boolean
        """
        pass

    def refuse(self, mention):
        """ Specifies whether or not the input mention shall be refused / not pass through the filter, 
        or if it can.
        
        Args:
            mention: a Mention instance

        Returns:
            a boolean
        """
        return not self.accept(mention)

    def filter(self, mentions):
        """ Returns the filtered input collection of mentions, that is, the collection made from the 
        mention that were accepted by the filter
        
        Args:
            mention: a collection of Mention instances

        Returns:
            a collection of Mention instances
        """
        return tuple(mention for mention in mentions if self.accept(mention))
    
    def filter_document(self, document):
        """ Modifies the collection of mentions associated to the input Document instance so that it 
        corresponds to the result of the original mention collection after having been filtered by 
        the filter.
        
        Args:
            document: a Document instance

        Returns:
            the input Document instance, after that its 'mentions' attribute value has been 
            filtered, if it existed
        
        Note:
            If the input Document instance possesses a coreference partition, this partition will be 
            modified so that its universe corresponds to the set of mentions associated to the document 
            after being filtered. That is, all mentions that may have been removed by the filter, 
            will also be removed from the universe associated to the partition.
        """
        if document.mentions:
            to_remove_mention_extents = []
            for mention in document.mentions:
                if self.refuse(mention):
                    to_remove_mention_extents.append(mention.extent)
            if to_remove_mention_extents:
                document_buffer = DocumentBuffer(document)
                document_buffer.remove_mentions(to_remove_mention_extents)
                document = document_buffer.flush(strict=False, enforce_coreference_partition_consistency=True)
        return document



class AcceptAllMentionFilter(_MentionFilter):
    """ Class representing an accept-all Mention instances filter.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def accept(self, mention):
        """ Specifies whether or not the input mention shall be accepted / pass through the filter, or 
        not.
        
        Args:
            mention: a Mention instance

        Returns:
            a boolean
        """
        return True


class NoVerbGramTypeMentionFilter(_MentionFilter):
    """ Class representing a Mention instances filter that filters out mentions whose gram type 
    attribute value is that of a verbal gram type.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def accept(self, mention):
        """ Specifies whether or not the input mention shall be accepted / pass through the filter, or 
        not.
        
        Args:
            mention: a Mention instance

        Returns:
            a boolean
        """
        return not mention.gram_type == VERB_GRAM_TYPE_TAG



REFERENTIAL_PROBABILITY_THRESHOLD_DEFAULT_VALUE = 0.5
referential_probability_threshold_attribute_data = ("referential_probability_threshold",
                                                    (lambda configuration: float(type(configuration).get(configuration, 
                                                                                                         "referential_probability_threshold", 
                                                                                                         REFERENTIAL_PROBABILITY_THRESHOLD_DEFAULT_VALUE)),
                                                     _create_simple_set_parameter_value_in_configuration("referential_probability_threshold"))
                                                    )
class NonReferentialMentionFilter(_MentionFilter):
    """ Class defining a mention filter that accepts mentions that are (or are bet upon to be) 
    referential, using a probability threshold value to decide whether a Mention instance is 
    referential or not, from its referential probability attribute value.
    
    Only Mention instances with a referential probability value above the threshold value will be 
    accepted. If the Mention instance does not possess a defined referential probability value, it 
    will be accepted. 
    
    Arguments:
        referential_probability_threshold: float comprised between 0. and 1., the probability 
            threshold to use to filter out mentions 
    
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        referential_probability_threshold: boolean, the 'referential_probability_threshold' value 
            that the instance has been initialized with
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    __INIT_PARAMETERS = OrderedDict((referential_probability_threshold_attribute_data,))
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple(__INIT_PARAMETERS.keys()) +\
                                                 _MentionFilter._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = _MentionFilter._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION +\
                                            tuple(__INIT_PARAMETERS.keys())
    
    def __init__(self, referential_probability_threshold=REFERENTIAL_PROBABILITY_THRESHOLD_DEFAULT_VALUE, 
                 configuration=None):
        super().__init__(configuration=configuration)
        # Set attribute values
        self.referential_probability_threshold = referential_probability_threshold
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        referential_probability_threshold, = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        kwargs["referential_probability_threshold"] = referential_probability_threshold
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, referential_probability_threshold=REFERENTIAL_PROBABILITY_THRESHOLD_DEFAULT_VALUE):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Cf the documentation of the class of know which argument(s) to use.
        
        Returns:
            a Mapping instance
        """
        configuration = super().define_configuration()
        configuration["referential_probability_threshold"] = referential_probability_threshold
        return configuration
    
    def accept(self, mention):
        """ Specifies whether or not the input mention shall be accepted / pass through the filter, or 
        not.
        
        Args:
            mention: a Mention instance

        Returns:
            a boolean
        """
        return MENTION_FEATURES.is_referential(mention, self.referential_probability_threshold)




### Filter combination ###
_mention_filter_names_collection_and_filter_class_pairs = ((("acceptall", AcceptAllMentionFilter.__name__), AcceptAllMentionFilter),
                                                          (("nonreferential", NonReferentialMentionFilter.__name__), NonReferentialMentionFilter),
                                                          (("noverbgramtype", NoVerbGramTypeMentionFilter.__name__), NoVerbGramTypeMentionFilter),
                                                          )
_mention_filter_name2mention_filter_class = OrderedDict((name, class_) for names, class_ in _mention_filter_names_collection_and_filter_class_pairs for name in names)
_create_mention_filter_from_factory_configuration =\
 define_create_from_factory_configuration_function(_mention_filter_name2mention_filter_class, "mention_filter")
filters_attribute_data = ("filters",
                              (_create_simple_get_parameter_value_from_configuration("filters", 
                                                                                     postprocess_fct=lambda confs: tuple(_create_mention_filter_from_factory_configuration(conf) for conf in confs)),
                               lambda instance: setattr(instance.configuration, "filters", tuple(filter_.factory_configuration for filter_ in instance.filters))
                               )
                              )
class MentionFiltersCombination(FiltersCombination):
    """ Class implementing a mention filter made from the successive use of several other mention 
    filter class instances.
    
    As such, the order in which the filters are specified is important.
    
    Arguments:
        filters: the collection of filters to apply in succession. The order in which they are 
            specified in this collection is the order in which they will be applied.
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        filters: the collection of filters to apply in successive order
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    _INIT_PARAMETERS = OrderedDict((filters_attribute_data,))
    
    @classmethod
    def define_configuration(cls, mention_filter_factory_configurations=None):
        """ Create a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Args:
            mention_filter_factory_configurations: collection of factory configurations of 
                mention filters

        Returns:
            a Mapping instance
        """
        if mention_filter_factory_configurations is None:
            mention_filter_factory_configurations = [AcceptAllMentionFilter.define_factory_configuration()]
        return super().define_configuration(mention_filter_factory_configurations)
    
    def filter(self, mentions):
        """ Return the filtered input collection of mentions, that is, the collection made from the 
        mention that were accepted by the filter
        
        Args:
            mentions: a collection of Mention instances

        Returns:
            a collection of Mention instances
        """
        return tuple(mention for mention in mentions if self.accept(mention))



mention_filter_names_collection_and_filter_class_pairs = _mention_filter_names_collection_and_filter_class_pairs + \
                                                            ((("combination", MentionFiltersCombination.__name__), MentionFiltersCombination),
                                                             )
mention_filter_name2mention_filter_class = OrderedDict((name, class_) for names, class_ in mention_filter_names_collection_and_filter_class_pairs for name in names)


# High level functions
_check_mention_filter_name = define_check_item_name_function(mention_filter_name2mention_filter_class, "mention_filter")

create_mention_filter = define_create_function(mention_filter_name2mention_filter_class, "mention_filter")

create_mention_filter_from_factory_configuration =\
 define_create_from_factory_configuration_function(mention_filter_name2mention_filter_class, "mention_filter")
 
generate_mention_filter_factory_configuration =\
 define_generate_factory_configuration_function(mention_filter_name2mention_filter_class, "mention_filter")
