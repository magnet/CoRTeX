# -*- coding: utf-8 -*-

"""
Defines classes used to represent a sample that can be used in the context of a ML task
"""

__all__ = ["AnaphoricitySample", 
           "SingletonSample", 
           "MentionPairSample",
           "_Sample", 
           "_ClassificationSample", 
           "POSSIBLE_SAMPLE_TYPES",
           ]

import abc

from cortex.api.markable import NULL_MENTION
from .parameters import POS_CLASS, NEG_CLASS

ANAPHORICITY_SAMPLE_TYPE = "anaphoricity"
SINGLETON_SAMPLE_TYPE = "singleton"
MENTION_PAIR_SAMPLE_TYPE = "mention_pair"
POSSIBLE_SAMPLE_TYPES = set([ANAPHORICITY_SAMPLE_TYPE, SINGLETON_SAMPLE_TYPE, MENTION_PAIR_SAMPLE_TYPE])

class _Sample(object, metaclass=abc.ABCMeta):
    """ _Sample is a base class for object classes destined to represent a sample in the 
    frame of a Machine Learning process.
    """

class _ClassificationSample(_Sample, metaclass=abc.ABCMeta):
    """"A _ClassificationSample is a _Sample child base class that can be used for classification ML 
    processes. It contains a class label for the instance.
    
    Arguments:
        label: anything, default is None; the label to be associated to the sample in the frame of 
            a classification problem
    
    Attributes:
        LEGIT_LABELS: collection of legitimate label values for the classification problem where a 
            sample shall be used
        
        label: the label to be associated to the sample in the frame of a classification problem
    """
    
    LEGIT_LABELS = tuple()
    
    def __init__(self, label=None):
        super().__init__()
        self.label = label
    
    @property
    def label(self):
        return self.__label
    @label.setter
    def label(self, value):
        if value is not None and value not in self.LEGIT_LABELS:
            raise ValueError("'{}' is not a permissible label value ('{}').".format(value, self.LEGIT_LABELS))
        self.__label = value

    def __repr__(self):
        return "{}(label={})".format(type(self).__name__, self.label)

    def __str__(self):
        return repr(self)
    
    @abc.abstractmethod
    def contextual_sample_hash(self):
        """ Return a hash number that is supposed to unequivocally refer to this instance in the 
        frame of the current python session.
        
        Returns:
            int
        """
        return hash("")
    
    @abc.abstractmethod
    def to_hash_string(self):
        return ""


class AnaphoricitySample(_ClassificationSample):
    """ A sample to be used in the frame of a ML classification problem which is itself solved in the 
    frame of an anaphoricity resolution ML problem.
    
    Arguments:
        mention: a Mention instance; the mention whose anaphoricity status is used or sought, through 
            the use of this instance
        
        label: anything, default is None; the label to be associated to the sample in the frame of 
            a classification problem
        
    Attributes:
        SAMPLE_TYPE: string specifying the type of the sample 
        
        LEGIT_LABELS: collection of legitimate label values for the classification problem where a 
        sample shall be used
        
        label: the label to be associated to the sample in the frame of a classification problem
        
        mention: the mention whose anaphoricity status is used or sought, through 
            the use of this instance
        
        document: the document that this instance's mention is defined in reference to 
    """
    
    SAMPLE_TYPE = ANAPHORICITY_SAMPLE_TYPE
    LEGIT_LABELS = (POS_CLASS, NEG_CLASS)
    
    def __init__(self, mention, label=None):
        super().__init__(label=label)
        self.mention = mention
        self.document = mention.document
    
    def __repr__(self):
        return "AnaphoricitySample(label={}, extent={}, document={})".format(self.label, 
                                                                             self.mention.extent, 
                                                                             repr(self.document))
    
    def contextual_sample_hash(self):
        """ Returns a hash number that is supposed to unequivocally refer to this instance in the 
        frame of the current python session.
        
        Returns:
            int
        """
        return hash(("A", self.mention.extent, id(self.document)))
    
    def to_hash_string(self):
        """ Returns a string allowing to identify this instance, in the context of the document that 
        its associated mention originates from.
        
        Returns:
            string
        """
        return "A_{}".format(self.mention.extent)



class SingletonSample(_ClassificationSample):
    """ A sample to be used in the frame of a ML classification problem which is itself solved in the 
    frame of an singleton resolution ML problem.
    
    Arguments:
        mention: a Mention instance; the mention whose singleton status is used or sought, through 
            the use of this instance
        
        label: anything, default is None; the label to be associated to the sample in the frame of 
            a classification problem
        
    Attributes:
        SAMPLE_TYPE: string specifying the type of the sample 
        
        LEGIT_LABELS: collection of legitimate label values for the classification problem where a 
            sample shall be used
        
        label: the label to be associated to the sample in the frame of a classification problem
        
        mention: the mention whose singleton status is used or sought, through 
            the use of this instance
        
        document: the document that this instance's mention is defined in reference to 
    """
    
    SAMPLE_TYPE = SINGLETON_SAMPLE_TYPE
    LEGIT_LABELS = (POS_CLASS, NEG_CLASS)
    
    def __init__(self, mention, label=None):
        super().__init__(label=label)
        self.mention = mention
        self.document = mention.document
    
    def __repr__(self):
        return "SingletonSample(label={}, extent={}, document={})".format(self.label, 
                                                                          self.mention.extent, 
                                                                          repr(self.document))
    
    def contextual_sample_hash(self):
        """ Returns a hash number that is supposed to unequivocally refer to this instance in the 
        frame of the current python session.
        
        Returns:
            int
        """
        return hash(("S", self.mention.extent, id(self.document)))
    
    def to_hash_string(self):
        """ Returns a string allowing to identify this instance, in the context of the document that 
        its associated mention originates from.
        
        Returns:
            string
        """
        return "S_{}".format(self.mention.extent)



class MentionPairSample(_ClassificationSample):
    """ A sample to be used in the frame of a ML classification problem which is itself solved in the 
    frame of an coreference resolution ML problem. Such a sample is made from a pair of two mentions, 
    ordered in reading order, or of the 'null' mention and an actual mention.
    
    Arguments:
        left_mention: a Mention instance, or the NULL_MENTION; the first mention of the pair, in 
            text reading order
        
        right_mention: a Mention instance; the second mention of the pair, in text reading order
        
        entity_head_extents: set-like of mention extents (valid for the document whose 
            left and right input mentions originate from) whose corresponding mention (for this document) 
            are the head of their respective entity, or None
        
        singleton_extents: set-like of mention extents (valid for the document whose left and right 
            input mentions originate from) whose corresponding mention (for this document) are singletons, 
            or None
        
        label: anything, default is None; the label to be associated to the sample in the frame of 
            a classification problem
        
    Attributes:
        SAMPLE_TYPE: string specifying the type of the sample
        
        LEGIT_LABELS: collection of legitimate label values for the classification problem where a 
            sample shall be used
        
        label: the label to be associated to the sample in the frame of a classification problem
        
        left: the first mention of the pair, in text reading order
        
        right: the second mention of the pair, in text reading order
        
        document: the document that this instance's mention(s) are defined in reference to 
        
        anaphoricity_sample: the AnaphoricitySample instance associated to the right mention in case 
            the left is the NULL_MENTION; if the left mention isn't, then this is None
        
        left_singleton: the SingletonSample instance associated to the left mention in case both 
            mentions are true mentions; is None else 
        
        right_singleton: the SingletonSample instance associated to the right mention in case both 
            mentions are true mentions; is None else
        
        entity_head_extents: set-like of mention extents (valid for the document whose 
            left and right input mentions originate from) whose corresponding mention (for this document) 
            are the head of their respective entity, or None
        
        singleton_extents: set-like of mention extents (valid for the document whose left and right 
            input mentions originate from) whose corresponding mention (for this document) are singletons, 
            or None
        
        pair: the (left_mention; right_mention) tuple
        
        extent_pair: the pair of extent values corresponding respectively to the left mention and 
            to the right mention
    """
    
    SAMPLE_TYPE = MENTION_PAIR_SAMPLE_TYPE
    LEGIT_LABELS = (POS_CLASS, NEG_CLASS)
    
    def __init__(self, left_mention, right_mention, entity_head_extents=None, singleton_extents=None, 
                 label=None):
        # Check input
        if left_mention is not NULL_MENTION and left_mention.document is not right_mention.document:
            msg = "Left mention's document instance ({}) is not the same as the right's ({})."
            msg = msg.format(left_mention.document, right_mention.document)
            raise ValueError(msg)
        # Initialize using parent class
        super().__init__(label=label)
        # Process input
        anaphoricity_sample = None
        left_singleton = None
        right_singleton = None
        if left_mention is NULL_MENTION:
            anaphoricity_sample = AnaphoricitySample(right_mention)
        else:
            left_singleton = SingletonSample(left_mention)
            right_singleton = SingletonSample(right_mention)
        # Assign attribute values
        self.left = left_mention # In reading order of the document, first
        self.right = right_mention # In reading order of the document, second
        self.document = self.right.document
        self.anaphoricity_sample = anaphoricity_sample
        self.left_singleton = left_singleton
        self.right_singleton = right_singleton
        self.entity_head_extents = entity_head_extents
        self.singleton_extents = singleton_extents
    
    @property
    def pair(self):
        return (self.left, self.right)
    @pair.setter
    def pair(self, value):
        raise AttributeError
    
    @property
    def extent_pair(self):
        return self.get_extent_pair_from_mention_pair(self.left, self.right)
    @extent_pair.setter
    def extent_pair(self, value):
        raise AttributeError
    
    @staticmethod
    def get_extent_pair_from_mention_pair(mention1, mention2):
        """ Returns the pair of extent values corresponding respectively to the left mention and to 
        the right mention.
        
        Returns:
            a (extent1, extent2) pair
        """
        return (mention1.extent, mention2.extent)
    
    def __repr__(self):
        return "{}(label={}, extent_pair={}, document={})".format(type(self).__name__, self.label, 
                                                                  self.extent_pair, repr(self.document))
    
    def contextual_sample_hash(self):
        """ Return a hash number that is supposed to unequivocally refer to this instance in the 
        frame of the current python session.
        
        Returns:
            int
        """
        return hash(("MP", self.left.extent, self.right.extent, id(self.document)))
    
    def to_hash_string(self):
        """ Return a string allowing to identify this instance, in the context of the document that 
        its associated mention originates from.
        
        Returns:
            string
        """
        return "MP_({},{})".format(*self.extent_pair)
