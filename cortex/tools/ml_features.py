# -*- coding: utf-8 -*-

"""
Defines structures to define and use a vectorization process for arbitrary sample objects

Version:
    0.1.0

Author:
    François NOYEZ

Copyright:
    Copyright (C) 2017-2018 Inria. All Rights Reserved.
"""

__all__ = ["NumericFeature", "NUMERIC_FEATURE_TYPE", 
           "MultiNumericFeature", "MULTI_NUMERIC_FEATURE_TYPE", 
           "CategoricalFeature", "CATEGORICAL_FEATURE_TYPE", "UnknownCategoricalFeatureValueError",
           "SampleVectorizer",
           "create_features_product", 
           "quantize_numeric_feature", 
           "create_product_features_from_groups_definition",
           "_Feature", 
           ]

import abc
import numpy as np
from scipy import sparse
import logging
import itertools



class _Feature(object):
    """ Class used to represent a ML feature, that is, an object that knows how to obtain and create a 
    specific value from the input that it is destined to receive.
    
    If the feature is categorical, it must know the universe of possible value it is supposed to be 
    able to manage. It must also be able to create the one-hot vector corresponding to a specific 
    value, or to populate a given vector with those value. That means that it must also be able to 
    tell the size of its numeric value vector. It must be able to manage the case where he is unable 
    to know which value to affect <=> deals with 'None' (for example, if the feature is supposed to 
    be boolean, and if there is the possibility of not knowing the true value, then, when carrying 
    out numeric encoding, True <=> 1, False <=> -1, None <=> 0)
    
    It must have a name, and must be able to provide the names of its sub-features in case of 
    one-hot encoding.
    It must be able to be combined with other features to create a new feature using the four basic 
    operation (at least if their type is numeric).
    
    Arguments:
        name: string; the name by which to identify the feature
        
        compute_fct: the base function that the feature will use to compute its value on a given input
    
    Attributes:
        FEATURE_TYPE: string, the type of the feature
        
        name: string; the name by which to identify the feature
        
        numeric_size: integer; the number of values  in the numeric vector corresponding to the 
            numeric representation of the result of the feature being applied to an input (that is, the 
            length of such a vector)
        
        numeric_column_names: the name to give to the column of the numeric vector that is the numeric 
            representation of the result of the feature being applied to an input
    """
    
    FEATURE_TYPE = None
    
    def __init__(self, name, compute_fct):
        self.name = name
        self._compute_fct = compute_fct
    
    @abc.abstractproperty
    def numeric_size(self):
        pass
    @numeric_size.setter
    def numeric_size(self, value):
        raise AttributeError
    
    @abc.abstractproperty
    def numeric_column_names(self):
        pass
    @numeric_column_names.setter
    def numeric_column_names(self, value):
        raise AttributeError
    
    def compute_value(self, sample):
        """ Computes and returns the feature value associated to the input sample.
        
        Args:
            sample: a python object that is fit to be an input for the 'compute_fct' attribute value
        
        Returns:
            python object, the output of the 'compute_fct' attribute value, a function, being applied 
            to the input sample
        """
        return self._compute_fct(sample)
    
    def to_numeric_dense_vector(self, sample):
        """ Returns the feature's value under a dense vector representation
        
        Args:
            sample: the sample from which to compute the feature's value

        Returns:
            a numpy 2D array (shape=(1,self.numeric_size,)) containing the numeric 
            representation of the feature's value wrt the input
        """
        array = np.zeros(shape=(1,self.numeric_size,), dtype=np.float)
        self.fill_numeric_vector(sample, array, 0)
        return array
    
    def to_numeric_sparse_vector(self, sample):
        """ Returns the feature's value under a sparse vector representation
        
        Args:
            sample: the sample from which to compute the feature's value

        Returns:
            a scipy.sparse.csr_matrix (shape = (1,self.numeric_size)) containing the sparse 
            numeric representation of the feature's value wrt the input
        """
        sparse_array = sparse.lil_matrix((1,self.numeric_size), dtype=np.float)
        self.fill_numeric_vector(sample, sparse_array, 0)
        return sparse_array.tocsr()
    
    @abc.abstractmethod
    def fill_numeric_vector(self, sample, matrix, row_id, column_offset=0):
        """ Fills the specified row of the input matrix with the value(s) obtained when considering a 
        conversion of the feature to a numeric representation (ex: one-hot encoding when dealing 
        with a categorical feature).
        
        Only the location of the matrix that the value corresponds to will be updated / filled, the 
        other location will be untouched.
        
        Args:
            sample: the sample from which to compute the feature's value
            matrix: the matrix to fill. Its shape must be (1,self.numeric_size,)
            row_id: the id of the row of the matrix whose column should be updated
            column_offset: non-negative integer (default=0), the id of the column which shall 
                the first to be filled
        """
        if row_id < 0:
            msg = "Input 'row_id' value ('{}') is inferior to 0, must be a non-negative integer."
            msg = msg.format(row_id)
            raise ValueError(msg)
        if row_id >= matrix.shape[0]:
            msg = "Input 'row_id' value ('{}') is superior or equal to nb of rows in input matrix ('{}')."
            msg = msg.format(row_id, matrix.shape[0])
            raise ValueError(msg)
        if column_offset < 0:
            msg = "Input 'column_offset' value ('{}') is inferior to 0, must be a non-negative integer."
            msg = msg.format(column_offset)
            raise ValueError(msg)
    
    @abc.abstractmethod
    def __repr__(self):
        return "_Feature(name='{}', FEATURE_TYPE='{}')".format(self.name, self.FEATURE_TYPE)
    
    def __str__(self):
        return repr(self)



NUMERIC_FEATURE_TYPE = "numeric"
class NumericFeature(_Feature):
    """ Class used to represent a numeric ML feature, that is, an object that knows how to obtain 
    and create a numeric value from the input that it is destined to receive.
    
    Arguments:
        name: string; the name by which to identify the feature
        
        compute_fct: the base function that the feature will use to compute its numeric value for a 
            given input
    
    Attributes:
        FEATURE_TYPE: string, the type of the feature
        
        name: string; the name by which to identify the feature
        
        numeric_size: integer; the number of values  in the numeric vector corresponding to the 
            numeric representation of the result of the feature being applied to an input (that is, the 
            length of such a vector)
        
        numeric_column_names: the name to give to the column of the numeric vector that is the numeric 
            representation of the result of the feature being applied to an input
    """
    
    
    FEATURE_TYPE = NUMERIC_FEATURE_TYPE
    
    @property
    def numeric_size(self):
        return 1
    @numeric_size.setter
    def numeric_size(self, value):
        raise AttributeError
    
    @property
    def numeric_column_names(self):
        return (self.name,)
    @numeric_column_names.setter
    def numeric_column_names(self, value):
        raise AttributeError
    
    def fill_numeric_vector(self, sample, matrix, row_id, column_offset=0):
        """ Fills the specified row of the input matrix with the value(s) obtained when considering a 
        conversion of the feature to a numeric representation (ex: one-hot encoding when dealing 
        with a categorical feature).
        Only the location of the matrix that the value corresponds to will be updated / filled, the 
        other location will be untouched.
        
        Args:
            sample: the sample from which to compute the feature's value
            matrix: the matrix to fill. Its shape must be (1,self.numeric_size,)
            row_id: the id of the row of the matrix whose column should be updated
            column_offset: non-negative integer (default=0), the id of the column which shall 
                the first to be filled
        """
        #super().fill_numeric_vector(sample, matrix, row_id, column_offset=column_offset)
        matrix[row_id,column_offset+0] = self.compute_value(sample)
    
    def __repr__(self):
        return "NumericFeature(name='{}')".format(self.name)



MULTI_NUMERIC_FEATURE_TYPE = "multi_numeric"
class MultiNumericFeature(_Feature):
    """ Class used to represent a multi-numeric ML feature, that is, an object that knows how to 
    obtain and create a vector of numeric values from the input that it is destined to receive.
    
    Arguments:
        name: string; the name by which to identify the feature
        
        compute_fct: the base function that the feature will use to compute its value on a given input,
            must have the following signature: "sample, matrix=None, row_id=0, column_offset=0", must 
            output a matrix (either numpy or scipy.sparse) if matrix=None, else the input matrix (which 
            is assumed to contains zeros where it should be updated) will be updated
        
        column_names: string collection containing the name used to identify the feature associated 
            with each column of the matrix output of the 'compute_fct'
    
    Attributes:
        FEATURE_TYPE: string, the type of the feature
        
        name: string; the name by which to identify the feature
        
        numeric_size: integer; the number of values  in the numeric vector corresponding to the 
            numeric representation of the result of the feature being applied to an input (that is, the 
            length of such a vector)
        
        numeric_column_names: the name to give to the column of the numeric vector that is the numeric 
            representation of the result of the feature being applied to an input
        
        column_names: string collection containing the name used to identify the feature associated 
            with each column of the matrix output of the 'compute_fct'
    """
    FEATURE_TYPE = MULTI_NUMERIC_FEATURE_TYPE
    
    def __init__(self, name, compute_fct, column_names):
        # Check input
        # Init parent
        super().__init__(name, compute_fct)
        # Set attribute values
        self._column_names = tuple(column_names) #get_unique_elts(column_names)
        self.logger = logging.getLogger("CategoricalFeature_{}".format(self.name))
    
    @property
    def numeric_size(self):
        return len(self._column_names)
    @numeric_size.setter
    def numeric_size(self, value):
        raise AttributeError
    
    @property
    def column_names(self):
        return self._column_names
    @column_names.setter
    def column_names(self, value):
        raise AttributeError
    
    @property
    def numeric_column_names(self):
        return tuple("[{}]__({})".format(self.name, value) for value in self._column_names)
    @numeric_column_names.setter
    def numeric_column_names(self, value):
        raise AttributeError
    
    def fill_numeric_vector(self, sample, matrix, row_id, column_offset=0):
        """ Fills the specified row of the input matrix with the value(s) obtained when considering a 
        conversion of the feature to a numeric representation (ex: one-hot encoding when dealing 
        with a categorical feature).
        
        Only the location of the matrix that the value corresponds to will be updated / filled, the 
        other location will be untouched.
        
        Args:
            sample: the sample from which to compute the feature's value
            matrix: the matrix to fill. Its shape must be (1,self.numeric_size,)
            row_id: the id of the row of the matrix whose column should be updated
            column_offset: non-negative integer (default=0), the id of the column which shall 
                the first to be filled
        """
        #super().fill_numeric_vector(sample, matrix, row_id, column_offset=column_offset)
        self._compute_fct(sample, matrix=matrix, row_id=row_id, column_offset=column_offset)
    
    def __repr__(self):
        return "MultiNumericFeature(name='{}', column_names='{}')".format(self.name, self.column_names)



CATEGORICAL_FEATURE_TYPE = "categorical"        
class CategoricalFeature(_Feature):
    """ Class used to represent a categorical ML feature, that is, an object that knows how to 
    obtain and create a categorical value from the input that it is destined to receive.
    
    For 'compute_fct' whose output is a (1,numeric_size)-shaped matrix
    
    Arguments:
        name: string; the name by which to identify the feature
        
        compute_fct: the base function that the feature will use to compute its value on a given input,
            must have the following signature: "sample, matrix=None, row_id=0, column_offset=0", must 
            output a matrix (eitheir numpy or scipy.sparse) if matrix=None, else the input matrix (which 
            s assumed to contains zeros where it should be updated) will be updated
        
        possible_values: collection of values; the space of values in which the 'compute_fct' may 
            end up in when called on an input
        
        strict: boolean; if true, raise an Exception if a computed value is not among the input 
            possible values; if False, only a warning log will be emitted, and the returned value will 
            be 'None' (and the one-hot representation will be a zeros-filled vector)
    
    Attributes:
        FEATURE_TYPE: string, the type of the feature
        
        name: string; the name by which to identify the feature
        
        numeric_size: integer; the number of values  in the numeric vector corresponding to the 
            numeric representation of the result of the feature being applied to an input (that is, the 
            length of such a vector)
        
        numeric_column_names: the name to give to the column of the numeric vector that is the numeric 
            representation of the result of the feature being applied to an input
        
        possible_values: collection of values; the space of values in which the 'compute_fct' may 
            end up in when called on an input
    """
    
    FEATURE_TYPE = CATEGORICAL_FEATURE_TYPE
    
    def __init__(self, name, compute_fct, possible_values, strict=False):
        # Check input
        # If collection of possible values is empty, this feature will be useless. Scream it to the world, then.
        if not possible_values:
            message = "The collection of possible values that this feature can take is empty. That won't do."
            raise ValueError(message)
        
        _possible_values = tuple(get_unique_elts(possible_values))
        self._possible_values_map = {value: index for index, value in enumerate(_possible_values)}
        if strict:
            def __compute_fct(sample):
                value = compute_fct(sample)
                try:
                    index = self._possible_values_map[value]
                except KeyError:
                    raise UnknownCategoricalFeatureValueError(self.name, value, self._possible_values) from None
                return value, index
            _compute_fct = lambda sample: __compute_fct(sample)[0]
            
            def _one_hot_fill_array(sample, array, row_id, column_offset=0):
                """
                    array: an array of shape (1,self.numeric_size) filled with 0
                """
                _, index = __compute_fct(sample) #value
                array[row_id,column_offset+index] = 1.0
            
        else:
            def __compute_fct(sample):
                value = compute_fct(sample)
                index = None
                try:
                    index = self._possible_values_map[value]
                except KeyError:
                    msg = "Computed value '{}' for feature '{}' when possible values are: {}"
                    msg = msg.format(value, self.name, self.possible_values)
                    self.logger.warning(msg)
                return value, index
            _compute_fct = compute_fct
            
            def _one_hot_fill_array(sample, array, row_id, column_offset=0):
                """
                    array: an array of shape (1,self.numeric_size) filled with 0
                """
                _, index = __compute_fct(sample) #value
                if index is not None:
                    array[row_id,column_offset+index] = 1.0
        
        # Init parent
        super().__init__(name, _compute_fct)
        # Set attribute values
        self.__compute_fct = __compute_fct
        self._possible_values = _possible_values
        self.strict = strict
        self._one_hot_fill_array = _one_hot_fill_array
        self.logger = logging.getLogger("CategoricalFeature_{}".format(self.name))
    
    @property
    def numeric_size(self):
        return len(self._possible_values)
    @numeric_size.setter
    def numeric_size(self, value):
        raise AttributeError
    
    @property
    def possible_values(self):
        return self._possible_values
    @possible_values.setter
    def possible_values(self, value):
        raise AttributeError
    
    @property
    def numeric_column_names(self):
        return tuple("[{}]__({})".format(self.name, value) for value in self._possible_values)
    @numeric_column_names.setter
    def numeric_column_names(self, value):
        raise AttributeError
    
    def compute_index(self, sample):
        return self.__compute_fct(sample)[1]
    
    
    def fill_numeric_vector(self, sample, matrix, row_id, column_offset=0):
        """ Fills the specified row of the input matrix with the value(s) obtained when considering a 
        conversion of the feature to a numeric representation (ex: one-hot encoding when dealing 
        with a categorical feature).
        
        Only the location of the matrix that the value corresponds to will be updated / filled, the 
        other location will be untouched.
        
        Args:
            sample: the sample from which to compute the feature's value
            matrix: the matrix to fill. Its shape must be (1,self.numeric_size,)
            row_id: the id of the row of the matrix whose column should be updated
            column_offset: non-negative integer (default=0), the id of the column which shall 
                the first to be filled
        """
        #super().fill_numeric_vector(sample, matrix, row_id, column_offset=column_offset)
        self._one_hot_fill_array(sample, matrix, row_id, column_offset=column_offset)
    
    def __repr__(self):
        return "CategoricalFeature(name='{}', possible_values='{}')".format(self.name, self.possible_values)



def create_features_product(feature1, feature2, new_name=None):
    """ Create a feature which correspond to the product of two features; that is, its value(s) on 
    a given sample reflects information brought by the result of applying both features to the sample.
    
    The type of the resulting product feature will depend upon the type of its input 
    feature:
        - "numeric" x "numeric" => "numeric"
        - "numeric" x "multinumeric" => "multinumeric"
        - "numeric" x "categorical" => "multinumeric"
        - "multinumeric" x "multinumeric" => "multinumeric"
        - "multinumeric" x "categorical" => "multinumeric"
        - "categorical" x "categorical" => "categorical"
    
    This product is commutative.
    
    Args:
        feature1: a _Feature child class instance, first feature of the product
        feature2: a _Feature child class instance, second feature of the product
        new_name: string, or None; the name to give to the product feature. If None, the name 
            will be a combination of the name of the features input in the product.

    Returns:
        a _Feature child class instance
    """
    possible_types = (NUMERIC_FEATURE_TYPE, MULTI_NUMERIC_FEATURE_TYPE, CATEGORICAL_FEATURE_TYPE)
    feature1_type = feature1.FEATURE_TYPE
    feature2_type = feature2.FEATURE_TYPE
    # Check input value
    if feature1_type not in possible_types:
        message = "The type ('{}') of the input feature1 '{}' is incorrect, possible types are: {}."
        message = message.format(feature1_type, feature1.name, possible_types)
        raise ValueError(message)
    if feature2_type not in possible_types:
        message = "The type ('{}') of the input feature2 '{}' is incorrect, possible types are: {}."
        message = message.format(feature2_type, feature2.name, possible_types)
        raise ValueError(message)
    
    type_combination = tuple(sorted((feature1_type, feature2_type)))
    
    name = new_name
    if not name:
        name = "product_[{}]_[{}]".format(feature1.name, feature2.name)
    
    # Create the product feature
    if type_combination == tuple(sorted((CATEGORICAL_FEATURE_TYPE, CATEGORICAL_FEATURE_TYPE))):
        compute_fct = lambda sample: "({})_({})".format(feature1.compute_value(sample), feature2.compute_value(sample))
        possible_values = tuple("({})_({})".format(pv1, pv2) for pv1 in feature1.possible_values for pv2 in feature2.possible_values)
        strict = feature1.strict or feature2.strict
        new_feature = CategoricalFeature(name, compute_fct, possible_values, strict=strict)
    
    elif type_combination == tuple(sorted((NUMERIC_FEATURE_TYPE, NUMERIC_FEATURE_TYPE))):
        compute_fct = lambda sample: feature1.compute_value(sample) * feature2.compute_value(sample)
        new_feature = NumericFeature(name, compute_fct)
    
    elif type_combination == tuple(sorted((MULTI_NUMERIC_FEATURE_TYPE, MULTI_NUMERIC_FEATURE_TYPE))):
        new_features_names = tuple("({})_({})".format(multi_fn1, multi_fn2) for multi_fn1, multi_fn2 in itertools.product(feature1.column_names, feature2.column_names))
        new_columns_nb = len(new_features_names)
        size1 = feature1.numeric_size
        size2 = feature2.numeric_size
        def _f_multinum_multinum(sample, matrix=None, row_id=0, column_offset=0):
            if matrix is None:
                result = np.zeros((1,new_columns_nb), dtype=np.float)
                _f_multinum_multinum(sample, matrix=result, row_id=0, column_offset=0)
                return result
            dense_vector1 = feature1.to_numeric_dense_vector(sample)
            feature2.fill_numeric_vector(sample, matrix, row_id, column_offset=column_offset+(size1-1)*size2)
            dense_vector2 = matrix[row_id,column_offset+(size1-1)*size2:column_offset+size1*size2]
            for i in range(size1):
                matrix[row_id,column_offset+i*size2:column_offset+(i+1)*size2] = dense_vector1[0,i] * dense_vector2
            return matrix
        compute_fct = _f_multinum_multinum
        new_feature = MultiNumericFeature(name, compute_fct, new_features_names)
    
    elif type_combination == tuple(sorted((CATEGORICAL_FEATURE_TYPE, NUMERIC_FEATURE_TYPE))):
        categorical_feature = feature1
        numeric_feature = feature2
        if feature2_type == CATEGORICAL_FEATURE_TYPE:
            categorical_feature = feature2
            numeric_feature = feature1
        cat_possible_values = categorical_feature.possible_values
        new_features_names = tuple("weighed_({})".format(n) for n in cat_possible_values)
        new_columns_nb = len(new_features_names)
        def _f_categorical_numeric(sample, matrix=None, row_id=0, column_offset=0):
            if matrix is None:
                result = sparse.lil_matrix((1,new_columns_nb), dtype=np.float)
                _f_categorical_numeric(sample, matrix=result, row_id=0, column_offset=0)
                return result.tocsr()
            nnz_column_id = categorical_feature.compute_index(sample)
            if nnz_column_id is not None:
                matrix[row_id,column_offset+nnz_column_id] = numeric_feature.compute_value(sample)
            return matrix
        compute_fct = _f_categorical_numeric
        new_feature = MultiNumericFeature(name, compute_fct, new_features_names)
    
    elif type_combination == tuple(sorted((MULTI_NUMERIC_FEATURE_TYPE, NUMERIC_FEATURE_TYPE))):
        multi_numeric_feature = feature1
        numeric_feature = feature2
        if feature2_type == MULTI_NUMERIC_FEATURE_TYPE:
            multi_numeric_feature = feature2
            numeric_feature = feature1
        new_features_names = tuple("({})_({})".format(numeric_feature.name, mn_fn) for mn_fn in multi_numeric_feature.column_names)
        multi_n_features_nb = multi_numeric_feature.numeric_size
        new_columns_nb = len(new_features_names)
        def _f_multinumeric_numeric(sample, matrix=None, row_id=0, column_offset=0):
            if matrix is None:
                result = np.zeros((1,new_columns_nb), dtype=np.float)
                _f_multinumeric_numeric(sample, matrix=result, row_id=0, column_offset=0)
                return result
            multi_numeric_feature.fill_numeric_vector(sample, matrix, row_id=row_id, column_offset=column_offset)
            matrix[row_id,column_offset:column_offset+multi_n_features_nb] = numeric_feature.compute_value(sample)*matrix[row_id,column_offset:column_offset+multi_n_features_nb]
            return matrix
        compute_fct = _f_multinumeric_numeric
        new_feature = MultiNumericFeature(name, compute_fct, new_features_names)
    
    elif type_combination == tuple(sorted((CATEGORICAL_FEATURE_TYPE, MULTI_NUMERIC_FEATURE_TYPE))):
        categorical_feature = feature1
        multi_numeric_feature = feature2
        if feature2_type == CATEGORICAL_FEATURE_TYPE:
            categorical_feature = feature2
            multi_numeric_feature = feature1
        cat_possible_values = categorical_feature.possible_values
        multi_features_names = multi_numeric_feature.column_names
        new_features_names = tuple("({})_({})".format(cat_fn, multi_fn) for cat_fn, multi_fn in itertools.product(cat_possible_values, multi_features_names))
        multi_n_features_nb = multi_numeric_feature.numeric_size
        new_columns_nb = len(new_features_names)
        def _f_categorical_multinum(sample, matrix=None, row_id=0, column_offset=0):
            if matrix is None:
                result = sparse.lil_matrix((1,new_columns_nb), dtype=np.float)
                _f_categorical_multinum(sample, matrix=result, row_id=0, column_offset=0)
                return result.tocsr()
            nnz_column_id = categorical_feature.compute_index(sample)
            if nnz_column_id is not None:
                multi_numeric_feature.fill_numeric_vector(sample, matrix, row_id=row_id, column_offset=column_offset+nnz_column_id*multi_n_features_nb)
            return matrix
        compute_fct = _f_categorical_multinum
        new_feature = MultiNumericFeature(name, compute_fct, new_features_names)
    
    return new_feature


def quantize_numeric_feature(feature, threshold_values, new_name=None):
    """ Creates a categorical feature whose possible values correspond to bins defined by the input 
    threshold values, and whose value for a given sample corresponds to the bin where the numeric 
    value got when applying the input numerical feature to the sample.
    
    Args:
        feature: a NumericFeature instance
        threshold_values: a collection of float used as threshold values
        new_name: string, or None; the name to give to the output categorical feature. If None, 
            the name will be a variation of the name of the input numeric feature.

    Returns:
        a _Feature child class instance
    """
    # Check input value
    if feature.FEATURE_TYPE != NUMERIC_FEATURE_TYPE:
        message = "'feature' input value ('{}') is not a categorical feature (feature type = '{}')."
        message = message.format(feature.name, feature.FEATURE_TYPE)
        raise ValueError(message)
    if not threshold_values:
        msg = "Empty set of threshold values, cannot create a quantized feature for the '{}' feature."
        msg = msg.format(feature.name)
        raise ValueError(threshold_values)
    
    name = new_name
    if not name:
        name = "quantized_[{}]".format(feature.name)
    
    threshold_values = np.array(sorted(set(threshold_values)), dtype=np.float)
    possible_values = tuple(itertools.chain(("value<{}".format(threshold_values[0]),),
                                            ("{}<=value<{}".format(v1, v2) for v1,v2 in zip(threshold_values[:-1], threshold_values[1:])),
                                            ("{}<=value".format(threshold_values[-1]),)
                                            ))
    previous_compute_fct = feature.compute_value
    compute_fct = lambda sample: possible_values[np.searchsorted(threshold_values, previous_compute_fct(sample), side="right", sorter=None)]
    return CategoricalFeature(name, compute_fct, possible_values, strict=False)


def create_product_features_from_groups_definition(features_groups, features, feature_name2feature=None):
    """ Creates the product feature corresponding to pairs of features that may be partitioned.
    
    Args:
        features_groups: collection of (first_features_names, second_features_names) pairs, where 
            a 'features_names' is a collection of name of features. One product feature shall be produced for 
            each pair of features defined by the pairs of feature names in the result of the cardinal product 
            of 'first_features_names' and 'second_features_names'
        features: the collection of _Feature child class instances from which to take the inputs 
            needed to create the product features. Each feature must have a distinct name. Each name 
            specified in the 'features_groups' input must correspond the name of a feature of this collection.
        feature_name2feature: map, or None; if not None, must be a map that, to each name 
            specified in the 'features_groups' input, must associate the name of a feature defined in the 
            'features' input.

    Returns:
        the collection of the resulting product features
    """
    group_features = []
    if features_groups:
        if feature_name2feature is None:
            feature_name2feature = {feature.name: feature for feature in features}
        for features_group in features_groups:
            first_features_names, second_features_names = features_group
            for feature_name1 in first_features_names:
                feature1 = feature_name2feature[feature_name1]
                for feature_name2 in second_features_names:
                    feature2 = feature_name2feature[feature_name2]
                    feature_product = create_features_product(feature1, feature2)
                    group_features.append(feature_product)
    return group_features


class SampleVectorizer(object):
    """ The aim of this class is to represent an object that can extract all at once the features 
    associated to a sample, and that can notably extract a vector representation of the sample
    
    Arguments:
        features: the ordered collection of features to use in order to encode as a vector (i.e. to 
            vectorize) a sample
    
    Attributes:
        features_names: collection of the names of the ordered features used to initialize the instance
        
        numeric_column_names: collection of the names of the columns of the vectors (matrices) 
            produced when using the instance to vectorize sample (collection of samples)
        
        features_nb: int, number of feature objects used to initialize the instance
        
        numeric_column_nb: number of columns of vectors (matrices) produced when using the instance 
            to vectorize sample (collection of samples)
    """
    def __init__(self, features):
        # Check samples
        if not features:
            raise ValueError("Input 'features' collection is empty.")
        # Set attribute values
        self._features = tuple(features)
        self._numeric_column_names = tuple(itertools.chain(*tuple(feature.numeric_column_names for feature in self._features)))
        self._numeric_column_nb = sum((feature.numeric_size for feature in self._features),0)
        self.logger = logging.getLogger("SampleVectorizer")
    
    @property
    def features_names(self):
        return tuple(feature.name for feature in self._features)
    @features_names.setter
    def features_names(self, value):
        raise AttributeError
    
    @property
    def numeric_column_names(self):
        return self._numeric_column_names
    @numeric_column_names.setter
    def numeric_column_names(self, value):
        raise AttributeError
    
    @property
    def features_nb(self):
        return len(self._features)
    @features_nb.setter
    def features_nb(self, value):
        raise AttributeError
    
    @property
    def numeric_column_nb(self):
        return self._numeric_column_nb
    @numeric_column_nb.setter
    def numeric_column_nb(self, value):
        raise AttributeError
    
    # Instance methods
    def _inner_eq(self, other):
        """ Tests that this class instance is equal to the input object.
        
        That means 
        that:
            - this instance's class is equal or a subclass of the other instance's class
            - the respective 'numeric_column_names' attribute value of each input are equal
        
        Args:
            other: the instance of this class for which to test for equality

        Returns:
            a (boolean, string or None) pair; the second item of the pair will be None if the 
            boolean value is True (all attribute values were found to be equal), or will a string 
            identifying the reason that the inputs were found not to be equal otherwise
        """
        if not isinstance(other, SampleVectorizer):
            return False, "Class"
        if self._numeric_column_names != other._numeric_column_names:
            return False, "_numeric_column_names"
        return True, None
        
    def __eq__(self, other):
        result, _ = self._inner_eq(other)
        return result
    
    def __ne__(self, other):
        return not self == other
    
    def __hash__(self):
        raise NotImplemented
    
    def __repr__(self):
        return "{}(feature_nb={}, feature_names={})".format(type(self).__name__, self.features_nb, self.features_names)
    
    def compute_value(self, sample):
        """ Computes the ordered collection of values corresponding to the application to each feature 
        to the input sample.
        
        Args:
            sample: the sample to apply each feature to

        Returns:
            the corresponding collection of ordered values
        """
        return tuple(feature.compute_value(sample) for feature in self._features)
    
    def vectorize(self, samples, output_format="sparse"):
        """ Creates the matrix corresponding to the vector representations of the input iterable of 
        samples.
        
        Args:
            samples: collection of samples
            output_format: string, either "sparse" (default) or "dense"

        Returns:
            either a sparse.csr_matrix or a 2D numpy.ndarray, where row n°i corresponds to the 
            feature value vector of the input n°i in the collection of samples
        """
        # Check input
        possible_format_values = ("sparse", "dense")
        if output_format not in possible_format_values:
            msg = "Incorrect 'output_format' value ('{}'), possible values are: {}".format(output_format, possible_format_values)
            raise ValueError(msg)
        
        samples_nb = len(samples)
        if output_format == "sparse":
            matrix = sparse.lil_matrix((samples_nb,self.numeric_column_nb), dtype=np.float)
            _postprocess = lambda m: m.tocsr()
        elif output_format == "dense":
            matrix = np.matrix(np.zeros((samples_nb,self.numeric_column_nb), dtype=np.float))
            _postprocess = lambda m: m.A
        
        self.logger.debug("Encoding {} samples...".format(samples_nb))
        for i, sample in enumerate(samples):
            try:
                self._vectorize_sample(sample, matrix, i)
            except UnknownCategoricalFeatureValueError as e:
                msg = "Incorrect sample '{}' for encoding (original exception = '{}').".format(sample, str(e))
                raise ValueError(msg) from None
        result = _postprocess(matrix)
        
        return result
    
    def _vectorize_sample(self, sample, matrix, row_id):
        """ Fills a row of the input matrix with the vector representation of the input sample.
        
        Assumes that the number of columns of the input matrix is higher than the length of the vector 
        representation of the input sample.
        
        Args:
            sample: sample whose vector representation shall be used to fill a row of the input matrix
            matrix: numpy.ndarray, or scipy.sparse.sparse_matrix instance, one of whose row shall 
                be filled with the vector representation of the input sample
            row_id: index of the row of the input matrix which shall be filled with the vector 
                representation of the input sample
        """
        self.fill_matrix(sample, matrix, row_id=row_id, column_offset=0)
    
    def fill_matrix(self, sample, matrix, row_id=0, column_offset=0):
        """ Fills a continuous sub-row of the input matrix with the vector representation of the input 
        sample.
        
        Assumes that the input matrix possesses enough columns so as to do that, even accounting for 
        the definition of the column where the filling of values should start.
        
        Args:
            sample: sample whose vector representation shall be used to fill a part of the input matrix
            matrix: numpy.ndarray, or scipy.sparse.sparse_matrix instance, part of one of whose 
                row shall be filled with the vector representation of the input sample
            row_id: index of the row of the input matrix whose part shall be filled with the 
                vector representation of the input sample
            column_offset: index of the first column where the filling of values should start
        """
        next_column_offset = column_offset
        for feature in self._features:
            numeric_size = feature.numeric_size
            feature.fill_numeric_vector(sample, matrix, row_id, column_offset=next_column_offset)
            next_column_offset += numeric_size
    
    def vectorize_to_dense_numeric(self, sample):
        """ Vectorizes the input sample value as into a dense vector.
        
        Args:
            sample: the sample from which to compute the feature's value

        Returns:
            a numpy 2D array (shape = (1,self.numeric_size)) containing the sparse 
            numeric representation of the feature's value wrt the input
        """
        return self.vectorize((sample,), output_format="dense")
    
    def vectorize_to_sparse_numeric(self, sample):
        """ Vectorizes the input sample value as into a sparse vector.
        
        Args:
            sample: the sample from which to compute the feature's value

        Returns:
            a scipy.sparse.csr_matrix (shape = (1,self.numeric_size)) containing the sparse 
            numeric representation of the feature's value wrt the input
        """
        return self.vectorize((sample,), output_format="sparse")
    
    def decode_numeric_vector(self, vector):
        """ Associates the non zero values of the input vector (assumed to result from the 
        vectorization of a sample by this instance) to the corresponding column names.
        
        Args:
            vector: one row matrix or 2D array

        Returns:
            collection of (index, column_name, value) tuples corresponding to the non-zero values of 
            the input vector
        """
        if len(vector.shape) != 2 or vector.shape[0] != 1:
            raise ValueError("Incorrect input, must be a one row matrix or 2D-array.")
        if sparse.issparse(vector):
            col_indices = vector.nonzero()[1]
        else:
            col_indices = np.nonzero(vector)[1]
        if len(col_indices):
            one_hot_feature_names = self.numeric_column_names
            return tuple((i, one_hot_feature_names[i], vector[0,i]) for i in col_indices)
        return tuple()


## Utilities
def get_unique_elts(items_iterable):
    """ Consumes an iterable over items, and return the collection of get_unique_elts item that this iterable 
    contained, in the order in which they appeared.
    
    Args:
        items: iterable of hashable objects

    Returns:
        collection of each get_unique_elts item
    """
    seen = set()
    output = []
    for item in items_iterable:
        if item not in seen:
            output.append(item)
            seen.add(item)
    
    return tuple(output)

# Exceptions
class UnknownCategoricalFeatureValueError(Exception):
    """ Exception to be raised when a categorical feature class instance encounters a value that it 
    was not made aware of, and when such as case is not tolerated.
    
    Arguments:
        feature_name: the name of the categorical feature that encountered this issue
        value: the incriminated value
        possible_values: the collection of values that the categorical feature knew of
    
    Attributes:
        feature_name: the name of the categorical feature that encountered this issue
        value: the incriminated value
        possible_values: the collection of values that the categorical feature knew of
    """
    def __init__(self, feature_name, value, possible_values, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.feature_name = feature_name
        self.value = value
        self.possible_values = possible_values
    
    def __repr__(self):
        s = "{}: computed value '{}' for feature '{}' when possible values are: {}."
        return s.format(type(self).__name__, self.value, self.feature_name, self.possible_values)
    
    def __str__(self):
        return repr(self)
