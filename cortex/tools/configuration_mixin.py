# -*- coding: utf-8 -*-

"""
Defines a MixIn class such that an instance of a child class can be represented / created from 
a configuration
"""

__all__ = ["ConfigurationMixIn", 
           "define_create_function",
           "define_create_from_factory_configuration_function",
           "define_generate_factory_configuration_function",
           "create_mapping_if_input_is_not_Mapping_instance",
           "add_default_value_configuration_if_non_existent",
           "_create_simple_get_parameter_value_from_configuration",
           "_create_simple_set_parameter_value_in_configuration",
           "_create_attribute_data",
           ]

import re
from collections import OrderedDict

from ..utils.singleton import Singleton
from .configuration import Mapping, UnknownMappingFieldError



class ConfigurationMixIn(object):
    """ This class provides an infrastructure for managing the synchronization between the 
    initialization parameters of a class instance, and the configuration object associated with 
    this instance, for classes that are destined to be used in a class hierarchy.
    
    For a specific class inheriting from 'ConfigurationMixIn', a 'configuration' is a Mapping 
    instance whose fields are (often) the names of the attribute of an instance of this class, and 
    whose value are information allowing to precisely and unequivocally define this instance, such 
    that, if another instance is created using the same configuration, it will have the same 
    deterministic trajectory as the original instance (that is, placed in the same situation, it will 
    behave in the same way).
    
    A 'factory configuration' is a configuration containing a 'name' field, whose value is a string 
    referencing a specific class, and a 'config' field, whose value is an appropriate configuration 
    for the specific class.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = tuple()
    
    def __init__(self, configuration=None):
        self.configuration = create_mapping_if_input_is_not_Mapping_instance(configuration)
        #self.configuration = Mapping(create_mapping_if_input_is_not_Mapping_instance(configuration))
    
    @property
    def NAME(self):
        return type(self).__name__
    @NAME.setter
    def NAME(self, value):
        raise AttributeError
    
    @property
    def factory_configuration(self):
        configuration = Mapping([("name", self.NAME), ("config", self.configuration)])
        return configuration
    @factory_configuration.setter
    def factory_configuration(self, value):
        raise AttributeError
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        return tuple(), OrderedDict((("configuration", configuration),))
    
    @classmethod
    def define_configuration(cls):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Cf the documentation of the class of know which argument(s) to use.
        
        Returns:
            a Mapping instance
        """
        return Mapping()
    
    @classmethod
    def define_factory_configuration(cls, *args, **kwargs):
        """ Creates a configuration object with the field 'name' whose value is the name of this class, 
        and the field 'config', whose value is the configuration object by the 'define_configuration' 
        method.
        
        Cf the signature of 'define_configuration' class method to know which parameters can be used.
        
        Returns:
            a Mapping instance
        """
        return Mapping({"name": cls.__name__, "config": cls.define_configuration(*args, **kwargs)})
    
    @classmethod
    def from_configuration(cls, configuration):
        """ Creates an instance of this class parametrized by the values defined in the input configuration.
        
        That means that the fields of the input configuration will be parsed for parameter values 
        that must or may be used to initialize an instance of this class, and then the resulting 
        parameter values will be used to that end.
        
        Returns:
            an instance of this class, parametrized by values defining in the input configuration
        """
        args, kwargs = cls._extract_args_kwargs_from_configuration(configuration)
        return cls(*args, **kwargs)
    
    @classmethod
    def create(cls, *args, **kwargs):
        """ Creates an instance of this class parametrized by the input parameters values.
        
        Cf the signature of 'define_configuration' to know which parameters can be used.
        
        Returns:
            an instance of this class, parametrized by the input parameters
        """
        configuration = cls.define_configuration(*args, **kwargs)
        return cls.from_configuration(configuration)
    
    @staticmethod
    def _set_configuration_values_from_instance(parameter_name2configuration_getter_setter_pair, instance):
        """ Uses the configuration setter functions defined in the input map to set a field value of 
        the 'configuration' attribute value of the input instance, using the corresponding attribute 
        values of the input instance. 
        
        Args:
            parameter_name2configuration_getter_setter_pair: a "parameter_name => (getter, setter)" 
                map, where:
                    - 'parameter_name' is a string identifying the python object attribute / configuration 
                      field to which the functions are associated
                    - 'getter' is a function whose role is to extract a suitable attribute value 
                      from a configuration
                    - 'setter' is a function whose role is to set the 'parameter_name' field value 
                      of the configuration that is associated to the 'configuration' attribute of a 
                      python object, from the corresponding attribute value of the python object
            instance: a python object which possesses a 'configuration' attribute whose value is a 
                Mapping instance
        """
        for _, configuration_value_setter_fct in parameter_name2configuration_getter_setter_pair.values(): #configuration_value_getter_fct
            configuration_value_setter_fct(instance)
    
    @staticmethod
    def _get_initialization_parameters_values_from_configuration(parameter_name2configuration_getter_setter_pair, configuration):
        """ Uses the configuration getter functions defined in the input map to extract the 
        corresponding values from the input configuration.
        
        Args:
            parameter_name2configuration_getter_setter_pair: a "parameter_name => (getter, setter)" 
                map, where:
                    - 'parameter_name' is a string identifying the python object attribute / configuration 
                      field to which the functions are associated
                    - 'getter' is a function whose role is to extract a suitable attribute value 
                      from a configuration
                    - 'setter' is a function whose role is to set the 'parameter_name' field value 
                      of the configuration that is associated to the 'configuration' attribute of a 
                      python object, from the corresponding attribute value of the python object
            configuration: a Mapping instance
        
        Returns:
            collection of values, whose order
        """
        return tuple(configuration_getter_fct(configuration) for configuration_getter_fct, _ in parameter_name2configuration_getter_setter_pair.values()) # configuration_setter_fct
    
    def __repr__(self):
        sep=", "
        string_format = "{}={}"
        strings_array = []
        for attribute_name in self._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION:
            try:
                value = getattr(self, attribute_name)
            except AttributeError:
                value = "N/A"
            string = string_format.format(attribute_name, repr(value))
            strings_array.append(string)
        return "{}({})".format(type(self).__name__, 
                               sep.join(strings_array),
                               )
    
    def __str__(self):
        sep="\n"
        string_format = "{}={}"
        strings_array = []
        for attribute_name in self._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION:
            try:
                value = getattr(self, attribute_name)
            except AttributeError:
                value = "N/A"
            string = string_format.format(attribute_name, repr(value))
            strings_array.append(string)
        return "{}{}{}".format(type(self).__name__, 
                               ":\n", sep.join(strings_array),
                               )

# Utilities
class _OwnNone(Singleton):
    """ Class to be used as a default value specific to a use by this library.
    
    As such, needed to be distinct from python's "None", but is planned to be used in a similar manner.
    Because of that, needs to be a Singleton-like class.
    """
    pass
_OWN_NONE = _OwnNone()


#########################################################################################################
# Utilities to create a factory configuration or an instance of a class derived from ConfigurationMixIn #
#########################################################################################################
def define_check_item_name_function(possible_class_name2class, class_family):
    """ Creates a function whose role is to check that an input name corresponds indeed to names that 
    relate to classes of a specific class family, and to return that specific class if it was found.
    
    Args:
        possible_class_name2class: a "possible_class_name => class" map
        class_family: a string, specifying how to call the class family, must be suitable to be part 
            of a python's function name (ex: 'resolver')

    Returns:
        a callable that raises an exception if the name input in it does not correspond to an 
        appropriate class family class, and return this class if it does
    """
    possible_values_string = "{"+(", ".join(possible_class_name2class.keys()))+"}"
    ordered_possible_values = tuple(possible_class_name2class.keys())
    fct_docstring = """
    Checks whether or not the input string is a valid {class_type:s} class name or surname.
    
    Raises an exception if it does not, and return the corresponding class if it does.
    
    Args:
        name: string, the name or surname of the class to instantiate. Possible names or surnames 
            are:
                {possible_names:}.

    Returns:
        the '{class_type:s}' class specified by the input 'name'
    
    Raises:
        ValueError: if the input 'name' parameter value is not a correct {class_type:s} class name or pseudo-name.
    
    Notes:
        Check the 'create' method of each class to know more about the possible specific (args, kwargs) values
    """.format(class_type=class_family, 
               possible_names="\t\n".join("{}, {}".format(*sl[::-1]) for sl in (ordered_possible_values[2*i:2*(i+1)]\
                                                                       for i in range(int(len(ordered_possible_values)/2))
                                                                       )
                                        )
               )
    fct_name = "check_{}_name".format(class_family)
    def _check_item_name(name):
        try:
            class_ = possible_class_name2class[name]
        except KeyError:
            message = "'{}' is not a valid '{}' name, possible names are: {}."
            message = message.format(name, class_family, possible_values_string)
            raise ValueError(message)
        return class_
    _check_item_name.__doc__ = fct_docstring
    _check_item_name.__name__ = fct_name
    return _check_item_name

def define_create_function(name2class, class_family):
    """ Creates a function whose role is to create an instance of class belonging to a specific 
    family of classes, from inputs corresponding to the name of the class, as well as parameters 
    that may be needed, or can be used, to parametrize the specific instance to be created. Those 
    other parameters should be the same than those accepted by the 'create' method of the specified 
    resolver class.
    
    Args:
        possible_class_name2class: a "possible_class_name => class" map
        class_family: string, specifying how to call the class family, must be suitable to be part 
            of a python's function name (ex: 'resolver')

    Returns:
        the "from name and arguments, specific class family, class instance creation" function
    """
    possible_names_string = _get_listing_string_for_docstring_creation(name2class, 16)
    fct_docstring = """
    Creates an instance of a {class_type:s} class.
    
    Args:
        name: string, the name or surname of the class to instantiate. Possible names or 
            surnames are:
{possible_names:}.
        args: args to pass to the 'create' method of the {class_type:s} class specified by the value of 'name'
        kwargs: kwargs to pass to the 'create' method of the {class_type:s} class specified by the value of 'name'

    Returns:
        an instance of the specified {class_type:s} class
    
    Raises:
        ValueError: if the input 'name' parameter value is not a correct {class_type:s} class name or pseudo-name.
    
    Notes:
        Check the 'create' method of each class to know more about the possible specific (args, kwargs) values
    """.format(class_type=class_family, possible_names=possible_names_string)
    fct_name = "create_{}".format(class_family)
    check_class_name_fct = define_check_item_name_function(name2class, class_family)
    def _create_fct(name, *args, **kwargs):
        class_ = check_class_name_fct(name)
        return class_.create(*args, **kwargs)
    _create_fct.__doc__ = fct_docstring
    _create_fct.__name__ = fct_name
    return _create_fct

def define_create_from_factory_configuration_function(name2class, item_type):
    """ Creates a function whose role is to create an instance of class belonging to a specific 
    family of classes, from a suitable 'factory configuration' input.
    
    Args:
        possible_class_name2class: a "possible_class_name => class" map
        class_family: string, specifying how to call the class family, must be suitable to be part 
            of a python's function name (ex: 'resolver')
    
    Returns:
        the "from factory configuration class instance creation" function
    """
    possible_names_string = _get_listing_string_for_docstring_creation(name2class, 16)
    fct_docstring = """
    Creates an instance of a {class_type:s} class.
    
    Args:
        factory_configuration: mapping, must contain the 'name' and 'config' fields, where name 
            is a valid {class_type:} class name or surname, and 'config' is the specific configuration of 
            the specified {class_type:} class instance to create. Possible values for the 'name' 
            field are:
{possible_names:}.
    
    Returns:
        an instance of the specified {class_type:s} class specified by the input factory configuration
    
    Raises:
        ValueError: if the input 'name' parameter value is not a correct {class_type:s} class name or pseudo-name.
    """.format(class_type=item_type, possible_names=possible_names_string)
    fct_name = "create_{}_from_factory_configuration".format(item_type)
    check_class_name_fct = define_check_item_name_function(name2class, item_type)
    def _factory(factory_configuration):
        name = factory_configuration["name"].strip()
        class_ = check_class_name_fct(name)
        configuration = add_default_value_configuration_if_non_existent(factory_configuration, "config", Mapping)
        return class_.from_configuration(configuration)
    _factory.__doc__ = fct_docstring
    _factory.__name__ = fct_name
    return _factory

def define_generate_factory_configuration_function(name2class, item_type):
    """ Creates a function whose role is to create a factory configuration that encode the parameters 
    of an instance of a class belonging to a specific family of classes, from inputs corresponding to 
    the name of the class, as well as parameters that may be needed, or can be used, to parametrize 
    the specific instance.
    
    Args:
        possible_class_name2class: a "possible_class_name => class" map
        class_family: string, specifying how to call the class family, must be suitable to be part 
            of a python's function name (ex: 'resolver')

    Returns:
        the "from name and arguments, specific class family, factory configuration creation" function
    """
    possible_names_string = _get_listing_string_for_docstring_creation(name2class, 16)
    fct_docstring = """
    Creates a proper {class_type:} class instance factory configuration from the input, that can be 
    used to instantiate the corresponding {class_type:} class instance using, for instance, the 
    'create_{class_type}_from_factory_configuration' function.
    
    Args:
        name: string, the name or surname of the class to instantiate. Possible names or 
            surnames are:
{possible_names:}
        args: args to pass to the 'create' method of the {class_type:s} class specified by the value of 'name'
        kwargs: kwargs to pass to the 'create' method of the {class_type:s} class specified by the value of 'name'

    Returns:
        the factory configuration mapping corresponding to the input
    
    Raises:
        ValueError: if the input 'name' parameter value is not a correct {class_type:s} class name or pseudo-name.
    
    Notes:
        Check the 'create' method of each class to know more about the possible specific (args, kwargs) values
    """.format(class_type=item_type, possible_names=possible_names_string)
    fct_name = "generate_{}_factory_configuration".format(item_type)
    check_class_name_fct = define_check_item_name_function(name2class, item_type)
    def _generate_factory_configuration(name, *args, **kwargs):
        class_ = check_class_name_fct(name)
        return class_.define_factory_configuration(*args, **kwargs)
    _generate_factory_configuration.__doc__ = fct_docstring
    _generate_factory_configuration.__name__ = fct_name
    return _generate_factory_configuration

def create_mapping_if_input_is_not_Mapping_instance(item_):
    """ Returns an empty, new Mapping instance if the input is not a Mapping instance.
    
    Args:
        item: python object
    
    Returns:
        a Mapping instance
    """
    if not isinstance(item_, Mapping):
        return Mapping()
    return item_

def add_default_value_configuration_if_non_existent(configuration, field_name, default_value):
    """ Returns either the value corresponding to the specified field of the input configuration object, 
    or the default value associated to this field, after having set it in the configuration object, if 
    this configuration object did not possess the specified field before.
    
    Args:
        configuration: mapping
        field_name: string, name of the configuration's field whose value one wants to fetch, or 
            set in the configuration with its default value if it does not exist in it yet
        default_value: value, or no-parameter taking callable; if callable, assume that a call to 
            this callable will produce the desired default value to set in the configuration; value to be 
            set in the input configuration if the specified field is not present in it

    Returns:
        the value associated to the specified field of the input configuration
    """
    try:
        return configuration[field_name]
    except UnknownMappingFieldError:
        default_value = default_value if not callable(default_value) else default_value()
        configuration[field_name] = default_value
        return default_value


#################################################################
# Helper functions for class attribute definition data creation #
#################################################################
def _get_listing_string_for_docstring_creation(name2class, spaces_nb):
    """ Creates a string to be inserted in the docstring of a function, and representing a list of 
    possibles values for the name of a class to be used.
    
    Args:
        name2class: a "name => class" map
        spaces_nb: an int, the number of spaces that must exist before each list item string 
            representation, in the final docstring
    
    Returns:
        a string
    """
    ordered_possible_values = tuple(name2class.keys())
    blanks = " "*int(spaces_nb)
    possible_names_string="\n".join("{}- '{}' or '{}'".format(blanks, *sl[::-1]) for sl in (ordered_possible_values[2*i:2*(i+1)]\
                                                                                                for i in range(int(len(ordered_possible_values)/2))
                                                                                                )
                                        )
    return possible_names_string

def _create_simple_get_parameter_value_from_configuration(attribute_name, default=_OWN_NONE, postprocess_fct=None):
    """ Creates a function that takes a configuration as an input, and returns the value associated to 
    the specified field in the configuration, potentially after going through a 'postprocessing' 
    function. If a default value to be used is specified, then, instead of raising an exception if 
    the specified field is not present in the configuration, the default value will be returned. 
    
    Args:
        attribute_name: name of the field of the configuration whose value should be fetched when 
            calling the function created by this function
        default: if this parameter is not used by the user, it means that the user wants an 
            exception to be raised if the input 'attribute_name' parameter value is not a field of the 
            configuration. If the user set it to a value other than its default value '_OWN_NONE', then this 
            is the value that will be used / returned in this case. It can also be a no-parameter taking 
            callable, in which case the used / returned value will be the value returned by a call to the callable.
        postprocess_fct: callable, or None; if not None, the value to be returned by the function 
            created by this function will first go through this postprocessing function, event if it was the 
            specified default value.
    
    Returns:
        a callable which does what is described above
    """
    if default is _OWN_NONE:
        _getter_fct = lambda configuration: configuration[attribute_name]
    else:
        # If default value should be things such as an empty list or an empty dict, we assume the passed parameter is a callable that create a new instance when called
        try:
            default_value = default()
        # Else, if it was not a callable, we assume it is an immutable object, such as a number or a string
        except TypeError as e:
            s = e.args[0]
            if re.search("not callable", s):
                default_value = default
            else:
                raise
        _getter_fct = lambda configuration: type(configuration).get(configuration, attribute_name, default_value)
    if postprocess_fct is None:
        return _getter_fct
    return (lambda configuration: postprocess_fct(_getter_fct(configuration)))

def _create_simple_set_parameter_value_in_configuration(attribute_name):
    """ Creates a function that take a python object as an input, python object which is assumed to 
    possess a 'configuration' attribute value which is a configuration, and create an attribute in 
    this python object which goes by the attribute name specified as an input, and whose value shall 
    be the value of the 'attribute_name' field of the configuration mentioned above, which is assumed 
    to possess such a field. If the attribute already exists in the python object, then its value be 
    replaced.
    
    Args:
        attribute_name: name of the attribute and of the field of the configuration to consider

    Returns:
        a callable which does what is described above
    """
    return (lambda instance: setattr(instance.configuration, attribute_name, getattr(instance, attribute_name)))

def _create_attribute_data(attribute_name, set_parameter_value_in_configuration_fct=None, 
                           get_parameter_value_from_configuration_fct=None,
                           default_value=_OWN_NONE, postprocess_fct=None):
    """ Assembles a tuple defining an attribute name, as well as the getter and setter functions used 
    to interact, with respect to an attribute going by this name, between the configuration attribute 
    value of an object, and the attribute of this object going by the specified name. This represents 
    the base tools used to ensure a synchronization between the attribute value of a python object, 
    and its representation in the configuration associated to this object.
    
    Args:
        attribute_name: name of the attribute and of the field of the configuration to consider
        set_parameter_value_in_configuration_fct: callable, or None, function to use to set the 
            attribute value of a python object from the field going by the same name of its configuration 
            attribute value, using the specified attribute name. If None, will be the result of calling the 
            '_create_simple_set_parameter_value_in_configuration' function with the input attribute name
        get_parameter_value_from_configuration_fct: callable, or None, function to use to extract 
            a value from a configuration. If None, will be the result of calling the 
            '_create_simple_get_parameter_value_from_configuration' function with the input attribute name, 
            as well as the 'default_value' and '' inputs
        default_value: see the '' function, to be used to create the 
            'get_parameter_value_from_configuration_fct' to use if the 
            'get_parameter_value_from_configuration_fct' input is None
        postprocess_fct: see the '' function, to be used to create the 
            'get_parameter_value_from_configuration_fct' to use if the 
            'get_parameter_value_from_configuration_fct' input is None

    Returns:
        an (attribute_name, (get_parameter_value_from_configuration_fct, set_parameter_value_in_configuration_fct)) tuple
    """
    if get_parameter_value_from_configuration_fct is None:
        if postprocess_fct is None and default_value is not _OWN_NONE and default_value is not None:
            postprocess_fct = type(default_value)
        get_parameter_value_from_configuration_fct = _create_simple_get_parameter_value_from_configuration(attribute_name, 
                                                                                                           default=default_value, 
                                                                                                           postprocess_fct=postprocess_fct)
    if set_parameter_value_in_configuration_fct is None:
        set_parameter_value_in_configuration_fct = _create_simple_set_parameter_value_in_configuration(attribute_name)
    return (attribute_name, (get_parameter_value_from_configuration_fct, set_parameter_value_in_configuration_fct))
