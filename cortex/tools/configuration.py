# -*- coding: utf-8 -*-

# Copyright 2004-2010 by Vinay Sajip. All Rights Reserved.
# Modified by François Noyez (June-July 2017)

# Permission to use, copy, modify, and distribute this software and its
# documentation for any purpose and without fee is hereby granted,
# provided that the above copyright notice appear in all copies and that
# both that copyright notice and this permission notice appear in
# supporting documentation, and that the name of Vinay Sajip
# not be used in advertising or publicity pertaining to distribution
# of the software without specific, written prior permission.
# VINAY SAJIP DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
# ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL
# VINAY SAJIP BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
# ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
# IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
# OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

"""
This is a configuration module for Python.

This module should work under Python versions >= 3.0, and cannot be used with
earlier versions since it uses new-style classes.

Development and testing has only been carried out (so far) on Python 3.5.

/!\\\\ The following link is for the version developed by Vinay Sajip for python 2.2.
See the test module (test_config.py) included in the distribution 
(http://www.red-dove.com/python_config.html, follow the download link).

A simple example - with the example configuration file::

    messages:
    [
      {
        stream : `sys.stderr`
        message: 'Welcome'
        name: 'Harry'
      }
      {
        stream : `sys.stdout`
        message: 'Welkom'
        name: 'Ruud'
      }
      {
        stream : $messages[0]._stream
        message: 'Bienvenue'
        name: Yves
      }
    ]

a program to read the configuration would be::

    from configuration import Configuration

    with open('simple.cfg') as stream:
        cfg = Configuration.load(stream)
    
    for m in cfg.messages:
        s = "{!s}, {!s}".format(m.message, m.name)
        try:
            print(s, file=m.stream)
        except IOError as e:
            print(e)

which, when run, would yield the console output::

    Welcome, Harry
    Welkom, Ruud
    Bienvenue, Yves

Warnings:
    Not suitable for values whose string representation uses operation characters, such as 1e+27, or 2e-6. 


/!\\\\ The following link is for the version developed by Vinay Sajip for python 2.2.

See this tutorial (http://www.red-dove.com/python_config.html) for more information.


Version:
    0.3.0


Author:
    François NOYEZ (Vinay Sajip from the version for python2, which this version is based on / adapted from)

Copyright:
    - Copyright (C) 2004-2010 Vinay Sajip. All Rights Reserved.
    - Copyright (C) 2017-2018 François NOYEZ. All Rights Reserved.


TODO:

- Respect all PEP8 specifications that can be?
- Improve documentation?
- Overhaul unittests?


Regarding the stream to be used in with this module's tools:

    For the configuration parser being considered, parenthesis in a string are associated with operations; 
    as such, there is no possibility to use a tuple as a value.
    Each time you want to put a collection of item as a field value, you must use the Sequence class. 
    In fact, if you try to input a list or a tuple as a field value of a Mapping instance, or as a value 
    of a Sequence instance, it will be automatically converted to a Sequence instance before being set.
    In the same way, if a dict is used as a field value in the configuration stream, it will be 
    interpreted as a Mapping instance once the stream is parsed.


When saving to a stream, all the module asks is that the stream object implement a 'write' method 
that takes only a string as a compulsory argument.

When parsing a Configuration from a stream, the stream needs to implement the following 
interface:
    - 'read' method that takes as sole compulsory argument a number (int) of str character to read, and 
      return a string (must return an empty string if there is nothing more to read)
    - 'readline' method that takes no argument, and return a string

A stream opened by a custom function passed as an argument to the 'load' method of the Configuration 
class must implement the two methods described above, and must be able to be closed upon the call 
to a 'close' method that takes no argument.
"""


__author__  = "François NOYEZ <francoisnoyez@gmail.com>"
__status__  = "alpha"
__version__ = "0.4.0"
__date__    = "07 December 2018"

__all__ = ["MappingError", "MappingFormatError", "ConfigurationResolutionError", 
           "UnknownMappingFieldError", "PossiblePythonMagicAttributeNameError", "ReferenceError", 
           "Container", "Mapping", "Namespace", "Configuration", "Sequence", "Reference", "Expression", 
           "ConfigurationReader", "MappingMerger", "MappingList",
           "save_mapping", "load_mapping", "load_configuration", "convert_configuration_to_mapping",
           ]

import logging
import os
import sys
import itertools
from collections import OrderedDict

WORD = "a"
NUMBER = "9"
STRING = "\""
EOF = ""
LCURLY = "{"
RCURLY = "}"
LBRACK = "["
LBRACK2 = "a["
RBRACK = "]"
LPAREN = "("
LPAREN2 = "(("
RPAREN = ")"
DOT = "."
COMMA = ","
COLON = ":"
AT = "@"
PLUS = "+"
MINUS = "-"
STAR = "*"
SLASH = "/"
MOD = "%"
BACKTICK = "`"
DOLLAR = "$"
TRUE = "True"
FALSE = "False"
NONE = "None"

WORD_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_"

NEWLINE = "\n"
if sys.platform == "win32":
    NEWLINE = "\r\n"
elif os.name == "mac":
    NEWLINE = "\r"

INDENT_STEP = "    "

class MappingError(Exception):
    """ This is the base class of exceptions raised by this module. """
    pass

class MappingFormatError(MappingError):
    """ This is the base class of exceptions raised due to syntax errors in configurations. """
    pass

class ConfigurationResolutionError(MappingError):
    """ This is the base class of exceptions raised due to semantic errors in configurations. """
    pass

class UnknownMappingFieldError(MappingError):
    """ Exception class to be used when a Mapping instance specific field is not found. """
    def __init__(self, field_name, *args, **kwargs):
        self.field_name = field_name
    def __str__(self):
        return "UnknownMappingFieldError: no field by the name '{}' in the mapping.".format(self.field_name)

class PossiblePythonMagicAttributeNameError(Exception):
    """ Exception class to be used when trying to access a Mapping instance field with a name that 
    can that of a python magic attribute.
    """
    pass


def _get_attribute(instance, attribute_name):
    """ Uses 'object.__getattribute__' to directly access the attribute of a python object (useful 
    for Mapping and Configuration instances).
    
    Args:
        instance: a python object
        attribute_name: the name of the attribute of the instance whose value is to be fetched

    Returns:
        the value of the 'attribute_name' attribute of the input instance, if it has one. If it 
        does not, an exception will be raised.
    """
    return object.__getattribute__(instance, attribute_name)

def _is_word(s):
    """ Tests whether or not the input value is an identifier. If the value passed in is not a
    string, False is returned. An identifier consists of alphanumerics or underscore characters.
    
    Args:
        s: string, the name to be tested

    Returns:
        boolean, True if a word, else False
    
    Examples:
    
        .. code:: python
        
            >>> _is_word('a word')
            False
            >>> _is_word('award')
            True
            >>> _is_word(9)
            False
            >>> _is_word('a_b_c_')
            True

    Notes:
        _is_word('9abc') will return True - not exactly correct, but adequate for the way it's used here.
    """
    if type(s) != type(""):
        return False
    s = s.replace("_", "")
    return s.isalnum()

def get_path(container):
    """ Uses the parent genealogy of a Container instance to build the attribute name / index path 
    that leads to it from the oldest parent.
    
    Args:
        container: a Container instance:

    Returns:
        string, the desired path (empty string "" if the container has no parent)
    
    Examples:
        
        .. code:: python
            
            >>> get_path(container)
            ancestor.child.subchild[2].container
    """
    if not isinstance(container, Container):
        raise TypeError("'get_path': input is not a Container instance.")
    parent = _get_attribute(container, "parent")
    suffixes = []
    while parent is not None:
        suffixes.append(_get_attribute(container, "suffix"))
        container = parent
        parent = _get_attribute(container, "parent")
    path = ""
    if suffixes:
        path_items = itertools.chain(suffixes[-1:], (suffix if suffix[0] == "[" else ".{}".format(suffix) for suffix in suffixes[1::-1]))
        path = "".join(path_items)
    return path

def _convert_input(value):
    """ Converts list/tuple to Sequence instance, and dict to Mapping instance
    
    Args:
        value: a python object

    Returns:
        the created Container instance if it was necessary, else the input object
    """
    if isinstance(value, (list, tuple)):
        return Sequence(value)
    if isinstance(value, dict):
        return Mapping(value.items())
    return value


class Container(object):
    """ This internal class is the base class for mapping and sequence classes.
    
    Attributes:
        parent: The parent of this instance in the hierarchy.
        suffix: A string which describes how to get this instance from the variable containing 
            its parent in python code (ex: parent.suffix or parent['suffix'] if parent is a Mapping 
            instance, or parent[suffix] if parent is a Sequence instance)
    """
    def __init__(self):
        _get_attribute(self, "_reinitialize_parent")()
    
    def _reinitialize_parent(self):
        """ Sets 'parent' and 'suffix' attribute to their default value. """
        _get_attribute(self, "_set_parent")(None, "")
    
    def _set_parent(self, parent, suffix):
        """ Sets 'parent' and 'suffix' attribute to the corresponding input values.
        
        Args:
            parent: a Container instance, the parent of this Container instance in the hierarchy.
            suffix: string, the suffix identifying the relation between this instance and its parent
        """
        object.__setattr__(self, "parent", parent)
        object.__setattr__(self, "suffix", suffix)
    
    def _evaluate(self, item):
        """ Evaluates items which are Reference or Expression instances, that is, returns the value 
        they represent.
        
        Returns other items directly.

        Reference instances are evaluated using 'Reference.resolve', and Expression instances 
        are evaluated using 'Expression._evaluate'.
        
        Args:
            item: the python object to be evaluated
        
        Returns:
            If the item is an instance of Reference or Expression, the evaluated value 
            is returned, otherwise the item is returned unchanged.
        """
        if isinstance(item, Reference):
            return item.resolve(self)
        elif isinstance(item, Expression):
            return item.evaluate(self)
        return item
    
    # Cannot use the 'abc' module, because of the nature of Mapping class, which inherit the Container class 
    # 'abc' seems to expect to be able to add methods to Container instances, but those will not
    # be able to be called on a Mapping instance, because of its special '__getattr__' method
    def _write_to_stream(self, stream, indent, container):
        """ Writes this instance to a stream at the specified indentation level.
        
        Args:
            stream: writable stream (file-like object), the stream to write to
            indent: non-negative int, the indentation level
            container: Container instance, the container of this instance
        """
        raise NotImplementedError

    def _write_value(self, value, stream, indent):
        """ Implements the different way of writing a value to a stream at a given indentation level 
        according to the type of the value (Mapping, Expression, Reference, etc.).
        
        Args:
            value: a python object
            stream: a stream-like object implementing the 'write' method
            indent: non-negative int, how many indentation step to use before writing the first 
                string line of the string representation of the value
        """
        if isinstance(self, Mapping):
            indent_string = " "
        else:
            indent_string = indent * "  "
        if isinstance(value, Reference):
            value_to_write = value.resolve(self)
            value_string = "{!s}{!r}{!s}".format(indent_string, value_to_write, NEWLINE)
        elif isinstance(value, Expression):
            value_to_write = value.evaluate(self)
            value_string = "{!s}{!r}{!s}".format(indent_string, value_to_write, NEWLINE)
        else:
            if isinstance(value, str): # and not _is_word(value):
                value = repr(value)
            value_string = "{!s}{!s}{!s}".format(indent_string, value, NEWLINE)
        stream.write(value_string)
    
    @staticmethod
    def _relinquish_parental_control(value):
        """ Removes the parental link of the input value if it is a Container instance.
        
        Args:
            value: a Container instance
        """
        if isinstance(value, Container):
            _get_attribute(value, "_reinitialize_parent")()






class Mapping(Container):
    """ This internal class implements key-value mappings in configurations.
    
    A correct key must not be of the form "__...__", so as to allow python to still carry out its 
    magic on an instance of this class.
    
    In order to access an attribute of an instance of this class, one must use the following 
    scheme:
        >>> object.__getattr__(mapping, attribute_name)
        
    Arguments:
        key_value_pairs_iterable: if not None, the Mapping instance will be filled with the 
            key and value gotten from reading this (key; value) pairs iterable
    
    Attributes:
        parent: the parent of this instance in the mapping hierarchy
        suffix: a string which describes how to get this instance from the variable containing 
            its parent in python code (ex: parent.suffix or parent['suffix'] if parent is a Mapping 
            instance, or parent[suffix] if parent is a Sequence instance)
        data: an ordered dict implementing the link between the keys and (value, comment) pairs 
            of the mapping
    """
    
    def __init__(self, key_value_pairs_iterable=None):
        super().__init__()
        object.__setattr__(self, "data", OrderedDict())
        if key_value_pairs_iterable is not None:
            if isinstance(key_value_pairs_iterable, dict):
                key_value_pairs_iterable = key_value_pairs_iterable.items()
            elif isinstance(key_value_pairs_iterable, Mapping):
                key_value_pairs_iterable = Mapping.items(key_value_pairs_iterable)
            for key, value in key_value_pairs_iterable:
                self[key] = value
    
    @staticmethod
    def _check_correct_key(key):
        """ Checks wether or not the input object can male a correct key for the Mapping class 
        according to the specification developped in the class' docstring.
        
        Args:
            key: a python object

        Returns:
            (boolean, str or None) pair, if the boolean is False, the string gives an 
            indication of why, else it is None
        """
        if type(key) != str:
            return False, "type"
        if key.startswith("__") and key.endswith("__"):
            return False, "magic_name"
        return True, None
    
    # Setter methods
    def _add_mapping(self, key, value, comment, setting=False):
        """ Adds a key-value mapping with a comment.
        
        Args:
            key: string, the key for the mapping
            value: python object, the value for the mapping
            comment: string, or None; the comment for the key
            setting: boolean; If True, ignore clashes. This is set to true when called from the 
                '__setattr__' method.
        
        Raises:
            MappingFormatError: if an existing key is seen again and setting is False
        """
        is_correct_key, reason_msg = _get_attribute(self, "_check_correct_key")(key)
        if not is_correct_key:
            if reason_msg == "type":
                raise TypeError("Incorrect key type ('{}'), must be of type 'str'.".format(type(key)))
            else:
                # Assume the user wants to replace one of python's magic attribute. Let python punish the user for its insolence.
                #object.__setattr__(self, key, value)
                msg = "Wrong mapping key ('{}'), must not be susceptible to be a magic python "\
                      "attribute name. Make sure the mapping key are strings that do not begin or end with '__'"
                msg = msg.format(key)
                raise PossiblePythonMagicAttributeNameError(msg)
        else:
            data = _get_attribute(self, "data")
            old_value_comment_pair = None
            if key in data:
                if not setting:
                    raise MappingFormatError("Repeated key: {!s}".format(key))
                old_value_comment_pair = data[key]
            value = _convert_input(value)
            data[key] = (value, comment)
            # Remove parenting if necessary
            if old_value_comment_pair is not None:
                old_value, _ = old_value_comment_pair #comment
                _get_attribute(self, "_relinquish_parental_control")(old_value)
            # Add parenting for new values (some of which may be old values, if the user feels feisty, which is why we do that after removing the parenting for the old values)
            if isinstance(value, Container):
                _get_attribute(value, "_set_parent")(self, key)
    
    def __setattr__(self, name, value):
        _get_attribute(self, "_add_mapping")(name, value, None, True)
    
    def __setitem__(self, key, value):
        _get_attribute(self, "_add_mapping")(key, value, None, True)
    
    # Getter methods
    def _get_mapped_value(self, key):
        """ Get a value through its key.
        
        Args:
            key: string, the key for the mapping
        
        Returns:
            python object, the value corresponding to the key
        
        Raises:
            AttributeError: if no such key exists in the configuration
        """
        is_correct_key, reason_msg = _get_attribute(self, "_check_correct_key")(key)
        if not is_correct_key:
            if reason_msg == "type":
                raise TypeError("Incorrect key type ('{}'), must be of type 'str'.".format(type(key)))
            else:
                object.__getattribute__(self, key)
        else:
            data = _get_attribute(self, "data")
            try:
                value, _ = data[key] #comment
            except KeyError:
                message = "No attribute exists corresponding to the name '{}'; is it truly a mapping field name?"
                message = message.format(key)
                #raise AttributeError(message)# from None
                raise UnknownMappingFieldError(key, message) # from None
            return _get_attribute(self, "_evaluate")(value)
    
    def __getattribute__(self, key):
        return _get_attribute(self, "_get_mapped_value")(key)
    
    def __getitem__(self, key):
        return _get_attribute(self, "_get_mapped_value")(key)#self._get_mapped_value(key)
    
    # Deleter methods
    def _del_mapping(self, key):
        """ Removes a key-value pair from this Mapping instance
        
        Args:
            key: string, the key for the mapping.
        
        Raises:
            AttributeError: If no such key exists in the configuration.
        """
        is_correct_key, reason_msg = _get_attribute(self, "_check_correct_key")(key)
        if not is_correct_key:
            if reason_msg == "type":
                raise TypeError("Incorrect key type ('{}'), must be of type 'str'.".format(type(key)))
            else:
                object.__delattr__(self, key)
        else:
            data = _get_attribute(self, "data")
            if key not in data:
                msg = "No attribute exists corresponding to the name '{}'; is it truly a mapping field name?"
                msg = msg.format(key)
                raise AttributeError(msg)# from None
            value, _ = data[key] #comment
            del data[key]
            _get_attribute(self, "_relinquish_parental_control")(value)
    
    def __delattr__(self, key):
        _get_attribute(self, "_del_mapping")(key)
    
    def __delitem__(self, key):
        _get_attribute(self, "_del_mapping")(key)
    
    # Instance methods
    def _write_to_stream(self, stream, indent, container):
        """ Writes this instance to a stream at the specified indentation level.

        May be redefined in subclasses.

        Args:
            stream: A writable stream (file-like object), the stream to write to
            indent: int, the indentation level
            container: Container instance, the parent container of this instance
        """
        indstr = indent * INDENT_STEP
        if len(self) == 0:
            stream.write(" { }%s" % NEWLINE)
        else:
            if isinstance(container, Mapping):
                stream.write(NEWLINE)
            stream.write("%s{%s" % (indstr, NEWLINE))
            type(self).save(self, stream, indent + 1)
            stream.write("%s}%s" % (indstr, NEWLINE))
    
    # Other object methods
    def __contains__(self, item):
        return item in _get_attribute(self, "data")
    
    def __len__(self):
        return len(_get_attribute(self, "data"))

    def __iter__(self):
        """ Return an iterator over the keys of the Mapping instance.
        
        Returns:
            iterator of keys of the Mapping instance
        """
        return iter(_get_attribute(self, "data"))
    
    def _inner_eq(self, other):
        """ Checks whether or not the input object can be said to be equal in value to this Mapping 
        instance.
        
        Args:
            key: a python object

        Returns:
            (boolean, str or None) pair, if the boolean is False, the string gives an 
            indication of why, else it is None
        """
        if not isinstance(other, type(self)):
            return False, "Not {} class instance: type = '{}'".format(type(self).__name__, type(other))
        return Mapping._detailed_equality(self, other)
    
    def __eq__(self, other):
        result, _ = _get_attribute(self, "_inner_eq")(other) #reason_message
        return result
    
    def __ne__(self, other):
        return not self == other
    
    def __hash__(self):
        raise TypeError("unhashable type: '{}'".format(type(self).__name__))
    
    def __str__(self):
        """ Produces a readable string representation of this Mapping instance.
        
        Returns:
            string
        """
        strings_array = []
        for key, (value, _) in sorted(_get_attribute(self, "data").items()): #comment
            if isinstance(value, (Mapping, Sequence, Reference, Expression)):
                string = str(value)
            else:
                string = repr(value)
            inner_strings_array = string.strip().split("\n")
            inner_strings_array = tuple(itertools.chain(("{}'{}': {}".format(INDENT_STEP, key, inner_strings_array[0]),), 
                                                        ("{}{}".format(INDENT_STEP, s) for s in inner_strings_array[1:])
                                                        )
                                        )
            strings_array.extend(inner_strings_array)
        return "%s({\n%s\n})" % (type(self).__name__, "\n".join(strings_array))
    
    def __repr__(self):
        """ Give an unambiguous string representation of this Mapping instance, in one string line """
        data = _get_attribute(self, "data")
        return "%s({%s})" % (type(self).__name__, ", ".join("{}: {}".format(repr(key), repr(value))\
                                                             for key, (value, _) in sorted(data.items()))) #comment
    
    def get_base_hash_string(self):
        """ Produces a normalized string representation of this Mapping instance, which shall serves 
        to unequivocally identify it.
        
        Returns:
            string
        """
        strings_array = []
        for key in sorted(_get_attribute(self, "data").keys()):
            value = self[key]
            if isinstance(value, (Mapping, Sequence)):
                value_string = type(value).get_base_hash_string(value)
            else:
                value_string = repr(value)
            string = "{}:\n{}".format(key, value_string)
            strings_array.append(string)
        return "\n".join(strings_array)
    
    # Class methods
    @classmethod
    def get(cls, mapping, key, default=None):
        """ Implements a dictionary-style 'get' operation on the input mapping instance.
        
        Args:
            mapping: a Mapping instance
            key: hashable object, representing a possible field of the input mapping
            default: default value to be associated to the key, and returned if the input mapping 
            has currently no value associated to the input key
        
        Returns:
            a python object
        """
        try:
            value = mapping[key]
        except UnknownMappingFieldError:
            value = default
        return value
    
    @classmethod
    def items(cls, mapping):
        """ Implements a dictionary-style 'items' operation on the input mapping instance.
        
        Creates a generator over (key, value) pairs.
        
        Args:
            mapping: a Mapping instance
        
        Yields:
            a (key, value) pair
        """
        for key in mapping:
            yield key, mapping[key]
    
    @classmethod
    def update(cls, mapping1, mapping2):
        """ Updates mapping1 with (key, value) pairs from mapping2.
        
        Args:
            mapping1: a Mapping instance, which shall be updated
            mapping2: a Mapping instance, which shalle provide the (key, value) pairs to update with
        """
        configuration_merger = MappingMerger(merge_resolver=carry_out_absolute_overwrite_merge_resolution)
        configuration_merger.merge(mapping1, mapping2)
        return mapping1
    
    @classmethod
    def save(cls, mapping, stream_or_file_path, indent=0):
        """ Saves a Mapping instance to the specified stream.
        
        Args:
            mapping: a Mapping instance, the mapping to persist to the stream
            stream_or_file_path: A write-only stream (file-like object), or a string., a stream to which the configuration is written, or a path to a 
                'utf-8'-encoded file used as a stream
            indent: int, the indentation level for the output.
        """
        if isinstance(stream_or_file_path, str):
            file_path = stream_or_file_path
            with open(file_path, "w", encoding="utf-8") as stream:
                Mapping.save(mapping, stream, indent=indent)
        
        else:
            stream = stream_or_file_path
            indstr = indent * INDENT_STEP
            data = _get_attribute(mapping, "data")
            maxlen = 0
            for key, (value, comment) in data.items():
                # Write comment one exists
                if _is_word(key):
                    skey = key
                else:
                    skey = repr(key)
                if comment:
                    stream.write("{!s}#{!s}".format(indstr, comment))
                # Write the key
                stream.write("%s%-*s :" % (indstr, maxlen, skey))
                # Write the value
                if isinstance(value, Container):
                    _get_attribute(value, "_write_to_stream")(stream, indent, mapping)
                else:
                    try:
                        _get_attribute(mapping, "_write_value")(value, stream, indent)
                    except ConfigurationResolutionError as e:
                        msg = e.args[0]
                        new_msg = "Indent level={}, key='{}': {}".format(indent, key, msg)
                        raise ConfigurationResolutionError(new_msg) from None
                        
    
    @classmethod
    def _detailed_equality(cls, mapping1, mapping2):
        """ Tests that the input Mapping instances are equal.
        
        That means 
        that:
            - they have the same length
            - they share the same keys
            - their respective value associated to a specific key are equal
        
        Args:
            mapping1: Mapping instance, first element of the comparison
            mapping2: Mapping instance, second element of the comparison

        Returns:
            a (boolean, string or None) pair; the second item of the pair will be None if the 
            boolean value is True (both inputs were found to be equal), or will be a string specifying 
            the reason for which the inputs were found not to be equal otherwise
        """
        l1 = len(mapping1)
        l2 = len(mapping2)
        if l1 != l2:
            return False, "Difference of length: '{}' v. '{}'".format(l1, l2)
        s1 = set(_get_attribute(mapping1, "data"))
        s2 = set(_get_attribute(mapping2, "data"))
        diff = s1.symmetric_difference(s2)
        if diff:
            return False, "The following key value(s) exist(s) in only one of the two Mappings: {}.".format(tuple(diff))
        keys = tuple(sorted(s1))
        for key in keys:
            value1 = mapping1[key]
            value2 = mapping2[key]
            if (isinstance(value1, Mapping) or isinstance(value2, Mapping)):
                if not Mapping.equality(value1, value2):
                    return False, "Different values for key '{}'".format(key)
            elif value1 != value2:
                return False, "Different values for key '{}'".format(key)
        return True, ""
    
    @classmethod
    def equality(cls, mapping1, mapping2):
        """ Tests that the inputs python objects are equal Mapping instances.
        
        That means 
        that:
            - both instances are Mapping or Mapping subclass instances
            - they have the same length
            - they share the same keys
            - their respective value associated to a specific key are equal
        
        Args:
            mapping1: Mapping instance, first element of the comparison
            mapping2: Mapping instance, second element of the comparison

        Returns:
            a boolean
        """
        result, _ = cls._detailed_equality(mapping1, mapping2) #message
        return result
    
    @classmethod
    def load(cls, stream_or_stream_uri, open_stream_from_string_fct=None):
        """ Parses a mapping from a stream, or from a string using the given function if one 
        is provided
        
        Args:
            stream_or_stream_uri: stream to be parsed (so the module's docstring to see which 
                interface is expected), or string; if string, will be used in conjunction with a function 
                to create a string if provided a string
            open_stream_from_string_fct: function to use to create a string, or None. It will be 
                used if the first argument is a string, or when it meets in the parsed configuration a 
                reference to a configuration present in another stream (stream which is then identified by a 
                string; ex: @another_stream.option21.option2). If None, the default function that will be 
                used if one that assumes that the string is a filepath, which will then be used to open the 
                corresponding file using the 'utf-8' encoding

        Returns:
            a Mapping instance  
        """
        # Since, for now, a saved configuration does not refer to namespaces, we can use the 'load' method defined for a Configuration instance
        return convert_configuration_to_mapping(Configuration.load(stream_or_stream_uri, 
                                                                   open_stream_from_string_fct=open_stream_from_string_fct))
    

class Namespace(object):
    """ This class is used for implementing default namespaces to be held by a Configuration instance.
    
    An instance acts as a namespace.
    
    Attributes:
        sys: the 'sys' python module
        os: the 'os' python module
    """
    def __init__(self):
        self.sys = sys
        self.os = os
    
    def __eq__(self, other):
        if not isinstance(other, Namespace):
            return False
        if vars(self) != vars(other):
            return False
        return True
    
    def __ne__(self, other):
        return not self == other
    
    def __hash__(self):
        raise TypeError("unhashable type: '{}'".format(type(self).__name__))
    
    def __repr__(self):
        return "Namespace('{!s}')".format(",".join(sorted(self.__dict__.keys())))

class Configuration(Mapping):
    """ This class represents a configuration, and is the only one which clients need to interface to, 
    under normal circumstances.
    
    In order to access an attribute of an instance of this class, one must use the following 
    scheme:
        >>> object.__getattr__(mapping, attribute_name)
        
    Arguments:
        key_value_pairs_iterable: if not None, the Mapping instance will be filled with the 
            key and value gotten from reading this (key; value) pairs iterable
    
    Attributes:
        parent: The parent of this instance in the mapping hierarchy.
        suffix: A string which describes how to get this instance from the variable containing 
            its parent in python code (ex: parent.suffix or parent['suffix'] if parent is a Mapping 
            instance, or parent[suffix] if parent is a Sequence instance)
        data: An ordered dict implementing the link between the keys and (value, comment) pairs 
            of the mapping.
        namespaces: collection of Namespaces instances to be associated to the configuration
    """

    def __init__(self, key_value_pairs_iterable=None):
        super().__init__(key_value_pairs_iterable=key_value_pairs_iterable)
        object.__setattr__(self, "namespaces", [Namespace()])
        object.__setattr__(self, "_resolving", set())
    
    # Instance methods
    def __str__(self):
        strings_array = []
        for key, (value, _) in _get_attribute(self, "data").items(): #comment
            if isinstance(value, (Mapping, Sequence, Reference, Expression)):
                string = str(value)
            else:
                string = repr(value)
            inner_strings_array = string.strip().split("\n")
            inner_strings_array = tuple(itertools.chain(("{}'{}': {}".format(INDENT_STEP, key, inner_strings_array[0]),), 
                                                        ("{}{}".format(INDENT_STEP, s) for s in inner_strings_array[1:])
                                                        )
                                        )
            strings_array.extend(inner_strings_array)
        return "%s({\n%s\n},\n%s\n)" % (type(self).__name__, "\n".join(strings_array), 
                                        "Namespaces={}".format(str(_get_attribute(self, "namespaces"))))
    
    def __repr__(self):
        data = _get_attribute(self, "data")
        return "%s({%s}, Namespaces=%s)" % (type(self).__name__, 
                                            ", ".join("{}: {}".format(repr(key), repr(value)) for key, (value, _) in data.items()), #comment 
                                            str(_get_attribute(self, "namespaces"))
                                            )
    
    def _inner_eq(self, other):
        """ Checks whether or not the input object can be said to be equal in value to this 
        Configuration instance.
        
        Args:
            key: a python object

        Returns:
            (boolean, str or None) pair, if the boolean is False, the string gives an indication of 
            why, else it is None
        """
        if not all(type(obj) is Configuration for obj in (self, other)):
            return False, "Either one of the input is not a Configuration instance"
        result, reason_message = super()._inner_eq(other)
        if not result:
            return result, reason_message
        namespaces1 = _get_attribute(self, "namespaces")
        namespaces2 = _get_attribute(other, "namespaces")
        if namespaces1 != namespaces2:
            return False, "namespaces"
        return True, None
    
    def __eq__(self, other):
        result, _ = _get_attribute(self, "_inner_eq")(other) #reason_message
        return result
    
    def __ne__(self, other):
        return not self == other
    
    def __hash__(self):
        raise TypeError("unhashable type: '{}'".format(type(self).__name__))
    
    # Class methods
    @classmethod
    def add_namespace(cls, configuration, namespace, name=None):
        """ Adds a namespace to the input configuration which can be used to evaluate (resolve) 
        dotted-identifier expressions.
        
        Args:
            configuration: a Configuration instance, the configuration to which add the namespace
            namespace: A module or other namespace suitable for passing as an argument to vars()., the namespace to be added.
            name: string, a name for the namespace, which, if specified, provides an additional level of 
                indirection.
        """
        namespaces = _get_attribute(configuration, "namespaces")
        if name is None:
            namespaces.append(namespace)
        else:
            setattr(namespaces[0], name, namespace)
    
    @classmethod
    def remove_namespace(cls, configuration, namespace, name=None):
        """ Removes a namespace added with 'add_namespace'.
        
        Args:
            namespace: The namespace to be removed.
            name: string, the name which was specified when 'add_namespace' was called.
        """
        namespaces = _get_attribute(configuration, "namespaces")
        if name is None:
            namespaces.remove(namespace)
        else:
            delattr(namespaces[0], name)
    
    @classmethod
    def load(cls, stream_or_stream_uri, open_stream_from_string_fct=None):
        """ Parses a configuration from a stream, or from a string using the given function if one 
        is provided
        
        Args:
            stream_or_stream_uri: stream to be parsed (so the module's docstring to see which 
                interface is expected), or string; if string, will be used in conjunction with a function 
                to create a string if provided a string
            open_stream_from_string_fct: function to use to create a string, or None. It will be 
                used if the first argument is a string, or when it meets in the parsed configuration a 
                reference to a configuration present in another stream (stream which is then identified by a 
                string; ex: @another_stream.option21.option2). If None, the default function that will be 
                used if one that assumes that the string is a filepath, which will then be used to open the 
                corresponding file using the 'utf-8' encoding

        Returns:
            a Configuration instance  
        """
        if open_stream_from_string_fct is None:
            open_stream_from_string_fct = lambda s: open(s, "r", encoding="utf-8")
        return ConfigurationReader(open_stream_from_string_fct=open_stream_from_string_fct).load(stream_or_stream_uri)



class Sequence(Container):
    """ This internal class implements a value which is a sequence of other values.
    It has an interface similar to that of the 'list' class.
    
    Arguments:
        item_iterable: iterable over items to fill the Sequence instance with
    
    Attributes:
        parent: The parent of this instance in the hierarchy.
        suffix: A string which describes how to get this instance from the variable containing 
            its parent in python code (ex: parent.suffix or parent['suffix'] if parent is a Mapping 
            instance, or parent[suffix] if parent is a Sequence instance)
        data: an ordered dict implementing the link between the keys and (value, comment) pairs 
            of the mapping
    """

    def __init__(self, item_iterable=None):
        super().__init__()
        object.__setattr__(self, "data", [])
        if item_iterable is not None:
            self.extend(item_iterable)
    
    # Instance methods
    def append(self, value, comment=None):
        """ Adds a value to the sequence.

        Args:
            value: python object, the value to add.
            comment: string, a comment for the value.
        """
        data = _get_attribute(self, "data")
        value = _convert_input(value)
        data.append((value, comment))
        if isinstance(value, Container):
            _get_attribute(value, "_set_parent")(self, "[{:d}]".format(len(self)-1))
    
    def set_value(self, index, value, comment=None):
        """ Sets the specified value to the specified position in the sequence.
        
        Args:
            index: int, position in the sequence whose value shall be set
            value: value to set at the specified sequence position
            comment: string, or None; comment to associate to the specified position
        """
        if not isinstance(index, int) or index < 0:
            raise TypeError("Only positive int are allowed as value for the input 'index'.")
        self[index] = value
        if comment is not None:
            data = _get_attribute(self, "data")
            value, _ = data[index] #comment
            data[index] = (value, comment)
    
    def extend_with_comments(self, value_comment_pairs_iterable):
        """ Extends the sequence with values, potentially associated to comments.
        
        Args:
            value_comment_pairs_iterable: iterable over (value, comment) pairs
        """
        for value, comment in value_comment_pairs_iterable:
            if not comment:
                comment = None
            self.append(value, comment=comment)
    
    def extend(self, values_iterable):
        """ Extends the sequence with values.
        
        Args:
            values_iterable: iterable over values
        """
        for value in values_iterable:
            self.append(value)
    
    def __getitem__(self, index):
        self._check_index_type(index)
        data = _get_attribute(self, "data")
        # If slice
        if isinstance(index, slice):
            value_comment_pairs_list = data[index]
            return list(self._evaluate(a) for a, _ in value_comment_pairs_list)
        # If int
        if index >= len(self):
            raise IndexError("Sequence assignment index out of range")
        value_comment_pair = self._index_check(data, index)
        value, _ = value_comment_pair #comment
        return self._evaluate(value)
    
    def __delitem__(self, index):
        self._check_index_type(index)
        data = _get_attribute(self, "data")
        if isinstance(index, slice):
            value_comment_pairs_list = data[index]
        else:
            if index >= len(self):
                raise IndexError("Sequence assignment index out of range")
            value_comment_pairs_list = [self._index_check(data, index)]
        # Remove parenting for potential Container instance that were removed
        for value, _ in value_comment_pairs_list: #comment
            self._relinquish_parental_control(value)
        # carry out the deletion
        del data[index]
    
    def __setitem__(self, index, value):
        self._check_index_type(index)
        if index >= len(self):
            raise IndexError("Sequence assignment index out of range")
        data = _get_attribute(self, "data")
        if isinstance(index, slice):
            values = value
            old_value_comment_pairs_list = data[index] # Fetch current data to remove parenting in potential Container instances
            # Assign new data, part one: use the current functioning to raise Exception if input is not iterable
            data[index] = (_convert_input(v) for v in values)
            # Part two: assign 'None' comments
            data[index] = ((v, None) for v in data[index])
            # Part three: assign parent if 'Container' instance
            new_value_comment_pairs_list = data[index]
            
        else:
            if index >= len(self):
                raise IndexError("Sequence assignment index out of range")
            old_value_comment_pairs_list = [self._index_check(data, index)]
            data[index] = (_convert_input(value), None)
            new_value_comment_pairs_list = [data[index]]
        # Remove parenting for potential Container instance that were removed
        for value, _ in old_value_comment_pairs_list: #comment
            self._relinquish_parental_control(value)
        # Add parenting for new values (some of which may be old values, if the user feels feisty, which is why we do that after removing the parenting for the old values)
        for value, _ in new_value_comment_pairs_list: #comment
            if isinstance(value, Container):
                _get_attribute(value, "_set_parent")(self, "[{:d}]".format(len(self)-1))
    
    @staticmethod
    def _check_index_type(index):
        """ Checks that input value is either an int or a slice.
        
        Raises:
            TypeError: if the input value is neither an int nor a slice
        """
        if not isinstance(index, (int, slice)):
            msg = "Sequence indices must be integers or slices, not {}".format(type(index).__name__)
            raise TypeError(msg)
    
    @staticmethod
    def _index_check(data, index):
        """ Checks that the input 'index' value is an appropriate 'key'-like value to be used on the 
        input 'data' value.
        
        Assumes the input 'data' value implements the '__getitem__' method.
        
        Args:
            data: python object implementing the '__getitem__' method
            index: int
        
        Raises:
            ConfigurationResolutionError: if 
        """
        try:
            value_comment_pair = data[index]
        except (IndexError, KeyError, TypeError):
            message = "{} is not a valid index for {}".format(index, get_path(self))
            raise ConfigurationResolutionError(message)# from None
        return value_comment_pair
    
    def reverse(self):
        """ Returns the collection of (value, comment) pairs corresponding to this sequence, but in 
        reserved order.
        
        Returns:
            collection of (value, comment) pairs
        """
        _get_attribute(self, "data").reverse()
    
    def sort(self, key=None, reverse=False):
        """ Sorts the sequence in-place.
        
        Args:
            key: callable, or None; if not None, is a callable that can take one of the sequence value 
                as input, and returns a python object for which an ordering is defined; will be used 
                to sort the sequence's values; if None, assumes the sequene's values can be sorted 
                as is
            reverse: boolean; whether or not to reverse the result of the sort
        """
        key_fct = key if key is not None else lambda x: x
        def _my_cmp_fct(value_comment):
            value, _ = value_comment #comment
            return key_fct(self._evaluate(value))
        _get_attribute(self, "data").sort(key=_my_cmp_fct, reverse=reverse)
    
    def count(self, value):
        """ Counts the number of times a specific value appears in the sequence.
        
        Args:
            value: a value, which may or may not be present in the sequence
        
        Returns:
            non-negative int
        """
        data = _get_attribute(self, "data")
        return sum((int(value == val) for val, _ in data), 0) #comment
    
    def copy(self):
        """ Creates a shallow copy of the sequence.
        
        Returns:
            Sequence instance
        """
        data = _get_attribute(self, "data")
        sequence = Sequence()
        sequence.extend_with_comments(data)
        return sequence
    
    def clear(self):
        """ Empties the sequence. """
        self[:] = tuple()
    
    def index(self, value):
        """ Finds the smallest index value such that the sequence's value associated to this index 
        is equal to the input value.
        
        Args:
            value: a value, which supposedly exists the sequence
        
        Returns:
            non-negative integer, the smallest index value found for the input value
        
        Raises:
            ValueError: if the input value does not exist in the sequence
        """
        for i, (v, _) in _get_attribute(self, "data"):
            if v == value:
                return i
        raise ValueError("{} is not in Sequence".format(value))
    
    def pop(self, index=None):
        """ Remove and returns item at index (default last).
        
        Args:
            index: non-negative int, or None; the potential index of the element to remove. If None, 
            it is the last element of the sequence that ill be removed.
        
        Returns:
            python object
        
        Raises:
            IndexError: if the sequence is empty or if index is out of range
        """
        index_ = len(self)-1
        if index is not None:
            try:
                index_ = int(index)
            except Exception:
                raise TypeError("{} object cannot be interpreted as an integer".format(type(index))) from None
            
        if not len(self):
            raise IndexError("pop from empty Sequence")
        elif index_ >= len(self):
            raise IndexError("index out of range")
    
        value = self[index]
        
        del self[index]
        
        return value
    
    def insert(self, index, value, comment=None):
        """ Inserts value before index, possibly associated to its comment.
        
        Args:
            index: int, position in the sequence whose value shall be set
            value: value to set at the specified sequence position
            comment: string, or None; comment to associate to the specified position
        """
        try:
            index_ = int(index)
        except Exception:
            raise TypeError("{} object cannot be interpreted as an integer".format(type(index))) from None
        data = _get_attribute(self, "data")
        data.insert(index_, (_convert_input(value), comment))
        if isinstance(value, Container):
            _get_attribute(value, "_set_parent")(self, "[{:d}]".format(len(self)-1))
    
    def __reversed__(self):
        """ Creates a generator over values listed in this sequence.
        
        Yields:
            python object
        """
        for index in range(len(self)-1, -1, -1):
            yield self[index]
    
    def __contains__(self, value):
        data = _get_attribute(self, "data")
        for val, _ in data: #comment
            if val == value:
                return True
        return False
    
    def __add__(self, other):
        if not isinstance(other, Sequence):
            msg = " can only concatenate Sequence (not \"{}\") to Sequence".format(type(other).__name__)
            raise TypeError(msg)
        data1 = _get_attribute(self, "data")
        data2 = _get_attribute(other, "data")
        sequence = Sequence()
        sequence.extend_with_comments(data1 + data2)
        return sequence
    
    def __iadd__(self, iterable):
        for value in iterable:
            self.append(value)
    
    def _raise_type_error(self, other, cmp_sign):
        if not isinstance(other, Sequence):
            msg = "unorderable types: Sequence() {} {}()".format(cmp_sign, type(other).__name__)
            raise TypeError(msg)
    
    def _le(self, other):
        l1 = len(self)
        l2 = len(other)
        for index in range(min(l1, l2)):
            val1 = self[index]
            val2 = other[index]
            if not val1 <= val2:
                return False
        if l2 > l1:
            return False
        return True
    
    def _ge(self, other):
        l1 = len(self)
        l2 = len(other)
        for index in range(min(l1, l2)):
            val1 = self[index]
            val2 = other[index]
            if not val1 >= val2:
                return False
        if l1 > l2:
            return False
        return True
    
    def __le__(self, other):
        self._raise_type_error(other, "<=")
        return self._le(other)
    
    def __gt__(self, other):
        self._raise_type_error(other, ">")
        return not self._le(other)
    
    def __ge__(self, other):
        self._raise_type_error(other, ">=")
        return self._ge(other)
    
    def __lt__(self, other):
        self._raise_type_error(other, "<")
        return not self._ge(other)
    
    def __len__(self):
        return len(_get_attribute(self, "data"))
    
    def __iter__(self):
        return iter(self[index] for index in range(len(self)))

    def __repr__(self):
        return "Sequence([{},])".format(", ".join(repr(v) for v in self[:]))
    
    def __str__(self):
        strings_array = []
        for value, _ in _get_attribute(self, "data"): #comment
            if isinstance(value, (Mapping, Sequence, Reference, Expression)):
                string = str(value)
            else:
                string = repr(value)
            inner_strings_array = string.strip().split("\n")
            inner_strings_array[-1] += ","
            strings_array.extend("{}{}".format(INDENT_STEP, s) for s in inner_strings_array)
        return "Sequence([\n{}\n])".format("\n".join(strings_array))
    
    def _inner_eq(self, other):
        """ Checks whether or not the input object can be said to be equal in value to this 
        Sequence instance.
        
        Args:
            key: a python object

        Returns:
            (boolean, str or None) pair, if the boolean is False, the string gives an indication of 
            why, else it is None
        """
        if not isinstance(other, Sequence):
            return False, "Not {} class instance: type = '{}'".format(type(self).__name__, type(other))
        l1 = len(self)
        l2 = len(other)
        if l1 != l2:
            return False, "Difference of length: '{}' v. '{}'".format(l1, l2)
        for index, (elt1, elt2) in enumerate(zip(self, other)):
            if elt1 != elt2:
                return False, "Different values for index n°{}".format(index)
        return True, None
    
    def __eq__(self, other):
        result, _ = self._inner_eq(other) # reason_message
        return result
    
    def __ne__(self, other):
        return not self == other
    
    def __hash__(self):
        raise TypeError("unhashable type: '{}'".format(type(self).__name__))
    
    def get_base_hash_string(self):
        """ Produces a normalized string representation of this Sequence instance, which shall serves 
        to unequivocally identify it.
        
        Returns:
            string
        """
        strings_array = []
        for i in range(len(self)):
            value = self[i]
            if isinstance(value, (Mapping, Sequence)):
                value_string = type(value).get_base_hash_string(value)
            else:
                value_string = repr(value)
            string = value_string
            strings_array.append(string)
        return "\n".join(strings_array)
    
    def _write_to_stream(self, stream, indent, container):
        """ Writes this instance to a stream at the specified indentation level.
        
        May be redefined in subclasses.
        
        Args:
            stream: A writable stream (file-like object), the stream to write to
            indent: int, the indentation level
            container: Container instance, the container of this instance
        """
        indstr = indent * INDENT_STEP
        if len(self) == 0:
            stream.write(" [ ]{!s}".format(NEWLINE))
        else:
            if isinstance(container, Mapping):
                stream.write(NEWLINE)
            stream.write("{!s}[{!s}".format(indstr, NEWLINE))
            self.save(stream, indent + 1)
            stream.write("{!s}]{!s}".format(indstr, NEWLINE))

    def save(self, stream, indent):
        """ Saves this instance to the specified stream.
        
        Args:
            stream: A write-only stream (file-like object)., a stream to which the configuration is written.
            indent: int, the indentation level for the output, > 0
        """
        if indent == 0:
            raise MappingError("Sequence cannot be saved as a top-level item")
        data = _get_attribute(self, "data")
        indstr = indent * INDENT_STEP
        for i in range(0, len(data)):
            value, comment = data[i]
            if comment:
                stream.write("{!s}#{!s}".format(indstr, comment))
            if isinstance(value, Container):
                _get_attribute(value, "_write_to_stream")(stream, indent, self)
            else:
                self._write_value(value, stream, indent)



class Reference(object):
    """ This internal class implements a value which is a reference to another value.
    
    Arguments:
        configuration: a Configuration instance, the configuration which contains this reference
        type_: BACKTICK or DOLLAR, the type of reference
        ident: string, the identifier which starts the reference
    
    Attributes:
        configuration: a Configuration instance, the configuration which contains this reference
        type_: BACKTICK or DOLLAR, the type of reference
    """
    
    def __init__(self, configuration, type_, ident):
        self.configuration = configuration
        self.type = type_
        self._elements = [ident]
        self._logger = logging.getLogger("{}: {}".format(__name__, "Reference instance"))

    def add_element(self, type_, ident):
        """ Adds an element to the reference.

        Args:
            type: BACKTICK or DOLLAR, the type of reference.
            ident: string, the identifier which continues the reference.
        """
        self._elements.append((type_, ident))
    
    def resolve(self, container):
        """ Resolves this instance in the context of a container.

        Args:
            container: Container instance, the container to resolve from.
        
        Returns:
            python object, the resolved value.
        
        Raises:
            ConfigurationResolutionError: If resolution fails.
        """
        result = None
        current_configuration = self._find_configuration(container)
        
        # Look for value in an imported module
        if self.type == BACKTICK:
            while current_configuration is not None:
                namespaces = _get_attribute(current_configuration, "namespaces")
                found = False
                s = self.get_string_representation()[1:-1]
                for namespace in namespaces:
                    try:
                        try:
                            result = eval(s, vars(namespace))
                        except TypeError: #Python 2.7 - vars is a dictproxy
                            result = eval(s, {}, vars(namespace))
                        found = True
                        break
                    except:
                        self._logger.debug("Unable to resolve {!r} in {!s}".format(s, namespace))
                        pass
                if found:
                    break
                
                current_configuration = self._find_configuration(_get_attribute(current_configuration, "parent"))
            
        else:
            while current_configuration is not None:
                firstkey = self._elements[0]
                resolving = _get_attribute(current_configuration, "_resolving")
                if firstkey in resolving:
                    resolving.remove(firstkey)
                    raise ConfigurationResolutionError("Circular reference: {!r}.".format(firstkey))
                resolving.add(firstkey)
                key = firstkey
                try:
                    result = current_configuration[key]
                    for item in self._elements[1:]:
                        key = item[1]
                        result = result[key]
                    resolving.remove(firstkey)
                    break
                except ConfigurationResolutionError:
                    raise
                except:
                    self._logger.debug("Unable to resolve {!r}: {!s}.".format(key, sys.exc_info()[1]))
                    result = None
                    pass
                resolving.discard(firstkey)
                
                current_configuration = self._find_configuration(_get_attribute(current_configuration, "parent"))
        
        if current_configuration is None:
            path = get_path(container)
            raise ConfigurationResolutionError("Unable to evaluate {!r} in the configuration '{!s}'.".format(self, path))
        
        return result
    
    def _find_configuration(self, container):
        """ Finds the closest enclosing configuration to the specified container.
        
        Args:
            container: Container instance, the container to start from.
        
        Returns:
            Configuration instance, the closest enclosing configuration, or None.
        """
        while container is not None and not isinstance(container, Configuration):
            container = _get_attribute(container, "parent")
        return container

    def __str__(self):
        return "Reference(\"{}\")".format(self.get_string_representation())

    def __repr__(self):
        return "Reference(type=\"{!r}\", elements=\"{!r}\", configuration=\"...\")".format(self.type, self._elements)
    
    def get_string_representation(self):
        """ Creates a string representation of the Reference instance, allowing a human to understand 
        the value that is being referenced in the frame of a configuration.
        
        Returns:
            string
        """
        s = self._elements[0]
        for token_type, token_value in self._elements[1:]:
            if token_type == DOT:
                s += ".{!s}".format(token_value)
            else:
                s += "[{!r}]".format(token_value)
        if self.type == BACKTICK:
            return BACKTICK + s + BACKTICK
        return DOLLAR + s
    
    def __eq__(self, other):
        if not isinstance(other, Reference):
            return False
        attribute_names = ("configuration", "type", "_elements")
        for attribute_name in attribute_names:
            value1 = getattr(self, attribute_name)
            value2 = getattr(other, attribute_name)
            if value1 != value2:
                return False
        return True
    
    def __ne__(self, other):
        return not self == other
    
    def __hash__(self):
        raise TypeError("unhashable type: '{}'".format(type(self).__name__))



class Expression(object):
    """ This internal class implements a value which is obtained by evaluating an expression.
    
    Arguments:
        op: string, belonging to [PLUS, MINUS, STAR, SLASH, MOD], the operation expressed in the expression
        lhs: a Reference instance or an Expression instance, or a python object for which the 
            operations represented by the availabe operations exists; the left-hand-side operand of 
            the expression
        rhs:  a Reference instance or an Expression instance, or a python object for which the 
            operations represented by the availabe operations exists; the right-hand-side operand of 
            the expression
    
    Attributes:
        op: string, belonging to [PLUS, MINUS, STAR, SLASH, MOD], the operation expressed in the expression
        lhs: a Reference instance or an Expression instance, or a python object for which the 
            operations represented by the availabe operations exists; the left-hand-side operand of 
            the expression
        rhs:  a Reference instance or an Expression instance, or a python object for which the 
            operations represented by the availabe operations exists; the right-hand-side operand of 
            the expression
    """
    def __init__(self, op, lhs, rhs):
        self.op = op
        self.lhs = lhs
        self.rhs = rhs
    
    def evaluate(self, container):
        """ Evaluates this instance in the context of a container.

        Args:
            container: Container instance, the container to evaluate in from.
        
        Returns:
            python object, the evaluated value.
        
        Raises:
            ConfigurationResolutionError: If evaluation fails.
            ZeroDivideError: If division by zero occurs.
            TypeError: If the operation is invalid, e.g. subtracting one string from another.
        """
        lhs = self.lhs
        if isinstance(lhs, Reference):
            lhs = lhs.resolve(container)
        elif isinstance(lhs, Expression):
            lhs = lhs.evaluate(container)
        rhs = self.rhs
        if isinstance(rhs, Reference):
            rhs = rhs.resolve(container)
        elif isinstance(rhs, Expression):
            rhs = rhs.evaluate(container)
        op = self.op
        if op == PLUS:
            result = lhs + rhs
        elif op == MINUS:
            result = lhs - rhs
        elif op == STAR:
            result = lhs * rhs
        elif op == SLASH:
            result = lhs / rhs
        else:
            result = lhs % rhs
        return result
    
    def __eq__(self, other):
        if not isinstance(other, Expression):
            return False
        attribute_names = ("op", "lhs", "rhs")
        for attribute_name in attribute_names:
            value1 = getattr(self, attribute_name)
            value2 = getattr(other, attribute_name)
            if value1 != value2:
                return False
        return True
    
    def __ne__(self, other):
        return not self == other
    
    def __hash__(self):
        raise TypeError("unhashable type: '{}'".format(type(self).__name__))
    
    def __repr__(self):
        return "Expression(op=\"{!r}\", lhs=\"{!r}\", rhs=\"{!r}\")".format(self.op, self.lhs, self.rhs)
    
    def __str__(self):
        return "Expression(\"{}\")".format(self.get_string_representation())
    
    def get_string_representation(self):
        return "{} {!s} {}".format(self._side_string_representation(self.lhs), self.op, 
                                   self._side_string_representation(self.rhs))
    
    @staticmethod
    def _side_string_representation(side):
        if isinstance(side, (Expression, Reference)):
            return side.get_string_representation()
        return repr(side)



class ConfigurationReader(object):
    """ This internal class implements a parser for configurations.
    
    Arguments:
        open_stream_from_string_fct: function to use to create a stream, or None. It will be 
            used during the parsing of a configuration, if the argument provided to refer to a stream 
            is a string, or when it meets in the parsed configuration a  reference to a configuration 
            present in another stream (stream which is then identified by a  string; 
            ex: @another_stream.option21.option2). If None, the default function that will be 
            used if one that assumes that the string is a filepath, which will then be used to open the 
            corresponding file using the 'utf-8' encoding
    """
    
    _DIGIT_CHARACTERS = "0123456789"
    _PUNCTUATION_CHARACTERS = ":-+*/%,.{}[]()@`$"
    _QUOTE_CHARACTERS = "'\""
    _WHITESPACE_CHARACTERS = " \t\r\n"
    _COMMENT_CHARACTERS = "#"
    _WORD_CHARACTERS = WORD_CHARACTERS
    _IDENT_CHARACTERS = _WORD_CHARACTERS + _DIGIT_CHARACTERS
    
    def __init__(self, open_stream_from_string_fct=None):
        # Check input values
        if open_stream_from_string_fct is None:
            open_stream_from_string_fct = lambda s: open(s, "r", encoding="utf-8")
        # The function used to open a stream from a string
        self.open_stream_from_string_fct = open_stream_from_string_fct
        
        # The configuration to fill with (key; value) pairs parsed from a stream
        self.configuration = None
        # Data pertaining to the currently parsed stream
        self._stream = None
        self._filename = None
        self._line_no = 0
        self._column_no = 0
        # Data pertaining to the current state of the parsing
        self._previous_character = None
        self._current_token_definition = (None, None) # ('token_type', 'token')
        self._comment = None
        self._problematic_characters_LIFO_buffer = []
        self._problematic_tokens_LIFO_buffer = []
    
    @property
    def _current_token(self):
        return self._current_token_definition[1]
    @_current_token.setter
    def _current_token(self, value):
        raise AttributeError()
    
    @property
    def _current_token_type(self):
        return self._current_token_definition[0]
    @_current_token_type.setter
    def _current_token_type(self, value):
        raise AttributeError()
    
    def load(self, stream_or_stream_uri, configuration=None):
        """ Creates and fills a configuration with (key; value) pairs obtained from parsing the specified 
        stream.
        
        Can continue to populate an existing configuration, if one is input.
        
        Multiple streams can be used to populate the same configuration, as long as there are no
        clashing keys. The stream is not closed. If the input is a string instead of a stream, it 
        will be used in conjunction with this instance's "open_stream_from_string_fct" in order to 
        create a stream which will then be used instead.
        
        Args:
            stream_or_stream_uri: stream to be parsed (so the module's docstring to see which 
                interface is expected), or string; if string, will be used in conjunction with this instance 
                'open_stream_from_string_fct' attribute to create a string if provided a string
            configuration: a Configuration Instance, or None; the configuration to update with the 
                content of the stream, or None. If None, a new Configuration instance will be created, 
                filled, and then returned instead.
        
        Returns:
            the parsed Configuration instance, or the input Configuration updated with the content of 
            the stream
        
        Raises:
            MappingFormatError: if there are syntax errors in the stream.
        """
        if isinstance(stream_or_stream_uri, str):
            stream = self.open_stream_from_string_fct(stream_or_stream_uri)
            try:
                configuration = self.load(stream, configuration=configuration)
            finally:
                stream.close()
            return configuration
        
        # Check input values
        if configuration is None:
            configuration = Configuration()
        self.configuration = configuration
        
        # Carry out the parsing
        stream = stream_or_stream_uri
        self._set_stream(stream)
        self._parse_next_token()
        self._parse_mapping_body(self.configuration)
        token_type, token_value = self._current_token_definition
        if token_type != EOF:
            msg = "{!s}: expecting EOF, found {!r}.".format(self._get_location(), token_value)
            raise MappingFormatError(msg)
        
        # Reinitialize in order to clear remaining data from the carried out parsing of the stream
        self.__init__(open_stream_from_string_fct=self.open_stream_from_string_fct)
        
        return configuration
    
    def _set_stream(self, stream):
        """ Sets the internal 'stream' attribute to the specified value, and prepare to read from it.

        Args:
            stream: A stream (file-like object). (expect 'readline' method), a stream from which to load the configuration.
        """
        filename = "?"
        if hasattr(stream, "name"):
            filename = stream.name
        self._stream = stream
        self._filename = filename
        self._line_no = 1
        self._column_no = 1
    
    def _parse_next_token(self):
        """ Parses a token from the stream. String values are created in a form where you need to eval() 
        the value to get the actual string.
        
        Updates this instance's attributes (linked to the current configuration parsing task) with 
        the parsed token.

        Multiline string tokenizing is thanks to David Janes (BlogMatrix)
        """
        if self._problematic_tokens_LIFO_buffer:
            return self._problematic_tokens_LIFO_buffer.pop()
        stream = self._stream
        self._comment = None
        token = ""
        token_type = EOF
        while True:
            c = self._get_next_character()
            
            if not c: # If no more characters, then it is end of the stream
                break
            elif c == "#": # If the line is supposed to be a comment...
                self._comment = stream.readline()
                self._line_no += 1
                continue
            
            if c in self._QUOTE_CHARACTERS: # If the line contains something put between quotes characters
                token = c
                quote = c
                token_type = STRING
                escaped = False
                multiline = False
                c1 = self._get_next_character()
                if c1 == quote:
                    c2 = self._get_next_character()
                    if c2 == quote:
                        multiline = True
                        token += quote
                        token += quote
                    else:
                        self._problematic_characters_LIFO_buffer.append(c2)
                        self._problematic_characters_LIFO_buffer.append(c1)
                else:
                    self._problematic_characters_LIFO_buffer.append(c1)
                while True:
                    c = self._get_next_character()
                    if not c:
                        break
                    token += c
                    if (c == quote) and not escaped:
                        if not multiline or (len(token) >= 6 and token.endswith(token[:3]) and token[-4] != "\\"):
                            break
                    if c == "\\":
                        escaped = not escaped
                    else:
                        escaped = False
                if not c:
                    msg = "{!s}: Unterminated quoted string: {!r}, {!r}".format(self._get_location(), token, c)
                    raise MappingFormatError(msg)
                break
            
            if c in self._WHITESPACE_CHARACTERS:
                self._previous_character = c
                continue
            elif c in self._PUNCTUATION_CHARACTERS:
                token = c
                token_type = c
                if self._previous_character == "]" or self._previous_character in self._IDENT_CHARACTERS:
                    if c == "[":
                        token_type = LBRACK2
                    elif c == "(":
                        token_type = LPAREN2
                break
            elif c in self._DIGIT_CHARACTERS: # Parse a number
                token = c
                token_type = NUMBER
                in_exponent = False
                while True:
                    c = self._get_next_character()
                    if not c:
                        break
                    if c in self._DIGIT_CHARACTERS:
                        token += c
                    elif c == "." and token.find(".") < 0 and not in_exponent:
                        token += c
                    elif c == "-" and token.find("-") < 0 and in_exponent:
                        token += c
                    elif c in "eE" and token.find("e") < 0 and token.find("E") < 0:
                        token += c
                        in_exponent = True
                    else:
                        if c and c not in self._WHITESPACE_CHARACTERS:
                            self._problematic_characters_LIFO_buffer.append(c)
                        break
                break
            elif c in self._WORD_CHARACTERS:
                token = c
                token_type = WORD
                c = self._get_next_character()
                while c and c in self._IDENT_CHARACTERS:
                    token += c
                    c = self._get_next_character()
                if c: # and c not in self._WHITESPACE_CHARACTERS:
                    self._problematic_characters_LIFO_buffer.append(c)
                if token == "True":
                    token_type = TRUE
                elif token == "False":
                    token_type = FALSE
                elif token == "None":
                    token_type = NONE
                break
            else:
                msg = "{!s}: Unexpected character: {!r}.".format(self._get_location(), c)
                raise MappingFormatError(msg)
        
        if token:
            self._previous_character = token[-1]
        else:
            self._previous_character = None
        
        self._current_token_definition = (token_type, token)
    
    def _get_next_character(self):
        """ Gets the next char from the stream. Update line and column numbers appropriately.

        Returns:
            string, the next character from the stream.
        """
        if self._problematic_characters_LIFO_buffer:
            c = self._problematic_characters_LIFO_buffer.pop()
        else:
            c = self._stream.read(1)
            self._column_no += 1
            if c == "\n":
                self._line_no += 1
                self._column_no = 1
        return c
    
    def _parse_mapping_body(self, mapping):
        """ Parses the internals of a mapping, and add entries to the provided Mapping.
        
        Args:
            mapping: a Mapping instance., the mapping to add entries to.
        """
        while self._current_token_definition[0] in (WORD, STRING):
            self._parse_key_value_pair(mapping)

    def _parse_key_value_pair(self, mapping):
        """ Parses a key-value pair, and add it to the provided Mapping.

        Args:
            mapping: a Mapping instance., the mapping to add entries to.
        
        Raises:
            MappingFormatError: if a syntax error is found.
        """
        # Get the potential comment associated to the current (key, value) pair to parse
        comment = self._comment
        
        # Parse the key
        token_type, token = self._current_token_definition
        if token_type == WORD:
            key = token
        elif token_type == STRING:
            key = eval(token)
        else:
            msg = "{!s}: expecting word or string, found {!r}.".format(self._get_location(), token)
            raise MappingFormatError(msg)
        
        # Parse the value
        ## Parse next token
        self._parse_next_token()
        token_type = self._current_token_type
        if token_type == COLON:
            # Parse next token
            self._parse_next_token()
            value = self._parse_value()#mapping, suffix
        else: # For now, we allow key on its own as a short form of key : True
            value = True
        
        # Add the (key, value) pair to the mapping
        add_mapping_fct = _get_attribute(mapping, "_add_mapping")
        try:
            add_mapping_fct(key, value, comment)
        except Exception as e:
            msg = "{!s}: {!s}, {!r}".format(self._get_location(), e, self._current_token)
            raise MappingFormatError(msg)# from None
        
        # Prepare for the next (key, pair) value parsing, or for the reaching of the end of the stream
        token_type, token = self._current_token_definition
        if token_type not in (EOF, WORD, STRING, RCURLY, COMMA):
            msg = "{!s}: expecting one of EOF, WORD, STRING, RCURLY, COMMA, found {!r}"
            raise MappingFormatError(msg.format(self._get_location(), token))
        if token_type == COMMA:
            # Parse next token
            self._parse_next_token()

    def _parse_value(self):
        """ Parses a value that is supposed to correspond to the next (group of) token(s).
        
        Returns:
            python object, the value
        
        Raises:
            MappingFormatError: if a syntax error is found.
        """
        token_type, token = self._current_token_definition
        if token_type in (STRING, WORD, NUMBER, LPAREN, DOLLAR, TRUE, FALSE, NONE, BACKTICK, MINUS):
            value = self._parse_scalar()
        elif token_type == LBRACK:
            value = self._parse_sequence()
        elif token_type in [LCURLY, AT]:
            value = self._parse_mapping()
        else:
            raise MappingFormatError("{!s}: unexpected input: {!r}".format(self._get_location(), token))
        return value
    
    def _parse_sequence(self):
        """ Parses a value that is supposed to be a sequence, according to the last parsed token.
        
        Returns:
            Sequence instance, a Sequence instance representing the sequence.
        
        Raises:
            MappingFormatError: if a syntax error is found.
        """
        sequence = Sequence()
        self._check_matching_type(LBRACK)
        comment = self._comment
        while self._current_token_type in (STRING, WORD, NUMBER, LCURLY, LBRACK, LPAREN, DOLLAR, 
                                           TRUE, FALSE, NONE, BACKTICK, MINUS):
            value = self._parse_value()
            sequence.append(value, comment)
            comment = self._comment
            if self._current_token_type == COMMA: # Parse the next token by calling the '_check_matching_type' method
                self._check_matching_type(COMMA)
                comment = self._comment
                continue
        self._check_matching_type(RBRACK) # The only correct way the leave a sequence is through a closing bracket; also, parse the next token
        return sequence
    
    def _check_matching_type(self, t):
        """ Ensures that the current token type matches the specified value, and advance to the next token.
        
        Args:
            t: A valid token type., the token type to _check_matching_type.
        
        Returns:
            a token tuple - see '_parse_next_token'., the token which was last read from the stream before this
            function is called.
        
        Raises:
            MappingFormatError: If the token does not _check_matching_type what's expected.
        """
        if self._current_token_type != t:
            raise MappingFormatError("{!s}: expecting {!s}, found {!r}".format(self._get_location(), t, self._current_token))
        previous_token_definition = self._current_token_definition
        self._parse_next_token()
        return previous_token_definition
    
    def _parse_mapping(self):
        """ Parses a value that is supposed to be a mapping, according to the last parsed token.
        
        Returns:
            Mapping instance, a Mapping instance representing the mapping.
        
        Raises:
            MappingFormatError: if a syntax error is found.
        """
        if self._current_token_type == LCURLY: # If we are at the beginning of the mapping to be parsed...
            self._check_matching_type(LCURLY) # ... we parse the next token...
            sub_mapping = Mapping() # ... create the mapping value...
            self._parse_mapping_body(sub_mapping)  # ... and fill it content by further parsing the stream (inception o.O).
            self._check_matching_type(RCURLY)
        else:
            self._check_matching_type(AT) # If we make a reference to another stream through its access name
            _, previous_token = self._check_matching_type(STRING)
            stream_access_string = eval(previous_token)
            sub_mapping = Configuration.load(stream_access_string, 
                                             open_stream_from_string_fct=self.open_stream_from_string_fct)
        return sub_mapping

    def _parse_scalar(self):
        """ Parses a scalar - a terminal value such as a string or number, or an Expression or Reference.

        Returns:
            any scalar, the parsed scalar
        
        Raises:
            MappingFormatError: if a syntax error is found.
        """
        lhs = self._parse_term()
        token_type = self._current_token_type
        while token_type in (PLUS, MINUS):
            self._check_matching_type(token_type)
            rhs = self._parse_term()
            lhs = Expression(token_type, lhs, rhs)
            token_type = self._current_token_type
        return lhs

    def _parse_term(self):
        """ Parses a term in an additive expression (a + b, a - b)

        Returns:
            any scalar, the parsed term
        
        Raises:
            MappingFormatError: if a syntax error is found.
        """
        lhs = self._parse_factor()
        token_type = self._current_token_type
        while token_type in (STAR, SLASH, MOD):
            self._check_matching_type(token_type)
            rhs = self._parse_factor()
            lhs = Expression(token_type, lhs, rhs)
            token_type = self._current_token_type
        return lhs

    def _parse_factor(self):
        """ Parses a factor in an multiplicative expression (a * b, a / b, a % b)

        Returns:
            any scalar, the parsed factor
        
        Raises:
            MappingFormatError: if a syntax error is found.
        """
        token_type = self._current_token_type
        if token_type in [NUMBER, WORD, STRING, TRUE, FALSE, NONE]:
            scalar = self._current_token
            if token_type != WORD:
                scalar = eval(scalar)
            self._check_matching_type(token_type)
        elif token_type == LPAREN:
            self._check_matching_type(LPAREN)
            scalar = self._parse_scalar()
            self._check_matching_type(RPAREN)
        elif token_type == DOLLAR:
            self._check_matching_type(DOLLAR)
            scalar = self._parse_reference(DOLLAR)
        elif token_type == BACKTICK:
            self._check_matching_type(BACKTICK)
            scalar = self._parse_reference(BACKTICK)
            self._check_matching_type(BACKTICK)
        elif token_type == MINUS:
            self._check_matching_type(MINUS)
            scalar = -self._parse_scalar()
        else:
            msg = "{!s}: unexpected input: {!r}.".format(self._get_location(), self._current_token)
            raise MappingFormatError(msg)
        return scalar

    def _parse_reference(self, type_):
        """ Parses a reference.

        Returns:
            Reference instance, the parsed reference
        
        Raises:
            MappingFormatError: if a syntax error is found.
        """
        _, previous_token = self._check_matching_type(WORD)
        reference = Reference(self.configuration, type_, previous_token)
        while self._current_token_type in (DOT, LBRACK2):
            self._parse_suffix(reference)
        return reference
    
    def _parse_suffix(self, reference):
        """ Parses a reference suffix.

        Args:
            reference: Reference instance, the reference of which this suffix is a part.
        
        Raises:
            MappingFormatError: if a syntax error is found.
        """
        token_type = self._current_token_type
        if token_type == DOT: # If current token is of the form '.', we expected '.field_name'
            self._check_matching_type(DOT)
            _, previous_token = self._check_matching_type(WORD)
            reference.add_element(DOT, previous_token)
        else: # Else, it must of the form '[integer]'
            self._check_matching_type(LBRACK2)
            token_type, token = self._current_token_definition
            if token_type not in (NUMBER, STRING):
                msg = "{!s}: expected number or string, found {!r}.".format(self._get_location(), token)
                raise MappingFormatError(msg)
            self._parse_next_token()
            token = eval(token)
            self._check_matching_type(RBRACK)
            reference.add_element(LBRACK, token)
    
    def _get_location(self):
        """ Returns the current _get_location (filename, line, column) in the stream as a string.

        Used when printing error messages,

        Returns:
            string, a string representing a _get_location in the stream being read.
        """
        return "{!s}({:d},{:d})".format(self._filename, self._line_no, self._column_no)



def carry_out_default_merge_resolution(map1, map2, key):
    """ Implements default merge resolver function for merge conflicts. Returns a string indicating what 
    action to take to resolve the conflict.

    Args:
            map1: Mapping instance, the map being merged into.
        map2: Mapping instance, the map being used as the merge operand.
        key: string, the key in map2 (which also exists in map1).
    
    Returns:
        string, one of "merge", "append", "mismatch" or "overwrite" indicating what action should be 
        taken. This should be appropriate to the objects being merged - e.g. there is no point returning 
        "merge" if the two objects are instances of Sequence.
    """
    obj1 = map1[key]
    obj2 = map2[key]
    if isinstance(obj1, Mapping) and isinstance(obj2, Mapping):
        return "merge"
    elif isinstance(obj1, Sequence) and isinstance(obj2, Sequence):
        return "append"
    return "mismatch"

def carry_out_overwrite_merge_resolution(map1, map2, key):
    """ Implements an overwriting merge resolver for merge conflicts. Calls 'carry_out_default_merge_resolution', but 
    where a "mismatch" is detected, returns "overwrite" instead.

    Args:
        map1: Mapping instance, the map being merged into.
        map2: Mapping instance, the map being used as the merge operand.
        key: string, the key in map2 (which also exists in map1).
    """
    result = carry_out_default_merge_resolution(map1, map2, key)
    if result == "mismatch":
        result = "overwrite"
    return result

def carry_out_absolute_overwrite_merge_resolution(map1, map2, key):
    """ Implements an overwriting merge resolver for merge conflicts. Returns "overwrite" whatever the 
    inputs are.

    Args:
        map1: Mapping instance, the map being merged into.
        map2: Mapping instance, the map being used as the merge operand.
        key: string, the key in map2 (which also exists in map1).
    """
    return "overwrite"

class MappingMerger(object):
    """ This class is used for merging two configurations.
    
    If a key exists in the merge operand but not the merge target, then the entry is copied from the 
    merge operand to the merge target.
    If a key exists in both configurations, then a merge resolver (a callable) is called to decide how to 
    handle the conflict.
    
    Arguments:
        merge_resolver: a callable, or None; if a callable, should take the argument list (map1, map2, key) 
            where map1 is the mapping being merged into, map2 is the merge operand and key is the 
            clashing key, and then should return a string indicating how the conflict should be resolved.
            For possible return values, see 'carry_out_default_merge_resolution'. If None, use a default merge resolver, 
            that preserves the previous behavior
    
    Attributes:
        merge_resolver: a callable, or None; if a callable, should take the argument list (map1, map2, key) 
            where map1 is the mapping being merged into, map2 is the merge operand and key is the 
            clashing key, and then should return a string indicating how the conflict should be resolved.
            For possible return values, see 'carry_out_default_merge_resolution'. If None, use a default merge resolver, 
            that preserves the previous behavior
    """

    def __init__(self, merge_resolver=None):
        merge_resolver = merge_resolver if merge_resolver is not None else carry_out_default_merge_resolution
        self.merge_resolver = merge_resolver

    def merge(self, merged, mergee):
        """ Merges two configurations. The second configuration is unchanged, and the first is changed 
        to reflect the results of the merge.

        Args:
            merged: Configuration instance, the configuration to merge into.
            mergee: Configuration instance, the configuration to merge.
        """
        self.merge_mapping(merged, mergee)
    
    def merge_mapping(self, map1, map2):
        """ Merges two mappings recursively. The second mapping is unchanged, and the first is changed 
        to reflect the results of the merge.

        Args:
            map1: Mapping instance, the mapping to merge into.
            map2: Mapping instance, the mapping to merge.
        """
        keys = set(iter(map1))
        for key in iter(map2):
            if key not in keys:
                map1[key] = map2[key]
            else:
                obj1 = map1[key]
                obj2 = map2[key]
                decision = self.merge_resolver(map1, map2, key)
                if decision == "merge":
                    self.merge_mapping(obj1, obj2)
                elif decision == "append":
                    self.merge_sequence(obj1, obj2)
                elif decision == "overwrite":
                    map1[key] = obj2
                elif decision == "mismatch":
                    self.handle_mismatch(obj1, obj2)
                else:
                    msg = "Unable to merge: do not know how to implement {!r}.".format(decision)
                    raise ValueError(msg)

    def merge_sequence(self, seq1, seq2):
        """ Merges two sequences. The second sequence is unchanged, and the first is changed to have the 
        _elements of the second appended to it.

        Args:
            seq1: Sequence instance, the sequence to merge into.
            seq2: Sequence instance, the sequence to merge.
        """
        data1 = _get_attribute(seq1, "data")
        data2 = _get_attribute(seq2, "data")
        for value_comment in data2:
            data1.append(value_comment)

    def handle_mismatch(self, obj1, obj2):
        """ Handles a mismatch between two objects.

        Args:
            obj1: python object, the object to merge into.
            obj2: python object, the object to merge.
        """
        raise MappingError("Unable to merge {!r} with {!r}.".format(obj1, obj2))



class MappingList(list):
    """ This class implements an ordered list of configurations and allows you to try getting the 
    configuration from each entry in turn, returning the first successfully obtained value.
    """
    
    def get_by_path(self, path):
        """ Obtains a value from the first configuration in the list which defines it.

        Args:
            path: string, the path of the value to retrieve.
        
        Returns:
            python object, the value from the earliest configuration in the list which defines it.
        
        Raises:
            MappingError: If no configuration in the list has an entry with the specified 
        path.
        """
        found = False
        result = None
        for entry in self:
            try:
                result = self._get_by_path_from_mapping(entry, path)
            except MappingError:
                pass
            else:
                found = True
                break
        if not found:
            raise MappingError("Unable to resolve {}".format(path))
        return result
    
    @staticmethod
    def _get_by_path_from_mapping(mapping, path):
        """ Obtains a value in a mapping via its path.
        
        Args:
            path: string, the path of the required value
        
        Returns:
            python object, the value at the specified path.
        
        Raises:
            MappingError: If the path is invalid
        """
        s = "__mapping__." + path
        local_context = {"__mapping__": mapping}
        try:
            return eval(s, None, local_context)
        except Exception as e:
            raise MappingError(str(e))# from None


#######
# API #
#######
from copy import deepcopy
def _general_deepcopy(item):
    """ Creates a deepcopy, using the specific, associated functions if the input is a Mapping 
    instance or a Sequence instance, or python's 'copy.deepcopy' otherwise.
    
    Args:
        item: a python object
    
    Returns:
        a python object
    """
    if isinstance(item, Mapping):
        return deepcopy_mapping(item)
    if isinstance(item, Sequence):
        return deepcopy_sequence(item)
    return deepcopy(item)
def deepcopy_sequence(sequence):
    """ Creates a deepcopy of a Sequence instance.
    
    Args:
        sequence: Sequence instance
    
    Returns:
        Sequence instance
    """
    return Sequence((_general_deepcopy(item) for item in sequence))
def deepcopy_mapping(mapping):
    """ Creates a deepcopy of a Mapping instance.
    
    Args:
        item: a Mapping instance
    
    Returns:
        a Mapping instance
    """
    return Mapping((key, _general_deepcopy(value)) for key, value in Mapping.items(mapping))

def convert_configuration_to_mapping(configuration, new_mapping=None):
    """ Resolves Expression and Reference instances found in a Configuration instance, to remove the 
    need for Namespaces, into a plain Mapping instance.
    
    Args:
        configuration: a Configuration instance
        new_mapping: a Mapping instance, or None; the mapping to update with the content of the 
            input configuration; if None, an empty Mapping instance will be created and updated
    
    Returns:
        Mapping instance, either the input one, or the newly created one
    """
    if not isinstance(configuration, Mapping):
        raise ValueError("Input is not a configuration")
    
    if new_mapping is None:
        new_mapping = Mapping()
    
    for key, value in Mapping.items(configuration):
        if isinstance(value, (Expression, Reference)):
            new_value = Mapping._evaluate(configuration, value)
        elif isinstance(value, Mapping):
            new_value = convert_configuration_to_mapping(value)
        else:
            new_value = value
        new_mapping[key] = new_value
    
    return new_mapping

def _create_open_file_from_file_name_fct_from_configuration_file_path(file_path, encoding):
    """ Creates a function that takes a file name as input, and return a read-only file object 
    corresponding to the local file corresponding to this file name.
    
    The path to this file is defined by assuming that the file name constitutes a relative file path 
    to the folder specified by the input path.
    
    Args:
        file_path: string, path to a local file or folder, will be used to to determine the path to 
            the local folder where the files to be opened will be assumed to be stored
        encoding: string, the encoding value to use when opening a file using the created function
    
    Returns:
        callable, that takes a file name as input, and output a read-only file object
    """
    current_working_directory_path = os.getcwd()
    coref_configuration_abs_file_path = os.path.join(current_working_directory_path, file_path)
    configuration_files_folder_path = os.path.dirname(coref_configuration_abs_file_path)
    def _open_file_from_file_name(file_name):
        file_path_ = os.path.join(configuration_files_folder_path, file_name)
        return open(file_path_, "r", encoding=encoding)
    return _open_file_from_file_name

def load_configuration(configuration_file_path, encoding="utf-8"):
    """ Parses of the content of a configuration file, assuming possible references to other 
    configuration file(s) within this file are about file(s) located in the same folder, and returns 
    the corresponding Configuration instance.
    
    Args:
        configuration_file_path: string, path to the file whose content is a string representation 
            of the configuration to parse
        encoding: string, the encoding value to use when opening file(s) whose content is the string 
            representation of a configuration
    
    Returns:
        Configuration instance
    """
    open_stream_from_string_fct =\
     _create_open_file_from_file_name_fct_from_configuration_file_path(configuration_file_path, encoding)
    configuration_reader = ConfigurationReader(open_stream_from_string_fct=open_stream_from_string_fct)
    configuration_file_name = os.path.basename(configuration_file_path)
    configuration = configuration_reader.load(configuration_file_name)
    return configuration

def load_mapping(file_path, encoding="utf-8"):
    """ Parses of the content of a configuration file, assuming possible references to other 
    configuration file(s) within this file are about file(s) located in the same folder, and returns 
    the corresponding Mapping instance, meaning that every possible Reference instance or 
    Expression instances used within the parsed Configuration instance are evaluated.
    
    Args:
        configuration_file_path: string, path to the file whose content is a string representation 
            of the configuration to parse
        encoding: string, the encoding value to use when opening file(s) whose content is the string 
            representation of a configuration
    
    Returns:
        Mapping instance
    """
    configuration = load_configuration(file_path, encoding=encoding)
    return convert_configuration_to_mapping(configuration)

def save_mapping(mapping, file_path, encoding="utf-8"):
    """ Saves a Mapping instance content into the file specified by the input path.
    
    Args:
        mapping: a Mapping instance, the mapping whose content is to be persisted
        file_path: string, the path to the file where the string representation of the mapping will 
            be saved
        encoding: string, the encoding value to use when opening the file where the content of the 
        mapping will be persisted
    """
    with open(file_path, "w", encoding=encoding) as f:
        Mapping.save(mapping, f)
