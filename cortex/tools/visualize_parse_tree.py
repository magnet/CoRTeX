# -*- coding: utf-8 -*-

"""
Defines functions used to create a visual representation of a constituency tree, and save it in an 
image file.
"""

__all__ = ["create_tree_figure_from_tree", 
           "create_tree_figure_from_tree_line"]

import os
import tempfile
import nltk.tree
import nltk.draw.tree
import nltk.draw.util

def _create_plain_tree_figure_ps_file(tree, file_path):
    """ Creates an image file representing the input constituency tree, to the specified input path. """
    nltk.draw.tree.TreeView(tree)._cframe.print_to_file(file_path)

def _create_tree_figure_ps_file(tree, file_path):
    """ Creates an image file representing the input constituency tree, to the specified input path.
    Uses drawing parameters to make the image better than when using the default parameters.
    """
    nltk.draw.tree.TreeView(tree)._cframe.print_to_file(file_path)
    
    cf = nltk.draw.util.CanvasFrame()
    tc = nltk.draw.TreeWidget(cf.canvas(),tree)
    tc['node_font'] = 'arial 14 bold'
    tc['leaf_font'] = 'arial 14'
    tc['node_color'] = '#005990'
    tc['leaf_color'] = '#3F8F57'
    tc['line_color'] = '#175252'
    cf.add_widget(tc,10,10) # (10,10) offsets
    cf.print_to_file(file_path)
    cf.destroy()

def create_tree_figure_from_tree(tree, png_figure_file_path, temp_files_folder_path=None, plain=False):
    """ Creates an image file representing the input constituency tree.
    
    Args:
        tree: a nltk.tree.Tree instance
        png_figure_file_path: string, path to the file where the image will be written
        temp_files_folder_path: string, or None; folder path where temporary files will be created. 
            If None, a temporary folder will be created, and used for this.
        plain: boolean, whether or not to display a tree as 'plain' or not
    """
    # Process input parameters
    if temp_files_folder_path is None:
        temp_files_folder_path = tempfile.mkdtemp()
    
    create_ps_file_fct = _create_plain_tree_figure_ps_file if plain else _create_tree_figure_ps_file
    
    # Create temporary ps file
    ps_figure_file_name = "output.ps"
    ps_figure_file_path = os.path.join(temp_files_folder_path, ps_figure_file_name)
    create_ps_file_fct(tree, ps_figure_file_path)
    
    # Convert the temporary ps file into the final image file
    convert_command = "convert {} {}".format(ps_figure_file_path, png_figure_file_path)
    os.system(convert_command)
    
    # Remove the temporary ps file
    os.remove(ps_figure_file_path)

def create_tree_figure_from_tree_line(tree_line, png_figure_file_path, temp_files_folder_path=None, plain=False):
    """ Creates an image file representing the input constituency tree.
    
    Args:
        tree_line: string, formatted representation of a constituency tree (see https://www.nltk.org/_modules/nltk/tree.html)
        png_figure_file_path: string, path to the file where the image will be written
        temp_files_folder_path: string, or None; folder path where temporary files will be created. 
            If None, a temporary folder will be created, and used for this.
        plain: boolean, whether or not to display a tree as 'plain' or not
    """
    tree = nltk.tree.Tree.fromstring(tree_line)
    create_tree_figure_from_tree(tree, png_figure_file_path, temp_files_folder_path=temp_files_folder_path, plain=plain)

