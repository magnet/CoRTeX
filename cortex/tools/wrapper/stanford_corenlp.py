# -*- coding: utf-8 -*-

"""
Defines a wrapper class around the 'stanford core nlp' external tool.

When used to work on documents written in English, necessitates the "stanford-corenlp-3.8.0-models.jar" file.
The parser models defined in this jar file, for the English language, were trained on the "English Penn Treebank", 
according to the documentation found here: https://nlp.stanford.edu/software/parser-faq.html#c.
A priori it means that the annotations that they produce are the same / are compatible with the 
annotations used to qualify the documents of the Ontonotes v5 corpus.
The possible named entities extraction models, and the tags that can be annotated using them, are 
described here: https://stanfordnlp.github.io/CoreNLP/ner.html. They do not exactly overlap with the 
ones used in the Ontonotes v5 corpus.

When used to work on documents written in French, necessitates the "stanford-french-corenlp-2017-06-09-models.jar" file.
The parser models defined in this jar file, for the French language, were trained on the "French Treebank", 
according to the documentation found here: https://nlp.stanford.edu/software/parser-faq.html#c.
Upon examination, it appears that it was the "modified Crabbe & Candito" version of the "French Treebank" 
that was used.
The possible named entities extraction models, and the tags that can be annotated using them, are 
described here: https://stanfordnlp.github.io/CoreNLP/ner.html.
It is not currently possible to carry out named entities extraction for the French language.

It is assumed that those models jar files are located in the same folder as the jar files defining 
the java classes of the Stanford Core NLP software.
The path to the folder where those jar files will be searched for can be either specified directly 
when using the wrapper, or can be set in the CoRTeX's configuration file, as the 'classpath' parameter 
of the 'stanford_core_nlp' section.
"""

__all__ = ["StanfordCoreNLPWrapper", 
           "NotAllowedValueError", 
           "MissingExternalToolError",
           ]

import logging
import os
import glob
import subprocess
import tempfile
import configparser
import re

from cortex.config import current_config_parser

ANNOTATORS = {"en": ['tokenize', 'ssplit', 'pos', 'lemma', 'ner', 'parse'], 
              "fr": ['tokenize', 'ssplit', 'pos', 'parse']
              }



logger = logging.getLogger(__name__)

CORENLP_JAR_FILE = "stanford-corenlp-3.8.0-models.jar"
CORENLP_FRENCH_MODELS_JAR_FILE = "stanford-french-corenlp-2017-06-09-models.jar"


class StanfordCoreNLPWrapper(object):
    """ Wrapper for the edu.stanford.nlp.pipeline.StanfordCoreNLP java class
    
    An instance of this class encompasses the parameters to use to process documents so as, for 
    instance, to tokenize them, split them in sentences, and parse them in constituency trees.
    Such an instance will then be able to process documents as individual raw text files located in 
    a specific folder, and output the corresponding results into another specific folder.
    
    Under the hood are calls made to the StanfordCoreNLP java class.
    For the English language, the specific jar file 'stanford-corenlp-3.8.0.jar' is needed.
    For the French language, the specific jar file 'stanford-french-corenlp-2017-06-09-models.jar' 
    is needed.
    
    Arguments:
        annotators: collection of strings, whose value must be among 
            {'tokenize', 'ssplit', 'pos', 'lemma', 'ner', 'parse'}, and specifying the processing 
            operations to carry out for each document. For French, only the following values 
            are supported: {'tokenize', 'ssplit', 'pos', 'parse'}
        
        output_format: string, specifying the format of output produced for each processed document, 
            must be among {"xml", "out", "json", "conll"}
        
        lang: string, specifies which language to use when processing the documents; must be among 
            {'en', 'fr'}
        
        options: supplementary parameters to use when calling the underlying java program; see the 
            documentation of the StanfordCoreNLP suite (https://stanfordnlp.github.io/CoreNLP/cmdline.html)
        
        classpath: string, or None; specifies a folder path, or an "environment CLASSPATH" like 
            variable content, where the java code files defining the edu.stanford.nlp.pipeline.StanfordCoreNLP 
            class is located
    
    Attributes:
        classpath: path to the java jar files containing the java classes and models to use to 
            process documents
        
        cmd: the collection of strings representing the common terms of the command line used to 
            process documents
    """
    _JAVA_CLASS = 'edu.stanford.nlp.pipeline.StanfordCoreNLP'

    _OUTPUT_FORMATS = {"xml": "xml", "text": "out", "json": "json", "conll": "conll"}
    _ANNOTATORS = ANNOTATORS

    def __init__(self, annotators=None, output_format="xml", lang="en", options=None, classpath=None):
        self._logger = logging.getLogger("{}.{}".format(__name__, self.__class__.__name__))

        if lang == "en":
            _classpath = _locate_corenlp_jar_file(CORENLP_JAR_FILE, classpath=classpath)
            if not _classpath:
                msg = None if not classpath else "No jar file were found in the folder located at '{}'.".format(classpath)
                self._logger.error("Stanford CoreNLP isn't available: the task can't be performed")
                raise MissingExternalToolError(tool="Stanford CoreNLP", msg=msg)
            self.classpath = _classpath
        elif lang == "fr":
            _classpath = _locate_corenlp_jar_file(CORENLP_FRENCH_MODELS_JAR_FILE, classpath)
            if not _classpath:
                self._logger.error("Stanford CoreNLP French models aren't available: the task can't be performed")
                raise MissingExternalToolError(tool="Stanford CoreNLP French models")
            self.classpath = _classpath
        else:
            message = "Incorrect language parameter value '{}'. Correct possible values are: {}."
            message = message.format(lang, sorted(self._ANNOTATORS))
        
        if annotators is None:
            annotators = ['tokenize', 'ssplit']
        self._check_params(annotators, lang, output_format)

        self.cmd = self._create_cmd_line(self.classpath, annotators, lang, output_format, options)

    def process(self, input_folder_path, output_folder_path):
        """Processes all the files in located at the specified input folder, and write the results 
        into the specified output folder.
        
        Args:
            input_folder_path: string, path to the folder containing the files to process
            output_folder_path: string, path to the folder where the result files will be written
        """
        self._process_dir(input_folder_path, output_folder_path)

    def _process_dir(self, input_folder_path, output_folder_path):
        """ Processes the raw text files contained in the specified input folder, into the specified 
        output folder.
        
        Args:
            input_folder_path: string, path to the folder containing the files to process
            output_folder_path: string, path to the folder where the result files will be written
        """
        self._logger.debug("Running the command line calling StanfordCoreNLP...")
        
        files_list_temp_file_path = self._create_files_list(input_folder_path)
        cmd = self.cmd + ['-outputDirectory', output_folder_path, '-filelist', files_list_temp_file_path]
        self._logger.debug("Command line: %s", " ".join(cmd))
        
        regexp = re.compile(r'Processing file (.*) ... writing to (.*)')
        with subprocess.Popen(cmd,
                              stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                              universal_newlines=True, bufsize=0) as result:
            current_file = ""
            stderr_content_array = []
            for line in result.stderr:
                stderr_content_array.append(line)
                if line.startswith("Processing file"):
                    match = regexp.match(line)
                    if match:
                        current_file = os.path.relpath(match.group(1), input_folder_path)
                if current_file and line == "done.\n":
                    self._logger.debug("File %s processed", current_file)
                    current_file = ""
            
            stdout_content = result.stdout.read()
            stderr_content = "".join(stderr_content_array)
        
        self._logger.debug("Finished running the command line calling StanfordCoreNLP.")
        
        os.remove(files_list_temp_file_path)
        
        if result.stderr:
            self._logger.debug("StanfordCoreNLP subprocess' stderr:\n{}".format(stderr_content))
        if result.stdout:
            self._logger.debug("StanfordCoreNLP subprocess' stdout:\n{}".format(stdout_content))

    @classmethod
    def _check_params(cls, annotators, lang, output_format):
        """ Checks that the input parameters values are correct. 
        
        Args:
            annotators: collection of strings, each string specifying an annotator to use when 
                calling the Stanford Core NLP pipeline to process documents
            lang: string, representing the language of the documents to process
            output_format: string, representing the file format to be used by Stanford Core NLP when 
                outputing its results
        
        Raises:
            NotAllowedValueError: if at least one input value does not belong to its universe of 
            supported values
        """
        if lang not in cls._ANNOTATORS:
            raise NotAllowedValueError(value=lang, allowed_values=cls._ANNOTATORS)
        if output_format not in cls._OUTPUT_FORMATS:
            raise NotAllowedValueError(value=output_format, allowed_values=cls._OUTPUT_FORMATS)
        if any(ann not in cls._ANNOTATORS[lang] for ann in annotators):
            raise NotAllowedValueError(value=annotators, allowed_values=cls._ANNOTATORS[lang])
            #"Allowed annotators for {} language are {}".format(lang, ", ".join(cls._ANNOTATORS[lang]))

    @classmethod
    def _create_cmd_line(cls, classpath, annotators, lang, output_format, options):
        """ Creates an appropriate command line description, to be used by a subprocess to call 
        the Stanford Core NLP pipeline.
        
        Args:
            classpath: string; specifies a folder path, or an "environment CLASSPATH" like 
                variable content, where the java code files defining the 
                'edu.stanford.nlp.pipeline.StanfordCoreNLP' java class to use is located
            annotators: collection of strings, each string specifying an annotator to use when 
                calling the Stanford Core NLP pipeline to process documents
            lang: string, representing the language of the documents to process
            output_format: string, representing the file format to be used by Stanford Core NLP when 
                outputing its results
            options: collection of strings, or None; representing the possible supplementary options 
                to pass to the Stanford Core NLP pipeline when processing document files
        
        Returns:
            collection of strings, representing an suitable input for the subprocess that shall call 
            the Stanford Core NLP pipeline
        """
        cmd = ['java']
        allocated_memory_option = "-Xmx2g"
        #allocated_memory_option = "-Xmx3g"
        if classpath:
            cmd.extend(['-cp', "{}/*:".format(classpath), allocated_memory_option])
        cmd.append(cls._JAVA_CLASS)
        if lang == "fr":
            cmd.extend(['-props', 'StanfordCoreNLP-french.properties'])
        cmd.extend([
            '-annotators', ','.join(annotators),
            '-outputFormat', output_format])
        if options:
            cmd.extend(options)
        return cmd

    @staticmethod
    def _create_files_list(folder_path):
        """ Creates a temporary file specifying the path to the files to be processed by the 
        Stanford Core NLP pipeline, and which shall be passed to it when launching the processing.
        
        Args:
            folder_path: string, path to an existing local folder

        Returns:
            string, the base_path of the temporary file
        """
        files_list = [os.path.join(folder_path, filename) for filename in _list_files_relative_to(folder_path)]

        with tempfile.NamedTemporaryFile(mode="w", delete=False) as result_file:
            for input_filepath in files_list:
                result_file.write("{}\n".format(input_filepath))

        return result_file.name


def _create_stanford_core_nlp_wrapper_instance(**init_params):
    try:
        stanford_corenlp_wrapper = StanfordCoreNLPWrapper(**init_params)
    except MissingExternalToolError as e:
        classpath_options = current_config_parser.get("stanford_core_nlp", "classpath")
        msg = "Check the 'classpath' subfield's value of the 'stanford_core_nlp' field of CoRTeX' configuration file. Current value is '{}'."
        msg = msg.format(classpath_options)
        previous_message = e.args[0]
        msg = "{}\n{}".format(previous_message, msg) if previous_message else msg
        raise NotAllowedValueError(msg=msg) from None
    return stanford_corenlp_wrapper


# Utilities
def _list_files_relative_to(path):
    """ Returns the list of paths to the files in the folder specified by the input path, relative to 
    this folder. Include files in subfolders. If the input path points to a file, return a list 
    containing only the input file.
    
    Args:
        path: string, path to the folder to scan for files

    Returns:
        list of file paths, relative to "path"
    """
    input_files = []
    if os.path.isfile(path):
        input_files.append(path)
        return input_files
    else:
        for dirs, _, files in os.walk(path):
            for filename in files:
                input_files.append(os.path.relpath(os.path.join(dirs, filename), start=path))
    return input_files

def _locate_corenlp_jar_file(jar_file_name=CORENLP_JAR_FILE, classpath=None):
    """ Checks that the specified jar local file exists, potentially using configuration-provided 
    data to look for it, and return the local path to it if it does. If it could not be found, 
    return an empty string.
    
    Args:
        jar_file_name: name of the jar file that is looked for
        classpath: string, or None; string akin to a CLASSPATH environment variable that might be 
            used by java programs (that is, "/path/to/folder1:/path/to/folder2", for instance), specifies 
            where to look for the specified jar file. If None, the value specified in the cortex 
            configuration's "stanford_core_nlp.classpath" field will be used

    Returns:
        string, path to found local jar file, or empty string
    """
    if classpath and glob.glob(os.path.join(classpath, jar_file_name)):
        return classpath
    
    try:
        classpath_options = current_config_parser.get("stanford_core_nlp", "classpath")
        for path in classpath_options.split(':'):
            if glob.glob(os.path.join(path, jar_file_name)):
                return path
    except configparser.NoOptionError:
        pass

    return ""

# Exceptions
class NotAllowedValueError(ValueError):
    """ Exception to be raised when a value is not supported.
    
    Arguments:
        value: python object, or None; the offending value, will be used fort the creation of a 
            generic exception message
        allowed_values: iterable over allowed values, will be used fort the creation of a 
            generic exception message
        msg: a specific exception message to be used by this exception instance; will override the 
            default, generic exception message
    """
    def __init__(self, value=None, allowed_values=None, msg=None):
        if msg is None:
            msg = ""
            if value:
                msg += "'{}' is not allowed. ".format(value)
            if allowed_values:
                msg += "Allowed values are {}".format(", ".join(["'{}'".format(v) for v in allowed_values]))
        super(NotAllowedValueError, self).__init__(msg)

class MissingExternalToolError(Exception):
    """ Exception to be raised when an external tool is needed and can't be found.
    
    Arguments:
        tool: string, name of the tool that was expected to be usable, will be used fort the creation of a 
            generic exception message
        msg: a specific exception message to be used by this exception instance; will override the 
            default, generic exception message
    """
    def __init__(self, tool=None, msg=None):
        if msg is None:
            msg = "This software needs {} to run that task..".format(tool)
        super(Exception, self).__init__(msg)
