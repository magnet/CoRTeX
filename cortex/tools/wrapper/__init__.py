# -*- coding: utf-8 -*-

""" 
Defines wrapper functions or classes around external tools or third party modules
"""

"""
Available subpackage(s)
-----------------------
conll2012_scorer
    Hosts original conll2012 task's scorer software, and define a wrapper around it
"""

"""
Available submodule(s)
----------------------
nada
    Defines a wrapper class around the 'nada' external tool

stanford_core_nlp
    Defines a wrapper class around the 'stanford core nlp' external tool
"""

from .conll2012_scorer import CONLL2012ScorerWrapper
from .nada import NadaWrapper
from .stanford_corenlp import StanfordCoreNLPWrapper