# -*- coding: utf-8 -*-''

"""
Defines a wrapper class around the 'nada' external tool, a tool used to estimate the probability that 
an occurrence of the word 'it' in the English language be non-referential.
"""

__all__ = ["UnavailableExternalRessourceError", 
           "NadaWrapper",
           ]

import os
import subprocess

from cortex.parameters import ENCODING
from cortex.config import current_config_parser
from cortex.utils.memoize import Memoized
from cortex.utils import CoRTeXException

@Memoized
def fetch_nada_binary_location_data():
    """ Parses CoRTeX' configuration file in order to define the local location of the files needed 
    to use the 'nada' software.
    
    Returns:
        a (EXEC_FILE_PATH, WEIGHTS_FILE_PATH, NGRAMS_FILE_PATH) tuples, where:
            - 'EXEC_FILE_PATH' is a string, the path to the nada executable file
            - 'WEIGHTS_FILE_PATH' is a string, the path to the file defining the weights associated 
              to the model used by the 'nada' software
            - 'NGRAMS_FILE_PATH' is a string, the path to the file defining a collection of ngrams, 
              associated to the model's weights file, the model used by the 'nada' software
    """
    ###########################
    # Parse the configuration #
    ###########################
    NADA_CONFIG_SECTION_NAME = "nada"
    if NADA_CONFIG_SECTION_NAME not in current_config_parser:
        message = "The configuration does not contain a 'nada' section; check the configuration source."
        raise UnavailableExternalRessourceError(message)
    NADA_FOLDER_PATH = current_config_parser.get(NADA_CONFIG_SECTION_NAME, "folder_path")
    EXEC_FILE_NAME = current_config_parser.get(NADA_CONFIG_SECTION_NAME, "exec_file_name")
    WEIGTHS_FILE_NAME = current_config_parser.get(NADA_CONFIG_SECTION_NAME, "weights_file_name")
    NGRAMS_FILE_NAME = current_config_parser.get(NADA_CONFIG_SECTION_NAME, "ngrams_file_name")
    EXEC_FILE_PATH = os.path.join(NADA_FOLDER_PATH, EXEC_FILE_NAME)
    WEIGHTS_FILE_PATH = os.path.join(NADA_FOLDER_PATH, WEIGTHS_FILE_NAME)
    NGRAMS_FILE_PATH = os.path.join(NADA_FOLDER_PATH, NGRAMS_FILE_NAME)
    if not os.path.isfile(EXEC_FILE_PATH):
        msg = "There is not 'nada' exec file located at '{}', check the location or cortex' current working configuration file."
        msg = msg.format(EXEC_FILE_PATH)
        raise UnavailableExternalRessourceError(msg)
    if not os.path.isfile(WEIGHTS_FILE_PATH):
        msg = "There is not 'nada' weigths file located at '{}', check the location or cortex' current working configuration file."
        msg = msg.format(WEIGHTS_FILE_PATH)
        raise UnavailableExternalRessourceError(msg)
    if not os.path.isfile(NGRAMS_FILE_PATH):
        msg = "There is not 'nada' ngrams file located at '{}', check the location or cortex' current working configuration file."
        msg = msg.format(NGRAMS_FILE_PATH)
        raise UnavailableExternalRessourceError(msg)
    return EXEC_FILE_PATH, WEIGHTS_FILE_PATH, NGRAMS_FILE_PATH



class NadaWrapper(object):
    """ Implements a wrapper to call the 'Nada' software.
    
    The Nada software takes as an input a stream of tokenized sentences (one per line), and outputs 
    back those same sentences (one per line) with, after each sentence, a list of 
    'token n° in the sentence:non-referential proba' for each token representing the word 'it' in 
    the sentence.
    
    This class allows to take as an input a generator of tokenized sentences (str), and returns a 
    collection of collections of non-referential proba (one collection per sentence, in the same order).
    """
    
    def __init__(self):
        EXEC_FILE_PATH, WEIGHTS_FILE_PATH, NGRAMS_FILE_PATH = fetch_nada_binary_location_data()
        self._CMD = [EXEC_FILE_PATH, WEIGHTS_FILE_PATH, NGRAMS_FILE_PATH]
    
    def process_sentences(self, tokenized_sentences):
        """ Applies 'nada' on in the input sentences.
        
        Args:
            tokenized_sentences: collection of strings, each string representing a tokenized sentence

        Returns:
            collection of collections of (token sentence index; non referential proba) pairs, 
            one collection per input sentence
        """
        string = "\n".join(s.strip() for s in tokenized_sentences)
        stdout_data, _ = self._apply_nada_exec_on_string(string)
        return tuple(map(self._process_sentence_data, stdout_data.strip().split("\n")))
    
    def _apply_nada_exec_on_string(self, nada_input):
        """ Executes a subprocess to call the 'nada' software on the input string.
        
        Args:
            nada_input: string, input to communicate to the 'nada' software when launching it in a 
                shell
        
        Returns:
            a (stdout content as a string, stderr content as a string) pair; representing the output 
            of the subprocess with which the 'nada' software was called
        """
        byte_nada_input = nada_input.encode(ENCODING)
        result = subprocess.Popen(self._CMD, stdin=subprocess.PIPE, stdout=subprocess.PIPE, 
                                  stderr=subprocess.PIPE)
        stdout_data, stderr_data = result.communicate(byte_nada_input) # stdout_data, stderr_data
        return stdout_data.decode(ENCODING), stderr_data.decode(ENCODING)
    
    def _apply_nada_exec_on_file(self, input_file_path, output_file_path, err_file_path):
        """ Executes a subprocess to call the 'nada' software on the content of a file specified by 
        its path, and write the result into an output file, also specified by its path.
        
        Args:
            input_file_path: string, path to file whose content is the input to be processed by the 
                'nada' software
            output_file_path: string, path to the file where the stdout, associated to the subprocess 
                with which the 'nada' software will be called, shall be written
            err_file_path: string, path to the file where the stderr, associated to the subprocess 
                with which the 'nada' software will be called, shall be written
        """
        with open(input_file_path, "rb") as f_input,\
             open(output_file_path, "wb") as f_output,\
             open(err_file_path, "wb") as f_err:
            with subprocess.Popen(self._CMD, stdin=f_input, stdout=f_output, stderr=f_err, 
                                  universal_newlines=True, bufsize=0) as result:
                result.communicate()
    
    @staticmethod
    def _process_sentence_data(sentence_data):
        """ Parses one line of the output of the 'nada' software into the corresponding collection 
        of (token index; proba value) pairs.
        
        Args:
            sentence_data: string, result of the application of the 'nada' software to one sentence
        
        Returns:
            collection of (token_index, proba) pairs, 
            where:
                - 'token_index' is an int, representing the index of the sentence's token whose proba 
                  value to be non-referential has been estimated by the 'nada' software
                - 'proba' is a float comprised between 0. and 1., representing a proba value that one 
                  specific token of the input sentence be non-referential, as estimated by the 'nada' 
                  software
        """
        return tuple(map((lambda x: (int(x[0]), float(x[1]))), 
                         (st.split(":") for st in sentence_data.split("\t")[1:])
                        )
                    )
        

class UnavailableExternalRessourceError(CoRTeXException):
    """ Exception to be raised if failing to locate a resource that is external to CoRTeX'. """
    pass
