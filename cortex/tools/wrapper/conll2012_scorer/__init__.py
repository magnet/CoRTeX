# -*- coding: utf-8 -*-

""" 
Hosts original conll2012 task's scorer software, and defines a wrapper around it
The software can be found here: https://github.com/conll/reference-coreference-scorers.
"""

"""
Available submodule(s)
----------------------
output_parser
    Defines utilities to parse the output of the original conll2012 task's scorer software

wrapper
    Defines a wrapper class so as to be able to use the original conll2012 task's scorer from python
"""

from .wrapper import CONLL2012ScorerWrapper