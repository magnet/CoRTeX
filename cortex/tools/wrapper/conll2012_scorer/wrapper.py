# -*- coding: utf-8 -*-

"""
Defines a wrapper class so as to be able to use the original conll2012 task's scorer from python
"""

__all__ = ["RunBashScriptError", 
           "MissingExternalToolError", 
           "CONLL2012ScorerWrapper",
           ]

import os
import logging
import subprocess

from .output_parser import parse_conll_scoring_software_output, _check_metric_value

CURRENT_FOLDER_PATH = os.path.dirname(__file__)
CONLL2012_SCORER_SOFTWARE_FOLDER_PATH = os.path.join(CURRENT_FOLDER_PATH, "conll2012_scorer_software")

class CONLL2012ScorerWrapper(object):
    """ Class defining a 'evaluate' method that wraps around the original conll2012 evaluation 
    software, allowing to use it from within python.
    """
    
    def __init__(self):
        self._logger = logging.getLogger("{}.{}".format(__name__, self.__class__.__name__))
        
        if not _is_perl_installed():
            self._logger.error("Perl is not installed: the task cannot be performed.")
            raise MissingExternalToolError(tool="Perl")
    
    def evaluate(self, metric, key_multi_documents_conll2012_file_path, 
                 response_multi_documents_conll2012_file_path, name=None):
        """ Applies the conll2012 evaluation software on adequate input local files.
        
        Args:
            metric: string, the metric desired to score the results:
                - muc: MUCScorer (Vilain et al, 1995)
                - bcub: B-Cubed (Bagga and Baldwin, 1998)
                - ceafm: CEAF (Luo et al, 2005) using mention-based similarity
                - ceafe: CEAF (Luo et al, 2005) using entity-based similarity
                - blanc: BLANC
                - all: uses all the metrics to score
            key_multi_documents_conll2012_file_path: string, path to conll2012 multidocument file 
                with expected coreference chains
            response_multi_documents_conll2012_file_path: string, path to conll2012 multidocument 
                file with output of coreference detection system
            name: string, optional (default = None); the name of a specific document to evaluate.
                If name is not given, all the documents in the dataset will be evaluated. If given 
                name is "none" then all the documents are scored but only total results are shown.

        Returns:
            a collection of ("metric_name", (overall_results, document name and result pairs collection)) pairs
        """
        # Check input values
        _check_metric_value(metric)
        if not os.path.isfile(key_multi_documents_conll2012_file_path):
            msg = "'{}' is not a path to an existing file.".format(key_multi_documents_conll2012_file_path)
            raise ValueError(msg)
        if not os.path.isfile(response_multi_documents_conll2012_file_path):
            msg = "'{}' is not a path to an existing file.".format(response_multi_documents_conll2012_file_path)
            raise ValueError(msg)
        
        # Create command
        scorer_script_file_path = os.path.join(CONLL2012_SCORER_SOFTWARE_FOLDER_PATH, "scorer.pl")
        cmd = [scorer_script_file_path, metric, key_multi_documents_conll2012_file_path, response_multi_documents_conll2012_file_path]
        if name is not None:
            if not isinstance(name, str):
                msg = "The 'name' input value is not None, but is not a string either (type = '{}')."
                msg = msg.format(type(name))
                raise TypeError(msg)
            cmd = cmd + [name,]
        
        # Process command
        self._logger.debug("Command line: %s", " ".join(cmd))
        with subprocess.Popen(cmd,
                              stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                              universal_newlines=True, bufsize=0) as result:
            stdout_content = result.stdout.read()
            #stderr_content = result.stderr.read()
        self._logger.debug("Command processed.")
        
        conll_scoring_software_output = stdout_content
        line_iterator = iter(conll_scoring_software_output.split("\n"))
        results = parse_conll_scoring_software_output(line_iterator, metric, which_document=name)
        
        return results


# Utilities
def _is_perl_installed():
    """ Tests whether or not perl is installed on the computer the current python process is running on.

    Returns:
        boolean, True or False
    
    Raises:
        RunBashScriptError: if the bash script used to carry out the test did not behave as expected
    """
    script_file_path = os.path.join(CURRENT_FOLDER_PATH, "is_perl_installed.sh")
    cmd = [script_file_path,]
    with subprocess.Popen(cmd, 
                      stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                      universal_newlines=True, bufsize=0) as result:
        stdout_content = result.stdout.read().strip()
        #stderr_content = result.stderr.read().strip()
    if stdout_content == "yes":
        return True
    elif stdout_content == "no":
        return False
    msg = "Output of bash script is not the one expected ('{}', expected 'yes' our 'no')"
    msg = msg.format(stdout_content)
    raise RunBashScriptError(msg)


# Exceptions
class MissingExternalToolError(Exception):
    """ Exception to be raised when an external tool is needed and can't be found. """
    def __init__(self, tool=None, msg=None):
        if msg is None:
            msg = "This software needs {} to run that task. Check README file.".format(tool)
        super(Exception, self).__init__(msg)

class RunBashScriptError(Exception):
    """ Exception to be raised when the call to a bash script did not result in an expected output. """
    pass
