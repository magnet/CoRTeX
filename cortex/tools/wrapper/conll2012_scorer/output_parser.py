# -*- coding: utf-8 -*-

"""
Defines utilities used to parse the output of the original conll2012 task's scorer
"""

__all__ = ["CONLL2012ScoreDocumentResultIterator", 
           "NoDocumentMentionDetectionScoresLinesError", 
           "parse_conll_scoring_software_output",
           ]

import numpy
import re


class CONLL2012ScoreDocumentResultIterator(object):
    """ Class representing a read-once iterator over lines from an input line iterator.
    
    When iterated over, it will read lines until it finds the last line of a 
    'document mention detection' result written over the lines of the input line iterator.
    After that, if it is iterated over again, will try to do the same, still using the lines that 
    might exist in the line iterator.
    
    Stops, and become forever unresponsive, when the inner line iterator becomes empty.
    
    Arguments:
        line_iterator: iterator over strings
    
    Attributes:
        line_iterator: the inner iterator over strings used by the class instance
    """
    def __init__(self, line_iterator):
        self.line_iterator = line_iterator
    
    def __iter__(self):
        """ Creates a generator over lines produced by iterating over the lines iterator associated 
        to this instance.
        
        Yields:
            string
        """
        for line in self.line_iterator:
            if line.strip() == "":
                raise StopIteration
            yield line
            if line.startswith("Invented"):
                break


##################################
# Mention identification results #
##################################
def _parse_document_mention_identification_results(lines):
    """ Parses the lines collection resulting from a conll2012 evaluation procedure done over a single 
    document, and extract the data pertaining to the mention detection performance
    
    Args:
        lines: collection of lines corresponding to a file-like object created at the end of a 
            conll2012 evaluation procedure

    Returns:
        a ((first_doc_name_found, second_doc_name_found), result) pair, if the input was indeed 
        a correct iterator over the lines of a conll2012 evaluation result file-like document; or 
        None, if the input lines iterator is incorrect; where:
            - 'first_doc_name_found' is a string, the ident of the 'ref' document of the comparison 
              and evaluation made by the conll2012 evaluation procedure, assuming the input is correct. 
              If it is incorrect, may be None instead.
            - 'second_doc_name_found' is a string, the ident of the 'sys' document of the comparison 
              and evaluation made by the conll2012 evaluation procedure, assuming the input is correct. 
              If it is incorrect, may be None instead.
            - 'result' is a dictionary with the following keys: 
                - "total_key_mentions_nb"
                - "total_response_mentions_nb" 
                - "strictly_correct_identified_mentions_nb".
    """
    is_document_lines_collection = False
    first_doc_name_found = None
    second_doc_name_found = None
    for line in lines:
        m = re.search("File ([^:]*):", line)
        if m is not None:
            if first_doc_name_found is None:
                first_doc_name_found = m.groups()[0]
            elif second_doc_name_found is None:
                second_doc_name_found = m.groups()[0]
                is_document_lines_collection = True
                break
    
    # If not a true collection of lines pertaining to the description of entites for a document
    if not is_document_lines_collection:
        return
    
    # Scrap the first lines, referring to the entities
    i = -1
    while True:
        i += 1
        line = lines[i]
        if re.search("Total key mentions", line) is not None:
            good_lines = lines[i:]
            break
    total_key_mentions_nb = int(re.search("Total key mentions: (\d+)", good_lines[0]).groups()[0])
    total_response_mentions_nb = int(re.search("Total response mentions: (\d+)", good_lines[1]).groups()[0])
    strictly_correct_identified_mentions_nb = int(re.search("Strictly correct identified mentions: (\d+)", good_lines[2]).groups()[0])
    
    result = {"total_key_mentions_nb": total_key_mentions_nb, 
              "total_response_mentions_nb": total_response_mentions_nb, 
              "strictly_correct_identified_mentions_nb": strictly_correct_identified_mentions_nb,
              }
    
    return (first_doc_name_found, second_doc_name_found), result

##################
# Overall results#
##################
# MUC, BCUB, CEAFM, CEAFE
FLOAT_PATTERN = "\d*\.?\d+"
RECALL_PRECISION_F1_PATTERN_FORMAT = "Recall:[ \t]+\(({fp:})[ \t]+/[ \t]+({fp:})\) ({fp:})%[ \t]+Precision:[ \t]+\(({fp:})[ \t]+/[ \t]+({fp:})\)[ \t]+({fp:})%[ \t]+F1: ({fp:})%"
RECALL_PRECISION_F1_PATTERN = RECALL_PRECISION_F1_PATTERN_FORMAT.format(fp=FLOAT_PATTERN)
def _parse_metric_overall_results(metric_overall_result_lines):
    """ Parses the lines sub-collection resulting from a conll2012 evaluation procedure that pertain 
    to the overall results for one of the following metrics: {'muc', 'bcub', 'ceafm', 'ceafe'}, and 
    extracts the corresponding data.
    
    Args:
        metric_overall_result_lines: sub-collection of lines, corresponding to a file-like object 
            created at the end of a conll2012 evaluation procedure, and that pertain to the overall results 
            for one of the following metrics: {'muc', 'bcub', 'ceafm', 'ceafe'}.

    Returns:
        a dictionary, with the following 
        keys:
            - "recall_num"
            - "recall_denom"
            - "recall"
            - "precision_num"
            - "precision_denom"
            - "precision"
            - "f1"
            - "overall_mention_identification_recall_num"
            - "overall_mention_identification_recall_denom"
            - "overall_mention_identification_recall"
            - "overall_mention_identification_precision_num"
            - "overall_mention_identification_precision_denom"
            - "overall_mention_identification_precision"
            - "overall_mention_identification_f1"
    """
    overall_mention_identification_scores = tuple(float(s) for s in re.search(RECALL_PRECISION_F1_PATTERN, metric_overall_result_lines[1]).groups())
    metric_scores = tuple(float(s) for s in re.search(RECALL_PRECISION_F1_PATTERN, metric_overall_result_lines[3]).groups())
    overall_results = {}
    for key_prefix, scores in zip(("overall_mention_identification_", ""), 
                                  (overall_mention_identification_scores, metric_scores)):
        for i, key_suffix in enumerate(("recall_num", "recall_denom", "recall", "precision_num", "precision_denom", "precision", "f1")):
            key = "{}{}".format(key_prefix, key_suffix)
            overall_results[key] = scores[i]
    
    return overall_results

# BLANC
def _parse_blanc_overall_results(blanc_overall_result_lines):
    """ Parses the lines sub-collection resulting from a conll2012 evaluation procedure that pertain 
    to the overall results for the 'blanc' metric, and extract the corresponding data.
    
    Args:
        blanc_overall_result_lines: sub-collection of lines, corresponding to a file-like object 
            created at the end of a conll2012 evaluation procedure, and that pertain to the overall results 
            for the 'blanc' metric.

    Returns:
        a dictionary, with the following 
        keys:
            - "overall_mention_identification_recall_num", "overall_mention_identification_recall_denom", 
              "overall_mention_identification_recall", "overall_mention_identification_precision_num", 
              "overall_mention_identification_precision_denom", "overall_mention_identification_precision", 
              "overall_mention_identification_f1"
            - "coreference_recall_num", "coreference_recall_denom", "coreference_recall", 
              "coreference_precision_num", "coreference_precision_denom", "coreference_precision", 
              "coreference_f1"
            - "non_coreference_recall_num", "non_coreference_recall_denom", "non_coreference_recall", 
              "non_coreference_precision_num", "non_coreference_precision_denom", "non_coreference_precision", 
              "non_coreference_f1"
            - "blanc_recall_num", "blanc_recall_denom", "blanc_recall", "blanc_precision_num", 
              "blanc_precision_denom", "blanc_precision", "blanc_f1"
    """
    string2result_key = {"Identification of Mentions": "overall_mention_identification",
                         "Coreference links": "coreference", 
                         "Non-coreference links": "non_coreference", 
                         "BLANC": "blanc"}
    COMMON_PATTERN = "([^:]+): +" + RECALL_PRECISION_F1_PATTERN_FORMAT.format(fp=FLOAT_PATTERN)
    overall_results = {}
    for line in blanc_overall_result_lines:
        m = re.search(COMMON_PATTERN, line)
        if m is not None:
            (string_result_key, recall_num, recall_denom, recall, precision_num, precision_denom, 
             precision, f1) = m.groups()
            result_key = string2result_key[string_result_key]
            for score_name, score_str_value in zip(("recall_num", "recall_denom", "recall", "precision_num", "precision_denom", "precision", "f1"), 
                                                   (recall_num, recall_denom, recall, precision_num, precision_denom, precision, f1)):
                overall_results["{}_{}".format(result_key, score_name)] = float(str(score_str_value))
    
    return overall_results

###################################
# Single document results parsers #
###################################
# MUC, BCUB, CEAFM, CEAFE
def _parse_conll_metric_single_document_results(line_iterator):
    """ Consumes the needed lines of the input line iterator, resulting from a conll2012 evaluation 
    procedure, in order to parse the results corresponding to the evaluation of a single document by 
    a metric among {'muc', 'bcub', 'ceafm', 'ceafe'}.
    
    Args:
        line_iterator: iterator over lines of a file-like object created at the end of a conll2012 
            evaluation procedure

    Returns:
        a ((first_doc_name_found, second_doc_name_found), metric_document_results) tuple, 
        where:
            - metric_document_results is a dictionary with the following 
              keys:
                - "total_key_mentions_nb"
                - "total_response_mentions_nb" 
                - "strictly_correct_identified_mentions_nb".
                - "coreference_recall_numv"
                - "coreference_recall_denom"
                - "coreference_recall"
                - "coreference_precision_num"
                - "coreference_precision_denom"
                - "coreference_precision"
                - "coreference_f1"
    """
    # First, parse mention detection score values
    document_mention_identification_results_lines = tuple(CONLL2012ScoreDocumentResultIterator(line_iterator))
    parse_result = _parse_document_mention_identification_results(document_mention_identification_results_lines)
    if parse_result is None:
        msg = "The following lines are not describing mention detection score in a document:\n{}"
        msg = msg.format("\n".join(document_mention_identification_results_lines))
        raise NoDocumentMentionDetectionScoresLinesError(msg)
    
    (first_doc_name_found, second_doc_name_found), mention_identification_result = parse_result
    
    # Then, parse the metric's recall, precision and f1 score values
    remaining_lines = tuple(next(line_iterator) for _ in range(2))
    (coreference_recall_num, coreference_recall_denom, coreference_recall, 
     coreference_precision_num, coreference_precision_denom, coreference_precision, 
     coreference_f1) = tuple(float(s) for s in re.search(RECALL_PRECISION_F1_PATTERN, remaining_lines[0]).groups())
    
    # Prepare output
    metric_document_results = {"coreference_recall_num": coreference_recall_num, 
                               "coreference_recall_denom": coreference_recall_denom,
                               "coreference_recall": coreference_recall,  
                               "coreference_precision_num": coreference_precision_num, 
                               "coreference_precision_denom": coreference_precision_denom, 
                               "coreference_precision": coreference_precision, 
                               "coreference_f1": coreference_f1,
                              }
    metric_document_results.update(mention_identification_result)
    
    return (first_doc_name_found, second_doc_name_found), metric_document_results

# BLANC
def _parse_conll_blanc_single_document_results(line_iterator):
    """ Consumes the needed lines of the input line iterator, resulting from a conll2012 evaluation 
    procedure, in order to parse the results corresponding to the evaluation of a single document 
    by the 'BLANC' metric.
    
    Args:
        line_iterator: iterator over lines of a file-like object created at the end of a conll2012 
            evaluation procedure

    Returns:
        a ((first_doc_name_found, second_doc_name_found), results) tuple, 
        where:
            - 'first_doc_name_found' is a string, the ident of the 'ref' document of the comparison 
              and evaluation made by the conll2012 evaluation procedure, assuming the input is correct. 
              If it is incorrect, may be None instead.
            - 'second_doc_name_found' is a string, the ident of the 'sys' document of the comparison 
              and evaluation made by the conll2012 evaluation procedure, assuming the input is correct. 
              If it is incorrect, may be None instead.
            - 'results' is a dictionary with the following keys:
                - "total_key_mentions_nb", "total_response_mentions_nb", "strictly_correct_identified_mentions_nb"
                - "coreference_recall_num", "coreference_recall_denom", "coreference_recall", 
                  "coreference_precision_num", "coreference_precision_denom", "coreference_precision", "coreference_f1"
                - "non_coreference_recall_num", "non_coreference_recall_denom", "non_coreference_recall", 
                  "non_coreference_precision_num", "non_coreference_precision_denom", "non_coreference_precision", 
                  "non_coreference_f1",
                - "blanc_recall_num", "blanc_recall_denom", "blanc_recall", "blanc_precision_num", 
                  "blanc_precision_denom", "blanc_precision", "blanc_f1"
    """
    # First, parse mention detection score values
    document_mention_identification_results_lines = tuple(CONLL2012ScoreDocumentResultIterator(line_iterator))
    parse_result = _parse_document_mention_identification_results(document_mention_identification_results_lines)
    if parse_result is None:
        msg = "The following lines are not describing mention detection score in a document:\n{}"
        msg = msg.format("\n".join(document_mention_identification_results_lines))
        raise NoDocumentMentionDetectionScoresLinesError(msg)
    (first_doc_name_found, second_doc_name_found), mention_identification_result = parse_result
    
    # Then, parse the metric's recall, precision and f1 score values
    remaining_lines = tuple(next(line_iterator) for _ in range(11))
    overall_results = _parse_blanc_overall_results(remaining_lines)
    
    # Prepare output
    results = dict(overall_results)
    ## Remove 'global_mention_identification' keys
    to_del_keys = []
    for key in results.keys():
        if key.startswith("overall_mention_identification"):
            to_del_keys.append(key)
    for key in to_del_keys:
        del results[key]
    ## Add data to compute mention identification scores
    results.update(mention_identification_result)
    '''
    for key, score in zip(("total_key_mentions_nb", "total_response_mentions_nb", "strictly_correct_identified_mentions_nb"), 
                          (total_key_mentions_nb, total_response_mentions_nb, strictly_correct_identified_mentions_nb)):
        results[key] = score
    '''
    return (first_doc_name_found, second_doc_name_found), results

###################################
# Multi documents results parsers #
###################################
# MUC, BCUB, CEAFM, CEAFE
def _parse_conll_metric_multi_documents_results(line_iterator):
    """ Parses the input line iterator, resulting from a conll2012 evaluation procedure, in order to 
    extract the results corresponding to the evaluation of a corpus of document(s) using a metric 
    among {'muc', 'bcub', 'ceafm', 'ceafe'}.
    
    Args:
        line_iterator: iterator over lines of a file-like object created at the end of a conll2012 
            evaluation procedure

    Returns:
        a (overall_results, results_per_document) pair, 
        where:
            - 'overall_results' is an output of the call to the '_parse_metric_overall_results' function
            - 'document_idents_and_result_pairs' is a collection of (doc_ients, metric_document_results) 
              pairs, where:
                  - 'doc_idents' is the pair of document idents associated to the parsed evaluation 
                    results
                  - 'metric_document_results' is a dictionary with the following keys:
                      - "total_key_mentions_nb", "total_response_mentions_nb", "strictly_correct_identified_mentions_nb"
                      - "coreference_recall_num", "coreference_recall_denom", "coreference_recall", 
                      "coreference_precision_num", "coreference_precision_denom", "coreference_precision", "coreference_f1"
    """
    # First, parse document individual metric_document_results
    results_per_document = []
    while True: # Iterate over results provided for each document
        # First, parse mention detection score values
        document_mention_identification_results_lines = tuple(CONLL2012ScoreDocumentResultIterator(line_iterator))
        if not len(document_mention_identification_results_lines):
            break
        
        parse_result = _parse_document_mention_identification_results(document_mention_identification_results_lines)
        if parse_result is None:
            msg = "The following lines are not describing mention detection score in a document:\n{}"
            msg = msg.format("\n".join(document_mention_identification_results_lines))
            raise NoDocumentMentionDetectionScoresLinesError(msg)
        (first_doc_name_found, second_doc_name_found), mention_identification_result = parse_result
        
        # Then, parse the metric's recall, precision and f1 score values
        remaining_lines = tuple(next(line_iterator) for _ in range(2))
        (coreference_recall_num, coreference_recall_denom, coreference_recall, 
         coreference_precision_num, coreference_precision_denom, coreference_precision, 
         coreference_f1) = tuple(float(s) for s in re.search(RECALL_PRECISION_F1_PATTERN, remaining_lines[0]).groups())
    
        # Prepare output
        metric_document_results = {"coreference_recall_num": coreference_recall_num, 
                                   "coreference_recall_denom": coreference_recall_denom,
                                   "coreference_recall": coreference_recall,  
                                   "coreference_precision_num": coreference_precision_num, 
                                   "coreference_precision_denom": coreference_precision_denom, 
                                   "coreference_precision": coreference_precision, 
                                   "coreference_f1": coreference_f1,
                                  }
        metric_document_results.update(mention_identification_result)
        
        doc_idents = (first_doc_name_found, second_doc_name_found)
        results_per_document.append((doc_idents, metric_document_results))
    
    # Then, parse overall metric_document_results
    remaining_lines = tuple(next(line_iterator) for _ in  range(5))
    overall_results = _parse_metric_overall_results(remaining_lines)
        
    return overall_results, results_per_document

# BLANC
def _parse_conll_blanc_multi_document_results(line_iterator):
    """ Parses the input line iterator, resulting from a conll2012 evaluation procedure, in order to 
    extract the results corresponding to the evaluation of a corpus of document(s) using the 'BLANC' 
    metric.
    
    Args:
        line_iterator: iterator over lines of a file-like object created at the end of a conll2012 
            evaluation procedure

    Returns:
        a (overall_results, document_idents_and_result_pairs) pair, 
        where:
            - 'overall_results' is an output of the call to the '_parse_blanc_overall_results' function
            - 'document_idents_and_result_pairs' is a collection of (doc_idents, mention_identification_result) 
              pairs, where:
                  - 'doc_idents' is the pair of document idents associated to the parsed mention 
                    identification results
                  - 'mention_identification_result' is a dictionary with the following keys:
                    - "total_key_mentions_nb"
                    - "total_response_mentions_nb"
                    - "strictly_correct_identified_mentions_nb"
    """
    # First, parse document individual results
    ## Parse each document's collection of line, and the BLANC score's collection of lines
    lines_per_document = []
    while True:
        document_mention_identification_results_lines = tuple(CONLL2012ScoreDocumentResultIterator(line_iterator))
        if not len(document_mention_identification_results_lines):
            break
        lines_per_document.append(document_mention_identification_results_lines)
    ## Get documents' mention identification results
    document_idents_and_result_pairs = [(found_doc_idents, mention_identification_result)\
                                         for found_doc_idents, mention_identification_result in (_parse_document_mention_identification_results(document_lines)\
                                                                                                 for document_lines in lines_per_document
                                                                                                )
                                        ]
    
    # Then, parse overall results
    blanc_score_lines = tuple(next(line_iterator) for _ in range(11))
    overall_results = _parse_blanc_overall_results(blanc_score_lines)
    
    return overall_results, document_idents_and_result_pairs


##### While file parsing #####
METRIC_PARAMETER_POSSIBLE_VALUES = set(("muc", "bcub", "ceafm", "ceafe", "blanc", "all"))
def _check_metric_value(metric):
    if metric not in METRIC_PARAMETER_POSSIBLE_VALUES:
        msg = "Incorrect 'metric' input value, possible values are: {}."
        msg = msg.format(METRIC_PARAMETER_POSSIBLE_VALUES)
        raise ValueError(msg)

METRIC_NAME2DOCUMENT_RESULT_PARSER_FCT = {"muc": _parse_conll_metric_single_document_results, 
                                          "bcub": _parse_conll_metric_single_document_results,
                                          "ceafm": _parse_conll_metric_single_document_results, 
                                          "ceafe": _parse_conll_metric_single_document_results,
                                          "blanc": _parse_conll_blanc_single_document_results, 
                                          }

METRIC_NAME2MULTI_DOCUMENT_RESULT_PARSER_FCT = {"muc": _parse_conll_metric_multi_documents_results, 
                                                "bcub": _parse_conll_metric_multi_documents_results,
                                                "ceafm": _parse_conll_metric_multi_documents_results, 
                                                "ceafe": _parse_conll_metric_multi_documents_results,
                                                "blanc": _parse_conll_blanc_multi_document_results, 
                                                }
METRIC_NAME2LINE_NB_AND_PARSE_OVERALL_METRIC_RESULTS_FCT_PAIR = {"muc": (5, _parse_metric_overall_results), 
                                                                 "bcub": (5, _parse_metric_overall_results),
                                                                 "ceafm": (5, _parse_metric_overall_results), 
                                                                 "ceafe": (5, _parse_metric_overall_results),
                                                                 "blanc": (11, _parse_blanc_overall_results), 
                                                                }
def _parse_a_line(line_iterator):
    """ Consumes and return one element of the input line iterator, raising a ValueError exception 
    if doing so was not possible because the input iterator was empty
    
    Args:
        line_iterator: iterator over lines of a file-like object created at the end of a conll2012 
            evaluation procedure

    Returns:
        the next line of the input iterator
    """
    try:
        line = next(line_iterator)
    except StopIteration:
        msg = "Iterator over lines got empty before parsing results for all five metrics."
        raise ValueError(msg) from None
    return line

def _parse_one_metric_overall_results(line_iterator, metric_name):
    """ Consumes the needed lines from the input lines iterator, that are supposed to describe the 
    overall results of a conll2012 evaluation procedure done over a corpus of document(s) for the 
    specified metric, and return those results.
    
    Args:
        line_iterator_: iterator over lines of a file-like object created at the end of a conll2012 
            evaluation procedure; must have been already consumed so that the next line to be consumed is 
            the one expected by the overall results parsing function corresponding to the specified metric
        metric_name: name of the specified metric, among {'muc', 'bcub', 'ceafm', 'ceafe', 'blanc'}

    Returns:
        the dictionary containing the overall results data
    """
    line_nb, parse_overall_results_fct = METRIC_NAME2LINE_NB_AND_PARSE_OVERALL_METRIC_RESULTS_FCT_PAIR[metric_name]
    lines = tuple(next(line_iterator) for _ in range(line_nb))
    overall_results = parse_overall_results_fct(lines)
    return overall_results

def _tally_one_metric_overall_results(line_iterator_, metric_name_, metrics_results_collection_):
    """ Consumes the needed lines from the input lines iterator, that are supposed to describe the 
    overall results of a conll2012 evaluation procedure done over a corpus of document(s) for the 
    specified metric, and add those results to the input editable collection.
    
    Args:
        line_iterator_: iterator over lines of a file-like object created at the end of a conll2012 
            evaluation procedure; must have been already consumed so that the next line to be consumed is 
            the one expected by the overall results parsing function corresponding to the specified metric
        metric_name_: name of the specified metric, among {'muc', 'bcub', 'ceafm', 'ceafe', 'blanc'}
        metrics_results_collection_: collection of (metric_name_, (overall_results, results_per_document)) 
            tuples, to whom the parsed results data will be appended. Here, 'results_per_document' is an empty list;
    """
    _ = _parse_a_line(line_iterator_)
    overall_results = _parse_one_metric_overall_results(line_iterator_, metric_name_)
    results_per_document = []
    metrics_results = (metric_name_, (overall_results, results_per_document))
    metrics_results_collection_.append(metrics_results)

def _tally_one_metric_multi_documents_corpus_results(line_iterator_, metric_name_, metrics_results_collection_):
    """ Consumes the needed lines from the input lines iterator, that are supposed to describe the 
    per-document and overall results of a conll2012 evaluation procedure done over a corpus of 
    document(s) using the specified metric, and add those results to the input editable collection.
    
    Args:
        line_iterator_: iterator over lines of a file-like object created at the end of a conll2012 
            evaluation procedure; must have been already consumed so that the next line to be consumed is 
            the one expected by the per-document and overall results parsing function corresponding to the 
            specified metric
        metric_name_: name of the specified metric, among {'muc', 'bcub', 'ceafm', 'ceafe', 'blanc'}
        metrics_results_collection_: collection of (metric_name_, (overall_results, results_per_document)) 
            tuples, to whom the parsed results data will be appended.
    """
    multi_document_result_parser_fct = METRIC_NAME2MULTI_DOCUMENT_RESULT_PARSER_FCT[metric_name_]
    metric_overall_results, results_per_document = multi_document_result_parser_fct(line_iterator_)
    results_per_document = sorted(results_per_document)
    metrics_results = (metric_name_, (metric_overall_results, results_per_document))
    metrics_results_collection_.append(metrics_results)

def _tally_one_metric_single_document_corpus_results(line_iterator_, metric_name_, metrics_results_collection_):
    """ Consumes the needed lines from the input lines iterator, that are supposed to describe the 
    per-document and overall results of a conll2012 evaluation procedure done over a corpus made of 
    only one document using the specified metric, and add those results to the input editable collection.
    
    Args:
        line_iterator_: iterator over lines of a file-like object created at the end of a conll2012 
            evaluation procedure done over a corpus made of only one document; must have been already consumed 
            so that the next line to be consumed is the one expected by the per-document and overall results 
            parsing function corresponding to the specified metric
        metric_name_: name of the specified metric, among {'muc', 'bcub', 'ceafm', 'ceafe', 'blanc'}
        metrics_results_collection_: collection of (metric_name_, (overall_results, results_per_document)) 
            tuples, to whom the parsed results data will be appended.
    """
    # Parse the document results
    document_result_parser_fct = METRIC_NAME2DOCUMENT_RESULT_PARSER_FCT[metric_name_]
    found_doc_idents, document_results = document_result_parser_fct(line_iterator_)
    results_per_document = [(found_doc_idents, document_results)]
    
    # Parse the overall results
    if metric_name_ == "blanc":
        # In that case, the 'BLANC' metric overall results correspond to those of the document, since the corpus is made of only one document.
        # All that is left is to recompute the mention detection overall results from those obtained for the document.
        overall_results = dict(document_results)
        ## Compute wanted values
        total_key_mentions_nb = overall_results["total_key_mentions_nb"]
        total_response_mentions_nb = overall_results["total_response_mentions_nb"]
        strictly_correct_identified_mentions_nb = overall_results["strictly_correct_identified_mentions_nb"]
        recall_num = float(strictly_correct_identified_mentions_nb)
        recall_denom = float(total_key_mentions_nb)
        recall = _reformat_percentage_like_conll2012_scorer(0 if recall_denom == 0 else 100* recall_num / recall_denom)
        precision_num = float(strictly_correct_identified_mentions_nb)
        precision_denom = float(total_response_mentions_nb)
        precision =  _reformat_percentage_like_conll2012_scorer(0 if recall_denom == 0 else 100* precision_num / precision_denom)
        f1 = _reformat_percentage_like_conll2012_scorer(0 if precision + recall == 0 else 2* (precision * recall) / (precision + recall))
        ## Add wanted (key; value) pairs
        for key_suffix, score in zip(("recall_num", "recall_denom", "recall", "precision_num", "precision_denom", "precision", "f1"),
                                     (recall_num, recall_denom, recall, precision_num, precision_denom, precision, f1)):
            key = "overall_mention_identification_{}".format(key_suffix)
            overall_results[key] = score
        ## Remove unwanted (key; value) pairs
        for key in ("total_key_mentions_nb", "total_response_mentions_nb", "strictly_correct_identified_mentions_nb"):
            del overall_results[key]
    
    else:
        _ = _parse_a_line(line_iterator_)
        lines = tuple(next(line_iterator_) for _ in range(5))
        overall_results = _parse_metric_overall_results(lines)
        
    # Append to output
    metrics_results = (metric_name_, (overall_results, results_per_document))
    metrics_results_collection_.append(metrics_results)


################
# API function #
################   
def parse_conll_scoring_software_output(line_iterator, metric_name, which_document=None):
    """ Parses the lines from the input line iterator, that are supposed to describe the results of 
    a conll2012 evaluation procedure, and return the parsed results.
    
    Args:
        line_iterator: iterator over all lines output by the conll2012 scorer software
        metric_name: string, name of the metric parameter that was input in the conll2012 software 
            in order to generate the results to be parsed. If incorrect, may cause the parsing to fail, 
            or to output garbage data
        which_document: 'none', None, or 
            other:
                - if 'none', assume that the no individual document results is present in the output of the 
                  conll2012 evaluation software, so that there is only overall results to be parsed
                - if None, try to parse per-document results as well as overall results, and assume that the 
                  evaluation corpus contains at least two documents
                - else, try to parse per-document results as well as overall results, and assume that the 
                  evaluation corpus contains only two document

    Returns:
        a collection of ("metric_name", (overall_results, document_idents_and_result_pairs)) 
        tuples, where:
            - 'metric_name' is a string, the name of the metric used to carry out the evaluation whose 
              results were parsed
            - 'overall_results' is the output of the overall results parsing function dedicated to 
              the metric whose name was input
            - 'document_idents_and_result_pairs' is a collection of (document_idents, results) pairs, 
              where:
                  - 'document_idents' is the pair of document idents associated to the parsed results
                  - 'results' is a dict containing the evaluation results associated to the pair of 
                    documents being evaluated, whose content may vary from one metric to another
    """
    _check_metric_value(metric_name)
    
    metrics_results_collection = []
    # No document
    if which_document == "none":
        # Pass the 'version' line
        _ = next(line_iterator)
        # Consume lines and parse overall results
        if metric_name != "all":
            _tally_one_metric_overall_results(line_iterator, metric_name, metrics_results_collection)
        else:
            while len(metrics_results_collection) < 5:
                line = _parse_a_line(line_iterator)
                m = re.search("METRIC (muc|bcub|ceafm|ceafe|blanc):", line)
                if m is not None:
                    metric_name = m.groups()[0]
                    _tally_one_metric_overall_results(line_iterator, metric_name, metrics_results_collection)
    
    # All document(s)
    elif which_document is None:
        # Pass the 'version' line
        _ = next(line_iterator)
        # Consume lines and parse per-document and overall results
        if metric_name != "all":
            _tally_one_metric_multi_documents_corpus_results(line_iterator, metric_name, metrics_results_collection)
        else:
            while len(metrics_results_collection) < 5:
                line = _parse_a_line(line_iterator)
                m = re.search("METRIC (muc|bcub|ceafm|ceafe|blanc):", line)
                if m is not None:
                    metric_name = m.groups()[0]
                    _tally_one_metric_multi_documents_corpus_results(line_iterator, metric_name, metrics_results_collection)
    
    # One specific document
    else:
        # Pass the 'version' line
        _ = next(line_iterator)
        # Consume lines and parse per-document and overall results
        if metric_name != "all":
            _tally_one_metric_single_document_corpus_results(line_iterator, metric_name, metrics_results_collection)
        else:
            while len(metrics_results_collection) < 5:
                line = _parse_a_line(line_iterator)
                m = re.search("METRIC (muc|bcub|ceafm|ceafe|blanc):", line)
                if m is not None:
                    metric_name = m.groups()[0]
                    _tally_one_metric_single_document_corpus_results(line_iterator, metric_name, metrics_results_collection)
    
    # Format output
    metrics_results_collection = tuple(metrics_results_collection)
    
    return metrics_results_collection


# Utilities
def _reformat_percentage_like_conll2012_scorer(number, dec_nb=2):
    """ Format a number, assumed to be a percentage, into a string, such that is displayed in the 
    text output of the original conll2012 scorer.
    
    Args:
        number: float, whose value should be comprised between 0. and 100.
        dec_nb: non-negative int, number of decimal to keep when flooring the input number, before 
        formating it to a string  
    
    Returns:
        float
    """
    coef = 10**dec_nb
    return float("{:5.2f}".format(numpy.floor(coef * number) / coef))


# Exceptions
class NoDocumentMentionDetectionScoresLinesError(Exception):
    """ Exception to be raised when failing to parse a collection of lines for mention detection scores. """
    pass
