# -*- coding: utf-8 -*-

""" 
Third party modules or packages used by the toolbox, wrapper around such modules, or tools made from those
"""

"""
Available subpackage(s)
-----------------------
wrapper
    Defines wrapper functions or classes around external tools or third party modules
"""

"""
Available submodule(s)
----------------------
configuration
    Defines mapping structures useful for configuration representation and persistence

ltqnorm
    Implement a lower tail quantile for standard normal distribution function

mst
    Implement functions to compute min/maximum spanning trees from graph definition

munkres
    Implement the Kuhn-Munkres algorithm, used to solve the Assignement Problem

visualize_parse_tree
    Implement basic functions that display constituency trees

ml_features
    Defines structures to define and use a vectorization process for arbitrary sample objects

configuration_mixin
    Defines a MixIn class such that an instance of a child class can be represented / created from 
    a configuration
"""

from .configuration import Mapping, save_mapping, load_mapping, deepcopy_mapping
from .configuration_mixin import (ConfigurationMixIn, 
                                  define_check_item_name_function, define_create_function, 
                                  define_create_from_factory_configuration_function, 
                                  define_generate_factory_configuration_function,
                                  _create_simple_get_parameter_value_from_configuration,
                                  _create_simple_set_parameter_value_in_configuration,
                                  _create_attribute_data,)
from .wrapper import CONLL2012ScorerWrapper
from .wrapper import NadaWrapper
from .wrapper import StanfordCoreNLPWrapper
