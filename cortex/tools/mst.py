# -*- coding: utf-8 -*-
""" Defines functions used to compute min/maximum spanning trees from graph definitions.

The aim of this module is to define algorithms used to compute minimum or maximum spanning tress on 
undirected graphs.
Here, an undirected graph is defined as a (vertices, edges) pair, 
where:
    - 'vertices' is a collection of hashable items that are considered th be the nodes of the graph
    - 'edges' is a "vertice pair => value" map (to consider the graph undirected, the algorithm will 
      assume that if the map define a value to be associated with the (i1,i2), it can define the same 
      value to be associated with the (i2,i1) pair)
"""

__all__ = ["prim_mst", 
           "prim_msf", 
           "kruskal_mst", 
           "kruskal_msf", 
           ]

import heapq
import logging
from collections import defaultdict

LOGGER = logging.getLogger("cortex.tools.mst")


# PRIM ALGORITHM
POSSIBLE_SPANNING_TREE_TYPES = ("min", "max")
def prim_mst(vertices, weights, spanning_tree_type="max"):
    """ Carries out the Prim algorithm for maximum/minimum spanning tree computation.
    
    Assumes 
    that:
        - only weights corresponding to (node1, node2) pairs where node1 < node2 are given (undirected graph)
        - the graph is connected. If it is not, prefer using the 'prim_msf' function.
    
    Args:
        vertices: collection of the unique nodes that make up the graph (a strict ordering must be defined on nodes)
        weights: a "(node1,node2) pair => value" map
        spanning_tree_type: string, either 'min' or 'max', the type of spanning tree to produce
    
    Returns:
        a (s, e, w) tuple 
        where:
            - 's' is the set of nodes that can be reached with the produced tree
            - 'e' is the collection of edges (a (u,v) node pair) that make up the produced tree
            - 'w' is the total weight resulting of walking along all the edges that make up the mst tree
    """
    # Check input value
    if spanning_tree_type not in POSSIBLE_SPANNING_TREE_TYPES:
        message = "prim_mst: incorrect 'spanning_tree_type' input value ('{}'), possible values are: {}."
        message = message.format(spanning_tree_type, POSSIBLE_SPANNING_TREE_TYPES)
        raise ValueError(message)
    
    # Initialize MST with first element
    initial_node = vertices[0] # FIXME: use 'initial_node' as an input of the algorithm instead of 'vertices'?
    mst_nodes = set((initial_node,))
    mst_edges = []
    
    # Compute the adjacency matrix 
    ## Define helper function
    if spanning_tree_type == "min":
        _convert_weight_fct = lambda x: x
    elif spanning_tree_type == "max":
        _convert_weight_fct = lambda x: -x
    ## Carry out the computation itself
    adjacency_matrix = defaultdict(list) 
    for node1, node2 in weights:
        weight = _convert_weight_fct(weights[node1,node2])
        adjacency_matrix[node1].append((weight, node1, node2))
        adjacency_matrix[node2].append((weight, node2, node1))
    
    # Carry out the PRIM algorithm
    ## Initialize priority queue
    usable_edges = adjacency_matrix[initial_node][:]
    heapq.heapify(usable_edges)
    ## Greedily add new nodes
    total_weight = 0.0
    while usable_edges:
        weight, node1, node2 = heapq.heappop(usable_edges)
        if node2 not in mst_nodes:
            total_weight = total_weight + weight
            mst_nodes.add(node2)
            if node1 < node2: 
                mst_edges.append((node1, node2))
            else:
                mst_edges.append((node2, node1))
            for edge in adjacency_matrix[node2]:
                if edge[2] not in mst_nodes:
                    heapq.heappush(usable_edges, edge)
    total_weight = _convert_weight_fct(total_weight)
    
    if len(mst_nodes) < len(vertices):
        LOGGER.warning("(prim_mst): graph is not connected, cannot compute spanning tree! Returning only partial tree...")
    
    return mst_nodes, mst_edges, total_weight


def prim_msf(vertices, weights, spanning_tree_type="max"):
    """ Carries out the Prim algorithm for maximum/minimum spanning forest computation.
    
    Assumes 
    that:
        - only weights corresponding to (node1, node2) pairs where node1 < node2 are given (undirected graph)
        - the graph is not necessarily connected, an edge does not exist if its weight is not given
    
    Args:
        vertices: collection of the unique nodes that make up the graph (a strict ordering must be defined on nodes)
        weights: a "(node1,node2) pair => value" map
        spanning_tree_type: string, either 'min' or 'max', the type of spanning tree to produce
    
    Returns:
        collection of (s, e, weight) tuples, each tuple corresponding to a disjoint mst subtree, 
        where:
            - 's' is the set of nodes that can be reached with the mst tree
            - 'e' is the collection of edges (a (node1,node2) node pair) that make up the mst tree
            - 'weight' is the total weight resulting of walking along all the edges that make up the mst trees forest
    """
    # Check input value
    if spanning_tree_type not in POSSIBLE_SPANNING_TREE_TYPES:
        message = "prim_msf: incorrect 'spanning_tree_type' input value ('{}'), possible values are: {}."
        message = message.format(spanning_tree_type, POSSIBLE_SPANNING_TREE_TYPES)
        raise ValueError(message)
    
    # Compute the adjacency matrix
    ## Define helper function
    if spanning_tree_type == "min":
        _convert_weight_fct = lambda x: x
    elif spanning_tree_type == "max":
        _convert_weight_fct = lambda x: -x
    ## Carry out the computation itself
    adjacency_matrix = defaultdict(list) 
    for node1, node2 in weights:
        weight = _convert_weight_fct(weights[node1,node2])
        adjacency_matrix[node1].append((weight, node1, node2))
        adjacency_matrix[node2].append((weight, node2, node1))
    
    # Carry out the PRIM algorithm
    remaining_nodes = set(vertices) # Used for disconnected graphs # FIXME: should we not sort the vertices first? So that the algo be deterministic with respect to its output?
    forest = []
    while remaining_nodes:
        # Initialize MST with first element
        initial_node = remaining_nodes.pop()
        mst_nodes = set((initial_node,))
        mst_edges = []
        # Initialize the priority queue
        usable_edges = adjacency_matrix[initial_node][:]
        heapq.heapify(usable_edges) # sort-like behavior involving the values of the scores happens here 
        # Initialize the tree weight
        tree_weight = 0.0
        # Carry out the PRIM algorithm: greedily add new nodes
        while usable_edges:
            weight, node1, node2 = heapq.heappop(usable_edges)
            if node2 not in mst_nodes:
                tree_weight += weight
                mst_nodes.add(node2)
                remaining_nodes.remove(node2)
                if node1 < node2:
                    mst_edges.append((node1, node2))
                else:
                    mst_edges.append((node2, node1))
                for edge in adjacency_matrix[node2]:
                    if edge[2] not in mst_nodes:
                        heapq.heappush(usable_edges, edge)
        tree_weight = _convert_weight_fct(tree_weight)
        # Add tree to forest
        forest.append((mst_nodes, mst_edges, tree_weight))
    
    return tuple(forest)


# KRUSKAL ALGORITHM
class _ItemPartition(object):
    """ Class used to represent a simple partition of hashable items.
    
    Arguments:
        items: the iterable of items representing the universe to partition
    
    Attributes:
        items: a collection listing the items representing the universe to partition
        mapping: a "item => set" map that specifies the set to which belong each items according 
            to the partition
    """
    def __init__(self, items):
        self.items = tuple(set(items))
        self.mapping = dict()
        for x in self.items:
            self.mapping[x] = set([x])

    def join(self, x1, x2):
        """ Merges together the respective set associated to each of the two inputs according to the 
        current state of the partition.
        
        Args:
            x1: hashable item, belonging to the universe to partition; represents the first subset to merge
            x2: hashable item, belonging to the universe to partition; represents the second subset to merge
        """
        set_union = self.mapping[x1].union(self.mapping[x2])
        if x1 not in set_union or x2 not in set_union:
            message = "input x1 value {} or input x2 value {} does not belong to the mapping associated to it!"
            message = message.format(x1, x2)
            raise ValueError(message)
        for x in set_union:
            self.mapping[x] = set_union

    def joined(self, x1, x2):
        """ Checks wether or not the two inputs belong to the same set according to the current state 
        of the partition.
        
        Args:
            x1: hashable item, belonging to the universe to partition; represents the first item to test
            x2: hashable item, belonging to the universe to partition; represents the second item to test
        
        Returns:
            boolean
        """
        return self.mapping[x1] == self.mapping[x2]


def kruskal_mst(vertices, weights, spanning_tree_type="max", positive_edges=None, negative_edges=None):
    """ Carries out the Kruskal algorithm for maximum/minimum spanning tree computation.
    
    It allows to compute a MST in the frame of the respect of specific constraints on the edges.
    
    Assumes 
    that:
        - only weights corresponding to (node1, node2) pairs where node1 < node2 are given (undirected graph)
        - the graph is connected. If it is not, prefer using the 'prim_msf' function.
    
    Args:
        vertices: collection of the unique nodes that make up the graph (a strict ordering must be defined on nodes)
        weights: a "(node1,node2) pair => value" map
        spanning_tree_type: string, either 'min' or 'max', the type of spanning tree to produce
        positive_edges: collection of edges which we want for sure in the produced tree (even 
            if an edge also belong to 'negative_egdes', it will still be taken into account), or None. If 
            None, will be considered to be empty.
        negative_edges: collection of edges which we know we do not want in the produced tree, or 
            None. If None, will be considered to be empty.
    
    Returns:
        a (s, e, w) tuple 
        where:
            - 's' is the set of nodes that can be reached with the produced tree
            - 'e' is the collection of edges (a (u,v) node pair) that make up the produced tree
            - 'w' is the total weight resulting of walking along all the edges that make up the mst tree
    """
    # Check input value
    if spanning_tree_type not in POSSIBLE_SPANNING_TREE_TYPES:
        message = "kruskal_mst: incorrect 'spanning_tree_type' input value ('{}'), possible values are: {}."
        message = message.format(spanning_tree_type, POSSIBLE_SPANNING_TREE_TYPES)
        raise ValueError(message)
    if positive_edges is None:
        positive_edges = tuple()
    if negative_edges is None:
        negative_edges = tuple()
    
    # Initialize nodes and edges
    mst_nodes = set(vertices)
    mst_edges = []
    # Create the vertex partition, and join together already the vertices linked by positive edges
    vertex_partition = _ItemPartition(vertices)
    for node1, node2 in positive_edges:
        mst_edges.append((node1,node2))
        vertex_partition.join(node1,node2)
    # Sort edges by weight (remove negative edges)
    set_negative_edges = set(negative_edges)
    sorted_edges = []
    for node1, node2 in weights:
        if (node1,node2) not in set_negative_edges: # FIXME: should not we also try for (node2,node1) not in set_negative_edges?
            weight = weights[(node1,node2)]
            sorted_edges.append((weight,node1,node2))
    if spanning_tree_type == "max":
        sorted_edges.sort(reverse=True)
    else:
        sorted_edges.sort()
    # Greedily add new nodes
    tree_weight = 0.0
    for weight, node1, node2 in sorted_edges:
        if not vertex_partition.joined(node1,node2):
            mst_edges.append((node1,node2))
            vertex_partition.join(node1,node2)
            tree_weight += weight
    return mst_nodes, mst_edges, tree_weight


def kruskal_msf(vertices, weights, spanning_tree_type="max", positive_edges=None, negative_edges=None):
    """ Carries out Kruskal algorithm for maximum/minimum spanning forest computation.
    
    It allows to compute a MSF in the frame of the respect of specific constraints on the edges.
    
    Assumes 
    that:
        - only weights corresponding to (node1, node2) pairs where node1 < node2 are given (undirected graph)
        - the graph is not necessarily connected, an edge does not exist if its weight is not given
    
    Args:
        vertices: collection of the unique nodes that make up the graph (a strict ordering must be defined on nodes)
        weights: a "(node1,node2) pair => value" map
        spanning_tree_type: string, either 'min' or 'max', the type of spanning tree to produce
        positive_edges: collection of edges which we want for sure in one of the produced tree 
            (even if an edge also belong to 'negative_egdes', it will still be taken into account), or None. 
            If None, will be considered to be empty.
        negative_edges: collection of edges which we know we do not want in one of the produced 
            tree, or None. If None, will be considered to be empty.
        
    Returns:
        collection of (s, e, weight) tuples, each tuple corresponding to a disjoint mst subtree, 
        where:
            - 's' is the set of nodes that can be reached with the mst tree
            - 'e' is the collection of edges (a (node1,node2) node pair) that make up the mst tree
            - 'weight' is the total weight resulting of walking along all the edges that make up the mst trees forest
    """
    if spanning_tree_type not in POSSIBLE_SPANNING_TREE_TYPES:
        message = "kruskal_mst: incorrect 'spanning_tree_type' input value ('{}'), possible values are: {}."
        message = message.format(spanning_tree_type, POSSIBLE_SPANNING_TREE_TYPES)
        raise ValueError(message)
    if positive_edges is None:
        positive_edges = tuple()
    if negative_edges is None:
        negative_edges = tuple()
    
    # Initialize edges
    mst_edges = []
    
    # Create the vertex partition, and join together already the vertices linked by positive edges
    vertex_partition = _ItemPartition(vertices)
    for node1, node2 in positive_edges:
        mst_edges.append((node1,node2))
        vertex_partition.join(node1,node2)
    
    # Sort edges by weight (remove negative edges)
    set_negative_edges = set(negative_edges)
    sorted_edges = []
    for node1, node2 in weights:
        if (node1,node2) not in set_negative_edges:
            weight = weights[(node1,node2)]
            sorted_edges.append((weight,node1,node2))
    if spanning_tree_type == "max":
        sorted_edges.sort(reverse=True)
    else:
        sorted_edges.sort()
    # Greedily add new nodes
    for weight, node1, node2 in sorted_edges:
        if not vertex_partition.joined(node1,node2):
            mst_edges.append((node1,node2))
            vertex_partition.join(node1,node2)
    
    # Separate forests
    vertex2head_vertex = dict()
    for node in vertex_partition.items:
        vertex2head_vertex[node] = min(vertex_partition.mapping[node]) # For each node, find the node of the subset where it belongs that has the lowest key / that is the 'head' of the subset
    # Initialize the forest associated to each node
    map_forest = dict()
    for node in vertex2head_vertex.values():
        if node not in map_forest:
            map_forest[node] = (set((node,)), [], 0.0) # (nodes, edges, weight)
    # Merge the forests
    for (node1,node2) in mst_edges:
        nodes, edges, tree_weight = map_forest[vertex2head_vertex[node1]]
        nodes.add(node1)
        nodes.add(node2)
        edges.append((node1,node2))
        tree_weight += weights.get((node1,node2), 0.0) # can be a constraint
        map_forest[vertex2head_vertex[node1]] = (nodes, edges, tree_weight)
    
    forests = tuple(zip(*sorted(map_forest.items(), key=lambda x: x[0])))[1]
    return forests
