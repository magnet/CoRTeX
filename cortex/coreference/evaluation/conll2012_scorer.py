# -*- coding: utf-8 -*-

"""
Defines utilities used to evaluate coreference partition prediction made for documents, using the 
CONLL2012 task's own software
"""

__all__ = ["CONLL2012Scorer", 
           "format_overall_results",
           "format_evaluation_result",
           "format_resolvers_evaluation_results",
           ]

import logging
import os
from collections import defaultdict
import itertools
import numpy
import re

from .utils import _normalize_with_0_standard_value, _uniformize_rows_strings_arrays
from cortex.io.conll2012_writer import CONLL2012MultiDocumentWriter, CONLL2012_MULTIDOCUMENT_EXTENSION
from cortex.tools.wrapper.conll2012_scorer.wrapper import CONLL2012ScorerWrapper
from cortex.utils.io import TemporaryDirectory
from cortex.utils import compute_f1_score

_LOGGER = logging.getLogger(__name__)


class CONLL2012Scorer(object):
    """ A class providing a method to evaluate a collection of ground truth / prediction 
    coreference partition pairs for documents, using the original CONLL2012 task's software.
    """
    
    def evaluate(self, document_ref_partition_predicted_partition_triplets, metric, 
                 skip_singletons=False, retrieve_individual_document_data=True):
        """ Evaluate the closeness of predicted coreference partitions compared to their associated 
        reference coreference partition, both partitions  being defined with respect to a specific 
        document.
        
        The results are returned individually, as well as for the whole input collection.
        
        Args:
            document_ref_partition_predicted_partition_triplets: collection of 
                (document, reference_coreference_partition, predicted_coreference_partition) triplets
            metric: string, the metric desired to score the results, one the 
                following:
                    - "muc": MUCScorer (Vilain et al, 1995)
                    - "bcub": B-Cubed (Bagga and Baldwin, 1998)
                    - "ceafm": CEAF (Luo et al, 2005) using mention-based similarity
                    - "ceafe": CEAF (Luo et al, 2005) using entity-based similarity
                    - "blanc": BLANC
            skip_singletons: boolean, whether or to not to remove the singletons from the 
                CoreferencePartition instances before evaluating
            retrieve_individual_document_data: boolean, whether or not to ask the scorer to compute 
                the individual results of the scorer for each document. Not possible for the 'blanc' metric 
                (unless only one document is input, in which case the overall result is the individual result 
                for the document)

        Returns:
            a ('metric_overall_results', 'document_ident_and_metric_name2results_pairs') pair
        
        For all individual metrics (so, not "all"), except for "blanc", the 'metric_overall_results' 
        is a map that has the following keys:
            - "overall_mention_identification_recall_num"
            - "overall_mention_identification_recall_denom"
            - "overall_mention_identification_recall"
            - "overall_mention_identification_precision_num"
            - "overall_mention_identification_precision_denom"
            - "overall_mention_identification_precision"
            - "overall_mention_identification_f1"
            - "recall_num"
            - "recall_denom"
            - "recall"
            - "precision_num"
            - "precision_denom"
            - "precision"
            - "f1"
        
        For "blanc", it is a map that has the following 
        keys:
            - "overall_mention_identification_recall_num"
            - "overall_mention_identification_recall_denom"
            - "overall_mention_identification_recall"
            - "overall_mention_identification_precision_num"
            - "overall_mention_identification_precision_denom"
            - "overall_mention_identification_precision"
            - "overall_mention_identification_f1"
            - "coreference_recall_num"
            - "coreference_recall_denom"
            - "oreference_recall"
            - "coreference_precision_num"
            - "coreference_precision_denom"
            - "coreference_precision, coreference_f1"
            - "non_coreference_recall_num"
            - "non_coreference_recall_denom"
            - "non_coreference_recall"
            - "non_coreference_precision_num"
            - "non_coreference_precision_denom"
            - "non_coreference_precision"
            - "non_coreference_f1"
            - "blanc_recall_num"
            - "blanc_recall_denom"
            - "blanc_recall"
            - "blanc_precision_num"
            - "blanc_precision_denom"
            - "blanc_precision"
            - "f1"
        
        If 'metric' is 'all', then 'metric_overall_results' is a map that has the following (key, values) 
        pairs:
            - "muc": muc_overall_results 
            - "bcub": bcub_overall_results 
            - "ceafm": expected_ceafm_overall_results
            - "ceafe": expected_ceafe_overall_results
            - "blanc": expected_blanc_overall_results
        
        'document_ident_and_metric_name2results_pairs' is a collection of 
        ('document_ident', 'metric_name2results_name') pairs:
            'metric_name2results_name' is a map that has one or more individual metric name as keys 
            (i.e.: "muc", "b3", "ceafm", "ceafe" , "blanc", "conll), depending on which metric name 
            was input to be used for the evaluation) and has 'results' map as values:
                For all individual metrics, (so, not "all"), except for "blanc", the results map 
                is a map that has the following keys:
                    - "total_key_mentions_nb"
                    - "total_response_mentions_nb"
                    - "strictly_correct_identified_mentions_nb"
                    - "coreference_recall_num"
                    - "coreference_recall_denom"
                    - "coreference_recall"
                    - "coreference_precision_num"
                    - "coreference_precision_denom"
                    - "coreference_precision"
                    - "coreference_f1"
                For "blanc, "it is a map that has the following 
                keys:
                    - "total_key_mentions_nb"
                    - "total_response_mentions_nb"
                    - "strictly_correct_identified_mentions_nb"
        """
        # Process input
        name = None if retrieve_individual_document_data else "none"
        
        # Instantiate tools
        conll_multi_document_writer = CONLL2012MultiDocumentWriter()
        conll2012_scorer_wrapper = CONLL2012ScorerWrapper()
        
        # Helper functions
        doc_index2doc_ident = {}
        def not_fill_assign_fct(i, document):
            pass
        def fill_assign_fct(i, document):
            temp_ident = "{}_{}".format(i, document.ident)
            doc_index2doc_ident[i] = document.ident
            document.ident = temp_ident
        def _create_ref_document_iterable(pred=True, fill_doc_index2doc_ident=False):
            """ Creates a generator over documents contained in the 
            'document_ref_partition_predicted_partition_triplets' collection, while assigning to 
            each document the selected kind of coreference partition.
            
            May also fill the 'doc_index2doc_ident' variable to keep track of each document's 
            original 'ident' attribute value.
            
            Args:
                pred: boolean, whether to set the 'predicted' coreference partition (True) or the 
                    'ground truth' coreference partition (False)
                fill_doc_index2doc_ident: boolean, whether or not the fill the 'doc_index2doc_ident' 
                    variable with each document's original 'ident' attribute value 
            
            Yields:
                Document instance
            """
            assign_fct = fill_assign_fct if fill_doc_index2doc_ident else not_fill_assign_fct
            #assign_fct = lambda i, doc: None
            for i, triplet in enumerate(document_ref_partition_predicted_partition_triplets):
                document = triplet[0]
                assign_fct(i, document)
                coreference_partition = triplet[int(pred)+1]
                document.coreference_partition = coreference_partition
                yield document
        
        # Keep the original coreference partition associated with the input documents
        original_coreference_partitions = tuple(document.coreference_partition\
                                                for document, _, _ in document_ref_partition_predicted_partition_triplets)
        
        # Write both files needed as inputs for the original CONLL2012 task scorer software, and then carry out the evaluation itself
        reference_documents_file_name = "ref_multi_documents.{}".format(CONLL2012_MULTIDOCUMENT_EXTENSION)
        system_documents_file_name = "sys_multi_documents.{}".format(CONLL2012_MULTIDOCUMENT_EXTENSION)
        with TemporaryDirectory() as temp_directory:
            reference_documents_file_path = os.path.join(temp_directory.temporary_folder_path,
                                                         reference_documents_file_name)
            system_documents_file_path = os.path.join(temp_directory.temporary_folder_path,
                                                      system_documents_file_name)
            # Write temporary file for reference coreference partitions
            conll_multi_document_writer.write(_create_ref_document_iterable(pred=False, fill_doc_index2doc_ident=True), 
                                              reference_documents_file_path, 
                                              skip_singleton=skip_singletons, strict=False)
            
            # Write temporary file for predicted coreference partitions
            conll_multi_document_writer.write(_create_ref_document_iterable(pred=True, fill_doc_index2doc_ident=False), 
                                              system_documents_file_path, 
                                              skip_singleton=skip_singletons, strict=False)
            
            # Process the files with the CONLL2012ScorerWrapper
            results = conll2012_scorer_wrapper.evaluate(metric, reference_documents_file_path, 
                                                        system_documents_file_path, name=name)
        
        # Create a map from 'document_temp_ident' to the corresponding 'metric_name2results' map
        metric_overall_results = {metric_name: overall_results for metric_name, (overall_results, _) in results} #results_per_document
        document_ident__2__metric_name2results = defaultdict(lambda: dict())
        unknown_document_nb = 0
        non_matched_ref_document_idents = []
        for metric_name, (_, results_per_document) in results: #overall_results
            for found_doc_conll_idents, document_result in results_per_document:
                first_doc_conll_ident_found, second_doc_conll_ident_found = found_doc_conll_idents
                # If this result is not the result of a scoring of a correct document
                if first_doc_conll_ident_found == "":
                    unknown_document_nb += 1
                    continue
                if second_doc_conll_ident_found == "":
                    document_temp_ident = first_doc_conll_ident_found
                    non_matched_ref_document_idents.append(document_temp_ident)
                    continue
                if first_doc_conll_ident_found != second_doc_conll_ident_found:
                    msg = "First parsed document name ('{}') and second parsed document name ('{}') are different but not empty. What."
                    msg = msg.format(first_doc_conll_ident_found, second_doc_conll_ident_found)
                    raise ValueError(msg)
                
                # Else
                document_temp_ident = first_doc_conll_ident_found
                document_ident__2__metric_name2results[document_temp_ident][metric_name] = document_result
        
        # Organize results in the same order as the one the documents were input
        document_ident_and_metric_name2results_pairs = sorted(document_ident__2__metric_name2results.items(), 
                                                                  key=lambda t: int(re.search("^(\d+)_.*", t[0]).groups()[0])
                                                                  )
        
        # Put original data back into the documents
        document_ident_and_metric_name2results_pairs_ = []
        final_fill_fct = lambda i, document_ident: None
        if document_ident_and_metric_name2results_pairs:
            final_fill_fct = lambda i, document_ident: document_ident_and_metric_name2results_pairs_.append((document_ident, document_ident_and_metric_name2results_pairs[i][1]))
        for i, (original_coreference_partition, (document, _, _)) in enumerate(zip(original_coreference_partitions, 
                                                                                   document_ref_partition_predicted_partition_triplets)
                                                                               ):
            original_ident = doc_index2doc_ident[i]
            document.coreference_partition = original_coreference_partition
            document.ident = original_ident
            final_fill_fct(i, original_ident)
        document_ident_and_metric_name2results_pairs = document_ident_and_metric_name2results_pairs_
        
        # Compute CONLL metric value if possible
        if metric == "all":
            _compute_conll_metric_results(metric_overall_results, document_ident_and_metric_name2results_pairs)
        
        # Log potential warnings
        if unknown_document_nb > 0:
            msg = "{} unknown document(s)'s results parsed".format(unknown_document_nb)
            _LOGGER.warning(msg)
        if len(non_matched_ref_document_idents) > 0:
            msg = "Could not carry out the evaluation for the following document names: {}."
            msg = msg.format(non_matched_ref_document_idents)
            _LOGGER.warning(msg)
        
        return metric_overall_results, document_ident_and_metric_name2results_pairs#document_ident__2__metric_name2results


def _compute_conll_f1_value(metric_name2results, value_kind):
    """ Computes the 'conll' value associated to a specific kind of value (ex: 'f1'), using 
    the input result values associated to different kind of metrics.
    It just consists in computed the average value of this kind of value for the 'muc','b3' and 
    'ceafm' metrics. 
    
    Args:
        metric_name2results: a "metric name => results" map, where 'results' is a 
            "value kind name => value" map
        value_kind: string, specifying the kind of value (ex: 'recall') for which to compute the 
            conll f1 value

    Returns:
        float, the computed conll f1 value
    """
    metric_names = ("muc", "bcub", "ceafm")
    if not all(metric_name in metric_name2results for metric_name in metric_names):
        msg = "Input data must contain values corresponding to at least the 'muc', 'bcub' and 'ceafm' "\
              "metrics (currently available in input data is '{}')."
        msg = msg.format(sorted(metric_name2results.keys()))
        raise ValueError(msg)
    conll_f1_value = sum((metric_name2results[metric_name][value_kind] for metric_name in (metric_names))) / 3.
    return conll_f1_value

def _get_NaN():
    """ Returns numpy.NaN """
    return numpy.NaN

def _compute_conll_metric_results(metric_overall_results, ident_and_metric_name2results_pairs):
    """ Computes the 'conll f1' value from the input result values associated to different kind of 
    metrics, and update the input results holding structures.
    It just consists in computed the average value of the 'f1' (or 'coreference f1') values defined 
    for the 'muc','b3' and 'ceafm' metrics. Do this for the overall results values, as well as for 
    the individual results values.
    
    Args:
        metric_overall_results: a "metric name => overall results" map, where 'overall results' is a 
            "value kind name => value" map
        ident_and_metric_name2results_pairs: a collection of (ident, metric_name2results) pairs, 
            where each 'metric_name2results' value is an "metric name => results" map corresponding 
            to an individual (reference partition, predicted partition) pair, associated to the 'ident' value
    """
    # Overall results
    key = "f1"
    metric_overall_results["conll"] = defaultdict(_get_NaN, {key: _compute_conll_f1_value(metric_overall_results, key)})
    
    # Results per document
    key = "coreference_f1"
    for _, metric_name2results in ident_and_metric_name2results_pairs: #ident
        metric_name2results["conll"] = defaultdict(_get_NaN, {key: _compute_conll_f1_value(metric_name2results, key)})




############################
# Format results functions #
############################
def _compute_detection_rec_prec_values(metric_results):
    """ Computes the recall and precision values corresponding to detection scores, in the input 
    results holding structure.
    
    Args:
        metric_results: a "score key => score value" map

    Returns:
        a (recall, precision) pair
    """
    num = metric_results["strictly_correct_identified_mentions_nb"]
    rec_denom = metric_results["total_key_mentions_nb"]
    prec_denom = metric_results["total_response_mentions_nb"]
    recall = _normalize_with_0_standard_value(num, rec_denom)
    precision = _normalize_with_0_standard_value(num, prec_denom)
    return recall, precision
    
def _get_macro_average_scores(metric_overall_result, ident_and_metric_name2results_pairs, metric_name):
    """ Computes the macro average score 'recall', 'precision', 'f1' values from individual results, 
    for a given metric.
    If the metric name is 'blanc', the values, associated to each score key of the output, will be NaN.
    
    Args:
        metric_results: a "score key => score value" map, containing aggregated / overall values 
            over all individual results. Not used, here only for API compatibility reasons.
        ident_and_metric_name2results_pairs: a collection of (ident, metric_name2results) pairs,
             where each 'metric_name2results' value is an "metric name => results" map corresponding 
             to an individual (reference partition, predicted partition) 
        metric_name: string, name of the metric for which to compute the macro average results

    Returns:
        a "score key => score value" map with the following kets: 'recall', 'precision', 'f1'
    """
    if not ident_and_metric_name2results_pairs:
        recall = precision = f1 = numpy.nan
    else:
        if metric_name == "blanc":
            # Values do not exist with 'blanc', sadly...
            recall = numpy.NaN
            precision = numpy.NaN
            f1 = numpy.NaN
        elif metric_name == "detection":
            recall_precision_iterator = (_compute_detection_rec_prec_values(metric_results)\
                                          for metric_results in (tuple(scores.values())[0]\
                                                                 for _, scores in ident_and_metric_name2results_pairs)
                                         ) #ident
            recalls, precisions = tuple(zip(*recall_precision_iterator))
            recall = 100*numpy.nanmean(recalls)
            precision = 100*numpy.nanmean(precisions)
            f1 = compute_f1_score(recall, precision)
        elif metric_name == "conll":
            f1_values = tuple(scores[metric_name]["coreference_f1"] for _, scores in ident_and_metric_name2results_pairs) #ident
            recall = numpy.NaN
            precision = numpy.NaN
            f1 = numpy.nanmean(f1_values)
        else:
            recalls, precisions = tuple(zip(*((scores[metric_name]["coreference_recall"], scores[metric_name]["coreference_precision"])\
                                              for _, scores in ident_and_metric_name2results_pairs
                                              )
                                            )
                                        ) #ident
            recall = numpy.nanmean(recalls)
            precision = numpy.nanmean(precisions)
            f1 = compute_f1_score(recall, precision)
        
    result = {"recall": recall, 
              "precision": precision,
              "f1": f1,
              }
    return result


def _get_micro_average_scores(metric_overall_result, ident_and_metric_name2results_pairs, metric_name):
    """ Computes the macro average score 'recall', 'precision', 'f1' values from overall results, 
    for a given metric.
    
    Args:
        metric_results: a "score key => score value" map, containing aggregated / overall values 
            over all individual results
        ident_and_metric_name2results_pairs: a collection of (ident, metric_name2results) pairs, 
            where each 'metric_name2results' value is an "metric name => results" map corresponding 
            to an individual (reference partition, predicted partition). Not used, here only for 
            API compatibility reasons.
        metric_name: string, name of the metric for which to compute the macro average results

    Returns:
        a "score key => score value" map with the following kets: 'recall', 'precision', 'f1'
    """
    if metric_name == "blanc":
        result = {"recall": metric_overall_result["blanc_recall"], 
                  "precision": metric_overall_result["blanc_precision"],
                  "f1": metric_overall_result["blanc_f1"],
                  }
    elif metric_name == "conll":
        result = {"recall": numpy.NaN, 
                  "precision": numpy.NaN,
                  "f1": metric_overall_result["f1"],
                  }
    elif metric_name == "detection":
        result = {"recall": metric_overall_result["overall_mention_identification_recall"], 
                  "precision": metric_overall_result["overall_mention_identification_precision"],
                  "f1": metric_overall_result["overall_mention_identification_f1"],
                  }
    else:
        result = {"recall": metric_overall_result["recall"], 
                  "precision": metric_overall_result["precision"],
                  "f1": metric_overall_result["f1"],
                  }
    return result



def _get_average_scores_strings_arrays(metric_overall_result, ident_and_metric_name2results_pairs, 
                                       metric_name, need_percentage=False):
    """ Computes the average 'recall', 'precision' and 'f1' score values corresponding to the pairs 
    of coreference partition already evaluated, and return the strings collections which can be used 
    to build the report describing those scores and the individual score values, by representing 
    them in a table.
    
    Args:
        metric_results: a "score key => score value" map, containing aggregated / overall values 
            over all individual results
        ident_and_metric_name2results_pairs: a collection of (ident, metric_name2results) pairs, 
            where each 'metric_name2results' value is an "metric name => results" map corresponding 
            to an individual (reference partition, predicted partition).
        metric_name: string, name of the metric whose individual and average score values should 
            be described
        need_percentage: boolean; whether or not the score values are percentage, and should be 
            represented as such (True), or not (False)

    Returns:
        a (header_row_strings_array, line_strings_arrays) pair, 
        where:
            - 'header_row_strings_array' is a collection of strings representing the header values of the 
              described table
            - 'line_strings_arrays' s a collection of strings representing the row values of the described 
              table
    """
    coef_ = 100 if need_percentage else 1
    compute_micro_average_scores = _get_micro_average_scores(metric_overall_result, ident_and_metric_name2results_pairs, metric_name)
    compute_macro_average_scores = _get_macro_average_scores(metric_overall_result, ident_and_metric_name2results_pairs, metric_name)
    micro_avg_score_line_ident = "MICRO AVG."
    macro_avg_score_line_ident = "MACRO AVG."
    format_line_strings_fct = lambda legend, scores: (legend, ) + tuple("{:6.2f}".format(round(coef_*scores[n],2)) for n in ("recall", "precision", "f1"))
    header_row_strings_array = (metric_name, "Recall (%)", "Precision (%)", "F1 (%)")
    line_strings_arrays = (format_line_strings_fct(micro_avg_score_line_ident, compute_micro_average_scores), 
                           format_line_strings_fct(macro_avg_score_line_ident, compute_macro_average_scores),
                           )
    return header_row_strings_array, line_strings_arrays

def format_overall_results(metric_overall_results, ident_and_metric_name2results_pairs, metric_name):
    """ Formats into a readable string the overall results, associated to a specific metric, and 
    produced by the CONLL2012Scorer's 'evaluate' method 
        
    Args:
        metric_overall_result: the first output of the CONLL2012Scorer's 'evaluate' method, cf 
            its docstring
        ident_and_metric_name2results_pairs: the second output of the 
            CONLL2012Scorer's 'evaluate' method, cf its docstring
        metric_name: the 'metric_name' parameter value input into the CONLL2012Scorer's 'evaluate' 
            function that produced the input results to format

    Returns:
        a string, the formatted result
    
    Warning:
        If the input 'metric_name' does not reference a metric that was used when computing the evaluation 
        result using the CONLL2012Scorer's 'evaluate' method, an Exception will be raised
    """
    metric_overall_result = metric_overall_results[metric_name]
    header_row_strings_array, line_strings_arrays = _get_average_scores_strings_arrays(metric_overall_result, 
                                                                                       ident_and_metric_name2results_pairs, 
                                                                                       metric_name, 
                                                                                       need_percentage=False)
    row_strings_arrays = [header_row_strings_array,] + list(line_strings_arrays)
    
    row_strings_arrays = _uniformize_rows_strings_arrays(row_strings_arrays)
    row_strings_arrays = tuple("| "+" | ".join(string_array)+" |" for string_array in row_strings_arrays)
    row_length = len(row_strings_arrays[0])
    dash_line = "-"*row_length
    
    strings_array = itertools.chain((dash_line, row_strings_arrays[0], dash_line), row_strings_arrays[1:], (dash_line,))
    
    report_string = "\n".join(strings_array)
    return report_string

def _format_detection_results(metric_overall_results, ident_and_metric_name2results_pairs):
    """ Format into a readable string the overall results, associated to the detection metric, and 
    produced by the CONLL2012Scorer's 'evaluate' method.
    
    Args:
        metric_overall_result: the first output of the CONLL2012Scorer's 'evaluate' method, cf 
            its docstring
        ident_and_metric_name2results_pairs: the second output of the 
            CONLL2012Scorer's 'evaluate' method, cf its docstring

    Returns:
        a string, the formatted result
    """
    one_metric_results = {"detection": tuple(value for key, value in metric_overall_results.items() if key != "conll")[0]}
    return format_overall_results(one_metric_results, ident_and_metric_name2results_pairs, "detection")

def _format_one_document_report(ident, metric_name2results, need_percentage=False):
    """ Formats into a readable report string an individual result, produced by the CONLL2012Scorer's 
    'evaluate' method.
    
    Args:
        ident: string, ident to associate to the output report string, for easy identification
            metric_name2results: a "metric name => results" map corresponding to an individual 
            (reference partition, predicted partition)
        need_percentage: boolean; whether or not the score values to be described are percentage, 
            and should be represented as such (True), or not (False)

    Returns:
        a string, containing a readable description of the results 
    """
    # Def helper functions
    coef_ = 100 if need_percentage else 1
    format_line_strings_fct = lambda metric_name, rec_prec_f1_values: (metric_name, ) + tuple("{:6.2f}".format(round(coef_*v,2)) for v in rec_prec_f1_values)
    keys_ = ("coreference_recall", "coreference_precision", "coreference_f1")
    get_rec_prec_f1_values = lambda metric_name, results: tuple(results[k] for k in keys_) if metric_name != "blanc" else (numpy.nan, numpy.nan, numpy.nan)
    
    # Detection scores
    detection_recall, detection_precision = _compute_detection_rec_prec_values(tuple(metric_name2results.values())[0])
    detection_recall = 100*detection_recall
    detection_precision = 100*detection_precision
    detection_f1 = compute_f1_score(detection_recall, detection_precision)
    detection_scores_line = format_line_strings_fct("detection", (detection_recall, detection_precision, detection_f1))
    
    # Metric + detection scores
    metric_scores_line_iterator = (format_line_strings_fct(metric_name, get_rec_prec_f1_values(metric_name, results))\
                                    for metric_name, results in sorted(metric_name2results.items(), key=lambda t: _METRIC_NAME2SORT_INDEX[t[0]])
                                   )
    
    # Create the arrays of strings
    header_row_strings_array = (ident, "Recall (%)", "Precision (%)", "F1 (%)")
    row_strings_arrays = itertools.chain((header_row_strings_array,), metric_scores_line_iterator, (detection_scores_line,))
    row_strings_arrays = _uniformize_rows_strings_arrays(row_strings_arrays)
    row_strings_arrays = tuple("| "+" | ".join(string_array)+" |" for string_array in row_strings_arrays)
    row_length = len(row_strings_arrays[0])
    dash_line = "-"*row_length
    if "conll" in metric_name2results:
        strings_array = itertools.chain((dash_line, row_strings_arrays[0], dash_line), row_strings_arrays[1:-2], (dash_line,), row_strings_arrays[-2:-1], (dash_line,), row_strings_arrays[-1:], (dash_line,))
    else:
        strings_array = itertools.chain((dash_line, row_strings_arrays[0], dash_line), row_strings_arrays[1:-1], (dash_line,), row_strings_arrays[-1:], (dash_line,))
    
    # Create the report
    report_string = "\n".join(strings_array)
    
    return report_string


_METRIC_NAME2SORT_INDEX = {"muc": 0, "bcub": 1, "ceafm": 2, "ceafe": 3, "blanc": 4, "conll": 5}
def format_evaluation_result(metric_overall_results, ident_and_metric_name2results_pairs, 
                             detailed=False):
    """ Formats the output of the CONLL2012Scorer's 'evaluate' method into a readable string
    
    Args:
        metric_overall_result: the first output of the CONLL2012Scorer's 'evaluate' method, cf 
            its docstring
        ident_and_metric_name2results_pairs: the second output of the 
            CONLL2012Scorer's 'evaluate' method, cf its docstring
        detailed: boolean, whether or not to add individual document score info in the produced 
            formatted result string

    Returns:
        a pair of (avg_report, document_reports) strings; the first pertains to the overall 
        results, whereas the second pertains to the individual documents results (it will be empty if 
        'detailed' was set to False)
    """
    if not metric_overall_results:
        raise ValueError("Empty results, nothing to display.")
    strings = [format_overall_results(metric_overall_results, ident_and_metric_name2results_pairs, metric_name)\
               for metric_name, _ in sorted(metric_overall_results.items(), key=lambda t: _METRIC_NAME2SORT_INDEX[t[0]])]\
                + [_format_detection_results(metric_overall_results, ident_and_metric_name2results_pairs),]
    avg_report = "\n".join(strings)
    
    document_reports = None
    if detailed:
        document_reports = tuple(_format_one_document_report(ident, metric_name2results, need_percentage=False)\
                                 for ident, metric_name2results in ident_and_metric_name2results_pairs
                                 )
    
    return avg_report, document_reports


def format_resolvers_evaluation_results(resolver_name2evaluation_result, sort_conll=False):
    """ Creates a string describing the evaluation overall results obtained for several resolvers, 
    notably so as to be able for a human to easily compare the resolvers' individual performance 
    levels.
    
    Args:
        resolver_name2evaluation_result: a "resolver ident => evaluation_result" map, where 
            'evaluation_result' is the output of a call to the CONLL2012Scorer's 'evaluate' method
        sort_conll: boolean, or callable on a (metric_overall_result, ident_and_metric_name2results_pairs) 
            pair:
                - if True; sort the resolver in order of decreasing 'CONLL score' value (assume that the evaluation 
                  results in the input map contain enough data to compute that score)
                - if False; sort the resolver in order of increasing 'resolver ident' value
                - if it is a callable; assume that this is a function suitable for being passed as the 'key' 
                  parameter of the 'sorted' function

    Returns:
        a string, representing a table displaying the evaluation results associated to each resolver
    """
    #               || metric_name1    || metric_name2    || CONLL f1 ||
    # resolver_name || nan | nan | nan || nan | nan | nan || nan      ||
    def _extract_data(metric_overall_results, metric_names):
        metric_name_results_tuple_pairs = []
        for metric_name in metric_names:
            results = metric_overall_results[metric_name]
            if metric_name == "blanc":
                recall = results["blanc_recall"]
                precision = results["blanc_precision"]
                f1 = results["blanc_f1"]
            elif metric_name == "conll":
                recall = numpy.NaN
                precision = numpy.NaN
                f1 = results["f1"]
            else:
                recall = results["recall"]
                precision = results["precision"]
                f1 = results["f1"]
            results_tuple = (recall, precision, f1)
            metric_name_results_tuple_pairs.append((metric_name, results_tuple))
        return metric_name_results_tuple_pairs
    
    def _format_line(resolver_name, metric_name_results_tuple_pairs, resolver_name_column_max_size):
        resolver_name_column_string = (" {:"+str(resolver_name_column_max_size-2)+"s} ").format(resolver_name)
        metric_results_column_strings = tuple(" {:6.2f}% | {:6.2f}% | {:6.2f}% ".format(*results_tuple)
                                              for _, results_tuple in metric_name_results_tuple_pairs #metric_name
                                              )
        metric_results_column_string = "||".join(metric_results_column_strings)
        return "||{}||{}||".format(resolver_name_column_string, metric_results_column_string)
    
    def _format_header_line(metric_names, resolver_name_column_max_size):
        return "||{}||{}||".format(#(" {:"+str(resolver_name_column_max_size-2)+"s} ").format("resolver ident"), 
                                   " "*resolver_name_column_max_size, 
                                   "||".join(" {:^27s} ".format(metric_name) for metric_name in metric_names)
                                   ) #29-2
    
    def _format_subheader_line(metric_names, resolver_name_column_max_size):
        column_string = " {:^7s} | {:^7s} | {:^7s} ".format("recall", "prec.", "f1")
        return "||{}||{}||".format(#(" {:"+str(resolver_name_column_max_size-2)+"s} ").format("resolver ident"), 
                                   " "*resolver_name_column_max_size, 
                                   "||".join(column_string for _ in metric_names)
                                   ) #29-2
    
    resolver_name_column_max_size = max((len(" {:s} ".format(resolver_name)) for resolver_name in resolver_name2evaluation_result.keys()), default=0)
    resolver_name_column_max_size = max(resolver_name_column_max_size, len(" resolver ident "))
    
    sort_key = lambda x: x[0]
    if not isinstance(sort_conll, bool):
        sort_key = sort_conll
    elif sort_conll:
        sort_key = lambda x: -x[1][0]["conll"]["f1"]
    
    resolver_name_evaluation_results_pairs = sorted(resolver_name2evaluation_result.items(), key=sort_key)
    _, (a_metric_overall_results, _) = resolver_name_evaluation_results_pairs[0]
    metric_names = sorted(a_metric_overall_results.keys(), key=lambda x: _METRIC_NAME2SORT_INDEX[x])
    strings_array = []
    header_line = _format_header_line(metric_names, resolver_name_column_max_size)
    subheader_line = _format_subheader_line(metric_names, resolver_name_column_max_size)
    dash_line = "-"*len(header_line)
    strings_array.append(dash_line)
    strings_array.append(header_line)
    strings_array.append(dash_line)
    strings_array.append(subheader_line)
    strings_array.append(dash_line)
    strings_array.append(dash_line)
    for resolver_name, (metric_overall_results, _) in resolver_name_evaluation_results_pairs: #ident_and_metric_name2results_pairs
        metric_name_results_tuple_pairs = _extract_data(metric_overall_results, metric_names)
        line = _format_line(resolver_name, metric_name_results_tuple_pairs, resolver_name_column_max_size)
        strings_array.append(line)
        strings_array.append(dash_line)
    
    report = "\n".join(strings_array)
    return report
