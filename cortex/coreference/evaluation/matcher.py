# -*- coding: utf-8 -*-

"""
Defines a class used to align the respective universes of two coreference partition
"""

__all__ = ["Matcher",
           ]


from cortex.api.coreference_partition import CoreferencePartition


class Matcher(object):
    """ This class is used to create 'extended copies' of two CoreferencePartition instances, so 
    that those two 'copies' share the same universe.
    
    The coreference links defined in each partition are retained in the coreference partition's 
    respective 'extended copy'.
    
    This operation will referred as 'to match'.
    """
    
    @classmethod
    def match(cls, ref_partition, sys_partition):
        """ Matches two coreference partition.
        
        Args:
            ref_partition: CoreferencePartition instance
            sys_partition: CoreferencePartition instance

        Returns:
            a (new_ref_paritition, new_sys_partition) pair, where the elements of the pair are 
            CoreferencePartition instances
        """
        # Build new ref_partition / sys_partition
        common_universe = ref_partition.get_universe().union(sys_partition.get_universe())
        
        # Create new sys_partition (- sys_partition singletons + non detected keys)
        new_sys_partition = CoreferencePartition(mention_extents=common_universe)
        cls._update_new_partition_with_previous_partition_info(new_sys_partition, sys_partition)
        
        # Create new ref_partition (+ detected non-singletons not in ref_partition)
        new_ref_partition = CoreferencePartition(mention_extents=common_universe)
        cls._update_new_partition_with_previous_partition_info(new_ref_partition, ref_partition)
        
        return new_ref_partition, new_sys_partition
    
    @staticmethod
    def _update_new_partition_with_previous_partition_info(new_partition, previous_partition):
        """ Creates entities in the new coreference partition that correspond the entities defined in 
        the previous coreference partition, assuming the universe the previous partition is a subset 
        of the universe of the new partition.
        
        Args:
            new_partition: CoreferencePartition instance
            previous_partition: CoreferencePartition instance
        """
        for entity in previous_partition:
            if len(entity) > 1:
                head_mention_extent = entity.head_mention_extent
                other_mention_extents = tuple(e for e in entity if e != head_mention_extent)
                for mention_extent in other_mention_extents:
                    new_partition.join(head_mention_extent, mention_extent)
