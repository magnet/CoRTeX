# -*- coding: utf-8 -*-

"""
Defines scorer classes implementing different kinds of coreference partition prediction evaluation 
process
"""

__all__ = ["DetectionScorer",
           "MucScorer", 
           "B3Scorer", 
           "CEAFScorer", 
           "CEAFEntityScorer", 
           "BLANCScorer", 
           "AllScorer",
           "MacroAverageScore",
           "MicroAverageScore",
           "_CoreferenceScorer",
           ]

import logging
import numpy
import abc
import operator
from collections import namedtuple

from .utils import _uniformize_rows_strings_arrays, _normalize_with_0_standard_value
from cortex.utils import EqualityMixIn, compute_f1_score, normalize
from cortex.api.entity import Entity
from cortex.tools.munkres import MUNKRES

_LOGGER = logging.getLogger("_CoreferenceScorer")

_COMMON_SCORE_ATTRIBUTE_NAMES = ("r", "p", "f")

_macro_average_score_attribute_names = _COMMON_SCORE_ATTRIBUTE_NAMES
MacroAverageScore = namedtuple('macro_average_score', _macro_average_score_attribute_names)
_micro_average_score_attribute_names = _COMMON_SCORE_ATTRIBUTE_NAMES + ("r_num", "r_den", "p_num", "p_den")
MicroAverageScore = namedtuple('micro_average_score', _micro_average_score_attribute_names)



class _CoreferenceScorer(EqualityMixIn, metaclass=abc.ABCMeta):
    """ Abstract class for Coreference Scorer 
    
    Such a scorer keeps track of the (ident, ground truth coreference partition, predicted 
    coreference partition) it has evaluated, as well as the scores obtained for them. It can be used 
    to iteratively evaluate over a collection of (ground truth coreference partition, predicted 
    coreference partition) pairs.
    
    It is then able to compute on the fly and display the individual as well as the overall results 
    (either 'micro average' or 'macro average').
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        ident2score: a 'ident => score' map containing the evaluation data of the pairs of 
            coreference partition already evaluated
    """
    
    _POSSIBLE_AVG_SCORE_TYPES = set(("micro", "macro"))
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = ("_scores", "_accum_scores",)
    
    def __init__(self):
        # Assign attribute values
        self._scores = []
        self._accum_scores = {}
    
    @property
    def NAME(self):
        return type(self).__name__
    @NAME.setter
    def NAME(self, value):
        raise AttributeError
    
    @property
    def ident2score(self):
        """ Returns a 'ident => score' map containing the evaluation data of the pairs of 
        coreference partition already evaluated

        Returns:
            a dict
        """
        return dict(self._scores)
    @ident2score.setter
    def ident2score(self, value):
        raise AttributeError
    
    def update(self, ident, ground_truth_coreference_partition, predicted_coreference_partition):
        """ Computes the score value associated to the pair or input coreference partitions, and 
        register it as associated to the specified ident.
        
        Args:
            ident: string, the string to be associated to the computed score, and which shall be 
                used to reference it when the formatted scores are displayed
            ground_truth_coreference_partition: a CoreferencePartition instance
            predicted_coreference_partition: a CoreferencePartition instance
        """
        return self._update(ident, ground_truth_coreference_partition, predicted_coreference_partition)
    
    def compute_micro_average_scores(self):
        """ Computes the micro average scores corresponding to the pairs of coreference partition 
        already evaluated.
        
        Returns:
            a MicroAverageScore instance
        """
        return self._micro_average_scores()
    
    # Instance methods
    def compute_macro_average_scores(self):
        """ Computes the macro average scores corresponding to the pairs of coreference partition 
        already evaluated.

        Returns:
            a MacroAverageScore instance
        """
        recalls, precisions = tuple(zip(*((scores.r, scores.p) for _, scores in self._scores))) #ident
        recall = numpy.nanmean(recalls)
        precision = numpy.nanmean(precisions)
        f1 = compute_f1_score(recall, precision)
        return MacroAverageScore(recall, precision, f1)
    
    def compute_average_scores(self, avg_scores_type="micro"):
        """ Computes the average scores corresponding to the pairs of coreference partition already 
        evaluated.
        
        Args:
            avg_scores_type: string, either 'micro' or 'macro'; the type of average score to compute

        Returns:
            a MacroAverageScore instance or a MicroAverageScore instance, depending on the value 
            of the 'avg_scores_type' parameter
        """
        get_average_scores_fct = self._get_compute_average_scores_fct(avg_scores_type)
        return get_average_scores_fct()
    
    def get_scores_report(self, avg_scores_type="micro"):
        """ Computes the average score values corresponding to the pairs of coreference partition 
        already evaluated, and return a formatted string describing averaged score values and the 
        individual score values.
        
        Args:
            avg_scores_type: string, either 'micro' or 'macro'; the type of average score to 
                compute and format

        Returns:
            a string
        """
        strings_array = self._get_report_strings_array(avg_scores_type=avg_scores_type)
        report_string = "\n".join(strings_array)
        return report_string
    
    def print_scores(self, avg_scores_type="micro"):
        """ Computes the scores and average scores corresponding to the pairs of coreference partition 
        already evaluated, and then a formatted string representing those scores.
        
        Print it and then return it.
        
        Args:
            avg_scores_type: string, either 'micro' or 'macro'; the type of average score to 
                compute and format

        Returns:
            a string
        """
        report_string = self.get_scores_report(avg_scores_type=avg_scores_type)
        print(report_string)
        return report_string
    
    def print_average_scores(self, avg_scores_type="micro"):
        """ Computes the average score valuess corresponding to the pairs of coreference partition 
        already evaluated, and then a formatted string representing those scores.
        
        Print it and then return it.
        
        Args:
            avg_scores_type: string, either 'micro' or 'macro'; the type of average score to 
                compute and format

        Returns:
            a string
        """
        header_row_strings_array, line_strings_array = self._get_average_scores_strings_arrays(avg_scores_type=avg_scores_type)
        row_strings_arrays = [header_row_strings_array, line_strings_array]
        
        row_strings_arrays = _uniformize_rows_strings_arrays(row_strings_arrays)
        row_strings_arrays = tuple("| "+" | ".join(string_array)+" |" for string_array in row_strings_arrays)
        row_length = len(row_strings_arrays[0])
        dash_line = "-"*row_length
        
        strings_array = (dash_line, row_strings_arrays[0], dash_line, row_strings_arrays[1], dash_line)
        
        report_string = "\n".join(strings_array)
        print(report_string)
        return report_string
    
    def _get_compute_average_scores_fct(self, avg_scores_type):
        """ Returns the average score value computation function associated to the input type of score 
        averaging.
        
        Args:
            avg_scores_type: string, either 'micro' or 'macro'; the type of average score to 
                compute and format

        Returns:
            callable
        """
        if avg_scores_type not in self._POSSIBLE_AVG_SCORE_TYPES:
            message = "Incorrect 'avg_scores_type' value '{}', possible values are: {}.".format(avg_scores_type, self._POSSIBLE_AVG_SCORE_TYPES)
            raise ValueError(message)
        return getattr(self, "compute_{}_average_scores".format(avg_scores_type))
    
    def _get_report_strings_array(self, avg_scores_type="micro"):
        """ Computes the average score values corresponding to the pairs of coreference partition 
        already evaluated, and return the strings collection which can be used to build the report 
        describing those averaged score values and the individual score values.
        
        Args:
            avg_scores_type: string, either 'micro' or 'macro'; the type of average score to 
                compute and format

        Returns:
            collection of strings
        """
        # Get the function now in order to fail right now if the argument is wrong
        get_average_scores_fct = self._get_compute_average_scores_fct(avg_scores_type)
        
        header_row_strings = ("Document", "Recall (%)", "Precision (%)", "F1 (%)")
        
        format_line_strings_fct = lambda legend, scores: (legend, ) + tuple("{:6.2f}".format(round(100*getattr(scores, n),2)) for n in _COMMON_SCORE_ATTRIBUTE_NAMES)
        
        # Individual scores
        row_strings_arrays = [header_row_strings]
        for ident, scores in sorted(self._scores, key=operator.itemgetter(0)):
            row_strings_arrays.append(format_line_strings_fct(ident, scores))
        
        # Average scores
        global_scores = get_average_scores_fct()
        avg_score_line_ident = "{} AVG. ({})".format(avg_scores_type.upper(), self.NAME)
        row_strings_arrays.append(format_line_strings_fct(avg_score_line_ident, global_scores))
        
        row_strings_arrays = _uniformize_rows_strings_arrays(row_strings_arrays)
        row_strings_arrays = tuple("| "+" | ".join(string_array)+" |" for string_array in row_strings_arrays)
        
        row_length = len(row_strings_arrays[0])
        name_row_format = "| {:<"+str(row_length-4)+"s} |"
        name_row = name_row_format.format(self.NAME)
        
        dash_line = "-"*row_length
        
        strings_array = (dash_line, name_row, dash_line, row_strings_arrays[0], dash_line) + row_strings_arrays[1:-1] + (dash_line, row_strings_arrays[-1], dash_line)
        
        return strings_array
    
    def _get_average_scores_strings_arrays(self, avg_scores_type="micro"):
        """ Computes the average score values corresponding to the pairs of coreference partition 
        already evaluated, and return the strings collection which can be used to build the report 
        describing those scores and the individual score values.
        
        Args:
            avg_scores_type: string, either 'micro' or 'macro'; the type of average score to 
                compute and format

        Returns:
            a collection of strings
        """
        average_scores = self.compute_average_scores(avg_scores_type)
        avg_score_line_ident = "{} AVG.".format(avg_scores_type.upper())
        format_line_strings_fct = lambda legend, scores: (legend, ) + tuple("{:6.2f}".format(round(100*getattr(scores, n),2)) for n in _COMMON_SCORE_ATTRIBUTE_NAMES)
        header_row_strings_array = (self.NAME, "Recall (%)", "Precision (%)", "F1 (%)")
        line_strings_array = format_line_strings_fct(avg_score_line_ident, average_scores)
        return header_row_strings_array, line_strings_array
    
    @abc.abstractmethod
    def _update(self, ident, key, system):
        """ Computes the score value associated to the pair or input coreference partitions, and 
        register it as associated to the specified ident.
        
        Args:
            ident: string, the string to be associated to the computed score, and which shall be 
                used to reference it when the formatted scores are displayed
            ground_truth_coreference_partition: a CoreferencePartition instance
            predicted_coreference_partition: a CoreferencePartition instance
        """
        pass
    
    @abc.abstractmethod
    def _micro_average_scores(self):
        """ Computes the micro average scores corresponding to the pairs of coreference partition 
        already evaluated.
        
        Returns:
            a MicroAverageScore instance
        """
        pass



class DetectionScorer(_CoreferenceScorer):
    """ Computes a recall and a precision scores from the comparison of two coreference partitions' 
    respective universe 
    
    Its computed score values are encapsulated into a '_DetectionScore' named tuple class.
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        ident2score: a 'ident => score' map containing the evaluation data of the pairs of 
            coreference partition already evaluated
    """
    
    _detection_specific_attribute_names = ("common_mentions_nb", "ref_mentions_nb", "sys_mentions_nb")
    _detection_score_attribute_name = _COMMON_SCORE_ATTRIBUTE_NAMES + _detection_specific_attribute_names
    _DetectionScore = namedtuple('muc_score', _detection_score_attribute_name)
    
    def _update(self, ident, ref_partition, sys_partition):
        """ Computes the score value associated to the pair or input coreference partitions, and 
        register it as associated to the specified ident.
        
        Args:
            ident: string, the string to be associated to the computed score, and which shall be 
                used to reference it when the formatted scores are displayed
            ground_truth_coreference_partition: a CoreferencePartition instance
            predicted_coreference_partition: a CoreferencePartition instance
        """
        scores = self._compute_scores(ref_partition, sys_partition)
        self._scores.append((ident, scores))
        
        for attribute_name in self._detection_specific_attribute_names:
            self._accum_scores[attribute_name] = self._accum_scores.get(attribute_name, 0) + getattr(scores, attribute_name)
    
    def _micro_average_scores(self):
        """ Compute the micro average scores corresponding to the pairs of coreference partition 
        already evaluated.
        
        Returns:
            a MicroAverageScore instance
        """
        # Averaging on mention counts
        r_num = p_num = self._accum_scores["common_mentions_nb"]
        r_den, p_den = self._accum_scores["ref_mentions_nb"], self._accum_scores["sys_mentions_nb"]
        recall = _normalize_with_0_standard_value(r_num, r_den)
        precision = _normalize_with_0_standard_value(p_num, p_den)
        f1 = compute_f1_score(recall, precision)
        return MicroAverageScore(recall, precision, f1, r_num, r_den, p_num, p_den)
    
    @classmethod
    def _compute_scores(cls, ref_partition, sys_partition):
        """ Compute the score value associated to the pair or input coreference partitions.
        
        Args:
            ground_truth_coreference_partition: a CoreferencePartition instance
            predicted_coreference_partition: a CoreferencePartition instance

        Returns:
            a cls._DetectionScore named tuple
        """
        ref_partition_universe = ref_partition.get_universe()
        sys_partition_universe = sys_partition.get_universe()
        ref_mentions_nb = len(ref_partition_universe) # Number of mentions in the 'ref_partition' CoreferencePartition
        sys_mentions_nb = len(sys_partition_universe) # Number of mentions in the 'sys_partition' CoreferencePartition
        common_mentions_nb = len(sys_partition_universe.intersection(ref_partition_universe)) # Number of mentions in the 'sys_partition' CoreferencePartition whose extent correspond to that of a mention of the 'ref_partition' CorefPartition
        recall = _normalize_with_0_standard_value(common_mentions_nb, ref_mentions_nb)
        precision = _normalize_with_0_standard_value(common_mentions_nb, sys_mentions_nb)
        f1 = compute_f1_score(recall, precision)
        return cls._DetectionScore(recall, precision, f1, common_mentions_nb, ref_mentions_nb, sys_mentions_nb)




class MucScorer(_CoreferenceScorer):
    """ Vilain et al. (2005) MUC metric

             # of common links to KEY and RESPONSE
    R(/P) =  -------------------------------------
                # of links in KEY (/RESPONSE)

             sum(correct(S)-missing(S))    sum(|S|-p(S))
    P/R =    -------------------------- =  -------------
                   sum(correct(S))          sum(|S|-1)
    
    
    Its computed score values are encapsulated into a '_MucScore' named tuple class.
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        ident2score: a 'ident => score' map containing the evaluation data of the pairs of 
            coreference partition already evaluated
    """
    
    _muc_specific_attribute_names = ("c", "t", "s")
    _muc_score_attribute_name = _COMMON_SCORE_ATTRIBUTE_NAMES + _muc_specific_attribute_names
    _MucScore = namedtuple('muc_score', _muc_score_attribute_name)
    
    def _update(self, ident, ref_partition, sys_partition):
        """ Computes the score value associated to the pair or input coreference partitions, and 
        register it as associated to the specified ident.
        
        Args:
            ident: string, the string to be associated to the computed score, and which shall be 
                used to reference it when the formatted scores are displayed
            ground_truth_coreference_partition: a CoreferencePartition instance
            predicted_coreference_partition: a CoreferencePartition instance
        """
        scores = self._compute_scores(ref_partition, sys_partition)
        self._scores.append((ident, scores))
        
        for attribute_name in self._muc_specific_attribute_names:
            self._accum_scores[attribute_name] = self._accum_scores.get(attribute_name, 0) + getattr(scores, attribute_name)

    def _micro_average_scores(self):
        """ Computes the micro average scores corresponding to the pairs of coreference partition 
        already evaluated.
        
        Returns:
            a MicroAverageScore instance
        """
        # Averaging on mention counts
        r_num = p_num = self._accum_scores["c"]
        r_den, p_den = self._accum_scores["t"], self._accum_scores["s"]
        recall = _normalize_with_0_standard_value(r_num, r_den)
        precision = _normalize_with_0_standard_value(p_num, p_den)
        f1 = compute_f1_score(recall, precision)
        return MicroAverageScore(recall, precision, f1, r_num, r_den, p_num, p_den)
    
    @classmethod
    def _compute_scores(cls, ref_partition, sys_partition):
        """ Computes the score value associated to the pair or input coreference partitions.
        
        Args:
            ground_truth_coreference_partition: a CoreferencePartition instance
            predicted_coreference_partition: a CoreferencePartition instance

        Returns:
            cls._MucScore named tuple
        """
        # Compute mentions nb
        correct_mentions_nb, ref_mentions_nb = cls._vilain_compare(ref_partition, sys_partition)
        _, sys_mentions_nb = cls._vilain_compare(sys_partition, ref_partition)
        # Compute scores
        recall = _normalize_with_0_standard_value(correct_mentions_nb, ref_mentions_nb)
        precision = _normalize_with_0_standard_value(correct_mentions_nb, sys_mentions_nb)
        f1 = compute_f1_score(recall,precision)
        return cls._MucScore(recall, precision, f1, correct_mentions_nb, ref_mentions_nb, sys_mentions_nb)
    
    @staticmethod
    def _vilain_compare(reference_partition, compared_partition):
        """ Computes numbers of common mentions and reference mentions following the algorithm 
        detailed in Vilain et al. (2005).
        
        Args:
            reference_partition: CoreferencePartition instance, the partition from which the 
                number of reference mentions will be computed
            compared_partition: CoreferencePartition instance

        Returns:
            a (common_mentions_nb, reference_partition_mentions_nb) pair
        """
        common_mentions_nb = 0
        reference_partition_mentions_nb = 0
        for reference_partition_entity in reference_partition:
            for entity in compared_partition:
                current_common_mentions_nb = len(reference_partition_entity & entity)
                if current_common_mentions_nb > 0:
                    common_mentions_nb += current_common_mentions_nb - 1
            reference_partition_mentions_nb += len(reference_partition_entity) - 1 # Assume that an Entity is not empty, as it should
        return common_mentions_nb, reference_partition_mentions_nb




class B3Scorer(_CoreferenceScorer):
    """ B-CUBED Scorer as described by Bagga & Baldwin (1998)

                # correct elements in OUTPUT CHAIN containing entity_i
    prec_i =    - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                    # elements in OUTPUT CHAIN containing entity_i
    
                # correct elements in OUTPUT CHAIN containing entity_i
    rec_i =     - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                    # elements in TRUTH CHAIN containing entity_i
    
    prec = sum_i-n(w_i*prec_i)
    rec = sum_i-n(w_i*rec_i)
    
    uniform weight: w = 1/n
    
    
    Its computed score values are encapsulated into a '_B3Score' named tuple class.
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        ident2score: a 'ident => score' map containing the evaluation data of the pairs of 
            coreference partition already evaluated
    """
    
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = ("_mention_count",) + _CoreferenceScorer._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    
    _b3_specific_attribute_names = ("ct",)
    _b3_score_attribute_name = _COMMON_SCORE_ATTRIBUTE_NAMES + _b3_specific_attribute_names
    _B3Score = namedtuple('b3_score', _b3_score_attribute_name)
    
    def __init__(self):
        super().__init__()
        self._mention_count = 0 # For normalization

    def _update(self, ident, ref_partition, sys_partition):
        """ Computes the score value associated to the pair or input coreference partitions, and 
        register it as associated to the specified ident.
        
        Args:
            ident: string, the string to be associated to the computed score, and which shall be 
                used to reference it when the formatted scores are displayed
            ground_truth_coreference_partition: a CoreferencePartition instance
            predicted_coreference_partition: a CoreferencePartition instance
        """
        scores = self._compute_scores(ref_partition, sys_partition)
        self._scores.append((ident, scores))
        
        mention_ct = scores.ct
        for attribute_name in _COMMON_SCORE_ATTRIBUTE_NAMES:
            # Weight scores by mention count
            score_contribution = getattr(scores, attribute_name) * mention_ct if mention_ct else 0
            self._accum_scores[attribute_name] = self._accum_scores.get(attribute_name, 0) + score_contribution
        self._mention_count += mention_ct

    def _micro_average_scores(self):
        """ Computes the micro average scores corresponding to the pairs of coreference partition 
        already evaluated.
        

        Returns:
            a MicroAverageScore instance
        """
        # Averaging on number of mentions
        accum_scores = self._accum_scores
        r_den = p_den = self._mention_count
        r_num = accum_scores["r"]
        p_num = accum_scores["p"]
        recall = r_num / r_den
        precision = p_num / p_den
        f1 = compute_f1_score(recall, precision)
        return MicroAverageScore(recall, precision, f1, r_num, r_den, p_num, p_den)
    
    @classmethod
    def _compute_scores(cls, ref_partition, sys_partition):  # TODO: check with imperfect mention detection!
        """ Computes the score value associated to the pair or input coreference partitions.
        
        Args:
            ground_truth_coreference_partition: a CoreferencePartition instance
            predicted_coreference_partition: a CoreferencePartition instance

        Returns:
            a cls._B3Score named tuple
        """
        # Build mention to entity maps, using mention offsets for comparison
        mention_extent2ref_entity = ref_partition.get_mention_extent_to_entity_map()
        mention_extent2sys_entity = sys_partition.get_mention_extent_to_entity_map()
        # FIXME: what to do about this legacy code?
        #if ref_partition != sys_partition:
        #    diff1 = ref_partition - sys_partition
        #    diff2 = sys_partition - ref_partition
        #    message = "B3Scorer warning: different mention sets in key and sys:\n{}:\n{}\n{}:\n{}".format("Ref mentions not in sys", diff1, "Sys mentions not in ref", diff2)
        #    _LOGGER.warning(message)
        # Mention counts
        ref_mention_nb = len(mention_extent2ref_entity)
        # Compute_scores sys and ref chain for each mention
        recall_per_entity = []
        precision_per_entity = []
        for sys_mention_extent, sys_entity in mention_extent2sys_entity.items():
            if sys_mention_extent not in mention_extent2ref_entity and len(sys_entity) == 1:
                continue # Do not count twinless singleton mention
            ref_entity = mention_extent2ref_entity.get(sys_mention_extent, Entity()) # mention not in the key
            correct = len(sys_entity.intersection(ref_entity))
            entity_recall = _normalize_with_0_standard_value(correct, len(ref_entity))
            entity_precision = _normalize_with_0_standard_value(correct, len(sys_entity))
            recall_per_entity.append(entity_recall)
            precision_per_entity.append(entity_precision)
        recall = normalize(sum(recall_per_entity, 0.0), ref_mention_nb)
        precision = normalize(sum(precision_per_entity, 0.0), ref_mention_nb)
        f1 = compute_f1_score(recall, precision)
        return cls._B3Score(recall, precision, f1, ref_mention_nb)




class CEAFScorer(_CoreferenceScorer):
    """ CEAF scorer from Luo (2005)
    
    Its computed score values are encapsulated into a '_CEAFScore' named tuple class.
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        ident2score: a 'ident => score' map containing the evaluation data of the pairs of 
            coreference partition already evaluated
    """
    
    _ceaf_specific_attribute_names = ("c", "t", "s")
    _ceaf_score_attribute_name = _COMMON_SCORE_ATTRIBUTE_NAMES + _ceaf_specific_attribute_names
    _CEAFScore = namedtuple('ceaf_score', _ceaf_score_attribute_name)
    
    def _update(self, ident, ref_partition, sys_partition):
        """ Computes the score value associated to the pair or input coreference partitions, and 
        register it as associated to the specified ident.
        
        Args:
            ident: string, the string to be associated to the computed score, and which shall be 
                used to reference it when the formatted scores are displayed
            ground_truth_coreference_partition: a CoreferencePartition instance
            predicted_coreference_partition: a CoreferencePartition instance
        """
        scores = self._compute_scores(ref_partition, sys_partition)
        self._scores.append((ident, scores))
        
        for attribute_name in self._ceaf_specific_attribute_names:
            self._accum_scores[attribute_name] = self._accum_scores.get(attribute_name, 0) + getattr(scores, attribute_name)

    def _micro_average_scores(self):
        """ Computes the micro average scores corresponding to the pairs of coreference partition 
        already evaluated.
        

        Returns:
            a MicroAverageScore instance
        """
        # Averaging on sim. counts
        r_num = p_num = self._accum_scores["c"]
        r_den, p_den = self._accum_scores["t"], self._accum_scores["s"]
        recall = _normalize_with_0_standard_value(r_num, r_den)
        precision = _normalize_with_0_standard_value(p_num, p_den)
        f1 = compute_f1_score(recall, precision)
        return MicroAverageScore(recall, precision, f1, r_num, r_den, p_num, p_den)
    
    @classmethod
    def _compute_scores(cls, ref_partition, sys_partition):
        """ Computes the score value associated to the pair or input coreference partitions.
        
        Args:
            ground_truth_coreference_partition: a CoreferencePartition instance
            predicted_coreference_partition: a CoreferencePartition instance

        Returns:
            a cls._CEAFScore named tuple
        """
        # Initialize 2-d arrays
        _ref_partition = tuple(ref_partition)
        _sys_partition = tuple(sys_partition)
        maps = [[0 for _ in range(len(_sys_partition))] for _ in range(len(_ref_partition))]
        # Compute all chain alignments
        for i, tc in enumerate(_ref_partition):
            for j, oc in enumerate(_sys_partition):
                common_mention_nb = len(tc.intersection(oc))
                maps[i][j] = -common_mention_nb # neg since we minimize
        # Find optimal map (i.e., max. bipartite matching) by running the Munkres algorithm
        indexes = MUNKRES.compute(maps)
        # Compute similarity scores
        best_map_sim = 0
        for i, j in indexes: # 'best_map_sim <= sum(len(c) for c in _ref_partition)' and 'best_map_sim <= sum(len(c) for c in _sys_partition)' by construction, so recall and precision are indeed <= 1
            common_mention_nb = -maps[i][j] # cf. neg scores
            best_map_sim += common_mention_nb
        ref_self_sim = sum((len(c) for c in _ref_partition), 0)
        sys_self_sim = sum((len(c) for c in _sys_partition), 0)
        recall = best_map_sim / ref_self_sim
        precision = best_map_sim / sys_self_sim
        f1 = compute_f1_score(recall, precision)
        return cls._CEAFScore(recall, precision, f1, best_map_sim, ref_self_sim, sys_self_sim)



class CEAFEntityScorer(_CoreferenceScorer):
    """ CEAF Entity scorer from Luo (2005)
    
    Its computed score values are encapsulated into a 'CEAFEScore' named tuple class.
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        ident2score: a 'ident => score' map containing the evaluation data of the pairs of 
            coreference partition already evaluated
    """
    
    _ceaf_entity_specific_attribute_names = ("c", "t", "s")
    _ceaf_entity_score_attribute_name = _COMMON_SCORE_ATTRIBUTE_NAMES + _ceaf_entity_specific_attribute_names
    _CEAFEntityScore = namedtuple('ceaf_entity_score', _ceaf_entity_score_attribute_name)

    def _update(self, ident, ref_partition, sys_partition):
        """ Computes the score value associated to the pair or input coreference partitions, and 
        register it as associated to the specified ident.
        
        Args:
            ident: string, the string to be associated to the computed score, and which shall be 
                used to reference it when the formatted scores are displayed
            ground_truth_coreference_partition: a CoreferencePartition instance
            predicted_coreference_partition: a CoreferencePartition instance
        """
        scores = self._compute_scores(ref_partition, sys_partition)
        self._scores.append((ident, scores))
        
        for attribute_name in self._ceaf_entity_specific_attribute_names:
            self._accum_scores[attribute_name] = self._accum_scores.get(attribute_name, 0) + getattr(scores, attribute_name)

    def _micro_average_scores(self):
        """ Computes the micro average scores corresponding to the pairs of coreference partition 
        already evaluated.
        
        Returns:
            a MicroAverageScore instance
        """
        # Averaging on sim. counts
        r_num = p_num = self._accum_scores["c"]
        r_den, p_den = self._accum_scores["t"], self._accum_scores["s"]
        recall = _normalize_with_0_standard_value(r_num, r_den)
        precision = _normalize_with_0_standard_value(p_num, p_den)
        f1 = compute_f1_score(recall, precision)
        return MicroAverageScore(recall, precision, f1, r_num, r_den, p_num, p_den)
    
    @classmethod
    def _compute_scores(cls, ref_partition, sys_partition):
        """ Computes the score value associated to the pair or input coreference partitions.
        
        Args:
            ground_truth_coreference_partition: a CoreferencePartition instance
            predicted_coreference_partition: a CoreferencePartition instance

        Returns:
            cls._CEAFEntityScore named tuple
        """
        # Initialize 2-d arrays
        _ref_partition = tuple(ref_partition)
        _sys_partition = tuple(sys_partition)
        ref_entities_nb = len(_ref_partition)
        sys_entities_nb = len(_sys_partition)
        maps = [[0 for j in range(sys_entities_nb)] for i in range(ref_entities_nb)]
        # Compute all chain alignments
        for i, tc in enumerate(_ref_partition):
            for j, oc in enumerate(_sys_partition):
                common_mention_nb = len(tc.intersection(oc))
                maps[i][j] = -common_mention_nb # neg since we minimize
        # Find optimal map (i.e., max. bipartite matching) by running Munkres algorithm
        indexes = MUNKRES.compute(maps)
        # Compute similarity scores
        best_map_sim = 0.0
        for i, j in indexes: # Since we must see each 'i' or 'j' only once, 'len(indexes) = min(ref_entities_nb, sys_entities_nb)', so recall and precision are indeed <= 1, since best_map_sim <= 1 by construction
            common_mention_nb = -maps[i][j] # cf. neg scores
            best_map_sim += (2.0 * common_mention_nb) / (len(_ref_partition[i]) + len(_sys_partition[j]))
        ref_self_sim = ref_entities_nb
        sys_self_sim = sys_entities_nb
        recall = best_map_sim / ref_self_sim
        precision = best_map_sim / sys_self_sim
        f1 = compute_f1_score(recall, precision)
        return cls._CEAFEntityScore(recall, precision, f1, best_map_sim, ref_self_sim, sys_self_sim)



class BLANCScorer(_CoreferenceScorer):
    """ BLANC Scorer as described by Bagga & Baldwin (1998)
    
    Formula for BLANC:
    
    +-----------+----------------------+----------------------+---------------------+
    | Score     | Coreference          |   Non-coreference    | Synthesis           |
    +===========+======================+======================+=====================+
    | Precision | Pc = rc / (rc + wc)  | Pn = rn / (rn+wn)    | BLANC-P = (Pc+Pn)/2 |
    +-----------+----------------------+----------------------+---------------------+
    | Recall    | Rc = rc / (rc + wn)  | Rn = rn / (rn+wc)    | BLANC-R = (Rc+Rn)/2 |
    +-----------+----------------------+----------------------+---------------------+
    | F-score   | Fc = 2*Pc*Rc/(Pc+Rc) | Fn = 2*Pn*Rn/(Pn+Rn) | BLANC-F = (Fc+Fn)/2 |
    +-----------+----------------------+----------------------+---------------------+
    
    The legend for the table is the following:
    
    (1) The coreference decisions (made by the coreference system)
        
        - A coreference link "c" holds between every two mentions that corefer.
        - A non-coreference link "n" holds between every two mentions that do not corefer.
    
    (2) The correctness decisions (made by the evaluator)
        
        - A right link "r" has the same value (coreference or non-coreference) in 
          GOLD and SYS (i.e. when the system is correct).
        - A wrong link "w" does not have the same value (coreference or non-coreference) in 
          GOLD and SYS (i.e. when the system is wrong).
    
    Its computed score values are encapsulated into a '_BLANCScore' named tuple class.
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        ident2score: a 'ident => score' map containing the evaluation data of the pairs of 
            coreference partition already evaluated
    """
    
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = ("_mention_count",) + _CoreferenceScorer._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    
    _blanc_specific_attribute_names = ("ct",)
    _blanc_score_attribute_name = _COMMON_SCORE_ATTRIBUTE_NAMES + _blanc_specific_attribute_names
    _BLANCScore = namedtuple('blanc_score', _blanc_score_attribute_name)
    
    def __init__(self):
        super().__init__()
        self._mention_count = 0 # For normalization
    
    def _update(self, ident, ref_partition, sys_partition):
        """ Computes the score value associated to the pair or input coreference partitions, and 
        register it as associated to the specified ident.
        
        Args:
            ident: string, the string to be associated to the computed score, and which shall be 
                used to reference it when the formatted scores are displayed
            ground_truth_coreference_partition: a CoreferencePartition instance
            predicted_coreference_partition: a CoreferencePartition instance
        """
        scores = self._compute_scores(ref_partition, sys_partition)
        self._scores.append((ident, scores))
        
        mention_ct = scores.ct
        for attribute_name in _COMMON_SCORE_ATTRIBUTE_NAMES:
            # Weight scores by mention count
            score_contribution = getattr(scores, attribute_name) * mention_ct
            self._accum_scores[attribute_name] = self._accum_scores.get(attribute_name, 0) + score_contribution
        self._mention_count += mention_ct
    
    def _micro_average_scores(self):
        """ Computes the micro average scores corresponding to the pairs of coreference partition 
        already evaluated.
        
        Returns:
            a MicroAverageScore instance
        """
        # Averaging on number of mentions
        accum_scores = self._accum_scores
        r_den = p_den = self._mention_count
        r_num = accum_scores["r"]
        p_num = accum_scores["p"]
        recall = r_num / r_den
        precision = p_num / p_den
        f1 = compute_f1_score(recall, precision)
        return MicroAverageScore(recall, precision, f1, r_num, r_den, p_num, p_den)
    
    @staticmethod
    def _get_coreferring_mention_extent_pairs_sets(entities):
        """ Returns a collection of frozensets containing each two coreferring mention extents.
        
        Args:
            entities: collection of entities

        Returns:
            a collection of frozensets containing each two coreferring mention extents
        """
        mention_extent_pairs_sets = []
        for entity in entities:
            entity = tuple(entity)
            entity_size = len(entity)
            for i in range(entity_size):
                for j in range(i+1, entity_size):
                    mention_extent_pairs_sets.append(frozenset((entity[i], entity[j])))
        return mention_extent_pairs_sets

    @staticmethod
    def _get_non_coreferring_mention_extent_pairs_sets(entities):
        """ Returns a collection of frozensets containing each two non coreferring mention extents.
        
        Args:
            entities: collection of entities

        Returns:
            a collection of frozensets containing each two non coreferring mention extents
        """
        mention_extent_pairs_sets = []
        entities_nb = len(entities)
        for p in range(entities_nb):
            entity1 = tuple(entities[p])
            for i in range(0, len(entity1)):
                for q in range(p+1, entities_nb):
                    entity2 = tuple(entities[q])
                    for j in range(0, len(entity2)):
                        mention_extent_pairs_sets.append(frozenset((entity1[i], entity2[j])))
        return mention_extent_pairs_sets
    
    @classmethod
    def _compute_scores(cls, ref_partition, sys_partition): # TODO: check with imperfect mention detection!
        """ Computes the score value associated to the pair or input coreference partitions.
        
        Args:
            ground_truth_coreference_partition: a CoreferencePartition instance
            predicted_coreference_partition: a CoreferencePartition instance

        Returns:
            a cls._BLANCScore named tuple
        """
        # FIXME: what to do about the legacy TODO located above?
        _sys_partition = tuple(sys_partition)
        _ref_partition = tuple(ref_partition)
        
        ref_coref = cls._get_coreferring_mention_extent_pairs_sets(_ref_partition)
        ref_non_coref = cls._get_non_coreferring_mention_extent_pairs_sets(_ref_partition)
        sys_coref = cls._get_coreferring_mention_extent_pairs_sets(_sys_partition)
        sys_non_coref = cls._get_non_coreferring_mention_extent_pairs_sets(_sys_partition)
        rc = len(set(ref_coref) & set(sys_coref)) # intersection
        wc = len(set(sys_coref) - set(ref_coref))
        rn = len(set(ref_non_coref) & set(sys_non_coref))
        wn = len(set(sys_non_coref) - set(ref_non_coref))
        
        Pc = 0 if rc == 0 else rc*1.0/(rc+wc)
        Pn = 0 if rn == 0 else rn*1.0/(rn+wn)
        precision = (Pc+Pn) / 2
        
        Rc = 0 if rc == 0 else rc*1.0/(rc+wn)
        Rn = 0 if rn == 0 else rn*1.0/(rn+wc)
        recall  = (Rc+Rn)/2
        
        Fc = compute_f1_score(Pc, Rc)
        Fn = compute_f1_score(Pn, Rn)
        f1 = (Fc+Fn) / 2
        
        ref_mention_ct = sum(len(c) for c in ref_partition)
        
        return cls._BLANCScore(recall, precision, f1, ref_mention_ct)



class AllScorer(EqualityMixIn):
    """ Class used to evaluate using all implemented CoreferenceScorer classes at once.
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
    """
    
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = ("muc", "b3", "ceaf", "ceaf_entity", "blanc")
    
    def __init__(self):
        # Assign attribute values
        self.muc = MucScorer()
        self.b3 = B3Scorer()
        self.ceaf = CEAFScorer()
        self.ceaf_entity = CEAFEntityScorer()
        self.blanc = BLANCScorer()
    
    @property
    def NAME(self):
        return type(self).__name__
    @NAME.setter
    def NAME(self, value):
        raise AttributeError
    
    def update(self, ident, ground_truth_coreference_partition, predicted_coreference_partition):
        """ Computes the coreference score associated to the () pair, and register it as associated 
        to the specified ident
        
        Args:
            ident: string, the string to be associated to the computed score, and which shall be 
                used to reference it when the formatted scores are displayed
            ground_truth_coreference_partition: a CoreferencePartition instance
            predicted_coreference_partition: a CoreferencePartition instance
        """
        if len(ground_truth_coreference_partition) == 0:
            return
        self.muc.update(ident, ground_truth_coreference_partition, predicted_coreference_partition)
        self.b3.update(ident, ground_truth_coreference_partition, predicted_coreference_partition)
        self.ceaf.update(ident, ground_truth_coreference_partition, predicted_coreference_partition)
        self.ceaf_entity.update(ident, ground_truth_coreference_partition, predicted_coreference_partition)
        self.blanc.update(ident, ground_truth_coreference_partition, predicted_coreference_partition)
    
    def print_average_scores(self, avg_scores_type="micro"):
        """ Computes the average score valuess corresponding to the pairs of coreference partition 
        already evaluated, and then a formatted string representing those scores.
        
        Print it and then return it.
        
        Args:
            avg_scores_type: string, either 'micro' or 'macro'; the type of average score to 
                compute and format

        Returns:
            a string
        """
        final_strings_array = self._get_average_scores_strings_array(avg_scores_type=avg_scores_type)
        report = "\n".join(final_strings_array)
        print(report)
        return report
    
    def print_scores(self, avg_scores_type="micro"):
        """ Computes the scores and average scores corresponding to the pairs of coreference partition 
        already evaluated, and then a formatted string representing those scores.
        
        Print it and then return it.
        
        Args:
            avg_scores_type: string, either 'micro' or 'macro'; the type of average score to 
                compute and format

        Returns:
            a string
        """
        strings = []
        strings.append(self.muc.print_average_scores(avg_scores_type=avg_scores_type))
        strings.append(self.b3.print_average_scores(avg_scores_type=avg_scores_type))
        strings.append(self.ceaf.print_average_scores(avg_scores_type=avg_scores_type))
        strings.append(self.ceaf_entity.print_average_scores(avg_scores_type=avg_scores_type))
        strings.append(self.blanc.print_average_scores(avg_scores_type=avg_scores_type))
        string = "\n".join(strings)
        return string
    
    def _get_average_scores_strings_array(self, avg_scores_type="micro"):
        """ Computes the average score values corresponding to the pairs of coreference partition 
        already evaluated, and return the strings collection which can be used to build the report 
        describing those scores and the individual score values.
        
        Args:
            avg_scores_type: string, either 'micro' or 'macro'; the type of average score to 
                compute and format

        Returns:
            a collection of strings
        """
        def _replace_first_tuple_elt(t, elt):
            return (elt,) + t[1:]
        header_row_strings_array, line_strings_array = self.muc._get_average_scores_strings_arrays(avg_scores_type=avg_scores_type)
        header_row_strings_array = _replace_first_tuple_elt(header_row_strings_array, "Scorer")
        line_strings_array = _replace_first_tuple_elt(line_strings_array, self.muc.NAME)
        row_strings_arrays = [header_row_strings_array, line_strings_array]
        for attribute_name in ("b3", "ceaf", "ceaf_entity", "blanc"):
            scorer = getattr(self, attribute_name)
            _, line_strings_array = scorer._get_average_scores_strings_arrays(avg_scores_type=avg_scores_type)
            line_strings_array = _replace_first_tuple_elt(line_strings_array, scorer.NAME)
            row_strings_arrays.append(line_strings_array)
        
        row_strings_arrays = _uniformize_rows_strings_arrays(row_strings_arrays)
        row_strings_arrays = tuple("| "+" | ".join(string_array)+" |" for string_array in row_strings_arrays)
        
        row_length = len(row_strings_arrays[0])
        avg_score_ident = "{} AVG.".format(avg_scores_type.upper())
        addendum_format = "| {:<"+str(row_length-4)+"s} |"
        avg_score_ident_row = addendum_format.format(avg_score_ident)
        
        dash_line = "-"*row_length
        
        muc_scores = self.muc.compute_average_scores(avg_scores_type=avg_scores_type)
        b3_scores = self.b3.compute_average_scores(avg_scores_type=avg_scores_type)
        ceafe_scores = self.ceaf_entity.compute_average_scores(avg_scores_type=avg_scores_type)
        average = (muc_scores.f + b3_scores.f + ceafe_scores.f) / 3
        final_average_string = "MUC/B3/CEAFE f1 average: {:6.2f} %".format(round(100*average,2))
        final_average_row = addendum_format.format(final_average_string)
        
        final_strings_array = (dash_line, avg_score_ident_row, dash_line, row_strings_arrays[0], dash_line) + row_strings_arrays[1:] + (dash_line, final_average_row, dash_line)
        
        return final_strings_array
