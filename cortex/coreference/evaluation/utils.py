# -*- coding: utf-8 -*-

"""
Provide utilities needed by the other modules of this package.
"""

import numpy

from cortex.utils import normalize

__all__ = ["create_get_resource_file_path_fct", 
           "create_get_iterator_over_resource_file_fct",
           ]

def _convert_NaN_to_0(number):
    """ Returns 0. if the input is NaN, else return the input.
    
    Args:
        number: a number (int, float...)

    Returns:
        a number (int, float...)
    """
    return 0.0 if numpy.isnan(number) else number

def _normalize_with_0_standard_value(numerator, denominator):
    """ Normalizes (divide) a value by another, but only if the denominator is strictly positive, 
    else return 0.
    
    Args:
        sub_nb: int or float
        total_nb: int or float

    Returns:
        the float number resulting from the division of the numerator by a float version of the 
        denominator, or 0. if the denominator value is not suitable for normalization
    """ 
    return _convert_NaN_to_0(normalize(numerator, denominator))

def _uniformize_rows_strings_arrays(rows_strings_arrays):
    """ Takes an input corresponding to a 2D-array like of string values, and make it so all the 
    string values belonging to a given column have all the same length, by padding them to the left.
    
    Args:
        rows_strings_arrays: collection of collection of strings, each collection being of the 
            same length

    Returns:
        collection of collection of strings, each collection being of the same length
    """
    columns_strings_arrays = tuple(zip(*rows_strings_arrays))
    max_column_width_array = tuple(max(len(s) for s in column_strings_array) for column_strings_array in columns_strings_arrays)
    column_formats = tuple("{:<"+str(w)+"s}" for w in max_column_width_array)
    new_columns_strings_array = tuple(tuple(format_string.format(s) for s in strings_column) for format_string, strings_column in zip(column_formats, columns_strings_arrays))
    new_rows_strings_arrays = tuple(zip(*new_columns_strings_array))
    return new_rows_strings_arrays