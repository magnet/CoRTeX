# -*- coding: utf-8 -*-

"""
Defines classes and function used to evaluate predictions made on CoreferencePartition instances
"""

"""
Available submodules
--------------------
matcher:
    Defines a class used to align the respective universes of two coreference partition

scorer:
    Defines scorer classes implementing different kind of coreference partition prediction evaluation 
    process

conll2012_scorer:
    Defines utilities to evaluate coreference partition prediction made for documents, using the 
    CONLL2012 task's own software
"""

__all__ = ["DetectionScorer", "Matcher", 
           "MucScorer", "B3Scorer", "CEAFScorer", "CEAFEntityScorer", "BLANCScorer", "AllScorer",
           "create_scorer", "create_scorer_from_factory_configuration", "generate_scorer_factory_configuration",  
           "CONLL2012Scorer", 
           "format_overall_results", "format_evaluation_result", "format_resolvers_evaluation_results",
           ]

from .conll2012_scorer import (CONLL2012Scorer, format_overall_results, format_evaluation_result, 
                               format_resolvers_evaluation_results)
from .scorer import (DetectionScorer, MucScorer, B3Scorer, CEAFScorer, CEAFEntityScorer, BLANCScorer, 
                     AllScorer)
from .matcher import Matcher
