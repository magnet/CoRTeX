# -*- coding: utf-8 -*-

"""
Defines classes and structures used to create, train, use and evaluate coreference resolution models.
"""

"""
Available subpackages
---------------------
generator
    Defines classes used for generating, from Document instances, iterators over Sample child classes

decoder
    Defines classes used to create a CoreferencePartition instance from the analysis of predictions 
    made for Sample instances

resolver
    Defines classes used to create, train and use coreference resolution models
    
evaluation
    Defines classes and function used to evaluate predictions made on CoreferencePartition instances
    
api
    Defines classes and functions to facilitate the implementation of experiments involving resolvers 
"""

from .resolver import (create_resolver_from_factory_configuration, create_resolver,
                       generate_resolver_factory_configuration, load_resolver, 
                       define_generate_factory_configuration_function)
from .evaluation import (CONLL2012Scorer, format_overall_results, format_evaluation_result, 
                         format_resolvers_evaluation_results)
from .api import (LocalVectorizationCache, evaluate_resolvers, evaluate_resolver, generate_evaluation_results,  
                  ResolverComparisonExperiment, ExistingResolverComparisonExperimentError, 
                  carry_out_resolver_grid_search, 
                  carry_out_efficient_online_interface_based_resolver_comparison_experiment, 
                  carry_out_efficient_online_interface_based_resolver_comparison_single_experiment, 
                  create_kfold_corpora_files_folder, parse_kfold_corpora)

from . import generator
from . import decoder
from . import resolver
from . import evaluation
