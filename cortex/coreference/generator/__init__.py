# -*- coding: utf-8 -*-

"""
Defines classes used to create collections of samples, to be used for machine learning tasks, from documents

The role of generators is to convert sets of Document instance into collection of Sample class 
instances to be used for machine learning tasks:
- to be able to create a machine-learning training dataset from a collection of qualified document data
- to be bale to create a machine-learning prediction dataset from a single document

Generator classes inherit from the 'ConfigurationmixIn' class, and as such their instances can be 
characterized by their configuration / an instance of those class can be created from such a configuration.
Cf 'cortex.tools.configuration_mixin'.
"""

"""
Available submodules
--------------------
base
    Defines base generator class

pairs
    Defines generator classes that yield sample instances corresponding to pair of document mentions

API:
----
create_generator
    Function used to create a Generator instance, from specifying the name of the Generator class, and 
    potentially some initialization parameters

generate_generator_factory_configuration
    Function used to create a specific Generator class instance's factory configuration, from specifying 
    the name of the Generator class, and potentially some initialization parameters

create_generator_from_factory_configuration
    Function used to create a Generator instance, from specifying its factory configuration
"""

__all__ = ["MentionPairSampleGenerator", 
           "SoonGenerator", 
           "NgCardieGenerator", 
           "ExtendedMentionPairSampleGenerator",
           "create_generator", 
           "create_generator_from_factory_configuration", 
           "generate_generator_factory_configuration",
           ]

from collections import OrderedDict

from .pairs import (MentionPairSampleGenerator, SoonGenerator, NgCardieGenerator, 
                    ExtendedMentionPairSampleGenerator,)
from cortex.tools import (define_check_item_name_function, define_create_function, 
                          define_generate_factory_configuration_function, 
                          define_create_from_factory_configuration_function,
                          )

generator_names_collection_and_generator_class_pairs = ((("mentionpair", MentionPairSampleGenerator.__name__), MentionPairSampleGenerator),
                                                        (("soon", SoonGenerator.__name__), SoonGenerator),
                                                        (("ngcardie", NgCardieGenerator.__name__), NgCardieGenerator),
                                                        (("extendedmentionpair", ExtendedMentionPairSampleGenerator.__name__), ExtendedMentionPairSampleGenerator),
                                                        )
generator_name2generator_class = OrderedDict((name, class_) for names, class_ in generator_names_collection_and_generator_class_pairs for name in names)

# High level functions
_check_generator_name = define_check_item_name_function(generator_name2generator_class, "generator")

create_generator = define_create_function(generator_name2generator_class, "generator")

create_generator_from_factory_configuration =\
 define_create_from_factory_configuration_function(generator_name2generator_class, "generator")

generate_generator_factory_configuration =\
 define_generate_factory_configuration_function(generator_name2generator_class, "generator")
