# -*- coding: utf-8 -*-

"""
Defines generator base class
"""

__all__ = ["_Generator",]

import abc
import logging

from cortex.utils import EqualityMixIn
from cortex.tools import ConfigurationMixIn
from cortex.api.document import DocumentDoesNotPossessMentionsError, DocumentDoesNotPossessCoreferencePartitionError

class _Generator(ConfigurationMixIn, EqualityMixIn, metaclass=abc.ABCMeta):
    """ Generator base class
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
        to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    
    Examples:
        >> training_samples_collection = generator.generate_training_set(documents_iterable)
    
        or 
    
        >> prediction_sample_collection = generator.generate_prediction_set(document)
    """
    
    SAMPLE_TYPE = None
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = ("configuration",)
    
    def __init__(self, configuration=None):
        # Initialize using parent class
        ConfigurationMixIn.__init__(self, configuration=configuration)
        # Set attribute values
        self.logger = logging.getLogger("generator")
        # All value that can be input into the '__init__' function must be recorded in the configuration
    
    @abc.abstractmethod
    def generate_training_set(self, documents_iterable):
        """ Generates a collection of labelled ML samples from the input Document instances iterable.
        
        Args:
            documents: an iterable over Document instances

        Returns:
            an iterable over labelled ML sample class instances
        """
        pass
    
    @abc.abstractmethod
    def generate_prediction_set(self, document):
        """ Generates a collection of unlabelled ML samples from the input Document instance.
        
        Args:
            document: Document instance

        Returns:
            an iterable over unlabelled ML sample class instances
        """
        pass
    
    def _preprocess_generation(self, document, train):
        """ Checks that the input Document instance is fit for the desired Sample instances
        generation process.
        
        Notably, check that mentions exists in the document; and check that a coreference partition 
        is associated to the document, if the set to be generated is to be used for training purposes.
        
        Args:
            document: the Document instance from which to generate the ML samples
            train: boolean, whether or not the Document must be able to provide information 
                about the coreference entities to which its mentions belong 
        
        Raises:
            DocumentDoesNotPossessMentionsError: if the input Document instance does not possess proper 
                mentions info
            DocumentDoesNotPossessCoreferencePartitionError: if the input Document instance does not 
                possess proper coreference partition info
        """
        # Check that the document indeed possess mentions
        self._check_document_for_mentions(document)
        if train:
            # If we want to add label values, we need to check first that the document indeed possess a coreference partition
            self._check_document_for_coreference_partition(document)
    
    @staticmethod
    def _check_document_for_mentions(document):
        """ Checks that the input Document instance possesses a proper 'mentions' attribute value 
        (that is, not a 'None' value).
        
        Args:
            document: Document instance
        
        Raises:
            DocumentDoesNotPossessMentionsError: if the input Document instance does not possess proper 
                mentions info
        """
        if document.mentions is None:
            raise DocumentDoesNotPossessMentionsError()
    
    @staticmethod
    def _check_document_for_coreference_partition(document):
        """ Checks that the input Document instance possesses a proper 'coreference_partition' 
        attribute value (that is, not a 'None' value).
        
        Args:
            document: Document instance
        
        Raises:
            DocumentDoesNotPossessCoreferencePartitionError: if the input Document instance does not 
                possess proper coreference partition info
        """
        if document.coreference_partition is None:
            message = "The document '{}' does not possess a coreference partition, cannot "\
                      "generate labelled samples from it.".format(document)
            raise DocumentDoesNotPossessCoreferencePartitionError(message)
