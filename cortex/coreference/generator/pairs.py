# -*- coding: utf-8 -*-

"""
Defines generator classes that yield sample instances corresponding to pair of mentions
"""

__all__ = ["MentionPairSampleGenerator", 
           "SoonGenerator", 
           "NgCardieGenerator", 
           "ExtendedMentionPairSampleGenerator",
           "_BaseMentionPairGenerator", 
           "_BaseMentionPairSampleGenerator", 
           "_BaseExtendedMentionPairSampleGenerator", 
           ]

import abc
from collections import OrderedDict

from .base import _Generator
from cortex.api.markable import NULL_MENTION
from cortex.learning.samples import MentionPairSample
from cortex.learning.parameters import POS_CLASS, NEG_CLASS
from cortex.learning.filter.pairs import create_pair_filter_from_factory_configuration, AcceptAllPairFilter
from cortex.tools import _create_simple_get_parameter_value_from_configuration, Mapping, deepcopy_mapping
from cortex.utils.display import get_display_progress_function
from cortex.api.document import DocumentDoesNotPossessCoreferencePartitionError

from cortex.config import language
from cortex.languages import create_get_language_dependent_object_fct
get_language_dependent_object = create_get_language_dependent_object_fct(language)

MENTION_FEATURES = get_language_dependent_object("mention_features")


PAIR_FILTER_DEFAULT_VALUE_FACTORY_CONFIGURATION = Mapping((("name", "acceptall"),))
pair_filter_attribute_data = ("pair_filter",
                              (_create_simple_get_parameter_value_from_configuration("pair_filter", 
                                                                                     default=lambda: deepcopy_mapping(PAIR_FILTER_DEFAULT_VALUE_FACTORY_CONFIGURATION), 
                                                                                     postprocess_fct=create_pair_filter_from_factory_configuration), 
                               lambda instance: setattr(instance.configuration, "pair_filter", instance.pair_filter.factory_configuration)
                               )
                              )
class _BaseMentionPairGenerator(_Generator):
    """ Base class for generator creating Sample instances where an instance is based on the given 
    of two mentions.
    
    Arguments:
        pair_filter: a PairFilter instance, or None; if None, an 'AcceptAll' pair filter instance 
            will be used. Cf 'cortex.learning.filter.pairs'.
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        pair_filter: instance of PairFilter subclass; if used to decide whether or not to produce a 
            ML sample for a given pair of mentions. Cf 'cortex.learning.filter.pairs'
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    
    In addition to the 'generate_training_set' and 'generate_prediction_set' methods 
    (cf 'cortex.coreference.generator.base'), the following method is available, so as to produce 
    more fine-grained samples collections:
    
    Examples:
        >>> sample = generator.generate_sample_from_mention_pair(antecedent_mention, subsequent_mention)
    """
    
    __INIT_PARAMETERS = OrderedDict((pair_filter_attribute_data,))
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple(__INIT_PARAMETERS.keys()) +\
                                                _Generator._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = _Generator._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION+\
                                            tuple(__INIT_PARAMETERS.keys())

    def __init__(self, pair_filter=None, configuration=None):
        # Check input values
        # Init parent
        super().__init__(configuration=configuration)
        # Process input values
        if pair_filter is None:
            pair_filter = create_pair_filter_from_factory_configuration(deepcopy_mapping(PAIR_FILTER_DEFAULT_VALUE_FACTORY_CONFIGURATION))
        
        # Assign attribute values
        self.pair_filter = pair_filter
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        pair_filter, = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        args = args + (pair_filter,)
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, pair_filter_factory_configuration=None):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Cf the documentation of the class of know which argument(s) to use.
        
        Returns:
            a Mapping instance
        """
        if pair_filter_factory_configuration is None:
            pair_filter_factory_configuration = AcceptAllPairFilter.define_factory_configuration()
        configuration = super().define_configuration()
        configuration["pair_filter"] = pair_filter_factory_configuration
        return configuration
    
    # Instance methods
    def generate_training_set(self, documents, verbose=False):
        """ Creates a generator over labelled ML samples, created from the input Document instances 
        iterable.
        
        Args:
            documents: an iterable over Document instances
            verbose: whether or not to log progress messages regarding the iteration process over 
                the input Document instances

        Yields:
            labelled _Sample child class instance
        """
        display_progress_fct = lambda i: None
        if verbose:
            try:
                documents_nb = len(documents)
            except (TypeError, AttributeError):
                documents_nb = None
            display_progress_fct = get_display_progress_function(self.logger, maximum_count=documents_nb, 
                                                                 logging_level="debug")
        yielded_samples_nb = 0
        self.logger.debug("Generating ML samples...")
        for i, document in enumerate(documents):
            display_progress_fct(i)
            
            try:
                self._check_document_for_coreference_partition(document)
            except DocumentDoesNotPossessCoreferencePartitionError:
                message = "Input document '{}' does not possess a coreference partition, but we "\
                          "need to be able to label the ML samples: skipped during generation."
                message = message.format(document)
                self.logger.warning(message)
            else:
                sample_iterable = self._generate_training_set(document)
                for sample in sample_iterable:
                    yield sample
                    yielded_samples_nb += 1
        self.logger.debug("Finished generating ML samples: {} ML sample(s) yielded.".format(yielded_samples_nb))
    
    def generate_prediction_set(self, document): #, entity_head_extents=None, singleton_extents=None
        """ Generates a collection of unlabelled ML samples from the input Document instance.
        
        Args:
            document: a Document instance

        Returns:
            an iterable over unlabelled _Sample child class instances
        """
        train = False
        self._preprocess_generation(document, train)
        return self._generate_sample_set(document, train=train) # , entity_head_extents=entity_head_extents, singleton_extents=singleton_extents
    
    def _generate_training_set(self, document): #, entity_head_extents=None, singleton_extents=None
        """ Generates a collection of labelled ML samples from the input Document instance.
        
        Args:
            document: a Document instance

        Returns:
            an iterable over labelled _Sample child class instances
        """
        train = True
        self._preprocess_generation(document, train)
        return self._generate_sample_set(document, train=train)  # , entity_head_extents=entity_head_extents, singleton_extents=singleton_extents
    
    def _generate_postprocessed_sample_from_mention_pair(self, antecedent_mention, subsequent_mention, 
                                                         sample_postprocessing_fct, 
                                                         entity_head_extents=None, singleton_extents=None):
        """ Creates the sample corresponding to an input pair of mention, and apply on it the input 
        postprocesing function.
        
        Do not test whether or not the pair pass the filter associated to this generator instance.
        
        Args:
            antecedent_mention: Mention instance, the first of mention of the pair, introduced 
                before the other in the document's reading order
            subsequent_mention: Mention instance, the second of mention of the pair, introduced 
                after the other in the document's reading order
            sample_postprocessing_fct: callable, function to apply to the sample created by this 
                method (output will be ignored)
            entity_head_extents: set-like of mention extents (valid for the input document) whose 
                corresponding mention (for this document) are the head of their respective entity; used to 
                define the corresponding attribute of a MentionPairSample instance
            singleton_extents: set-like of mention extents (valid for the input document) whose 
                corresponding mention (for this document) are singletons; used to define the corresponding 
                attribute of a MentionPairSample instance

        Returns:
            a MentionPairSample class instance
        """
        sample = MentionPairSample(antecedent_mention, subsequent_mention, 
                                   entity_head_extents=entity_head_extents, 
                                   singleton_extents=singleton_extents)
        sample_postprocessing_fct(sample)
        return sample
    
    @abc.abstractmethod
    def _generate_sample_set(self, document, train=False, entity_head_extents=None, singleton_extents=None):
        """ Returns a mention pair sample class instances iterator, using the generator's own sampling method. 
        
        Args:
            document: Document instance; must possess a collection of mentions
            train: boolean (default = True), whether or not to give a label to a generated 
                classification sample; if True, then a coreference partition must be defined for the input 
                document
            entity_head_extents: set-like of mention extents (valid for the input document) whose 
                corresponding mention (for this document) are the head of their respective entity; used to 
                define the corresponding attribute of a MentionPairSample instance
            singleton_extents: set-like of mention extents (valid for the input document) whose 
                corresponding mention (for this document) are singletons; used to define the corresponding 
                attribute of a MentionPairSample instance

        Returns:
            an iterator over MentionPairSample instances
        
        Warning:
            If show_label is True, the 'coreference_partition' attribute of the input Document 
            instance must indeed be a CoreferencePartition Instance, and not None.
        """
        pass
    
    @abc.abstractmethod
    def _generate_sample_set_from_mentions(self, mentions, sample_postprocessing_fct, 
                                           entity_head_extents=None, singleton_extents=None):
        """ Returns an iterator over mention pair samples built from the collection of mentions of a 
        document
        
        Args:
            mentions: collection of mentions ordered from first to last using the order defined 
                on the Mention class instances
            sample_postprocessing_fct: function that takes a MentionPairSample instance as input 
                (output will be ignored)
            entity_head_extents: set-like of mention extents (valid for the input document) whose 
                corresponding mention (for this document) are the head of their respective entity; used to 
                define the corresponding attribute of a MentionPairSample instance; or None
            singleton_extents: set-like of mention extents (valid for the input document) whose 
                corresponding mention (for this document) are singletons; used to define the corresponding 
                attribute of a MentionPairSample instance; or None

        Returns:
            an iterator over MentionPairSample instances
        """
        pass
    
    @abc.abstractmethod
    def generate_sample_from_mention_pair(self, antecedent_mention, subsequent_mention):
        """ Creates the sample corresponding to an input pair of mention.
        
        Args:
            antecedent_mention: Mention instance, the first of mention of the pair, introduced 
                before the other in the document's reading order
            subsequent_mention: Mention instance, the second of mention of the pair, introduced 
                after the other in the document's reading order

        Returns:
            _Sample child class instance
        """
        pass



class _BaseMentionPairSampleGenerator(_BaseMentionPairGenerator):
    """ Base class for MentionPairSample instances generator, where the instance shall be made out 
    only of pairs of true mentions, that is, Mention instances
    
    Arguments:
        pair_filter: a PairFilter instance, or None; if None, an 'AcceptAll' pair filter instance 
            will be used. Cf 'cortex.learning.filter.pairs'
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SAMPLE_TYPE: MentionPairSample.SAMPLE_TYPE
        
        pair_filter: instance of PairFilter subclass; if used to decide whether or not to produce a 
            ML sample for a given pair of mentions. Cf 'cortex.learning.filter.pairs'
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    SAMPLE_TYPE = MentionPairSample.SAMPLE_TYPE
    
    @staticmethod
    def _add_label_to_sample(mention_pair_sample, coreference_partition):
        """ Adds correct binary classification label to the input mention pair sample class instance, 
        using the info provided by the input coreference classification structure.
        It is assumed that both input are consistent with one another (come from the same document...).
        
        Args:
            mention_pair_sample: MentionPairSample instance
            coreference_partition: CoreferencePartition instance
        """
        antecedent_mention, subsequent_mention = mention_pair_sample.pair
        if coreference_partition.are_coreferent(subsequent_mention.extent, 
                                                antecedent_mention.extent):
            mention_pair_sample.label = POS_CLASS
        else:
            mention_pair_sample.label = NEG_CLASS
    
    def generate_sample_from_mention_pair(self, antecedent_mention, subsequent_mention, train=True, 
                                          entity_head_extents=None, singleton_extents=None):
        """ Creates the sample corresponding to an input pair of mention.
        May correspond either to a training sample, or a prediction sample.
        Do not test whether or not the pair pass the filter associated to this generator instance.
        
        Args:
            antecedent_mention: Mention instance, the first of mention of the pair, introduced 
                before the other in the document's reading order
            subsequent_mention: Mention instance, the second of mention of the pair, introduced 
                after the other in the document's reading order
            train: boolean (default = True), whether or not to give a label to a generated 
                classification sample; if True, then a coreference partition must be defined for the input 
                document
            entity_head_extents: set-like of mention extents (valid for the input document) whose 
                corresponding mention (for this document) are the head of their respective entity; used to 
                define the corresponding attribute of a MentionPairSample instance
            singleton_extents: set-like of mention extents (valid for the input document) whose 
                corresponding mention (for this document) are singletons; used to define the corresponding 
                attribute of a MentionPairSample instance

        Returns:
            a MentionPairSample class instance
        """
        sample_postprocessing_fct = lambda sample: None
        if train:
            sample_postprocessing_fct = lambda sample: self._add_label_to_sample(sample, 
                                                                                 sample.document.coreference_partition)
        return self._generate_postprocessed_sample_from_mention_pair(antecedent_mention, subsequent_mention, 
                                                                     sample_postprocessing_fct, 
                                                                     entity_head_extents=entity_head_extents, 
                                                                     singleton_extents=singleton_extents)
    
    def _generate_sample_set(self, document, entity_head_extents=None, singleton_extents=None, 
                             train=True):
        """ Returns a mention pair sample class instances iterator, using the generator's own sampling 
        method. 
        
        Args:
            document: Document instance; must possess a collection of mentions
            train: boolean (default = True), whether or not to give a label to a generated 
                classification sample; if True, then a coreference partition must be defined for the input 
                document
            entity_head_extents: set-like of mention extents (valid for the input document) whose 
                corresponding mention (for this document) are the head of their respective entity; used to 
                define the corresponding attribute of a MentionPairSample instance
            singleton_extents: set-like of mention extents (valid for the input document) whose 
                corresponding mention (for this document) are singletons; used to define the corresponding 
                attribute of a MentionPairSample instance

        Returns:
            an iterator over MentionPairSample instances
        

        Warning:
            If 'train' is True, the 'coreference_partition' attribute of the input Document instance 
            must indeed be a CoreferencePartition Instance, and not None.
        """
        # Get mention collection used to carry out samples generation
        mentions = document.mentions
        sample_postprocessing_fct = lambda sample: None
        if train:
            coreference_partition = document.coreference_partition
            sample_postprocessing_fct = lambda sample: self._add_label_to_sample(sample, coreference_partition)
        # Carry out the generation
        return self._generate_sample_set_from_mentions(mentions, sample_postprocessing_fct, 
                                                       entity_head_extents=entity_head_extents, 
                                                       singleton_extents=singleton_extents)



class MentionPairSampleGenerator(_BaseMentionPairSampleGenerator):
    """ Default mention pair generator class.
    
    Creates a MentionPairSample instance for every distinct pair of mentions, where the first mention 
    of the pair must come first in the document, according to the reading order defined on the document
    
    Arguments:
        pair_filter: a PairFilter instance, or None; if None, an 'AcceptAll' pair filter instance 
            will be used. Cf 'cortex.learning.filter.pairs'
        
        configuration: a Mapping instance; or None; info about the class instance that the user wants 
            to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SAMPLE_TYPE: MentionPairSample.SAMPLE_TYPE
        
        pair_filter: instance of PairFilter subclass; if used to decide whether or not to produce a 
            ML sample for a given pair of mentions. Cf 'cortex.learning.filter.pairs'
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    
    Examples:
        >>> training_samples_collection = generator.generate_training_set(documents_iterable)
    
        or 
    
        >>> prediction_sample_collection = generator.generate_prediction_set(document)
    """
    
    def _generate_sample_set_from_mentions(self, mentions, sample_postprocessing_fct, 
                                           entity_head_extents=None, singleton_extents=None):
        """ Creates a generator over every mention pair samples built from the input collection of 
        mentions.
        
        Args:
            mentions: collection of mentions ordered from first to last using the order defined 
                on the Mention class instances
            sample_postprocessing_fct: callable, function to apply to the sample created by this 
                method (output will be ignored)
            entity_head_extents: set-like of mention extents (valid for the input document) whose 
                corresponding mention (for this document) are the head of their respective entity; used to 
                define the corresponding attribute of a MentionPairSample instance; or None
            singleton_extents: set-like of mention extents (valid for the input document) whose 
                corresponding mention (for this document) are singletons; used to define the corresponding 
                attribute of a MentionPairSample instance; or None

        Yields:
            MentionPairSample instance
        """
        # Left to right
        for j in range(1, len(mentions)): # We start from 1 since there is no pair to create with the first mention anyway
            subsequent_mention = mentions[j]
            # Right to left
            for i in range(j-1, -1, -1):
                antecedent_mention = mentions[i]
                if self.pair_filter.accept(antecedent_mention, subsequent_mention):
                    sample = self._generate_postprocessed_sample_from_mention_pair(antecedent_mention, subsequent_mention, 
                                                                                   sample_postprocessing_fct, 
                                                                                   entity_head_extents=entity_head_extents, 
                                                                                   singleton_extents=singleton_extents)
                    yield sample


################### SOON GENERATOR ###################
class SoonGenerator(_BaseMentionPairSampleGenerator):
    """ Generator class implementing Soon et al. 2001 sampling method.
    
    Arguments:
        pair_filter: a PairFilter instance, or None; if None, an 'AcceptAll' pair filter instance 
            will be used. Cf 'cortex.learning.filter.pairs'
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SAMPLE_TYPE: MentionPairSample.SAMPLE_TYPE
        
        pair_filter: instance of PairFilter subclass; if used to decide whether or not to produce a 
            ML sample for a given pair of mentions. Cf 'cortex.learning.filter.pairs'
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    
    Examples:
        >>> training_samples_collection = generator.generate_training_set(documents_iterable)
    
        or 
    
        >>> prediction_sample_collection = generator.generate_prediction_set(document)
    """
    
    def _generate_sample_set_from_mentions(self, mentions, sample_postprocessing_fct, 
                                           entity_head_extents=None, singleton_extents=None):
        """ Creates a generator over mention pair samples created from the input collections using 
        the Soon et al. 2001 sampling method.
        
        Args:
            mentions: collection of mentions ordered from first to last using the order defined 
                on the Mention class instances
            sample_postprocessing_fct: callable, function to apply to the sample created by this 
                method (output will be ignored)
            entity_head_extents: set-like of mention extents (valid for the input document) whose 
                corresponding mention (for this document) are the head of their respective entity; used to 
                define the corresponding attribute of a MentionPairSample instance; or None
            singleton_extents: set-like of mention extents (valid for the input document) whose 
                corresponding mention (for this document) are singletons; used to define the corresponding 
                attribute of a MentionPairSample instance; or None

        Yields:
            MentionPairSample instance
        """
        # Left to right
        for j in range(1, len(mentions)):
            subsequent_mention = mentions[j]
            # Right to left
            for i in range(j-1, -1, -1):
                antecedent_mention = mentions[i]
                if self.pair_filter.accept(antecedent_mention, subsequent_mention):
                    sample = self._generate_postprocessed_sample_from_mention_pair(antecedent_mention, subsequent_mention, 
                                                                     sample_postprocessing_fct, 
                                                                     entity_head_extents=entity_head_extents, 
                                                                     singleton_extents=singleton_extents)
                    yield sample
                    # Stop after closest antecedent
                    if sample.label == POS_CLASS:
                        break



################### NG & CARDIE GENERATOR ###################
class NgCardieGenerator(_BaseMentionPairSampleGenerator):
    """ Generator class implementing the Ng & Cardie 2002 sampling method.
    
    Arguments:
        pair_filter: a PairFilter instance, or None; if None, an 'AcceptAll' pair filter instance 
            will be used. Cf 'cortex.learning.filter.pairs'
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SAMPLE_TYPE: MentionPairSample.SAMPLE_TYPE
        
        pair_filter: instance of PairFilter subclass; if used to decide whether or not to produce a 
            ML sample for a given pair of mentions. Cf 'cortex.learning.filter.pairs'
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    
    Examples:
        >>> training_samples_collection = generator.generate_training_set(documents_iterable)
    
        or 
    
        >>> prediction_sample_collection = generator.generate_prediction_set(document)
    """
    
    def _generate_sample_set_from_mentions(self, mentions, sample_postprocessing_fct, 
                                           entity_head_extents=None, singleton_extents=None):
        """ Creates a generator over mention pair samples using Ng & Cardie 2002 sampling method
        
        Args:
            mentions: collection of mentions ordered from first to last using the order defined 
                on the Mention class instances
            sample_postprocessing_fct: callable, function to apply to the sample created by this 
                method (output will be ignored)
            entity_head_extents: set-like of mention extents (valid for the input document) whose 
                corresponding mention (for this document) are the head of their respective entity; used to 
                define the corresponding attribute of a MentionPairSample instance; or None
            singleton_extents: set-like of mention extents (valid for the input document) whose 
                corresponding mention (for this document) are singletons; used to define the corresponding 
                attribute of a MentionPairSample instance; or None

        Yields:
            MentionPairSample instance
        """
        # Left to right
        for j in range(1, len(mentions)): # We can begin at 1 since there is no pair to form with before anyway
            subsequent_mention = mentions[j]
            # Right to left
            for i in range(j - 1, -1, -1):
                antecedent_mention = mentions[i]
                if self.pair_filter.accept(antecedent_mention, subsequent_mention):
                    # Skip pronominal antecent mention for non-pronominal anaphora
                    if MENTION_FEATURES.is_expanded_pronoun(antecedent_mention) and not MENTION_FEATURES.is_expanded_pronoun(subsequent_mention):
                        continue
                    sample = self._generate_postprocessed_sample_from_mention_pair(antecedent_mention, subsequent_mention, 
                                                                     sample_postprocessing_fct, 
                                                                     entity_head_extents=entity_head_extents, 
                                                                     singleton_extents=singleton_extents)
                    yield sample
                    # Stop after closest antecedent
                    if sample.label == POS_CLASS:
                        break






################### STRUCTURED MENTION PAIR GENERATOR ####################
class _BaseExtendedMentionPairSampleGenerator(_BaseMentionPairGenerator):
    """ Base class for MentionPairSample instances generator, where the instance shall be made out 
    only of pairs of Mention instances, or of pairs made of one Mention instance, and the NULLMENTION.
    
    Arguments:
        pair_filter: a PairFilter instance, or None; if None, an 'AcceptAll' pair filter instance 
            will be used. Cf 'cortex.learning.filter.pairs'
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SAMPLE_TYPE: MentionPairSample.SAMPLE_TYPE
        
        pair_filter: instance of PairFilter subclass; if used to decide whether or not to produce a 
            ML sample for a given pair of mentions. Cf 'cortex.learning.filter.pairs'
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    SAMPLE_TYPE = MentionPairSample.SAMPLE_TYPE
    
    @staticmethod
    def _add_label_to_sample(sample, coreference_partition, entity_head_extents):
        """ Adds correct binary classification label to the input mention pair sample class instance, 
        using the info provided by the input coreference classification structure.
        
        It is assumed that both input are consistent with one another (come from the same document...).
        
        Args:
            mention_pair_sample: MentionPairSample instance
            coreference_partition: CoreferencePartition instance
            entity_head_extents: set-like of mention extents (valid for the input document) whose 
                corresponding mention (for this document) are the head of their respective entity; used to 
                define the corresponding attribute of a MentionPairSample instance
        """
        antecedent_mention, subsequent_mention = sample.pair
        if antecedent_mention is NULL_MENTION:
            if subsequent_mention.extent in entity_head_extents:
                sample.label = POS_CLASS
            else:
                sample.label = NEG_CLASS
        else:
            if coreference_partition.are_coreferent(subsequent_mention.extent, 
                                                    antecedent_mention.extent):
                sample.label = POS_CLASS
            else:
                sample.label = NEG_CLASS
    
    def generate_sample_from_mention_pair(self, antecedent_mention, subsequent_mention, train=True, 
                                          entity_head_extents=None, singleton_extents=None):
        """ Creates the sample corresponding to an input pair of mention.
        
        May correspond either to a training sample, or a prediction sample.
        Do not test whether or not the pair pass the filter associated to this generator instance.
        
        Args:
            antecedent_mention: Mention instance, the first of mention of the pair, introduced 
                before the other in the document's reading order
            subsequent_mention: Mention instance, the second of mention of the pair, introduced 
                after the other in the document's reading order
            train: boolean (default = True), whether or not to give a label to a generated 
                classification sample; if True, then a coreference partition must be defined for the input 
                document
            entity_head_extents: set-like of mention extents (valid for the input document) whose 
                corresponding mention (for this document) are the head of their respective entity; used to 
                define the corresponding attribute of a MentionPairSample instance
            singleton_extents: set-like of mention extents (valid for the input document) whose 
                corresponding mention (for this document) are singletons; used to define the corresponding 
                attribute of a MentionPairSample instance

        Returns:
            a MentionPairSample class instance
        """
        sample_postprocessing_fct = lambda sample: None
        if train:
            sample_postprocessing_fct = lambda sample: self._add_label_to_sample(sample, 
                                                                                 sample.document.coreference_partition, 
                                                                                 sample.document.coreference_partition.entity_head_extents)
        return self._generate_postprocessed_sample_from_mention_pair(antecedent_mention, subsequent_mention, 
                                                                     sample_postprocessing_fct, 
                                                                     entity_head_extents=entity_head_extents, 
                                                                     singleton_extents=singleton_extents)
    
    def _generate_sample_set(self, document, train=False, entity_head_extents=None, singleton_extents=None):
        """ Returns a mention pair sample class instances iterator, using the generator's own sampling 
        method. 
        
        Args:
            document: Document instance; must possess a collection of mentions
            train: boolean (default = True), whether or not to give a label to a generated 
                classification sample; if True, then a coreference partition must be defined for the input 
                document
            entity_head_extents: set-like of mention extents (valid for the input document) whose 
                corresponding mention (for this document) are the head of their respective entity; used to 
                define the corresponding attribute of a MentionPairSample instance
            singleton_extents: set-like of mention extents (valid for the input document) whose 
                corresponding mention (for this document) are singletons; used to define the corresponding 
                attribute of a MentionPairSample instance

        Returns:
            an iterator over MentionPairSample instances
        

        Warning:
            If 'train' is True, the 'coreference_partition' attribute of the input Document instance 
            must indeed be a CoreferencePartition Instance, and not None
        """
        # Get mention collection used to carry out samples generation
        mentions = (NULL_MENTION,) + document.mentions  # (None, mention) pairs use anaphoricity features
        sample_postprocessing_fct = lambda sample: None
        if train:
            coreference_partition = document.coreference_partition
            true_entity_head_extents = coreference_partition.entity_head_extents
            sample_postprocessing_fct = lambda sample: self._add_label_to_sample(sample, coreference_partition, true_entity_head_extents)
        # Carry out the generation
        return self._generate_sample_set_from_mentions(mentions, sample_postprocessing_fct, 
                                           entity_head_extents=entity_head_extents, 
                                           singleton_extents=singleton_extents)


class ExtendedMentionPairSampleGenerator(_BaseExtendedMentionPairSampleGenerator):
    """ Default mention pair generator
    
    Creates a MentionPairSample instance for every distinct pair of mentions, where the first mention 
    of the pair must come first in the document, according to the reading order defined on the document
    NULLMENTION is added at the beginning of the text.
    
    Arguments:
        pair_filter: a PairFilter instance, or None; if None, an 'AcceptAll' pair filter instance 
            will be used. Cf 'cortex.learning.filter.pairs'
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        SAMPLE_TYPE: MentionPairSample.SAMPLE_TYPE
        
        pair_filter: instance of PairFilter subclass; if used to decide whether or not to produce a 
            ML sample for a given pair of mentions. Cf 'cortex.learning.filter.pairs'
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    
    Examples:
        >>> training_samples_collection = generator.generate_training_set(documents_iterable)
    
        or 
    
        >>> prediction_sample_collection = generator.generate_prediction_set(document)
    """
    
    def _generate_sample_set_from_mentions(self, mentions, sample_postprocessing_fct, 
                                           entity_head_extents=None, singleton_extents=None):
        """ Creates a generator over every possible mention pair samples from the input collection 
        of mentions.
        
        Args:
            mentions: collection of mentions ordered from first to last using the order defined 
                on the Mention class instances
            sample_postprocessing_fct: callable, function to apply to the sample created by this 
                method (output will be ignored)
            entity_head_extents: set-like of mention extents (valid for the input document) whose 
                corresponding mention (for this document) are the head of their respective entity; used to 
                define the corresponding attribute of a MentionPairSample instance
            singleton_extents: set-like of mention extents (valid for the input document) whose 
                corresponding mention (for this document) are singletons; used to define the corresponding 
                attribute of a MentionPairSample instance

        Yields:
            MentionPairSample instance
        """
        # Left to right
        for j in range(1, len(mentions)): # 1 instead of 0 because we added NULL_MENTION at the beginning of the collection.
            subsequent_mention = mentions[j]
            # Right to left
            for i in range(j-1, -1, -1):
                antecedent_mention = mentions[i]
                if self.pair_filter.accept(antecedent_mention, subsequent_mention):
                    sample = self._generate_postprocessed_sample_from_mention_pair(antecedent_mention, subsequent_mention, 
                                                                     sample_postprocessing_fct, 
                                                                     entity_head_extents=entity_head_extents, 
                                                                     singleton_extents=singleton_extents)
                    yield sample
