# -*- coding: utf-8 -*-

"""
Defines classes used to create, train and use coreference resolution models
Instance of such classes will be referred to as 'coreference resolver', or 'resolver' for short, 
when there is no risk of confusion.

The role of resolvers is to predict a coreference partition for a given document that possess mentions.

Resolver classes inherit from the 'ConfigurationmixIn' class, and as such their instances can be 
characterized by their configuration / an instance of those class can be created from such a configuration.
Cf 'cortex.tools.configuration_mixin'.
"""

"""
Available submodules
--------------------
base
    Defines base resolver class

pairs
    Defines resolver classes that work by treating the coreference partition prediction problem as a 
    binary classification problem on pairs of mentions 

structured_pairs
    Defines resolver classes that work by treating the coreference partition prediction problem as a 
    graph construction problem, where mentions are nodes and coreference links are edges

rules
    Defines resolver classes based on the usage of a set of deterministic rules

constraints
    Defines a MixIn class used by resolver that supports the integration of external knowledges about 
    Must-Link or Cannot_link mention pairs

API:
----
create_resolver
    Function used to create a Resolver instance, from specifying the name of the Resolver class, and 
    potentially some initialization parameters

generate_resolver_factory_configuration
    Function used to create a specific Resolver class instance's factory configuration, from specifying 
    the name of the Resolver class, and potentially some initialization parameters

create_resolver_from_factory_configuration
    Function used to create a Resolver instance, from specifying its factory configuration

load_resolver
    Function used to instantiate a specific Resolver instance from the instantiation data located in 
    an archive file 
"""

__all__ = ["create_resolver", 
           "create_resolver_from_factory_configuration", 
           "load_resolver", 
           "generate_resolver_factory_configuration", 
           "MentionPairSampleResolver", 
           "SoonResolver", 
           "McCarthyLehnertResolver", 
           "NgCardieResolver", 
           "HACResolver", 
           "OraclePairResolver", 
           "OraclePairScoreResolver", 
           "MSTResolver", 
           "HybridBestFirstMSTResolver", 
           "ConstrainedMSTResolver", 
           "ExtendedMSTResolver", 
           "TransitiveClosureStructureResolver", 
           "PositiveGraphStructureResolver", 
           "ClosestFirstDocumentStructuredResolver", 
           "ExtendedClosestFirstDocumentStructuredResolver", 
           "BestFirstDocumentStructuredResolver", 
           "ExtendedBestFirstDocumentStructuredResolver", 
           "ConstrainedBestFirstDocumentStructuredResolver", 
           "ConstrainedExtendedBestFirstDocumentStructuredResolver", 
           ]


import os
import logging
from collections import OrderedDict

from .base import _CoreferenceResolver, _TrainableResolver
from .pairs import (SoonResolver, 
                    McCarthyLehnertResolver, 
                    NgCardieResolver, 
                    HACResolver, 
                    OraclePairResolver, OraclePairScoreResolver)
from .structured_pairs import (MSTResolver, 
                              HybridBestFirstMSTResolver, 
                              ConstrainedMSTResolver, 
                              ExtendedMSTResolver, 
                              TransitiveClosureStructureResolver, 
                              PositiveGraphStructureResolver, 
                              ClosestFirstDocumentStructuredResolver,
                              ExtendedClosestFirstDocumentStructuredResolver,
                              BestFirstDocumentStructuredResolver,
                              ExtendedBestFirstDocumentStructuredResolver,
                              ConstrainedBestFirstDocumentStructuredResolver,
                              ConstrainedExtendedBestFirstDocumentStructuredResolver,
                              )

from .rules import RulesResolver
from cortex.tools import (define_check_item_name_function, define_create_function, 
                          define_generate_factory_configuration_function, 
                          define_create_from_factory_configuration_function,
                          )
from cortex.tools import load_mapping
from cortex.utils.io import TemporaryDirectory, unzip_archive_to_folder


resolver_names_collection_and_resolver_class_pairs = ((("soon", SoonResolver.__name__), SoonResolver),
                                                      (("mccarthylehnert", McCarthyLehnertResolver.__name__), McCarthyLehnertResolver), 
                                                      (("ngcardie", NgCardieResolver.__name__), NgCardieResolver), 
                                                      (("hac", HACResolver.__name__), HACResolver), 
                                                      (("oraclepair", OraclePairResolver.__name__), OraclePairResolver),
                                                      (("oraclepairscore", OraclePairScoreResolver.__name__), OraclePairScoreResolver),
                                                      (("rules", RulesResolver.__name__), RulesResolver),
                                                      (("mst", MSTResolver.__name__), MSTResolver), 
                                                      (("hybridbestfirstmst", HybridBestFirstMSTResolver.__name__), HybridBestFirstMSTResolver), 
                                                      (("constrainedmst", ConstrainedMSTResolver.__name__), ConstrainedMSTResolver), 
                                                      (("extendedmst", ExtendedMSTResolver.__name__), ExtendedMSTResolver), 
                                                      (("transitiveclosurestructure", TransitiveClosureStructureResolver.__name__), TransitiveClosureStructureResolver), 
                                                      (("positivegraphstructure", PositiveGraphStructureResolver.__name__), PositiveGraphStructureResolver), 
                                                      (("closestfirstdocumentstructured", ClosestFirstDocumentStructuredResolver.__name__), ClosestFirstDocumentStructuredResolver), 
                                                      (("extendedclosestfirstdocumentstructured", ExtendedClosestFirstDocumentStructuredResolver.__name__), ExtendedClosestFirstDocumentStructuredResolver), 
                                                      (("bestfirstdocumentstructured", BestFirstDocumentStructuredResolver.__name__), BestFirstDocumentStructuredResolver), 
                                                      (("extendedbestfirstdocumentstructured", ExtendedBestFirstDocumentStructuredResolver.__name__), ExtendedBestFirstDocumentStructuredResolver), 
                                                      (("constrainedbestfirstdocumentstructured", ConstrainedBestFirstDocumentStructuredResolver.__name__), ConstrainedBestFirstDocumentStructuredResolver), 
                                                      (("constrainedextendedbestfirstdocumentstructured", ConstrainedExtendedBestFirstDocumentStructuredResolver.__name__), ConstrainedExtendedBestFirstDocumentStructuredResolver), 
                                                      #(("", .__name__), ), 
                                                      )
resolver_name2resolver_class = OrderedDict((name, class_) for names, class_ in resolver_names_collection_and_resolver_class_pairs for name in names)

# High level functions
_check_resolver_name = define_check_item_name_function(resolver_name2resolver_class, "resolver")

create_resolver = define_create_function(resolver_name2resolver_class, "resolver",)

create_resolver_from_factory_configuration =\
 define_create_from_factory_configuration_function(resolver_name2resolver_class, "resolver")

generate_resolver_factory_configuration =\
 define_generate_factory_configuration_function(resolver_name2resolver_class, "resolver")


def load_resolver(archive_file_path):
    """ Instantiates a Resolver class from the instantiation data located at the specified archive 
    file path .
    
    Args:
        archive_file_path: string, path to the archive file containing the needed data

    Returns:
        a Resolver class instance created from the instantiation data found in the speicided archive file
    """
    logger = logging.getLogger("load_resolver")
    logger.info("Loading resolver located at '{}'...".format(archive_file_path))
    with TemporaryDirectory(suffix="_tmp_load_resolver") as temp_dir:
        folder_path = temp_dir.temporary_folder_path
        # Unzip the archive into the temporary folder
        unzip_archive_to_folder(archive_file_path, folder_path)
        # Read the configuration from the configuration file
        config_file_path = os.path.join(folder_path, _CoreferenceResolver._CONFIGURATION_PERSISTENCE_FILE_NAME)
        factory_configuration = load_mapping(config_file_path)
        resolver = create_resolver_from_factory_configuration(factory_configuration)
        if isinstance(resolver, _TrainableResolver):
            model_interface_folder_path = os.path.join(folder_path, _TrainableResolver._MODEL_INTERFACE_PERSISTENCE_FOLDER_NAME)
            resolver.load_model(model_interface_folder_path)
    logger.info("Finished loading resolver located at '{}'.".format(archive_file_path))
    return resolver
