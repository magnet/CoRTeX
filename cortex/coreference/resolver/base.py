# -*- coding: utf-8 -*-

"""
Defines base resolver class
"""

__all__ = ["_CoreferenceResolver", 
           "_TrainableResolver",
           ]

import abc
import logging
import os
from collections import OrderedDict

from cortex.learning.model_interface import create_model_interface_from_factory_configuration
from ..generator import create_generator_from_factory_configuration
from ..decoder import create_decoder_from_factory_configuration
from cortex.tools import ConfigurationMixIn, _create_simple_get_parameter_value_from_configuration, save_mapping
from cortex.utils import EqualityMixIn
from cortex.api.document import DocumentDoesNotPossessMentionsError
from cortex.utils.io import TemporaryDirectory, zip_folder_to_archive



class _CoreferenceResolver(ConfigurationMixIn, EqualityMixIn, metaclass=abc.ABCMeta):
    """ Coreference resolver base class
    
    The info needed to instantiate a CoreferenceResolver can be saved in an archive using the 'save' 
    method.
    To instantiate a CoreferenceResolver whose data has been persisted as such, use the 'load_resolver' 
    function, cf 'cortex.coreference.resolver'.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        _CONFIGURATION_PERSISTENCE_FILE_NAME: string, define the name of the file in which 
            the configuration defining a resolver class instance will be persisted, when using the 'save' 
            method
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    
    Examples:
        >>> predicted_coreference_partition = resolver.resolve(document)
        >>> resolver.save(folder_path, archive_name)
    """
    
    _CONFIGURATION_PERSISTENCE_FILE_NAME = "configuration.txt"
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = ("configuration",) +\
                                                EqualityMixIn._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    
    def __init__(self, configuration=None):
        # Initialize using parent class
        ConfigurationMixIn.__init__(self, configuration=configuration)
        # Assign attribute values
        self.verbose = False
        self._logger = logging.getLogger(self.NAME)
    
    # Instance methods
    @abc.abstractmethod
    def resolve(self, document):
        """ Predicts a coreference partition for the input document.
        
        Args:
            document: a Document instance

        Returns:
            a CoreferencePartition instance
        """
    
    def save(self, folder_path, archive_name):
        """ Saves the resolver configuration data in an archive located in the specified folder.
        
        Args:
            folder_path: string, path to folder where the archive will be created
            archive_name: string, name of the archive file to create; if it does not end with 
                '.zip', this suffix will be added

        Returns:
            string, the path to the created archive file
        """
        archive_file_path = os.path.join(folder_path, archive_name)
        self._logger.info("Saving resolver at '{}'...".format(archive_file_path))
        with TemporaryDirectory(temporary_root_folder_path=folder_path, 
                                suffix="_tmp_save_resolver") as temp_dir:
            temp_folder_path = temp_dir.temporary_folder_path
            # Save factory configuration needed to build this resolver instance
            config_file_path = os.path.join(temp_folder_path, self._CONFIGURATION_PERSISTENCE_FILE_NAME)
            factory_configuration = self.factory_configuration
            save_mapping(factory_configuration, config_file_path)
            # Create a '.zip' archive file out of this
            archive_file_path = zip_folder_to_archive(temp_folder_path, archive_file_path)
        self._logger.info("Finished saving resolver at '{}'.".format(archive_file_path))
        return archive_file_path
    
    def _preprocessing_resolution(self, document):
        """ Checks that the input document is suitable for coreference partition prediction.
        Here, the condition is that a collection of mentions must have been defined for the document.
        
        Args:
            document: Document instance
        
        Raises:
            DocumentDoesNotPossessMentionsError: if the input Document instance's 'mentions' attribute 
                value is None.
        """
        # Check that mentions do exist for the input Document instance
        if document.mentions is None:
            message = "Input document '{}' does not possess mentions, impossible to apply 'resolve' method."
            message = message.format(document)
            raise DocumentDoesNotPossessMentionsError(message)



model_interface_attribute_data = ("_model_interface", 
                                 (_create_simple_get_parameter_value_from_configuration("model_interface", 
                                                                                        postprocess_fct=create_model_interface_from_factory_configuration),
                                  lambda instance: setattr(instance.configuration, "model_interface", instance._model_interface.factory_configuration)
                                  )
                                 )
generator_attribute_data = ("_generator", 
                           (_create_simple_get_parameter_value_from_configuration("generator", 
                                                                                  postprocess_fct=create_generator_from_factory_configuration),
                            lambda instance: setattr(instance.configuration, "generator", instance._generator.factory_configuration)
                            )
                           )
decoder_attribute_data = ("_decoder",
                         (_create_simple_get_parameter_value_from_configuration("decoder", 
                                                                                postprocess_fct=create_decoder_from_factory_configuration),
                          lambda instance: setattr(instance.configuration, "decoder", instance._decoder.factory_configuration)
                          )
                         )
class _TrainableResolver(_CoreferenceResolver):
    """ General class of Resolver which interpret the coreference resolution problem as 
    a machine-learning task (ex: classification task).
    
    Implements a 'resolve' method that uses the combination of use of the following resolver 
    components in order to predict a coreference partition for a given document:
    - a "generator", whose role is to convert a Document instance into a set of machine-learning samples 
        (in the frame of the coreference resolution problem being interpreted as a machine-learning task) 
    - a "model interface", whose role is to be able to learn to score machine-learning samples with 
        regards to the machine-learning task at hand
    - a "decoder", whose role is to interpret back the scores, predicted for the machine-learning 
        samples of a set of such samples produced for a document, into a coreference partition
    
    Can train its underlying machine learning model using the 'train' method.
    
    Can have its model interface component be coupled with a _BaseVectorizationCache child class 
    instance for the training and prediction phases, using the 'toggle_vectorization_cache' method.
    
    Arguments:
        model_interface: a _ModelInterface child class instance, representing the interface to the 
            underlying machine learning model to train and to use
        
        generator: a _Generator child class instance, used to generate, from document(s), 
            machine-learning samples used as input by the underlying machine learning model
        
        decoder: a _CoreferenceDecoder child class instance, used to interpret scores associated to 
            machine-learning samples into a coreference partition, for the document used to generated 
            the scored machine-learning samples
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        sample_type: string specifying the type of the sample used in the frame of the underlying 
            machine-learning task
        
        document_hash2cache: a "document hash => cache" map, or None; the current value of the 
            underlying model interface's vectorization cache (cf 'cortex.coreference.api.vectorization_cache.py')
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    
    Examples:
        >>> trainable_resolver.toggle_vectorization_cache(vectorization_cache=vectorization_cache) # Couple the resolver with the cache
        >>> trainable_resolver.train(documents)
        >>> predicted_coreference_partition = trainable_resolver.resolve(document)
        >>> trainable_resolver.toggle_vectorization_cache(vectorization_cache=None) # After using the resolver, uncouple it from cache
        >>> trainable_resolver.save(folder_path, archive_name)
    """
    
    __INIT_PARAMETERS = OrderedDict((model_interface_attribute_data, generator_attribute_data, decoder_attribute_data))
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple(__INIT_PARAMETERS.keys()) +\
                                                _CoreferenceResolver._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = _CoreferenceResolver._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION+\
                                            tuple(__INIT_PARAMETERS.keys())
    _MODEL_INTERFACE_PERSISTENCE_FOLDER_NAME = "model_interface_data"
    
    def __init__(self, model_interface, generator, decoder, configuration=None):
        # Check input values
        self._check_subcomponent(model_interface, generator, decoder)
        # Initialize using parent class
        super().__init__(configuration=configuration)
        # Assign attribute values
        self._model_interface = model_interface
        self._generator = generator
        self._decoder = decoder
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        model_interface, generator, decoder = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        args = args + (model_interface, generator, decoder)
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, model_interface_factory_configuration, 
                             generator_factory_configuration, 
                             decoder_factory_configuration):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class.
        
        Args:
            model_interface_factory_configuration: a factory configuration that can be used to 
                create a specific ModelInterface instance
            generator_factory_configuration: a factory configuration that can be used to create 
                a specific Generator instance
            decoder_factory_configuration: a factory configuration that can be used to create 
                a specific Decoder instance

        Returns:
            a Mapping instance
        """
        configuration = super().define_configuration()
        configuration["generator"] = generator_factory_configuration
        configuration["model_interface"] = model_interface_factory_configuration
        configuration["decoder"] = decoder_factory_configuration
        return configuration
    
    @property
    def sample_type(self):
        return self._generator.SAMPLE_TYPE
    @sample_type.setter
    def sample_type(self, value):
        raise AttributeError
    
    @property
    def document_hash2cache(self):
        """ Returns the value of underlying model interface's 'document hash => vectorization cache' """
        return self._model_interface.document_hash2cache
    @document_hash2cache.setter
    def document_hash2cache(self, value):
        """ Sets the value of underlying model interface's 'document hash => vectorization cache' """
        self._model_interface.document_hash2cache = value
    
    # Instance methods
    def toggle_vectorization_cache(self, vectorization_cache=None):
        """ Couples the underlying model interface with the input vectorization cache, or uncouples it.
        
        Args:
            vectorization_cache: a _BaseVectorizationCache child class instance, or None:
                - if a _BaseVectorizationCache child class, couples it with the underlying model interface
                - if None, removes the coupling of the underlying model interface with a potentially previously 
                  coupled vectorization cache
        """
        document_hash2cache = None if vectorization_cache is None else vectorization_cache.document_hash2cache
        return self._model_interface.toggle_memoization(document_hash2cache=document_hash2cache)
    
    def load_model(self, folder_path):
        """ Loads the underlying machine learning model state located in the specified folder, so as 
        to be able to carry out predictions using this state.
        
        Args:
            folder_path: string, path to folder where the underlying machine learning model data 
                is located
        """
        self._model_interface.load_model(folder_path)
    
    def save(self, folder_path, archive_name):
        """ Saves the resolver instance state in an archive located in the specified folder.
        
        Args:
            folder_path: string, path to folder where the archive will be created
            archive_name: string, name of the archive file to create; if it does not end with 
                '.zip', this suffix will be added

        Returns:
            a string, the path to the created archive file
        """
        archive_file_path = os.path.join(folder_path, archive_name)
        self._logger.info("Saving resolver at '{}'...".format(archive_file_path))
        with TemporaryDirectory(temporary_root_folder_path=folder_path, 
                                suffix="_tmp_save_resolver") as temp_dir:
            temp_folder_path = temp_dir.temporary_folder_path
            # Save model_interface data
            model_interface_folder_path = os.path.join(temp_folder_path, self._MODEL_INTERFACE_PERSISTENCE_FOLDER_NAME)
            os.mkdir(model_interface_folder_path)
            self._model_interface.save_model(model_interface_folder_path)
            # Save configuration needed to build this resolver instance
            config_file_path = os.path.join(temp_folder_path, self._CONFIGURATION_PERSISTENCE_FILE_NAME)
            factory_configuration = self.factory_configuration
            save_mapping(factory_configuration, config_file_path)
            # Create a '.zip' archive file out of this
            archive_file_path = zip_folder_to_archive(temp_folder_path, archive_file_path)
        self._logger.info("Finished saving resolver at '{}'.".format(archive_file_path))
        return archive_file_path
    
    @abc.abstractmethod
    def train(self, documents):
        """ Trains the underlying machine learning model.
        
        Args:
            documents: an iterable over Document instances possessing mentions and a coreference 
                partition
        """
        pass
    
    @abc.abstractmethod
    def _check_subcomponent(self, model_interface, generator, decoder):
        """ Checks that the input model interface, generator, and decoder are correct; that is, 
        that they can be used together.
        
        Raises an exception if the check fails.
        """
        pass
