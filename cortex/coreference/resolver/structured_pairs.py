# -*- coding: utf-8 -*-

"""
Define resolver classes that work by treating the coreference partition prediction problem as a 
graph construction problem, where mentions are nodes and coreference links are edges.

Most resolvers implemented here consider the coreference prediction problem as a '(latent) structure 
on the graph of mention pairs' inference problem, and they use the algorithmic architecture described 
in the following paper:
'Lassalle, E., and Denis, P.  2015.  "Joint Anaphoricity Detection and Coreference Resolution 
with Constrained Latent Structures"' (http://researchers.lille.inria.fr/~pdenis/papers/aaai15.pdf)

Such resolvers seek to predict a graph structure on the mentions of a document from scores 
given to the pairs of mentions built from the mentions of this document; then decode the scores 
associated to its edges to build a predicted coreference partition.
To create its graph structure, it may or may not incorporate edges constraints, computed using 
potentially external knowledge.

For the training phase, for a given document, such resolvers use their inner ML model to compute 
the scores associated to pairs of mentions, then use them to compute a 'true' graph structure, 
built in a way similar to a predicted graph, but along with the knowledge of the true coreference 
partition associated to the document; then they compute a predicted graph structure, compare the 
two by computing an associated loss value; and finally, they use this loss value, along with the 
definition of the edges making the two graph structures, to update their respective underlying ML 
model. As such, they needs to use ML models that support online training.

Here, a graph is such that a node is a mention (either a 'true' mention, or the NULL_MENTION), and 
an edge corresponds to a pair of mention. A mention being identified by its extent, in the frame of 
the document its originates from, then an edge will be represented as a pair of mention extents:
Ex:
edge = (extent1, extent2), where 'extent1' is the extent of the antecedent mention of the pair, and 
'extent2' is the extent of the subsequent mention of the pair.
"""

__all__ = ["MSTResolver", 
           "HybridBestFirstMSTResolver", 
           "ConstrainedMSTResolver", 
           "ExtendedMSTResolver", 
           "TransitiveClosureStructureResolver", 
           "PositiveGraphStructureResolver", 
           "ClosestFirstDocumentStructuredResolver", 
           "ExtendedClosestFirstDocumentStructuredResolver", 
           "BestFirstDocumentStructuredResolver", 
           "ExtendedBestFirstDocumentStructuredResolver", 
           "ConstrainedBestFirstDocumentStructuredResolver", 
           "ConstrainedExtendedBestFirstDocumentStructuredResolver",
           "_DocumentStructuredPairResolver",
           "_TreeLossDocumentStructuredPairResolver",
           "_UpdateModeTreeLossDocumentStructuredPairsResolver",
           "_BaseMSTResolver",
           "_BaseMentionPairMSTResolver",
           "_BaseClosestFirstDocumentStructuredResolver",
           "_BaseBestFirstDocumentStructuredResolver",
           "_BaseConstrainedBestFirstDocumentStructuredResolver",
           ]

import abc
import numpy
import itertools
from collections import OrderedDict

from cortex.api.markable import NULL_MENTION, get_mention_pair_extent_pair
from cortex.api.coreference_partition import CoreferencePartition
from ..generator.pairs import MentionPairSampleGenerator, ExtendedMentionPairSampleGenerator
from ..decoder.mst import (MSTPrimCoreferenceDecoder, HybridBestFirstMSTCoreferenceDecoder, 
                           MSTKruskalCoreferenceDecoder)
from ..decoder.pairs import (AggressiveMergeCoreferenceDecoder, ClosestFirstCoreferenceDecoder, 
                             BestFirstCoreferenceDecoder, ExtendedBestFirstCoreferenceDecoder, 
                             ConstrainedBestFirstCoreferenceDecoder, 
                             ConstrainedExtendedBestFirstCoreferenceDecoder, 
                             )
from .base import _TrainableResolver
from .constraints import ConstrainedEdgesDefinerMixIn
from cortex.learning.sample_features_vectorizer.pairs import (ExtendedMentionPairSampleFeaturesVectorizer,
                                                              MentionPairSampleFeaturesVectorizer, 
                                                              SuperExtendedMentionPairSampleFeaturesVectorizer)
from cortex.learning.ml_model.classifier.online_binary_classifier import PerceptronOnlineBinaryClassifier
from cortex.learning.model_interface import OnlineInterface
from cortex.learning.samples import POS_CLASS
from cortex.learning.filter.pairs import AcceptAllPairFilter
from cortex.tools import (_create_simple_set_parameter_value_in_configuration, 
                            _create_simple_get_parameter_value_from_configuration, 
                            )
from cortex.tools.mst import prim_mst, prim_msf, kruskal_mst, kruskal_msf
from cortex.utils import _check_isinstance, get_unique_elts
from cortex.utils.display import get_display_progress_function

from cortex.config import language
from cortex.languages import create_get_language_dependent_object_fct
get_language_dependent_object = create_get_language_dependent_object_fct(language)

MENTION_FEATURES = get_language_dependent_object("mention_features")



class _DocumentStructuredPairResolver(_TrainableResolver):
    """ Document structured pair resolver
    
    This class represents a trainable resolver that considers the coreference prediction problem as 
    a '(latent) structure on the graph of mention pairs' inference problem.
    It is the base class for implementing resolver following this architecture, which is described 
    in the following paper:
    'Lassalle, E., and Denis, P.  2015.  "Joint Anaphoricity Detection and Coreference Resolution 
    with Constrained Latent Structures"' (http://researchers.lille.inria.fr/~pdenis/papers/aaai15.pdf)
    
    Such resolvers seek to predict a graph structure on the mentions of a document from scores 
    given to the pairs of mentions built from the mentions of this document; then decode the scores 
    associated to its edges to build a predicted coreference partition.
    To create its graph structure, it may or may not incorporate edges constraints, computed using 
    potentially external knowledge.
    
    For the training phase, for a given document, such resolvers use their inner ML model to compute 
    the scores associated to pairs of mentions, then use them to compute a 'true' graph structure, 
    built in a way similar to a predicted graph, but along with the knowledge of the true coreference 
    partition associated to the document; then they compute a predicted graph structure, compare the 
    two by computing an associated loss value; and finally, they use this loss value, along with the 
    definition of the edges making the two graph structures, to update their respective underlying ML 
    model. As such, they needs to use ML models that support online training.
    
    As a consequence, the classes inheriting this one need to implement notably the following methods:
    _compute_prediction_phase_extended_mention_pair_scores: 
        how to compute scores associated to pairs of mentions, and for which pairs of mentions, when 
        the resolver only have to carry out a prediction
    
    _compute_training_phase_extended_mention_pair_scores: 
        how to compute scores associated to pairs of mentions, and for which pairs of mentions, when 
        the resolver have to carry out a training phase
    
    _compute_ground_truth_graph_edges:
        how to computes the 'true' graph structure from scores associated to pairs of mentions
    
    _compute_predicted_graph_edges:
        how to computes the 'predicted' graph structure from scores associated to pairs of mentions
    
    _compute_graph_loss:
        how to computes the loss value resulting from comparing the 'true' and the 'predicted' 
        graph structures
    
    _generate_edge_samples:
        how to create collection of MentionPairSample instances corresponding to pairs of mentions made 
        from mentions coming from the input document, pairs that are specified by input edges collection.
    
    As such, can be instantiated only with a _OnlineBinaryClassifier child class instance, a 
    _BaseMentionPairGenerator child class instance and a __BaseMentionPairCoreferenceDecoder child 
    class instance.
    
    Arguments:
        model_interface: a _OnlineBinaryClassifier child class instance, representing the interface 
            to the underlying online machine learning model to train and to use
        
        generator: a _BaseMentionPairGenerator child class instance, used to generate, from document(s), 
            machine-learning samples used as input by the underlying machine learning model
        
        decoder: a __BaseMentionPairCoreferenceDecoder child class instance, used to interpret 
            scores associated to machine-learning samples into a coreference partition, for the document 
            used to generate the scored machine-learning samples
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        sample_type: string specifying the type of the sample used in the frame of the underlying 
            machine-learning task
        
        document_hash2cache: document_hash2cache: a "document hash => cache" map, or None; the 
            current value of the underlying model interface's vectorization cache (cf 
            'cortex.coreference.api.vectorization_cache.py')
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    @abc.abstractmethod
    def _check_subcomponent(self, model_interface, generator, decoder):
        """ Checks that the input model interface, generator, and decoder are correct; that is: 
        - the generator is a _OnlineBinaryClassifier child class instance
        
        Raises:
            ValueError: if one input is not of the expected type
        """
        _check_isinstance(OnlineInterface, model_interface, "model_interface")
        super()._check_subcomponent(model_interface, generator, decoder)
    
    @classmethod
    def define_configuration(cls, generator_factory_configuration, decoder_factory_configuration, 
                             sample_features_vectorizer_factory_configuration, 
                             classifier_factory_configuration=None):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class.
        
        Args:
            generator_factory_configuration: a factory configuration that can be used to create 
                a specific _Generator child class instance
            decoder_factory_configuration: a factory configuration that can be used to create 
                a specific _CoreferenceDecoder child class instance
            sample_features_vectorizer_factory_configuration: a factory configuration that can be 
                used to create a specific _BaseExtendedMentionPairSampleFeaturesVectorizer child class instance
            classifier_factory_configuration: a factory configuration that can be used to 
                create a specific _OnlineBinaryClassifier child class instance; or None, in which case the 
                factory configuration for a 'PerceptronOnlineBinaryClassifier' instance (parametrized with 
                'avg=True, warm_start=False') will be used

        Returns:
            a Mapping instance
        """
        # Check and process inputs
        if classifier_factory_configuration is None:
            classifier_factory_configuration =\
             PerceptronOnlineBinaryClassifier.define_factory_configuration("TOKEN_POS_CLASS", "TOKEN_NEG_CLASS", 
                                                                          avg=True, warm_start=False)
            
        model_interface_factory_configuration =\
         OnlineInterface.define_factory_configuration(sample_features_vectorizer_factory_configuration, 
                                                      classifier_factory_configuration)
        return super().define_configuration(model_interface_factory_configuration, 
                                               generator_factory_configuration, 
                                               decoder_factory_configuration)
    
    # Instance methods
    def train(self, documents): #use_entity_heads=False, use_singletons=False:
        """ Trains the underlying binary classification machine learning model.
        
        Args:
            documents: collection of Document instances
        
        Warning:
            If the underlying model interface has already learnt a model, it will be reset and learnt 
            anew (unless the model interface possess the ability to continue the training of its model 
            with new data, and if this is its default behavior when calling its 'learn' method).
        """
        iteration_nb = self._model_interface.iteration_nb
        document_nb = len(documents)
        #iteration_display_progress_function = lambda i: None
        #document_display_progress_function = lambda i: None
        #if self.verbose:
        percentage_interval = 1
        logging_level = "DEBUG"
        message = "Training progress = {percentage:6.2f}% (Processed iteration(s) = {current:d} / {total:d})"
        iteration_display_progress_function = get_display_progress_function(self._logger, 
                                                                            maximum_count=iteration_nb, 
                                                                            percentage_interval=percentage_interval,
                                                                            personalized_progress_message=message, 
                                                                            logging_level=logging_level)
        
        percentage_interval = 5
        logging_level = "DEBUG"
        message = "Processed documents progress = {percentage:6.2f}% (Processed document(s) = {current:d} / {total:d})"
        document_display_progress_function = get_display_progress_function(self._logger, 
                                                                           maximum_count=document_nb, 
                                                                           percentage_interval=percentage_interval,
                                                                           personalized_progress_message=message, 
                                                                           logging_level=logging_level)
            
        self._model_interface.train_mode = True # For instance: for linear classifier, set 'use_avg' to False, otherwise average weights will be used for inference during learning
        self._model_interface.initialize_model()
        for i in range(iteration_nb):
            iteration_display_progress_function(i)
            for j, document in enumerate(documents):
                document_display_progress_function(j)
                if not document.mentions: # FIXME: because some documents may not have any mention defined:/
                    continue
                self.train_document(document) #, use_entity_heads=use_entity_heads, use_singletons=use_singletons
        self._model_interface.train_mode = False
    
    
    def train_document(self, document): #use_entity_heads=False, use_singletons=False
        """ Carries out an atomic online training phase.
        
        That is, a prediction - adjustement phase, using one document as support. 
        
        Args:
            document: a Document instance
        
        Warning:
            Assume that the model interface model has been initialized.
            Assume that the input document indeed possess a CoreferencePartition instance.
        """
        self._logger.debug("Training using document '{}'...".format(document))
        
        # Compute "ground truth structure edges, predicted structure edges, and loss resulting from comparing the two" from the current state of the model 
        ## Entity heads
        #entity_head_extents = None
        #if use_entity_heads:
        #    entity_head_extents = document.coreference_partition.entity_head_extents
        ## Singletons
        #singleton_extents = None
        #if use_singletons:
        #    singletons = document.coreference_partition.singleton_extents
        self._logger.debug("Getting update model data...")
        true_edge_ML_samples, predicted_edge_ML_samples, loss = self._carry_out_training_prediction_step(document)
        self._logger.debug("Finished getting update model data.")
        
        # Update the model
        self._logger.debug("Updating model...")
        positive_class_samples = true_edge_ML_samples
        negative_class_samples = predicted_edge_ML_samples
        self._model_interface.learn_sum(positive_class_samples, negative_class_samples, loss) # FIXME (legacy) commented out code: learn_sum(pos_list=true_edge_ML_samples, neg_list=predicted_edge_ML_samples, loss=loss, rate=1./float(len(mentions)+1))
        self._logger.debug("Finished updating model.")
    
    
    def _carry_out_training_prediction_step(self, document): #, entity_head_extents=entity_head_extents, singleton_extents=singleton_extents
        """ Carries out the "prediction" part of a "prediction - correction" step of the training 
        process, using one document of the training as support.
        
        That means: 
        
        1) Uses the underlying model to compute score attributed to potential graph edges, where 
           nodes are mentions of the input document.
        2) Creates a virtual ground truth structure (a graph, a tree...) from the potential edges, 
           using the ground truth coreference partition associated to the input document.
        3) Creates a virtual predicted structure from the potential edges, possibly aiming that the 
           loss value resulting from the comparison of the ground truth structure with the predicted 
           structure result in a loss value that be as informative as possible, in the frame of wanting 
           to improve the underlying ML model.
        4) Compute the loss value resulting from the comparison of the ground truth structure and the 
           predicted structure. 
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance

        Returns:
            a (ground_truth_graph_edge_ML_samples, predicted_graph_edge_ML_samples, loss) triplet, 
            where:
                - 'ground_truth_graph_edge_ML_samples' is
                - 'predicted_graph_edge_ML_samples' is 
                - 'loss' is the loss value, a float (may be positive or negative)
        """
        # Compute pairwise scores
        self._logger.debug("Computing training pair scores...")
        extended_edge2score, true_extended_edge2score, true_coreference_edge2score =\
         self._compute_training_phase_extended_mention_pair_scores(document) #, entity_head_extents=entity_head_extents, singleton_extents=singleton_extents
        self._logger.debug("Finished computing training pair scores.")
        
        # Compute true graph structure
        self._logger.debug("Computing ground truth graph edges...")
        ground_truth_graph_edges = self._compute_ground_truth_graph_edges(document, extended_edge2score, 
                                                                          true_extended_edge2score, 
                                                                          true_coreference_edge2score) #, entity_head_extents=entity_head_extents
        self._logger.debug("Finished computing ground truth graph edges.")
            
        # Compute predicted graph structure
        #edge_loss_scores = self._compute_edge2loss_score_for_predicted_tree_edges_computation(extended_edge2score, ground_truth_graph_edges)
        # Hypothesis: scores corresponding to predicted edges are positive? Wrong, not the case for all of predicted edges, see TransitiveClosureResolver
        self._logger.debug("Computing predicted graph edges...")
        predicted_graph_edges = self._compute_predicted_graph_edges(document, extended_edge2score, 
                                                                    ground_truth_graph_edges) #, entity_head_extents=entity_head_extents
        self._logger.debug("Finished computing predicted graph edges.")
        
        # Compute loss
        self._logger.debug("Computing graph loss...")
        graph_loss = self._compute_graph_loss(ground_truth_graph_edges, predicted_graph_edges)
        self._logger.debug("Finished computing graph loss.")
        
        # Recompute positive / negative samples collections
        # FIXME: could we not re-use the collection of ML samples generated at the beginning of the function instead of having to compute it anew?
        self._logger.debug("Recomputing positive and negative sample collections...")
        ground_truth_graph_edge_ML_samples = self._generate_edge_samples(document, ground_truth_graph_edges)
        predicted_graph_edge_ML_samples = self._generate_edge_samples(document, predicted_graph_edges)
        self._logger.debug("Finished recomputing positive and negative sample collections.")
        
        # Compute final loss
        ground_truth_graph_edges_score_sum = sum((extended_edge2score.get(s.extent_pair,0) for s in ground_truth_graph_edge_ML_samples), 0.)
        predicted_graph_edges_score_sum = sum((extended_edge2score.get(s.extent_pair,0) for s in predicted_graph_edge_ML_samples), 0.)
        loss = predicted_graph_edges_score_sum - ground_truth_graph_edges_score_sum + numpy.sqrt(graph_loss)
        
        return ground_truth_graph_edge_ML_samples, predicted_graph_edge_ML_samples, loss
    
    
    @abc.abstractmethod
    def _compute_training_phase_extended_mention_pair_scores(self, document):
        """ Computes scores to be associated to extended mention pairs (so, also for pairs whose 
        antecedent mention is the NULL_MENTION); using classification prediction on the mention pair 
        samples collection generated from the input document; and interpreting the classification 
        confidence scores as weights on the edges of a graph of mentions.
        
        Score values may be artificially created for for pairs whose antecedent mention is the 
        NULL_MENTION, if the underlying generation process is unable to generate such mention pairs.
        The scores are float values.
        The output of this method is destined to be used as input to build a virtual ground truth 
        graph during the "prediction" of an atomic training step carried out using the input document 
        as support.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance

        Returns:
            a (extended_edge2score, true_extended_edge2score, true_coreference_edge2score) 
            triplet, where:
                - 'extended_edge2score' is the "edge => score" map resulting for carrying out classification 
                  prediction on the samples generated from the input document, using the underlying generation 
                  process
                - 'true_extended_edge2score' is a submap of the 'extended_edge2score' map, only 
                  for edges that correspond to mention pairs that are made of mentions that belong to the same 
                  coreference entity, (their corresponding sample have been labelled with the positive class 
                  label), or correspond to non-anaphoric mentions (the subsequent 'mention' of the pair is a 
                  true mention, whereas the antecedent 'mention' of the pair is the NULL_MENTION)
                - 'true_coreference_edge2score' is is a submap of the 'extended_edge2score' map, only 
                  for edges that correspond to mention pairs that are made of mentions that belong to the same 
                  coreference entity (their corresponding sample have been labelled with the positive class 
                  label), which means that none of the mentions of those pairs is the NULL_MENTION
        """
        pass
    
    def _compute_training_phase_naturally_extended_mention_pair_scores(self, document):
        """ Computes scores to be associated to extended mention pairs (so, also for pairs whose 
        antecedent mention is the NULL_MENTION); using classification prediction on the mention pair 
        samples collection generated from the input document, assuming the underlying generation 
        process is able to generate mentions pairs where the antecedent mention is the NULL_MENTION; 
        and interpreting the classification confidence scores as weights on the edges of a graph of 
        mentions.
        
        The scores are float values.
        The output of this method is destined to be used as input to build a virtual ground truth 
        graph during the "prediction" of an atomic training step carried out using the input document 
        as support.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance

        Returns:
            a (extended_edge2score, true_extended_edge2score, true_coreference_edge2score) 
            triplet, where:
                - 'extended_edge2score' is the "edge => score" map resulting for carrying out classification 
                  prediction on the samples generated from the input document, using the underlying generation 
                  process
                - 'true_extended_edge2score' is a submap of the 'extended_edge2score' map, only 
                  for edges that correspond to mention pairs that are made of mentions that belong to the same 
                  coreference entity, (their corresponding sample have been labelled with the positive class 
                  label), or correspond to non-anaphoric mentions (the subsequent 'mention' of the pair is a 
                  true mention, whereas the antecedent 'mention' of the pair is the NULL_MENTION)
                - 'true_coreference_edge2score' is is a submap of the 'extended_edge2score' map, only 
                  for edges that correspond to mention pairs that are made of mentions that belong to the same 
                  coreference entity (their corresponding sample have been labelled with the positive class 
                  label), which means that none of the mentions of those pairs is the NULL_MENTION
        """
        # Generate ML samples
        ML_sample_iterable = self._generator.generate_training_set((document,))
        ML_samples = tuple(ML_sample_iterable)
        
        # Fill the maps with the score values
        ## Get the scores resulting from the prediction
        extended_edge2score = self._predict_edge2score(ML_samples)
        ## Create the needed subset maps
        true_extended_edge2score = dict() # Same as extended_edge2score, but only on pairs of mentions that do corefer with one another
        true_coreference_edge2score = dict() # Same as 'true_extended_edge2score', but without NULL_MENTION
        for sample in ML_samples:
            if sample.label == POS_CLASS:
                extent_pair = sample.extent_pair
                score = extended_edge2score[extent_pair]
                true_extended_edge2score[extent_pair] = score
                if NULL_MENTION.extent not in extent_pair:
                    true_coreference_edge2score[extent_pair] = score
        
        return extended_edge2score, true_extended_edge2score, true_coreference_edge2score
    
    def _predict_edge2score(self, ML_samples):
        """ Carries out classification prediction on the input samples collection, and return the 
        corresponding "edge => classification prediction score" map.
        
        The scores are float values.
        
        Args:
            ML_samples: collection of MentionPairSample instances

        Returns:
            an "edge => classification prediction score value" map
        """
        # Predict classes and associated edges scores
        predictions = self._model_interface.predict(ML_samples)
        # Fill the map with the score values
        edges_scores = dict()
        for sample, classification_prediction in zip(ML_samples, predictions): # Pairs are scored separately
            extent_pair = sample.extent_pair
            score = classification_prediction.get_label_score(POS_CLASS)
            edges_scores[extent_pair] = score
        return edges_scores
    
    def _compute_training_phase_artificially_extended_mention_pair_scores(self, document):
        """ Computes scores to be associated to extended mention pairs (so, also for pairs whose 
        antecedent mention is the NULL_MENTION); using classification prediction on the mention pair 
        samples collection generated from the input document, assuming the underlying generation 
        process is unable to generate mentions pairs where the antecedent mention is the NULL_MENTION; 
        and interpreting the classification confidence scores as weights on the edges of a graph of 
        mentions.
        
        Since the underlying generation process is unable to generate mention pairs whose antecedent 
        mention is the NULL_MENTION, score values will be artificially created for them, with their 
        value set to 0.
        The scores are float values.
        The output of this method is destined to be used as input to build a virtual ground truth 
        graph during the "prediction" of an atomic training step carried out using the input document 
        as support.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            document: a Document instance

        Returns:
            a (extended_edge2score, true_extended_edge2score, true_coreference_edge2score) 
            triplet, where:
                - 'extended_edge2score' is the "edge => score" map resulting for carrying out classification 
                  prediction on the samples generated from the input document, using the underlying generation 
                  process, and supplemented with values for edges made from mention pairs where the antecedent 
                  mention is the NULL_MENTION
                - 'true_extended_edge2score' is a submap of the 'extended_edge2score' map, only 
                  for edges that correspond to mention pairs that are made of mentions that belong to the same 
                  coreference entity, (their corresponding sample have been labelled with the positive class 
                  label), or correspond to non-anaphoric mentions (the subsequent 'mention' of the pair is a 
                  true mention, whereas the antecedent 'mention' of the pair is the NULL_MENTION)
                - 'true_coreference_edge2score' is is a submap of the 'extended_edge2score' map, only 
                  for edges that correspond to mention pairs that are made of mentions that belong to the same 
                  coreference entity (their corresponding sample have been labelled with the positive class 
                  label), which means that none of the mentions of those pairs is the NULL_MENTION
        """
        # Get the initial maps with the score values
        edge2score, true_edge2score, true_coreference_edge2score =\
         self._compute_training_phase_naturally_extended_mention_pair_scores(document)
        
        # Insert null score for NULL_MENTION-mention pairs when not using anaphoricity features.
        # FIXME: Using a score value of 0 implies the assumption that the probability to be associated 
        # with the POS_CLASS increases with the score's value, and that edges that should be classified 
        # in the POS_CLASS class should have a positive score value?
        extended_edge2score = edge2score
        true_extended_edge2score = true_edge2score
        for mention in document.mentions:
            extent_pair = get_mention_pair_extent_pair(NULL_MENTION, mention)
            # FIXME: could we not replace this by something akin to a '.get' method on python dictionary?
            extended_edge2score[extent_pair] = 0. #FIXME: Why the value '0.'?
            true_extended_edge2score[extent_pair] = 0.    
        
        return extended_edge2score, true_extended_edge2score, true_coreference_edge2score
    
    
    @abc.abstractmethod
    def _compute_ground_truth_graph_edges(self, document, extended_edge2score, true_extended_edge2score, 
                                          true_coreference_edge2score):
        """ Computes a latent "ground truth" graph associated to the mentions of the input document 
        (that is to say, a graph that takes into account the info brought by the ground truth 
        coreference partition associated to the input document), and return the edges of this graph.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            extended_edge2score: an "edge => score" map, where the edges correspond to all mention 
                pairs that are implicitly defined by the application of the underlying generation process on 
                the input document
            true_extended_edge2score: an "edge => score" map, where the edges correspond to 
                mention pairs that are implicitly defined by the application of the underlying generation 
                process on the input document, and which are associated to the positive class
            true_coreference_edge2score: an "edge => score" map, where the edges correspond to 
                mention pairs that are implicitly defined by the application of the underlying generation 
                process on the input document, and which correspond to true mentions belonging to the same 
                coreference entity

        Returns:
            a collection of edges
        """
        pass
    
    
    @abc.abstractmethod
    def _compute_predicted_graph_edges(self, document, extended_edge2score, ground_truth_graph_edges):
        """ Computes a latent "predicted" graph associated to the mentions of the input document
        (that is to say, a graph that does not take into account the info brought by the ground truth 
        coreference partition associated to the input document, but only relies on score values 
        associated to possible edges, as well as the "ground truth" graph that it will be compared 
        to), and return the edges of this graph.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            extended_edge2score: an "edge => score" map, where the edges correspond to all mention 
                pairs that are implicitly defined by the application of the underlying generation process on 
                the input document
            ground_truth_graph_edges: collection of edges of the latent "ground truth" graph 
                that has been associated to the mentions of the input document

        Returns:
            a collection of edges
        """
        pass
    
    
    @abc.abstractmethod
    def _compute_graph_loss(self, ground_truth_graph_edges, predicted_graph_edges):
        """ Computes the loss value, resulting from the comparison of the computed predicted graph 
        to the computed ground truth graph.
        
        The graphs are defined here only through the respective set of edges that constitute them.
        The output loss value results from the addition of loss values incurred by edges of the 
        predicted graph which is being compared to the ground truth graph.
        
        Args:
            predicted_graph_edges: collection of edges defining the predicted graph
            ground_truth_graph_edges: collection of edges defining the ground truth graph

        Returns:
            a float, the computed loss value
        """
        pass
    
    
    @abc.abstractmethod
    def _generate_edge_samples(self, document, edges):
        """ Creates collection of MentionPairSample instances corresponding to pairs of mentions made 
        from mentions coming from the input document, pairs that are specified by input edges collection.
        
        Only edges whose corresponding mentions pair that can be produced by the underlying generator, 
        from the input document, shall see the creation of a corresponding MentionPairSample instance.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            edges: collection of edges

        Returns:
            collection of MentionPairSample instances, where all mentions of the pairs are 
            true mentions (no one is the NULL_MENTION)
        """
        pass
    
    def _generate_extended_mention_pair_edge_samples(self, document, edges):
        """ Creates collection of MentionPairSample instances corresponding to mentions pairs created 
        from mentions coming from the input document, or from the NULL_MENTION. The pairs are 
        specified by the input edges collection.
        
        Assume the underlying generator is a _BaseMentionPairGenerator child class instance.
        Assume that the input edges are all made of extent of mentions that are defined in the input 
        document OR are that of the NULL_MENTION.
               
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            edges: collection of edges

        Returns:
            collection of MentionPairSample instances, where one of the mention of the pair 
            can be the NULL_MENTION
        """
        get_mention_from_extent = lambda extent_: NULL_MENTION if extent_ == NULL_MENTION.extent else document.extent2mention[extent_]
        edge_samples = []
        for mention_extent_pair in edges:
            mention_pair = tuple(get_mention_from_extent(extent) for extent in mention_extent_pair)
            ML_sample = self._generator.generate_sample_from_mention_pair(*mention_pair, train=True) #, entity_head_extents=entity_head_extents, singleton_extents=singleton_extents
            edge_samples.append(ML_sample)
        return edge_samples
    
    def _generate_mention_pair_edge_samples(self, document, edges):
        """ Creates collection of MentionPairSample instances corresponding to pairs of mentions made 
        from mentions coming from the input document, pairs that are specified by input edges collection.
        Edges that refer to the NULL_MENTION will not be considered, and no corresponding 
        MentionPairSample instance be created and added to the output collection. 
        
        Assume the underlying generator is a _BaseMentionPairGenerator child class instance.
        Assume that the input edges are all made of extent of mentions that are defined in the input 
        document OR are that of the NULL_MENTION.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            edges: collection of edges

        Returns:
            collection of MentionPairSample instances, where all mentions of the pairs are true 
            mentions (no one is the NULL_MENTION)
        """
        edge_samples = []
        for mention_extent_pair in edges:
            if NULL_MENTION.extent in mention_extent_pair: # Cannot create pair using NULL_MENTION for a MentionPairSampleGenerator
                continue
            mention_pair = tuple(document.extent2mention[extent] for extent in mention_extent_pair)
            ML_sample = self._generator.generate_sample_from_mention_pair(*mention_pair, train=True) #, entity_head_extents=entity_head_extents, singleton_extents=singleton_extents
            edge_samples.append(ML_sample)
        return edge_samples
    
    
    @abc.abstractmethod
    def _compute_prediction_phase_extended_mention_pair_scores(self, document):
        """ Computes scores to be associated to extended mention pairs (so, also for pairs whose 
        antecedent mention is the NULL_MENTION); using classification prediction on the mention pair 
        samples collection generated from the input document; and interpreting the classification 
        confidence scores as weights on the edges of a graph of mentions.
        
        Score values may be artificially created for for pairs whose antecedent mention is the 
        NULL_MENTION, if the underlying generation process is unable to generate such mention pairs.
        The scores are float values.
        The output of this method is destined to be used as input to the decoding process, in the 
        frame of the prediction of a coreference partition for the input document.
        
        Args:
            document: a Document instance

        Returns:
            an "edge => score" map
        
        Notes
            This abstract method must be defined here instead of within the "..ResolverMixin" classes, 
            because of the python MRO (Method Resolution Order). If defined in those classes instead of 
            here, then it will not be possible to override them in future classes, with the way the code 
            is currently written, and call to a method by this name will return None.
        """
        pass
    
    def _compute_naturally_extended_mention_pair_prediction_pair_scores(self, document): # , entity_head_extents=None,  singleton_extents=None
        """ Computes scores to be associated to extended mention pairs (so, also for pairs whose 
        antecedent mention is the NULL_MENTION); using classification prediction on the mention pair 
        samples collection generated from the input document, assuming the underlying generation 
        process is able to generate mentions pairs where the antecedent mention is the NULL_MENTION; 
        and interpreting the classification confidence scores as weights on the edges of a graph of 
        mentions.
        
        The scores are float values.
        The output of this method is destined to be used as input to the decoding process, in the 
        frame of the prediction of a coreference partition for the input document.
        
        Args:
            document: a Document instance

        Returns:
            an "edge => score" map resulting for carrying out classification prediction on the 
            samples generated from the input document, using the underlying generation process
        """
        # Generate ML samples
        ML_sample_iterable = self._generator.generate_prediction_set(document)
        ML_samples = tuple(ML_sample_iterable)
        # Fill the map with the score values
        extended_edge2score = self._predict_edge2score(ML_samples)
        return extended_edge2score
    
    def _compute_artificially_extended_mention_pair_prediction_pair_scores(self, document): # , entity_head_extents=None,  singleton_extents=None
        """ Computes scores to be associated to extended mention pairs (so, also for pairs whose 
        antecedent mention is the NULL_MENTION); using classification prediction on the mention pair 
        samples collection generated from the input document, assuming the underlying generation 
        process is unable to generate mentions pairs where the antecedent mention is the NULL_MENTION; 
        and interpreting the classification confidence scores as weights on the edges of a graph of 
        mentions.
        
        Since the underlying generation process is unable to generate mention pairs whose antecedent 
        mention is the NULL_MENTION, score values will be artificially created for them, with their 
        value set to 0.
        The scores are float values.
        The output of this method is destined to be used as input to the decoding process, in the 
        frame of the prediction of a coreference partition for the input document.
        
        Args:
            document: a Document instance

        Returns:
            an "edge => score" map resulting for carrying out classification prediction on the 
            samples generated from the input document, using the underlying generation process, and 
            supplemented with values for edges made from mention pairs where the antecedent mention is 
            the NULL_MENTION
        """
        # Get initial scores map
        extended_edge2score = self._compute_naturally_extended_mention_pair_prediction_pair_scores(document)
        # Insert null score for NULL_MENTION-mention pairs when not using anaphoricity features.
        # FIXME: Assume that the extended_edge2score are all non-negative, and that the probability to be associated with the POS_CLASS increases with the score's value? 
        for mention in document.mentions:
            extent_pair = get_mention_pair_extent_pair(NULL_MENTION,mention)
            extended_edge2score[extent_pair] = 0. #FIXME: Why the value '0.'?
        return extended_edge2score
    
    



class _SimpleDocumentStructuredPairResolverMixIn(object):
    """ This mixin class implements the 'resolve' method for resolver classes that do not use edges 
    constraints in their algorithm.
    """
    
    # Instance methods
    def resolve(self, document): #, entity_head_extents=None, singleton_extents=None
        """ Predicts a coreference partition for the input document.
        
        Args:
            document: a Document instance

        Returns:
            a CoreferencePartition instance
        """
        # Check input
        self._preprocessing_resolution(document)
        # Predict coreference scores
        self._logger.debug("Predicting the coreference scores...")
        extents_pair2score = self._compute_prediction_phase_extended_mention_pair_scores(document) # true_score, cluster_scores #, entity_head_extents=entity_head_extents, singleton_extents=singleton_extents
        self._logger.debug("Finished predicting the coreference scores.")
        # Decode coreference scores
        self._logger.debug("Decoding the coreference scores...")
        scores_are_proba = self._model_interface.SCORES_ARE_PROBA
        coreference_partition = self._decoder.decode(document, extents_pair2score, 
                                                     scores_are_proba=scores_are_proba)
        self._logger.debug("Finished decoding the coreference scores.")
        return coreference_partition


class _ConstrainedDocumentStructuredPairResolverMixIn(object):
    """ This mixin class implements the 'resolve' method for resolver classes that use edges 
    constraints in their algorithm.
    """
    
    # Instance methods
    def resolve(self, document): #, entity_head_extents=None, singleton_extents=None
        """ Predicts a coreference partition for the input document.
        
        Args:
            document: a Document instance

        Returns:
            a CoreferencePartition instance
        """
        # Check input
        self._preprocessing_resolution(document)
        # Predict coreference scores
        self._logger.debug("Predicting the coreference scores...")
        scores = self._compute_prediction_phase_extended_mention_pair_scores(document) # true_score, cluster_scores #, entity_head_extents=entity_head_extents, singleton_extents=singleton_extents
        self._logger.debug("Finished predicting the coreference scores.")
        # Compute constraints
        self._logger.debug("Computing the constraints on the edges...")
        positive_edges, negative_edges = self.get_constraints(document) #, entity_head_extents=entity_head_extents
        self._logger.debug("Finished computing the constraints on the edges.")
        # Decode coreference scores
        self._logger.debug("Decoding the coreference scores...")
        coreference_partition = self._decoder.decode(document, scores, positive_edges=positive_edges, 
                                                     negative_edges=negative_edges)
        self._logger.debug("Finished decoding the coreference scores.")
        return coreference_partition




root_loss_attribute_data = ("root_loss",
                            (_create_simple_get_parameter_value_from_configuration("root_loss", postprocess_fct=float),
                             _create_simple_set_parameter_value_in_configuration("root_loss"))
                            )
class _TreeLossDocumentStructuredPairResolver(_DocumentStructuredPairResolver):
    """ This class represent document structured resolvers whose method to compute the loss from 
    the comparison of a 'true' and a 'predicted' graph structure implies the call to a function that 
    can be parametrized by a 'root_loss' value.
    
    Cf the _DocumentStructuredPairResolver class
    
    This 'root_loss' value should be a non-negative real number.
    
    As such, can be instantiated only with a _OnlineBinaryClassifier child class instance, a 
    _BaseMentionPairGenerator child class instance and a __BaseMentionPairCoreferenceDecoder child 
    class instance.
    
    Arguments:
        model_interface: a _OnlineBinaryClassifier child class instance, representing the interface 
            to the underlying online machine learning model to train and to use
        
        generator: a _BaseMentionPairGenerator child class instance, used to generate, from document(s), 
            machine-learning samples used as input by the underlying machine learning model
        
        decoder: a __BaseMentionPairCoreferenceDecoder child class instance, used to interpret 
            scores associated to machine-learning samples into a coreference partition, for the document 
            used to generate the scored machine-learning samples
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        sample_type: string specifying the type of the sample used in the frame of the underlying 
            machine-learning task
        
        document_hash2cache: document_hash2cache: a "document hash => cache" map, or None; the 
            current value of the underlying model interface's vectorization cache (cf 
            'cortex.coreference.api.vectorization_cache.py')
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    __INIT_PARAMETERS = OrderedDict((root_loss_attribute_data,))
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple(__INIT_PARAMETERS.keys()) +\
                                                 _DocumentStructuredPairResolver._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = _DocumentStructuredPairResolver._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION+\
                                            tuple(__INIT_PARAMETERS.keys())
    
    def __init__(self, model_interface, generator, decoder, root_loss, configuration=None):
        # Check input values
        # Initialize using parent class
        super().__init__(model_interface, generator, decoder, configuration=configuration)
        # Assign attribute values
        self.root_loss = root_loss
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        root_loss, = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        args = args + (root_loss,)
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, generator_factory_configuration, decoder_factory_configuration, 
                             sample_features_vectorizer_factory_configuration, root_loss,
                             classifier_factory_configuration=None):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class.
        
        Args:
            generator_factory_configuration: a factory configuration that can be used to create 
                a specific _Generator child class instance
            decoder_factory_configuration: a factory configuration that can be used to create 
                a specific _CoreferenceDecoder child class instance
            sample_features_vectorizer_factory_configuration: a factory configuration that can be 
                used to create a specific _BaseExtendedMentionPairSampleFeaturesVectorizer child class instance
            root_loss: a non-negative real number; cf the class documentation
            classifier_factory_configuration: a factory configuration that can be used to 
                create a specific _OnlineBinaryClassifier child class instance; or None, in which case the 
                factory configuration for a 'PerceptronOnlineBinaryClassifier' instance (parametrized with 
                'avg=True, warm_start=False') will be used

        Returns:
            a Mapping instance
        """
        configuration = super().define_configuration(generator_factory_configuration, decoder_factory_configuration, 
                                                         sample_features_vectorizer_factory_configuration, 
                                                         classifier_factory_configuration=classifier_factory_configuration)
        configuration["root_loss"] = root_loss
        return configuration
    
    # Instance methods
    def _compute_graph_loss(self, ground_truth_graph_edges, predicted_graph_edges):
        """ Computes the loss value, resulting from the comparison of the computed predicted graph 
        to the computed ground truth graph.
        
        The graphs are defined here only through the respective set of edges that constitute them.
        The output loss value results from the addition of loss values incurred by edges of the 
        predicted graph which is being compared to the ground truth graph.
        
        Args:
            predicted_graph_edges: collection of edges defining the predicted graph
            ground_truth_graph_edges: collection of edges defining the ground truth graph

        Returns:
            a float, the computed loss value
        """
        return self._compute_tree_loss(predicted_graph_edges, ground_truth_graph_edges,  
                                       root_node=NULL_MENTION.extent, root_node_loss=self.root_loss)
    
    @classmethod
    def _compute_tree_loss(cls, predicted_tree_edges, ground_truth_tree_edges, root_node=None, 
                           root_node_loss=1.0):
        """ Compute the loss value, resulting from the comparison of the computed predicted tree to 
        the computed ground truth tree
        
        The loss is defined in the "Joint Anaphoricity Detection and Coreference Resolution with 
        Constrained Latent Structures" paper (the one used in the expression "argmax_T( w.phi(T) + loss(T,T') )", 
        where T' is the computed ground truth tree").
        This loss value is computed as the addition of loss values incurred by edges of the 
        predicted tree that do not exist in the ground truth tree, and by edges of the 
        ground truth tree that do not exist in the predicted tree.
        
        The trees are defined here only through the respective set of edges that constitute them.
        
        Args:
            predicted_tree_edges: collection of edges defining the predicted tree
            ground_truth_tree_edges: collection of edges defining the ground truth tree
            root_node: the object identifying the root node of a tree, when considering an edge
            root_node_loss: non-negative float, the loss value possibly incurred by an edge one 
                of whose node is the root node. The loss value possibly incurred by other edges is equal to 1.

        Returns:
            float, the computed loss value
        """
        if root_node_loss < 0:
            message = "Incorrect input 'root_node_loss' value '{}', must be non-negative."
            message = message.format(root_node_loss)
            raise ValueError(message)
        
        ground_truth_tree_edges_set = set(ground_truth_tree_edges)
        predicted_tree_edges_set = set(predicted_tree_edges)
        loss = 0.0
        
        # Loss incurred for edges present in prediction but absent from ground truth
        for u,v in predicted_tree_edges:
            if not ((u,v) in ground_truth_tree_edges_set or (v,u) in ground_truth_tree_edges_set):
                if u == root_node or v == root_node:
                    loss += root_node_loss
                else:
                    loss += 1.0
        
        # Loss incurred for edges present in ground truth but absent from prediction
        for u,v in ground_truth_tree_edges:
            if not ((u,v) in predicted_tree_edges_set or (v,u) in predicted_tree_edges_set):
                if u == root_node or v == root_node:
                    loss += root_node_loss
                else:
                    loss += 1.0
        
        return loss
    
    @classmethod
    def _add_loss_value_to_weights(cls, predicted_tree_possible_edge2weight, ground_truth_tree_edges, 
                                   root_node=None, root_node_loss=1.0):
        """ Adds potential individual loss values to the weigh values respectively associated to 
        possible edges of the predicted tree to be computed, for possible edges that do not belong 
        to the set of edges that constitute the ground truth tree.
        
        This is done so as to be able to compute "argmax_T( w.phi(T) + loss(T,T') )", where T' is 
        the computed ground truth tree", afterward, in order to compute the 'best' predicted tree 
        (cf the "Joint Anaphoricity Detection and Coreference Resolution with Constrained Latent 
        Structures" paper).
        
        It is assumed that the edges defined in the input 'ground_truth_tree_edges' parameter value 
        constitute a subset of the set of edges defined in the input 'predicted_tree_possible_edge2weight' 
        parameter value. This way, the only possible errors to be punished by the addition of a loss 
        value to the weight of an edge, is for edges that are defined in the latter, but not in the 
        former.
        
        Args:
            predicted_tree_possible_edge2weight: an "edge => weight value" map, which shall be 
                used as input to the process tasked with computing a predicted tree (cf above)
            ground_truth_tree_edges: collection of the edges constituting the ground truth tree
            root_node: the object identifying the root node of a tree, when considering an edge
            root_node_loss: non-negative float, the loss value used to increase the weight 
                associated to an edge, one of whose node is the root node. For information, the loss value 
                possibly incurred by the other edges is equal to 1.

        Returns:
            an "extent => weight value" map, corresponding to possible updated values found in 
            the input map
        
        Notes:
            This method is only required from the '_UpdateModeTreeLossDocumentStructuredPairsResolver' 
            class onward, but is being defined here because of its behavioral link with the 
            '_compute_tree_loss' method, which, itself, must be defined here.
        """
        if root_node_loss < 0:
            message = "Incorrect input 'root_node_loss' value '{}', must be non-negative."
            message = message.format(root_node_loss)
            raise ValueError(message)
        
        ground_truth_tree_edges_set = set(ground_truth_tree_edges) # edges of the 'true' tree
        predicted_tree_possible_edge2loss_weight = dict(predicted_tree_possible_edge2weight)
        predicted_tree_possible_edges = tuple(predicted_tree_possible_edge2loss_weight)
        for u, v in predicted_tree_possible_edges:
            # Possible edges that do not belong to the set of edges constituting the ground truth 
            # tree have their respective weight value be increased by the associated loss value
            # FIXME: assumes that u <= v: can we be sure of it? Update the definition of 'edge' given at the beginning of this module? 
            #predicted_tree_possible_edge2loss_weight[u,v] = predicted_tree_possible_edge2weight[u,v]
            if not ((u,v) in ground_truth_tree_edges_set or (v,u) in ground_truth_tree_edges_set): # normally u < v (this is just a security)
                if u == root_node or v == root_node:
                    predicted_tree_possible_edge2loss_weight[(u,v)] += root_node_loss
                else:
                    predicted_tree_possible_edge2loss_weight[(u,v)] += 1.0
        return predicted_tree_possible_edge2loss_weight







update_mode_attribute_data = ("update_mode",
                            (_create_simple_get_parameter_value_from_configuration("update_mode", 
                                                                                   postprocess_fct=str),
                             _create_simple_set_parameter_value_in_configuration("update_mode"))
                            )
POSSIBLE_UPDATE_MODE_VALUES = ("ML", "PB")
class _UpdateModeTreeLossDocumentStructuredPairsResolver(_TreeLossDocumentStructuredPairResolver):
    """ This class represent tree loss document structured resolvers whose method used to compute the 
    predicted graph structure, from scores on pairs of mentions and from a 'true' graph structure, 
    can be parametrized by a choice represented by an 'update_mode' parameter.
    
    Cf the _TreeLossDocumentStructuredPairResolver class.
    
    The value of the 'update_mode' parameter can be either 'PB' (no modification made to the scores 
    given to pairs of mentions) or 'ML' (the scores corresponding to pairs of mentions for which an 
    edge does not exist in the input 'true' graph structure are increased, possibly using the 
    'root_loss' value with which the resolver has been parametrized).
    
    As such, can be instantiated only with a _OnlineBinaryClassifier child class instance, a 
    _BaseMentionPairGenerator child class instance and a __BaseMentionPairCoreferenceDecoder child 
    class instance.
    
    Arguments:
        model_interface: a _OnlineBinaryClassifier child class instance, representing the interface 
            to the underlying online machine learning model to train and to use
        
        generator: a _BaseMentionPairGenerator child class instance, used to generate, from document(s), 
            machine-learning samples used as input by the underlying machine learning model
        
        decoder: a __BaseMentionPairCoreferenceDecoder child class instance, used to interpret scores 
            associated to machine-learning samples into a coreference partition, for the document used 
            to generate the scored machine-learning samples
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        update_mode: string, either 'PB' or 'ML'; cf the class documentation
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        sample_type: string specifying the type of the sample used in the frame of the underlying 
            machine-learning task
        
        document_hash2cache: document_hash2cache: a "document hash => cache" map, or None; the 
            current value of the underlying model interface's vectorization cache (cf 
            'cortex.coreference.api.vectorization_cache.py')
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        update_mode: string, either 'PB' or 'ML'; cf the class documentation
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    __INIT_PARAMETERS = OrderedDict((update_mode_attribute_data,))
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple(__INIT_PARAMETERS.keys()) +\
                                                 _TreeLossDocumentStructuredPairResolver._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = _TreeLossDocumentStructuredPairResolver._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION+\
                                            tuple(__INIT_PARAMETERS.keys())
    
    def __init__(self, model_interface, generator, decoder, root_loss, update_mode, configuration=None):
        # Check input values
        if update_mode not in POSSIBLE_UPDATE_MODE_VALUES:
            msg = "Incorrect 'update_mode' input value '{}', possible values are: {}."
            msg = msg.format(POSSIBLE_UPDATE_MODE_VALUES)
            raise ValueError(msg)
        # Initialize using parent class
        super().__init__(model_interface, generator, decoder, root_loss, 
                         configuration=configuration)
        # Assign attribute values
        self.update_mode = update_mode
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        update_mode, = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        args = args + (update_mode,)
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, generator_factory_configuration, decoder_factory_configuration, 
                             sample_features_vectorizer_factory_configuration, root_loss, update_mode, 
                             classifier_factory_configuration=None):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class.
        
        Args:
            generator_factory_configuration: a factory configuration that can be used to create 
                a specific _Generator child class instance
            decoder_factory_configuration: a factory configuration that can be used to create 
                a specific _CoreferenceDecoder child class instance
            sample_features_vectorizer_factory_configuration: a factory configuration that can be 
                used to create a specific _BaseExtendedMentionPairSampleFeaturesVectorizer child class instance
            root_loss: a non-negative real number; cf the class documentation
            update_mode: string, either 'PB' or 'ML'; cf the class documentation
            classifier_factory_configuration: a factory configuration that can be used to 
                create a specific _OnlineBinaryClassifier child class instance; or None, in which case the 
                factory configuration for a 'PerceptronOnlineBinaryClassifier' instance (parametrized with 
                'avg=True, warm_start=False') will be used

        Returns:
            a Mapping instance
        """
        # Check and process inputs
        
        configuration = super().define_configuration(generator_factory_configuration, decoder_factory_configuration, 
                                                         sample_features_vectorizer_factory_configuration, root_loss, 
                                                         classifier_factory_configuration=classifier_factory_configuration)
        configuration["update_mode"] = update_mode
        return configuration
    
    # Instance methods
    def _compute_predicted_graph_edges(self, document, extended_edge2weight, ground_truth_graph_edges): #, entity_heads=None
        """ Computes a latent "predicted" graph associated to the mentions of the input document
        (that is to say, a graph that does not take into account the info brought by the ground truth 
        coreference partition associated to the input document, but only relies on score values 
        associated to possible edges, as well as the "ground truth" graph that it will be compared 
        to), and return the edges of this graph.
        
        Here, the graphs are in fact trees.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            extended_edge2weight: an "edge => score" map, where the edges correspond to all 
                mention pairs that are implicitly defined by the application of the underlying generation 
                process on the input document
            ground_truth_graph_edges: collection of edges of the latent "ground truth" graph 
                that has been associated to the mentions of the input document

        Returns:
            collection of edges
        """
        extended_edge2loss_updated_weight = self._compute_edge2loss_score_for_predicted_tree_edges_computation(extended_edge2weight, 
                                                                                                               ground_truth_graph_edges)
        predicted_graph_edges = self._compute_predicted_tree_edges_from_edge2loss_score(document, extended_edge2loss_updated_weight)
        return predicted_graph_edges
    
    def _compute_edge2loss_score_for_predicted_tree_edges_computation(self, predicted_tree_possible_edge2weight, 
                                                                      ground_truth_graph_edges):
        """ Computes a possibly updated version of the weight values associated to possible edges 
        for the computation of the predicted tree.
        
        This is done so as to be able to compute "argmax_T( w.phi(T) + loss(T,T') )", where T' is 
        the computed ground truth tree", afterward, in order to compute the 'best' predicted tree 
        (cf the "Joint Anaphoricity Detection and Coreference Resolution with Constrained Latent 
        Structures" paper): here, the loss value in the expression is decomposed into individual 
        loss values "loss(T,T')_i" associated to each possible edge "i" of the predicted tree to be 
        computed:
        loss(T,T') = sum_i( loss(T,T')_i )
        => w.phi(T) + loss(T,T') = sum_i( w . phi(T)_i ) + sum_i( loss(T,T')_i )
        => w.phi(T) + loss(T,T') = sum_i( w . phi(T)_i + loss(T,T')_i )
        => argmax_T( w.phi(T) + loss(T,T') ) = argmax_T( sum_i( w . phi(T)_i + loss(T,T')_i ) )
        
        Args:
            predicted_tree_possible_edge2weight: an "edge => weight" map, whose keys constitute 
                the set of edges from which the predicted tree will be computed
            ground_truth_graph_edges: collection of edges of the ground truth tree in comparison 
                to which the predicted tree shall be computed

        Returns:
            an "edge => weight" map
        """
        if self.update_mode == "ML":
            extended_edge2loss_updated_weight = self._add_loss_value_to_weights(predicted_tree_possible_edge2weight, 
                                                                                ground_truth_graph_edges, 
                                                                                root_node=NULL_MENTION.extent, 
                                                                                root_node_loss=self.root_loss)
        elif self.update_mode == "PB":
            extended_edge2loss_updated_weight = dict(predicted_tree_possible_edge2weight)
        else:
            message = "Wrong 'update_mode' attribute value '{}'; must belong to the following possiblee values: {}."
            message = message.format(self.update_mode, list(POSSIBLE_UPDATE_MODE_VALUES))
            raise ValueError(message)
        return extended_edge2loss_updated_weight
    
    @abc.abstractmethod
    def _compute_predicted_tree_edges_from_edge2loss_score(self, document, edge2weight):
        """ Computes a latent "predicted" graph associated to the mentions of the input document
        (that is to say, a graph that does not take into account the info brought by the ground truth 
        coreference partition associated to the input document, but only relies on weight values 
        associated to possible edges), and return the edges of this graph.
        
        Here, the graph is in fact a tree.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            edge2weight: an "edge => weigt" map, where the edges correspond to all mention pairs 
                that are implicitly defined by the application of the underlying generation process on the 
                input document

        Returns:
            a collection of edges corresponding to the computed "predicted" graph
        """
        pass
    






########## MST (Maximum Spanning Tree) Resolver ##########
class _BaseMSTResolver(_UpdateModeTreeLossDocumentStructuredPairsResolver):
    """ Base class MST learning-based resolver:
    
    The 'true' and 'predicted' graph structures are computed from scores associated to potential 
    edges by computing a Maximum Spanning Tree on those edges.
    
    Cf the _UpdateModeTreeLossDocumentStructuredPairsResolver class.
    
    As such, can be instantiated only with a _OnlineBinaryClassifier child class instance, a 
    _BaseMentionPairGenerator child class instance and a __BaseMentionPairCoreferenceDecoder child 
    class instance.
    
    Arguments:
        model_interface: a _OnlineBinaryClassifier child class instance, representing the interface 
            to the underlying online machine learning model to train and to use
        
        generator: a _BaseMentionPairGenerator child class instance, used to generate, from document(s), 
            machine-learning samples used as input by the underlying machine learning model
        
        decoder: a __BaseMentionPairCoreferenceDecoder child class instance, used to interpret scores 
            associated to machine-learning samples into a coreference partition, for the document used 
            to generate the scored machine-learning samples
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        update_mode: string, either 'PB' or 'ML'; cf the class documentation
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        sample_type: string specifying the type of the sample used in the frame of the underlying 
            machine-learning task
        
        document_hash2cache: document_hash2cache: a "document hash => cache" map, or None; the 
            current value of the underlying model interface's vectorization cache (cf 
            'cortex.coreference.api.vectorization_cache.py')
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        update_mode: string, either 'PB' or 'ML'; cf the class documentation
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    # Instance methods
    # FIXME: in the following method, in the case where the generator is 'Extended', is it not wrong to manually manage pairs that are using NULL_MENTION?
    def _compute_ground_truth_graph_edges(self, document, extended_edge2score, true_extended_edge2score, 
                                          true_coreference_edge2score): #, entity_head_extents=None
        """ Computes a latent "ground truth" graph associated to the mentions of the input document 
        (that is to say, a graph that takes into account the info brought by the ground truth 
        coreference partition associated to the input document), and return the edges of this graph.
        
        Here, it is computed as a forest (one tree per coreference entity), and with the addition 
        of edges between, on the one hand, nodes corresponding to non-anaphoric mentions, and one 
        the other hand, the root node corresponding to the NULL_MENTION, so as to create a proper 
        tree.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            extended_edge2score: an "edge => score" map, where the edges correspond to all mention 
                pairs that are implicitly defined by the application of the underlying generation process on 
                the input document. Not used here, exists only for API compatibility reasons.
            true_extended_edge2score: an "edge => score" map, where the edges correspond to 
                mention pairs that are implicitly defined by the application of the underlying generation 
                process on the input document, and which are associated to the positive class. Not used here, 
                exists only for API compatibility reasons.
            true_coreference_edge2score: an "edge => score" map, where the edges correspond to 
                mention pairs that are implicitly defined by the application of the underlying generation 
                process on the input document, and which correspond to true mentions belonging to the same 
                coreference entity

        Returns:
            a collection of edges
        """
        mention_extents = tuple(m.extent for m in document.mentions)
        # FIXME: in the following function, we use python's ordering on tuples of numbers, not the ordering defined on Mention instances
        true_mention_extent_forest = prim_msf(mention_extents, true_coreference_edge2score, 
                                              spanning_tree_type="max")
        true_mst_edges = []
        for nodes, edges, _ in true_mention_extent_forest: #node <=> set of extents, edges <=> list of pairs of extent, total tree weight value
            # FIXME: in the following, we use python's ordering on tuples of numbers, not the ordering defined on Mention instances
            first_mention_extent = sorted(nodes)[0] # The first (leftmost for left to right reading order languages) mention is linked to the root
            true_mst_edges += [(NULL_MENTION.extent, first_mention_extent)] + edges
        return true_mst_edges
    
    def _compute_predicted_tree_edges_from_edge2loss_score(self, document, edge2weight):
        """ Computes a latent "predicted" graph associated to the mentions of the input document
        (that is to say, a graph that does not take into account the info brought by the ground truth 
        coreference partition associated to the input document, but only relies on weight values 
        associated to possible edges), and return the edges of this graph.
        
        Here, the graph is in fact a tree.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            edge2weight: an "edge => weigt" map, where the edges correspond to all mention pairs 
                that are implicitly defined by the application of the underlying generation process on the 
                input document

        Returns:
            a collection of edges corresponding to the computed "predicted" graph
        """
        nodes = tuple(m.extent for m in itertools.chain((NULL_MENTION,), document.mentions))
        _, predicted_mst_edges, _ = prim_mst(nodes, edge2weight, spanning_tree_type="max") #nodes, , total_tree_weight
        return predicted_mst_edges







class _BaseMentionPairMSTResolver(_BaseMSTResolver):
    """ Base MST learning-based resolver where the underlying mention pairs generation process does 
    not support the NULL_MENTION, so support for it has to be artificially implemented.
     
    Cf the _BaseMSTResolver class.
    
    As such, can be instantiated only with a _OnlineBinaryClassifier child class instance, a 
    MentionPairSampleGenerator instance and a __BaseMentionPairCoreferenceDecoder child class instance.
    
    Arguments:
        model_interface: a _OnlineBinaryClassifier child class instance, representing the interface 
            to the underlying online machine learning model to train and to use
        
        generator: a MentionPairSampleGenerator instance, used to generate, from document(s), 
            machine-learning samples used as input by the underlying machine learning model
        
        decoder: a __BaseMentionPairCoreferenceDecoder child class instance, used to interpret scores 
            associated to machine-learning samples into a coreference partition, for the document used 
            to generate the scored machine-learning samples
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        update_mode: string, either 'PB' or 'ML'; cf the class documentation
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        sample_type: string specifying the type of the sample used in the frame of the underlying 
            
        
        document_hash2cache: document_hash2cache: a "document hash => cache" map, or None; the 
            current value of the underlying model interface's vectorization cache (cf 
            'cortex.coreference.api.vectorization_cache.py')
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        update_mode: string, either 'PB' or 'ML'; cf the class documentation
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _check_subcomponent(self, model_interface, generator, decoder):
        """ Checks that the input model interface, generator, and decoder are correct; that is: 
        - the model_interface is a _OnlineBinaryClassifier child class instance
        - the generator is a MentionPairSampleGenerator instance
        
        Raises:
            ValueError: if one input is not of the expected type
        """
        _check_isinstance(MentionPairSampleGenerator, generator, "generator")
        super()._check_subcomponent(model_interface, generator, decoder)
    
    @classmethod
    def define_configuration(cls, decoder_factory_configuration, sample_features_vectorizer_factory_configuration, 
                             root_loss, update_mode, classifier_factory_configuration=None,
                              generator_pair_filter_factory_configuration=None):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class.
        
        Args:
            decoder_factory_configuration: a factory configuration that can be used to create 
                a specific _CoreferenceDecoder child class instance
            sample_features_vectorizer_factory_configuration: a factory configuration that can be 
                used to create a specific _BaseExtendedMentionPairSampleFeaturesVectorizer child class instance
            root_loss: a non-negative real number; cf the class documentation
            update_mode: string, either 'PB' or 'ML'; cf the class documentation
            classifier_factory_configuration: a factory configuration that can be used to 
                create a specific _OnlineBinaryClassifier child class instance; or None, in which case the 
                factory configuration for a 'PerceptronOnlineBinaryClassifier' instance (parametrized with 
                'avg=True, warm_start=False') will be used
            generator_pair_filter_factory_configuration: a factory configuration that can be used 
                to create a specific PairFilter instance, that shall be used to generate the factory 
                configuration of an instance of the _Generator child class compatible with this resolver class; or 
                None, in which case the factory configuration for an 'AcceptAllPairFilter' instance will be used

        Returns:
            a Mapping instance
        """
        # Check and process inputs
        if generator_pair_filter_factory_configuration is None:
            generator_pair_filter_factory_configuration = AcceptAllPairFilter.define_factory_configuration()
        
        generator_factory_configuration =\
         MentionPairSampleGenerator.define_factory_configuration(pair_filter_factory_configuration=generator_pair_filter_factory_configuration)
        return super().define_configuration(generator_factory_configuration, decoder_factory_configuration, 
                                                sample_features_vectorizer_factory_configuration, 
                                                root_loss, update_mode, 
                                                classifier_factory_configuration=classifier_factory_configuration)
    
    # Instance methods
    def _generate_edge_samples(self, document, edges):
        """ Creates collection of MentionPairSample instances corresponding to pairs of mentions made 
        from mentions coming from the input document, pairs that are specified by input edges collection.
        
        Only edges whose corresponding mentions pair that can be produced by the underlying generator, 
        from the input document, shall see the creation of a corresponding MentionPairSample instance.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            edges: collection of edges

        Returns:
            a collection of MentionPairSample instances, where all mentions of the pairs are 
            true mentions (no one is the NULL_MENTION)
        """
        return self._generate_mention_pair_edge_samples(document, edges)
    
    def _compute_training_phase_extended_mention_pair_scores(self, document):
        """ Computes scores to be associated to extended mention pairs (so, also for pairs whose 
        antecedent mention is the NULL_MENTION); using classification prediction on the mention pair 
        samples collection generated from the input document; and interpreting the classification 
        confidence scores as weights on the edges of a graph of mentions.
        
        Score values may be artificially created for for pairs whose antecedent mention is the 
        NULL_MENTION, if the underlying generation process is unable to generate such mention pairs.
        The scores are float values.
        The output of this method is destined to be used as input to build a virtual ground truth 
        graph during the "prediction" of an atomic training step carried out using the input document 
        as support.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance

        Returns:
            a (extended_edge2score, true_extended_edge2score, true_coreference_edge2score) 
            triplet, where:
                - 'extended_edge2score' is the "edge => score" map resulting for carrying out classification 
                  prediction on the samples generated from the input document, using the underlying generation 
                  process
                - 'true_extended_edge2score' is a submap of the 'extended_edge2score' map, only 
                  for edges that correspond to mention pairs that are made of mentions that belong to the same 
                  coreference entity, (their corresponding sample have been labelled with the positive class 
                  label), or correspond to non-anaphoric mentions (the subsequent 'mention' of the pair is a 
                  true mention, whereas the antecedent 'mention' of the pair is the NULL_MENTION)
                - 'true_coreference_edge2score' is is a submap of the 'extended_edge2score' map, only 
                  for edges that correspond to mention pairs that are made of mentions that belong to the same 
                  coreference entity (their corresponding sample have been labelled with the positive class 
                  label), which means that none of the mentions of those pairs is the NULL_MENTION
        """
        return self._compute_training_phase_artificially_extended_mention_pair_scores(document)
    
    def _compute_prediction_phase_extended_mention_pair_scores(self, document):
        """ Computes scores to be associated to extended mention pairs (so, also for pairs whose 
        antecedent mention is the NULL_MENTION); using classification prediction on the mention pair 
        samples collection generated from the input document; and interpreting the classification 
        confidence scores as weights on the edges of a graph of mentions.
        
        Score values may be artificially created for for pairs whose antecedent mention is the 
        NULL_MENTION, if the underlying generation process is unable to generate such mention pairs.
        The scores are float values.
        The output of this method is destined to be used as input to the decoding process, in the 
        frame of the prediction of a coreference partition for the input document.
        
        Args:
            document: a Document instance

        Returns:
            an "edge => score" map
        """
        return self._compute_artificially_extended_mention_pair_prediction_pair_scores(document)
    






class MSTResolver(_SimpleDocumentStructuredPairResolverMixIn, _BaseMentionPairMSTResolver):
    """ MST learning-based resolver where the underlying mention pairs generation process does 
    not support the NULL_MENTION, so support for it has to be artificially implemented, and where 
    the decoding procedure relies purely on the use of the Prim algorithm for computing a Maximum  
    Spanning Tree.
    
    Cf the _BaseMentionPairMSTResolver class.
    
    As such, can be instantiated only with a _OnlineBinaryClassifier child class instance, a 
    MentionPairSampleGenerator instance and a MSTPrimCoreferenceDecoder instance.
    
    Arguments:
        model_interface: a _OnlineBinaryClassifier child class instance, representing the interface 
            to the underlying online machine learning model to train and to use
        
        generator: a MentionPairSampleGenerator instance, used to generate, from document(s), 
            machine-learning samples used as input by the underlying machine learning model
        
        decoder: a MSTPrimCoreferenceDecoder instance, used to interpret scores associated to 
            machine-learning samples into a coreference partition, for the document used to generate 
            the scored machine-learning samples
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        update_mode: string, either 'PB' or 'ML'; cf the class documentation
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        sample_type: string specifying the type of the sample used in the frame of the underlying 
            machine-learning task
        
        document_hash2cache: document_hash2cache: a "document hash => cache" map, or None; the 
            current value of the underlying model interface's vectorization cache (cf 
            'cortex.coreference.api.vectorization_cache.py')
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        update_mode: string, either 'PB' or 'ML'; cf the class documentation
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _check_subcomponent(self, model_interface, generator, decoder):
        """ Checks that the input model interface, generator, and decoder are correct; that is: 
        - the model_interface is a _OnlineBinaryClassifier child class instance
        - the generator is a MentionPairSampleGenerator instance
        - the decoder is a MSTPrimCoreferenceDecoder instance
        
        Raises:
            ValueError: if one input is not of the expected type
        """
        _check_isinstance(MSTPrimCoreferenceDecoder, decoder, "decoder")
        super()._check_subcomponent(model_interface, generator, decoder)
    
    @classmethod
    def define_configuration(cls, sample_features_vectorizer_factory_configuration=None, root_loss=1.0, 
                             update_mode="PB", classifier_factory_configuration=None, 
                             generator_pair_filter_factory_configuration=None):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class.
        
        Args:
            sample_features_vectorizer_factory_configuration: a factory configuration that can be 
                used to create a specific _BaseMentionPairSampleFeaturesVectorizer child class instance; 
                or None, in which case the factory configuration for a 'MentionPairSampleFeaturesVectorizer' 
                instance (parametrized with 'quantize=True') will be used
            root_loss: a non-negative real number; cf the class documentation
            update_mode: string, either 'PB' or 'ML'; cf the class documentation
            classifier_factory_configuration: a factory configuration that can be used to 
                create a specific _OnlineBinaryClassifier child class instance; or None, in which case the 
                factory configuration for a 'PerceptronOnlineBinaryClassifier' instance (parametrized with 
                'avg=True, warm_start=False') will be used
            generator_pair_filter_factory_configuration: a factory configuration that can be used 
                to create a specific PairFilter instance, that shall be used to generate the factory 
                configuration of an instance of the _Generator child class compatible with this resolver class; 
                or None, in which case the factory configuration for an 'AcceptAllPairFilter' instance will 
                be used

        Returns:
            a Mapping instance
        """
        if sample_features_vectorizer_factory_configuration is None:
            sample_features_vectorizer_factory_configuration =\
             MentionPairSampleFeaturesVectorizer.define_factory_configuration(quantize=True)
        decoder_factory_configuration = MSTPrimCoreferenceDecoder.define_factory_configuration()
        return super().define_configuration(decoder_factory_configuration, 
                                                sample_features_vectorizer_factory_configuration, 
                                                root_loss, update_mode, 
                                                classifier_factory_configuration=classifier_factory_configuration,
                                                generator_pair_filter_factory_configuration=generator_pair_filter_factory_configuration)




POSSIBLE_EXTENDED_MENTION_PAIR_SAMPLES_VECTORIZER_NAMES = {"extendedmentionpair", ExtendedMentionPairSampleFeaturesVectorizer.__name__, 
                                                           "superextendedmentionpair", SuperExtendedMentionPairSampleFeaturesVectorizer.__name__,
                                                           }
class ExtendedMSTResolver(_SimpleDocumentStructuredPairResolverMixIn, _BaseMSTResolver):
    """ MST learning-based resolver where the underlying mention pairs generation process does 
    support the NULL_MENTION, and where the decoding procedure relies purely on the use of 
    the Prim algorithm for computing a Maximum Spanning Tree.
    
    Cf the _BaseMSTResolver class. 
    
    As such, can be instantiated only with a _OnlineBinaryClassifier child class instance, a 
    ExtendedMentionPairSampleGenerator instance and a MSTPrimCoreferenceDecoder instance.
    The sample features vectorizer class used to parametrize this resolver must inherit from 
    '_BaseExtendedMentionPairSampleFeaturesVectorizer', it must have 'extended' in its name.
    
    Arguments:
        model_interface: a _OnlineBinaryClassifier child class instance, representing the interface 
            to the underlying online machine learning model to train and to use
        
        generator: an ExtendedMentionPairSampleGenerator instance, used to generate, 
            from document(s), machine-learning samples used as input by the underlying machine-learning 
            model
        
        decoder: a MSTPrimCoreferenceDecoder instance, used to interpret scores 
            associated to machine-learning samples into a coreference partition, for the document used 
            to generate the scored machine-learning samples
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        update_mode: string, either 'PB' or 'ML'; cf the class documentation
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        sample_type: string specifying the type of the sample used in the frame of the underlying 
            machine-learning task
        
        document_hash2cache: document_hash2cache: a "document hash => cache" map, or None; the 
            current value of the underlying model interface's vectorization cache (cf 
            'cortex.coreference.api.vectorization_cache.py')
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        update_mode: string, either 'PB' or 'ML'; cf the class documentation
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _check_subcomponent(self, model_interface, generator, decoder):
        """ Checks that the input model interface, generator, and decoder are correct; 
        that is: 
        - the model_interface is a _OnlineBinaryClassifier child class instance
        - the generator is a ExtendedMentionPairSampleGenerator instance
        - the decoder is a MSTPrimCoreferenceDecoder instance
        
        Raises:
            ValueError: if one input is not of the expected type
        """
        _check_isinstance(ExtendedMentionPairSampleGenerator, generator, "generator")
        _check_isinstance(MSTPrimCoreferenceDecoder, decoder, "decoder")
        super()._check_subcomponent(model_interface, generator, decoder)
    
    @classmethod
    def define_configuration(cls, sample_features_vectorizer_factory_configuration=None, root_loss=1.0, 
                             update_mode="PB", classifier_factory_configuration=None, 
                             generator_pair_filter_factory_configuration=None):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class.
        
        Args:
            sample_features_vectorizer_factory_configuration: a factory configuration that can be 
                used to create a specific _BaseExtendedMentionPairSampleFeaturesVectorizer child class 
                instance; or None, in which case the factory configuration for an 
                'ExtendedMentionPairSampleFeaturesVectorizer' instance (parametrized with 
                'quantize=True') will be used
            root_loss: a non-negative real number; cf the class documentation
            update_mode: string, either 'PB' or 'ML'; cf the class documentation
            classifier_factory_configuration: a factory configuration that can be used to 
                create a specific _OnlineBinaryClassifier child class instance; or None, in which case the 
                factory configuration for a 'PerceptronOnlineBinaryClassifier' instance (parametrized with 
                'avg=True, warm_start=False') will be used
            generator_pair_filter_factory_configuration: a factory configuration that can be used 
                to create a specific PairFilter instance, that shall be used to generate the factory 
                configuration of an instance of the _Generator child class compatible with this resolver class; 
                or None, in which case the factory configuration for an 'AcceptAllPairFilter' instance will 
                be used

        Returns:
            a Mapping instance
        """
        # Check and process inputs
        if sample_features_vectorizer_factory_configuration is None:
            sample_features_vectorizer_factory_configuration =\
             ExtendedMentionPairSampleFeaturesVectorizer.define_factory_configuration(quantize=True)
        if generator_pair_filter_factory_configuration is None:
            generator_pair_filter_factory_configuration = AcceptAllPairFilter.define_factory_configuration()
        if sample_features_vectorizer_factory_configuration["name"] not in POSSIBLE_EXTENDED_MENTION_PAIR_SAMPLES_VECTORIZER_NAMES:
            msg = "Incorrect input classifier_configurations name '{}': must belong to '{}'."
            msg = msg.format(classifier_factory_configuration["name"], 
                             POSSIBLE_EXTENDED_MENTION_PAIR_SAMPLES_VECTORIZER_NAMES)
            raise ValueError(msg)
        
        generator_factory_configuration =\
         ExtendedMentionPairSampleGenerator.define_factory_configuration(pair_filter_factory_configuration=generator_pair_filter_factory_configuration)
        decoder_factory_configuration = MSTPrimCoreferenceDecoder.define_factory_configuration()
        return super().define_configuration(generator_factory_configuration, decoder_factory_configuration, 
                                                sample_features_vectorizer_factory_configuration, 
                                                root_loss, update_mode, 
                                                classifier_factory_configuration=classifier_factory_configuration)
    
    # Instance methods
    def _generate_edge_samples(self, document, edges):
        """ Creates collection of MentionPairSample instances corresponding to pairs of mentions made 
        from mentions coming from the input document, pairs that are specified by input edges collection.
        
        Only edges whose corresponding mentions pair that can be produced by the underlying generator, 
        from the input document, shall see the creation of a corresponding MentionPairSample instance.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            edges: collection of edges

        Returns:
            a collection of MentionPairSample instances, where all mentions of the pairs are 
            true mentions (no one is the NULL_MENTION)
        """
        return self._generate_extended_mention_pair_edge_samples(document, edges)
    
    def _compute_training_phase_extended_mention_pair_scores(self, document):
        """ Computes scores to be associated to extended mention pairs (so, also for pairs whose 
        antecedent mention is the NULL_MENTION); using classification prediction on the mention pair 
        samples collection generated from the input document; and interpreting the classification 
        confidence scores as weights on the edges of a graph of mentions.
        
        Score values may be artificially created for for pairs whose antecedent mention is the 
        NULL_MENTION, if the underlying generation process is unable to generate such mention pairs.
        The scores are float values.
        The output of this method is destined to be used as input to build a virtual ground truth 
        graph during the "prediction" of an atomic training step carried out using the input document 
        as support.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance

        Returns:
            a (extended_edge2score, true_extended_edge2score, true_coreference_edge2score) 
            triplet, where:
                - 'extended_edge2score' is the "edge => score" map resulting for carrying out classification 
                  prediction on the samples generated from the input document, using the underlying generation 
                  process
                - 'true_extended_edge2score' is a submap of the 'extended_edge2score' map, only 
                  for edges that correspond to mention pairs that are made of mentions that belong to the same 
                  coreference entity, (their corresponding sample have been labelled with the positive class 
                  label), or correspond to non-anaphoric mentions (the subsequent 'mention' of the pair is a 
                  true mention, whereas the antecedent 'mention' of the pair is the NULL_MENTION)
                - 'true_coreference_edge2score' is is a submap of the 'extended_edge2score' map, only 
                  for edges that correspond to mention pairs that are made of mentions that belong to the same 
                  coreference entity (their corresponding sample have been labelled with the positive class 
                  label), which means that none of the mentions of those pairs is the NULL_MENTION
        """
        return self._compute_training_phase_naturally_extended_mention_pair_scores(document)

    def _compute_prediction_phase_extended_mention_pair_scores(self, document):
        """ Computes scores to be associated to extended mention pairs (so, also for pairs whose 
        antecedent mention is the NULL_MENTION); using classification prediction on the mention pair 
        samples collection generated from the input document; and interpreting the classification 
        confidence scores as weights on the edges of a graph of mentions.
        
        Score values may be artificially created for for pairs whose antecedent mention is the 
        NULL_MENTION, if the underlying generation process is unable to generate such mention pairs.
        The scores are float values.
        The output of this method is destined to be used as input to the decoding process, in the 
        frame of the prediction of a coreference partition for the input document.
        
        Args:
            document: a Document instance

        Returns:
            an "edge => score" map
        """
        return self._compute_naturally_extended_mention_pair_prediction_pair_scores(document)
    





########## Hybrid Best-MST ##########
class HybridBestFirstMSTResolver(_SimpleDocumentStructuredPairResolverMixIn, _BaseMentionPairMSTResolver):
    """ Resolver using MST to compute its graph structure, but using first a 'Best' algorithm to 
    determine edges for pairs of mentions whose subsequent mention is a pronoun and whose antecedent 
    mention is not a pronoun.
    
    The pairs created following this procedure are considered to be Must-Links, 
    and the pairs that were rejected are considered Cannot-Links, for the Maximum Spanning Tree 
    computation that follows. The underlying mention pairs generation process does 
    not support the NULL_MENTION, so support for it has to be artificially implemented.
    
    Cf the _BaseMentionPairMSTResolver class.
    
    As such, can be instantiated only with a _OnlineBinaryClassifier child class instance, a 
    MentionPairSampleGenerator instance and a HybridBestFirstMSTCoreferenceDecoder instance.
    
    Arguments:
        model_interface: a _OnlineBinaryClassifier child class instance, representing the interface 
            to the underlying online machine learning model to train and to use
        
        generator: a MentionPairSampleGenerator instance, used to generate, from document(s), 
            machine-learning samples used as input by the underlying machine learning model
        
        decoder: a HybridBestFirstMSTCoreferenceDecoder instance, used to interpret scores associated to 
            machine-learning samples into a coreference partition, for the document used to generate 
            the scored machine-learning samples
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        update_mode: string, either 'PB' or 'ML'; cf the class documentation
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        sample_type: string specifying the type of the sample used in the frame of the underlying 
            machine-learning task
        
        document_hash2cache: document_hash2cache: a "document hash => cache" map, or None; the 
            current value of the underlying model interface's vectorization cache (cf 
            'cortex.coreference.api.vectorization_cache.py')
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        update_mode: string, either 'PB' or 'ML'; cf the class documentation
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _check_subcomponent(self, model_interface, generator, decoder):
        """ Checks that the input model interface, generator, and decoder are correct; that is: 
        - the model_interface is a _OnlineBinaryClassifier child class instance
        - the generator is a MentionPairSampleGenerator instance
        - the decoder is a HybridBestFirstMSTCoreferenceDecoder instance
        
        Raises:
            ValueError: if one input is not of the expected type
        """
        _check_isinstance(HybridBestFirstMSTCoreferenceDecoder, decoder, "decoder")
        super()._check_subcomponent(model_interface, generator, decoder)
    
    @classmethod
    def define_configuration(cls, sample_features_vectorizer_factory_configuration=None, root_loss=1.0, 
                             update_mode="PB", classifier_factory_configuration=None, 
                             generator_pair_filter_factory_configuration=None):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class.
        
        Args:
            sample_features_vectorizer_factory_configuration: a factory configuration that can be 
                used to create a specific _BaseMentionPairSampleFeaturesVectorizer child class instance; 
                or None, in which case the factory configuration for a 'MentionPairSampleFeaturesVectorizer' 
                instance (parametrized with 'quantize=True') will be used
            root_loss: a non-negative real number; cf the class documentation
            update_mode: string, either 'PB' or 'ML'; cf the class documentation
            classifier_factory_configuration: a factory configuration that can be used to 
                create a specific _OnlineBinaryClassifier child class instance; or None, in which case the 
                factory configuration for a 'PerceptronOnlineBinaryClassifier' instance (parametrized with 
                'avg=True, warm_start=False') will be used
            generator_pair_filter_factory_configuration: a factory configuration that can be used 
                to create a specific PairFilter instance, that shall be used to generate the factory 
                configuration of an instance of the _Generator child class compatible with this resolver class; 
                or None, in which case the factory configuration for an 'AcceptAllPairFilter' instance will 
                be used

        Returns:
            a Mapping instance
        """
        # Check and process inputs
        if sample_features_vectorizer_factory_configuration is None:
            sample_features_vectorizer_factory_configuration =\
             MentionPairSampleFeaturesVectorizer.define_factory_configuration(quantize=True)
        
        decoder_factory_configuration = HybridBestFirstMSTCoreferenceDecoder.define_factory_configuration()
        return super().define_configuration(decoder_factory_configuration, 
                                                sample_features_vectorizer_factory_configuration, 
                                                root_loss, update_mode, 
                                                classifier_factory_configuration=classifier_factory_configuration,
                                                generator_pair_filter_factory_configuration=generator_pair_filter_factory_configuration)
    
    # Instance methods
    def _compute_ground_truth_graph_edges(self, document, extended_edge2score, true_extended_edge2score, 
                                          true_coreference_edge2score): #, entity_head_extents=None
        """ Computes a latent "ground truth" graph associated to the mentions of the input document 
        (that is to say, a graph that takes into account the info brought by the ground truth 
        coreference partition associated to the input document), and return the edges of this graph.
        
        Here, it is computed as a forest (one tree per coreference entity), and with the addition 
        of edges between, on the one hand, nodes corresponding to non-anaphoric mentions, and one 
        the other hand, the root node corresponding to the NULL_MENTION, so as to create a proper 
        tree.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            extended_edge2score: an "edge => score" map, where the edges correspond to all mention 
                pairs that are implicitly defined by the application of the underlying generation process on 
                the input document. Not used here, exists only for API compatibility reasons.
            true_extended_edge2score: an "edge => score" map, where the edges correspond to 
                mention pairs that are implicitly defined by the application of the underlying generation 
                process on the input document, and which are associated to the positive class. Not used here, 
                exists only for API compatibility reasons.
            true_coreference_edge2score: an "edge => score" map, where the edges correspond to 
                mention pairs that are implicitly defined by the application of the underlying generation 
                process on the input document, and which correspond to true mentions belonging to the same 
                coreference entity

        Returns:
            a collection of edges
        """
        # Compute latent true forest (one tree per cluster) and add links to root to create true tree
        ## Define the collections of edges we respectively want and do not want in the final tree, 
        ## applying the 'Best' algorithm for subsequent mentions which are pronominal mentions
        entities = tuple(document.coreference_partition)
        ground_truth_graph_best_edges = []
        ground_truth_graph_nonbest_edges = []
        for entity in entities:
            sorted_mention_extents = sorted(entity)
            sorted_mentions = tuple(document.extent2mention[extent] for extent in sorted_mention_extents)
            entity_size = len(entity)
            first_mention = sorted_mentions[0]
            ground_truth_graph_best_edges.append(get_mention_pair_extent_pair(NULL_MENTION, first_mention))
            for j in range(1,entity_size): # We start from 1 since the first mention has already been forcibly associated with the NULL_MENTION (see above).
                subsequent_mention = sorted_mentions[j]
                if not MENTION_FEATURES.is_expanded_pronoun(subsequent_mention):
                    continue
                max_score = -numpy.inf
                max_score_antecedent_mention = None
                for i in range(j-1, -1, -1):
                    antecedent_mention = sorted_mentions[i]
                    # Skip pronominal antecent mention for non-pronominal anaphora
                    if MENTION_FEATURES.is_expanded_pronoun(antecedent_mention) and not MENTION_FEATURES.is_expanded_pronoun(subsequent_mention):
                        continue
                    score = true_coreference_edge2score.get(get_mention_pair_extent_pair(antecedent_mention,subsequent_mention))
                    if score is None:
                        continue
                    if score > max_score:
                        max_score = score
                        max_score_antecedent_mention = antecedent_mention
                if max_score_antecedent_mention is None:
                    max_score_antecedent_mention = NULL_MENTION
                ground_truth_graph_best_edges.append(get_mention_pair_extent_pair(max_score_antecedent_mention,subsequent_mention))
                # Remove non selected edges (i.e. at most one backward attachment for pronouns)
                for i in range(j-1, -1, -1):
                    antecedent_mention = sorted_mentions[i]
                    if antecedent_mention.extent != max_score_antecedent_mention.extent:
                        ground_truth_graph_nonbest_edges.append(get_mention_pair_extent_pair(antecedent_mention,subsequent_mention))
        
        ## Compute the MST
        mention_extents = tuple(itertools.chain((NULL_MENTION.extent,), (m.extent for m in document.mentions)))
        _, true_mst_edges, _ = kruskal_mst(mention_extents, true_coreference_edge2score, 
                                           positive_edges=ground_truth_graph_best_edges, 
                                           negative_edges=ground_truth_graph_nonbest_edges, 
                                           spanning_tree_type="max")
        # FIXME: can I remove this legacy commented out code?
#        true_forest = kruskal_msf(mentions, cluster_scores, positive_edges=ground_truth_graph_best_edges, negative_edges=ground_truth_graph_nonbest_edges)
#        true_mst_edges = []
##        print "##### NB ENTITES =", len(document.get_entities()), " ### NB TRUE CLUSTERS =", len(true_forest)
#        for nodes, edges, _ in true_forest:
#            leftmost = sorted(nodes)[0] # leftmost mention linked to the root
#            true_mst_edges += [(NULL_MENTION, leftmost)] + edges
        
        return true_mst_edges
    
    def _compute_predicted_tree_edges_from_edge2loss_score(self, document, edge2weight): #, entity_head_extents=None
        """ Computes a latent "predicted" graph associated to the mentions of the input document
        (that is to say, a graph that does not take into account the info brought by the ground truth 
        coreference partition associated to the input document, but only relies on weight values 
        associated to possible edges), and return the edges of this graph.
        
        Here, the graph is in fact a tree.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            edge2weight: an "edge => weigt" map, where the edges correspond to all mention pairs 
                that are implicitly defined by the application of the underlying generation process on the 
                input document

        Returns:
            a collection of edges corresponding to the computed "predicted" graph
        """
        mentions = document.mentions
        
        ## Define the collections of edges we respectively want and do not want in the final tree, applying the 'Best' algorithm
        predicted_graph_best_edges = []
        predicted_graph_nonbest_edges = []
        for j in range(len(mentions)):
            subsequent_mention = mentions[j]
            if not MENTION_FEATURES.is_expanded_pronoun(subsequent_mention):
                continue
            max_score = 0. # FIXME: Because the loss score values are all supposed to be >= 0?
            # Is it supposed to be the decoding threshold values associated to scores that are not proba values?
            max_score_antecedent_mention = None
            for i in range(j-1, -1, -1):
                antecedent_mention = mentions[i]
                # Skip pronominal antecent mention for non-pronominal anaphora
                if MENTION_FEATURES.is_expanded_pronoun(antecedent_mention) and not MENTION_FEATURES.is_expanded_pronoun(subsequent_mention):
                    continue
                score = edge2weight.get(get_mention_pair_extent_pair(antecedent_mention,subsequent_mention)) # Some pairs can have been filtered out
                if score is None:
                    continue
                if score > max_score:
                    max_score = score
                    max_score_antecedent_mention = antecedent_mention
            if max_score_antecedent_mention is None:
                max_score_antecedent_mention = NULL_MENTION
            predicted_graph_best_edges.append(get_mention_pair_extent_pair(max_score_antecedent_mention,subsequent_mention))
            # Remove non selected edges (i.e. at most one backward attachment for pronouns)
            for i in range(j-1, -1, -1):
                antecedent_mention = mentions[i]                
                if antecedent_mention.extent != max_score_antecedent_mention.extent:
                    predicted_graph_nonbest_edges.append(get_mention_pair_extent_pair(antecedent_mention,subsequent_mention))
        
        ## Compute the MST
        for mention in mentions:
            edge2weight[get_mention_pair_extent_pair(NULL_MENTION,mention)] = 0.
        mention_extents = tuple(m.extent for m in itertools.chain((NULL_MENTION,), mentions))
        _, pred_mst_edges, _ = kruskal_mst(mention_extents, edge2weight, 
                                           positive_edges=predicted_graph_best_edges, 
                                           negative_edges=predicted_graph_nonbest_edges, 
                                           spanning_tree_type="max")
        
        return pred_mst_edges






###### Constrained resolvers ######
class ConstrainedMSTResolver(_ConstrainedDocumentStructuredPairResolverMixIn, 
                             ConstrainedEdgesDefinerMixIn, 
                             _BaseMentionPairMSTResolver):
    """ MST learning-based resolver where the underlying mention pairs generation process does 
    not support the NULL_MENTION, so support for it has to be artificially implemented, and where 
    the decoding procedure relies purely on the use of the Kruskal algorithm for computing a Maximum 
    Spanning Tree, so as to be able to use and account for constraints on possible edges.
    
    Cf the _BaseMentionPairMSTResolver class.
    Cf the ConstrainedEdgesDefinerMixIn class.
    
    As such, can be instantiated only with a _OnlineBinaryClassifier child class instance, a 
    MentionPairSampleGenerator instance and a MSTKruskalCoreferenceDecoder instance.
    
    Arguments:
        model_interface: a _OnlineBinaryClassifier child class instance, representing the interface 
            to the underlying online machine learning model to train and to use
        
        generator: a MentionPairSampleGenerator instance, used to generate, from document(s), 
            machine-learning samples used as input by the underlying machine learning model
        
        decoder: a MSTKruskalCoreferenceDecoder instance, used to interpret scores associated to 
            machine-learning samples into a coreference partition, for the document used to generate 
            the scored machine-learning samples
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        update_mode: string, either 'PB' or 'ML'; cf the class documentation
        
        constraints_definition: None, collection of (constraint_type, list or tuple, dict) 
            tuples, where: 
                - 'constraint type' must be one of the possible constraint types (cf this class' docstring)
                - 'args' and 'kwargs' will be passed along to the constraints function; for now, none of them 
                  requires supplementary parameters, so args and kwargs must remain empty for them
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        sample_type: string specifying the type of the sample used in the frame of the underlying 
            machine-learning task
        
        document_hash2cache: document_hash2cache: a "document hash => cache" map, or None; the 
            current value of the underlying model interface's vectorization cache (cf 
            'cortex.coreference.api.vectorization_cache.py')
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        update_mode: string, either 'PB' or 'ML'; cf the class documentation
        
        constraints_definition: None, collection of (constraint_type, list or tuple, dict) 
            tuples, where: 
                - 'constraint type' must be one of the possible constraint types (cf this class' docstring)
                - 'args' and 'kwargs' will be passed along to the constraints function; for now, none of them 
                  requires supplementary parameters, so args and kwargs must remain empty for them
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = get_unique_elts(_BaseMentionPairMSTResolver._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION\
                                                       + ConstrainedEdgesDefinerMixIn._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION)
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = _TreeLossDocumentStructuredPairResolver._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION+\
                                            get_unique_elts(ConstrainedEdgesDefinerMixIn._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION)
    
    def __init__(self, model_interface, generator, decoder, root_loss, 
                 update_mode, constraints_definition, configuration=None):
        # Check input values
        # Initialize using parent class
        _BaseMentionPairMSTResolver.__init__(self, model_interface, generator, decoder, root_loss, 
                                            update_mode, configuration=configuration)
        ConstrainedEdgesDefinerMixIn.__init__(self, constraints_definition, configuration=self.configuration)
    
    def _check_subcomponent(self, model_interface, generator, decoder):
        """ Checks that the input model interface, generator, and decoder are correct; that is: 
        - the model_interface is a _OnlineBinaryClassifier child class instance
        - the generator is a MentionPairSampleGenerator instance
        - the decoder is a MSTPrimCoreferenceDecoder instance
        
        Raises:
            ValueError: if one input is not of the expected type
        """
        _check_isinstance(MSTKruskalCoreferenceDecoder, decoder, "decoder")
        super()._check_subcomponent(model_interface, generator, decoder)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args1, kwargs1 = _BaseMentionPairMSTResolver._extract_args_kwargs_from_configuration(configuration)
        args2, kwargs2 = ConstrainedEdgesDefinerMixIn._extract_args_kwargs_from_configuration(configuration)
        args = args1 + args2
        kwargs = kwargs1
        kwargs.update(kwargs2)
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, sample_features_vectorizer_factory_configuration=None, root_loss=1.0, 
                             update_mode="PB", classifier_factory_configuration=None, 
                             generator_pair_filter_factory_configuration=None, 
                             constraints_definition=None):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class.
        
        Args:
            sample_features_vectorizer_factory_configuration: a factory configuration that can be 
                used to create a specific _BaseMentionPairSampleFeaturesVectorizer child class instance; 
                or None, in which case the factory configuration for a 'MentionPairSampleFeaturesVectorizer' 
                instance (parametrized with 'quantize=True') will be used
            root_loss: a non-negative real number; cf the class documentation
            update_mode: string, either 'PB' or 'ML'; cf the class documentation
            classifier_factory_configuration: a factory configuration that can be used to 
                create a specific _OnlineBinaryClassifier child class instance; or None, in which case the factory 
                configuration for a 'PerceptronOnlineBinaryClassifier' instance (parametrized with 
                'avg=True, warm_start=False') will be used
            generator_pair_filter_factory_configuration: a factory configuration that can be used 
                to create a specific PairFilter instance, that shall be used to generate the factory 
                configuration of an instance of the _Generator child class compatible with this resolver class; 
                or None, in which case the factory configuration for an 'AcceptAllPairFilter' instance will 
                be used
            constraints_definition: None, collection of (constraint_type, list or tuple, dict) 
                tuples, where: 
                - 'constraint type' must be one of the possible constraint types (cf this class' docstring)
                - 'args' and 'kwargs' will be passed along to the constraints function; for now, none of them 
                requires supplementary parameters, so args and kwargs must remain empty for them

        Returns:
            a Mapping instance
        """
        if sample_features_vectorizer_factory_configuration is None:
            sample_features_vectorizer_factory_configuration =\
             MentionPairSampleFeaturesVectorizer.define_factory_configuration(quantize=True)
        
        decoder_factory_configuration = MSTKruskalCoreferenceDecoder.define_factory_configuration()
        
        configuration1 = ConstrainedEdgesDefinerMixIn.define_configuration(constraints_definition=constraints_definition)
        configuration2 = _BaseMentionPairMSTResolver.define_configuration(decoder_factory_configuration, 
                                                                          sample_features_vectorizer_factory_configuration, 
                                                                          root_loss, update_mode, 
                                                                          classifier_factory_configuration=classifier_factory_configuration,
                                                                          generator_pair_filter_factory_configuration=generator_pair_filter_factory_configuration)
        type(configuration1).update(configuration1, configuration2)
        return configuration1
    
    # Instance methods
    def _compute_ground_truth_graph_edges(self, document, extended_edge2score, true_extended_edge2score, 
                                          true_coreference_edge2score): #, entity_head_extents=None
        """ Computes a latent "ground truth" graph associated to the mentions of the input document 
        (that is to say, a graph that takes into account the info brought by the ground truth 
        coreference partition associated to the input document), and return the edges of this graph.
        
        Here, it is computed as a forest (one tree per coreference entity), and with the addition 
        of edges between, on the one hand, nodes corresponding to non-anaphoric mentions, and one 
        the other hand, the root node corresponding to the NULL_MENTION, so as to create a proper 
        tree. Also, additional constraints are taken into account when computing the initial forest.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            extended_edge2score: an "edge => score" map, where the edges correspond to all mention 
                pairs that are implicitly defined by the application of the underlying generation process on 
                the input document. Not used here, exists only for API compatibility reasons.
            true_extended_edge2score: an "edge => score" map, where the edges correspond to 
                mention pairs that are implicitly defined by the application of the underlying generation 
                process on the input document, and which are associated to the positive class. Not used here, 
                exists only for API compatibility reasons.
            true_coreference_edge2score: an "edge => score" map, where the edges correspond to 
                mention pairs that are implicitly defined by the application of the underlying generation 
                process on the input document, and which correspond to true mentions belonging to the same 
                coreference entity

        Returns:
            a collection of edges
        """
        # Get constraints
        positive_edges, negative_edges = self.get_constraints(document) #, entity_head_extents=entity_head_extents
        # Compute the true (sub)tree: compute trees corresponding to each coreference entity, then add to each tree the NULL_MENTION root and the (NULL_MENTION,entity_head) edge
        mention_extents = tuple(m.extent for m in document.mentions)
        # FIXME: in the following function, we use python's ordering on tuples of numbers, not the ordering defined on Mention instances
        true_forest = kruskal_msf(mention_extents, true_coreference_edge2score, positive_edges=positive_edges, 
                                  negative_edges=negative_edges, spanning_tree_type="max")
        ground_truth_graph_edges = []
        for nodes, edges, _ in true_forest:
            # FIXME: in the following, we use python's ordering on tuples of numbers, not the ordering defined on Mention instances
            first_mention_extent = sorted(nodes)[0] # The first (leftmost for left to right reading order languages) mention is linked to the root
            ground_truth_graph_edges += [(NULL_MENTION.extent, first_mention_extent)] + edges
        return ground_truth_graph_edges
    
    def _compute_predicted_tree_edges_from_edge2loss_score(self, document, edge2weight): #, entity_head_extents=None
        """ Computes a latent "predicted" graph associated to the mentions of the input document
        (that is to say, a graph that does not take into account the info brought by the ground truth 
        coreference partition associated to the input document, but only relies on weight values 
        associated to possible edges), and return the edges of this graph.
        
        Here, the graph is in fact a tree.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            edge2weight: an "edge => weigt" map, where the edges correspond to all mention pairs 
                that are implicitly defined by the application of the underlying generation process on the 
                input document

        Returns:
            a collection of edges corresponding to the computed "predicted" graph
        """
        # Get constraints
        positive_edges, negative_edges = self.get_constraints(document) #, entity_head_extents=entity_head_extents
        # Predict edges
        mention_extents = tuple(m.extent for m in itertools.chain((NULL_MENTION,), document.mentions))
        _, predicted_graph_edges, _ = kruskal_mst(mention_extents, edge2weight, 
                                                positive_edges=positive_edges, 
                                                negative_edges=negative_edges, 
                                                spanning_tree_type="max")
        return predicted_graph_edges




########## TRANSITIVE CLOSURE ##########
class TransitiveClosureStructureResolver(_SimpleDocumentStructuredPairResolverMixIn, 
                                         _DocumentStructuredPairResolver):
    """ Class corresponding to a document structured resolver where the latent structure is a graph 
    where:
        - an edge of the 'true' graph structure is defined for each existing coreference link between 
          mentions
        - an edge of the 'predicted' graph structure is defined for each coreference link between mention 
          in the coreference partition defined by joining together all the mention whose pair's score is 
          positive.
    
    The underlying mention pairs generation process does not support the NULL_MENTION, so support 
    for it has to be artificially implemented
    The loss computed from the comparison of the predicted graph to the ground truth graph is the 
    number of edges in the symmetric difference of the respective set of edges associated to each 
    graph.
    
    Cf the _DocumentStructuredPairResolver class.
    
    As such, can be instantiated only with a _OnlineBinaryClassifier child class instance, a 
    MentionPairSampleGenerator instance and a AggressiveMergeCoreferenceDecoder instance.
    
    Arguments:
        model_interface: a _OnlineBinaryClassifier child class instance, representing the interface 
            to the underlying online machine learning model to train and to use
        
        generator: a MentionPairSampleGenerator instance, used to generate, from document(s), 
            machine-learning samples used as input by the underlying machine learning model
        
        decoder: a AggressiveMergeCoreferenceDecoder instance, used to interpret scores associated to 
            machine-learning samples into a coreference partition, for the document used to generate 
            the scored machine-learning samples
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        sample_type: string specifying the type of the sample used in the frame of the underlying 
            machine-learning task
        
        document_hash2cache: document_hash2cache: a "document hash => cache" map, or None; the 
            current value of the underlying model interface's vectorization cache (cf 
            'cortex.coreference.api.vectorization_cache.py')
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _check_subcomponent(self, model_interface, generator, decoder):
        """ Checks that the input model interface, generator, and decoder are correct; that is: 
        - the model_interface is a _OnlineBinaryClassifier child class instance
        - the generator is a MentionPairSampleGenerator instance
        - the decoder is a AggressiveMergeCoreferenceDecoder instance
        
        Raises:
            ValueError: if one input is not of the expected type
        """
        _check_isinstance(MentionPairSampleGenerator, generator, "generator")
        _check_isinstance(AggressiveMergeCoreferenceDecoder, decoder, "decoder")
        super()._check_subcomponent(model_interface, generator, decoder)
    
    @classmethod
    def define_configuration(cls, sample_features_vectorizer_factory_configuration=None, 
                             classifier_factory_configuration=None, 
                             generator_pair_filter_factory_configuration=None):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class.
        
        Args:
            sample_features_vectorizer_factory_configuration: a factory configuration that can be 
                used to create a specific _BaseMentionPairSampleFeaturesVectorizer child class instance; 
                or None, in which case the factory configuration for a 'MentionPairSampleFeaturesVectorizer' 
                instance (parametrized with 'quantize=True') will be used
            classifier_factory_configuration: a factory configuration that can be used to 
                create a specific _OnlineBinaryClassifier child class instance; or None, in which case the 
                factory configuration for a 'PerceptronOnlineBinaryClassifier' instance (parametrized with 
                'avg=True, warm_start=False') will be used
            generator_pair_filter_factory_configuration: a factory configuration that can be used 
                to create a specific PairFilter instance, that shall be used to generate the factory 
                configuration of an instance of the _Generator child class compatible with this resolver class; 
                or None, in which case the factory configuration for an 'AcceptAllPairFilter' instance will 
                be used

        Returns:
            a Mapping instance
        """
        # Check and process inputs
        if sample_features_vectorizer_factory_configuration is None:
            sample_features_vectorizer_factory_configuration =\
             MentionPairSampleFeaturesVectorizer.define_factory_configuration(quantize=True)
        if generator_pair_filter_factory_configuration is None:
            generator_pair_filter_factory_configuration = AcceptAllPairFilter.define_factory_configuration()
        
        generator_factory_configuration =\
         MentionPairSampleGenerator.define_factory_configuration(pair_filter_factory_configuration=generator_pair_filter_factory_configuration)
        decoder_factory_configuration = AggressiveMergeCoreferenceDecoder.define_factory_configuration()
        return super().define_configuration(generator_factory_configuration, decoder_factory_configuration, 
                                               sample_features_vectorizer_factory_configuration, 
                                               classifier_factory_configuration=classifier_factory_configuration)
    
    # Instance methods
    def _compute_training_phase_extended_mention_pair_scores(self, document):
        """ Computes scores to be associated to extended mention pairs (so, also for pairs whose 
        antecedent mention is the NULL_MENTION); using classification prediction on the mention pair 
        samples collection generated from the input document; and interpreting the classification 
        confidence scores as weights on the edges of a graph of mentions.
        
        Score values may be artificially created for for pairs whose antecedent mention is the 
        NULL_MENTION, if the underlying generation process is unable to generate such mention pairs.
        The scores are float values.
        The output of this method is destined to be used as input to build a virtual ground truth 
        graph during the "prediction" of an atomic training step carried out using the input document 
        as support.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance

        Returns:
            a (extended_edge2score, true_extended_edge2score, true_coreference_edge2score) 
            triplet, where:
                - 'extended_edge2score' is the "edge => score" map resulting for carrying out classification 
                  prediction on the samples generated from the input document, using the underlying generation 
                  process
                - 'true_extended_edge2score' is a submap of the 'extended_edge2score' map, only 
                  for edges that correspond to mention pairs that are made of mentions that belong to the same 
                  coreference entity, (their corresponding sample have been labelled with the positive class 
                  label), or correspond to non-anaphoric mentions (the subsequent 'mention' of the pair is a 
                  true mention, whereas the antecedent 'mention' of the pair is the NULL_MENTION)
                - 'true_coreference_edge2score' is is a submap of the 'extended_edge2score' map, only 
                  for edges that correspond to mention pairs that are made of mentions that belong to the same 
                  coreference entity (their corresponding sample have been labelled with the positive class 
                  label), which means that none of the mentions of those pairs is the NULL_MENTION
        """
        return self._compute_training_phase_artificially_extended_mention_pair_scores(document)
    
    def _generate_edge_samples(self, document, edges):
        """ Creates collection of MentionPairSample instances corresponding to pairs of mentions made 
        from mentions coming from the input document, pairs that are specified by input edges collection.
        
        Only edges whose corresponding mentions pair that can be produced by the underlying generator, 
        from the input document, shall see the creation of a corresponding MentionPairSample instance.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            edges: collection of edges

        Returns:
            collection of MentionPairSample instances, where all mentions of the pairs are 
        t    rue mentions (no one is the NULL_MENTION)
        """
        return self._generate_mention_pair_edge_samples(document, edges)
    
    def _compute_prediction_phase_extended_mention_pair_scores(self, document):
        """ Computes scores to be associated to extended mention pairs (so, also for pairs whose 
        antecedent mention is the NULL_MENTION); using classification prediction on the mention pair 
        samples collection generated from the input document; and interpreting the classification 
        confidence scores as weights on the edges of a graph of mentions.
        
        Score values may be artificially created for for pairs whose antecedent mention is the 
        NULL_MENTION, if the underlying generation process is unable to generate such mention pairs.
        The scores are float values.
        The output of this method is destined to be used as input to the decoding process, in the 
        frame of the prediction of a coreference partition for the input document.
        
        Args:
            document: a Document instance

        Returns:
            an "edge => score" map
        """
        return self._compute_artificially_extended_mention_pair_prediction_pair_scores(document)
    
    def _compute_ground_truth_graph_edges(self, document, extended_edge2score, true_extended_edge2score, 
                                          true_coreference_edge2score): #, entity_head_heads=None
        """ Computes a latent "ground truth" graph associated to the mentions of the input document 
        (that is to say, a graph that takes into account the info brought by the ground truth 
        coreference partition associated to the input document), and return the edges of this graph.
        
        Here, it is computed such that there exist one edge for each (antecedent mention, subsequent 
        mention) that can be defined for each coreference entity that exists in the coreference 
        partition associated to the input document.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            extended_edge2score: an "edge => score" map, where the edges correspond to all mention 
                pairs that are implicitly defined by the application of the underlying generation process on 
                the input document. Not used here, exists only for API compatibility reasons.
            true_extended_edge2score: an "edge => score" map, where the edges correspond to 
                mention pairs that are implicitly defined by the application of the underlying generation 
                process on the input document, and which are associated to the positive class. Not used here, 
                exists only for API compatibility reasons.
            true_coreference_edge2score: an "edge => score" map, where the edges correspond to 
                mention pairs that are implicitly defined by the application of the underlying generation 
                process on the input document, and which correspond to true mentions belonging to the same 
                coreference entity. Not used here, exists only for API compatibility reasons.

        Returns:
            a collection of edges
        """
        # Compute latent true chains and link to root (provides a tree)
        entities = tuple(document.coreference_partition)
        ground_truth_graph_edges = []
        for entity in entities:
            mention_extents = sorted(entity)
            for subsequent_mention_extent in mention_extents:
                subsequent_mention = document.extent2mention[subsequent_mention_extent]
                for antecedent_mention_extent in mention_extents:
                    antecedent_mention = document.extent2mention[antecedent_mention_extent]
                    if antecedent_mention >= subsequent_mention:
                        break
                    ground_truth_graph_edges.append(get_mention_pair_extent_pair(antecedent_mention,subsequent_mention))
        return ground_truth_graph_edges

    def _compute_predicted_graph_edges(self, document, extended_edge2score, ground_truth_graph_edges): #, entity_head_heads=None
        """ Computes a latent "predicted" graph associated to the mentions of the input document
        (that is to say, a graph that does not take into account the info brought by the ground truth 
        coreference partition associated to the input document, but only relies on score values 
        associated to possible edges, as well as the "ground truth" graph that it will be compared 
        to), and return the edges of this graph.
        
        Here, the graphs are in fact trees.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            extended_edge2weight: an "edge => score" map, where the edges correspond to all 
                mention pairs that are implicitly defined by the application of the underlying generation 
                process on the input document
            ground_truth_graph_edges: collection of edges of the latent "ground truth" graph 
                that has been associated to the mentions of the input document

        Returns:
            a collection of edges
        """
        # Compute predicted chain, no loss maximization available
        mentions = document.mentions
        predicted_graph_edges = []
        # FIXME: maybe we can replace this part by a call to the associated CoreferenceDecoder?
        # No, not exactly, because here we are interested by some specific pairs, not all the pair that can be made from the elements belonging to a set / entity; 
        # and a CoreferencePartition instance can only give us that info.
        # FIXME: assumes that the score are not proba, since a threshold value of 0 is used below!
        # But some code can be factorized alright.
        # Join together all mentions whose pair was given a positive score
        coreference_partition = CoreferencePartition(mention_extents=(m.extent for m in mentions))
        for subsequent_mention in mentions:
            for antecedent_mention in mentions:
                extent_pair = get_mention_pair_extent_pair(antecedent_mention,subsequent_mention)
                if extent_pair not in extended_edge2score: # Some pairs can have been filtered out
                    continue
                # FIXME: in the associated decoder class, we enumerate pairs of mentions using range and index, whereas here we directly use ordering relation: check that there is an equivalency, or resolve the inconsistency of use
                if antecedent_mention >= subsequent_mention:
                    break
                if extended_edge2score[extent_pair] > 0:
                    coreference_partition.join(*extent_pair)
        # Create an edge for each coreference link defined by the coreference partition
        for subsequent_mention in mentions:
            for antecedent_mention in mentions:
                if antecedent_mention >= subsequent_mention:
                    break
                extent_pair = get_mention_pair_extent_pair(antecedent_mention,subsequent_mention)
                if coreference_partition.are_coreferent(*extent_pair):
                    predicted_graph_edges.append(extent_pair)
        return predicted_graph_edges

    def _compute_graph_loss(self, ground_truth_graph_edges, predicted_graph_edges):
        """ Computes the loss value, resulting from the comparison of the computed predicted graph 
        to the computed ground truth graph.
        
        The graphs are defined here only through the respective set of edges that constitute them.
        The output loss value results from the addition of loss values incurred by edges of the 
        predicted graph which is being compared to the ground truth graph.
        
        Args:
            predicted_graph_edges: collection of edges defining the predicted graph
            ground_truth_graph_edges: collection of edges defining the ground truth graph

        Returns:
            a float, the computed loss value
        """
        return len(set(ground_truth_graph_edges).symmetric_difference(set(predicted_graph_edges)))



########## POSITIVE GRAPH ##########
class PositiveGraphStructureResolver(TransitiveClosureStructureResolver):
    """ Class corresponding to a document structured resolver where the latent structure is a graph 
    where:
        - an edge of the 'true' graph structure is defined for each existing coreference link between 
          mentions
        - an edge of the 'predicted' graph structure is defined for each mention pair whose score is 
          positive
    
    The underlying mention pairs generation process does not support the NULL_MENTION, so support 
    for it has to be artificially implemented
    
    Cf the TransitiveClosureStructureResolver class.
    
    As such, can be instantiated only with a _OnlineBinaryClassifier child class instance, a 
    MentionPairSampleGenerator instance and a AggressiveMergeCoreferenceDecoder instance.
    
    Arguments:
        model_interface: a _OnlineBinaryClassifier child class instance, representing the interface 
            to the underlying online machine learning model to train and to use
        
        generator: a MentionPairSampleGenerator instance, used to generate, from document(s), 
            machine-learning samples used as input by the underlying machine learning model
        
        decoder: a AggressiveMergeCoreferenceDecoder instance, used to interpret scores associated to 
            machine-learning samples into a coreference partition, for the document used to generate 
            the scored machine-learning samples
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        sample_type: string specifying the type of the sample used in the frame of the underlying 
            machine-learning task
        
        document_hash2cache: document_hash2cache: a "document hash => cache" map, or None; the 
            current value of the underlying model interface's vectorization cache (cf 
            'cortex.coreference.api.vectorization_cache.py')
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _compute_predicted_graph_edges(self, document, extended_edge2score, ground_truth_graph_edges): #, entity_head_heads=None
        """ Computes a latent "predicted" graph associated to the mentions of the input document
        (that is to say, a graph that does not take into account the info brought by the ground truth 
        coreference partition associated to the input document, but only relies on score values 
        associated to possible edges, as well as the "ground truth" graph that it will be compared 
        to), and return the edges of this graph.
        
        Here, the graphs are in fact trees.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            extended_edge2weight: an "edge => score" map, where the edges correspond to all 
                mention pairs that are implicitly defined by the application of the underlying generation 
                process on the input document
            ground_truth_graph_edges: collection of edges of the latent "ground truth" graph 
                that has been associated to the mentions of the input document

        Returns:
            a collection of edges
        """
        # FIXME: maybe we can replace this part by a call to the 'decode(, scores_are_proba=False) of a ClosestFirstCoreferenceDecoder instance?
        # No, not exactly, because here we are interested by some specific pairs, not all the pair that can be made from the elements belonging to a set / entity; 
        # and a CoreferencePartition instance can only give us that info.
        # But some code can be factorized alright.
        # FIXME: assumes that the score are not proba, since a threshold value of 0 is used below!
        # Compute predicted chain, no loss maximization available
        mentions = document.mentions
        predicted_graph_edges = []
        for subsequent_mention in mentions:
            for antecedent_mention in mentions:
                extent_pair = get_mention_pair_extent_pair(antecedent_mention,subsequent_mention)
                if extent_pair not in extended_edge2score: # Some pairs can have been filtered out
                    continue
                if antecedent_mention >= subsequent_mention:
                    break
                if extended_edge2score[extent_pair] > 0:
                    predicted_graph_edges.append(extent_pair)
        return predicted_graph_edges



########## CLOSEST-FIRST ##########
class _BaseClosestFirstDocumentStructuredResolver(_TreeLossDocumentStructuredPairResolver):
    """ Base class for resolver whose latent structure is a graph where an edge of the 'true' graph 
    structure is defined as an existing coreference link between a mention and its closest subsequent 
    mentions; and the decoding is done using the 'closest first' algorithm.
    
    Cf the _TreeLossDocumentStructuredPairResolver class.
    
    As such, can be instantiated only with a _OnlineBinaryClassifier child class instance, a 
    _BaseMentionPairGenerator child class instance and a ClosestFirstCoreferenceDecoder instance.
    
    Arguments:
        model_interface: a _OnlineBinaryClassifier child class instance, representing the interface 
            to the underlying online machine learning model to train and to use
        
        generator: a _BaseMentionPairGenerator child class instance, used to generate, from document(s), 
            machine-learning samples used as input by the underlying machine learning model
        
        decoder: a ClosestFirstCoreferenceDecoder instance, used to interpret scores associated to 
            machine-learning samples into a coreference partition, for the document used to generate 
            the scored machine-learning samples
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        sample_type: string specifying the type of the sample used in the frame of the underlying 
            machine-learning task
        
        document_hash2cache: document_hash2cache: a "document hash => cache" map, or None; the 
            current value of the underlying model interface's vectorization cache (cf 
            'cortex.coreference.api.vectorization_cache.py')
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _check_subcomponent(self, model_interface, generator, decoder):
        """ Checks that the input model interface, generator, and decoder are correct; that is: 
        - the model_interface is a _OnlineBinaryClassifier child class instance
        - the decoder is a ClosestFirstCoreferenceDecoder instance
        
        Raises:
            ValueError: if one input is not of the expected type
        """
        _check_isinstance(ClosestFirstCoreferenceDecoder, decoder, "decoder")
        super()._check_subcomponent(model_interface, generator, decoder)
    
    @classmethod
    def define_configuration(cls, generator_factory_configuration, 
                             sample_features_vectorizer_factory_configuration, 
                             root_loss, classifier_factory_configuration=None):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class.
        
        Args:
            generator_factory_configuration: a factory configuration that can be used to create 
                a specific _Generator child class instance
            sample_features_vectorizer_factory_configuration: a factory configuration that can be 
                used to create a specific _BaseExtendedMentionPairSampleFeaturesVectorizer child class instance
            root_loss: a non-negative real number; cf the class documentation
            classifier_factory_configuration: a factory configuration that can be used to 
                create a specific _OnlineBinaryClassifier child class instance; or None, in which case the 
                factory configuration for a 'PerceptronOnlineBinaryClassifier' instance (parametrized with 
                'avg=True, warm_start=False') will be used

        Returns:
            a Mapping instance
        """
        decoder_factory_configuration = ClosestFirstCoreferenceDecoder.define_factory_configuration()
        return super().define_configuration(generator_factory_configuration, decoder_factory_configuration, 
                                               sample_features_vectorizer_factory_configuration, root_loss, 
                                               classifier_factory_configuration=classifier_factory_configuration)
    
    # Instance methods
    def _compute_ground_truth_graph_edges(self, document, extended_edge2score, true_extended_edge2score, 
                                          true_coreference_edge2score): #, entity_head_extents=None
        """ Computes a latent "ground truth" graph associated to the mentions of the input document 
        (that is to say, a graph that takes into account the info brought by the ground truth 
        coreference partition associated to the input document), and return the edges of this graph.
        
        Here, it is computed such that there exist one edge for each (antecedent mention, subsequent 
        mention)that can be defined for each coreference entity that exists in the coreference 
        partition associated to the input document, but with the limitation that the antecedent 
        mention and the subsequent mention must be consecutive, within the collection of mentions 
        belonging to a given coreference entity. Also, one edge is created between the head mention 
        of each coreference entity (the first mention of the entity to appear in the document, in 
        its reading order), and the NULL_MENTION.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            extended_edge2score: an "edge => score" map, where the edges correspond to all mention 
                pairs that are implicitly defined by the application of the underlying generation process on 
                the input document. Not used here, exists only for API compatibility reasons.
            true_extended_edge2score: an "edge => score" map, where the edges correspond to 
                mention pairs that are implicitly defined by the application of the underlying generation 
                process on the input document, and which are associated to the positive class. Not used here, 
                exists only for API compatibility reasons.
            true_coreference_edge2score: an "edge => score" map, where the edges correspond to 
                mention pairs that are implicitly defined by the application of the underlying generation 
                process on the input document, and which correspond to true mentions belonging to the same 
                coreference entity. Not used here, exists only for API compatibility reasons.

        Returns:
            a collection of edges
        """
        # Compute latent true chains and link to root (provides a tree)
        entities = tuple(document.coreference_partition)
        ground_truth_graph_edges = []
        ne_extent = NULL_MENTION.extent
        for entity in entities:
            mention_extents = sorted(entity)
            extent_pair = (ne_extent, mention_extents[0])
            ground_truth_graph_edges.append(extent_pair)
            for i in range(1, len(mention_extents)):
                extent_pair = (mention_extents[i-1], mention_extents[i])
                ground_truth_graph_edges.append(extent_pair)
        return ground_truth_graph_edges



class ClosestFirstDocumentStructuredResolver(_SimpleDocumentStructuredPairResolverMixIn, 
                                             _BaseClosestFirstDocumentStructuredResolver):
    """ Class for resolver whose latent structure is a graph where:
    
    - an edge of the 'true' graph structure is defined as an existing coreference link between a 
      mention and its closest subsequent mentions
    - an edge of the 'predicted' graph structure is defined as a pair of mentions whose are the 
      closest to each while having a positive pair score
    
    and where the underlying mention pairs generation process does not support the NULL_MENTION, so 
    support for it has to be artificially implemented.
    
    The decoding is done using the 'closest first' algorithm.
    
    Cf the _BaseClosestFirstDocumentStructuredResolver class.
    
    As such, can be instantiated only with a _OnlineBinaryClassifier child class instance, a 
    MentionPairSampleGenerator instance and a ClosestFirstCoreferenceDecoder instance.
    
    Arguments:
        model_interface: a _OnlineBinaryClassifier child class instance, representing the interface 
            to the underlying online machine learning model to train and to use
        
        generator: a MentionPairSampleGenerator instance, used to generate, from document(s), 
            machine-learning samples used as input by the underlying machine learning model
        
        decoder: a ClosestFirstCoreferenceDecoder instance, used to interpret scores associated to 
            machine-learning samples into a coreference partition, for the document used to generate 
            the scored machine-learning samples
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        sample_type: string specifying the type of the sample used in the frame of the underlying 
            machine-learning task
        
        document_hash2cache: document_hash2cache: a "document hash => cache" map, or None; the 
            current value of the underlying model interface's vectorization cache (cf 
            'cortex.coreference.api.vectorization_cache.py')
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _check_subcomponent(self, model_interface, generator, decoder):
        """ Checks that the input model interface, generator, and decoder are correct; that is: 
        
        - the model_interface is a _OnlineBinaryClassifier child class instance
        - the generator is a MentionPairSampleGenerator instance
        - the decoder is a ClosestFirstCoreferenceDecoder instance
        
        Raises:
            ValueError: if one input is not of the expected type
        """
        _check_isinstance(MentionPairSampleGenerator, generator, "generator")
        super()._check_subcomponent(model_interface, generator, decoder)
    
    @classmethod
    def define_configuration(cls, sample_features_vectorizer_factory_configuration=None, 
                             root_loss=1.0, classifier_factory_configuration=None, 
                             generator_pair_filter_factory_configuration=None):
        """ Create a configuration object, which can then be used to create a specific instance of 
        this class.
        
        Args:
            sample_features_vectorizer_factory_configuration: a factory configuration that can be 
                used to create a specific _BaseMentionPairSampleFeaturesVectorizer child class instance; 
                or None, in which case the factory configuration for a 'MentionPairSampleFeaturesVectorizer' 
                instance (parametrized with 'quantize=True') will be used
            root_loss: a non-negative real number; cf the class documentation
            classifier_factory_configuration: a factory configuration that can be used to 
                create a specific _OnlineBinaryClassifier child class instance; or None, in which case the 
                factory configuration for a 'PerceptronOnlineBinaryClassifier' instance (parametrized with 
                'avg=True, warm_start=False') will be used
            generator_pair_filter_factory_configuration: a factory configuration that can be used 
                to create a specific PairFilter instance, that shall be used to generate the factory 
                configuration of an instance of the _Generator child class compatible with this resolver class; or 
                None, in which case the factory configuration for an 'AcceptAllPairFilter' instance will be used

        Returns:
            a Mapping instance
        """
        # Check and process inputs
        if sample_features_vectorizer_factory_configuration is None:
            sample_features_vectorizer_factory_configuration =\
             MentionPairSampleFeaturesVectorizer.define_factory_configuration(quantize=True)
        if generator_pair_filter_factory_configuration is None:
            generator_pair_filter_factory_configuration = AcceptAllPairFilter.define_factory_configuration()
        generator_factory_configuration =\
         MentionPairSampleGenerator.define_factory_configuration(pair_filter_factory_configuration=generator_pair_filter_factory_configuration)
        return super().define_configuration(generator_factory_configuration, 
                                            sample_features_vectorizer_factory_configuration, root_loss, 
                                            classifier_factory_configuration=classifier_factory_configuration)
    
    # Instance methods
    def _compute_training_phase_extended_mention_pair_scores(self, document):
        """ Computes scores to be associated to extended mention pairs (so, also for pairs whose 
        antecedent mention is the NULL_MENTION); using classification prediction on the mention pair 
        samples collection generated from the input document; and interpreting the classification 
        confidence scores as weights on the edges of a graph of mentions.
        
        Score values may be artificially created for for pairs whose antecedent mention is the 
        NULL_MENTION, if the underlying generation process is unable to generate such mention pairs.
        The scores are float values.
        The output of this method is destined to be used as input to build a virtual ground truth 
        graph during the "prediction" of an atomic training step carried out using the input document 
        as support.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance

        Returns:
            a (extended_edge2score, true_extended_edge2score, true_coreference_edge2score) 
            triplet, where:
                - 'extended_edge2score' is the "edge => score" map resulting for carrying out classification 
                  prediction on the samples generated from the input document, using the underlying generation 
                  process
                - 'true_extended_edge2score' is a submap of the 'extended_edge2score' map, only 
                  for edges that correspond to mention pairs that are made of mentions that belong to the same 
                  coreference entity, (their corresponding sample have been labelled with the positive class 
                  label), or correspond to non-anaphoric mentions (the subsequent 'mention' of the pair is a 
                  true mention, whereas the antecedent 'mention' of the pair is the NULL_MENTION)
                - 'true_coreference_edge2score' is is a submap of the 'extended_edge2score' map, only 
                  for edges that correspond to mention pairs that are made of mentions that belong to the same 
                  coreference entity (their corresponding sample have been labelled with the positive class 
                  label), which means that none of the mentions of those pairs is the NULL_MENTION
        """
        return self._compute_training_phase_artificially_extended_mention_pair_scores(document)
    
    def _generate_edge_samples(self, document, edges):
        """ Creates collection of MentionPairSample instances corresponding to pairs of mentions made 
        from mentions coming from the input document, pairs that are specified by input edges collection.
        
        Only edges whose corresponding mentions pair that can be produced by the underlying generator, 
        from the input document, shall see the creation of a corresponding MentionPairSample instance.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            edges: collection of edges

        Returns:
            a collection of MentionPairSample instances, where all mentions of the pairs are 
            true mentions (no one is the NULL_MENTION)
        """
        return self._generate_mention_pair_edge_samples(document, edges)
    
    def _compute_prediction_phase_extended_mention_pair_scores(self, document):
        """ Computes scores to be associated to extended mention pairs (so, also for pairs whose 
        antecedent mention is the NULL_MENTION); using classification prediction on the mention pair 
        samples collection generated from the input document; and interpreting the classification 
        confidence scores as weights on the edges of a graph of mentions.
        
        Score values may be artificially created for for pairs whose antecedent mention is the 
        NULL_MENTION, if the underlying generation process is unable to generate such mention pairs.
        The scores are float values.
        The output of this method is destined to be used as input to the decoding process, in the 
        frame of the prediction of a coreference partition for the input document.
        
        Args:
            document: a Document instance

        Returns:
            an "edge => score" map
        """
        return self._compute_artificially_extended_mention_pair_prediction_pair_scores(document)
    
    def _compute_predicted_graph_edges(self, document, extended_edge2score, ground_truth_graph_edges): #, entity_head_extents=None
        """ Computes a latent "predicted" graph associated to the mentions of the input document
        (that is to say, a graph that does not take into account the info brought by the ground truth 
        coreference partition associated to the input document, but only relies on score values 
        associated to possible edges, as well as the "ground truth" graph that it will be compared 
        to), and return the edges of this graph.
        
        Here, the graphs are in fact trees.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            extended_edge2weight: an "edge => score" map, where the edges correspond to all 
                mention pairs that are implicitly defined by the application of the underlying generation 
                process on the input document
            ground_truth_graph_edges: collection of edges of the latent "ground truth" graph 
                that has been associated to the mentions of the input document

        Returns:
            a collection of edges
        """
        # Compute predicted chain, no loss maximization available
        mentions = document.mentions
        predicted_graph_edges = []
        for j in range(len(mentions)):
            subsequent_mention = mentions[j]
            linked = False
            for i in range(j-1,-1,-1):
                antecedent_mention = mentions[i]
                extent_pair = get_mention_pair_extent_pair(antecedent_mention, subsequent_mention)
                if extent_pair not in extended_edge2score: # Some pairs can have been filtered out
                    continue
                if extended_edge2score[extent_pair] > 0: # Found the closest positive
                    predicted_graph_edges.append(extent_pair)
                    linked = True
                    break
            if not linked:
                extent_pair = get_mention_pair_extent_pair(NULL_MENTION, subsequent_mention)
                predicted_graph_edges.append(extent_pair) # Link the leftmost element of a chain to the root
        return predicted_graph_edges


class ExtendedClosestFirstDocumentStructuredResolver(_SimpleDocumentStructuredPairResolverMixIn, 
                                                     _BaseClosestFirstDocumentStructuredResolver):
    """ Class for resolver whose latent structure is a graph where:
    
    - an edge of the 'true' graph structure is defined as an existing coreference link between a 
      mention and its closest subsequent mentions
    - an edge of the 'predicted' graph structure is defined as a pair of mentions whose are the closest 
      to each while having a positive pair score
    
    and where the underlying mention pairs generation process does support the NULL_MENTION.
    
    The decoding is done using the 'closest first' algorithm.
    
    The sample features vectorizer class used to parametrize this resolver must inherit from 
    '_BaseExtendedMentionPairSampleFeaturesVectorizer', it must have 'extended' in its name.
    
    Cf the _BaseClosestFirstDocumentStructuredResolver class.
    
    As such, can be instantiated only with a _OnlineBinaryClassifier child class instance, a 
    ExtendedMentionPairSampleGenerator instance and a ClosestFirstCoreferenceDecoder instance.
    
    Arguments:
        model_interface: a _OnlineBinaryClassifier child class instance, representing the interface 
            to the underlying online machine learning model to train and to use
        
        generator: a ExtendedMentionPairSampleGenerator instance, used to generate, from document(s), 
            machine-learning samples used as input by the underlying machine learning model
        
        decoder: a ClosestFirstCoreferenceDecoder instance, used to interpret scores associated to 
            machine-learning samples into a coreference partition, for the document used to generate 
            the scored machine-learning samples
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        sample_type: string specifying the type of the sample used in the frame of the underlying 
            machine-learning task
        
        document_hash2cache: document_hash2cache: a "document hash => cache" map, or None; the 
            current value of the underlying model interface's vectorization cache (cf 
            'cortex.coreference.api.vectorization_cache.py')
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _check_subcomponent(self, model_interface, generator, decoder):
        """ Checks that the input model interface, generator, and decoder are correct; that is: 
        
        - the model_interface is a _OnlineBinaryClassifier child class instance
        - the generator is a ExtendedMentionPairSampleGenerator instance
        - the decoder is a ClosestFirstCoreferenceDecoder instance
        
        Raises:
            ValueError: if one input is not of the expected type
        """
        _check_isinstance(ExtendedMentionPairSampleGenerator, generator, "generator")
        super()._check_subcomponent(model_interface, generator, decoder)
    
    @classmethod
    def define_configuration(cls, sample_features_vectorizer_factory_configuration=None, 
                             root_loss=1.0, classifier_factory_configuration=None, 
                             generator_pair_filter_factory_configuration=None):
        """ Create     a configuration object, which can then be used to create a specific instance of 
        this class.
        
        Args:
            sample_features_vectorizer_factory_configuration: a factory configuration that can be 
                used to create a specific _BaseExtendedMentionPairSampleFeaturesVectorizer child class instance; 
                or None, in which case the factory configuration for a 'ExtendedMentionPairSampleFeaturesVectorizer' 
                instance (parametrized with 'quantize=True') will be used
            root_loss: a non-negative real number; cf the class documentation
            update_mode: string, either 'PB' or 'ML'; cf the class documentation
            classifier_factory_configuration: a factory configuration that can be used to 
                create a specific _OnlineBinaryClassifier child class instance; or None, in which case the 
                factory configuration for a 'PerceptronOnlineBinaryClassifier' instance (parametrized with 
                'avg=True, warm_start=False') will be used
            generator_pair_filter_factory_configuration: a factory configuration that can be used 
                to create a specific PairFilter instance, that shall be used to generate the factory 
                configuration of an instance of the _Generator child class compatible with this resolver class; 
                or None, in which case the factory configuration for an 'AcceptAllPairFilter' instance will 
                be used

        Returns:
            a Mapping instance
        """
        # Check and process inputs
        if sample_features_vectorizer_factory_configuration is None:
            sample_features_vectorizer_factory_configuration =\
             ExtendedMentionPairSampleFeaturesVectorizer.define_factory_configuration(quantize=True)
        if generator_pair_filter_factory_configuration is None:
            generator_pair_filter_factory_configuration = AcceptAllPairFilter.define_factory_configuration()
        if sample_features_vectorizer_factory_configuration["name"] not in POSSIBLE_EXTENDED_MENTION_PAIR_SAMPLES_VECTORIZER_NAMES:
            msg = "Incorrect input classifier_configurations name '{}': must belong to '{}'."
            msg = msg.format(classifier_factory_configuration["name"], 
                             POSSIBLE_EXTENDED_MENTION_PAIR_SAMPLES_VECTORIZER_NAMES)
            raise ValueError(msg)
        
        generator_factory_configuration =\
         ExtendedMentionPairSampleGenerator.define_factory_configuration(pair_filter_factory_configuration=generator_pair_filter_factory_configuration)
        return super().define_configuration(generator_factory_configuration, 
                                               sample_features_vectorizer_factory_configuration, root_loss, 
                                               classifier_factory_configuration=classifier_factory_configuration)
    
    # Instance methods
    def _generate_edge_samples(self, document, edges):
        """ Creates collection of MentionPairSample instances corresponding to pairs of mentions made 
        from mentions coming from the input document, pairs that are specified by input edges collection.
        
        Only edges whose corresponding mentions pair that can be produced by the underlying generator, 
        from the input document, shall see the creation of a corresponding MentionPairSample instance.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            edges: collection of edges

        Returns:
            a collection of MentionPairSample instances, where all mentions of the pairs are 
            true mentions (no one is the NULL_MENTION)
        """
        return self._generate_extended_mention_pair_edge_samples(document, edges)
    
    def _compute_training_phase_extended_mention_pair_scores(self, document):
        """ Computes scores to be associated to extended mention pairs (so, also for pairs whose 
        antecedent mention is the NULL_MENTION); using classification prediction on the mention pair 
        samples collection generated from the input document; and interpreting the classification 
        confidence scores as weights on the edges of a graph of mentions.
        
        Score values may be artificially created for for pairs whose antecedent mention is the 
        NULL_MENTION, if the underlying generation process is unable to generate such mention pairs.
        The scores are float values.
        The output of this method is destined to be used as input to build a virtual ground truth 
        graph during the "prediction" of an atomic training step carried out using the input document 
        as support.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance

        Returns:
            a (extended_edge2score, true_extended_edge2score, true_coreference_edge2score) 
            triplet, where:
                - 'extended_edge2score' is the "edge => score" map resulting for carrying out classification 
                  prediction on the samples generated from the input document, using the underlying generation 
                  process
                - 'true_extended_edge2score' is a submap of the 'extended_edge2score' map, only 
                  for edges that correspond to mention pairs that are made of mentions that belong to the same 
                  coreference entity, (their corresponding sample have been labelled with the positive class 
                  label), or correspond to non-anaphoric mentions (the subsequent 'mention' of the pair is a 
                  true mention, whereas the antecedent 'mention' of the pair is the NULL_MENTION)
                - 'true_coreference_edge2score' is is a submap of the 'extended_edge2score' map, only 
                  for edges that correspond to mention pairs that are made of mentions that belong to the same 
                  coreference entity (their corresponding sample have been labelled with the positive class 
                  label), which means that none of the mentions of those pairs is the NULL_MENTION
        """
        return self._compute_training_phase_naturally_extended_mention_pair_scores(document)
    
    def _compute_prediction_phase_extended_mention_pair_scores(self, document):
        """ Computes scores to be associated to extended mention pairs (so, also for pairs whose 
        antecedent mention is the NULL_MENTION); using classification prediction on the mention pair 
        samples collection generated from the input document; and interpreting the classification 
        confidence scores as weights on the edges of a graph of mentions.
        
        Score values may be artificially created for for pairs whose antecedent mention is the 
        NULL_MENTION, if the underlying generation process is unable to generate such mention pairs.
        The scores are float values.
        The output of this method is destined to be used as input to the decoding process, in the 
        frame of the prediction of a coreference partition for the input document.
        
        Args:
            document: a Document instance

        Returns:
            an "edge => score" map
        """
        return self._compute_naturally_extended_mention_pair_prediction_pair_scores(document)
    
    def _compute_predicted_graph_edges(self, document, extended_edge2score, ground_truth_graph_edges): #, entity_head_extents=None
        """ Computes a latent "predicted" graph associated to the mentions of the input document
        (that is to say, a graph that does not take into account the info brought by the ground truth 
        coreference partition associated to the input document, but only relies on score values 
        associated to possible edges, as well as the "ground truth" graph that it will be compared 
        to), and return the edges of this graph.
        
        Here, the graphs are in fact trees.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            extended_edge2weight: an "edge => score" map, where the edges correspond to all 
                mention pairs that are implicitly defined by the application of the underlying generation 
                process on the input document
            ground_truth_graph_edges: collection of edges of the latent "ground truth" graph 
                that has been associated to the mentions of the input document

        Returns:
            a collection of edges
        """
        # Compute predicted chain, no loss maximization available
        mentions = document.mentions
        predicted_graph_edges = []
        for j in range(len(mentions)):
            subsequent_mention = mentions[j]
            linked = False
            default_edge_extent_pair = get_mention_pair_extent_pair(NULL_MENTION, subsequent_mention)
            default_edge_score = extended_edge2score[default_edge_extent_pair]
            for i in range(j-1,-1,-1):
                antecedent_mention = mentions[i]
                extent_pair = get_mention_pair_extent_pair(antecedent_mention, subsequent_mention)
                if extent_pair not in extended_edge2score: # Some pairs can have been filtered out
                    continue
                score = extended_edge2score[extent_pair] 
                if score > 0: # Found the closest positive
                    if score > default_edge_score: # Else NULLMENTION will be selected as closest
                        predicted_graph_edges.append(extent_pair)
                        linked = True
                    break
            if not linked:
                predicted_graph_edges.append(default_edge_extent_pair)
        return predicted_graph_edges






########## BEST-FIRST ##########
class _BaseBestFirstDocumentStructuredResolver(_UpdateModeTreeLossDocumentStructuredPairsResolver):
    """ Base class for resolver whose latent structure is a graph where an edge of the 'true' graph 
    structure is defined as corresponding to a pair of mentions chosen by the 'best first' algorithm, 
    and which corresponds to an existing coreference link; and where the decoding is done using the 
    'best first' algorithm.
    
    Cf the _UpdateModeTreeLossDocumentStructuredPairsResolver class.
    
    As such, can be instantiated only with a _OnlineBinaryClassifier child class instance, a 
    _BaseMentionPairGenerator child class instance and a __BaseMentionPairCoreferenceDecoder child 
    class instance.
    
    Arguments:
        model_interface: a _OnlineBinaryClassifier child class instance, representing the interface 
            to the underlying online machine learning model to train and to use
        
        generator: a _BaseMentionPairGenerator child class instance, used to generate, from document(s), 
            machine-learning samples used as input by the underlying machine learning model
        
        decoder: a __BaseMentionPairCoreferenceDecoder child class instance, used to interpret scores 
            associated to machine-learning samples into a coreference partition, for the document used 
            to generate the scored machine-learning samples
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        update_mode: string, either 'PB' or 'ML'; cf the class documentation
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        sample_type: string specifying the type of the sample used in the frame of the underlying 
            machine-learning task
        
        document_hash2cache: document_hash2cache: a "document hash => cache" map, or None; the 
            current value of the underlying model interface's vectorization cache (cf 
            'cortex.coreference.api.vectorization_cache.py')
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        update_mode: string, either 'PB' or 'ML'; cf the class documentation
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    # Instance methods
    def _compute_ground_truth_graph_edges(self, document, extended_edge2score, true_extended_edge2score, 
                                          true_coreference_edge2score): #, entity_head_extents=None
        """ Computes a latent "ground truth" graph associated to the mentions of the input document 
        (that is to say, a graph that takes into account the info brought by the ground truth 
        coreference partition associated to the input document), and return the edges of this graph.
        
        As for the algorithm used, cf the class' documentation.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            extended_edge2score: an "edge => score" map, where the edges correspond to all mention 
                pairs that are implicitly defined by the application of the underlying generation process on 
                the input document. Not used here, exists only for API compatibility reasons.
            true_extended_edge2score: an "edge => score" map, where the edges correspond to 
                mention pairs that are implicitly defined by the application of the underlying generation 
                process on the input document, and which are associated to the positive class
            true_coreference_edge2score: an "edge => score" map, where the edges correspond to 
                mention pairs that are implicitly defined by the application of the underlying generation 
                process on the input document, and which correspond to true mentions belonging to the same 
                coreference entity. Not used here, exists only for API compatibility reasons.

        Returns:
            a collection of edges
        """
        # Compute latent true chains and link to root (provides a tree)
        entities = tuple(document.coreference_partition)
        ground_truth_graph_edges = []
        for entity in entities:
            sorted_mention_extents = sorted(entity)
            sorted_mentions = tuple(document.extent2mention[extent] for extent in sorted_mention_extents)
            entity_size = len(entity)
            first_mention = sorted_mentions[0]
            ground_truth_graph_edges.append(get_mention_pair_extent_pair(NULL_MENTION, first_mention))
            for j in range(1,entity_size): # We start from 1 since the first mention has already been forcibly associated with the NULL_MENTION (see above).
                subsequent_mention = sorted_mentions[j]
                max_score = -numpy.inf
                max_score_antecedent_mention = None
                for i in range(j-1, -1, -1):
                    antecedent_mention = sorted_mentions[i]
                    # Skip pronominal antecent mention for non-pronominal anaphora
                    if MENTION_FEATURES.is_expanded_pronoun(antecedent_mention) and not MENTION_FEATURES.is_expanded_pronoun(subsequent_mention):
                        continue
                    score = true_extended_edge2score.get(get_mention_pair_extent_pair(antecedent_mention,subsequent_mention))
                    if score is None:
                        continue
                    if score > max_score:
                        max_score = score
                        max_score_antecedent_mention = antecedent_mention
                if max_score_antecedent_mention is None:
                    max_score_antecedent_mention = NULL_MENTION
                ground_truth_graph_edges.append(get_mention_pair_extent_pair(max_score_antecedent_mention,subsequent_mention))
        return ground_truth_graph_edges



class BestFirstDocumentStructuredResolver(_SimpleDocumentStructuredPairResolverMixIn, 
                                          _BaseBestFirstDocumentStructuredResolver):
    """ Class for resolver whose latent structure is a graph where:
    
    - an edge of the 'true' graph  structure is defined as corresponding to a pair of mentions 
      chosen by the 'best first' algorithm, and which corresponds to an existing coreference link
    - an edge of the 'predicted' graph structure is defined as a pair of mentions chosen by the 
      'best first' algorithm
    
    and where the underlying mention pairs generation process does not support the NULL_MENTION, so 
    support for it has to be artificially implemented.
    
    The decoding is done using the 'best first' algorithm.
    
    Cf the _BaseBestFirstDocumentStructuredResolver class.
    
    As such, can be instantiated only with a _OnlineBinaryClassifier child class instance, a 
    MentionPairSampleGenerator instance and a BestFirstCoreferenceDecoder instance.
    
    Arguments:
        model_interface: a _OnlineBinaryClassifier child class instance, representing the interface 
            to the underlying online machine learning model to train and to use
        
        generator: a MentionPairSampleGenerator instance, used to generate, from document(s), 
            machine-learning samples used as input by the underlying machine learning model
        
        decoder: a BestFirstCoreferenceDecoder instance, used to interpret scores associated to 
            machine-learning samples into a coreference partition, for the document used to generate 
            the scored machine-learning samples
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        update_mode: string, either 'PB' or 'ML'; cf the class documentation
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        sample_type: string specifying the type of the sample used in the frame of the underlying 
            machine-learning task
        
        document_hash2cache: document_hash2cache: a "document hash => cache" map, or None; the 
            current value of the underlying model interface's vectorization cache (cf 
            'cortex.coreference.api.vectorization_cache.py')
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        update_mode: string, either 'PB' or 'ML'; cf the class documentation
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _check_subcomponent(self, model_interface, generator, decoder):
        """ Checks that the input model interface, generator, and decoder are correct; that is: 
        
        - the model_interface is a _OnlineBinaryClassifier child class instance
        - the generator is a MentionPairSampleGenerator instance
        - the decoder is a BestFirstCoreferenceDecoder instance
        
        Raises:
            ValueError: if one input is not of the expected type
        """
        _check_isinstance(MentionPairSampleGenerator, generator, "generator")
        _check_isinstance(BestFirstCoreferenceDecoder, decoder, "decoder")
        super()._check_subcomponent(model_interface, generator, decoder)
    
    @classmethod
    def define_configuration(cls, sample_features_vectorizer_factory_configuration=None, root_loss=1.0, 
                             update_mode="PB", classifier_factory_configuration=None, 
                             generator_pair_filter_factory_configuration=None):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class.
        
        Args:
            sample_features_vectorizer_factory_configuration: a factory configuration that can be 
                used to create a specific _BaseMentionPairSampleFeaturesVectorizer child class instance; 
                or None, in which case the factory configuration for a 'MentionPairSampleFeaturesVectorizer' 
                instance (parametrized with 'quantize=True') will be used
            root_loss: a non-negative real number; cf the class documentation
            update_mode: string, either 'PB' or 'ML'; cf the class documentation
            classifier_factory_configuration: a factory configuration that can be used to 
                create a specific _OnlineBinaryClassifier child class instance; or None, in which case the 
                factory configuration for a 'PerceptronOnlineBinaryClassifier' instance (parametrized with 
                'avg=True, warm_start=False') will be used
            generator_pair_filter_factory_configuration: a factory configuration that can be used 
                to create a specific PairFilter instance, that shall be used to generate the factory 
                configuration of an instance of the _Generator child class compatible with this resolver class; 
                or None, in which case the factory configuration for an 'AcceptAllPairFilter' instance will 
                be used

        Returns:
            a Mapping instance
        """
        # Check and process inputs
        if sample_features_vectorizer_factory_configuration is None:
            sample_features_vectorizer_factory_configuration =\
             MentionPairSampleFeaturesVectorizer.define_factory_configuration(quantize=True)
        if generator_pair_filter_factory_configuration is None:
            generator_pair_filter_factory_configuration = AcceptAllPairFilter.define_factory_configuration()
        generator_factory_configuration =\
         MentionPairSampleGenerator.define_factory_configuration(pair_filter_factory_configuration=generator_pair_filter_factory_configuration)
        decoder_factory_configuration = BestFirstCoreferenceDecoder.define_factory_configuration()
        return super().define_configuration(generator_factory_configuration, decoder_factory_configuration, 
                                               sample_features_vectorizer_factory_configuration, root_loss, 
                                               update_mode, classifier_factory_configuration=classifier_factory_configuration)
    
    # Instance methods
    def _generate_edge_samples(self, document, edges):
        """ Creates collection of MentionPairSample instances corresponding to pairs of mentions made 
        from mentions coming from the input document, pairs that are specified by input edges collection.
        
        Only edges whose corresponding mentions pair that can be produced by the underlying generator, 
        from the input document, shall see the creation of a corresponding MentionPairSample instance.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            edges: collection of edges

        Returns:
            a collection of MentionPairSample instances, where all mentions of the pairs are 
            true mentions (no one is the NULL_MENTION)
        """
        return self._generate_mention_pair_edge_samples(document, edges)
    
    def _compute_training_phase_extended_mention_pair_scores(self, document):
        """ Computes scores to be associated to extended mention pairs (so, also for pairs whose 
        antecedent mention is the NULL_MENTION); using classification prediction on the mention pair 
        samples collection generated from the input document; and interpreting the classification 
        confidence scores as weights on the edges of a graph of mentions.
        
        Score values may be artificially created for for pairs whose antecedent mention is the 
        NULL_MENTION, if the underlying generation process is unable to generate such mention pairs.
        The scores are float values.
        The output of this method is destined to be used as input to build a virtual ground truth 
        graph during the "prediction" of an atomic training step carried out using the input document 
        as support.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance

        Returns:
            a (extended_edge2score, true_extended_edge2score, true_coreference_edge2score) 
            triplet, where:
                - 'extended_edge2score' is the "edge => score" map resulting for carrying out classification 
                  prediction on the samples generated from the input document, using the underlying generation 
                  process
                - 'true_extended_edge2score' is a submap of the 'extended_edge2score' map, only 
                  for edges that correspond to mention pairs that are made of mentions that belong to the same 
                  coreference entity, (their corresponding sample have been labelled with the positive class 
                  label), or correspond to non-anaphoric mentions (the subsequent 'mention' of the pair is a 
                  true mention, whereas the antecedent 'mention' of the pair is the NULL_MENTION)
                - 'true_coreference_edge2score' is is a submap of the 'extended_edge2score' map, only 
                  for edges that correspond to mention pairs that are made of mentions that belong to the same 
                  coreference entity (their corresponding sample have been labelled with the positive class 
                  label), which means that none of the mentions of those pairs is the NULL_MENTION
        """
        return self._compute_training_phase_artificially_extended_mention_pair_scores(document)

    def _compute_prediction_phase_extended_mention_pair_scores(self, document):
        """ Computes scores to be associated to extended mention pairs (so, also for pairs whose 
        antecedent mention is the NULL_MENTION); using classification prediction on the mention pair 
        samples collection generated from the input document; and interpreting the classification 
        confidence scores as weights on the edges of a graph of mentions.
        
        Score values may be artificially created for for pairs whose antecedent mention is the 
        NULL_MENTION, if the underlying generation process is unable to generate such mention pairs.
        The scores are float values.
        The output of this method is destined to be used as input to the decoding process, in the 
        frame of the prediction of a coreference partition for the input document.
        
        Args:
            document: a Document instance

        Returns:
            an "edge => score" map
        """
        return self._compute_artificially_extended_mention_pair_prediction_pair_scores(document)
    
    def _compute_predicted_tree_edges_from_edge2loss_score(self, document, edge2weight): #, entity_head_extents=None
        """ Computes a latent "predicted" graph associated to the mentions of the input document
        (that is to say, a graph that does not take into account the info brought by the ground truth 
        coreference partition associated to the input document, but only relies on weight values 
        associated to possible edges), and return the edges of this graph.
        
        Here, the graph is in fact a tree.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            edge2weight: an "edge => weigt" map, where the edges correspond to all mention pairs 
                that are implicitly defined by the application of the underlying generation process on the 
                input document

        Returns:
            a collection of edges corresponding to the computed "predicted" graph
        """
        mentions = document.mentions
        predicted_graph_edges = []
        for j in range(len(mentions)):
            subsequent_mention = mentions[j]
            max_score = 0. # FIXME: Because the loss score values are all supposed to be >= 0?
            # Is it supposed to be the decoding threshold values associated to scores that are not proba values?
            max_score_antecedent_mention = None
            for i in range(j-1, -1, -1):
                antecedent_mention = mentions[i]
                # Skip pronominal antecent mention for non-pronominal anaphora
                if MENTION_FEATURES.is_expanded_pronoun(antecedent_mention) and not MENTION_FEATURES.is_expanded_pronoun(subsequent_mention):
                    continue
                score = edge2weight.get(get_mention_pair_extent_pair(antecedent_mention,subsequent_mention)) # Some pairs can have been filtered out
                if score is None:
                    continue
                if score > max_score:
                    max_score = score
                    max_score_antecedent_mention = antecedent_mention
            if max_score_antecedent_mention is None:
                max_score_antecedent_mention = NULL_MENTION
            predicted_graph_edges.append(get_mention_pair_extent_pair(max_score_antecedent_mention, subsequent_mention))
        return predicted_graph_edges



class ExtendedBestFirstDocumentStructuredResolver(_SimpleDocumentStructuredPairResolverMixIn, 
                                                  _BaseBestFirstDocumentStructuredResolver):
    """ Class for resolver whose latent structure is a graph where:
    
        - an edge of the 'true' graph  structure is defined as corresponding to a pair of mentions 
          chosen by the 'best first' algorithm, and which corresponds to an existing coreference link
        - an edge of the 'predicted' graph structure is defined as a pair of mentions chosen by the 
          'best first' algorithm
    
    and where the underlying mention pairs generation process does support the NULL_MENTION.
    
    The decoding is done using the 'best first' algorithm.
    
    The sample features vectorizer class used to parametrize this resolver must inherit from 
    '_BaseExtendedMentionPairSampleFeaturesVectorizer', it must have 'extended' in its name.
    
    Cf the _BaseBestFirstDocumentStructuredResolver class.
    
    As such, can be instantiated only with a _OnlineBinaryClassifier child class instance, a 
    ExtendedMentionPairSampleGenerator instance and a ExtendedBestFirstCoreferenceDecoder instance.
    
    Arguments:
        model_interface: a _OnlineBinaryClassifier child class instance, representing the interface 
            to the underlying online machine learning model to train and to use
        
        generator: a ExtendedMentionPairSampleGenerator instance, used to generate, from document(s), 
            machine-learning samples used as input by the underlying machine learning model
        
        decoder: a ExtendedBestFirstCoreferenceDecoder instance, used to interpret scores associated to 
            machine-learning samples into a coreference partition, for the document used to generate 
            the scored machine-learning samples
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        update_mode: string, either 'PB' or 'ML'; cf the class documentation
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        sample_type: string specifying the type of the sample used in the frame of the underlying 
            machine-learning task
        
        document_hash2cache: document_hash2cache: a "document hash => cache" map, or None; the 
            current value of the underlying model interface's vectorization cache (cf 
            'cortex.coreference.api.vectorization_cache.py')
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        update_mode: string, either 'PB' or 'ML'; cf the class documentation
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _check_subcomponent(self, model_interface, generator, decoder):
        """ Checks that the input model interface, generator, and decoder are correct; that is: 
        
        - the model_interface is a _OnlineBinaryClassifier child class instance
        - the generator is a ExtendedMentionPairSampleGenerator instance
        - the decoder is a ExtendedBestFirstCoreferenceDecoder instance
        
        Raises:
            ValueError: if one input is not of the expected type
        """
        _check_isinstance(ExtendedMentionPairSampleGenerator, generator, "generator")
        _check_isinstance(ExtendedBestFirstCoreferenceDecoder, decoder, "decoder")
        super()._check_subcomponent(model_interface, generator, decoder)
    
    @classmethod
    def define_configuration(cls, sample_features_vectorizer_factory_configuration=None, root_loss=1.0, 
                             update_mode="PB", classifier_factory_configuration=None, 
                             generator_pair_filter_factory_configuration=None):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class.
        
        Args:
            sample_features_vectorizer_factory_configuration: a factory configuration that can be 
                used to create a specific _BaseExtendedMentionPairSampleFeaturesVectorizer child class instance; 
                or None, in which case the factory configuration for a 'ExtendedMentionPairSampleFeaturesVectorizer' 
                instance (parametrized with 'quantize=True') will be used
            root_loss: a non-negative real number; cf the class documentation
            update_mode: string, either 'PB' or 'ML'; cf the class documentation
            classifier_factory_configuration: a factory configuration that can be used to 
                create a specific _OnlineBinaryClassifier child class instance; or None, in which case the 
                factory configuration for a 'PerceptronOnlineBinaryClassifier' instance (parametrized with 
                'avg=True, warm_start=False') will be used
            generator_pair_filter_factory_configuration: a factory configuration that can be used 
                to create a specific PairFilter instance, that shall be used to generate the factory 
                configuration of an instance of the _Generator child class compatible with this resolver class; 
                or None, in which case the factory configuration for an 'AcceptAllPairFilter' instance will 
                be used

        Returns:
            a Mapping instance
        """
        # Check and process inputs
        if sample_features_vectorizer_factory_configuration is None:
            sample_features_vectorizer_factory_configuration =\
             ExtendedMentionPairSampleFeaturesVectorizer.define_factory_configuration(quantize=True)
        if generator_pair_filter_factory_configuration is None:
            generator_pair_filter_factory_configuration = AcceptAllPairFilter.define_factory_configuration()
        if sample_features_vectorizer_factory_configuration["name"] not in POSSIBLE_EXTENDED_MENTION_PAIR_SAMPLES_VECTORIZER_NAMES:
            msg = "Incorrect input classifier_configurations name '{}': must belong to '{}'."
            msg = msg.format(classifier_factory_configuration["name"], 
                             POSSIBLE_EXTENDED_MENTION_PAIR_SAMPLES_VECTORIZER_NAMES)
            raise ValueError(msg)
        
        generator_factory_configuration =\
         ExtendedMentionPairSampleGenerator.define_factory_configuration(pair_filter_factory_configuration=generator_pair_filter_factory_configuration)
        decoder_factory_configuration = ExtendedBestFirstCoreferenceDecoder.define_factory_configuration()
        return super().define_configuration(generator_factory_configuration, decoder_factory_configuration, 
                                                sample_features_vectorizer_factory_configuration, 
                                                root_loss, update_mode, 
                                                classifier_factory_configuration=classifier_factory_configuration)
    
    # Instance methods
    def _generate_edge_samples(self, document, edges):
        """ Creates collection of MentionPairSample instances corresponding to pairs of mentions made 
        from mentions coming from the input document, pairs that are specified by input edges collection.
        
        Only edges whose corresponding mentions pair that can be produced by the underlying generator, 
        from the input document, shall see the creation of a corresponding MentionPairSample instance.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            edges: collection of edges

        Returns:
            a collection of MentionPairSample instances, where all mentions of the pairs are 
            true mentions (no one is the NULL_MENTION)
        """
        return self._generate_extended_mention_pair_edge_samples(document, edges)
    
    def _compute_training_phase_extended_mention_pair_scores(self, document):
        """ Computes scores to be associated to extended mention pairs (so, also for pairs whose 
        antecedent mention is the NULL_MENTION); using classification prediction on the mention pair 
        samples collection generated from the input document; and interpreting the classification 
        confidence scores as weights on the edges of a graph of mentions.
        
        Score values may be artificially created for for pairs whose antecedent mention is the 
        NULL_MENTION, if the underlying generation process is unable to generate such mention pairs.
        The scores are float values.
        The output of this method is destined to be used as input to build a virtual ground truth 
        graph during the "prediction" of an atomic training step carried out using the input document 
        as support.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance

        Returns:
            a (extended_edge2score, true_extended_edge2score, true_coreference_edge2score) 
            triplet, where:
                - 'extended_edge2score' is the "edge => score" map resulting for carrying out classification 
                  prediction on the samples generated from the input document, using the underlying generation 
                  process
                - 'true_extended_edge2score' is a submap of the 'extended_edge2score' map, only 
                  for edges that correspond to mention pairs that are made of mentions that belong to the same 
                  coreference entity, (their corresponding sample have been labelled with the positive class 
                  label), or correspond to non-anaphoric mentions (the subsequent 'mention' of the pair is a 
                  true mention, whereas the antecedent 'mention' of the pair is the NULL_MENTION)
                - 'true_coreference_edge2score' is is a submap of the 'extended_edge2score' map, only 
                  for edges that correspond to mention pairs that are made of mentions that belong to the same 
                  coreference entity (their corresponding sample have been labelled with the positive class 
                  label), which means that none of the mentions of those pairs is the NULL_MENTION
        """
        return self._compute_training_phase_naturally_extended_mention_pair_scores(document)
    
    def _compute_prediction_phase_extended_mention_pair_scores(self, document):
        """ Computes scores to be associated to extended mention pairs (so, also for pairs whose 
        antecedent mention is the NULL_MENTION); using classification prediction on the mention pair 
        samples collection generated from the input document; and interpreting the classification 
        confidence scores as weights on the edges of a graph of mentions.
        
        Score values may be artificially created for for pairs whose antecedent mention is the 
        NULL_MENTION, if the underlying generation process is unable to generate such mention pairs.
        The scores are float values.
        The output of this method is destined to be used as input to the decoding process, in the 
        frame of the prediction of a coreference partition for the input document.
        
        Args:
            document: a Document instance

        Returns:
            an "edge => score" map
        """
        return self._compute_naturally_extended_mention_pair_prediction_pair_scores(document)
    
    def _compute_predicted_tree_edges_from_edge2loss_score(self, document, edge2weight): #, entity_head_extents=None
        """ Computes a latent "predicted" graph associated to the mentions of the input document
        (that is to say, a graph that does not take into account the info brought by the ground truth 
        coreference partition associated to the input document, but only relies on weight values 
        associated to possible edges), and return the edges of this graph.
        
        Here, the graph is in fact a tree.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            edge2weight: an "edge => weigt" map, where the edges correspond to all mention pairs 
                that are implicitly defined by the application of the underlying generation process on the 
                input document

        Returns:
            a collection of edges corresponding to the computed "predicted" graph
        """
        mentions = document.mentions
        predicted_graph_edges = []
        for j in range(len(mentions)):
            subsequent_mention = mentions[j]
            max_score = 0. # FIXME: Because the loss score values are all supposed to be >= 0?
            # Is it supposed to be the decoding threshold values associated to scores that are not proba values?
            max_score_antecedent_mention = None
            for i in range(j-1, -1, -1):
                antecedent_mention = mentions[i]
                # Skip pronominal antecent mention for non-pronominal anaphora
                if MENTION_FEATURES.is_expanded_pronoun(antecedent_mention) and not MENTION_FEATURES.is_expanded_pronoun(subsequent_mention):
                    continue
                score = edge2weight.get(get_mention_pair_extent_pair(antecedent_mention,subsequent_mention)) # Some pairs can have been filtered out
                if score is None:
                    continue
                if score > max_score:
                    max_score = score
                    max_score_antecedent_mention = antecedent_mention
            default_extent_pair = get_mention_pair_extent_pair(NULL_MENTION, subsequent_mention)
            # FIXME: because of the following code lines, a pair containing the current subsequent mention won't be added, 
            # even if a suitable real antecedent mention has been found: is that normal?
            # Though, if max_antecedent_mention is still equal to None, at least we are sur ethat no pair will be built from using this None value
            if default_extent_pair not in edge2weight: # Some pairs can have been filtered out
                continue
            if edge2weight[default_extent_pair] > max_score or max_score_antecedent_mention is None:
                max_score_antecedent_mention = NULL_MENTION
            predicted_graph_edges.append(get_mention_pair_extent_pair(max_score_antecedent_mention, subsequent_mention))
        return predicted_graph_edges




##### Constrained Best First #####
class _BaseConstrainedBestFirstDocumentStructuredResolver(_ConstrainedDocumentStructuredPairResolverMixIn, 
                                                          ConstrainedEdgesDefinerMixIn, 
                                                          _BaseBestFirstDocumentStructuredResolver):
    """ Base class for resolver whose latent structure is a graph where an edge of the 'true' graph 
    structure is defined as corresponding to a pair of mentions that either:
    
        - corresponds to a Must-Link
        
        or
        
        - is chosen by the 'best first' algorithm, and which corresponds to an existing coreference link, 
    
    and which does not correspond to a Cannot-Link; 
    and where the decoding is done using the 'constrained best first' algorithm.
    
    Cf the _BaseBestFirstDocumentStructuredResolver class.
    Cf the ConstrainedEdgesDefinerMixIn class.
    
    As such, can be instantiated only with a _OnlineBinaryClassifier child class instance, a 
    _BaseMentionPairGenerator child class instance and a __BaseMentionPairCoreferenceDecoder child 
    class instance.
    
    Arguments:
        model_interface: a _OnlineBinaryClassifier child class instance, representing the interface 
            to the underlying online machine learning model to train and to use
        
        generator: a _BaseMentionPairGenerator child class instance, used to generate, from document(s), 
            machine-learning samples used as input by the underlying machine learning model
        
        decoder: a __BaseMentionPairCoreferenceDecoder child class instance, used to interpret scores 
            associated to machine-learning samples into a coreference partition, for the document used 
            to generate the scored machine-learning samples
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        update_mode: string, either 'PB' or 'ML'; cf the class documentation
        
        constraints_definition: None, collection of (constraint_type, list or tuple, dict) 
            tuples, where: 
                - 'constraint type' must be one of the possible constraint types (cf this class' docstring)
                - 'args' and 'kwargs' will be passed along to the constraints function; for now, none of them 
                  requires supplementary parameters, so args and kwargs must remain empty for them
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        sample_type: string specifying the type of the sample used in the frame of the underlying 
            machine-learning task
        
        document_hash2cache: document_hash2cache: a "document hash => cache" map, or None; the 
            current value of the underlying model interface's vectorization cache (cf 
            'cortex.coreference.api.vectorization_cache.py')
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        update_mode: string, either 'PB' or 'ML'; cf the class documentation
        
        constraints_definition: None, collection of (constraint_type, list or tuple, dict) 
            tuples, where: 
                - 'constraint type' must be one of the possible constraint types (cf this class' docstring)
                - 'args' and 'kwargs' will be passed along to the constraints function; for now, none of them 
                  requires supplementary parameters, so args and kwargs must remain empty for them
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = get_unique_elts(_BaseBestFirstDocumentStructuredResolver._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION\
                                                       + ConstrainedEdgesDefinerMixIn._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION)
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = _TreeLossDocumentStructuredPairResolver._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION+\
                                            get_unique_elts(ConstrainedEdgesDefinerMixIn._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION)
    
    def __init__(self, model_interface, generator, decoder, root_loss, 
                 update_mode, constraints_definition, configuration=None):
        # Check input values
        # Initialize using parent class
        _BaseBestFirstDocumentStructuredResolver.__init__(self, model_interface, generator, decoder, 
                                                         root_loss, update_mode, configuration=configuration)
        ConstrainedEdgesDefinerMixIn.__init__(self, constraints_definition, configuration=self.configuration)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args1, kwargs1 = _BaseBestFirstDocumentStructuredResolver._extract_args_kwargs_from_configuration(configuration)
        args2, kwargs2 = ConstrainedEdgesDefinerMixIn._extract_args_kwargs_from_configuration(configuration)
        args = args1 + args2
        kwargs = kwargs1
        kwargs.update(kwargs2)
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, generator_factory_configuration, decoder_factory_configuration, 
                             sample_features_vectorizer_factory_configuration, 
                             root_loss, update_mode, classifier_factory_configuration=None, 
                             constraints_definition=None):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class.
        
        Args:
            generator_factory_configuration: a factory configuration that can be used to create 
                a specific _Generator child class instance
            decoder_factory_configuration: a factory configuration that can be used to create 
                a specific _CoreferenceDecoder child class instance
            sample_features_vectorizer_factory_configuration: a factory configuration that can be 
                used to create a specific _BaseExtendedMentionPairSampleFeaturesVectorizer child class instance
            root_loss: a non-negative real number; cf the class documentation
            update_mode: string, either 'PB' or 'ML'; cf the class documentation
            classifier_factory_configuration: a factory configuration that can be used to 
                create a specific _OnlineBinaryClassifier child class instance; or None, in which case the 
                factory configuration for a 'PerceptronOnlineBinaryClassifier' instance (parametrized with 
                'avg=True, warm_start=False') will be used

        Returns:
            a Mapping instance
        """
        configuration1 = ConstrainedEdgesDefinerMixIn.define_configuration(constraints_definition=constraints_definition)
        configuration2 = _BaseBestFirstDocumentStructuredResolver.define_configuration(generator_factory_configuration, 
                                                                                      decoder_factory_configuration, 
                                                                                      sample_features_vectorizer_factory_configuration, 
                                                                                      root_loss, update_mode, 
                                                                                      classifier_factory_configuration=classifier_factory_configuration)
        type(configuration1).update(configuration1, configuration2)
        return configuration1
    
    # Instance methods
    def _compute_ground_truth_graph_edges(self, document, extended_edge2score, true_extended_edge2score, 
                                          true_coreference_edge2score): #, entity_head_extents=None
        """ Computes a latent "ground truth" graph associated to the mentions of the input document 
        (that is to say, a graph that takes into account the info brought by the ground truth 
        coreference partition associated to the input document), and return the edges of this graph.
        
        As for the algorithm used, cf the class' documentation.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            extended_edge2score: an "edge => score" map, where the edges correspond to all mention 
                pairs that are implicitly defined by the application of the underlying generation process on 
                the input document. Not used here, exists only for API compatibility reasons.
            true_extended_edge2score: an "edge => score" map, where the edges correspond to 
                mention pairs that are implicitly defined by the application of the underlying generation 
                process on the input document, and which are associated to the positive class
            true_coreference_edge2score: an "edge => score" map, where the edges correspond to 
                mention pairs that are implicitly defined by the application of the underlying generation 
                process on the input document, and which correspond to true mentions belonging to the same 
                coreference entity. Not used here, exists only for API compatibility reasons.

        Returns:
            a collection of edges
        """
        # Get constraints
        positive_edges, negative_edges = self.get_constraints(document) #, entity_head_extents=entity_head_extents
        
        #raise Exception()
        neg_set = set(negative_edges)
        already_set = set() # Corresponds to mentions at right position in a positive constraint
        
        # Join positive constrains
        for extent1, extent2 in positive_edges:
            m1 = document.extent2mention[extent1]
            m2 = document.extent2mention[extent2]
            if m1 < m2:
                already_set.add(extent1)
            if m2 < m1:
                already_set.add(extent2)
        
        ground_truth_graph_edges = list(positive_edges)
        entities = tuple(document.coreference_partition)
        for entity in entities:
            sorted_mention_extents = sorted(entity)
            sorted_mentions = tuple(document.extent2mention[extent] for extent in sorted_mention_extents)
            entity_size = len(entity)
            first_mention = sorted_mentions[0]
            ground_truth_graph_edges.append(get_mention_pair_extent_pair(NULL_MENTION, first_mention))
            for j in range(1,entity_size):
                subsequent_mention = sorted_mentions[j]
                if subsequent_mention.extent in already_set:
                    continue # Subsequent_mention already linked to an antecedent
                max_score = -numpy.Inf
                max_score_antecedent_mention = None
                for i in range(j-1, -1, -1):
                    antecedent_mention = sorted_mentions[i]
                    # Skip pronominal antecent mention for non-pronominal anaphora
                    if MENTION_FEATURES.is_expanded_pronoun(antecedent_mention) and not MENTION_FEATURES.is_expanded_pronoun(subsequent_mention):
                        continue
                    # Skip neg_set elements
                    extent_pair = get_mention_pair_extent_pair(antecedent_mention, subsequent_mention)
                    if extent_pair in neg_set or get_mention_pair_extent_pair(subsequent_mention, antecedent_mention) in neg_set:
                        continue
                    score = true_extended_edge2score.get(extent_pair)
                    if score is None:
                        continue
                    if score > max_score:
                        max_score = score
                        max_score_antecedent_mention = antecedent_mention
                if max_score_antecedent_mention is None:
                    max_score_antecedent_mention = NULL_MENTION
                ground_truth_graph_edges.append(get_mention_pair_extent_pair(max_score_antecedent_mention, subsequent_mention))
        
        return ground_truth_graph_edges


class ConstrainedBestFirstDocumentStructuredResolver(_BaseConstrainedBestFirstDocumentStructuredResolver):
    """ Class for resolver whose latent structure is a graph where:
    
        - an edge of the 'true' graph structure is defined as corresponding to a pair of mentions 
          that either:
            
            - corresponds to a Must-Link
            
            or
            
            - is chosen by the 'best first' algorithm, and which corresponds to an existing coreference 
            link, and which does not correspond to a Cannot-Link; 
        
        - an edge of the 'predicted' graph structure is defined as a corresponding pair of mentions 
          that either:
            
            - corresponds to a Must-Link
            
            or
            
            - is chosen by the 'best first' algorithm
    
    and where the underlying mention pairs generation process does not support the NULL_MENTION, so 
    support for it has to be artificially implemented.
    
    The decoding is done using the 'constrained best first' algorithm.
    
    As such, can be instantiated only with a _OnlineBinaryClassifier child class instance, a 
    MentionPairSampleGenerator instance and a ConstrainedBestFirstCoreferenceDecoder instance.
    
    Arguments:
        model_interface: a _OnlineBinaryClassifier child class instance, representing the interface 
            to the underlying online machine learning model to train and to use
        
        generator: a MentionPairSampleGenerator instance, used to generate, from document(s), 
            machine-learning samples used as input by the underlying machine learning model
        
        decoder: a ConstrainedBestFirstCoreferenceDecoder instance, used to interpret scores 
            associated to machine-learning samples into a coreference partition, for the document used 
            to generate the scored machine-learning samples
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        update_mode: string, either 'PB' or 'ML'; cf the class documentation
        
        constraints_definition: None, collection of (constraint_type, list or tuple, dict) 
            tuples, where: 
                - 'constraint type' must be one of the possible constraint types (cf this class' docstring)
                - 'args' and 'kwargs' will be passed along to the constraints function; for now, none of them 
                  requires supplementary parameters, so args and kwargs must remain empty for them
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        sample_type: string specifying the type of the sample used in the frame of the underlying 
            machine-learning task
        
        document_hash2cache: document_hash2cache: a "document hash => cache" map, or None; the 
            current value of the underlying model interface's vectorization cache (cf 
            'cortex.coreference.api.vectorization_cache.py')
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        update_mode: string, either 'PB' or 'ML'; cf the class documentation
        
        constraints_definition: None, collection of (constraint_type, list or tuple, dict) 
            tuples, where: 
                - 'constraint type' must be one of the possible constraint types (cf this class' docstring)
                - 'args' and 'kwargs' will be passed along to the constraints function; for now, none of them 
                  requires supplementary parameters, so args and kwargs must remain empty for them
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _check_subcomponent(self, model_interface, generator, decoder):
        """ Checks that the input model interface, generator, and decoder are correct; that is: 
        
        - the model_interface is a _OnlineBinaryClassifier child class instance
        - the generator is a MentionPairSampleGenerator instance
        - the decoder is a ConstrainedBestFirstCoreferenceDecoder instance
        
        Raises:
            ValueError: if one input is not of the expected type
        """
        _check_isinstance(MentionPairSampleGenerator, generator, "generator")
        _check_isinstance(ConstrainedBestFirstCoreferenceDecoder, decoder, "decoder")
        super()._check_subcomponent(model_interface, generator, decoder)
    
    @classmethod
    def define_configuration(cls, sample_features_vectorizer_factory_configuration=None, root_loss=1.0, 
                             update_mode="PB", classifier_factory_configuration=None, 
                             generator_pair_filter_factory_configuration=None, 
                             constraints_definition=None):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class.
        
        Args:
            sample_features_vectorizer_factory_configuration: a factory configuration that can be 
                used to create a specific _BaseMentionPairSampleFeaturesVectorizer child class instance; 
                or None, in which case the factory configuration for a 'MentionPairSampleFeaturesVectorizer' 
                instance (parametrized with 'quantize=True') will be used
            root_loss: a non-negative real number; cf the class documentation
            update_mode: string, either 'PB' or 'ML'; cf the class documentation
            classifier_factory_configuration: a factory configuration that can be used to 
                create a specific _OnlineBinaryClassifier child class instance; or None, in which case the 
                factory configuration for a 'PerceptronOnlineBinaryClassifier' instance (parametrized with 
                'avg=True, warm_start=False') will be used
            generator_pair_filter_factory_configuration: a factory configuration that can be used 
                to create a specific PairFilter instance, that shall be used to generate the factory 
                configuration of an instance of the _Generator child class compatible with this resolver class; 
                or None, in which case the factory configuration for an 'AcceptAllPairFilter' instance will 
                be used
            constraints_definition: None, collection of (constraint_type, list or tuple, dict) 
                tuples, where: 
                    - 'constraint type' must be one of the possible constraint types (cf this class' docstring)
                    - 'args' and 'kwargs' will be passed along to the constraints function; for now, none of them 
                      requires supplementary parameters, so args and kwargs must remain empty for them

        Returns:
            a Mapping instance
        """
        # Check and process inputs
        if sample_features_vectorizer_factory_configuration is None:
            sample_features_vectorizer_factory_configuration =\
             MentionPairSampleFeaturesVectorizer.define_factory_configuration(quantize=True)
        if generator_pair_filter_factory_configuration is None:
            generator_pair_filter_factory_configuration = AcceptAllPairFilter.define_factory_configuration()
        
        generator_factory_configuration =\
         MentionPairSampleGenerator.define_factory_configuration(pair_filter_factory_configuration=generator_pair_filter_factory_configuration)
        decoder_factory_configuration = ConstrainedBestFirstCoreferenceDecoder.define_factory_configuration()
        return super().define_configuration(generator_factory_configuration, decoder_factory_configuration, 
                                               sample_features_vectorizer_factory_configuration, root_loss, 
                                               update_mode, classifier_factory_configuration=classifier_factory_configuration,
                                               constraints_definition=constraints_definition)
    
    # Instance methods
    def _generate_edge_samples(self, document, edges):
        """ Creates collection of MentionPairSample instances corresponding to pairs of mentions made 
        from mentions coming from the input document, pairs that are specified by input edges collection.
        
        Only edges whose corresponding mentions pair that can be produced by the underlying generator, 
        from the input document, shall see the creation of a corresponding MentionPairSample instance.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            edges: collection of edges

        Returns:
            a collection of MentionPairSample instances, where all mentions of the pairs are 
            true mentions (no one is the NULL_MENTION)
        """
        return self._generate_mention_pair_edge_samples(document, edges)
    
    def _compute_training_phase_extended_mention_pair_scores(self, document):
        """ Computes scores to be associated to extended mention pairs (so, also for pairs whose 
        antecedent mention is the NULL_MENTION); using classification prediction on the mention pair 
        samples collection generated from the input document; and interpreting the classification 
        confidence scores as weights on the edges of a graph of mentions.
        
        Score values may be artificially created for for pairs whose antecedent mention is the 
        NULL_MENTION, if the underlying generation process is unable to generate such mention pairs.
        The scores are float values.
        The output of this method is destined to be used as input to build a virtual ground truth 
        graph during the "prediction" of an atomic training step carried out using the input document 
        as support.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance

        Returns:
            a (extended_edge2score, true_extended_edge2score, true_coreference_edge2score) 
            triplet, where:
                - 'extended_edge2score' is the "edge => score" map resulting for carrying out classification 
                  prediction on the samples generated from the input document, using the underlying generation 
                  process
                - 'true_extended_edge2score' is a submap of the 'extended_edge2score' map, only 
                  for edges that correspond to mention pairs that are made of mentions that belong to the same 
                  coreference entity, (their corresponding sample have been labelled with the positive class 
                  label), or correspond to non-anaphoric mentions (the subsequent 'mention' of the pair is a 
                  true mention, whereas the antecedent 'mention' of the pair is the NULL_MENTION)
                - 'true_coreference_edge2score' is is a submap of the 'extended_edge2score' map, only 
                  for edges that correspond to mention pairs that are made of mentions that belong to the same 
                  coreference entity (their corresponding sample have been labelled with the positive class 
                  label), which means that none of the mentions of those pairs is the NULL_MENTION
        """
        return self._compute_training_phase_artificially_extended_mention_pair_scores(document)

    def _compute_prediction_phase_extended_mention_pair_scores(self, document):
        """ Computes scores to be associated to extended mention pairs (so, also for pairs whose 
        antecedent mention is the NULL_MENTION); using classification prediction on the mention pair 
        samples collection generated from the input document; and interpreting the classification 
        confidence scores as weights on the edges of a graph of mentions.
        
        Score values may be artificially created for for pairs whose antecedent mention is the 
        NULL_MENTION, if the underlying generation process is unable to generate such mention pairs.
        The scores are float values.
        The output of this method is destined to be used as input to the decoding process, in the 
        frame of the prediction of a coreference partition for the input document.
        
        Args:
            document: a Document instance

        Returns:
            an "edge => score" map
        """
        return self._compute_artificially_extended_mention_pair_prediction_pair_scores(document)
    
    def _compute_predicted_tree_edges_from_edge2loss_score(self, document, edge2weight): #, entity_head_extents=None
        """ Computes a latent "predicted" graph associated to the mentions of the input document
        (that is to say, a graph that does not take into account the info brought by the ground truth 
        coreference partition associated to the input document, but only relies on weight values 
        associated to possible edges), and return the edges of this graph.
        
        Here, the graph is in fact a tree.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            edge2weight: an "edge => weigt" map, where the edges correspond to all mention pairs 
                that are implicitly defined by the application of the underlying generation process on the 
                input document

        Returns:
            collection of edges corresponding to the computed "predicted" graph
        """
        # Get constraints
        positive_edges, negative_edges = self.get_constraints(document) #, entity_head_extents
        neg_set = set(negative_edges)
        already_set = set() # Corresponds to mentions at right position in a positive constraint
        
        # Join positive constrains
        for extent1, extent2 in positive_edges:
            m1 = document.extent2mention[extent1]
            m2 = document.extent2mention[extent2]
            if m1 < m2:
                already_set.add(extent1)
            if m2 < m1:
                already_set.add(extent2)
        
        predicted_graph_edges = list(positive_edges)
        mentions = document.mentions
        for j in range(len(mentions)):
            subsequent_mention = mentions[j]
            if subsequent_mention.extent in already_set:
                continue
            max_score = 0. # FIXME: Because the loss score values are all supposed to be >= 0?
            # Is it supposed to be the decoding threshold values associated to scores that are not proba values?
            max_score_antecedent_mention = None
            for i in range(j-1, -1, -1):
                antecedent_mention = mentions[i]
                # Skip pronominal antecent mention for non-pronominal anaphora
                if MENTION_FEATURES.is_expanded_pronoun(antecedent_mention) and not MENTION_FEATURES.is_expanded_pronoun(subsequent_mention):
                    continue
                # Skip neg_set elements
                extent_pair = get_mention_pair_extent_pair(antecedent_mention, subsequent_mention)
                if extent_pair in neg_set or get_mention_pair_extent_pair(subsequent_mention, antecedent_mention) in neg_set:
                    continue
                score = edge2weight.get(extent_pair)
                if score is None:
                        continue
                if score > max_score:
                    max_score_antecedent_mention = antecedent_mention
                    max_score = score
            if max_score_antecedent_mention is None:
                max_score_antecedent_mention = NULL_MENTION
            predicted_graph_edges.append(get_mention_pair_extent_pair(max_score_antecedent_mention, subsequent_mention))
        return predicted_graph_edges



class ConstrainedExtendedBestFirstDocumentStructuredResolver(_BaseConstrainedBestFirstDocumentStructuredResolver):
    """ Class for resolver whose latent structure is a graph where:
    
        - an edge of the 'true' graph structure is defined as corresponding to a pair of mentions 
          that either:
            - corresponds to a Must-Link
            
            or
            
            - is chosen by the 'best first' algorithm, and which corresponds to an existing coreference 
              link, and which does not correspond to a Cannot-Link; 
        
        - an edge of the 'predicted' graph structure is defined as a corresponding pair of mentions 
          that either:
            - corresponds to a Must-Link
            
            or
            
            - is chosen by the 'best first' algorithm
    
    and where the underlying mention pairs generation process does support the NULL_MENTION.
    
    The decoding is done using the 'constrained best first' algorithm.
    
    The sample features vectorizer class used to parametrize this resolver must inherit from 
    '_BaseExtendedMentionPairSampleFeaturesVectorizer', it must have 'extended' in its name.
    
    Cf the _BaseConstrainedBestFirstDocumentStructuredResolver class.
    
    As such, can be instantiated only with a _OnlineBinaryClassifier child class instance, a 
    ExtendedMentionPairSampleGenerator instance and a ConstrainedExtendedBestFirstCoreferenceDecoder 
    instance.
    
    Arguments:
        model_interface: a _OnlineBinaryClassifier child class instance, representing the interface 
            to the underlying online machine learning model to train and to use
        
        generator: a ExtendedMentionPairSampleGenerator instance, used to generate, from document(s), 
            machine-learning samples used as input by the underlying machine learning model
        
        decoder: a ConstrainedExtendedBestFirstCoreferenceDecoder instance, used to interpret scores 
            associated to machine-learning samples into a coreference partition, for the document used 
            to generate the scored machine-learning samples
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        update_mode: string, either 'PB' or 'ML'; cf the class documentation
        
        constraints_definition: None, collection of (constraint_type, list or tuple, dict) 
            tuples, where: 
                - 'constraint type' must be one of the possible constraint types (cf this class' docstring)
                - 'args' and 'kwargs' will be passed along to the constraints function; for now, none of them 
                  requires supplementary parameters, so args and kwargs must remain empty for them
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        sample_type: string specifying the type of the sample used in the frame of the underlying 
            machine-learning task
        
        document_hash2cache: document_hash2cache: a "document hash => cache" map, or None; the 
            current value of the underlying model interface's vectorization cache (cf 
            'cortex.coreference.api.vectorization_cache.py')
        
        root_loss: a non-negative real number, the loss value incurred by a possible edge of the 
            "predicted" graph that is not found in the "ground truth" graph, when this edge contains the 
            root node (corresponding to the NULL_MENTION). Default value is equal to 1, the same loss 
            value incurred by possible edges whose nodes are not the root node.
        
        update_mode: string, either 'PB' or 'ML'; cf the class documentation
        
        constraints_definition: None, collection of (constraint_type, list or tuple, dict) 
            tuples, where: 
                - 'constraint type' must be one of the possible constraint types (cf this class' docstring)
                - 'args' and 'kwargs' will be passed along to the constraints function; for now, none of them 
                  requires supplementary parameters, so args and kwargs must remain empty for them
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _check_subcomponent(self, model_interface, generator, decoder):
        """ Checks that the input model interface, generator, and decoder are correct; that is: 
        
        - the model_interface is a _OnlineBinaryClassifier child class instance
        - the generator is a ExtendedMentionPairSampleGenerator instance
        - the decoder is a ConstrainedExtendedBestFirstCoreferenceDecoder instance
        
        Raises:
            ValueError: if one input is not of the expected type
        """
        _check_isinstance(ExtendedMentionPairSampleGenerator, generator, "generator")
        _check_isinstance(ConstrainedExtendedBestFirstCoreferenceDecoder, decoder, "decoder")
        super()._check_subcomponent(model_interface, generator, decoder)
    
    @classmethod
    def define_configuration(cls, sample_features_vectorizer_factory_configuration=None, root_loss=1.0, 
                             update_mode="PB", classifier_factory_configuration=None, 
                             generator_pair_filter_factory_configuration=None,
                             constraints_definition=None):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class.
        
        Args:
            sample_features_vectorizer_factory_configuration: a factory configuration that can be 
                used to create a specific _BaseExtendedMentionPairSampleFeaturesVectorizer child class instance; 
                or None, in which case the factory configuration for a 'ExtendedMentionPairSampleFeaturesVectorizer' 
                instance (parametrized with 'quantize=True') will be used
            root_loss: a non-negative real number; cf the class documentation
            update_mode: string, either 'PB' or 'ML'; cf the class documentation
            classifier_factory_configuration: a factory configuration that can be used to 
                create a specific _OnlineBinaryClassifier child class instance; or None, in which case the 
                factory configuration for a 'PerceptronOnlineBinaryClassifier' instance (parametrized with 
                'avg=True, warm_start=False') will be used
            generator_pair_filter_factory_configuration: a factory configuration that can be used 
                to create a specific PairFilter instance, that shall be used to generate the factory 
                configuration of an instance of the _Generator child class compatible with this resolver class; 
                or None, in which case the factory configuration for an 'AcceptAllPairFilter' instance will 
                be used
            constraints_definition: None, collection of (constraint_type, list or tuple, dict) 
                tuples, where: 
                    - 'constraint type' must be one of the possible constraint types (cf this class' docstring)
                    - 'args' and 'kwargs' will be passed along to the constraints function; for now, none of them 
                      requires supplementary parameters, so args and kwargs must remain empty for them

        Returns:
            a Mapping instance
        """
        # Check and process inputs
        if sample_features_vectorizer_factory_configuration is None:
            sample_features_vectorizer_factory_configuration =\
             ExtendedMentionPairSampleFeaturesVectorizer.define_factory_configuration(quantize=True)
        if generator_pair_filter_factory_configuration is None:
            generator_pair_filter_factory_configuration = AcceptAllPairFilter.define_factory_configuration()
        if sample_features_vectorizer_factory_configuration["name"] not in POSSIBLE_EXTENDED_MENTION_PAIR_SAMPLES_VECTORIZER_NAMES:
            msg = "Incorrect input classifier_configurations name '{}': must belong to '{}'."
            msg = msg.format(classifier_factory_configuration["name"], 
                             POSSIBLE_EXTENDED_MENTION_PAIR_SAMPLES_VECTORIZER_NAMES)
            raise ValueError(msg)
        
        generator_factory_configuration =\
         ExtendedMentionPairSampleGenerator.define_factory_configuration(pair_filter_factory_configuration=generator_pair_filter_factory_configuration)
        decoder_factory_configuration = ConstrainedExtendedBestFirstCoreferenceDecoder.define_factory_configuration()
        return super().define_configuration(generator_factory_configuration, decoder_factory_configuration, 
                                               sample_features_vectorizer_factory_configuration, root_loss, 
                                                update_mode, classifier_factory_configuration=classifier_factory_configuration,
                                                constraints_definition=constraints_definition)
    
    # Instance methods
    def _generate_edge_samples(self, document, edges):
        """ Creates collection of MentionPairSample instances corresponding to pairs of mentions made 
        from mentions coming from the input document, pairs that are specified by input edges collection.
        
        Only edges whose corresponding mentions pair that can be produced by the underlying generator, 
        from the input document, shall see the creation of a corresponding MentionPairSample instance.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            edges: collection of edges

        Returns:
            collection of MentionPairSample instances, where all mentions of the pairs are 
        true mentions (no one is the NULL_MENTION)
        """
        return self._generate_extended_mention_pair_edge_samples(document, edges)
    
    def _compute_training_phase_extended_mention_pair_scores(self, document):
        """ Computes scores to be associated to extended mention pairs (so, also for pairs whose 
        antecedent mention is the NULL_MENTION); using classification prediction on the mention pair 
        samples collection generated from the input document; and interpreting the classification 
        confidence scores as weights on the edges of a graph of mentions.
        
        Score values may be artificially created for for pairs whose antecedent mention is the 
        NULL_MENTION, if the underlying generation process is unable to generate such mention pairs.
        The scores are float values.
        The output of this method is destined to be used as input to build a virtual ground truth 
        graph during the "prediction" of an atomic training step carried out using the input document 
        as support.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance

        Returns:
            a (extended_edge2score, true_extended_edge2score, true_coreference_edge2score) 
            triplet, where:
                - 'extended_edge2score' is the "edge => score" map resulting for carrying out classification 
                  prediction on the samples generated from the input document, using the underlying generation 
                  process
                - 'true_extended_edge2score' is a submap of the 'extended_edge2score' map, only 
                  for edges that correspond to mention pairs that are made of mentions that belong to the same 
                  coreference entity, (their corresponding sample have been labelled with the positive class 
                  label), or correspond to non-anaphoric mentions (the subsequent 'mention' of the pair is a 
                  true mention, whereas the antecedent 'mention' of the pair is the NULL_MENTION)
                - 'true_coreference_edge2score' is is a submap of the 'extended_edge2score' map, only 
                  for edges that correspond to mention pairs that are made of mentions that belong to the same 
                  coreference entity (their corresponding sample have been labelled with the positive class 
                  label), which means that none of the mentions of those pairs is the NULL_MENTION
        """
        return self._compute_training_phase_naturally_extended_mention_pair_scores(document)

    def _compute_prediction_phase_extended_mention_pair_scores(self, document):
        """ Computes scores to be associated to extended mention pairs (so, also for pairs whose 
        antecedent mention is the NULL_MENTION); using classification prediction on the mention pair 
        samples collection generated from the input document; and interpreting the classification 
        confidence scores as weights on the edges of a graph of mentions.
        
        Score values may be artificially created for for pairs whose antecedent mention is the 
        NULL_MENTION, if the underlying generation process is unable to generate such mention pairs.
        The scores are float values.
        The output of this method is destined to be used as input to the decoding process, in the 
        frame of the prediction of a coreference partition for the input document.
        
        Args:
            document: a Document instance

        Returns:
            an "edge => score" map
        """
        return self._compute_naturally_extended_mention_pair_prediction_pair_scores(document)
    
    def _compute_predicted_tree_edges_from_edge2loss_score(self, document, edge2weight): #, entity_head_extents=None
        """ Computes a latent "predicted" graph associated to the mentions of the input document
        (that is to say, a graph that does not take into account the info brought by the ground truth 
        coreference partition associated to the input document, but only relies on weight values 
        associated to possible edges), and return the edges of this graph.
        
        Here, the graph is in fact a tree.
        
        Args:
            document: a Document instance, whose 'coreference_partition' attribute value is indeed 
                a proper, consistent CoreferencePartition instance
            edge2weight: an "edge => weigt" map, where the edges correspond to all mention pairs 
                that are implicitly defined by the application of the underlying generation process on the 
                input document

        Returns:
            a collection of edges corresponding to the computed "predicted" graph
        """
        # Get constraints
        positive_edges, negative_edges = self.get_constraints(document) #, entity_head_extents
        neg_set = set(negative_edges)
        already_set = set() # Corresponds to mentions at right position in a positive constraint
        
        # Join positive constrains
        for extent1, extent2 in positive_edges:
            m1 = document.extent2mention[extent1]
            m2 = document.extent2mention[extent2]
            if m1 < m2:
                already_set.add(extent1)
            if m2 < m1:
                already_set.add(extent2)
        
        predicted_graph_edges = list(positive_edges)
        mentions = document.mentions
        for j in range(len(mentions)):
            subsequent_mention = mentions[j]
            if subsequent_mention.extent in already_set:
                continue
            max_score = 0. # FIXME: Because the loss score values are all supposed to be >= 0?
            # Is it supposed to be the decoding threshold values associated to scores that are not proba values?
            max_score_antecedent_mention = None
            for i in range(j-1, -1, -1):
                antecedent_mention = mentions[i]
                # Skip pronominal antecent mention for non-pronominal anaphora
                if MENTION_FEATURES.is_expanded_pronoun(antecedent_mention) and not MENTION_FEATURES.is_expanded_pronoun(subsequent_mention):
                    continue
                # Skip neg_set elements
                extent_pair = get_mention_pair_extent_pair(antecedent_mention, subsequent_mention)
                if extent_pair in neg_set or get_mention_pair_extent_pair(subsequent_mention, antecedent_mention) in neg_set:
                    continue
                score = edge2weight.get(extent_pair)
                if score is None:
                    continue
                if score > max_score:
                    max_score = score
                    max_score_antecedent_mention = antecedent_mention
            default_extent_pair = get_mention_pair_extent_pair(NULL_MENTION, subsequent_mention)
            # FIXME: because of the following code lines, a pair containing the current subsequent mention won't be added, 
            # even if a suitable real antecedent mention has been found: is that normal?
            # Though, if max_antecedent_mention is still equal to None, at least we are sur ethat no pair will be built from using this None value
            if default_extent_pair not in edge2weight: # Some pairs can have been filtered out
                continue
            if edge2weight[default_extent_pair] > max_score or max_score_antecedent_mention is None:
                max_score_antecedent_mention = NULL_MENTION
            predicted_graph_edges.append(get_mention_pair_extent_pair(max_score_antecedent_mention, subsequent_mention))
        return predicted_graph_edges

