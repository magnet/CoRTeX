# -*- coding: utf-8 -*-

"""
Defines a MixIn class used by resolver that supports the integration of external knowledges about 
Must-Link or Cannot_link mention pairs.
"""

__all__ = ["ConstrainedEdgesDefinerMixIn",
           "get_anaphoricity_constraints",
           "get_proper_match_constraints",
           "get_animated_constraints",
           "get_embedding_constraints",
           "CONSTRAINT_NAME2CONSTRAINT_FCT",
           ]

from collections import OrderedDict, defaultdict

from cortex.tools import ConfigurationMixIn, _create_simple_set_parameter_value_in_configuration
from cortex.utils import EqualityMixIn
from cortex.parameters.mention_data_tags import NAME_GRAM_TYPE_TAG, UNKNOWN_VALUE_TAG
from cortex.api.markable import NULL_MENTION

from cortex.config import language
from cortex.languages import create_get_language_dependent_object_fct
get_language_dependent_object = create_get_language_dependent_object_fct(language)

MENTION_FEATURES = get_language_dependent_object("mention_features")
RULES_TYPE2EDGES_CONSTRAINTS_FCT = get_language_dependent_object("rules2edges_constraints_fct")


# Definition of functions that specify whether or not some edges can be determined to be Must-Link ('ML'), or Cannot-Link ('CL')
## CL anaphoricity: cut root-mention for anaphoric, cut out-left arcs from head
def get_anaphoricity_constraints(document, positive_edges, negative_edges, entity_head_extents=None):#mentions, 
    """ Defines Cannot-Link constraints based on the knowledge that some mentions are anaphora and 
    some are not.
    
    For each mention pair for which this function has defined a constraint, the corresponding input 
    set of mention pairs will be updated
    
    Args:
        document: a Document instance for which mentions are defined
        positive_edges: set of pairs of mention extents. Not used, exists for API compatibility 
            purposes.
        negative_edges: set of pairs of mention extents, will be potentially updated
        entity_head_extents: set of mention extents; or None. If None, this specific function 
            will do nothing, since it is necessary to determine whether or not a mention is an anaphora.
    """
    mentions = document.mentions
    if entity_head_extents is not None:
        for subsequent_mention in mentions:
            if subsequent_mention.extent in entity_head_extents:
                for antecedent_mention in mentions:
                    if antecedent_mention < subsequent_mention:
                        negative_edges.add((antecedent_mention.extent, subsequent_mention.extent))
                    else:
                        break
            else:
                negative_edges.add((NULL_MENTION.extent, subsequent_mention.extent))

## ML proper noun match
def get_proper_match_constraints(document, positive_edges, negative_edges, entity_head_extents=None):#mentions, 
    """ Defines Must-Link constraints based on the knowledge that some mentions' respective raw text 
    value are equal
    
    For each mention pair for which this function has defined a constraint, the corresponding input 
    set of mention pairs will be updated.
    
    Args:
        document: a Document instance for which mentions are defined
        positive_edges: set of pairs of mention extents, will be potentially updated
        negative_edges: set of pairs of mention extents. Not used, exists for API compatibility 
            purposes.
        entity_head_extents: set of mention extents, or None; not used, exists for shared 
            signature convention reasons
    """
    mentions = document.mentions
    for subsequent_mention in mentions:
        if subsequent_mention.gram_type == NAME_GRAM_TYPE_TAG:
            for antecedent_mention in mentions:
                if antecedent_mention >= subsequent_mention:
                    break
                if antecedent_mention.gram_type == NAME_GRAM_TYPE_TAG\
                 and antecedent_mention.raw_text.lower() == subsequent_mention.raw_text.lower():
                    positive_edges.add((antecedent_mention.extent,subsequent_mention.extent))

## CL animated
def get_animated_constraints(document, positive_edges, negative_edges, entity_head_extents=None):#mentions, 
    """ Defines Cannot-Link constraints based on the knowledge that some mentions refer to an animated 
    entity, and some refer to an inanimated entity
    
    For each mention pair for which this function has defined a constraint, the corresponding input 
    set of mention pairs will be updated.
    
    Args:
        document: a Document instance for which mentions are defined
        positive_edges: set of pairs of mention extents. Not used, exists for API compatibility 
            purposes.
        negative_edges: set of pairs of mention extents, will be potentially updated
        entity_head_extents: set of mention extents, or None. Not used, exists for API compatibility 
            purposes.
    """
    mentions = document.mentions
    for subsequent_mention in mentions:
        a1 = MENTION_FEATURES.is_animated(subsequent_mention)
        for antecedent_mention in mentions:
            if antecedent_mention >= subsequent_mention:
                break
            a2 = MENTION_FEATURES.is_animated(antecedent_mention)
            if a1 != UNKNOWN_VALUE_TAG and a2 != UNKNOWN_VALUE_TAG and not (a1 and a2):
                negative_edges.add((antecedent_mention.extent,subsequent_mention.extent))

## CL embedding
def get_embedding_constraints(document, positive_edges, negative_edges, entity_head_extents=None):#mentions, 
    """ Defines Cannot-Link constraints based on the knowledge that some mentions are embedded in 
    one another
    
    For each mention pair for which this function has defined a constraint, the corresponding input 
    set of mention pairs will be updated.
    
    Args:
        document: a Document instance for which mentions are defined
        positive_edges: set of pairs of mention extents. Not used, exists for API compatibility 
            purposes.
        negative_edges: set of pairs of mention extents, will be potentially updated
        entity_head_extents: set of mention extents, or None. Not used, exists for API compatibility 
            purposes.
    """
    mentions = document.mentions
    for subsequent_mention in mentions:
        if not MENTION_FEATURES.is_expanded_pronoun(subsequent_mention):
            for antecedent_mention in mentions:
                if antecedent_mention >= subsequent_mention:
                    break
                if antecedent_mention.includes(subsequent_mention) or subsequent_mention.includes(antecedent_mention):
                    negative_edges.add((antecedent_mention.extent,subsequent_mention.extent))

        # CL NE type constraints
        # FIXME: why is the code commented out, and without a condition 'if'? Shall it be reimplemented?
#        for mention1 in mentions:
#            t1 = mention1.get_ne_type()
#            for mention2 in mentions:
#                t2 = mention2.get_ne_type()
#                # NE type constraints
#                if t1 != None and t2 != None and t2 != t1:
#                    if not (t1 in ["PERSON", "NORP"] and t2 in ["PERSON", "NORP"]):
#                        negative_edges.append((mention1, mention2))
                # Gender/Number constraints
#                if not (mention1.gender_agr(mention2) and mention1.number_agr(mention2)):
#                    negative_edges.append((mention1, mention2))
        # CL pronouns
        # FIXME: why is the code commented out, and without a condition 'if'? Shall it be reimplemented?
#        for m2 in mentions:
#            t2 = m2.get_ne_type()
#            if m2.is_expanded_pronoun() and t2 == "PERSON":
#                for m1 in mentions:
#                    if m1 >= m2:
#                        break
#                    t1 = m1.get_ne_type()
#                    if not t1 in ["PERSON", "NORP"]:
#                        negative_edges.add((m1, m2))
#                    if not m1.morph_agr(m2):
#                        negative_edges.add((m1, m2))

CONSTRAINT_NAME2CONSTRAINT_FCT = {"anaphoricity": get_anaphoricity_constraints,
                                  "proper_match": get_proper_match_constraints,
                                  "animated": get_animated_constraints,
                                  "embedding": get_embedding_constraints}
CONSTRAINT_NAME2CONSTRAINT_FCT.update(("rules_{}".format(s), f) for s,f in RULES_TYPE2EDGES_CONSTRAINTS_FCT.items())
def _create_get_constraints_fct(constraint_name, args, kwargs):
    """ Creates, from choosing one of the existing constraints addition functions, a function whose 
    role is to carry out the addition of constraints, that can be defined on a document, to already 
    existing set(s) of constraints, that are already defined.
    
    Which constraints are dealt with depends on the input 'constraint_name' parameter value.
    Moreover, it is possible to specify supplementary args and kwargs parameters to pass to the 
    underlying function being used.
    
    Args:
        constraint_name: string, name of the constraint to use to build the function, must be among 
            the following values: {'anaphoricity', 'proper_match', 'animated', 'embedding'}
        args: tuple of values to pass as supplementary 'args' when calling the underlying 
            constraints addition function
        kwargs: dict of values to pass as supplementary 'kwargs' when calling the underlying 
            constraints function

    Returns:
        callable
    
    Raises:
        ValueError: if the input 'constraint_name' value is incorrect
    """
    try:
        fct = CONSTRAINT_NAME2CONSTRAINT_FCT[constraint_name]
    except KeyError:
        msg = "Incorrect constraint name '{}', possible names are: {}."
        msg = msg.format(constraint_name, tuple(CONSTRAINT_NAME2CONSTRAINT_FCT.keys()))
        raise ValueError(msg)
    return lambda document, positive_edges, negative_edges, entity_head_extents=None: fct(document, positive_edges, negative_edges, *args, entity_head_extents=entity_head_extents, **kwargs)



constraints_definition_attribute_data = ("constraints_definition",
                                         (lambda configuration: tuple((name, tuple(args), dict((k,kwargs[k]) for k in kwargs)) for name, args, kwargs in configuration["constraints_definition"]),
                                          _create_simple_set_parameter_value_in_configuration("constraints_definition"))
                                         )
class ConstrainedEdgesDefinerMixIn(ConfigurationMixIn, EqualityMixIn):
    """ Class used to implement for a resolver class an interface allowing to define constraints 
    (Must-Link or Cannot-Link) for pairs of mentions
    
    For now, the different types of constraint are the following:
    
    * "anaphoricity": defines Cannot-Links based on an external, previous knowledge, that some 
      mentions are anaphora and some are not
    * "proper_match': defines Must-Links based on the fact that some mentions' raw text value may be 
      equal
    * "animated": defines Cannot-Links based on the fact that some mentions refer to an animated 
      entity, and some refer to an inanimated entity
    * "embedding": defines Cannot-Links based on the knowledge that some mentions are embedding in 
      one another
    
    If the CoRTeX' working language implement sets of rules, they may come with functions that can 
    specify constraints (cf 'cortex.languages.'language'.rules), in which case the following types 
    of constraints are also available:
        - "rules_'constraint name'", where 'constraint name' is one possible name of constraints function 
          implemented by the language (cf 'cortex.languages.'language'.rules.RULES_TYPE2EDGES_CONSTRAINTS_FCT) 
    
    An initialization parameter of this class is a collection of such constraint types (to know 
    which constraint function(s) to use) and arguments to be passed to the corresponding constraint 
    function.
    
    Arguments:
        constraints_definition:
            collection of (constraint_type, args, kwargs) triplets, where 
            * 'constraint type' must be one of the possible constraint types (cf this class' docstring)
            * 'args' and 'kwargs' will be passed along to the constraints function; for now, none of them 
              requires supplementary parameters, so args and kwargs must remain empty for them
        
        configuration:
            a Mapping instance; or None; info about the class instance that the user wants to keep 
            around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME:
            the name of the class, which shall be used to unequivocally identify it among its peers
    
        constraints_definition:
            collection of (constraint_type, args, kwargs) triplets, where 
            * 'constraint type' must be one of the possible constraint types (cf this class' docstring)
            * 'args' and 'kwargs' will be passed along to the constraints function; for now, none of them 
              requires supplementary parameters, so args and kwargs must remain empty for them
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    
    
    An instance of this class can be used the following way:
    
    Examples:
        >>> constraints_definition = [("animated", [], {}),
        ...                           ("proper_match", [], {}),
        ...                          ]
        >>> constrained_edges_definer = ConstrainedEdgesDefinerMixIn(constraints_definition)
        >>> positive_edges, negative_edges = constrained_edges_definer.get_constraints(document)
    """
    
    __INIT_PARAMETERS = OrderedDict((constraints_definition_attribute_data,))
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple(__INIT_PARAMETERS.keys()) +\
                                                EqualityMixIn._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = tuple(__INIT_PARAMETERS.keys())
    
    def __init__(self, constraints_definition, configuration=None):
        # Check input values
        _constraints = set()
        constraint_fcts = []
        constraint_name2args_kwargs = defaultdict(lambda: ([], {}))
        for curr_constraint_name, curr_args, curr_kwargs in constraints_definition:
            args, kwargs = constraint_name2args_kwargs[curr_constraint_name]
            args.extend(curr_args)
            kwargs.update(curr_kwargs)
        for constraint_name, (args, kwargs) in sorted(constraint_name2args_kwargs.items()):
            constraint_fct = _create_get_constraints_fct(constraint_name, args, kwargs)
            constraint_fcts.append(constraint_fct)
        # Initialize using parent class
        ConfigurationMixIn.__init__(self, configuration=configuration)
        # Assign attribute values
        self.constraints_definition = constraints_definition
        self._constraint_fcts = constraint_fcts
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        constraints_definition, = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        args = args + (constraints_definition,)
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, constraints_definition=None):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Cf the documentation of the class of know which argument(s) to use.
        
        Returns:
            a Mapping instance
        """
        # Check and process inputs
        if constraints_definition is None:
            constraints_definition = [("anaphoricity", tuple(), {}),
                                      ("proper_match", tuple(), {}),
                                      ("animated", tuple(), {}),
                                      ("embedding", tuple(), {}),
                                      ]
        configuration = super().define_configuration()
        configuration["constraints_definition"] = constraints_definition
        return configuration
    
    # Instance methods
    def get_constraints(self, document):#mentions, entity_head_extents=None
        """ Defines the sets of Must-Link and Cannot-Link constraints regarding the mention pairs 
        built from the mentions of a document.
        
        Args:
            document: a Document instance

        Returns:
            a (positive_edges, negative_edges) pair, where:
            - 'positive_edges' is a set of pairs of mention extents, representing the Must-Links 
              associated to the document's mentions
            - 'negative_edges' is a set of pairs of mention extents, representing the Cannot-Links 
              associated to the document's mentions
        """
        positive_edges = set()
        negative_edges = set()
        for constraint_fct in self._constraint_fcts:
            constraint_fct(document, positive_edges, negative_edges, entity_head_extents=None)#mentions, # FIXME: change that the day the AnaphorictyResolver will be implemented back
        positive_edges = tuple(positive_edges)
        negative_edges = tuple(negative_edges)                  
        return positive_edges, negative_edges
