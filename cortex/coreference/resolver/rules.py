# -*- coding: utf-8 -*-


"""
Defines resolver classes based on the usage of a set of deterministic rules.

Those possible set of rules must be provided by the appropriate language package.
Such a set of rules may not exist for the working language.
"""

__all__ = ["RulesResolver",
           ]

from collections import OrderedDict

from .base import _CoreferenceResolver
from cortex.tools import (_create_simple_get_parameter_value_from_configuration, 
                            _create_simple_set_parameter_value_in_configuration)

from cortex.config import language
from cortex.languages import create_get_language_dependent_object_fct
get_language_dependent_object = create_get_language_dependent_object_fct(language)

RULES_TYPE2COREF_RESOLVE_FCT = get_language_dependent_object("rules2coref_resolve_fct")

__all__ = ("RulesResolver", "RULES_TYPE2COREF_RESOLVE_FCT", )


def _get_rules_resolve_args_from_configuration(configuration):
    rules_resolve_args = type(configuration).get(configuration, "rules_resolve_args", tuple())
    rules_resolve_args = tuple(rules_resolve_args) if rules_resolve_args is not None else tuple()
    return rules_resolve_args
def _get_rules_resolve_kwargs_from_configuration(configuration):
    rules_resolve_kwargs = type(configuration).get(configuration, "rules_resolve_kwargs", dict())
    rules_resolve_kwargs = dict((key, rules_resolve_kwargs[key]) for key in iter(rules_resolve_kwargs)) if rules_resolve_kwargs is not None else dict()
    return rules_resolve_kwargs



rules_type_attribute_data = ("rules_type", 
                             (_create_simple_get_parameter_value_from_configuration("rules_type"),
                              _create_simple_set_parameter_value_in_configuration("rules_type"))
                             )
rules_resolve_args_attribute_data = ("rules_resolve_args",
                                     (_create_simple_get_parameter_value_from_configuration("rules_resolve_args", 
                                                                                            default=tuple, 
                                                                                            postprocess_fct=lambda args_collection: tuple(args_collection) if args_collection is not None else tuple()),
                                      _create_simple_set_parameter_value_in_configuration("rules_resolve_args"))
                                     )
rules_resolve_kwargs_attribute_data = ("rules_resolve_kwargs",
                                     (_create_simple_get_parameter_value_from_configuration("rules_resolve_kwargs", 
                                                                                            default=dict, 
                                                                                            postprocess_fct=lambda name2arg_mapping: dict((key, name2arg_mapping[key]) for key in iter(name2arg_mapping)) if name2arg_mapping is not None else dict()),
                                      _create_simple_set_parameter_value_in_configuration("rules_resolve_kwargs"))
                                     )
class RulesResolver(_CoreferenceResolver):
    """ Rule based resolver.
    
    Represents resolvers whose 'resolve' method is based on the use of a deterministic, hand-made set 
    of rules.
    
    For instance, the 'sieves' set of rules, for the English language.
    
    Such a set of rules may not exist for all language; if an instance of this resolver Class is 
    implemented while CoRTeX' working language does not possess such rules set(s), an Exception will 
    be raised.
    
    Arguments:
        rules_type: string, name of the rule sets to use, must be defined in the possible sets of 
            rules available for CoRTeX' current working language; will be used to fetch the corresponding 
            'resolve' function defined for this rules set (Cf 'cortex.languages."current language".rules)
        
        rules_resolve_args: list or tuple, the args to be passed to the 'resolve' function fetched 
            using the 'rules_type' parameter value; or None
        
        rules_resolve_kwargs: dict, the kwargs to be passed to the 'resolve' function fetched using 
            the 'rules_type' parameter value; or None
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    __INIT_PARAMETERS = OrderedDict((rules_type_attribute_data, rules_resolve_args_attribute_data, 
                                     rules_resolve_kwargs_attribute_data))
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple(__INIT_PARAMETERS.keys()) +\
                                                _CoreferenceResolver._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = _CoreferenceResolver._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION+\
                                            tuple(__INIT_PARAMETERS.keys())
    
    def __init__(self, rules_type, rules_resolve_args=None, rules_resolve_kwargs=None, configuration=None):
        # Check input values
        if rules_type not in RULES_TYPE2COREF_RESOLVE_FCT:
            msg = "Incorrect coreference resolve rules type name '{}', possibles names are: {}."
            msg = msg.format(rules_type, tuple(sorted(RULES_TYPE2COREF_RESOLVE_FCT.keys())))
            raise ValueError(msg)
        # Init parent
        super().__init__(configuration=configuration)
        # Process input values
        if rules_resolve_args is None:
            rules_resolve_args = tuple()
        if rules_resolve_kwargs is None:
            rules_resolve_kwargs = dict()
        self.rules_type = rules_type
        self.rules_resolve_args = rules_resolve_args
        self.rules_resolve_kwargs = rules_resolve_kwargs
        self._resolve_fct = RULES_TYPE2COREF_RESOLVE_FCT[self.rules_type]
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        rules_type, rules_resolve_args, rules_resolve_kwargs = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        args = args + (rules_type,)
        kwargs["rules_resolve_args"] = rules_resolve_args
        kwargs["rules_resolve_kwargs"] = rules_resolve_kwargs
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, rules_type, rules_resolve_args=None, rules_resolve_kwargs=None):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Cf the documentation of the class of know which argument(s) to use.
        
        Returns:
            a Mapping instance
        """
        # Check and process inputs
        if rules_resolve_args is None:
            rules_resolve_args = tuple()
        if rules_resolve_kwargs is None:
            rules_resolve_kwargs = {}
        configuration = super().define_configuration()
        configuration["rules_type"] = rules_type
        configuration["rules_resolve_args"] = rules_resolve_args
        configuration["rules_resolve_kwargs"] = rules_resolve_kwargs
        return configuration
    
    # Instance methods
    def resolve(self, document):
        """ Predicts a coreference partition for the input document.
        
        Args:
            document: a Document instance

        Returns:
            a CoreferencePartition instance
        """
        return self._resolve_fct(document, *self.rules_resolve_args, **self.rules_resolve_kwargs)
