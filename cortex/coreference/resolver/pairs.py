# -*- coding: utf-8 -*-

"""
Defines resolver classes that work by treating the coreference partition prediction problem as a 
binary classification problem on pairs of mentions
"""

__all__ = ["MentionPairSampleResolver", 
           "SoonResolver", 
           "McCarthyLehnertResolver", 
           "NgCardieResolver", 
           "HACResolver", 
           "OraclePairResolver", 
           "OraclePairScoreResolver",
           "_BaseMentionPairSampleTrainableResolver",
           ]

from collections import OrderedDict
import itertools

from cortex.learning.parameters import POS_CLASS
from cortex.learning.filter.pairs import AcceptAllPairFilter
from cortex.learning.ml_model.classifier import (online_classifiers_map, sklearn_classifiers_map, 
                                                 LogisticRegressionSKLEARNBinaryClassifier,
                                                 )
from cortex.learning.sample_features_vectorizer.pairs import MentionPairSampleFeaturesVectorizer
from cortex.learning.model_interface import OnlineInterface,  SKLEARNInterface
from ..generator.pairs import (_BaseMentionPairGenerator, MentionPairSampleGenerator, 
                                            SoonGenerator, NgCardieGenerator)
from ..decoder.pairs import (_BaseMentionPairCoreferenceDecoder, ClosestFirstCoreferenceDecoder, AggressiveMergeCoreferenceDecoder, 
                                          OracleAggressiveMergeCoreferenceDecoder, BestFirstCoreferenceDecoder, HACCoreferenceDecoder)
from .base import _CoreferenceResolver, _TrainableResolver, generator_attribute_data
from cortex.api.markable import get_mention_pair_extent_pair
from cortex.api.coreference_partition import CoreferencePartition
from cortex.utils import _check_isinstance



class _BaseMentionPairSampleTrainableResolver(_TrainableResolver):
    """ Base class for learning-based resolver using exclusively machine-learning samples based on 
    pairs of true mentions ("pairwise"). 
    
    This class represents trainable resolver that consider the coreference prediction problem as a 
    binary classification problem on pairs of mentions of the given document.
    
    Can be instantiated using only '_BaseMentionPairGenerator' subclasses instances and 
    '_BaseMentionPairCoreferenceDecoder' classes instances.
    
    Arguments:
        model_interface: a _ModelInterface child class instance, representing the interface to the 
            underlying machine learning model to train and to use
        
        generator: a _BaseMentionPairGenerator child class instance, used to generate, from document(s), 
            machine-learning samples used as input by the underlying machine learning model
        
        decoder: a _CoreferenceDecoder child class instance, used to interpret scores associated to 
            machine-learning samples into a coreference partition, for the document used to generated 
            the scored machine-learning samples
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        sample_type: string specifying the type of the sample used in the frame of the underlying 
            machine-learning task
        
        document_hash2cache: document_hash2cache: a "document hash => cache" map, or None; the 
            current value of the underlying model interface's vectorization cache (cf 
            'cortex.coreference.api.vectorization_cache.py')
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _check_subcomponent(self, model_interface, generator, decoder):
        """ Checks that the input model interface, generator, and decoder are correct; that is, 
        that are suitable to be used with MentionPairSample instances.
        
        Raises:
            ValueError: if one input is not of the expected type
        """
        _check_isinstance(_BaseMentionPairGenerator, generator, "generator")
        _check_isinstance(_BaseMentionPairCoreferenceDecoder, decoder, "decoder")
        super()._check_subcomponent(model_interface, generator, decoder)
    
    # Instance methods
    def train(self, documents): # , use_entity_heads=False, use_singletons=False
        """ Trains the resolver, using the input collection fo documents.
        
        Args:
            documents: a Document instances iterable
        
        Warning:
            If the underlying  model interface has already learnt a model, it will be reset and trained 
            anew (unless the model interface possess the ability to continue the training of its model 
            with new data, and if this is its default behavior when calling its 'learn' method).
        """
        self._logger.info("Building the training data set...")
        ML_samples = tuple(self._generator.generate_training_set(documents))
        self._logger.info("Finished building the training data set ({} samples generated).".format(len(ML_samples)))
        self._logger.info("Training the model...")
        self._model_interface.learn(ML_samples)
        self._logger.info("Finished training the model.")
    
    def resolve(self, document): #, entity_heads=None, singletons=None
        """ Predicts a coreference partition for the input document.
        
        Args:
            document: a Document instance

        Returns:
            a CoreferencePartition instance
        """
        # Check input
        self._preprocessing_resolution(document)
        
        # Get ML samples generator
        self._logger.debug("Building the prediction data set...")
        ML_samples_iterable = self._generator.generate_prediction_set(document) # , entity_head_extents=entity_head_extents,  singleton_extents=singleton_extents
        
        # Predict coreference scores
        self._logger.debug("Predicting the coreference scores...")
        coreference_scores = self._predict_coreference_scores(ML_samples_iterable)
        
        # Decode coreference scores
        self._logger.debug("Decoding the coreference scores...")
        scores_are_proba = self._model_interface.SCORES_ARE_PROBA
        predicted_coreference_partition = self._decoder.decode(document, coreference_scores, 
                                                              scores_are_proba=scores_are_proba)
        
        return predicted_coreference_partition
    
    def _predict_coreference_scores(self, pair_classification_samples_iterable):
        """ Predicts scores associated to pairs of mention extents, using the underlying ML model 
        interface.
        
        Assume that the model interface carries out a classification machine learning task, and that, 
        for a given machine-learning sample, it associates a positive valued score, representing a 
        confidence value, to each possible classification label. 
        
        Args:
            pair_classification_samples_iterable: an iterable over mention pair classification 
                sample instances for which decoding should occur

        Returns:
            an "(extent1, extent2) => score value" map
        """
        # Compute pairwise coreference_scores
        pair_classification_samples = tuple(pair_classification_samples_iterable)
        predictions = self._model_interface.predict(pair_classification_samples)
        coreference_scores = {get_mention_pair_extent_pair(*pair_classification_sample.pair): prediction.get_label_score(POS_CLASS)\
                              for pair_classification_sample, prediction in zip(pair_classification_samples, predictions)}
        return coreference_scores




class SoonResolver(_BaseMentionPairSampleTrainableResolver):
    """ Implementation of Soon et al. 2001 resolver system.
     
    This system uses a 2-step approach:
    (i) pairwise classification of mention pairs
    (ii) greedy single-link clustering.

    The generation process of ML dataset uses resampling: only the closest antecedent (in the 
    reading order of the document) yields a sample associated to the positive class, and only the 
    mention intervening between the closest antecedent and the anaphora.

    Decoding uses closest-first clustering: only the closest mention for which the classifier 
    return 1 yields a coreference link. Transitive closure is performed over the coreference links.
    
    As such, can be instantiated only with a SoonGenerator instance and a 
    ClosestFirstCoreferenceDecoder instance.
    
    Arguments:
        model_interface: a _ModelInterface child class instance, representing the interface to the 
            underlying machine learning model to train and to use
        
        generator: a SoonGenerator instance, used to generate, from document(s), 
            machine-learning samples used as input by the underlying machine learning model
        
        decoder: a ClosestFirstCoreferenceDecoder instance, used to interpret scores associated to 
            machine-learning samples into a coreference partition, for the document used to generated 
            the scored machine-learning samples
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        sample_type: string specifying the type of the sample used in the frame of the underlying 
            machine-learning task
        
        document_hash2cache: document_hash2cache: a "document hash => cache" map, or None; the 
            current value of the underlying model interface's vectorization cache (cf 
            'cortex.coreference.api.vectorization_cache.py')
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _check_subcomponent(self, model_interface, generator, decoder):
        """ Check that the input model interface, generator, and decoder are correct; that is: 
        - they are suitable to be used with MentionPairSample instances
        - the generator is a SoonGenerator instance
        - the decoder is a ClosestFirstCoreferenceDecoder instance
        
        Raises:
        ValueError: if one input is not of the expected type
        """
        _check_isinstance(SoonGenerator, generator, "generator")
        _check_isinstance(ClosestFirstCoreferenceDecoder, decoder, "decoder")
        super()._check_subcomponent(model_interface, generator, decoder)
    
    @classmethod
    def define_configuration(cls, sample_features_vectorizer_factory_configuration=None, 
                              classifier_factory_configuration=None, 
                              generator_pair_filter_factory_configuration=None):
        """ Create a configuration object, which can then be used to create a specific instance of 
        this class.
        
        Args:
            sample_features_vectorizer_factory_configuration: a factory configuration that can be 
                used to create a specific _BaseMentionPairSampleFeaturesVectorizer child class instance; or 
                None, in which case the factory configuration for a 'MentionPairSampleFeaturesVectorizer' 
                instance (parametrized with 'quantize=True') will be used
            classifier_factory_configuration: a factory configuration that can be used to 
                create a specific _BinaryClassifier child class instance; or None, in which case the factory 
                configuration for a 'LogisticRegressionSKLEARNBinaryClassifier' instance (parametrized with 
                'class_weight="balanced"') will be used
            generator_pair_filter_factory_configuration: a factory configuration that can be used 
                to create a specific PairFilter instance, that shall be used to generate the factory 
                configuration of an instance of the Generator class compatible with this resolver class; or 
                None, in which case the factory configuration for an 'AcceptAllPairFilter' instance will be 
                used

        Returns:
            a Mapping instance
        """
        # Check and process inputs
        if generator_pair_filter_factory_configuration is None:
            generator_pair_filter_factory_configuration = AcceptAllPairFilter.define_factory_configuration()
        if sample_features_vectorizer_factory_configuration is None:
            sample_features_vectorizer_factory_configuration =\
             MentionPairSampleFeaturesVectorizer.define_factory_configuration(quantize=True)
        if classifier_factory_configuration is None:
            classifier_factory_configuration =\
             LogisticRegressionSKLEARNBinaryClassifier.define_factory_configuration("TOKEN_POS_CLASS", "TOKEN_NEG_CLASS", 
                                                                                    class_weight="balanced")
        
        model_interface_class = _get_appropriate_model_interface_class_for_input_classifier_factory_configuration(classifier_factory_configuration)
        
        generator_factory_configuration =\
         SoonGenerator.define_factory_configuration(pair_filter_factory_configuration=generator_pair_filter_factory_configuration)
        decoder_factory_configuration = ClosestFirstCoreferenceDecoder.define_factory_configuration()
        model_interface_factory_configuration =\
         model_interface_class.define_factory_configuration(sample_features_vectorizer_factory_configuration, 
                                                            classifier_factory_configuration)
        return super().define_configuration(model_interface_factory_configuration, generator_factory_configuration, decoder_factory_configuration)
        


class McCarthyLehnertResolver(_BaseMentionPairSampleTrainableResolver):
    """ Resolver inspired by McCarthy & Lehnert (1995) resolver system.
    
    This system uses a 2-step approach:
    (i) paiwise classification of mention pairs
    (ii) greedy aggressive clustering.

    The generation process of ML dataset interpret all coreference links as positive class labelled 
    samples, and all the other links as negative class labelled samples.

    Decoding uses aggressive clustering: all samples that are labelled with the positive class are 
    clustered together.
    
    As such, can be instantiated only with a MentionPairSampleGenerator instance and a 
    AggressiveMergeCoreferenceDecoder instance.
    
    Arguments:
        model_interface: a _ModelInterface child class instance, representing the interface to the 
            underlying machine learning model to train and to use
        
        generator: a MentionPairSampleGenerator instance, used to generate, from document(s), 
            machine-learning samples used as input by the underlying machine learning model
        
        decoder: a AggressiveMergeCoreferenceDecoder instance, used to interpret scores associated 
            to machine-learning samples into a coreference partition, for the document used to generated 
            the scored machine-learning samples
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        sample_type: string specifying the type of the sample used in the frame of the underlying 
            machine-learning task
        
        document_hash2cache: document_hash2cache: a "document hash => cache" map, or None; the 
            current value of the underlying model interface's vectorization cache (cf 
            'cortex.coreference.api.vectorization_cache.py')
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _check_subcomponent(self, model_interface, generator, decoder):
        """ Checks that the input model interface, generator, and decoder are correct; that is: 
        - they are suitable to be used with MentionPairSample instances
        - the generator is a MentionPairSampleGenerator instance
        - the decoder is a AggressiveMergeCoreferenceDecoder instance
        
        Raises:
            ValueError: if one input is not of the expected type
        """
        _check_isinstance(MentionPairSampleGenerator, generator, "generator")
        _check_isinstance(AggressiveMergeCoreferenceDecoder, decoder, "decoder")
        super()._check_subcomponent(model_interface, generator, decoder)
    
    @classmethod
    def define_configuration(cls, sample_features_vectorizer_factory_configuration=None, 
                              classifier_factory_configuration=None, 
                              generator_pair_filter_factory_configuration=None):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class.
        
        Args:
            sample_features_vectorizer_factory_configuration: a factory configuration that can be 
                used to create a specific _BaseMentionPairSampleFeaturesVectorizer child class instance; or 
                None, in which case the factory configuration for a 'MentionPairSampleFeaturesVectorizer' 
                instance (parametrized with 'quantize=True') will be used
            classifier_factory_configuration: a factory configuration that can be used to 
                create a specific _BinaryClassifier child class instance; or None, in which case the factory 
                configuration for a 'LogisticRegressionSKLEARNBinaryClassifier' instance (parametrized with 
                'class_weight="balanced"') will be used
            generator_pair_filter_factory_configuration: a factory configuration that can be used 
                to create a specific PairFilter instance, that shall be used to generate the factory 
                configuration of an instance of the Generator class compatible with this resolver class; or 
                None, in which case the factory configuration for an 'AcceptAllPairFilter' instance will be used

        Returns:
            a Mapping instance
        """
        # Check and process inputs
        if generator_pair_filter_factory_configuration is None:
            generator_pair_filter_factory_configuration = AcceptAllPairFilter.define_factory_configuration()
        if sample_features_vectorizer_factory_configuration is None:
            sample_features_vectorizer_factory_configuration =\
             MentionPairSampleFeaturesVectorizer.define_factory_configuration(quantize=True)
        if classifier_factory_configuration is None:
            classifier_factory_configuration =\
             LogisticRegressionSKLEARNBinaryClassifier.define_factory_configuration("TOKEN_POS_CLASS", "TOKEN_NEG_CLASS", 
                                                                                    class_weight="balanced")
        
        model_interface_class = _get_appropriate_model_interface_class_for_input_classifier_factory_configuration(classifier_factory_configuration)
        
        generator_factory_configuration =\
         MentionPairSampleGenerator.define_factory_configuration(pair_filter_factory_configuration=generator_pair_filter_factory_configuration)
        decoder_factory_configuration = AggressiveMergeCoreferenceDecoder.define_factory_configuration()
        model_interface_factory_configuration =\
         model_interface_class.define_factory_configuration(sample_features_vectorizer_factory_configuration, 
                                                            classifier_factory_configuration)
        return super().define_configuration(model_interface_factory_configuration, 
                                               generator_factory_configuration, 
                                               decoder_factory_configuration)



class NgCardieResolver(_BaseMentionPairSampleTrainableResolver):
    """ Implementation of Ng & Cardie 2002 resolver system.
    
    This system also uses Soon et al. 2-step approach, with some refinements:
    (i) best-first link selection
    (ii) non-pronominal anaphora have non-pronominal antecedents.
    
    Can be instantiated only with a NgCardieGenerator instance and a 
    BestFirstCoreferenceDecoder instance.
    
    Arguments:
        model_interface: a _ModelInterface child class instance, representing the interface to the 
            underlying machine learning model to train and to use
        
        generator: a NgCardieGenerator instance, used to generate, from document(s), 
            machine-learning samples used as input by the underlying machine learning model
        
        decoder: a BestFirstCoreferenceDecoder instance, used to interpret scores associated 
            to machine-learning samples into a coreference partition, for the document used to generated 
            the scored machine-learning samples
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        sample_type: string specifying the type of the sample used in the frame of the underlying 
            machine-learning task
        
        document_hash2cache: document_hash2cache: a "document hash => cache" map, or None; the 
            current value of the underlying model interface's vectorization cache (cf 
            'cortex.coreference.api.vectorization_cache.py')
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _check_subcomponent(self, model_interface, generator, decoder):
        """ Checks that the input model interface, generator, and decoder are correct; that is: 
        - they are suitable to be used with MentionPairSample instances
        - the generator is a NgCardieGenerator instance
        - the decoder is a BestFirstCoreferenceDecoder instance
        
        Raises:
            ValueError: if one input is not of the expected type
        """
        _check_isinstance(NgCardieGenerator, generator, "generator")
        _check_isinstance(BestFirstCoreferenceDecoder, decoder, "decoder")
        super()._check_subcomponent(model_interface, generator, decoder)
    
    @classmethod
    def define_configuration(cls, sample_features_vectorizer_factory_configuration=None, 
                              classifier_factory_configuration=None, 
                              generator_pair_filter_factory_configuration=None):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class.
        
        Args:
            sample_features_vectorizer_factory_configuration: a factory configuration that can be 
                used to create a specific _BaseMentionPairSampleFeaturesVectorizer child class instance; or 
                None, in which case the factory configuration for a 'MentionPairSampleFeaturesVectorizer' 
                instance (parametrized with 'quantize=True') will be used
            classifier_factory_configuration: a factory configuration that can be used to 
                create a specific _BinaryClassifier child class instance; or None, in which case the factory 
                configuration for a 'LogisticRegressionSKLEARNBinaryClassifier' instance (parametrized with 
                'class_weight="balanced"') will be used
            generator_pair_filter_factory_configuration: a factory configuration that can be used 
                to create a specific PairFilter instance, that shall be used to generate the factory 
                configuration of an instance of the Generator class compatible with this resolver class; or 
                None, in which case the factory configuration for an 'AcceptAllPairFilter' instance will be used

        Returns:
            a Mapping instance
        """
        # Check and process inputs
        if generator_pair_filter_factory_configuration is None:
            generator_pair_filter_factory_configuration = AcceptAllPairFilter.define_factory_configuration()
        if sample_features_vectorizer_factory_configuration is None:
            sample_features_vectorizer_factory_configuration =\
             MentionPairSampleFeaturesVectorizer.define_factory_configuration(quantize=True)
        if classifier_factory_configuration is None:
            classifier_factory_configuration =\
            LogisticRegressionSKLEARNBinaryClassifier.define_factory_configuration("TOKEN_POS_CLASS", "TOKEN_NEG_CLASS", 
                                                                                  class_weight="balanced")
        
        model_interface_class = _get_appropriate_model_interface_class_for_input_classifier_factory_configuration(classifier_factory_configuration)
        
        generator_factory_configuration =\
         NgCardieGenerator.define_factory_configuration(pair_filter_factory_configuration=generator_pair_filter_factory_configuration)
        decoder_factory_configuration = BestFirstCoreferenceDecoder.define_factory_configuration()
        model_interface_factory_configuration =\
         model_interface_class.define_factory_configuration(sample_features_vectorizer_factory_configuration, 
                                                            classifier_factory_configuration)
        return super().define_configuration(model_interface_factory_configuration, 
                                               generator_factory_configuration, 
                                               decoder_factory_configuration)



class HACResolver(_BaseMentionPairSampleTrainableResolver):
    """ Hierarchical Agglomerative Clustering resolver.
    
    Can be instantiated only with a MentionPairSampleGenerator instance and a HACCoreferenceDecoder 
    instance.
    
    Arguments:
        model_interface: a _ModelInterface child class instance, representing the interface to the 
            underlying machine learning model to train and to use
        
        generator: a MentionPairSampleGenerator instance, used to generate, from document(s), 
            machine-learning samples used as input by the underlying machine learning model
        
        decoder: a HACCoreferenceDecoder instance, used to interpret scores associated 
            to machine-learning samples into a coreference partition, for the document used to generated 
            the scored machine-learning samples
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        sample_type: string specifying the type of the sample used in the frame of the underlying 
            machine-learning task
        
        document_hash2cache: document_hash2cache: a "document hash => cache" map, or None; the 
            current value of the underlying model interface's vectorization cache (cf 
            'cortex.coreference.api.vectorization_cache.py')
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _check_subcomponent(self, model_interface, generator, decoder):
        """ Checks that the input model interface, generator, and decoder are correct; that is: 
        - they are suitable to be used with MentionPairSample instances
        - the generator is a MentionPairSampleGenerator instance
        - the decoder is a HACCoreferenceDecoder instance
        
        Raises:
            ValueError: if one input is not of the expected type
        """
        _check_isinstance(MentionPairSampleGenerator, generator, "generator")
        _check_isinstance(HACCoreferenceDecoder, decoder, "decoder")
        super()._check_subcomponent(model_interface, generator, decoder)
    
    @classmethod
    def define_configuration(cls, sample_features_vectorizer_factory_configuration=None, 
                              classifier_factory_configuration=None, 
                              generator_pair_filter_factory_configuration=None, 
                              clustering_type="group_average"):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class.
        
        Args:
            sample_features_vectorizer_factory_configuration: a factory configuration that can be 
                used to create a specific _BaseMentionPairSampleFeaturesVectorizer child class instance; or 
                None, in which case the factory configuration for a 'MentionPairSampleFeaturesVectorizer' 
                instance (parametrized with 'quantize=True') will be used
            classifier_factory_configuration: a factory configuration that can be used to 
                create a specific _BinaryClassifier child class instance; or None, in which case the factory 
                configuration for a 'LogisticRegressionSKLEARNBinaryClassifier' instance (parametrized with 
                'class_weight="balanced"') will be used
            generator_pair_filter_factory_configuration: a factory configuration that can be used 
                to create a specific PairFilter instance, that shall be used to generate the factory 
                configuration of an instance of the Generator class compatible with this resolver class; or
         None, in which case the factory configuration for an 'AcceptAllPairFilter' instance will be used
            clustering_type: string, parameter that shall be used to specify the factory 
                configuration of an instance of the Decoder class compatible with this resolver class

        Returns:
            a Mapping instance
        """
        # Check and process inputs
        if generator_pair_filter_factory_configuration is None:
            generator_pair_filter_factory_configuration = AcceptAllPairFilter.define_factory_configuration()
        if sample_features_vectorizer_factory_configuration is None:
            sample_features_vectorizer_factory_configuration =\
             MentionPairSampleFeaturesVectorizer.define_factory_configuration(quantize=True)
        if classifier_factory_configuration is None:
            classifier_factory_configuration =\
             LogisticRegressionSKLEARNBinaryClassifier.define_factory_configuration("TOKEN_POS_CLASS", "TOKEN_NEG_CLASS", 
                                                                                    class_weight="balanced")
        
        model_interface_class = _get_appropriate_model_interface_class_for_input_classifier_factory_configuration(classifier_factory_configuration)
        
        generator_factory_configuration =\
         MentionPairSampleGenerator.define_factory_configuration(pair_filter_factory_configuration=generator_pair_filter_factory_configuration)
        decoder_factory_configuration = HACCoreferenceDecoder.define_factory_configuration(clustering_type=clustering_type)
        model_interface_factory_configuration =\
         model_interface_class.define_factory_configuration(sample_features_vectorizer_factory_configuration, 
                                                            classifier_factory_configuration)
        return super().define_configuration(model_interface_factory_configuration, generator_factory_configuration, decoder_factory_configuration)



#################################### Oracles ########################################
class OraclePairResolver(_CoreferenceResolver):
    """ Resolver that predicts a coreference partition for a document from ground truth data, held 
    by a ground truth version of the document, using the following algorithm:
    
    For all the pairs of mentions, generated for the document for which to make a prediction, if 
    both mention of the pair are also found in the ground truth document, make them coreferent in the 
    predicted coreference partition if they are coreferent according to the coreference partition 
    of the ground truth document.
    
    As such, ground truth data is needed, and the 'resolve' method also expects a second input, 
    corresponding to a ground truth version of the document for which to predict a coreference 
    partition already, and so which possesses a coreference partition.
    
    The resolver works by considering pairs of mention, and uses a MentionPairSampleGenerator instance 
    to generate them, trough the generation of the corresponding ML samples.
    
    Arguments:
        generator: a MentionPairSampleGenerator instance, used to generate, from document(s), 
            machine-learning samples used to represent pairs of mentions
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    __INIT_PARAMETERS = OrderedDict((generator_attribute_data,))
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple(__INIT_PARAMETERS.keys()) +\
                                                _CoreferenceResolver._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = _CoreferenceResolver._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION+\
                                            tuple(__INIT_PARAMETERS.keys())
    
    def __init__(self, generator, configuration=None):
        # Check input values
        _check_isinstance(MentionPairSampleGenerator, generator, "generator")#strict=False
        # Initialize with parent class
        super().__init__(configuration=configuration)
        # Assign attribute values
        self._generator = generator
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extracts *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        generator, = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        args = args + (generator,)
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, generator_pair_filter_factory_configuration=None):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class.
        
        Args:
            generator_pair_filter_factory_configuration: a factory configuration that can be used 
                to create a specific PairFilter instance, that shall be used to generate the factory 
                configuration of an instance of the Generator class compatible with this resolver class; or 
                None, in which case the factory configuration for an 'AcceptAllPairFilter' instance will be used

        Returns:
            a Mapping instance
        """
        # Check and process inputs
        if generator_pair_filter_factory_configuration is None:
            generator_pair_filter_factory_configuration = AcceptAllPairFilter.define_factory_configuration()
        
        generator_factory_configuration =\
         MentionPairSampleGenerator.define_factory_configuration(pair_filter_factory_configuration=generator_pair_filter_factory_configuration)
        configuration = super().define_configuration()
        configuration["generator"] = generator_factory_configuration
        return configuration
    
    # Instance methods
    def resolve(self, document, ground_truth_document):
        """Predicts a coreference partition for a document, by using the info provided by the ground 
        truth version of the document. If the input 'ground_truth_document' does not possess mention 
        or a non-empty coreference partition value, then the returned value will be None.
        
        Args:
            document: Document instance for which we want to predict a coreference partition; 
                mentions must be defined for it
            ground_truth_document: Document instance referring to the same textual document than 
                'document', and for which mentions and a coreference partition exist and are known. We assume 
                that the universe of the coreference partition associated to the ground truth document is 
                consistent with the mentions defined for it. No alignment will be carried out if the 
                universes are different.

        Returns:
            a CoreferencePartition instance, or None
        """
        # Check input
        self._preprocessing_resolution(document)
        ref_extent2mention = ground_truth_document.extent2mention
        ref_partition = ground_truth_document.coreference_partition
        
        # Filter sys_mentions if None and use it to initialize predicted coreference partition
        self._logger.debug("Initializing predicted coreference partition...")
        mentions = document.mentions
        predicted_coreference_partition = CoreferencePartition(mention_extents=(m.extent for m in mentions))
        
        # Apply oracle
        self._logger.debug("Build predicted coreference partition...")
        if not ref_extent2mention or not ref_partition:
            return predicted_coreference_partition
        
        samples_iterable = self._generator.generate_prediction_set(document) # May filter some pairs
        for sample in samples_iterable:
            left_mention_extent, right_mention_extent = sample.extent_pair
            if not (left_mention_extent in ref_extent2mention 
                    and right_mention_extent in ref_extent2mention):
                continue
            if ref_partition.are_coreferent(left_mention_extent, right_mention_extent):
                predicted_coreference_partition.join(left_mention_extent, right_mention_extent)
        
        return predicted_coreference_partition



class OraclePairScoreResolver(_BaseMentionPairSampleTrainableResolver):
    """ Resolver that predict a coreference partition in a similar way to the McCarthyLehnertResolver, 
    but make sure that the predicted coreference partition will not contain coreference links that 
    do not exist in the true coreference partition.
    
    As such, ground truth data is needed, and it is assumed that the document input in the 'resolve' 
    method already possesses a coreference partition, whose value will be used as ground truth.
    
    As such, can be instantiated only with a MentionPairSampleGenerator instance and an 
    OracleAggressiveMergeCoreferenceDecoder instance.
    
    Arguments:
        model_interface: a _ModelInterface child class instance, representing the interface to the 
            underlying machine learning model to train and to use
        
        generator: a MentionPairSampleGenerator instance, used to generate, from document(s), 
            machine-learning samples used as input by the underlying machine learning model
        
        decoder: an OracleAggressiveMergeCoreferenceDecoder instance, used to interpret scores associated 
            to machine-learning samples into a coreference partition, for the document used to generated 
            the scored machine-learning samples
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        document_hash2cache: document_hash2cache: a "document hash => cache" map, or None; the 
            current value of the underlying model interface's vectorization cache (cf 
            'cortex.coreference.api.vectorization_cache.py')
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _check_subcomponent(self, model_interface, generator, decoder):
        """ Checks that the input model interface, generator, and decoder are correct; that is: 
        - they are suitable to be used with MentionPairSample instances
        - the generator is a MentionPairSampleGenerator instance
        - the decoder is a OracleAggressiveMergeCoreferenceDecoder instance
        
        Raises:
            ValueError: if one input is not of the expected type
        """
        _check_isinstance(MentionPairSampleGenerator, generator, "generator")
        _check_isinstance(OracleAggressiveMergeCoreferenceDecoder, decoder, "decoder")
        super()._check_subcomponent(model_interface, generator, decoder)
    
    @classmethod
    def define_configuration(cls, sample_features_vectorizer_factory_configuration=None, 
                              classifier_factory_configuration=None, 
                              generator_pair_filter_factory_configuration=None):
        """ Creates a configuration object, which can then be used to create a specific instance of 
        this class.
        
        Args:
            sample_features_vectorizer_factory_configuration: a factory configuration that can be 
                used to create a specific _BaseMentionPairSampleFeaturesVectorizer child class instance; or 
                None, in which case the factory configuration for a 'MentionPairSampleFeaturesVectorizer' 
                instance (parametrized with 'quantize=True') will be used
            classifier_factory_configuration: a factory configuration that can be used to 
                create a specific _BinaryClassifier child class instance; or None, in which case the factory 
                configuration for a 'LogisticRegressionSKLEARNBinaryClassifier' instance (parametrized with 
                'class_weight="balanced"') will be used
            generator_pair_filter_factory_configuration: a factory configuration that can be used 
                to create a specific PairFilter instance, that shall be used to generate the factory 
                configuration of an instance of the Generator class compatible with this resolver class; 
                or None, in which case the factory configuration for an 'AcceptAllPairFilter' instance 
                will be used

        Returns:
            a Mapping instance
        """
        # Check and process inputs
        if generator_pair_filter_factory_configuration is None:
            generator_pair_filter_factory_configuration = AcceptAllPairFilter.define_factory_configuration()
        if sample_features_vectorizer_factory_configuration is None:
            sample_features_vectorizer_factory_configuration =\
             MentionPairSampleFeaturesVectorizer.define_factory_configuration(quantize=True)
        if classifier_factory_configuration is None:
            classifier_factory_configuration =\
             LogisticRegressionSKLEARNBinaryClassifier.define_factory_configuration("TOKEN_POS_CLASS", "TOKEN_NEG_CLASS", 
                                                                                    class_weight="balanced")
        
        model_interface_class = _get_appropriate_model_interface_class_for_input_classifier_factory_configuration(classifier_factory_configuration)
        
        generator_factory_configuration =\
         MentionPairSampleGenerator.define_factory_configuration(pair_filter_factory_configuration=generator_pair_filter_factory_configuration)
        decoder_factory_configuration = OracleAggressiveMergeCoreferenceDecoder.define_factory_configuration()
        model_interface_factory_configuration =\
         model_interface_class.define_factory_configuration(sample_features_vectorizer_factory_configuration, 
                                                            classifier_factory_configuration)
        return super().define_configuration(model_interface_factory_configuration, 
                                               generator_factory_configuration, 
                                               decoder_factory_configuration)
    
    # Instance methods
    def resolve(self, ground_truth_document):
        """ Predicts a coreference partition for the input document, assuming it already possesses 
        ground truth coreference partition data.
        
        Args:
            ground_truth_document: a Document instance for which a CoreferencePartition instance 
                must be defined, which already possesses ground truth coreference partition data.

        Returns:
            a CoreferencePartition instance
        """
        # Check input
        self._preprocessing_resolution(ground_truth_document)
        
        # Get ML samples generator
        self._logger.info("Building the prediction data set...")
        ML_samples_iterable = self._generator.generate_prediction_set(ground_truth_document) # , entity_head_extents=entity_head_extents,  singleton_extents=singleton_extents
        
        # Predict coreference scores
        self._logger.info("Predicting the coreference scores...")
        coreference_scores = self._predict_coreference_scores(ML_samples_iterable)
        
        # Decode coreference scores
        self._logger.debug("Decoding coreference scores...")
        ref_coreference_partition = ground_truth_document.coreference_partition
        scores_are_proba = self._model_interface.SCORES_ARE_PROBA
        predicted_coreference_partition = self._decoder.decode(ground_truth_document, coreference_scores, 
                                                              ref_coreference_partition, 
                                                              scores_are_proba=scores_are_proba)
        
        return predicted_coreference_partition



# Utilities
def _get_appropriate_model_interface_class_for_input_classifier_factory_configuration(classifier_factory_configuration):
    """ Returns the model interface class that is appropriate to wrap around the classifier model to 
    be used, according to the input classifier factory configuration.
    
    That is, "sklearn" type classifier will cause the SKLEARNInterface to be returned, whereas 
    "online" type classifier will cause the OnlineInterface to be returned.
    
    Args:
        classifier_factory_configuration: mapping, factory configuration for a classifier ml model

    Returns:
        _BaseClassifierInterface child class
    
    Raises:
        ValueError: if the name, or pseudo-name, of the classifier ml model, specified in the input factory 
            configuration, is not recognized
    """
    if classifier_factory_configuration["name"] in online_classifiers_map:
        model_interface_class = OnlineInterface
    elif classifier_factory_configuration["name"] in sklearn_classifiers_map:
        model_interface_class = SKLEARNInterface
    else:
        msg = "Incorrect name '{}' defined in input 'classifier_factory_configuration': must belong to '{}'"
        msg = msg.format(classifier_factory_configuration["name"], set(itertools.chain(online_classifiers_map.keys(), 
                                                                            sklearn_classifiers_map.keys())
                                                            )
                         )
        raise ValueError(msg)
    return model_interface_class
