# -*- coding: utf-8 -*-

"""
Defines decoder classes that work by decoding scores by modeling them as graph weighed edges, and 
computing min/maximum spanning trees from such a graph
"""

__all__ = ["MSTPrimCoreferenceDecoder", 
           "MSTKruskalCoreferenceDecoder", 
           "HybridBestFirstMSTCoreferenceDecoder",
          ]

import itertools
import numpy

from .pairs import _BaseExtendedMentionPairCoreferenceDecoder, _SimpleMentionPairCoreferenceDecoderMixIn
from cortex.api.markable import NULL_MENTION, get_mention_pair_extent_pair
from cortex.api.coreference_partition import CoreferencePartition
from cortex.tools.mst import prim_mst, kruskal_mst

from cortex.config import language
from cortex.languages import create_get_language_dependent_object_fct
get_language_dependent_object = create_get_language_dependent_object_fct(language)

MENTION_FEATURES = get_language_dependent_object("mention_features")


class MSTPrimCoreferenceDecoder(_SimpleMentionPairCoreferenceDecoderMixIn, 
                            _BaseExtendedMentionPairCoreferenceDecoder):
    """ Maximum spanning tree decoder class, using Prim algorithm.
    
    Interpret the scores as weighted edges between the nodes of the graph whose nodes are the mentions 
    of the considered document, and create coreference links between mentions that are linked to one 
    another on the computed maximum spanning tree on that graph.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _decode(self, document, scores, scores_are_proba=False):
        """ Interprets the input score values, corresponding to pairs of mentions of the input document, 
        in order to predict a coreference partition to associate to this document.
        
        Args:
            document: a Document instance whose 'mentions' attribute must not be None
                scores: a (mention1 extent, mention2 extent) => float" map
            scores_are_proba: boolean, whether or not the binary classification scores should be 
                interpreted as proba values (float between 0 and 1), or not. Is not used, is kept only for 
                API compatibility purposes.

        Returns:
            a CoreferencePartition instance 
        """
        # MST: mention extents --> vertices; scores --> edge weights
        # FIXME: do 'scores' needs to contain values for (NULL_MENTION, mention) scores?
        null_mention_extent = NULL_MENTION.extent
        mentions = document.mentions
        mentions_extents = tuple(m.extent for m in mentions)
        coreference_partition = CoreferencePartition(mention_extents=mentions_extents)
        mentions_extents_ = tuple(itertools.chain((null_mention_extent,), mentions_extents))
        
        _, mst_edges, _ = prim_mst(mentions_extents_, scores, spanning_tree_type="max")
        
        for antecedent_mention_extent, subsequent_mention_extent in mst_edges:
            if antecedent_mention_extent != null_mention_extent:# and subsequent_mention_extent != null_mention_extent: # Join mentions in the same tree
                coreference_partition.join(antecedent_mention_extent, subsequent_mention_extent)
        
        return coreference_partition



class MSTKruskalCoreferenceDecoder(_BaseExtendedMentionPairCoreferenceDecoder):
    """ Maximum spanning tree decoder using Kruskal algorithm to incorporate constraints on the 
    edges that must be present or absent in the computed maximum spanning tree.
    
    Interpret the scores as weighted edges between the nodes of the graph whose nodes are the mentions 
    of the considered document, and create coreference links between mentions that are linked to one 
    another on the computed maximum spanning tree on that graph.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def decode(self, document, scores, positive_edges=None, negative_edges=None):
        """ Interprets the input score values, corresponding to pairs of mentions of the input document, 
        in order to predict a coreference partition to associate to this document.
        
        Args:
            document: a Document instance whose 'mentions' attribute must not be None
                scores: a (mention1 extent, mention2 extent) => float" map
            positive_edges: a collection of pairs of mention extents, which specifies the mention 
                pairs that must belong to the same entity, in the output coreference partition; or None
            negative_edges: a collection of pairs of mention extents, which specifies the mention 
                pairs that must not belong to the same entity, in the output coreference partition; or None

        Returns:
            a CoreferencePartition instance  
        """
        # FIXME: do 'scores' needs to contain values for (NULL_MENTION, mention) scores?
        # MST: mention extents --> vertices; scores --> edge weights
        null_mention_extent = NULL_MENTION.extent
        mentions = document.mentions
        mentions_extents = tuple(m.extent for m in mentions)
        coreference_partition = CoreferencePartition(mention_extents=mentions_extents)
        mentions_extents_ = tuple(itertools.chain((null_mention_extent,), mentions_extents))
        
        _, mst_edges, _ = kruskal_mst(mentions_extents_, scores, spanning_tree_type="max", 
                                      positive_edges=positive_edges, negative_edges=negative_edges)
        
        for antecedent_mention_extent, subsequent_mention_extent in mst_edges:
            if antecedent_mention_extent != null_mention_extent:# and subsequent_mention_extent != null_mention_extent: # Join mentions in the same tree
                coreference_partition.join(antecedent_mention_extent, subsequent_mention_extent)
        
        return coreference_partition



class HybridBestFirstMSTCoreferenceDecoder(_BaseExtendedMentionPairCoreferenceDecoder):
    """ Apply Soon et al (2001) decoding (cf the ClosestFirstCoreferenceDecoder class) when the 
    current mention being considered is an expanded pronoun mention; and compute a maximum spanning 
    tree (using Kruskal algorithm) for the rest.
    
    The way it is done is that the links computed used the Soon & al method are considered as a 
    'positive_edges' constraint, and those that were rejected during the Soon & al method are 
    considered as a 'negative_edges' constraint, when running the Kruskal algorithm.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def decode(self, document, scores, scores_are_proba=False, positive_edges=None, negative_edges=None):
        # FIXME: why are those parameters here, since the collection of positive and negative edges are defined within the function? Is it just bad copypasting?
        # Or is it for signature compatibility reasons?
        """ Interprets the input score values, corresponding to pairs of mentions of the input document, 
        in order to predict a coreference partition to associate to this document.
        
        Args:
            document: a Document instance whose 'mentions' attribute must not be None
            scores: a (mention1 extent, mention2 extent) => float" map
            scores_are_proba: boolean, whether or not the binary classification scores should be 
                interpreted as proba values (float between 0 and 1), or not
            positive_edges: a collection of pairs of mention extents, which specifies the mention 
                pairs that must belong to the same entity, in the output coreference partition; or None. 
                Is not used, is kept only for API compatibility purposes.
            negative_edges: a collection of pairs of mention extents, which specifies the mention 
                pairs that must not belong to the same entity, in the output coreference partition; or None.
                Is not used, is kept only for API compatibility purposes.

        Returns:
            a CoreferencePartition instance 
        """
        # MST: mention extents --> vertices; scores --> weights
        null_mention_extent = NULL_MENTION.extent
        score_threshold = 0.5 if scores_are_proba else 0.0
        mentions = document.mentions
        mention_extents = tuple(m.extent for m in mentions)
        coreference_partition = CoreferencePartition(mention_extents=mention_extents)
        
        # Compute best fist link for pronouns
        best_links = []
        nonbest_links = []
        for j in range(1,len(mentions)):
            subsequent_mention = mentions[j]
            if not MENTION_FEATURES.is_expanded_pronoun(subsequent_mention):
                continue
            max_score = -numpy.Inf
            max_score_antecedent_mention = None
            # Right to left
            for i in range(j-1,-1,-1):
                antecedent_mention = mentions[i]
                # Get score
                extent_pair = get_mention_pair_extent_pair(antecedent_mention, subsequent_mention)
                # Below: must use '.get' because mention extent may not be defined because of the use of a filter during the sample generation phase
                score = scores.get(extent_pair)
                # store the BEST preceding antecedent for each anaphora
                if score is not None and score > score_threshold:
                    if score > max_score:
                        max_score_antecedent_mention = antecedent_mention
                        max_score = score
            # We must also consider the (NULL_MENTION, mention) case: if the corresponding score is the highest, we join with no one
            extent_pair = get_mention_pair_extent_pair(NULL_MENTION, subsequent_mention)
            score = scores[extent_pair] # FIXME: assume that scores contains values for (NULL_MENTION, mention) pairs, but do not make this assumption a few lines below.
            if score > max_score:
                max_score_antecedent_mention = None
                max_score = score
            # Create pair for best antecedent if one was found
            if max_score_antecedent_mention is not None:
                extent_pair = get_mention_pair_extent_pair(max_score_antecedent_mention, subsequent_mention)
                best_links.append(extent_pair)
                # Remove non selected edges (i.e. at most one backward attachment for pronouns)
                for i in range(j-1,-1,-1):
                    antecedent_mention = mentions[i]                
                    if antecedent_mention is not max_score_antecedent_mention:
                        extent_pair = get_mention_pair_extent_pair(antecedent_mention, subsequent_mention)
                        # FIXME: we do that only if max_score_antecedent_mention is None. 
                        # But what if it is None because the max_score corresponds to the pair with the NULL_MENTION?
                        # Yes, keep it that way.
                        # That means that for this mention, no coreference link with a previous mention should be found, 
                        # so either it is a singleton, or a non-anaphoric mention.
                        nonbest_links.append(extent_pair)
        
        # Find MST on remaining mentions
        weights = dict(scores)
        for mention in mentions:
            extent_pair = get_mention_pair_extent_pair(NULL_MENTION, mention)
            weights[extent_pair] = 0. # FIXME: this value should depends on the value of 'scores_are_proba', right?
            # FIXME: WARNING! The above line can erase already existing values! The priority is to keep to potentially already existing values, right?
            # No, it is MST, supposed to be used with MentionPairSampleGenerator, no score for such pairs. But why 0?
        mention_extents_ = tuple(itertools.chain((null_mention_extent,), mention_extents))
        
        _, mst_edges, _ = kruskal_mst(mention_extents_, weights, positive_edges=best_links, negative_edges=nonbest_links)
        
        # Join links        
        for antecedent_mention_extent, subsequent_mention_extent in mst_edges:
            if antecedent_mention_extent != null_mention_extent:# and subsequent_mention_extent != null_mention_extent: # Join mentions in the same tree
                coreference_partition.join(antecedent_mention_extent, subsequent_mention_extent)
        
        return coreference_partition
