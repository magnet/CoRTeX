# -*- coding: utf-8 -*-

"""
Defines decoder classes that work by decoding scores associated to pairs of mentions
"""

__all__ = ["ClosestFirstCoreferenceDecoder", 
           "AggressiveMergeCoreferenceDecoder", 
           "OracleAggressiveMergeCoreferenceDecoder", 
           "BestFirstCoreferenceDecoder", 
           "NBestFirstCoreferenceDecoder",
           "ConstrainedBestFirstCoreferenceDecoder",
           "BestNextCoreferenceDecoder", 
           "BestLinkCoreferenceDecoder", 
           "NBestLinkCoreferenceDecoder", 
           "MixedFirstCoreferenceDecoder", 
           "PermutationCoreferenceDecoder", 
           "HACCoreferenceDecoder"
           "ExtendedBestFirstCoreferenceDecoder", 
           "ConstrainedExtendedBestFirstCoreferenceDecoder", 
           "__BaseMentionPairCoreferenceDecoder", 
           "_BaseMentionPairCoreferenceDecoder",
           "_BaseExtendedMentionPairCoreferenceDecoder",
           "_BaseNBestCoreferenceDecoder",
           "_SimpleMentionPairCoreferenceDecoderMixIn",
           "_ConstrainedMentionPairCoreferenceDecoderMixIn",
           ]

import abc
import numpy
import heapq
from collections import OrderedDict

from .base import _CoreferenceDecoder
from cortex.api.markable import NULL_MENTION, get_mention_pair_extent_pair
from cortex.api.coreference_partition import CoreferencePartition
from cortex.tools import (_create_simple_set_parameter_value_in_configuration, 
                            _create_simple_get_parameter_value_from_configuration)
from cortex.tools.munkres import MUNKRES

from cortex.config import language
from cortex.languages import create_get_language_dependent_object_fct
get_language_dependent_object = create_get_language_dependent_object_fct(language)

MENTION_FEATURES = get_language_dependent_object("mention_features")

class __BaseMentionPairCoreferenceDecoder(_CoreferenceDecoder):
    """ Base class for decoder operating on binary classification prediction scores related to pairs 
    of Mention instances.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """

class _BaseMentionPairCoreferenceDecoder(__BaseMentionPairCoreferenceDecoder):
    """ Base class for decoder operating on binary classification prediction scores related to pairs 
    of Mention instances, where no mention is the NULLMENTION object.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """

class _BaseExtendedMentionPairCoreferenceDecoder(_CoreferenceDecoder):
    """ Base class for decoder operating on binary classification prediction scores related to pairs 
    of mention, where one mention can be the NULLMENTION object. 
    Assume that the 'scores' input to the 'decode' method contains scores for every 
    (NULL_MENTION, mention) pair, for the mentions contained in the Document instance.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """


class _SimpleMentionPairCoreferenceDecoderMixIn(_CoreferenceDecoder):
    """ Base class for decoder operating only on prediction scores.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
    """
    
    def decode(self, document, scores, scores_are_proba=False):
        """ Interprets the input score values, corresponding to pairs of mentions of the input document, 
        in order to predict a coreference partition to associate to this document.
        
        Args:
            document: a Document instance whose 'mentions' attribute must not be None
            scores: a (mention1 extent, mention2 extent) => float" map
            scores_are_proba: boolean, whether or not the binary classification scores should be 
                interpreted as proba values (float between 0 and 1), or not

        Returns:
            a CoreferencePartition instance 
        """
        return self._decode(document, scores, scores_are_proba=scores_are_proba)
    
    @abc.abstractmethod
    def _decode(self, document, scores, scores_are_proba=False):
        """ Interprets the input score values, corresponding to pairs of mentions of the input document, 
        in order to predict a coreference partition to associate to this document.
        
        Args:
            document: a Document instance whose 'mentions' attribute must not be None
            scores: a (mention1 extent, mention2 extent) => float" map
            scores_are_proba: boolean, whether or not the binary classification scores should be 
                interpreted as proba values (float between 0 and 1), or not

        Returns:
            a CoreferencePartition instance 
        """
        pass


########## CLOSEST FIRST DECODER ##########
class ClosestFirstCoreferenceDecoder(_SimpleMentionPairCoreferenceDecoderMixIn, 
                                     _BaseMentionPairCoreferenceDecoder):
    """ Soon et al (2001) decoding.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _decode(self, document, scores, scores_are_proba=False):
        """ Interprets the input score values, corresponding to pairs of mentions of the input document, 
        in order to predict a coreference partition to associate to this document.
        
        Args:
            document: a Document instance whose 'mentions' attribute must not be None
            scores: a (mention1 extent, mention2 extent) => float" map
            scores_are_proba: boolean, whether or not the binary classification scores should be 
                interpreted as proba values (float between 0 and 1), or not

        Returns:
            a CoreferencePartition instance 
        """
        # Check input
        score_threshold = 0.5 if scores_are_proba else 0.0
        mentions = document.mentions
        coreference_partition = CoreferencePartition(mention_extents=(m.extent for m in mentions))
        
        # Enumerate mentions from first to last (not considering the first, assuming it is anaphoric) # FIXME: is that the correct interpretation?
        for j in range(1, len(mentions)):
            subsequent_mention = mentions[j]
            # Enumerate mentions from last to first possible
            for i in range(j-1,-1,-1):
                antecedent_mention = mentions[i]
                # Selecting the CLOSEST preceding antecedent for each anaphoric mention
                extent_pair = get_mention_pair_extent_pair(antecedent_mention, subsequent_mention)
                # Below: must use '.get' because mention extent may not be defined because of the use of a filter during the sample generation phase
                score = scores.get(extent_pair)
                if score is not None and score > score_threshold: # FIXME: what to do about this legacy comment? 'check if antecedent_mention,subsequent_mention has been filtered out'
                    coreference_partition.join(antecedent_mention.extent, subsequent_mention.extent)
                    break
        
        return coreference_partition


########## AGRESSIVE MERGE DECODER ##########
class AggressiveMergeCoreferenceDecoder(_SimpleMentionPairCoreferenceDecoderMixIn, 
                                        _BaseMentionPairCoreferenceDecoder):
    """ McCarthy & Lehnert (1995) decoding.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _decode(self, document, scores, scores_are_proba=False):
        """ Interprets the input score values, corresponding to pairs of mentions of the input document, 
        in order to predict a coreference partition to associate to this document.
        
        Args:
            document: a Document instance whose 'mentions' attribute must not be None
            scores: a (mention1 extent, mention2 extent) => float" map
            scores_are_proba: boolean, whether or not the binary classification scores should be 
                interpreted as proba values (float between 0 and 1), or not.

        Returns:
            a CoreferencePartition instance 
        """
        # Check input
        score_threshold = 0.5 if scores_are_proba else 0.0
        mentions = document.mentions
        coreference_partition = CoreferencePartition(mention_extents=(m.extent for m in mentions))
        
        # Enumerate mentions from first to last (not considering the first, assuming it is anaphoric) # FIXME: is that the correct interpretation?
        for j in range(1,len(mentions)):
            subsequent_mention = mentions[j]
            # Enumerate mentions from last to first possible
            for i in range(j-1,-1,-1):
                antecedent_mention = mentions[i]
                extent_pair = get_mention_pair_extent_pair(antecedent_mention, subsequent_mention)
                # Below: must use '.get' because mention extent may not be defined because of the use of a filter during the sample generation phase
                score = scores.get(extent_pair)
                # Merge all positive pairs
                if score is not None and score > score_threshold: # FIXME: legacy comment 'check if antecedent_mention,subsequent_mention has been filtered out'
                    coreference_partition.join(antecedent_mention.extent, subsequent_mention.extent)
        
        return coreference_partition


class OracleAggressiveMergeCoreferenceDecoder(_BaseMentionPairCoreferenceDecoder):
    """ McCarthy & Lehnert (1995) decoding, but only pairs of mentions that indeed coreferring 
    according to ground truth will be joined in the same entity during the creation of the 
    CoreferencePartition instance.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def decode(self, document, scores, ref_coreference_partition, scores_are_proba=False):
        """ Interprets the input score values, corresponding to pairs of mentions of the input document, 
        in order to predict a coreference partition to associate to this document.
        
        Args:
            document: a Document instance whose 'mentions' attribute must not be None
            scores: a (mention1 extent, mention2 extent) => float" map
            ref_coreference_partition: a CoreferencePartition instance; the ground truth needed 
                to implement the decoding for the input (document, scores) pair
            scores_are_proba: boolean, whether or not the binary classification scores should be 
                interpreted as proba values (float between 0 and 1), or not.

        Returns:
            a CoreferencePartition instance 
        """
        return self._decode(document, scores, ref_coreference_partition, scores_are_proba=scores_are_proba)
    
    def _decode(self, document, scores, ref_coreference_partition, scores_are_proba=False):
        """ Interprets the input score values, corresponding to pairs of mentions of the input document, 
        in order to predict a coreference partition to associate to this document.
        
        Args:
            document: a Document instance whose 'mentions' attribute must not be None
            scores: a (mention1 extent, mention2 extent) => float" map
            ref_coreference_partition: a CoreferencePartition instance; the ground truth needed 
                to implement the decoding for the input (document, scores) pair
            scores_are_proba: boolean, whether or not the binary classification scores should be 
                interpreted as proba values (float between 0 and 1), or not.

        Returns:
            a CoreferencePartition instance 
        """
        # Check input
        score_threshold = 0.5 if scores_are_proba else 0.0
        mentions = document.mentions
        coreference_partition = CoreferencePartition(mention_extents=(m.extent for m in mentions))
        
        # Decode using oracle data: enumerate mentions from first to last (not considering the first, assuming it is anaphoric) # FIXME: is that the correct interpretation?
        for j in range(1, len(mentions)):
            subsequent_mention = mentions[j]
            # Enumerate mentions from last to first possible
            for i in range(j-1,-1,-1):
                antecedent_mention = mentions[i]
                extent_pair = get_mention_pair_extent_pair(antecedent_mention, subsequent_mention)
                # Below: must use '.get' because mention extent may not be defined because of the use of a filter during the sample generation phase
                score = scores.get(extent_pair)
                # Merge all positive pairs
                if score is not None and score > score_threshold and ref_coreference_partition.are_coreferent(antecedent_mention.extent, subsequent_mention.extent):
                    coreference_partition.join(antecedent_mention.extent, subsequent_mention.extent)
    
        return coreference_partition


########## BEST FIRST DECODER ##########
class BestFirstCoreferenceDecoder(_SimpleMentionPairCoreferenceDecoderMixIn, _BaseMentionPairCoreferenceDecoder):
    """ Ng & Cardie (2002) decoding.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _decode(self, document, scores, scores_are_proba=False):
        """ Interprets the input score values, corresponding to pairs of mentions of the input document, 
        in order to predict a coreference partition to associate to this document.
        
        Args:
            document: a Document instance whose 'mentions' attribute must not be None
            scores: a (mention1 extent, mention2 extent) => float" map
            scores_are_proba: boolean, whether or not the binary classification scores should be 
                interpreted as proba values (float between 0 and 1), or not.

        Returns:
            a CoreferencePartition instance 
        """
        # Check input
        score_threshold = 0.5 if scores_are_proba else 0.0
        mentions = document.mentions
        coreference_partition = CoreferencePartition(mention_extents=(m.extent for m in mentions))
        
        # Enumerate mentions from first to last (not considering the first, assuming it is anaphoric) # FIXME: is that the correct interpretation?
        for j in range(1,len(mentions)):
            subsequent_mention = mentions[j]
            max_score = -numpy.Inf
            max_score_antecedent_mention = None
            # Enumerate mentions from last to first possible
            for i in range(j-1,-1,-1):
                antecedent_mention = mentions[i]
                # Skip pronominal antecent mention for non-pronominal anaphora
                if MENTION_FEATURES.is_expanded_pronoun(antecedent_mention) and not MENTION_FEATURES.is_expanded_pronoun(subsequent_mention):
                    continue
                # Get score
                extent_pair = get_mention_pair_extent_pair(antecedent_mention, subsequent_mention)
                # Below: must use '.get' because mention extent may not be defined because of the use of a filter during the sample generation phase
                score = scores.get(extent_pair)
                if score is not None and score > score_threshold and score > max_score:
                    # Store the BEST antecedent mention for each anaphora
                    max_score_antecedent_mention = antecedent_mention
                    max_score = score             
            # Create pair for best antecedent if one was found
            if max_score_antecedent_mention is not None:
                coreference_partition.join(max_score_antecedent_mention.extent, subsequent_mention.extent)
        
        return coreference_partition


class ExtendedBestFirstCoreferenceDecoder(_SimpleMentionPairCoreferenceDecoderMixIn, 
                                          _BaseExtendedMentionPairCoreferenceDecoder):
    """ Ng & Cardie (2002) decoding, on scores associated to pairs of mentions which can contain the 
    NULLMENTION.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _decode(self, document, scores, scores_are_proba=False):
        """ Interprets the input score values, corresponding to pairs of mentions of the input document, 
        in order to predict a coreference partition to associate to this document.
        
        Args:
            document: a Document instance whose 'mentions' attribute must not be None
            scores: a (mention1 extent, mention2 extent) => float" map
            scores_are_proba: boolean, whether or not the binary classification scores should be 
                interpreted as proba values (float between 0 and 1), or not.

        Returns:
            a CoreferencePartition instance 
        """
        # Check input
        score_threshold = 0.5 if scores_are_proba else 0.0
        mentions = document.mentions
        coreference_partition = CoreferencePartition(mention_extents=(m.extent for m in mentions))
        
        # Enumerate mentions from first to last (not considering the first, assuming it is anaphoric) # FIXME: is that the correct interpretation?
        for j in range(1,len(mentions)): # Start from the second mention, since there is no antecedent to the first anyway
            subsequent_mention = mentions[j]
            max_score = -numpy.Inf
            max_score_antecedent_mention = None
            # Enumerate mentions from last to first possible
            for i in range(j-1,-1,-1):
                antecedent_mention = mentions[i]
                # Skip pronominal antecent mention for non-pronominal anaphora
                if MENTION_FEATURES.is_expanded_pronoun(antecedent_mention) and not MENTION_FEATURES.is_expanded_pronoun(subsequent_mention):
                    continue
                # Get score
                extent_pair = get_mention_pair_extent_pair(antecedent_mention, subsequent_mention)
                # Below: must use '.get' because mention extent may not be defined because of the use of a filter during the sample generation phase
                score = scores.get(extent_pair)
                if score is not None and score > score_threshold and score > max_score:
                    # Store the BEST antecedent mention for each anaphora
                    max_score_antecedent_mention = antecedent_mention
                    max_score = score
            # We must also consider the (NULL_MENTION, mention) case: if the corresponding score is the highest, we join with no one
            extent_pair = get_mention_pair_extent_pair(NULL_MENTION, subsequent_mention)
            score = scores[extent_pair]
            if score > max_score:
                max_score_antecedent_mention = None
                max_score = score
            # Create pair for best antecedent if one was found
            if max_score_antecedent_mention is not None:
                coreference_partition.join(max_score_antecedent_mention.extent, subsequent_mention.extent)
        
        return coreference_partition  



########## CONSTRAINED DECODER ##########
class _ConstrainedMentionPairCoreferenceDecoderMixIn(_CoreferenceDecoder):
    """ Base class for decoder operating on prediction scores, along with info regarding constraints 
    on which score should be considered in priority, or should be completely ignored.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def decode(self, document, scores, scores_are_proba=False, positive_edges=None, negative_edges=None):
        """ Interprets the input score values, corresponding to pairs of mentions of the input document, 
        in order to predict a coreference partition to associate to this document.
        
        Args:
            document: a Document instance whose 'mentions' attribute must not be None
            scores: a (mention1 extent, mention2 extent) => float" map
            scores_are_proba: boolean, whether or not the binary classification scores should be 
                interpreted as proba values (float between 0 and 1), or not
            positive_edges: a collection of pairs of mention extents, which specifies the mention 
                pairs that must belong to the same entity, in the output coreference partition; or None.
            negative_edges: a collection of pairs of mention extents, which specifies the mention 
                pairs that must not belong to the same entity, in the output coreference partition; or None.

        Returns:
            a CoreferencePartition instance 
        """
        return self._decode(document, scores, scores_are_proba=scores_are_proba, 
                            positive_edges=positive_edges, negative_edges=negative_edges)
    
    @abc.abstractmethod
    def _decode(self, document, scores, scores_are_proba=False, positive_edges=None, negative_edges=None):
        """ Interprets the input score values, corresponding to pairs of mentions of the input document, 
        in order to predict a coreference partition to associate to this document.
        
        Args:
            document: a Document instance whose 'mentions' attribute must not be None
            scores: a (mention1 extent, mention2 extent) => float" map
            scores_are_proba: boolean, whether or not the binary classification scores should be 
                interpreted as proba values (float between 0 and 1), or not
            positive_edges: a collection of pairs of mention extents, which specifies the mention 
                pairs that must belong to the same entity, in the output coreference partition; or None.
            negative_edges: a collection of pairs of mention extents, which specifies the mention 
                pairs that must not belong to the same entity, in the output coreference partition; or None.

        Returns:
            a CoreferencePartition instance 
        """
        pass


class ConstrainedBestFirstCoreferenceDecoder(_ConstrainedMentionPairCoreferenceDecoderMixIn, 
                                             _BaseMentionPairCoreferenceDecoder):
    """ Ng & Cardie (2002) decoding, but with taking into account constraints first.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _decode(self, document, scores, scores_are_proba=False, positive_edges=None, negative_edges=None):
        """ Interprets the input score values, corresponding to pairs of mentions of the input document, 
        in order to predict a coreference partition to associate to this document.
        
        Args:
            document: a Document instance whose 'mentions' attribute must not be None
            scores: a (mention1 extent, mention2 extent) => float" map
            scores_are_proba: boolean, whether or not the binary classification scores should be 
                interpreted as proba values (float between 0 and 1), or not
            positive_edges: a collection of pairs of mention extents, which specifies the mention 
                pairs that must belong to the same entity, in the output coreference partition; or None.
            negative_edges: a collection of pairs of mention extents, which specifies the mention 
                pairs that must not belong to the same entity, in the output coreference partition; or None.

        Returns:
            a CoreferencePartition instance 
        """
        # Check input
        score_threshold = 0.5 if scores_are_proba else 0.0
        mentions = document.mentions
        coreference_partition = CoreferencePartition(mention_extents=(m.extent for m in mentions))
        if positive_edges is None:
            positive_edges = tuple()
        if negative_edges is None:
            negative_edges = tuple()
        
        neg_set = set(negative_edges)
        already_set = set() # Corresponds to mentions at right position in a positive constraint
        
        # Join positive constrains
        for extent1, extent2 in positive_edges:
            coreference_partition.join(extent1, extent2)
            m1 = document.extent2mention[extent1]
            m2 = document.extent2mention[extent2]
            if m1 < m2:
                already_set.add(extent1)
            if m2 < m1:
                already_set.add(extent2)
        
        # Apply best first strategy
        # Left to right
        for j in range(1,len(mentions)):
            subsequent_mention = mentions[j]
            if subsequent_mention.extent in already_set:
                continue # Subsequent_mention already linked to an antecedent
            max_score = -numpy.Inf
            max_score_antecedent_mention = None
            # Right to left
            for i in range(j-1,-1,-1):
                antecedent_mention = mentions[i]
                # Skip pronominal antecent mention for non-pronominal anaphora
                if MENTION_FEATURES.is_expanded_pronoun(antecedent_mention) and not MENTION_FEATURES.is_expanded_pronoun(subsequent_mention):
                    continue
                # Skip neg_set elements
                extent_pair = get_mention_pair_extent_pair(antecedent_mention, subsequent_mention)
                if extent_pair in neg_set or get_mention_pair_extent_pair(subsequent_mention, antecedent_mention) in neg_set:
                    continue # Non authorized link
                # Get score
                # Below: must use '.get' because mention extent may not be defined because of the use of a filter during the sample generation phase
                score = scores.get(extent_pair)
                if score is not None and score > score_threshold and score > max_score:
                    # Store the BEST antecedent mention for each anaphora
                    max_score_antecedent_mention = antecedent_mention
                    max_score = score
            # Create pair for best antecedent if one was found
            if max_score_antecedent_mention is not None:
                coreference_partition.join(max_score_antecedent_mention.extent, subsequent_mention.extent)
        
        return coreference_partition


class ConstrainedExtendedBestFirstCoreferenceDecoder(_ConstrainedMentionPairCoreferenceDecoderMixIn, 
                                                     _BaseExtendedMentionPairCoreferenceDecoder):
    """ Ng & Cardie (2002) decoding, but with taking into account constraints first, on scores 
    associated to pairs of mentions which can contain the NULLMENTION.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _decode(self, document, scores, scores_are_proba=False, positive_edges=None, negative_edges=None):
        """ Interprets the input score values, corresponding to pairs of mentions of the input document, 
        in order to predict a coreference partition to associate to this document.
        
        Args:
            document: a Document instance whose 'mentions' attribute must not be None
            scores: a (mention1 extent, mention2 extent) => float" map
            scores_are_proba: boolean, whether or not the binary classification scores should be 
                interpreted as proba values (float between 0 and 1), or not
            positive_edges: a collection of pairs of mention extents, which specifies the mention 
                pairs that must belong to the same entity, in the output coreference partition; or None.
            negative_edges: a collection of pairs of mention extents, which specifies the mention 
                pairs that must not belong to the same entity, in the output coreference partition; or None.

        Returns:
            a CoreferencePartition instance 
        """
        # Check input
        score_threshold = 0.5 if scores_are_proba else 0.0
        mentions = document.mentions
        coreference_partition = CoreferencePartition(mention_extents=(m.extent for m in mentions))
        if positive_edges is None:
            positive_edges = tuple()
        if negative_edges is None:
            negative_edges = tuple()
        
        neg_set = set(negative_edges)
        already_set = set() # corresponds to mentions at right position in a positive constraint
        
        # Join positive constrains
        for extent1, extent2 in positive_edges:
            coreference_partition.join(extent1, extent2)
            m1 = document.extent2mention[extent1]
            m2 = document.extent2mention[extent2]
            if m1 < m2:
                already_set.add(extent1)
            if m2 < m1:
                already_set.add(extent2)
        
        # Apply best-first strategy
        # Left to right
        for j in range(1,len(mentions)):
            subsequent_mention = mentions[j]
            if subsequent_mention.extent in already_set:
                continue # Subsequent_mention already linked to an antecedent
            max_score = -numpy.Inf
            max_score_antecedent_mention = None
            # Right to left
            for i in range(j-1,-1,-1):
                antecedent_mention = mentions[i]
                # Skip pronominal antecent mention for non-pronominal anaphora
                if MENTION_FEATURES.is_expanded_pronoun(antecedent_mention) and not MENTION_FEATURES.is_expanded_pronoun(subsequent_mention):
                    continue
                # Skip neg_set elements
                extent_pair = get_mention_pair_extent_pair(antecedent_mention, subsequent_mention)
                if extent_pair in neg_set or get_mention_pair_extent_pair(subsequent_mention, antecedent_mention) in neg_set:
                    continue # Non authorized link
                # Get score
                # Below: must use '.get' because mention extent may not be defined because of the use of a filter during the sample generation phase
                score = scores.get(extent_pair)
                if score is not None and score > score_threshold and score > max_score:
                    # Store the BEST antecedent mention for each anaphora
                    max_score_antecedent_mention = antecedent_mention
                    max_score = score
            # Create pair for best next mention if one was found
            if scores[get_mention_pair_extent_pair(NULL_MENTION, subsequent_mention)] > max_score:
                max_score_antecedent_mention = None
            if max_score_antecedent_mention is not None:
                coreference_partition.join(max_score_antecedent_mention.extent, subsequent_mention.extent)
        
        return coreference_partition



########## BEST NEXT DECODER ##########
class BestNextCoreferenceDecoder(_SimpleMentionPairCoreferenceDecoderMixIn, _BaseMentionPairCoreferenceDecoder):
    """ Deconding such that, for a given mention, instead of considering the potential link with the 
    mentions that come before it (according to the document's reading order), such as with the 
    Ng & Cardie decoder, consider only the potential link with the mentions that come after.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _decode(self, document, scores, scores_are_proba=False):
        """ Interprets the input score values, corresponding to pairs of mentions of the input document, 
        in order to predict a coreference partition to associate to this document.
        
        Args:
            document: a Document instance whose 'mentions' attribute must not be None
            scores: a (mention1 extent, mention2 extent) => float" map
            scores_are_proba: boolean, whether or not the binary classification scores should be 
                interpreted as proba values (float between 0 and 1), or not.

        Returns:
            a CoreferencePartition instance 
        """
        # Check input
        score_threshold = 0.5 if scores_are_proba else 0.0
        mentions = document.mentions
        coreference_partition = CoreferencePartition(mention_extents=(m.extent for m in mentions))
        
        # Enumerate mentions from first to last (not considering the first, assuming it is anaphoric) # FIXME: is that the correct interpretation?
        mentions_nb = len(mentions)
        for i in range(0, mentions_nb):
            antecedent_mention = mentions[i]
            max_score = -numpy.Inf
            max_score_subsequent_mention = None
            # Enumerate mentions from next to last
            for j in range(i+1, mentions_nb):
                subsequent_mention = mentions[j]
                # Skip pronominal antecent mention for non-pronominal anaphora
                if MENTION_FEATURES.is_expanded_pronoun(antecedent_mention) and not MENTION_FEATURES.is_expanded_pronoun(subsequent_mention):
                    continue
                # Get score
                extent_pair = get_mention_pair_extent_pair(antecedent_mention, subsequent_mention)
                # Below: must use '.get' because mention extent may not be defined because of the use of a filter during the sample generation phase
                score = scores.get(extent_pair)
                if score is not None and score > score_threshold and score > max_score:
                    # Store the BEST subsequent mention for each anaphora
                    max_score_subsequent_mention = subsequent_mention
                    max_score = score
            # Create pair for best subsequent mention if one was found
            if max_score_subsequent_mention is not None:
                coreference_partition.join(antecedent_mention.extent, max_score_subsequent_mention.extent)
        
        return coreference_partition



########## BEST LINK DECODER ##########
class BestLinkCoreferenceDecoder(_SimpleMentionPairCoreferenceDecoderMixIn, _BaseMentionPairCoreferenceDecoder):
    """ Like the BestFirstCoreferenceDecoder (Ng & Cardie, 2002), but, for a given mention, instead 
    of considering the potential link with the mentions that come before it (according to the 
    document's reading order), consider all potential links with other mentions.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _decode(self, document, scores, scores_are_proba=False):
        """ Interprets the input score values, corresponding to pairs of mentions of the input document, 
        in order to predict a coreference partition to associate to this document.
        
        Args:
            document: a Document instance whose 'mentions' attribute must not be None
            scores: a (mention1 extent, mention2 extent) => float" map
            scores_are_proba: boolean, whether or not the binary classification scores should be 
                interpreted as proba values (float between 0 and 1), or not.

        Returns:
            a CoreferencePartition instance 
        """
        # Check input
        score_threshold = 0.5 if scores_are_proba else 0.0
        mentions = document.mentions
        coreference_partition = CoreferencePartition(mention_extents=(m.extent for m in mentions))
        
        # Enumerate all mentions
        for subsequent_mention in mentions:
            max_score = -numpy.Inf
            max_score_mention = None
            for antecedent_mention in mentions:
                if subsequent_mention is antecedent_mention:
                    continue
                # Get score
                score = scores.get(get_mention_pair_extent_pair(subsequent_mention, antecedent_mention))
                if score is not None and score > score_threshold and score > max_score:
                    # Store the BEST link for each mention
                    max_score_mention = antecedent_mention
                    max_score = score
            # Create pair for best link mention if one was found
            if max_score_mention is not None:
                coreference_partition.join(subsequent_mention.extent, max_score_mention.extent)
        
        return coreference_partition



########## CLOSEST-BEST MIXED FIRST DECODER ##########
class MixedFirstCoreferenceDecoder(_SimpleMentionPairCoreferenceDecoderMixIn, _BaseMentionPairCoreferenceDecoder):
    """ Apply Soon et al (2001) decoding (cf the ClosestFirstCoreferenceDecoder class) when the 
    current mention being considered is an expanded pronoun mention; and apply Ng & Cardie (2002) 
    decoding otherwise (cf the BestFirstCoreferenceDecoder class).
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _decode(self, document, scores, scores_are_proba=False):
        """ Interprets the input score values, corresponding to pairs of mentions of the input document, 
        in order to predict a coreference partition to associate to this document.
        
        Args:
            document: a Document instance whose 'mentions' attribute must not be None
            scores: a (mention1 extent, mention2 extent) => float" map
            scores_are_proba: boolean, whether or not the binary classification scores should be 
                interpreted as proba values (float between 0 and 1), or not.

        Returns:
            a CoreferencePartition instance 
        """
        # Check input
        score_threshold = 0.5 if scores_are_proba else 0.0
        mentions = document.mentions
        coreference_partition = CoreferencePartition(mention_extents=(m.extent for m in mentions))
        
        # Closest first for expanded pronoun mention
        # Enumerate mentions from first to last (not considering the first, assuming it is anaphoric) # FIXME: is that the correct interpretation?
        for j in range(1,len(mentions)):
            subsequent_mention = mentions[j]
            if MENTION_FEATURES.is_expanded_pronoun(subsequent_mention): # Closest-first for pronouns
                # Enumerate mentions from last to first possible
                for i in range(j-1,-1,-1):
                    antecedent_mention = mentions[i]
                    # Get score
                    extent_pair = get_mention_pair_extent_pair(antecedent_mention, subsequent_mention)
                    # Below: must use '.get' because mention extent may not be defined because of the use of a filter during the sample generation phase
                    score = scores.get(extent_pair)
                    if score is not None and score > score_threshold:
                        # Selecting the CLOSEST preceding antecedent for each anaphora
                        coreference_partition.join(antecedent_mention.extent, subsequent_mention.extent)
                        break
            
            # Best-first for non expanded pronoun mentions
            else:
                max_score = -numpy.Inf
                max_score_antecedent_mention = None
                # Enumerate mentions from last to first possible
                for i in range(j-1,-1,-1):
                    antecedent_mention = mentions[i]
                    # Skip pronominal antecent mention for non-pronominal anaphora
                    if MENTION_FEATURES.is_expanded_pronoun(antecedent_mention) and not MENTION_FEATURES.is_expanded_pronoun(subsequent_mention):
                    #if MENTION_FEATURES.is_expanded_pronoun(antecedent_mention):
                        continue
                    # Get score
                    extent_pair = get_mention_pair_extent_pair(antecedent_mention, subsequent_mention)
                    # Below: must use '.get' because mention extent may not be defined because of the use of a filter during the sample generation phase
                    score = scores.get(extent_pair)
                    if score is not None and score > score_threshold and score > max_score:
                        # Store the BEST antecedent mention for each anaphora
                        max_score_antecedent_mention = antecedent_mention
                        max_score = score
                # create pair for best antecedent if one was found
                if max_score_antecedent_mention is not None:
                    coreference_partition.join(max_score_antecedent_mention.extent, subsequent_mention.extent)
        
        return coreference_partition



########## N BEST FIRST DECODER ##########
n_best_parameter_data = ("n_best",
                         (_create_simple_get_parameter_value_from_configuration("n_best", postprocess_fct=int),
                          _create_simple_set_parameter_value_in_configuration("n_best"))
                         )
class _BaseNBestCoreferenceDecoder(_SimpleMentionPairCoreferenceDecoderMixIn, _BaseMentionPairCoreferenceDecoder):
    """ Base class for decoder classes that needs to be instantiated with a 'n_best' parameter.
    
    Arguments:
        n_best: int, the maximum number of 'best link' to create for the currently considered 
            mention in the primary loop of the 'decode' algorithm
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        n_best: int, the maximum number of 'best link' to create for the currently considered 
            mention in the primary loop of the 'decode' algorithm
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    __INIT_PARAMETERS = OrderedDict((n_best_parameter_data,))
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple(__INIT_PARAMETERS.keys()) +\
                                                _BaseMentionPairCoreferenceDecoder._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = _BaseMentionPairCoreferenceDecoder._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION+\
                                            tuple(__INIT_PARAMETERS.keys())
    
    def __init__(self, n_best, configuration=None):
        # Check input values
        if not isinstance(n_best, int) or not n_best:
            message = "Incorrect 'n_best' input value '{}', must be a positive integer.".format(n_best)
            raise ValueError(message)
        # Init parent
        super().__init__(configuration=configuration)
        # Assign attribute values
        self.n_best = n_best
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extract *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        n_best, = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        args = args + (n_best,)
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, n_best):
        """ Create a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Cf the documentation of the class of know which argument(s) to use.
        
        Returns:
            a Mapping instance
        """
        configuration = super().define_configuration()
        configuration["n_best"] = n_best
        return configuration


class NBestFirstCoreferenceDecoder(_BaseNBestCoreferenceDecoder):
    """ Like the BestFirstCoreferenceDecoder, but create a coreference link with all N best 
    preceding mentions, instead of just the best.
    
    Arguments:
        n_best: int, the maximum number of 'best link' to create for the currently considered 
            mention in the primary loop of the 'decode' algorithm
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        n_best: int, the maximum number of 'best link' to create for the currently considered 
            mention in the primary loop of the 'decode' algorithm
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _decode(self, document, scores, scores_are_proba=False):
        """ Interprets the input score values, corresponding to pairs of mentions of the input document, 
        in order to predict a coreference partition to associate to this document.
        
        Args:
            document: a Document instance whose 'mentions' attribute must not be None
            scores: a (mention1 extent, mention2 extent) => float" map
            scores_are_proba: boolean, whether or not the binary classification scores should be 
                interpreted as proba values (float between 0 and 1), or not.

        Returns:
            a CoreferencePartition instance 
        """
        # Check input
        score_threshold = 0.5 if scores_are_proba else 0.0
        mentions = document.mentions
        coreference_partition = CoreferencePartition(mention_extents=(m.extent for m in mentions))
        
        # Enumerate mentions from first to last (not considering the first, assuming it is anaphoric) # FIXME: is that the correct interpretation?
        for j in range(1,len(mentions)):
            subsequent_mention = mentions[j]
            list_best = []
            # Enumerate mentions from last to first possible
            for i in range(j-1,-1,-1):
                antecedent_mention = mentions[i]
                # Skip pronominal antecent mention for non-pronominal anaphora
                if MENTION_FEATURES.is_expanded_pronoun(antecedent_mention) and not MENTION_FEATURES.is_expanded_pronoun(subsequent_mention):
                    continue
                # Get score
                extent_pair = get_mention_pair_extent_pair(antecedent_mention, subsequent_mention)
                # Below: must use '.get' because mention extent may not be defined because of the use of a filter during the sample generation phase
                score = scores.get(extent_pair)
                if score is not None and score > score_threshold:
                    # Store the BEST preceding antecedent for each anaphora
                    heapq.heappush(list_best, (score, antecedent_mention))
            # Create link for n best antecedent if found
            for _, best_antecedent_mention in heapq.nlargest(self.n_best, list_best): #best_score
                coreference_partition.join(best_antecedent_mention.extent, subsequent_mention.extent)
        
        return coreference_partition



########## N BEST LINK DECODER ##########
class NBestLinkCoreferenceDecoder(_BaseNBestCoreferenceDecoder):
    """ Like the BestLinkCoreferenceDecoder, but, for a given mention, instead of creating a link 
    with only the best mentions among the other mentions, create a link with the N best mentions of 
    the other mentions.
    
    Arguments:
        n_best: int, the maximum number of 'best link' to create for the currently considered 
            mention in the primary loop of the 'decode' algorithm
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        n_best: int, the maximum number of 'best link' to create for the currently considered 
            mention in the primary loop of the 'decode' algorithm
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _decode(self, document, scores, scores_are_proba=False):
        """ Interprets the input score values, corresponding to pairs of mentions of the input document, 
        in order to predict a coreference partition to associate to this document.
        
        Args:
            document: a Document instance whose 'mentions' attribute must not be None
            scores: a (mention1 extent, mention2 extent) => float" map
            scores_are_proba: boolean, whether or not the binary classification scores should be 
                interpreted as proba values (float between 0 and 1), or not.

        Returns:
            a CoreferencePartition instance 
        """
        # Check input
        score_threshold = 0.5 if scores_are_proba else 0.0
        mentions = document.mentions
        coreference_partition = CoreferencePartition(mention_extents=(m.extent for m in mentions))
        
        # Enumerate all mentions
        for m1 in mentions:
            list_best = []
            for m2 in mentions:
                if m1 is m2:
                    continue
                # Get score
                score = scores.get(get_mention_pair_extent_pair(m1, m2))
                if score is not None and score > score_threshold:
                    # Store the BEST link for each mention
                    heapq.heappush(list_best, (score, m2))
            # Create n best links
            for _, best_link in heapq.nlargest(self.n_best, list_best):
                coreference_partition.join(best_link.extent, m1.extent)
        
        return coreference_partition



########## PERMUTATION DECODER ##########
class PermutationCoreferenceDecoder(_SimpleMentionPairCoreferenceDecoderMixIn, 
                                    _BaseMentionPairCoreferenceDecoder):
    """ Use the Munkres algorithm to decide whoch mentions to put in the same coreference entities.
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    def _decode(self, document, scores, scores_are_proba=False):
        """ Interprets the input score values, corresponding to pairs of mentions of the input document, 
        in order to predict a coreference partition to associate to this document.
        
        Args:
            document: a Document instance whose 'mentions' attribute must not be None
            scores: a (mention1 extent, mention2 extent) => float" map
            scores_are_proba: boolean, whether or not the binary classification scores should be 
                interpreted as proba values (float between 0 and 1), or not.

        Returns:
            a CoreferencePartition instance 
        """
        # Check input
        score_threshold = 0.5 if scores_are_proba else 0.0
        mentions = document.mentions
        coreference_partition = CoreferencePartition(mention_extents=(m.extent for m in mentions))
        
        if mentions:
            # Resolve using Munkres (output equivalent to a permutation)
            ## Build the Munkres input score matrix
            n = len(mentions)
            weights = [[0 for j in range(n)] for i in range(n)]
            for i in range(n):
                antecedent_mention = mentions[i]
                for j in range(i,n):
                    subsequent_mention = mentions[j]
                    extent_pair = get_mention_pair_extent_pair(antecedent_mention, subsequent_mention)
                    score = scores.get(extent_pair)
                    if score is not None and score > score_threshold:
                        weights[i][j] = weights[j][i] = -score # neg since we minimize
            ## Apply Munkres algorithm, and use its output to decide which mentions to link together
            indexes = MUNKRES.compute(weights)
            for (i,j) in indexes:
                antecedent_mention = mentions[i]
                subsequent_mention = mentions[j]
                coreference_partition.join(antecedent_mention.extent, subsequent_mention.extent)
        
        return coreference_partition



########## HAC DECODER ##########
POSSIBLE_CLUSTERING_TYPE_VALUES = ("group_average", "single_link", "complete_link")
clustering_type_attribute_data = ("clustering_type",
                                  (_create_simple_get_parameter_value_from_configuration("clustering_type", postprocess_fct=str),
                                   _create_simple_set_parameter_value_in_configuration("clustering_type")
                                   )
                                  )
class HACCoreferenceDecoder(_SimpleMentionPairCoreferenceDecoderMixIn, _BaseMentionPairCoreferenceDecoder):
    """ Hierarchical Agglomerative Clustering decoding.
    
    Arguments:
        clustering_type: string, either 'group_average', 'single_link', or 'complete_link', the 
            clustering type to use within the 'decode' algorithm
        
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        clustering_type: string, either 'group_average', 'single_link', or 'complete_link', the 
            clustering type to use within the 'decode' algorithm
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    """
    
    __INIT_PARAMETERS = OrderedDict((clustering_type_attribute_data,))
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = tuple(__INIT_PARAMETERS.keys()) +\
                                                _BaseMentionPairCoreferenceDecoder._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    _ATTRIBUTE_NAMES_FOR_REPR_DEFINITION = _BaseMentionPairCoreferenceDecoder._ATTRIBUTE_NAMES_FOR_REPR_DEFINITION+\
                                            tuple(__INIT_PARAMETERS.keys())
    
    def __init__(self, clustering_type, configuration=None):
        # Check input values
        clustering_type = clustering_type.lower()
        if clustering_type not in POSSIBLE_CLUSTERING_TYPE_VALUES:
            message = "Incorrect 'clustering_type' input value '{}', possible values are: {}."
            message = message.format(clustering_type, self.POSSIBLE_CLUSTERING_TYPE_VALUES)
            raise ValueError(message)
        # Init parent
        super().__init__(configuration=configuration)
        # Assign attribute values
        self.clustering_type = clustering_type
        # All value that can be input into the '__init__' function must be recorded in the configuration
        self._set_configuration_values_from_instance(self.__INIT_PARAMETERS, self)
    
    @classmethod
    def _extract_args_kwargs_from_configuration(cls, configuration):
        """ Extract *args and **kwargs suitable to be used to instantiate this class, from the input, 
        assumed proper, configuration.
        
        Args:
            configuration: a mapping which can be parsed to extract args and kwargs values suitable 
                for the initialization of an instance of this class

        Returns:
            a (args; kwargs) pair, where 'args is a tuple, and 'kwargs' is a dict
        """
        args, kwargs = super()._extract_args_kwargs_from_configuration(configuration)
        clustering_type, = cls._get_initialization_parameters_values_from_configuration(cls.__INIT_PARAMETERS, configuration)
        args = args + (clustering_type,)
        return args, kwargs
    
    @classmethod
    def define_configuration(cls, clustering_type="group_average"):
        """ Create a configuration object, which can then be used to create a specific instance of 
        this class, using the 'from_configuration' class method.
        
        Cf the documentation of the class of know which argument(s) to use.
        
        Returns:
            a Mapping instance
        """
        configuration = super().define_configuration()
        configuration["clustering_type"] = clustering_type
        return configuration
    
    def _decode(self, document, scores, scores_are_proba=False):
        """ Interprets the input score values, corresponding to pairs of mentions of the input document, 
        in order to predict a coreference partition to associate to this document.
        
        Args:
            document: a Document instance whose 'mentions' attribute must not be None
            scores: a (mention1 extent, mention2 extent) => float" map
            scores_are_proba: boolean, whether or not the binary classification scores should be 
                interpreted as proba values (float between 0 and 1), or not.

        Returns:
            a CoreferencePartition instance  
        """
        mentions = document.mentions
        coreference_partition = CoreferencePartition(mention_extents=(m.extent for m in mentions))
        # scores[antecedent_mention,subsequent_mention] = score (POS_CLASS > 0, NEG_CLASS < 0)
        update_similarity_scores_fct = getattr(self, "_{:s}_similarity_scores_update".format(self.clustering_type))
        # construct score matrix based for positive class
        similarity_scores = {}
        # Enumerate mentions from first to last (not considering the first, assuming it is anaphoric) # FIXME: is that the correct interpretation?
        for j in range(1,len(mentions)):
            subsequent_mention = mentions[j]
            # Enumerate mentions from last to first possible
            for i in range(j-1,-1,-1):
                antecedent_mention = mentions[i]
                a_m_extent = antecedent_mention.extent
                s_m_extent = subsequent_mention.extent
                score = scores[(a_m_extent,s_m_extent)]
                similarity_scores[(a_m_extent,s_m_extent)] = similarity_scores[(s_m_extent,a_m_extent)] = score 
        # Normalize scores, interpret them as similarity scores between the mentions of a pair
        scores = tuple(zip(*tuple(similarity_scores.items())))[1]
        max_score = max(scores)
        min_score = min(scores)
        den = max_score - min_score
        for pair, score in similarity_scores.items():
            similarity_scores[pair] = (score - min_score) / den
        score_threshold = 0.5
        # Build the coreference partition
        mentions_nb = len(mentions)
        active_clusters = dict(zip((m.extent for m in mentions), (1 for i in range(mentions_nb))))
        for _ in range(mentions_nb-1):
            # Find the pair of mention having the highest score (<=> the most similar pair of clusters):
            # find argmax of filtered-by-remaining-activated_mention scores
            (extent1, extent2), highest_score = sorted(((p,s) for (p,s) in similarity_scores.items()\
                                                          if p[0] in active_clusters and p[1] in active_clusters), 
                                                         key=lambda t: t[1], reverse=True)[0] # FIXME: does it really do what it should? Why this '[0]' at the end?
            if highest_score < score_threshold:
                break
            # Merge
            coreference_partition.join(extent1, extent2)
            # Update similarity scores
            for mention in mentions:
                extent = mention.extent
                if extent != extent1 and extent != extent2:
                    new_score = update_similarity_scores_fct(similarity_scores, extent1, extent2, extent)
                    similarity_scores[(extent1,extent)] = similarity_scores[(extent,extent1)] = new_score
            # Deactivate the second mention
            active_clusters.pop(extent2)
        
        return coreference_partition
    
    @staticmethod
    def _group_average_similarity_scores_update(similarity_scores, extent1, extent2, extent):
        """ Compute the average of the similarity scores respectively associated to the '(extent1, extent)' 
        and the '(extent2, extent)' pairs.
        
        Args:
            similarity_scores: a (mention1 extent, mention2 extent) => float" map
            extent1: extent of first mention to use for computation
            extent2: extent of first mention to use for computation
            extent: extent of the mention being tested

        Returns:
            float
        """
        return (similarity_scores[extent1,extent] + similarity_scores[extent2,extent]) / 2.0
    
    @staticmethod
    def _single_link_similarity_scores_update(similarity_scores, extent1, extent2, extent):
        """ Compute the maximum of the similarity scores respectively associated to the '(extent1, extent)' 
        and the '(extent2, extent)' pairs.
        
        Args:
            similarity_scores: a (mention1 extent, mention2 extent) => float" map
            extent1: extent of first mention to use for computation
            extent2: extent of first mention to use for computation
            extent: extent of the mention being tested

        Returns:
            float
        """
        return max(similarity_scores[extent1,extent], similarity_scores[extent2,extent])
    
    @staticmethod
    def _complete_link_similarity_scores_update(similarity_scores, extent1, extent2, extent):
        """ Compute the minimum of the similarity scores respectively associated to the '(extent1, extent)' 
        and the '(extent2, extent)' pairs.
        
        Args:
            similarity_scores: a (mention1 extent, mention2 extent) => float" map
            extent1: extent of first mention to use for computation
            extent2: extent of first mention to use for computation
            extent: extent of the mention being tested

        Returns:
            float
        """
        return min(similarity_scores[extent1,extent], similarity_scores[extent2,extent])

