# -*- coding: utf-8 -*-

"""
Defines classes used to create a CoreferencePartition instance from the analysis of predictions made for 
Sample instances

The aim of a Decode class is to provide a 'decode' method which knows how to interpret ML predictions 
scores into information about the coreference partition over the mentions of a document.

Decoder classes inherit from the 'ConfigurationmixIn' class, and as such their instances can be 
characterized by their configuration / an instance of those class can be created from such a 
configuration.
Cf 'cortex.tools.configuration_mixin'.
"""

"""
Available submodules
--------------------
base
    Defines base decoder class

pairs
    Defines decoder classes that work by decoding scores associated to pairs of mentions

mst
    Defines decoder classes that work by decoding scores by modeling them as graph weighed edges, and 
    computing min/maximum spanning trees from such a graph

API:
----
create_decoder
    Function used to create a Decoder instance, from specifying the name of the Decoder class, and 
    potentially some initialization parameters

generate_decoder_factory_configuration
    Function used to create a specific Decoder class instance's factory configuration, from specifying 
    the name of the Decoder class, and potentially some initialization parameters

create_decoder_from_factory_configuration
    Function used to create a Decoder instance, from specifying its factory configuration
"""

__all__ = ["ClosestFirstCoreferenceDecoder", 
           "AggressiveMergeCoreferenceDecoder", 
           "OracleAggressiveMergeCoreferenceDecoder", 
           "BestFirstCoreferenceDecoder", 
           "BestNextCoreferenceDecoder", 
           "BestLinkCoreferenceDecoder", 
           "MixedFirstCoreferenceDecoder", 
           "NBestFirstCoreferenceDecoder", 
           "NBestLinkCoreferenceDecoder", 
           "PermutationCoreferenceDecoder", 
           "HACCoreferenceDecoder", 
           "ExtendedBestFirstCoreferenceDecoder", 
           "ConstrainedBestFirstCoreferenceDecoder", 
           "ConstrainedExtendedBestFirstCoreferenceDecoder", 
           "MSTPrimCoreferenceDecoder", 
           "MSTKruskalCoreferenceDecoder", 
           "HybridBestFirstMSTCoreferenceDecoder",
           "create_decoder",
           "create_decoder_from_factory_configuration",
           "generate_decoder_factory_configuration",
           ]


from collections import OrderedDict

from .pairs import (ClosestFirstCoreferenceDecoder, AggressiveMergeCoreferenceDecoder, 
                    OracleAggressiveMergeCoreferenceDecoder, BestFirstCoreferenceDecoder, 
                    BestNextCoreferenceDecoder, BestLinkCoreferenceDecoder, 
                    MixedFirstCoreferenceDecoder, NBestFirstCoreferenceDecoder, 
                    NBestLinkCoreferenceDecoder, PermutationCoreferenceDecoder, 
                    HACCoreferenceDecoder, ExtendedBestFirstCoreferenceDecoder, 
                    ConstrainedBestFirstCoreferenceDecoder, 
                    ConstrainedExtendedBestFirstCoreferenceDecoder, 
                    )
from .mst import (MSTPrimCoreferenceDecoder, MSTKruskalCoreferenceDecoder, 
                                        HybridBestFirstMSTCoreferenceDecoder)
from cortex.tools import (define_check_item_name_function, define_create_function, 
                          define_generate_factory_configuration_function, 
                          define_create_from_factory_configuration_function,
                          )


decoder_names_collection_and_decoder_class_pairs = ((("closestfirst", ClosestFirstCoreferenceDecoder.__name__), ClosestFirstCoreferenceDecoder),
                                                    (("aggressivemerge", AggressiveMergeCoreferenceDecoder.__name__), AggressiveMergeCoreferenceDecoder),
                                                    (("oracleaggressivemerge", OracleAggressiveMergeCoreferenceDecoder.__name__), OracleAggressiveMergeCoreferenceDecoder), 
                                                    (("bestfirst", BestFirstCoreferenceDecoder.__name__), BestFirstCoreferenceDecoder),
                                                    (("bestnext", BestNextCoreferenceDecoder.__name__), BestNextCoreferenceDecoder),
                                                    (("bestlink", BestLinkCoreferenceDecoder.__name__), BestLinkCoreferenceDecoder),
                                                    (("mixedfirst", MixedFirstCoreferenceDecoder.__name__), MixedFirstCoreferenceDecoder),
                                                    (("nbestfirst", NBestFirstCoreferenceDecoder.__name__), NBestFirstCoreferenceDecoder),
                                                    (("nbestlink", NBestLinkCoreferenceDecoder.__name__), NBestLinkCoreferenceDecoder),
                                                    (("permutation", PermutationCoreferenceDecoder.__name__), PermutationCoreferenceDecoder),
                                                    (("hac", HACCoreferenceDecoder.__name__), HACCoreferenceDecoder),
                                                    (("extendedbestfirst", ExtendedBestFirstCoreferenceDecoder.__name__), ExtendedBestFirstCoreferenceDecoder),
                                                    (("constrainedbestfirst", ConstrainedBestFirstCoreferenceDecoder.__name__), ConstrainedBestFirstCoreferenceDecoder), 
                                                    (("constrainedextendedbestfirst", ConstrainedExtendedBestFirstCoreferenceDecoder.__name__), ConstrainedExtendedBestFirstCoreferenceDecoder), 
                                                    (("mstprim", MSTPrimCoreferenceDecoder.__name__), MSTPrimCoreferenceDecoder), 
                                                    (("mstkruskal", MSTKruskalCoreferenceDecoder.__name__), MSTKruskalCoreferenceDecoder), 
                                                    (("hybridbestfirstmst", HybridBestFirstMSTCoreferenceDecoder.__name__), HybridBestFirstMSTCoreferenceDecoder), 
                                                    )
decoder_name2decoder_class = OrderedDict((name, class_) for names, class_ in decoder_names_collection_and_decoder_class_pairs for name in names)


# High level functions
_check_decoder_name = define_check_item_name_function(decoder_name2decoder_class, "decoder")

create_decoder = define_create_function(decoder_name2decoder_class, "decoder")

create_decoder_from_factory_configuration =\
 define_create_from_factory_configuration_function(decoder_name2decoder_class, "decoder")

generate_decoder_factory_configuration =\
 define_generate_factory_configuration_function(decoder_name2decoder_class, "decoder")

