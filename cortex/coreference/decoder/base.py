# -*- coding: utf-8 -*-

"""
Defines decoder base class
"""

__all__ = ["_CoreferenceDecoder",]

import abc

from cortex.utils import EqualityMixIn
from cortex.tools import ConfigurationMixIn

class _CoreferenceDecoder(ConfigurationMixIn, EqualityMixIn, metaclass=abc.ABCMeta):
    """ Decoder base class
    
    Arguments:
        configuration: a Mapping instance; or None; info about the class instance that 
            the user wants to keep around; values of fields corresponding to the attributes of the instance 
            that can be parametrized through the '__init__' method will be replaced by the value needed 
            to reflect the attribute's respective actual value after initialization of the instance
    
    Attributes:
        NAME: the name of the class, which shall be used to unequivocally identify it among its peers
        
        configuration: a Mapping instance; info about the class instance: values of fields correspond 
            to the values of the corresponding attributes of the instance, and generally correspond to 
            parameters used to create a class instance
        
        factory_configuration: a Mapping instance; contains info about the class instance; contains 
            a 'name' field, whose value the instance's 'NAME' attribute value, and a 'config' field, 
            whose value is the instance's 'configuration' attribute value
    
    Examples:
        >>> coreference_partition = decoder.decode(prediction_scores)
    """
    
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = ("configuration",)
    
    @abc.abstractmethod
    def decode(self, data):
        pass
