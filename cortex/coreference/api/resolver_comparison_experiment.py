# -*- coding: utf-8 -*-

"""
Define a class used to implement a robust 'train resolvers => evaluate trained resolvers => display results' pipeline.
"""

__all__ = ["ResolverComparisonExperiment",
           "ExistingResolverComparisonExperimentError",
           ]

import logging
import os
from cityhash import CityHash128
from collections import defaultdict, OrderedDict
import pickle
import glob
import re
import shutil
import itertools

from .evaluate import normalize_evaluate_resolvers_configuration, generate_evaluation_results
from ..resolver import create_resolver_from_factory_configuration, load_resolver
from .vectorization_cache import LocalVectorizationCache
from cortex.io import parse_corpus
from cortex.config import (get_language_dependent_object, get_experiments_folder_path, 
                           language as current_working_language, get_cache_folder_path)
from ..evaluation.conll2012_scorer import format_resolvers_evaluation_results
from cortex.learning.sample_features_vectorizer import get_normalized_vectorizer_factory_configuration
from cortex.tools import Mapping, deepcopy_mapping, save_mapping, load_mapping
from cortex.utils.display import get_display_progress_function
from cortex.utils import CoRTeXException




class ResolverComparisonExperiment(object):
    """ This class defines a protocol to train and evaluate resolvers from the input of their 
    respective factory configuration.
    
    It allows to store the trained resolvers and their respective evaluation result on the local 
    drive as they are trained / as they are evaluated, allowing the running of the experiment to be 
    robust to unexpected crashes or interruptions.
    
    Such an experiment class instance may correspond to an already existing experiment data, in which 
    case its results can be parsed anew. But they can also be erased to start another experiment anew, 
    if that is wished. 
    
    Creating an instance does not modify anything on the hard drive.
    
    After creating an instance, one can either initialize the experiment with its parameters by 
    calling the 'initialize_experiment' method, or load the experiment data from its local folder, 
    assuming it already existed, by calling the 'initialize_experiment_from_persisted_state' method.
    
    An experiment is identified by its 'ident' value (a string, used to create a local folder by 
    the same name).
    
    Calling the 'initialize_experiment' method wipes clean the folder containing the persisted state 
    of the experiment identified by the 'ident' string, unless the 'reset' parameter is set to False 
    (the default value).
    
    The experiment folder will be created in the folder located at 'experiments_folder_path'.
    If no value is given for 'experiments_folder_path' during the instantiation, the value read from 
    CoRTeX' configuration will be used instead.
    
    The resolver factory configurations to use to create and train the resolvers are specified via 
    'resolver_ident2resolver_factory_configuration' mapping when initializing the experiment 
    (cf example below).
    
    The resolvers are evaluated using the CONLL2012 original task's evaluation software, using the 
    "all" metric (cf cortex.coreference.evalation.conll2012_scorer'). Other parameters for the 
    evaluation are specified using an 'evaluation_configuration' when initializing the experiment 
    (cf the example below).
    
    The resolvers are trained and evaluated using documents corpora whose info is specified via a 
    'corpora_configuration' when initializing the experiment (cf the example below).
    
    
    Arguments:
        ident: string, ident of the experiment, for reference purposes; will also be used as 
               a name to create the subfolder where the experiment's data will be hosted
        
        document_preprocessing_version: string, or None; if not None, the use of caching for 
                                        vectorization of ml samples will be activated, and the value 
                                        of the string will be used to specify the 
                                        'document_preprocessing_version' parameter; which must be 
                                        identical from one use to another, but must be set so as to 
                                        remind the user that the caching should be used only as long 
                                        as the preprocessing of the documents and the characterization 
                                        of the mentions of the documents result in the same values
        
        experiments_folder_path: string, or None; path to the root folder where will be created 
                                 the subfolder where the experiment's data will be hosted; if None, 
                                 will use the value defined in the CoRTeX' configuration file
        
        verbose: boolean, whether or not to display log messages when loading the corpora of 
                 documents
    
    Attributes:
        ident: string, ident of the experiment, for reference purposes; will also be used as a name 
               to create the subfolder where the experiment's data will be hosted
        
        experiments_folder_path: string, or None; path to the root folder where will be created the 
                                 subfolder where the experiment's data will be hosted; if None, will 
                                 use the value defined in the CoRTeX' configuration file
        
        experiment_folder_path: path to the folder where the experiment's data will be hosted
        
        document_preprocessing_version: string, or None; if not None, the use of caching for 
                                        vectorization of ml samples will be activated, and the value 
                                        of the string will be used to specify the 
                                        'document_preprocessing_version' parameter; which must be 
                                        identical from one use to another, but must be set so as to 
                                        remind the user that the caching should be used only as long 
                                        as the preprocessing of the documents and the characterization 
                                        of the mentions of the documents result in the same values
        
        verbose: boolean, whether or not to display log messages when loading the corpora of documents
        
        remaining_resolver_ident_resolver_factory_configuration_pairs: collection of 
                                                                      (resolver ident, resolver factory configuration) 
                                                                      pairs, corresponding to the 
                                                                      resolvers that are yet to be 
                                                                      trained
        
        remaining_resolver_ident_resolver_to_evaluate_pairs: collection of (resolver ident, resolver) 
                                                             pairs, corresponding to the trained 
                                                             resolvers that are yet to be evaluated
        
        resolver_ident2evaluation_result: a "resolver_ident => evaluation_result" map, where 
                                          'evaluation_result' is the output of a call to the 
                                          CONLL2012Scorer's 'evaluate' method (cf 
                                          cortex.coreference.evaluation.conll2012_scorer.py), storing 
                                          the evaluation results corresponding the resolvers that 
                                          have already been trained and evaluated
        
        evaluation_configuration: mapping whose (field, value) pairs represents optional 
            parameters to the 'evaluate_...' functions (cf 'cortex.coreference.api.evaluate'); to use 
            when evaluating the trained resolvers
            Ex:
            
            | {"skip_singletons": False, "detect_mentions": True, "retrieve_individual_document_data": True}
            
        corpora_configuration: mapping specifying corpora data to use for training and evaluating 
            the resolvers
            Ex: 
            
            | {"train": {"corpus_file_path": "/path/to/train_corpus_file.txt", "document_root_folder_path": "/path/to/document/root/folder"},
            | "test": {"corpus_file_path": "/path/to/test_corpus_file.txt", "document_root_folder_path": "/path/to/document/root/folder"},
            | }
        
        experiment_configuration: mapping, built from combining all parameters and configurations 
            used to define the experiment; used to check whether or not two experiments are the same, 
            notably when trying to initialize an experiment in the case that data for an experiment with 
            the same ident already exists
        
        train_corpus: Corpus instance used for training resolvers, loaded using info described in 
            'corpora_configuration'; or None, if not loaded yet
        
        test_corpus: Corpus instance used for evaluating resolvers, loaded using info described in 
            'corpora_configuration'; or None, if not loaded yet
    
    
    Examples:
    
        >>> experiments_folder_path = "/home/user/cortex_experiments"
        >>> ident = "expe1"
        >>> resolver_comparison_experiment = ResolverComparisonExperimen(ident, document_preprocessing_version, 
        ...                                                             experiments_folder_path=experiments_folder_path)
        
        If one initializes an experiment from scratch:
        
        >>> resolver_ident2resolver_factory_configuration = {"resolver1": resolver_factory_configuration1,
        ...                                                 "resolver2": resolver_factory_configuration2}
        >>> evaluation_configuration = {"detect_mentions": False, "skip_singletons": False, "retrieve_individual_document_data": True}
        >>> corpora_configuration = {"train": {"corpus_file_path": "/path/to/train_corpus_file.txt", "document_root_folder_path": "/path/to/document/root/folder"},
        ...                         "test": {"corpus_file_path": "/path/to/test_corpus_file.txt", "document_root_folder_path": "/path/to/document/root/folder"},}
        >>> resolver_comparison_experiment.initialize_experiment(resolver_ident2resolver_factory_configuration, 
        ...                                                     evaluation_configuration, corpora_configuration)
        
        If one initializes from existing data:
        
        >>> resolver_comparison_experiment.initialize_experiment_from_persisted_state()
        
        If one initializes from scratch, and wants to erase a potentially already existing content:
        
        >>> resolver_comparison_experiment.initialize_experiment(resolver_ident2resolver_factory_configuration, 
        ...                                                     evaluation_configuration, corpora_configuration,
        ...                                                     reset=True)
        
        Once the experiment is initialized, you can run it, or print its results...
        
        >>> resolver_comparison_experiment.run()
        >>> resolver_comparison_experiment.print_currently_held_evaluation_results(sort_conll=True)
        
        ...or get the best resolver instance according to this experiment
        
        >>> (resolver_ident, resolver, evaluation_result) = resolver_comparison_experiment.get_current_best_resolver()
    """
    
    _EVALUATION_CONFIGURATION_FILE_NAME = "evaluation_configuration.txt"
    _CORPORA_CONFIGURATION_FILE_NAME = "corpora_configuration.txt"
    _RESOLVER_CONFIGURATION_FOLDER_NAME = "resolver_factory_configurations"
    _TRAINED_RESOLVER_FOLDER_NAME = "trained_resolvers"
    _EVALUATION_RESULTS_FOLDER_NAME = "evaluation_results"
    _EXPERIMENT_CONFIGURATION_FILE_NAME = "experiment_configuration.txt"
    
    def __init__(self, ident, document_preprocessing_version, experiments_folder_path=None, verbose=False):
        # Check input value
        if not ident:
            msg = "'ident' input value must be a non-empty string, not '{}'"
            msg = msg.format(ident)
            raise ValueError(msg)
        if experiments_folder_path is None:
            experiments_folder_path = get_experiments_folder_path()
        # Process input values
        experiment_folder_path = os.path.join(experiments_folder_path, ident)
        # Set attribute values
        self.ident = ident
        self.experiments_folder_path = experiments_folder_path
        self.experiment_folder_path = experiment_folder_path
        self.document_preprocessing_version = document_preprocessing_version
        self.verbose = verbose
        self._logger = logging.getLogger("ResolverComparisonExperiment")
    
    # Instance methods
    def initialize_experiment(self, resolver_ident2resolver_factory_configuration, 
                              evaluation_configuration, corpora_configuration, reset=False):
        """ Specifies parameters values detailing the experiment to run, that is to say, which resolver 
        factory configuration to use, to train on which corpus, to evaluate on which corpus with 
        which parameters.
        
        Will check whether or not the experiment has previously been initialized, and if yes, will 
        load the potentially existing data about trained / evaluated resolvers.
        
        If the data corresponds to another, different experiment, and input 'reset' parameter value 
        is True, then will wipe clean the folder, persist the experiment's configuration data in it; 
        else, if 'reset' value is False, will raise an Exception.
        
        Else, will create the experiment's data hosting folder, and will use it to persist the 
        experiment's configuration data in it.
        
        Args:
            resolver_ident2resolver_factory_configuration: map from 'resolver ident' to 
                'resolver factory configuration' (cf 'cortex.tools.configuration_mixin.ConfigurationMixIn')
                resolver configuration instances
            evaluation_configuration: mapping whose (field, value) pairs represents optional 
                parameters to the 'evaluate_...' functions (cf 'cortex.coreference.api.evaluate'); to use 
                when evaluating the trained resolvers. Cf the class documentation.
            corpora_configuration: mapping specifying corpora data to use for training and evaluating 
                the resolvers. Cf the class documentation.
            reset: boolean (default = False), whether or not to wipe out data of previous experiment 
                by the same ident if such data happens to exists. If result=False and data of a previous 
                experiment exists, and the configuration of the previous experiment is not identical to that 
                of the experiment that is being initialized, then an exception will be raised
        
        Raises:
            ExistingResolverComparisonExperimentError: if another experiment's data exists, that is 
                associated to the same ident value of this experiment, where the data of this experiment is 
                to be stored, and if 'reset' is False
        """
        # Check and process inputs
        normalized_configuration = normalize_evaluate_resolvers_configuration(evaluation_configuration)
        del normalized_configuration["metric"]
        evaluation_configuration = normalized_configuration
        
        # Synthetize the experiment configuration
        experiment_configuration =\
         self._create_configuration_from_initialize_experiment_inputs(resolver_ident2resolver_factory_configuration, 
                                                                      evaluation_configuration, 
                                                                      corpora_configuration)
        
        # Check whether or not an experiment with the same ident already exists
        previous_experiment_configuration = self._check_existence_experiment_configuration()
        if previous_experiment_configuration is not None:
            if Mapping.equality(previous_experiment_configuration, experiment_configuration):
                self.initialize_experiment_from_persisted_state()
                return
            
            if not reset:
                msg = "An experiment with the ident '{}' and with a different experiment "\
                      "configuration already exists in the experiments folder '{}'.\nEither use "\
                      "another ident, or force a reset using the the 'reset' parameter of the "\
                      "'initialize_experiment' method.\nThe current, in-memory experiment configuration "\
                      "is the following:\n{}\nThe one from the already existing experiment is the following:\n{}."
                msg = msg.format(self.ident, self.experiments_folder_path, experiment_configuration, previous_experiment_configuration)
                raise ExistingResolverComparisonExperimentError(msg)
        
        # Prepare the experiment to be run
        remaining_resolver_ident_resolver_factory_configuration_pairs = list(resolver_ident2resolver_factory_configuration.items())
        remaining_resolver_ident_resolver_to_evaluate_pairs = []
        resolver_ident2evaluation_result = OrderedDict()
        self._initialize_experiment(remaining_resolver_ident_resolver_factory_configuration_pairs, 
                                    remaining_resolver_ident_resolver_to_evaluate_pairs,
                                    resolver_ident2evaluation_result,
                                    evaluation_configuration, corpora_configuration, 
                                    experiment_configuration)
        # Clean the folder where the experiment data shall be persisted
        self._create_persisted_state_anew()
        # Save the experiment data base configurations data
        self._persist_base_configuration_data(resolver_ident2resolver_factory_configuration, 
                                              evaluation_configuration, corpora_configuration)
        return self
    
    def initialize_experiment_from_persisted_state(self):
        """ Loads the experiment configuration, and already computed data, from the already existing 
        data of a previous experiment by the same ident.
        
        Raises:
            ExperimentDataNotFound: if the experiment path made from the experiment ident a the 
                'experiments_folder_path' does not correspond to an existing local folder
        """
        experiment_folder_path = self.experiment_folder_path
        
        if not os.path.exists(experiment_folder_path):
            msg = "Nothing exists at the experiment location '{}', nothing to initialize the experiment with."
            msg = msg.format(experiment_folder_path)
            raise ExperimentDataNotFound(msg)
        elif not os.path.isdir(experiment_folder_path):
            msg = "The object located at '{}' is not a folder, cannot initialize the experiment."
            msg = msg.format(experiment_folder_path)
            raise ExperimentDataNotFound(msg)
        
        parsed_resolver_info_resolver_idents = set()
        
        # Parse the evaluation results that were already computed
        evaluation_results_folder_path = os.path.join(experiment_folder_path, self._EVALUATION_RESULTS_FOLDER_NAME)
        glob_pattern = os.path.join(evaluation_results_folder_path, "*_evaluation_result.pkl")
        resolver_ident2evaluation_result = {}
        for file_path in glob.glob(glob_pattern):
            file_name = os.path.basename(file_path)
            resolver_ident = re.match("(.*)_evaluation_result\.pkl", file_name).groups()[0]
            with open(file_path, "br") as f:
                evaluation_results = pickle.load(f)
            resolver_ident2evaluation_result[resolver_ident] = evaluation_results
        parsed_resolver_info_resolver_idents.update(resolver_ident2evaluation_result.keys())
        
        # Parse the resolvers that were already trained, but whose evaluation results were not already computed
        trained_resolver_folder_path = os.path.join(experiment_folder_path, self._TRAINED_RESOLVER_FOLDER_NAME)
        glob_pattern = os.path.join(trained_resolver_folder_path, "*.zip")
        remaining_resolver_ident_resolver_to_evaluate_pairs = []
        for archive_file_path in glob.glob(glob_pattern):
            archive_file_name = os.path.basename(archive_file_path)
            resolver_ident = re.match("(.*)\.zip", archive_file_name).groups()[0]
            if resolver_ident not in parsed_resolver_info_resolver_idents:
                resolver = load_resolver(archive_file_path)
                remaining_resolver_ident_resolver_to_evaluate_pairs.append((resolver_ident, resolver))
                parsed_resolver_info_resolver_idents.add(resolver_ident)
        
        # Parse the resolver configuration whose resolver was not trained yet
        resolver_factory_configurations_folder_path = os.path.join(experiment_folder_path, self._RESOLVER_CONFIGURATION_FOLDER_NAME)
        glob_pattern = os.path.join(resolver_factory_configurations_folder_path, "*.txt")
        remaining_resolver_ident_resolver_factory_configuration_pairs = []
        for resolver_factory_configuration_file_path in glob.glob(glob_pattern):
            file_name = os.path.basename(resolver_factory_configuration_file_path)
            resolver_ident = re.match("(.*)\.txt", file_name).groups()[0]
            if resolver_ident not in parsed_resolver_info_resolver_idents:
                resolver_factory_configuration = load_mapping(resolver_factory_configuration_file_path)
                remaining_resolver_ident_resolver_factory_configuration_pairs.append((resolver_ident, resolver_factory_configuration))
                parsed_resolver_info_resolver_idents.add(resolver_ident)
        
        # Parse the evaluation configuration
        evaluation_configuration_file_path = os.path.join(experiment_folder_path, self._EVALUATION_CONFIGURATION_FILE_NAME)
        evaluation_configuration = load_mapping(evaluation_configuration_file_path)
        # Persist the corpora configuration data
        corpora_configuration_file_path = os.path.join(experiment_folder_path, self._CORPORA_CONFIGURATION_FILE_NAME)
        corpora_configuration = load_mapping(corpora_configuration_file_path)
        # Parse the experiment configuration
        experiment_configuration = self._check_existence_experiment_configuration()
        
        # Finally, initialize the experiment with the parsed data
        self._initialize_experiment(remaining_resolver_ident_resolver_factory_configuration_pairs, 
                                    remaining_resolver_ident_resolver_to_evaluate_pairs,
                                    resolver_ident2evaluation_result,
                                    evaluation_configuration, corpora_configuration, experiment_configuration)
    
    def train_remaining_resolvers(self):
        """ Trains the resolver corresponding to the remaining resolver factory configurations whose 
        training had not yet been carried out.
        """
        # Fetch remaining resolver configuration to train
        remaining_resolver_ident_resolver_factory_configuration_pairs = self.remaining_resolver_ident_resolver_factory_configuration_pairs
        
        if remaining_resolver_ident_resolver_factory_configuration_pairs:
            # Load the corpus
            self._load_train_corpus()
            
            # Setup logging of progress info
            remaining_resolver_factory_configuration_to_train_nb = len(remaining_resolver_ident_resolver_factory_configuration_pairs)
            display_progress_function, current_count = self._create_display_progress_function(remaining_resolver_factory_configuration_to_train_nb, train=True)
            
            self._logger.debug("Organizing configuration of resolver to be trained...")
            # Separate the configuration between those that can be trained independently, and those that can be trained in a chained manner
            remaining_resolver_ident2factory_configuration = dict(remaining_resolver_ident_resolver_factory_configuration_pairs)
            (factory_configuration___resolver_ident2iteration_nb_pairs, 
             other_resolver_ident2factory_configuration) = self._separate_factory_configuration(remaining_resolver_ident2factory_configuration)
            
            # Detect whether or not there exist previously trained resolvers from which to resume a training chain
            ## Separate the configuration of all the resolvers from the experiment to detect those that can be trained in a chained manner
            total_remaining_resolver_ident2factory_configuration = self.experiment_configuration["resolver_ident2resolver_configuration"]
            total_remaining_resolver_ident2factory_configuration = dict((key, total_remaining_resolver_ident2factory_configuration[key])\
                                                                        for key in total_remaining_resolver_ident2factory_configuration
                                                                        )
            total_factory_configuration___resolver_ident2iteration_nb_pairs, _ =\
             self._separate_factory_configuration(total_remaining_resolver_ident2factory_configuration) #other_resolver_ident2factory_configuration
            ## For each group of configuration whose training can be chained, fetch the matching group defining all the experiment's resolver configurations
            ## Then, once the matching group is found, take the diff between the resolver idents of the latter, and the resolver ident of the former
            ## If the diff is not empty, keep the resolver ident belonging to the diff whose 'iteration nb' parameter value of its online classifier is the highest
            ## total_factory_configuration___resolver_ident2iteration_nb_pairs = list(total_factory_configuration___resolver_ident2iteration_nb_pairs)
            previous_resolver_ident___factory_configuration___resolver_ident2iteration_nb_triplets = []
            for factory_configuration, resolver_ident2iteration_nb_pairs in factory_configuration___resolver_ident2iteration_nb_pairs:
                previous_resolver_ident = None
                for index, (total_factory_configuration, total_resolver_ident2iteration_nb_pairs)\
                 in enumerate(total_factory_configuration___resolver_ident2iteration_nb_pairs):
                    if factory_configuration != total_factory_configuration:
                        continue
                    diff = set(total_resolver_ident2iteration_nb_pairs).difference(set(resolver_ident2iteration_nb_pairs))
                    if diff:
                        previous_resolver_ident = sorted(diff, key=lambda x: total_resolver_ident2iteration_nb_pairs[x], reverse=True)[0]
                    break
                total_factory_configuration___resolver_ident2iteration_nb_pairs.pop(index)
                datum = (previous_resolver_ident, factory_configuration, resolver_ident2iteration_nb_pairs)
                previous_resolver_ident___factory_configuration___resolver_ident2iteration_nb_triplets.append(datum)
            self._logger.debug("Finished organizing configuration of resolver to be trained.")
            
            # Carry out the training
            ## Train the resolvers by group of factory configurations for which only the 'iteration_nb' of the associated online classifier varies ("chain training")
            new_current_count = self._chain_training_of_online_classifier_resolvers(previous_resolver_ident___factory_configuration___resolver_ident2iteration_nb_triplets, 
                                                                                    display_progress_function, current_count=current_count)
            ## Train the other configurations ("independent training")
            other_resolver_ident_resolver_factory_configuration_pairs = list(other_resolver_ident2factory_configuration.items())
            self._independent_training_of_resolvers(other_resolver_ident_resolver_factory_configuration_pairs, 
                                                    display_progress_function, current_count=new_current_count)
    
    def evaluate_remaining_resolvers(self):
        """ Evaluates the remaining resolvers that had been already trained, but not yet evaluated.
        Must be called after training the remaining resolvers to train, in order to be useful / meaningful.
        """
        remaining_resolver_ident_resolver_to_evaluate_pairs = self.remaining_resolver_ident_resolver_to_evaluate_pairs
        
        if remaining_resolver_ident_resolver_to_evaluate_pairs:
            resolver_ident2resolver = dict(remaining_resolver_ident_resolver_to_evaluate_pairs)
            resolver_idents = [resolver_ident for resolver_ident, _ in remaining_resolver_ident_resolver_to_evaluate_pairs] #resolver
            resolver_ident2evaluation_result = self.resolver_ident2evaluation_result
            
            # Load the corpus
            self._load_test_corpus()
            test_corpus = self.test_corpus
            
            # Setup logging of progress info
            remaining_resolver_factory_configuration_to_evaluate_nb = len(remaining_resolver_ident_resolver_to_evaluate_pairs)
            display_progress_function, current_count = self._create_display_progress_function(remaining_resolver_factory_configuration_to_evaluate_nb, train=False)
            
            # Carry out the evaluation
            document_preprocessing_version = self.document_preprocessing_version
            for resolver_ident, evaluation_result in generate_evaluation_results(resolver_ident2resolver, 
                                                                                 test_corpus, 
                                                                                 document_preprocessing_identifier_cache_parameter=document_preprocessing_version, 
                                                                                 display_progress_function=display_progress_function,
                                                                                 current_count=current_count, 
                                                                                 **self._evaluation_kwargs):
                ## Immediately persist the evaluation result
                self._save_resolver_evaluation_result(resolver_ident, evaluation_result)
                
                ## Put the result in the appropriate map, and remove the resolver from the collection of remaining resolver to evaluate
                resolver_ident2evaluation_result[resolver_ident] = evaluation_result
                index = resolver_idents.index(resolver_ident)
                remaining_resolver_ident_resolver_to_evaluate_pairs.pop(index)
                resolver_idents.pop(index)
    
    def format_currently_held_evaluation_results(self, do_print=False, sort_conll=True):
        """ Prints and returns the formatted resolvers evaluation results.
        
        Args:
            sort_conll: boolean, or callable on a (metric_overall_result, document_ident_and_results_per_metric_name_pairs) 
                pair:
                    - if True; sort the resolver in order of decreasing 'CONLL score' value
                    - if False; sort the resolver in order of increasing 'resolver ident' value
                    - if it is a callable; assume that this is a function suitable for being passed 
                      as the 'key' parameter of the 'sorted' function

        Returns:
            a string, the formatted resolvers evaluation results string
        """
        report = self._format_currently_held_evaluation_results(sort_conll=sort_conll)
        if do_print:
            print(report)
        return report
    
    def print_currently_held_evaluation_results(self, sort_conll=True):
        return self.format_currently_held_evaluation_results(do_print=True, sort_conll=sort_conll)
    
    def get_current_best_resolver(self):
        """ Return data corresponding to the best trained resolver according to the current state of 
        the evaluation results.
        
        The 'best' here means 'highest CONLL f1 score value'.

        Returns:
            a (result ident, resolver, evaluation result) tuple
        """
        sorted_resolver_ident_evaluation_result_pairs = sorted(((resolver_ident, metric_overall_results["conll"]["f1"])\
                                                                for resolver_ident, (metric_overall_results, _) in self.resolver_ident2evaluation_result.items()),  #document_ident_and_results_per_metric_name_pairs
                                                                key=lambda x: x[1]
                                                                )
        resolver_ident, evaluation_result = sorted_resolver_ident_evaluation_result_pairs[-1]
        resolver = self._load_trained_resolver(resolver_ident)
        return resolver_ident, resolver, evaluation_result
    
    def run(self, do_print=False, sort_conll=True):
        """ Trains the remaining resolvers to be trained, then evaluate the remaining resolvers to 
        be evaluated, then return the formatted evaluation results string.
        
        Args:
            do_print: boolean, whether or not to print the formatted evaluation results once the 
                training and evaluation are done
            sort_conll: boolean, or callable on a (metric_overall_result, document_ident_and_results_per_metric_name_pairs) 
                pair, used when formatting the evaluation results:
                    - if True; sort the resolver in order of decreasing 'CONLL score' value
                    - if False; sort the resolver in order of increasing 'resolver ident' value
                    - if it is a callable; assume that this is a function suitable for being passed 
                      as the 'key' parameter of the 'sorted' function

        Returns:
            a string, the formatted resolvers evaluation results string
        """
        # Train and persist resolvers
        self._logger.info("Training remaining resolvers...")
        self.train_remaining_resolvers()
        self._logger.info("Finished training remaining resolvers.")
        
        # Get and persist evaluation results
        self._logger.info("Evaluating remaining trained resolvers...")
        self.evaluate_remaining_resolvers()
        self._logger.info("Finished evaluating remaining trained resolvers.")
        
        # Format, persist and display evaluation results
        self._logger.info("Formatting evaluation results...")
        formatted_results = self._format_currently_held_evaluation_results(sort_conll=sort_conll)
        self._logger.info("Finished formatting evaluation results.")
        if do_print:
            print(formatted_results)
        
        return formatted_results
    
    def _create_persisted_state_anew(self):
        """ Creates the folder architecture needed to host the experiment's data. Wipe potentially 
        previously existing folder located at the path specified by the 'experiment_folder_path' 
        attribute value.
        
        Raises:
            ValueError: if something that is not a folder already exists at the path specified by the 
                'experiment_folder_path' attribute value. 
        """
        experiment_folder_path = self.experiment_folder_path
        # Check potential prior existence, and remove the folder if that is the case
        if os.path.exists(experiment_folder_path):
            if not os.path.isdir(experiment_folder_path):
                msg = "Something is located at '{}' that is not a folder, cannot proceed."
                msg = msg.format(experiment_folder_path)
                raise ValueError(msg)
            else:
                shutil.rmtree(experiment_folder_path)
        # Create the experiment folder anew
        os.mkdir(experiment_folder_path)
        # Create the folders to host the resolver configurations, the trained resolvers, as well as the evaluation results
        for folder_name in (self._RESOLVER_CONFIGURATION_FOLDER_NAME, 
                            self._TRAINED_RESOLVER_FOLDER_NAME, 
                            self._EVALUATION_RESULTS_FOLDER_NAME):
            folder_path = os.path.join(experiment_folder_path, folder_name)
            os.mkdir(folder_path)
    
    def _persist_experiment_configuration_data(self):
        """ Persists the configuration, defining and distinctly identifying the experiment, inside 
        the experiment's folder. 
        """
        experiment_configuration_file_path = os.path.join(self.experiment_folder_path, 
                                                          self._EXPERIMENT_CONFIGURATION_FILE_NAME)
        save_mapping(self.experiment_configuration, experiment_configuration_file_path)
    
    def _check_existence_experiment_configuration(self):
        """ Checks whether or not data defining an experiment has been persisted in the folder 
        located at the path specified by the 'experiment_folder_path' attribute value.
        

        Returns:
            a Mapping instance representing the configuration of an already existing experiment, 
                or None if no such data was found
        """
        experiment_configuration = None
        experiment_configuration_file_path = os.path.join(self.experiment_folder_path, 
                                                          self._EXPERIMENT_CONFIGURATION_FILE_NAME)
        if os.path.isfile(experiment_configuration_file_path):
            experiment_configuration = load_mapping(experiment_configuration_file_path)
        return experiment_configuration
    
    def _persist_base_configuration_data(self, resolver_ident2resolver_factory_configuration, 
                                         evaluation_configuration, corpora_configuration):
        """ Persists, within the folder architecture associated to the experiment, the specific 
        configurations respectively associated to the resolvers to train and evaluate, to the 
        evaluation parameters, and to the corpora parameters. 
        """
        experiment_folder_path = self.experiment_folder_path
        # Persist the resolver configuration data
        resolver_factory_configurations_folder_path = os.path.join(experiment_folder_path, self._RESOLVER_CONFIGURATION_FOLDER_NAME)
        for resolver_ident, resolver_factory_configuration in resolver_ident2resolver_factory_configuration.items():
            file_name = "{}.txt".format(resolver_ident)
            file_path = os.path.join(resolver_factory_configurations_folder_path, file_name)
            save_mapping(resolver_factory_configuration, file_path)
        # Persist the evaluation configuration data
        evaluation_configuration_file_path = os.path.join(experiment_folder_path, self._EVALUATION_CONFIGURATION_FILE_NAME)
        save_mapping(evaluation_configuration, evaluation_configuration_file_path)
        # Persist the corpora configuration data
        corpora_configuration_file_path = os.path.join(experiment_folder_path, self._CORPORA_CONFIGURATION_FILE_NAME)
        save_mapping(corpora_configuration, corpora_configuration_file_path)
        # Persist the experiment configuration
        self._persist_experiment_configuration_data()
    
    def _create_configuration_from_initialize_experiment_inputs(self, resolver_ident2resolver_factory_configuration, 
                                                                evaluation_configuration, 
                                                                corpora_configuration):
        """ Creates the experiment configuration from the input of the resolvers factory configurations, 
        the evaluation configuration, and the corpora configuration.
        

        Returns:
            a Mapping instance, the experiment configuration corresponding to the input parameter 
            values
        """
        experiment_configuration = Mapping({"resolver_ident2resolver_configuration": resolver_ident2resolver_factory_configuration, 
                                            "evaluation_configuration": evaluation_configuration, 
                                            "corpora_configuration": corpora_configuration,
                                            })
        return experiment_configuration
    
    def _save_trained_resolver(self, resolver_ident, resolver):
        """ Saves, within the experiment's data hosting folder architecture, the trained state of a 
        resolver identified by the input ident value.
        
        Args:
            resolver_ident: string, value identifying the resolver whose state shall be persisted
            resolver: a _Resolver child class instance, whose state shall be persisted
        """
        experiment_folder_path = self.experiment_folder_path
        trained_resolver_folder_path = os.path.join(experiment_folder_path, self._TRAINED_RESOLVER_FOLDER_NAME)
        archive_file_name = "{}.zip".format(resolver_ident)
        resolver.save(trained_resolver_folder_path, archive_file_name)
    
    def _save_resolver_evaluation_result(self, resolver_ident, evaluation_result):
        """ Saves, within the experiment's data hosting folder architecture, the evaluation results of 
        a resolver identified by the input ident value.
        
        Args:
            resolver_ident: string, value identifying the resolver whose state shall be persisted
            evaluation_result: the output of a call to the CONLL2012Scorer's 'evaluate' method 
                (cf cortex.coreference.evaluation.conll2012_scorer.py); associated to the input resolver 
                ident value
        """
        experiment_folder_path = self.experiment_folder_path
        evaluation_results_folder_path = os.path.join(experiment_folder_path, self._EVALUATION_RESULTS_FOLDER_NAME)
        file_name = "{}_evaluation_result.pkl".format(resolver_ident)
        file_path = os.path.join(evaluation_results_folder_path, file_name)
        with open(file_path, "bw") as f:
            pickle.dump(evaluation_result, f)
    
    def _load_trained_resolver(self, resolver_ident):
        """ Loads the resolver's state associated to the input ident, and assumed to exist within the 
        experiment's data hosting folder architecture, into a corresponding _Resolver child class 
        instance.
        Assume that such persistence data associated to the input 'resolver_ident' value indeed exists.
        
        Args:
            resolver_ident: a string, value identifying the resolver whose state shall be loaded

        Returns:
            a _Resolver child class instance
        """
        experiment_folder_path = self.experiment_folder_path
        trained_resolver_folder_path = os.path.join(experiment_folder_path, self._TRAINED_RESOLVER_FOLDER_NAME)
        archive_file_name = "{}.zip".format(resolver_ident)
        archive_file_path = os.path.join(trained_resolver_folder_path, archive_file_name)
        resolver = load_resolver(archive_file_path)
        return resolver
    
    def _extract_evaluation_kwargs_from_configuration(self, configuration):
        """ Extracts evaluation parameter values from the input configuration.
        
        Args:
            configuration: a mapping

        Returns:
            a python dict, suitable for being passaed as a 'kwarg' parameter to the evaluation 
            function to be used to evaluate a resolver
        """
        evaluation_kwargs = {"metric": "all"} # Since we want to be able to compute the 'CONLL' metric results, we need to compute 'all' metrics
        for parameter_name in ("skip_singletons", "retrieve_individual_document_data", "detect_mentions"): #"metric", 
            if parameter_name in configuration:
                evaluation_kwargs[parameter_name] = configuration[parameter_name]
        return evaluation_kwargs
    
    def _initialize_experiment(self, remaining_resolver_ident_resolver_factory_configuration_pairs, 
                               remaining_resolver_ident_resolver_to_evaluate_pairs,
                               resolver_ident2evaluation_result,
                               evaluation_configuration, corpora_configuration, experiment_configuration):
        """ Sets the attribute values of the instance so that its state corresponds to a proper 
        initialized experiment.
        
        Args:
            remaining_resolver_ident_resolver_factory_configuration_pairs: collection of 
                (resolver ident, resolver factory configuration) pairs, corresponding to the resolvers that 
                are yet to be trained
            remaining_resolver_ident_resolver_to_evaluate_pairs: collection of 
                (resolver ident, resolver) pairs, corresponding to the trained resolvers that are yet to be 
                evaluated
            resolver_ident2evaluation_result: a "resolver_ident => evaluation_result" map, where 
                'evaluation_result' is the output of a call to the CONLL2012Scorer's 'evaluate' method
                (cf cortex.coreference.evaluation.conll2012_scorer.py), storing the evaluation results 
                corresponding the resolvers that have already been trained and evaluated
            evaluation_configuration: mapping whose (field, value) pairs represents optional 
                parameters to the 'evaluate_...' functions (cf 'cortex.coreference.api.evaluate'); to use 
                when evaluating the trained resolvers
            corpora_configuration: mapping specifying corpora data to use for training and evaluating 
                the resolvers
            experiment_configuration: mapping, built from combining all parameters and configurations 
                used to define the experiment; used to check whether or not two experiments are the same, 
                notably when trying to initialize an experiment in the case that data for an experiment with 
                the same ident already exists
        """
        self.remaining_resolver_ident_resolver_factory_configuration_pairs = remaining_resolver_ident_resolver_factory_configuration_pairs
        self.remaining_resolver_ident_resolver_to_evaluate_pairs = remaining_resolver_ident_resolver_to_evaluate_pairs
        self.resolver_ident2evaluation_result = resolver_ident2evaluation_result
        self.evaluation_configuration = evaluation_configuration
        self.corpora_configuration = corpora_configuration
        self.experiment_configuration = experiment_configuration
        self._evaluation_kwargs = self._extract_evaluation_kwargs_from_configuration(self.evaluation_configuration)
        self.train_corpus = None
        self.test_corpus = None
        self._is_initialized = True
    
    def _load_corpus(self, corpus_type):
        """ Loads, and set as value the corresponding attribute, the corpus used for training or for 
        evaluation, according to the input type.
        
        Args:
            corpus_type: string, either 'train' or 'test', corresponding to the type of corpus 
                to load
        """
        attribute_name = "{}_corpus".format(corpus_type)
        if getattr(self, attribute_name) is None:
            corpus_file_path, documents_root_folder_path = _check_corpus_options(self.corpora_configuration, corpus_type)
            corpus_name = "{}_corpus='{}'".format(corpus_type, os.path.basename(corpus_file_path))
            self._logger.info("Loading the {} corpus...".format(corpus_type))
            corpus = parse_corpus(corpus_file_path, root_path=documents_root_folder_path, 
                                  name=corpus_name, verbose=self.verbose)
            self._logger.info("Finished loading the {} corpus.".format(corpus_type))
            setattr(self, attribute_name, corpus)
    
    def _load_train_corpus(self):
        """ Loads, and set as value the corresponding attribute, the corpus used for training. """
        self._load_corpus("train")
    
    def _load_test_corpus(self):
        """ Loads, and set as value the corresponding attribute, the corpus used for evaluation. """
        self._load_corpus("test")
    
    def _independent_training_of_resolvers(self, remaining_resolver_ident_resolver_factory_configuration_pairs, 
                                           display_progress_function, current_count=0):
        """ Carries out the training of resolvers corresponding to the input collection of resolver 
        factory configuration, by iterating over this collection.
        Persist the trained resolvers as soon as their training is over, and then add them to the 
        queue of resolvers to evaluate. 
        
        Args:
            remaining_resolver_ident_resolver_factory_configuration_pairs: collection of 
                (resolver ident, resolver factory configuration) pairs, corresponding to the resolvers that 
                are yet to be trained
            display_progress_function: callable, function that takes an iteration index as input, 
                and may carry out an action to display a message to inform about the corresponding progress 
                state
            current_count: non-negative integer, the value used to initialize the progress index 
                value with

        Returns:
            an int, the last value of the progress index, corresponding to the last resolver 
            trained during the call to this method
        """
        train_corpus = self.train_corpus
        
        # Setup the future use of vectorization caching
        language_parameter_version = get_language_dependent_object("LANGUAGE_PARAMETER_VERSION")
        cortex_cache_folder_path = get_cache_folder_path()
        document_preprocessing_version = self.document_preprocessing_version
        persist_as_svmlight_like = False
        
        # Group the resolver configuration to train by common (sample_type, vectorizer_configuration) pairs
        # That way we will be able to efficiently use the caching operations
        resolver_ident2vectorizer_ident_tuple = {}
        vectorizer_ident_tuple2resolvers = defaultdict(list)
        for resolver_ident, resolver_factory_configuration in remaining_resolver_ident_resolver_factory_configuration_pairs:
            resolver = create_resolver_from_factory_configuration(resolver_factory_configuration)
            normalized_vectorizer_configuration = get_normalized_vectorizer_factory_configuration(resolver.configuration["model_interface"]["config"]["sample_features_vectorizer"])
            vectorizer_hash_string = Mapping.get_base_hash_string(normalized_vectorizer_configuration)
            vectorizer_hash = str(CityHash128(vectorizer_hash_string))
            sample_type = resolver.sample_type
            vectorizer_ident_tuple = (vectorizer_hash, sample_type)
            vectorizer_ident_tuple2resolvers[vectorizer_ident_tuple].append((resolver_ident, resolver))
            resolver_ident2vectorizer_ident_tuple[resolver_ident] = vectorizer_ident_tuple
        remaining_resolver_ident_resolver_pairs = list(itertools.chain(*tuple(vectorizer_ident_tuple2resolvers.values())))
        
        # Train resolvers
        remaining_resolver_ident_resolver_to_evaluate_pairs = self.remaining_resolver_ident_resolver_to_evaluate_pairs
        previous_vectorizer_ident_tuple = None
        vectorization_cache = None
        new_current_count = current_count
        while remaining_resolver_ident_resolver_pairs:
            display_progress_function(new_current_count)
            resolver_ident, resolver = remaining_resolver_ident_resolver_pairs.pop()
            
            ## Setup the use of vectorization caching
            vectorizer_configuration = resolver.configuration["model_interface"]["config"]["sample_features_vectorizer"]
            sample_type = resolver.sample_type
            current_vectorizer_ident_tuple = resolver_ident2vectorizer_ident_tuple[resolver_ident]
            if previous_vectorizer_ident_tuple != current_vectorizer_ident_tuple:
                vectorization_cache = LocalVectorizationCache(cortex_cache_folder_path, 
                                                              current_working_language, 
                                                              document_preprocessing_version, 
                                                              language_parameter_version, 
                                                              sample_type, 
                                                              vectorizer_configuration, 
                                                              train_corpus.documents,
                                                              persist_as_svmlight_like=persist_as_svmlight_like)
                vectorization_cache.load_caches()
                previous_vectorizer_ident_tuple = current_vectorizer_ident_tuple
            
            ## Couple the resolver and the vectorization cache
            resolver.toggle_vectorization_cache(vectorization_cache=vectorization_cache)
            
            ## Train the resolver
            self._logger.info("Training the '{}' resolver...".format(resolver_ident))
            resolver.train(train_corpus)
            self._logger.info("Finished training the '{}' resolver.".format(resolver_ident))
        
            ## Immediately persist the trained resolver
            self._save_trained_resolver(resolver_ident, resolver)
            
            ## Uncouple the resolver and the vectorization cache
            resolver.toggle_vectorization_cache(vectorization_cache=None)
            
            ## Update the cache if necessary
            vectorization_cache.persist_updated_caches()
            
            remaining_resolver_ident_resolver_to_evaluate_pairs.append((resolver_ident, resolver)) 
            new_current_count += 1
        
        return new_current_count
    
    def _chain_training_of_online_classifier_resolvers(self, previous_resolver_ident___factory_configuration___resolver_ident2iteration_nb_triplets, 
                                                      display_progress_function, current_count=0):
        """ Carries out the training of resolvers corresponding to the input collection of resolver 
        factory configurations, by iterating over this collection. The resolver factory configurations 
        are bundled together, when possible. A bundle of resolver factory configurations is a set of 
        resolver factory configuration where the only difference between the configurations is the 
        number of iteration to carry out when trained the corresponding resolvers, for resolver using 
        online classifiers. It is useful to process those resolver together, since that allows to 
        reuse a previously trained resolver as initial value for the training of a resolver whose 
        iteration number is higher.
        Persist the trained resolvers as soon as their training is over, and then add them to the 
        queue of resolvers to evaluate. 
        
        Args:
            previous_resolver_ident___factory_configuration___resolver_ident2iteration_nb_triplets: 
                collection of (previous_resolver_ident, resolver_factory_configuration, resolver_ident2iteration_nb) 
                triplets, corresponding to resolvers that are yet to be trained, where:
                    - 'previous_resolver_ident' is the ident of the already trained resolver whose state is 
                        appropriate to use as the initial trained state of a resolver whose training is yet to be 
                        done; can be None, in which case the training of the first resolver of the bundle will 
                        be carried out from scratch
                    - 'resolver_factory_configuration' is the factory configuration to use to create the first 
                        resolver to train, in case that the 'revious_resolver_ident' is None
                    - 'resolver_ident2iteration_nb' is a "resolver_ident => iterations_nb" map, corresponding to 
                        resolvers that are yet to be trained, and whose initial trained state can be initialized by 
                        using the finished trained state of a similar, previously trained resolver
            display_progress_function: callable, function that takes an iteration index as input, 
                and may carry out an action to display a message to inform about the corresponding progress 
                state
            current_count: non-negative integer, the value used to initialize the progress index 
                value with

        Returns:
            an int, the last value of the progress index, corresponding to the last resolver 
            trained during the call to this method
        """
        train_corpus = self.train_corpus
        remaining_resolver_ident_resolver_to_evaluate_pairs = self.remaining_resolver_ident_resolver_to_evaluate_pairs
        
        # Setup the future use of vectorization caching
        language_parameter_version = get_language_dependent_object("LANGUAGE_PARAMETER_VERSION")
        cortex_cache_folder_path = get_cache_folder_path()
        document_preprocessing_version = self.document_preprocessing_version
        persist_as_svmlight_like = False
        
        new_current_count = current_count
        for (previous_resolver_ident, resolver_factory_configuration, 
             resolver_ident2iteration_nb) in previous_resolver_ident___factory_configuration___resolver_ident2iteration_nb_triplets:
            
            # Setup the cache
            resolver_ = create_resolver_from_factory_configuration(resolver_factory_configuration)
            sample_type = resolver_.sample_type
            vectorizer_configuration = resolver_.configuration["model_interface"]["config"]["sample_features_vectorizer"]
            vectorization_cache = LocalVectorizationCache(cortex_cache_folder_path, 
                                                          current_working_language, 
                                                          document_preprocessing_version, 
                                                          language_parameter_version, 
                                                          sample_type, 
                                                          vectorizer_configuration, 
                                                          train_corpus.documents,
                                                          persist_as_svmlight_like=persist_as_svmlight_like)
            
            # Load cache in memory
            vectorization_cache.load_caches()
            
            # Train resolvers
            load_resolver_from_resolver_ident_fct = self._load_trained_resolver
            for resolver_ident, trained_resolver in self._generate_efficiently_trained_online_interface_based_resolver(resolver_factory_configuration, 
                                                                                                                       resolver_ident2iteration_nb, 
                                                                                                                       previous_resolver_ident, 
                                                                                                                       train_corpus, 
                                                                                                                       load_resolver_from_resolver_ident_fct,
                                                                                                                       vectorization_cache, 
                                                                                                                       display_progress_function,
                                                                                                                       new_current_count,
                                                                                                                       self._logger):
                
                ## Immediately persist the trained resolver
                self._save_trained_resolver(resolver_ident, trained_resolver)
                
                ## Update the cache if necessary
                vectorization_cache.persist_updated_caches()
                
                remaining_resolver_ident_resolver_to_evaluate_pairs.append((resolver_ident, trained_resolver)) 
                new_current_count += 1
        
        return new_current_count
    
    def _create_display_progress_function(self, remaining_resolver_to_process_nb, train=True):
        """ Creates a 'display progress function', based on the total number of resolver to train and 
        evaluate in the frame of the experiment.
        
        Args:
            remaining_resolver_to_process_nb: non-negative int, number of remaining resolvers to 
                process, and whose processing progress the output function shall be about
            train: boolean, whether or not the output function is about progress regarding the 
                training of resolvers (True), or about progress regarding the evaluation of resolvers (False)

        Returns:
            a (display_progress_function, current_count) pair, 
            where:
                - 'display_progress_function' is a function that takes an iteration index as input, and may 
                    carry out an action to display a message to inform about the corresponding progress state
                - 'current_count' is the initial value that the index measuring the progress of the process 
                    must take, in order to be consistent with the input 'remaining_resolver_to_process_nb' value
        """
        total_resolver_factory_configuration_nb = len(self.experiment_configuration["resolver_ident2resolver_configuration"])
        maximum_count = total_resolver_factory_configuration_nb
        current_count = total_resolver_factory_configuration_nb - remaining_resolver_to_process_nb
        ls = str(len(str(maximum_count)))
        verb_s = "train" if train else "evaluate"
        personalized_progress_message = "Processed {percentage:6.2f}% of total number of resolvers to "+verb_s+" ({current:"+ls+"d} out of {total:"+ls+"d})"
        percentage_interval = 5
        display_progress_function = get_display_progress_function(self._logger, 
                                                                  maximum_count=maximum_count, 
                                                                  percentage_interval=percentage_interval, 
                                                                  personalized_progress_message=personalized_progress_message, 
                                                                  logging_level="info")
        return display_progress_function, current_count
    
    def _format_evaluation_configuration_parameters(self):
        """ Creates a string describing the evaluation parameters used by the experiment.
        
        Returns:
            a string
        """
        evaluation_kwargs = self._evaluation_kwargs
        test_corpus_file_path, test_documents_root_folder_path = _check_corpus_options(self.corpora_configuration, "test")
        strings_array = ["Evaluation parameters:", 
                         "    Test corpus:", 
                         "        File path: {}".format(test_corpus_file_path), 
                         "        Document root folder path: {}".format(test_documents_root_folder_path),
                         ] + ["    {}: {}".format(key, value) for key, value in sorted(evaluation_kwargs.items(), key=lambda x: x[0])]
        report = "\n".join(strings_array)
        return report
    
    def _format_currently_held_evaluation_results(self, sort_conll=True):
        """ Creates a string describing the evaluation overall results obtained for the resolvers 
        trained up until now in the frame of the experiment, notably so as to be able for a human 
        to easily compare the resolvers' individual performance levels.
        
        Args:
            sort_conll: boolean, or callable on a "evaluation_result" object, where 
                'evaluation_result' is the output of a call to the CONLL2012Scorer's 'evaluate' method
                (cf cortex.coreference.evaluation.conll2012_scorer.py)
                    - if True; sort the resolver in order of decreasing 'CONLL score' value (assume 
                        that the evaluation results in the input map contain enough data to compute 
                        that score)
                    - if False; sort the resolver in order of increasing 'resolver ident' value
                    - if it is a callable; assume that this is a function suitable for being passed 
                        as the 'key' parameter of the 'sorted' function

        Returns:
            a string, representing a table displaying the evaluation results associated to each 
            resolver trained up until now in the frame of the experiment
        """
        formatted_results = format_resolvers_evaluation_results(self.resolver_ident2evaluation_result, 
                                                                sort_conll=sort_conll)
        evaluation_parameter_report = self._format_evaluation_configuration_parameters()
        strings_array = [evaluation_parameter_report, 
                         "Evaluation results:",
                         formatted_results,
                         ]
        report = "\n".join(strings_array)
        return report    
    
    # Static methods
    @staticmethod
    def _separate_factory_configuration(resolver_ident2factory_configuration):
        """ Organizes the input resolver factory configuration into a pair object.
        
        This pair of objects is:
            - on one hand, bundles of factory configuration for resolver using an underlying online 
                classifier, and for which only the 'iteratioos_nb' parameter value of this classifier varies 
                from one factory configuration to another
            - on the other hand, the remaining resolver factory configuration, which cannot be bundled 
                together
        
        Args:
            resolver_ident2factory_configuration: a "resolver_ident => resolver_factory_configuration" map

        Returns:
            a (factory_configuration___resolver_ident2iteration_nb_pairs, remaining_resolver_ident2factory_configuration) 
            pair, where:
                - 'factory_configuration___resolver_ident2iteration_nb_pairs' is a collection of 
                  (factory_configuration, resolver_ident2iteration_nb) pairs, each representing a 
                  bundle, where:
                      - 'factory_configuration' is the resolver factory configuration that is shared by the 
                        resolvers of the bundle, but whose subconfiguration which is associated to the underlying 
                        online classifier does not possess an 'iterations_nb' field
                     - 'resolver_ident2iteration_nb' is a "resolver_ident => iterations_nb" map, specifying 
                        which 'iterations_nb' parameter value is associated to which resolver ident, within the 
                    bundle
                - 'remaining_resolver_ident2factory_configuration' is a "resolver_ident => resolver_factory_configuration" map, 
                  corresponding tor the resolver factory configurations that could no be bundled together
        """
        from cortex.learning.ml_model.classifier import online_classifiers_map
        
        # First, separate the configuration to be able to focus on those that do refer to an online classifier
        resolver_ident2non_online_classifier_factory_configuration = {}
        resolver_ident2online_classifier_factory_configuration = {}
        for resolver_ident, factory_configuration in resolver_ident2factory_configuration.items():
            if "model_interface" in factory_configuration["config"] and factory_configuration["config"]["model_interface"]["config"]["classifier"]["name"] in online_classifiers_map:
                resolver_ident2online_classifier_factory_configuration[resolver_ident] = factory_configuration
            else:
                resolver_ident2non_online_classifier_factory_configuration[resolver_ident] = factory_configuration
                    
        
        # Next, put together the configuration using an online classifier which varies only with the 'iteration_nb' value of the configuration of the classifier
        # That means we need a way to compare configuration without taking into account the 'iteration_nb'
        online_classifier_factory_configuration_without_iteration_nb_collections = []
        for resolver_ident, factory_configuration in resolver_ident2online_classifier_factory_configuration.items():
            # Create the factory configuration without the 'iteration_nb' parameter
            factory_configuration_without_iteration_nb = deepcopy_mapping(factory_configuration)
            iteration_nb = factory_configuration_without_iteration_nb["config"]["model_interface"]["config"]["classifier"]["config"]["iteration_nb"]
            del factory_configuration_without_iteration_nb["config"]["model_interface"]["config"]["classifier"]["config"]["iteration_nb"]
            
            datum = (resolver_ident, iteration_nb, factory_configuration_without_iteration_nb)
            
            # Else, iterate over each collection until one is found where there is equality of configuration. If none is found, there create a new one
            found = False
            for online_classifier_factory_configuration_without_iteration_nb_collection in online_classifier_factory_configuration_without_iteration_nb_collections:
                _, _, representative_factory_configuration = online_classifier_factory_configuration_without_iteration_nb_collection[0] #representative_resolver_ident, representative_iteration_nb
                if factory_configuration_without_iteration_nb == representative_factory_configuration:
                    online_classifier_factory_configuration_without_iteration_nb_collection.append(datum)
                    found = True
            if not found:
                online_classifier_factory_configuration_without_iteration_nb_collection = [datum]
                online_classifier_factory_configuration_without_iteration_nb_collections.append(online_classifier_factory_configuration_without_iteration_nb_collection)
            
        # Finally, package the discriminated configurations so as to be easily usable
        factory_configuration___resolver_ident2iteration_nb_pairs = []
        remaining_resolver_ident2factory_configuration = dict(resolver_ident2non_online_classifier_factory_configuration)
        for online_classifier_factory_configuration_without_iteration_nb_collection in online_classifier_factory_configuration_without_iteration_nb_collections:
            # If the current collection contains only one configuration, we put it back with the others, non-online classifier configuration, to be trained independently from others
            if len(online_classifier_factory_configuration_without_iteration_nb_collection) == 1:
                resolver_ident, _, _ = online_classifier_factory_configuration_without_iteration_nb_collection[0] #iteration_nb, factory_configuration_without_iteration_nb
                remaining_resolver_ident2factory_configuration[resolver_ident] = resolver_ident2factory_configuration[resolver_ident]
            else:
                resolver_ident2iteration_nb = {resolver_ident: iteration_nb\
                                                     for resolver_ident, iteration_nb, _ in online_classifier_factory_configuration_without_iteration_nb_collection #factory_configuration
                                                    }
                _, _, factory_configuration = online_classifier_factory_configuration_without_iteration_nb_collection[0] #resolver_ident, iteration_nb
                factory_configuration___resolver_ident2iteration_nb_pairs.append((factory_configuration, resolver_ident2iteration_nb))
        
        return factory_configuration___resolver_ident2iteration_nb_pairs, remaining_resolver_ident2factory_configuration
    
    @staticmethod
    def _generate_efficiently_trained_online_interface_based_resolver(resolver_factory_configuration, 
                                                                      resolver_ident2iteration_nb, 
                                                                      previous_resolver_ident, 
                                                                      train_corpus, 
                                                                      load_resolver_from_resolver_ident_fct,
                                                                      vectorization_cache, 
                                                                      display_progress_function,
                                                                      current_count,
                                                                      logger):
        """ Creates a generator over trained resolvers, resolver that uses an underlying online 
        classification model, and whose factory configuration is the same, except for the 'iteration_nb' 
        parameter of their respective underlying online classifier. 
        
        The resolver are sequentially trained, ordered by increasing 'iteration_nb' parameter value, 
        and thus beginning with the other with the smallest 'iteration_nb' parameter value, so that 
        each trained resolver's state can be used to initialize the trained state of the following 
        resolver to train.
        
        It is possible to provide the ident of a resolver that has been previously trained in the 
        frame of the experiment, to use as the trained resolver initialization state, in which case 
        it is assumed that its configuration is consistent with the input resolver factory 
        configuration value..
        
        It is assumed that the input vectorization cache object is adapted to the documents that make up 
        the training corpus, as well as the vectorizer and the sample type associated the the resolver 
        factory configuration.
        
        Args:
            resolver_factory_configuration: factory configuration whose value is shared by the 
                resolver to train using this method, except for the 'iteration_nb' parameter of the 
                underlying online classifier
            resolver_ident2iteration_nb: a "resolver_ident => iterations_nb" map, used to 
                determine which complete resolver factory configuration to associate to which resolver ident, 
                when building and training the resolvers
            previous_resolver_ident: string, ident of the already trained resolver whose state 
                is appropriate to use as the initial trained state of a resolver whose trained is yet to be 
                finished; can be None, in which case the training of the first resolver of will be carried 
                out from scratch
            train_corpus: Corpus instance, to use as the training
            load_resolver_from_resolver_ident_fct: callable, function that takes a resolver ident 
                as an input, and output the corresponding, loaded _Resolver child class instance
            vectorization_cache: a _BaseVectorizationCache child class instance, the cache to use 
                when training the resolvers, assumed to be consistent with the input training corpus, as well 
                as with the vectorizer defined in the resolvers' shared configuration
            display_progress_function: callable, function that takes an iteration index as input, 
                and may carry out an action to display a message to inform about the corresponding progress 
                state 
            current_count: non negative int, the initial value that the index measuring the 
                progress of the process must take. Should be consistent with the input 
                'display_progress_function' value
            logger: logging.Logger instance to use when logging messages
        
        Yields:
            a (resolver_ident, current_resolver) pair, 
            where:
                - 'current_resolver' is a trained resolver
                - 'resol_verident' is a string representing the trained resolver ident
        """
        previous_iteration_nb = 0
        new_current_count = current_count
        for resolver_ident, iteration_nb in sorted(resolver_ident2iteration_nb.items(), key=lambda x: x[1]):
            display_progress_function(new_current_count)
            
            # Create the resolver from a modified configuration
            diff = iteration_nb - previous_iteration_nb
            if previous_resolver_ident is None: # TODO: make it so this function be robust to crashes, by being able to use a previously trained resolver as a starting point, if it is possible?
                current_resolver_factory_configuration = deepcopy_mapping(resolver_factory_configuration)
                current_resolver_factory_configuration["config"]["model_interface"]["config"]["classifier"]["config"]["iteration_nb"] = diff
                current_resolver_factory_configuration["config"]["model_interface"]["config"]["classifier"]["config"]["warm_start"] = True
                current_resolver = create_resolver_from_factory_configuration(current_resolver_factory_configuration)
            else:
                current_resolver = load_resolver_from_resolver_ident_fct(previous_resolver_ident)
                current_resolver._model_interface._classifier.iteration_nb = diff
                current_resolver._model_interface._classifier.warm_start = True
                current_resolver.configuration["model_interface"]["config"]["classifier"]["config"]["iteration_nb"] = diff
                current_resolver.configuration["model_interface"]["config"]["classifier"]["config"]["warm_start"] = True
            
            ## Couple the resolver and the vectorization cache
            current_resolver.toggle_vectorization_cache(vectorization_cache=vectorization_cache)
            
            ## Train the resolver
            logger.info("Training the '{}' resolver...".format(resolver_ident))
            current_resolver.train(train_corpus)
            logger.info("Finished training the '{}' resolver.".format(resolver_ident))
            
            ## Uncouple the resolver and the vectorization cache
            current_resolver.toggle_vectorization_cache(vectorization_cache=None)
            
            ## Yield the trained resolver
            ### Modify back the configuration of the resolver before saving it
            current_resolver.configuration["model_interface"]["config"]["classifier"]["config"]["iteration_nb"] = iteration_nb
            ### Yield
            yield resolver_ident, current_resolver
            
            previous_resolver_ident = resolver_ident
            previous_iteration_nb = iteration_nb
            new_current_count += 1


# Utilities
def _check_corpus_options(configuration, corpus_type):
    """ Fetches the data needed to load the corpus whose type is input from the input corpora 
    configuration.
    
    Args:
        configuration: a mapping
        corpus_type: a string, must correspond to the name of a field of the input configuration

    Returns:
        a (corpus_file_path, documents_root_folder_path) pair
    """
    sub_config = configuration[corpus_type]
    corpus_file_path = sub_config["corpus_file_path"]
    documents_root_folder_path = sub_config["documents_root_folder_path"]
    if corpus_file_path == "":
        message = "Incorrect 'corpus_file_path' field value ('{}') for '{}' corpus configuration: must be a path to a file."
        message = message.format(corpus_file_path, corpus_type)
        raise ValueError(message)
    if documents_root_folder_path == "":
        message = "Incorrect 'documents_root_folder_path' field value ('{}') for '{}' corpus configuration: must be a path to a folder."
        message = message.format(documents_root_folder_path, corpus_type)
        raise ValueError(message)
    return corpus_file_path, documents_root_folder_path


# Exceptions
class ExperimentDataNotFound(CoRTeXException):
    """ Exception to be raised if a specific experiment 's hosting data folder path  does not 
    correspond to an existing local folder.
    """
    pass

class ExistingResolverComparisonExperimentError(CoRTeXException):
    """ Exception to be raised if, for the experiment corresponding to a current 
    ResolverComparisonExperiment instance, another experiment's data exists, that is associated to 
    the same ident value the current experiment's.    
    """
    pass
