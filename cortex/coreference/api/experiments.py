# -*- coding: utf-8 -*-

"""
Define utilities to represent simple experiments that can be made on resolvers.
"""

__all__ = ["carry_out_resolver_grid_search", 
           "carry_out_efficient_online_interface_based_resolver_comparison_experiment",
           "carry_out_efficient_online_interface_based_resolver_comparison_single_experiment", 
           "create_kfold_corpora_files_folder", "parse_kfold_corpora",
           ]

import logging
import os
from cityhash import CityHash128
from collections import OrderedDict
import re
from sklearn.model_selection import KFold
from collections import defaultdict
import glob

from cortex.io.corpus_io import parse_corpus_file, write_corpus_file
from ..resolver import _check_resolver_name
from .resolver_comparison_experiment import ResolverComparisonExperiment
from cortex.tools import  deepcopy_mapping, Mapping
from cortex.utils import get_parameter_grids
from cortex.utils.io import TemporaryDirectory



def create_kfold_corpora_files_folder(corpus_file_path, output_folder_path, n_splits=3, 
                                      shuffle=False, random_state=None):
    """ Creates files data representing a kfold split from input corpora data, useful for carrying 
    cross-validation-like machine learning tasks
    
    Args:
        corpus_file_path: path to a corpus file defining a collection of document references, 
                          collection that will be split to generate the kfold split
        output_folder_path: path to folder where subfolders containing the kfold splits subcorpora 
                            files will be written
        n_split: int (default = 3), the value of 'k' in 'kfold', i.e. the number of 
                 (subtraining corpus, subtest corpus) pairs to generate
        shuffle: boolean, whether or not to shuffle the document references collection before 
                 splitting it
        random_state: the random state (can be an int) to use to initialize the RNG used when 
                      shuffling the data, if 'shuffle' is True
    """
    (format_, params), document_paths = parse_corpus_file(corpus_file_path)
    kfolds = KFold(n_splits=n_splits, shuffle=shuffle, random_state=random_state)
    indices_collection = tuple(kfolds.split(document_paths))
    
    if not os.path.exists(output_folder_path):
        os.makedirs(output_folder_path)
    
    for fold_index, (train_indices, test_indices) in enumerate(indices_collection):
        for corpus_type, indices in (("train", train_indices), 
                                     ("test", test_indices),
                                    ):
            corpus_file_name = "CV{}_{}.txt".format(fold_index+1, corpus_type)
            subcorpus_file_path = os.path.join(output_folder_path, corpus_file_name)
            
            subcorpus_document_paths = tuple(document_paths[i] for i in indices)
            write_corpus_file(subcorpus_document_paths, subcorpus_file_path, format_, corpus_parse_options=params)



def parse_kfold_corpora(kfold_folder_path, documents_root_folder_path):
    """ Parses a folder defining a kfold split over a corpus into the corresponding collection of 
    corpora configuration
    
    Args:
        kfold_folder_path: path to folder where subfolders containing the kfold splits subcorpora 
                           files are located
        documents_root_folder_path: path to which the document paths described in the input corpus 
                                    file are relative

    Returns:
        the collection of corpora configuration corresponding to the parsed (train subcorpus, test subcorpus) 
        pairs (each one can be a suitable input for initializing a 'ResolverComparisonExperiment' 
        instance)
    """
    fold_index2corpora_configuration = defaultdict(lambda: Mapping({"train": Mapping(), "test": Mapping()}))
    
    glob_pattern = os.path.join(kfold_folder_path, "CV*.txt")
    file_paths = tuple(glob.glob(glob_pattern, recursive=False))
    fold_index2file_names = defaultdict(set)
    for file_path in file_paths:
        file_name = os.path.basename(file_path)
        fold_index_str, corpus_type = re.match("CV(\d+)_(train|test).txt", file_name).groups()
        fold_index = int(fold_index_str)
        fold_index2file_names[fold_index].add(corpus_type)
        fold_index2corpora_configuration[fold_index][corpus_type]["corpus_file_path"] = file_path
        fold_index2corpora_configuration[fold_index][corpus_type]["documents_root_folder_path"] = documents_root_folder_path
    
    cv_indices = set(fold_index for fold_index in fold_index2file_names.keys())
    expected_cv_indices = set(range(1, len(cv_indices)+1))
    if cv_indices != expected_cv_indices:
        msg = "Incorrect set of cv indices '{}', must be '{}'"
        msg = msg.format(sorted(cv_indices), sorted(expected_cv_indices))
        raise ValueError(msg)
    expected_file_names = set(["train", "test"])
    for fold_index, file_names in fold_index2file_names.items():
        if set(file_names) != expected_file_names:
            msg = "Incorrect corpus file types set '{}' associated to CV n°{}, must be equal to {}."
            msg = msg.format(sorted(file_names), sorted(expected_file_names))
    
    _, cv_corpora_configurations = tuple(zip(*sorted(fold_index2corpora_configuration.items())))
    
    return cv_corpora_configurations




def carry_out_efficient_online_interface_based_resolver_comparison_single_experiment(resolver_factory_configuration, 
                                                                                     iteration_nbs, 
                                                                                     corpora_configuration, 
                                                                                     evaluation_configuration, 
                                                                                     document_preprocessing_version, 
                                                                                     experiment_folder_path=None):
    """ Trains resolvers built from the input configuration, each time with a different 'iteration_nb' 
    parameter value for the 'ml_model' part of the configuration, and then evaluate them.
    This function's aim is to carry out an experiment to observe the impact of the 'iterations_nb' 
    parameter value on the performance of the resolver, all other parameter values being otherwise 
    equal.
    
    Args:
        resolver_factory_configuration: a factory configuration for a resolver whose 
                                        'model_interface' attribute is an OnlineInterface subclass 
                                        instance (notably, which relies on the use of an 'iteration_nb' 
                                        parameter)
        iteration_nbs: collection of 'iteration_nb' values
        corpora_configuration: mapping, needed parameter for launching an experiment (Cf the 
                               'ResolverComparisonExperiment' class)
        evaluation_configuration: mapping, needed parameter for launching an experiment (Cf the 
                                  'ResolverComparisonExperiment' class)
        document_preprocessing_version: string, needed parameter for using LocalVectorizationCache 
                                        (Cf the 'LocalVectorizationCache' class)
        experiment_folder_path: string, or None; parameter for resolver comparison experiment creation
                                (Cf the 'ResolverComparisonExperiment' class)

    Returns:
        a "iteration_nb => evaluation_result" map, where 'evaluation_result' is the output of 
        a call to the CONLL2012Scorer's 'evaluate' method (Cf cortex.coreference.evaluation.conll2012_scorer.py)
    """
    resolver_factory_configuration_iteration_nbs_pairs = [(resolver_factory_configuration, iteration_nbs)]
    iteration_nb2evaluation_result_maps =\
     carry_out_efficient_online_interface_based_resolver_comparison_experiment(resolver_factory_configuration_iteration_nbs_pairs, 
                                                                                corpora_configuration, 
                                                                                evaluation_configuration, 
                                                                                document_preprocessing_version, 
                                                                                experiment_folder_path=experiment_folder_path)
    iteration_nb2evaluation_result = iteration_nb2evaluation_result_maps[0]
    return iteration_nb2evaluation_result


def carry_out_efficient_online_interface_based_resolver_comparison_experiment(resolver_factory_configuration_iteration_nbs_pairs, 
                                                                              corpora_configuration, 
                                                                              evaluation_configuration, 
                                                                              document_preprocessing_version, 
                                                                              experiment_folder_path=None):
    """ Trains resolvers built from the input configuration, each time with a different 'iteration_nb' 
    parameter value for the 'ml_model' part of the configuration, and then evaluate them.
    
    This function's aim is to carry out an experiment to observe the impact of the 'iterations_nb' 
    parameter value on the performance of resolvers, all other parameter value being otherwise equal.
    
    This functions allows to carry out such experiments for several base resolver factory 
    configuration at once.
    
    Args:
        resolver_factory_configuration_iteration_nbs_pairs: a collection of (factory configuration; collection of 'iteration_nb' values) 
                                                            pairs, where the factory configurations 
                                                            are about resolvers whose 'model_interface' 
                                                            attribute is an OnlineInterface subclass 
                                                            instance
        corpora_configuration: mapping, needed parameter for launching an experiment (Cf the 
                              'ResolverComparisonExperiment' class)
        evaluation_configuration: mapping, needed parameter for launching an experiment (Cf the 
                                  'ResolverComparisonExperiment' class)
        document_preprocessing_version: string, needed parameter for using LocalVectorizationCache 
                                        (Cf the 'LocalVectorizationCache' class)
        experiment_folder_path: string, or None; parameter for resolver comparison experiment creation 
                                (Cf the 'ResolverComparisonExperiment' class)

    Returns:
        a collection of "iteration_nb => evaluation_result" maps, each element of which is 
        associated to the (resolver_factory_configuration, iteration_nbs) pair located at the same position, 
        in the input 'resolver_factory_configuration_iteration_nbs_pairs' collection; 
        where 'evaluation_result' is the output of a call to the CONLL2012Scorer's 'evaluate' method 
        (Cf cortex.coreference.evaluation.conll2012_scorer.py)
    """
    logger = logging.getLogger("carry_out_efficient_online_interface_based_resolver_comparison_experiment")
    
    # Define the resolver configuration that the experiment must handle
    resolver_ident2resolver_factory_configuration = OrderedDict()
    for i, (resolver_factory_configuration, iteration_nbs) in enumerate(resolver_factory_configuration_iteration_nbs_pairs):
        resolver_name = resolver_factory_configuration["name"]
        for iteration_nb in sorted(iteration_nbs):
            resolver_ident = "{}_{}_iteration_nb={}".format(resolver_name, i, iteration_nb)
            resolver_factory_configuration = deepcopy_mapping(resolver_factory_configuration)
            resolver_factory_configuration["config"]["model_interface"]["config"]["classifier"]["config"]["iteration_nb"] = iteration_nb
            resolver_ident2resolver_factory_configuration[resolver_ident] = resolver_factory_configuration
    
    # Define experiment name and experiments folder path
    if experiment_folder_path is None:
        experiment_ident = "online_interface_iteration_nbs_experiment"
        experiments_folder_path = None
    else:
        experiments_folder_path, experiment_ident = os.path.split(experiment_folder_path)
    
    # Carry out the experiment
    logger.info("Carrying out online classifier based comparison experiment...")
    resolver_ident2evaluation_result = _carry_out_experiment_in_folder(resolver_ident2resolver_factory_configuration, 
                                                                       experiment_ident, document_preprocessing_version, 
                                                                       evaluation_configuration, 
                                                                       corpora_configuration, 
                                                                       experiments_folder_path=experiments_folder_path)
    logger.info("Finished carrying out online classifier based comparison experiment.")
    
    index_2_iteration_nb2evaluation_result = defaultdict(dict)
    for resolver_ident, evaluation_result in resolver_ident2evaluation_result.items():
        _, index_str, iteration_nb_str = re.match("(.+)_(\d+)_iteration_nb=(\d+)", resolver_ident).groups() #resolver_name
        index = int(index_str)
        iteration_nb = int(iteration_nb_str)
        index_2_iteration_nb2evaluation_result[index][iteration_nb] = evaluation_result
    _, iteration_nb2evaluation_result_maps = tuple(zip(*sorted(index_2_iteration_nb2evaluation_result.items(), key=lambda x: x[0]))) #indices
    
    return iteration_nb2evaluation_result_maps



def carry_out_resolver_grid_search(resolver_name, parameter_name2parameter_possible_values, 
                                   corpora_configuration, evaluation_configuration, 
                                   document_preprocessing_version, 
                                   evaluation_result_scorer=None,
                                   experiment_folder_path=None):
    """ Carries out a grid-search for a specific resolver class.
    
    Args:
        resolver_name: string, name or pseudo-name of the resolver class for which to carry out 
                       a grid-search
        parameter_name2parameter_possible_values: dict of string to sequence, or sequence of such.
                                                  The parameter grid to explore, as a dictionary that 
                                                  is mapping resolver creation parameter names to 
                                                  sequences of allowed values.
        corpora_configuration: mapping, needed parameter for launching an experiment (Cf the 
                              'ResolverComparisonExperiment' class)
        evaluation_configuration: mapping, needed parameter for launching an experiment (Cf the 
                                  'ResolverComparisonExperiment' class)
        document_preprocessing_version: string, needed parameter for using LocalVectorizationCache 
                                        (Cf the 'LocalVectorizationCache' class)
        evaluation_result_scorer: callable from an 'evaluation result' to a score, or None; if None, 
                                  a callable returning the 'CONLL' scsore value will be used 
        experiment_folder_path: string, or None; parameter for resolver comparison experiment 
                                creation (Cf the 'ResolverComparisonExperiment' class)

    Returns:
        a collection of (resolver creation parameters; score; resolver factory configuration) tuples
    """
    logger = logging.getLogger("carry_out_resolver_grid_search")
    
    # Check input
    resolver_class = _check_resolver_name(resolver_name)
    if evaluation_result_scorer is None:
        evaluation_result_scorer = lambda evaluation_result: evaluation_result[0]["conll"]["f1"]
    
    # Prepare parameter configuration creation for carrying out a grid search
    parameter_name2parameter_possible_values_collection = parameter_name2parameter_possible_values
    if isinstance(parameter_name2parameter_possible_values, dict):
        parameter_name2parameter_possible_values_collection = [parameter_name2parameter_possible_values]
    factory_parameters_collection = get_parameter_grids(parameter_name2parameter_possible_values_collection)
    
    # Define the resolver configuration that the experiment will have to handle
    resolver_ident2factory_parameters = OrderedDict()
    resolver_ident2resolver_factory_configuration = OrderedDict()
    for i, factory_parameters in enumerate(factory_parameters_collection):
        resolver_factory_configuration = resolver_class.define_factory_configuration(**factory_parameters)
        string_to_hash = Mapping.get_base_hash_string(resolver_factory_configuration)
        hash_string = str(CityHash128(string_to_hash))
        resolver_ident = "m{}".format(hash_string)
        resolver_ident2factory_parameters[resolver_ident] = (i, factory_parameters)
        resolver_ident2resolver_factory_configuration[resolver_ident] = resolver_factory_configuration
    
    # Define experiment name and experiments folder path
    if experiment_folder_path is None:
        experiment_ident = "grid_search_experiment"
        experiments_folder_path = None
    else:
        experiments_folder_path, experiment_ident = os.path.split(experiment_folder_path)
    
    # Carry out the experiment
    logger.info("Carrying out '{}' resolver grid search experiment...".format(resolver_name))
    resolver_ident2evaluation_result = _carry_out_experiment_in_folder(resolver_ident2resolver_factory_configuration, 
                                                                       experiment_ident, document_preprocessing_version, 
                                                                       evaluation_configuration, 
                                                                       corpora_configuration, 
                                                                       experiments_folder_path=experiments_folder_path)
    logger.info("Finished carrying out '{}' resolver grid search experiment.".format(resolver_name))
    
    # Create output
    index2factory_parameters_evaluation_score_factory_configuration_triplets = {}
    for resolver_ident, evaluation_result in resolver_ident2evaluation_result.items():
        index, factory_parameters = resolver_ident2factory_parameters[resolver_ident]
        factory_configuration = resolver_ident2resolver_factory_configuration[resolver_ident]
        score = evaluation_result_scorer(evaluation_result)
        index2factory_parameters_evaluation_score_factory_configuration_triplets[index] = (factory_parameters, score, factory_configuration)
    #factory_parameters_evaluation_score_pairs = [pair for _, pair in sorted(index2factory_parameters_evaluation_score_pairs.items(), key=lambda x: x[0])]
    factory_parameters_evaluation_score_factory_configuration_triplets = sorted(index2factory_parameters_evaluation_score_factory_configuration_triplets.values(), 
                                                                                key=lambda x: x[1], reverse=True)
    
    return factory_parameters_evaluation_score_factory_configuration_triplets

def _carry_out_experiment_in_folder(resolver_ident2resolver_factory_configuration, experiment_ident, 
                                     document_preprocessing_version, evaluation_configuration, 
                                     corpora_configuration, experiments_folder_path=None):
    """ Carries out the resolver comparison experiment corresponding to the input parameters values, 
    with the possibility to choose which local folder should act as root folder for the subfolder 
    hosting the experiment's data.
    
    Args:
        resolver_ident2resolver_factory_configuration: a "resolver ident => resolver factory configuration" 
                                                       map (Cf the 'ConfigurationMixIn' class)
        experiment_ident: string, ident of the experiment, for reference purposes; will also be 
                          used as a name to create the subfolder where the experiment's data will be 
                          hosted
        document_preprocessing_version: string, or None; if not None, the use 
                                        of caching for vectorization of ml samples will be activated, 
                                        and the value of the string will be used to specify the 
                                        'document_preprocessing_version' parameter; which must be 
                                        identical from one use to another, but must be set so as to 
                                        remind the user that the caching should be used only as long 
                                        as the preprocessing of the documents and the characterization 
                                        of the mentions of the documents result in the same values
        evaluation_configuration: mapping whose (field, value) pairs represents optional parameters 
                                  to the 'evaluate_...' functions (Cf the ResolverComparisonExperiment' 
                                  class)
        corpora_configuration: mapping specifying corpora data to use for training and testing (Cf 
                               the ResolverComparisonExperiment' class)
        experiments_folder_path: string, or None; the path to the folder which will host the 
                                 experiment's data; if None, a temporary folder will be created and 
                                 used, and destroyed as soon as the experiment is over

    Returns:
        a "resolver_ident => evaluation_result" map, where 'evaluation_result' is the output of a 
        call to the CONLL2012Scorer's 'evaluate' method (Cf cortex.coreference.evaluation.conll2012_scorer.py)
    """
    _fct = lambda experiments_folder_path_: _carry_out_experiment(resolver_ident2resolver_factory_configuration, 
                                                                  experiments_folder_path_, experiment_ident, 
                                                                  document_preprocessing_version, 
                                                                  evaluation_configuration, corpora_configuration)
    if experiments_folder_path is None:
        with TemporaryDirectory(delete_upon_exit=True) as temp_dir:
            experiments_folder_path = temp_dir.temporary_folder_path
            resolver_ident2evaluation_result = _fct(experiments_folder_path)
    else:
        resolver_ident2evaluation_result = _fct(experiments_folder_path)
    return resolver_ident2evaluation_result

def _carry_out_experiment(resolver_ident2resolver_factory_configuration, experiments_folder_path_, 
                          experiment_ident, document_preprocessing_version, evaluation_configuration, 
                          corpora_configuration):
    """ Carries out the resolver comparison experiment corresponding to the input parameters values, 
    using the specified local folder should act as root folder for the subfolder hosting the 
    experiment's data.
    
    Args:
        resolver_ident2resolver_factory_configuration: map from 'resolver ident' to 
            'resolver factory configuration' (Cf the 'ConfigurationMixIn' class)
        experiments_folder_path: string, the path to the root folder where will be created the 
            subfolder which will host the experiment's data
        experiment_ident: string, ident of the experiment, for reference purposes; will also be 
            used as a name to create the subfolder where the experiment's data will be hosted
        document_preprocessing_version: string, or None; if not None, the use 
            of caching for vectorization of ml samples will be activated, and the value of the string 
            will be used to specify the 'document_preprocessing_version' parameter; which must be 
            identical from one use to another, but must be set so as to remind the user that the caching 
            should be used only as long as the preprocessing of the documents and the characterization 
            of the mentions of the documents result in the same values
        evaluation_configuration: mapping whose (field, value) pairs represents optional 
            parameters to the 'evaluate_...' functions (Cf the ResolverComparisonExperiment' class)
        corpora_configuration: mapping specifying corpora data to use for training and testing (Cf 
            the ResolverComparisonExperiment' class)

    Returns:
        a "resolver_ident => evaluation_result" map, where 'evaluation_result' is the output of a 
        call to the CONLL2012Scorer's 'evaluate' method (Cf cortex.coreference.evaluation.conll2012_scorer.py)
    """
    logger = logging.getLogger("_carry_out_experiment")
    
    # Create the experiment object
    verbose = True
    resolver_comparison_experiment = ResolverComparisonExperiment(experiment_ident, 
                                                                  document_preprocessing_version, 
                                                                  experiments_folder_path=experiments_folder_path_, 
                                                                  verbose=verbose)
    
    # Initialize the experiment
    resolver_comparison_experiment.initialize_experiment(resolver_ident2resolver_factory_configuration, 
                                                         evaluation_configuration, 
                                                         corpora_configuration, reset=False)
    
    # Train the resolvers
    logger.info("Training remaining resolvers...")
    resolver_comparison_experiment.train_remaining_resolvers()
    logger.info("Finished training remaining resolvers.")
    
    # Evaluate the resolvers, and get the results
    logger.info("Evaluating remaining trained resolvers...")
    resolver_comparison_experiment.evaluate_remaining_resolvers()
    logger.info("Finished evaluating remaining trained resolvers.")
    
    resolver_ident2evaluation_result = resolver_comparison_experiment.resolver_ident2evaluation_result
    return resolver_ident2evaluation_result
