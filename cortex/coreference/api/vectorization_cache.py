# -*- coding: utf-8 -*-

"""
Define a context manager class that is used to persist to and load from local folder cached 
vectorization data.
"""

__all__ = ["LocalVectorizationCache",
           ]

import logging
import os
from cityhash import CityHash128
from collections import defaultdict
import pickle

from cortex.learning.samples import POSSIBLE_SAMPLE_TYPES
from cortex.learning.sample_features_vectorizer import get_normalized_vectorizer_factory_configuration
from cortex.tools import Mapping, save_mapping
from cortex.utils.io import persist_to_svmlight_like_file, load_from_svmlight_like_file

class _BaseVectorizationCache(object):
    """ Base class for vectorization cache classes. """
    pass

class LocalVectorizationCache(_BaseVectorizationCache):
    """ The aim of this class is to provide an easy way to persist and load cached data pertaining 
    to the vectorization of ML samples created from a collection of Document instances.
    
    When this class is initialized, the existence of the folder architecture corresponding to the 
    input parameters will be checked, and the architecture will be created if it does not exist.
    
    When entering the chunck of code wrapped by this class, this class will try to load the 
    potentially existing cached data corresponding to the vectorized ML samples of the Document 
    instances contained in the collection passed as a parameter.
    
    When exiting, this class will check for the existence of new or updated cache, and will save 
    those.
    
    Within the chunck of code, the cached data can be accessed using the 'document_hash2cache' 
    attribute of an instance of this class. This data needs to be propagated to the objects that 
    needs to use / to compute ML sample vectorization data (such as a trainable resolver, through 
    the use of the 'toggle_vectorization_cache' method, for instance) in order for the caching to 
    occur. This class only deals with loading and saving the cached data, not the caching process 
    itself.
    
    If the first initialization parameter is None, nothing will happen: no loading, no saving.
    
    Arguments:
        cortex_cache_folder_path: string, or None; path to the root folder used for persisting 
            CoRTex cache data; if None, this class will do nothing: no loading cache, no saving cache
        
        language: the language of the Document instances, is used to specify which subtree of the 
            cache persistence folder architecture to use
        
        document_preprocessing_version: string, something to identify (from the point of view 
            of the user) the preprocessing that the input documents went through; this value must 
            be identical from one use to another when inputing identical documents / documents that went 
            through the same preprocessing, so that the caching system be activated for them. It must be 
            set by the user so as to remind the user that the caching should be used only as long as the 
            preprocessing of the documents and the characterization of the mentions of the documents 
            result in the same values
        
        language_parameter_version: string, identifier for the version of the cortex' language 
            package used, notably allows not to mix cache data computed with different version of CoRTeX 
            that might result in different values in each CoRTeX version for an identical ML sample 
        
        sample_type: the type of ML sample whose vectorization is the object of the caching 
            process
        
        vectorizer_factory_configuration: the configuration of the vectorizer used to compute the 
            values to be cached; allows not to mix cache data computed using different vectorizer applied 
            to the ML samples generated from a specific document
        
        documents: collection of Documents instances that will be used to generate the ML samples 
            whose vectorized cached value we want to use (either to compute and then save, or to load 
            and use directly).
        
        persist_as_svmlight_like: boolean (default=False), whether or not to persist cached 
            data as svmlight-like file, or as python's pickle file. 'svmlight-like' allows to open a 
            file for visual inspection of the vectorization value associated to a ML sample, but takes 
            a lot more space than a pickle file. Hence the default behavior is to use pickle files.
        
        sparse_format: if using svmlight-like files for storing cached data, whether or not 
            to load them as sparse vectors (default behavior) or as dense vectors.
    

    Warning:
        If the ML samples that needs to be vectorized refer to a Document instance that 
        does not belong to the input 'documents' parameter, the possibly already existing cached data 
        for this document will not be loaded when entering the code wrapped by an instance of this class, 
        and will be overwritten instead of being potentially updated, when exiting the code wrapped 
        by the instance of this class.
    
    Attributes:
        cortex_cache_folder_path: string, or None; path to the root folder used for persisting 
            CoRTex cache data; if None, this class will do nothing: no loading cache, no saving cache
        
        cache_folder_path: string, or None; path to the cache folder associated to the specific 
            parameters used to initialize the instance. It is in ths folder that will be stored the files 
            that each represent the vectorization of ML samples created from a specific document.
        
        documents: collection of Documents instances that will be used to generate the ML samples 
            whose vectorized cached value we want to use (either to compute and then save, or to load 
            and use directly).
        
        document_hash2cache: a "document hash => cache" map; or None (if no cache loading occurred 
            yet)
        
        document_hash2len_before: a "document hash => cache length" map, or None (if no cache 
            loading occurred yet); store information about the length of the cache associated to each 
            document just after it was loaded; is used to determine which cache file to update at the 
            end of the process
        
        persist_as_svmlight_like: boolean (default=False), whether or not to persist cached 
            data as svmlight-like file, or as python's pickle file. 'svmlight-like' allows to open a 
            file for visual inspection of the vectorization value associated to a ML sample, but takes 
            a lot more space than a pickle file. Hence the default behavior is to use pickle files.
        
        sparse_format: if using svmlight-like files for storing cached data, whether or not 
            to load them as sparse vectors (default behavior) or as dense vectors.
    
    Warning:
        The cached data assumes that:
        
            - the documents and the mentions have already been preprocessed / characterized, and that the 
              experience for which the user wishes to use the cached data does not involve a different 
              characterization process, that could give different attribute values to the mention than the one 
              that the mentions had when the ML samples based upon them were vectorized for the first time.
              For instance, if the user decides to carry out its own document preprocessing (annotating the 
              sentences' constituency trees, for instance), then that can in turn influence the mentions 
              characterization, so if this document preprocessing changes, the user should reflect that by 
              using another 'document_preprocessing_version' parameter value when initializing an instance of 
              this class
            - the language parameters are the same (cf the '__init__' file of one language subpackage, such 
              as 'english'); if they are not, then the cahce will have to be built anew, which means that the 
              vectorization will be computed anew
            - the vectorizer_configuration subconfiguration objects (such as the configuration for a 
              PairGramTypeHierarchy, for instance), do not contains 'fancy', manually added field(s). If one 
              of them does, the vectorization will be computed anew, without hope of using it ever again unless 
              the exact same configurations are used.
            - the documents' respective 'base_hash_string' value is the same, character per character. 
              As those depends on the 'raw_text' parameter of a Document instance, as well as all the markable 
              collections associated to it (tokens, named_entities, mentions, sentences, quotations) and its 
              coreference_partition, any change in one of them will result in the vectorization computation 
              being carried out anew.
    """
    
    def __init__(self, cortex_cache_folder_path, language, document_preprocessing_version, 
                 language_parameter_version, sample_type, vectorizer_factory_configuration, documents, 
                 persist_as_svmlight_like=False, sparse_format=True):
        logger = logging.getLogger("LocalVectorizationCache")
        vectorizer_cache_folder_path = None
        if cortex_cache_folder_path is not None:
            # Check input(s)
            if not os.path.isdir(cortex_cache_folder_path):
                msg = "Incorrect 'cortex_cache_folder_path' value: no folder located at '{}'."
                msg = msg.format(cortex_cache_folder_path)
                raise ValueError(msg)
            if sample_type not in POSSIBLE_SAMPLE_TYPES:
                msg = "Incorrect input 'sample_type' value '{}', possible values are: {}."
                msg = msg.format(sample_type, POSSIBLE_SAMPLE_TYPES)
                raise ValueError(msg)
            
            ### Vectorizer config sub-sub-sub-subfolder: the folder name is created from the hash of the 'hash string' associated to the vectorizer's configuration
            ### Fetch only the parts of the vectorizer configuration that are related to its impact on the definition of the vectorizer:
            ### It would not do if hash is different just because someone decided to add a useless field to the configuration...
            normalized_vectorizer_factory_configuration = get_normalized_vectorizer_factory_configuration(vectorizer_factory_configuration)
            vectorizer_hash_string = Mapping.get_base_hash_string(normalized_vectorizer_factory_configuration)
            vectorizer_hash = str(CityHash128(vectorizer_hash_string))
            
            # Create path to folder corresponding to vectorization parameters
            msg = "Initializing LocalVectorizationCache('{}', '{}', '{}', '{}', '{}', '{}')..."
            msg = msg.format(cortex_cache_folder_path, language, document_preprocessing_version, 
                             language_parameter_version, sample_type, str(vectorizer_hash))
            logger.debug(msg)
            ## Check existence (and create if necessary) of folder destined to contain the caching files
            ### Language
            language_cache_folder_path = os.path.join(cortex_cache_folder_path, language)
            _create_folder_if_not_exist(language_cache_folder_path)
            ### Document preprocessing version subfolder
            document_preprocessing_version_folder_path = os.path.join(language_cache_folder_path, document_preprocessing_version)
            _create_folder_if_not_exist(document_preprocessing_version_folder_path)
            ### ML features version sub-subfolder
            language_parameter_version_folder_path = os.path.join(document_preprocessing_version_folder_path, language_parameter_version)
            _create_folder_if_not_exist(language_parameter_version_folder_path)
            ### Sample type sub-sub-subfolder
            sample_type_cache_folder_path = os.path.join(language_parameter_version_folder_path, sample_type)
            _create_folder_if_not_exist(sample_type_cache_folder_path)
            ### Vectorizer config sub-sub-sub-subfolder
            vectorizer_cache_folder_path = os.path.join(sample_type_cache_folder_path, vectorizer_hash)
            _create_folder_if_not_exist(vectorizer_cache_folder_path)
            ### Add a file representing the configuration of the vectorizer in the corresponding folder
            vectorizer_configuration_save_file_name = "vectorizer.cfg"
            vectorizer_configuration_save_file_path = os.path.join(vectorizer_cache_folder_path, vectorizer_configuration_save_file_name)
            save_mapping(normalized_vectorizer_factory_configuration, vectorizer_configuration_save_file_path)
            logger.debug("Done.")
        
        self.cortex_cache_folder_path = cortex_cache_folder_path
        self.cache_folder_path = vectorizer_cache_folder_path
        self.documents = documents
        self.document_hash2cache = None
        self.document_hash2len_before = None
        self.persist_as_svmlight_like = persist_as_svmlight_like
        self.sparse_format = sparse_format
        self._logger = logger
    
    def load_document_cache_file(self, file_path):
        """ Loads a document cache stored as a local pickle file.
        
        Args:
            file_path: string, path to document cache file

        Returns:
            a 'ML sample hash string => vector' map
        """
        with open(file_path, "br") as f:
            document_cache = pickle.load(f)
        return document_cache
    
    def save_document_cache_file(self, document_cache, file_path):
        """ Saves a document cache as a local pickle file.
        
        Args:
            document_cache: a 'ML sample hash string => vector' map, the document cache to save
            file_path: string, path to the local pickle file where to save the document cache
        """
        with open(file_path, "bw") as f:
            pickle.dump(document_cache, f)
    
    def load_document_cache_file_from_svmlight_like_file(self, file_path):
        """ Loads a document cache stored as a local svmlight-like formatted file.
        
        Args:
            file_path: string, path to document cache file

        Returns:
            a 'ML sample hash string => vector' map
        """
        document_cache = {load_from_svmlight_like_file(file_path, sparse=True)}
        return document_cache
    
    def save_document_cache_file_to_svmlight_like_file(self, document_cache, file_path):
        """ Saves a document cache as a local svmlight-like formatted file.
        
        Args:
            document_cache: a 'ML sample hash string => vector' map, the document cache to save
            file_path: string, path to the local svmlight-like formatted file where to save the document cache
        """
        persist_to_svmlight_like_file(file_path, ((value, key) for key, value in document_cache.items()))
    
    def load_caches(self):
        """ Loads the potentially existing document caches corresponding to the instance's configuration. """
        if self.cortex_cache_folder_path is not None:
            # Determine which loading fct to use
            load_fct = self.load_document_cache_file
            file_name_format = "{}.pkl"
            if self.persist_as_svmlight_like:
                load_fct = lambda file_path: self.load_document_cache_file_from_svmlight_like_file(file_path, sparse=self.sparse_format)
                file_name_format = "{}.svmlight_like"
            self._logger.debug("Loading potentially already persisted document cache(s)...")
            # Create the identifying hash string corresponding to each document
            document_hash2cache = defaultdict(dict)
            ## For each document, check whether or not a previous cache file already exists, and if it does, load it
            for document in self.documents:
                document_hash = document.hash_string
                document_cache_file_name = file_name_format.format(document_hash)
                document_cache_file_path = os.path.join(self.cache_folder_path, document_cache_file_name)
                if os.path.exists(document_cache_file_path):
                    document_cache = load_fct(document_cache_file_path)
                    document_hash2cache[document_hash] = document_cache
            self.document_hash2cache = document_hash2cache
            self.document_hash2len_before = dict((key, len(value)) for key, value in self.document_hash2cache.items())
            self._logger.debug("Done (loaded cache for {} document(s)).".format(len(self.document_hash2cache)))
    
    def persist_updated_caches(self):
        """ Saves the potentially existing document caches currently held in memory, if the instance's 
        configuration allows it.
        """
        if self.cortex_cache_folder_path is not None:
            # Determine which saving fct to use
            save_fct = self.save_document_cache_file
            file_name_format = "{}.pkl"
            if self.persist_as_svmlight_like:
                save_fct = lambda document_cache, file_path: self.save_document_cache_file_to_svmlight_like_file(document_cache, file_path)
                file_name_format = "{}.svmlight_like"
            self._logger.debug("Persisting potentially new or updated document cache(s)...")
            # Persist the cache(s) when necessary
            document_hash2len_before = self.document_hash2len_before
            saved_nb = 0
            for document_hash, document_cache in self.document_hash2cache.items():
                old_len = document_hash2len_before.get(document_hash)
                new_len = len(document_cache)
                if old_len is None or old_len != new_len:
                    document_cache_file_name = file_name_format.format(document_hash)
                    document_cache_file_path = os.path.join(self.cache_folder_path, document_cache_file_name)
                    save_fct(document_cache, document_cache_file_path)
                    saved_nb += 1
                    document_hash2len_before[document_hash] = new_len
            self._logger.debug("Done (persisted new or updated cache for {} document(s)).".format(saved_nb))
    
    def __enter__(self):
        self.load_caches()
        return self
    
    def __exit__(self, *args):
        self.persist_updated_caches()

# Utilities
def _create_folder_if_not_exist(folder_path):
    """ Checks whether a folder exists or not at the specified path, and create it if it does not.
    
    Args:
        folder_path: string, path to a local folder
    
    Raises:
        ValueError: if something already exists at the location specified by the input path, that is not 
            a folder
    """
    if not os.path.isdir(folder_path):
        if os.path.exists(folder_path):
            msg = "Something is located at '{}' that is not a folder, cannot proceed."
            msg = msg.format(folder_path)
            raise ValueError(msg)
        else:
            os.mkdir(folder_path)
