# -*- coding: utf-8 -*-

"""
Define utilities used to evaluate coreference resolvers provided adequate evaluation configuration 
parameters.
"""

__all__ = ["evaluate_resolvers",
           "evaluate_resolver",
           "generate_evaluation_results",
           "normalize_evaluate_resolvers_configuration",
           ]

import logging
from cityhash import CityHash128
from collections import defaultdict

import itertools

from .vectorization_cache import LocalVectorizationCache
from cortex.config import (get_language_dependent_object, 
                           language as current_working_language, get_cache_folder_path)
from cortex.api.coreference_partition import CoreferencePartition
from cortex.coreference.evaluation.conll2012_scorer import CONLL2012Scorer, format_resolvers_evaluation_results
from cortex.learning.sample_features_vectorizer import get_normalized_vectorizer_factory_configuration
from cortex.tools import Mapping
from cortex.utils.display import get_display_progress_function
from cortex.api.document import DocumentDoesNotPossessMentionsError


def evaluate_resolvers(resolver_ident2resolver, test_corpus, metric="all", skip_singletons=True, 
                       retrieve_individual_document_data=True, detect_mentions=True,
                       document_preprocessing_identifier_cache_parameter=None):
    """ Evaluates resolvers on a corpus of Document instances which each possesses a ground truth 
    coreference partition.
    
    Args:
        resolver_ident2resolver: a 'resolver ident' to 'resolver' map
        test_corpus: a Corpus instance, whose Document instances each possesses a ground truth 
            coreference partition; WARNING: the Document instances must be all distinct!
        metric: string, among {"muc", "b3", "ceafm", "ceafe", "blanc", "all"}: the metric to use 
            to evaluate the predictions
        skip_singletons: boolean, whether or not to skip singleton when writting the conll2012 
            files that the CONLL2012 task evaluation software uses as input.
        retrieve_individual_document_data: boolean, whether or not to get individual results for 
            each document (note that this option is not available for the "blanc" metric, for unknown reasons; 
            as such, if this option and the "blanc" or "all" metric are selected at the same time, the 
            result associated to the"blanc" metric for a given document will be marked as 'nan')
        detect_mentions: boolean, whether to detect mentions before resolving a coreference 
            partition, or to use the mentions already defined for the reference document
        document_preprocessing_identifier_cache_parameter: string, or None; if not None, the use 
            of caching for vectorization of ml samples will be activated, and the value of the string will 
            be used to specify the 'document_preprocessing_version' parameter; which must be identical from 
            one use to another, but must be set so as to remind the user that the caching should be used 
            only as long as the preprocessing of the documents and the characterization of the mentions of 
            the documents result in the same values

    Returns:
        a "resolver ident' => 'evaluation result'" map (Cf the 'CONLL2012Scorer' class)
    """
    return dict(generate_evaluation_results(resolver_ident2resolver, test_corpus, metric=metric, 
                                            skip_singletons=skip_singletons, 
                                            retrieve_individual_document_data=retrieve_individual_document_data, 
                                            detect_mentions=detect_mentions, 
                                            document_preprocessing_identifier_cache_parameter=document_preprocessing_identifier_cache_parameter)
                )


def evaluate_resolver(resolver, test_corpus, metric="all", skip_singletons=True, 
                      retrieve_individual_document_data=True, detect_mentions=True, 
                      document_preprocessing_identifier_cache_parameter=None):
    """ Evaluates a resolver on a corpus of Document instances which each possesses a ground truth 
    coreference partition.
    
    Args:
        resolver: a Resolver instance
        test_corpus: a Corpus instance, whose Document instances each possesses a ground truth 
            coreference partition; WARNING: the Document instances must be all distinct!
        metric: string, among {"muc", "b3", "ceafm", "ceafe", "blanc", "all"}: the metric to use 
            to evaluate the predictions
        skip_singletons: boolean, whether or not to skip singleton when writting the conll2012 
            files that the CONLL2012 task evaluation software uses as input.
        retrieve_individual_document_data: boolean, whether or not to get individual results for 
            each document (note that this option is not available for the "blanc" metric, for unknown reasons; 
            as such, if this option and the "blanc" or "all" metric are selected at the same time, the 
            result associated to the"blanc" metric for a given document will be marked as 'nan')
        detect_mentions: boolean, whether to detect mentions before resolving a coreference 
            partition, or to use the mentions already defined for the reference document
        document_preprocessing_identifier_cache_parameter: string, or None; if not None, the use 
            of caching for vectorization of ml samples will be activated, and the value of the string will 
            be used to specify the 'document_preprocessing_version' parameter; which must be identical from 
            one use to another, but must be set so as to remind the user that the caching should be used 
            only as long as the preprocessing of the documents and the characterization of the mentions of 
            the documents result in the same values

    Returns:
        a (metric_overall_results, metric_name2results_per_document) pair (Cf the 'CONLL2012Scorer' class)
    """
    resolver_ident = "{}_ident".format(resolver.NAME)
    resolver_ident2resolver = {resolver_ident: resolver}
    resolver_ident2evaluation_result = evaluate_resolvers(resolver_ident2resolver, test_corpus, 
                                                          metric=metric, skip_singletons=skip_singletons, 
                                                          retrieve_individual_document_data=retrieve_individual_document_data, 
                                                          detect_mentions=detect_mentions, 
                                                          document_preprocessing_identifier_cache_parameter=document_preprocessing_identifier_cache_parameter)
    evaluation_result = resolver_ident2evaluation_result[resolver_ident]
    return evaluation_result


def generate_evaluation_results(resolver_ident2resolver, test_corpus, metric="all", skip_singletons=True, 
                                retrieve_individual_document_data=True, detect_mentions=True, 
                                document_preprocessing_identifier_cache_parameter=None, 
                                display_progress_function=None, current_count=0):
    """ Creates a generator over evaluation results of input resolvers, on a corpus of Document 
    instances which each possesses a ground truth coreference partition.
    
    Args:
        resolver_ident2resolver: a 'resolver ident' to 'Resolver instance' map
        test_corpus: a Corpus instance, whose Document instances each possesses a ground truth 
            coreference partition; WARNING: the Document instances must be all distinct!
        metric: string, among {"muc", "b3", "ceafm", "ceafe", "blanc", "all"}: the metric to use 
            to evaluate the predictions
        skip_singletons: boolean, whether or not to skip singleton when writting the conll2012 
            files that the CONLL2012 task evaluation software uses as input.
        retrieve_individual_document_data: boolean, whether or not to get individual results for 
            each document (note that this option is not available for the "blanc" metric, for unknown reasons; 
            as such, if this option and the "blanc" or "all" metric are selected at the same time, the 
            result associated to the"blanc" metric for a given document will be marked as 'nan')
        detect_mentions: boolean, whether to detect mentions before resolving a coreference 
            partition, or to use the mentions already defined for the reference document
        document_preprocessing_identifier_cache_parameter: string, or None; if not None, the use 
            of caching for vectorization of ml samples will be activated, and the value of the string will 
            be used to specify the 'document_preprocessing_version' parameter; which must be identical from 
            one use to another, but must be set so as to remind the user that the caching should be used 
            only as long as the preprocessing of the documents and the characterization of the mentions of 
            the documents result in the same values
        display_progress_function: function to use to display progress by inputing the current 
            count value; or None, in which case one will be created
        current_count: int, the count value from which to continue the incrementation for each 
            evaluated resolver, and to input into the display_progress_fct

    Yields:
        a ('resolver ident', 'evaluation results') pair (Cf the 'CONLL2012Scorer' class)
    """
    # Get and instantiate tools
    MentionDetectorClass = get_language_dependent_object("mention_detector")
    MentionCharacterizerClass = get_language_dependent_object("mention_characterizer") # Warning: make sure the toolbox was configured to be used with the language with which the document were written.
    from cortex.learning.filter.mentions import NoVerbGramTypeMentionFilter
    
    mention_detector = MentionDetectorClass()
    mention_characterizer = MentionCharacterizerClass(keep_existing_attribute_value=True)
    no_verb_gram_type_mention_filter = NoVerbGramTypeMentionFilter()
    conll2012_scorer = CONLL2012Scorer()
    logger = logging.getLogger("evaluate_resolver")
    
    debug_message = "'document_preprocessing_identifier_cache_parameter' value is '{}', caching will {}be used."
    debug_message = debug_message.format(document_preprocessing_identifier_cache_parameter, 
                                         "" if document_preprocessing_identifier_cache_parameter else "not ")
    logger.debug(debug_message)
    
    # Define document collection
    reference_documents = test_corpus.documents
    
    # Detect mentions anew
    logger.info("Create prediction documents by potentially detecting mentions anew...")
    prediction_documents = _create_prediction_documents(reference_documents, mention_detector, 
                                                        detect_mentions=detect_mentions)
    logger.info("Finished create prediction documents by potentially detecting mentions anew.")
    ## Characterize the detected mentions
    logger.info("Characterizing the detected mentions...")
    mention_characterizer.enrich_documents_mentions(prediction_documents)
    logger.info("Finished characterizing the detected mentions.")
    ## Filter out the mentions potentially classified as verbal
    logger.info("Filtering the detected verbal mentions...")
    for document in prediction_documents:
        no_verb_gram_type_mention_filter.filter_document(document)
    logger.info("Finished filtering the detected verbal mentions.")
    
    # Setup the use of vectorization caching
    language_parameter_version = get_language_dependent_object("LANGUAGE_PARAMETER_VERSION")
    cortex_cache_folder_path = get_cache_folder_path() if document_preprocessing_identifier_cache_parameter else None
    document_preprocessing_version = document_preprocessing_identifier_cache_parameter
    persist_as_svmlight_like = False
    
    # Group the resolver configuration to train by common (sample_type, vectorizer_configuration) pairs
    # That way we will be able to efficiently use the caching operations
    resolver_ident2vectorizer_ident_tuple = {}
    vectorizer_ident_tuple2resolvers = defaultdict(list)
    for resolver_ident, resolver in resolver_ident2resolver.items():
        vectorizer_configuration_ = resolver.configuration["model_interface"]["config"]["sample_features_vectorizer"]
        normalized_vectorizer_configuration = get_normalized_vectorizer_factory_configuration(vectorizer_configuration_)
        vectorizer_hash_string = Mapping.get_base_hash_string(normalized_vectorizer_configuration)
        vectorizer_hash = str(CityHash128(vectorizer_hash_string))
        sample_type = resolver.sample_type
        vectorizer_ident_tuple = (vectorizer_hash, sample_type)
        vectorizer_ident_tuple2resolvers[vectorizer_ident_tuple].append((resolver_ident, resolver))
        resolver_ident2vectorizer_ident_tuple[resolver_ident] = vectorizer_ident_tuple
    resolver_ident_resolver_pairs = list(itertools.chain(*tuple(vectorizer_ident_tuple2resolvers.values())))
    
    # Setup logging of progress info when iterating over resolvers
    if display_progress_function is None:
        maximum_count = len(resolver_ident_resolver_pairs)
        ls = str(len(str(maximum_count)))
        percentage_interval = 5
        personalized_progress_message = "Processed {percentage:6.2f}% of resolver to evaluate ({current:"+ls+"d} out of {total:"+ls+"d})"
        display_progress_function = get_display_progress_function(logger, 
                                                                  maximum_count=maximum_count, 
                                                                  percentage_interval=percentage_interval, 
                                                                  personalized_progress_message=personalized_progress_message, 
                                                                  logging_level="info")
        new_current_count = 0
    else:
        new_current_count = current_count
    
    # Setup logging of progress info when iterating over documents, and setup evaluation function
    maximum_count = len(reference_documents)
    ls = str(len(str(maximum_count)))
    progress_message = "Processed {percentage:6.2f}% of the documents ({current:"+ls+"d} out of {total:"+ls+"d})"
    documents_progress_display_fct = get_display_progress_function(logger, maximum_count=maximum_count, percentage_interval=5, 
                                                                   personalized_progress_message=progress_message, 
                                                                   logging_level="debug")
    evaluation_fct =\
    lambda doc_ref_part_predicted_part_triplets: conll2012_scorer.evaluate(doc_ref_part_predicted_part_triplets, 
                                                                           metric=metric, skip_singletons=skip_singletons, 
                                                                           retrieve_individual_document_data=retrieve_individual_document_data)
    
    # Carry out the evaluation itself
    logger.info("Generating evaluation performances for all resolvers...")
    ## Carry out the loop over resolvers
    previous_vectorizer_ident_tuple = None
    vectorization_cache = None
    for resolver_ident, resolver in resolver_ident_resolver_pairs:
        display_progress_function(new_current_count)
        
        ## Setup the use of vectorization caching
        vectorizer_configuration = resolver.configuration["model_interface"]["config"]["sample_features_vectorizer"]
        sample_type = resolver.sample_type
        current_vectorizer_ident_tuple = resolver_ident2vectorizer_ident_tuple[resolver_ident]
        if previous_vectorizer_ident_tuple != current_vectorizer_ident_tuple:
            vectorization_cache = LocalVectorizationCache(cortex_cache_folder_path, 
                                                          current_working_language, 
                                                          document_preprocessing_version, 
                                                          language_parameter_version, 
                                                          sample_type, 
                                                          vectorizer_configuration, 
                                                          prediction_documents,
                                                          persist_as_svmlight_like=persist_as_svmlight_like)
            vectorization_cache.load_caches()
            previous_vectorizer_ident_tuple = current_vectorizer_ident_tuple
        
        ## Couple the resolver and the vectorization cache
        resolver.toggle_vectorization_cache(vectorization_cache=vectorization_cache)
        
        ## Evaluate the resolver
        logger.info("Evaluating the '{}' resolver...".format(resolver_ident))
        evaluation_result = _evaluate_resolver_on_documents(resolver, 
                                                           evaluation_fct, reference_documents, 
                                                           prediction_documents, 
                                                           documents_progress_display_fct, logger)
        logger.info("Finished evaluating the '{}' resolver.".format(resolver_ident))
        
        ## Uncouple the resolver and the vectorization cache
        resolver.toggle_vectorization_cache(vectorization_cache=None)
        
        ## Update the cache if necessary
        vectorization_cache.persist_updated_caches()
            
        ## Yield the evaluation result
        yield (resolver_ident, evaluation_result)
        new_current_count += 1
    logger.info("Finished generating evaluation performances for all resolvers.")


def _evaluate_resolver_on_documents(resolver, evaluation_fct, reference_documents, prediction_documents, 
                                    documents_progress_display_fct, logger):
    """ Evaluates a resolver using preprocessed Document instances.
    
    Args:
        resolver: _Resolver child class instance
        evaluation_fct: function that takes as as input a collection of 
            (ref_partition, predicted_partition, document) triplets, and return a 
            (metric_overall_results, metric_name2results_per_document) pair
        reference_documents: collection of Document instances, which possess their respective 
            coreference partition ground truth value; each Document instance is associated to its 
            counterpart (sharing the same collection index) in the 'prediction_documents' input value
        prediction_documents: collection of Document instances, which possess their respective 
            coreference partition predicted value; each Document instance is associated to its counterpart 
            (sharing the same collection index) in the 'reference_documents' input value
        documents_progress_display_fct: callable, progress display function to use when iterating 
            over one of the input collection of Document instances
        logger: logging.Logger instance to use for logging messages
    
    Returns:
        an 'evaluation_results' object (Cf the 'CONLL2012Scorer' class)
    """
    # Predict a coreference partition for each document
    logger.debug("Resolving a coreference partition for all the prediction documents...")
    document_ref_partition_predicted_partition_triplets = []
    for i, (ref_document, prediction_document) in enumerate(zip(reference_documents, prediction_documents)):
        documents_progress_display_fct(i)
        try:
            predicted_partition = resolver.resolve(prediction_document)
        except DocumentDoesNotPossessMentionsError:
            message = "Prediction document version of the reference document '{}' does not possess "\
                      "mentions after detection and filtering phase, an empty Coreference partition will be predicted."
            message = message.format(ref_document)
            logger.warning(message)
            predicted_partition = CoreferencePartition()
        ref_partition = ref_document.coreference_partition
        document_ref_partition_predicted_partition_triplets.append((ref_document, ref_partition, predicted_partition))
    logger.debug("Finished resolving a coreference partition for all the prediction documents.")
    
    logger.debug("Evaluating the prediction...")
    evaluation_result = evaluation_fct(document_ref_partition_predicted_partition_triplets)
    logger.debug("Finished evaluating the prediction.")
    
    return evaluation_result

def _create_prediction_documents(reference_documents, mention_detector, detect_mentions=True):
    """ Creates collection of 'predicted version' of the documents of the input collection, notably 
    by carrying out a mention detection process on each of them.
    
    Args:
        reference_documents: collection of Document instance, on which to perform the mention 
            detection process
        mention_detector: a _MentionDetector child class instance (a priori dependent upon the 
            current working language)
        detect_mentions: boolean, whether or not to properly detect mentions (True; potentially 
            with errors), or to use an oracle to carry out a perfect mention detection (False)

    Returns:
        a collection of Document instances
    """
    if detect_mentions:
        prediction_documents = [mention_detector.detect_mentions(document, detect_verbs=False, inplace=False)\
                                for document in reference_documents
                                ] # 'inplace' is important to create new document instance, and to not lose the ground truth data
    else:
        prediction_documents = [mention_detector.oracle_detect_mentions(document) for document in reference_documents]
    return prediction_documents


_EVALUATE_RESOLVERS_PARAMETERS_DEFAULT_VALUES = {"detect_mentions": True, "skip_singletons": True, 
                                                "metric": "all", "retrieve_individual_document_data": True}
def normalize_evaluate_resolvers_configuration(configuration):
    """ Extracts from the input map only the potential (field, value) pairs corresponding to evaluation 
    function parameters, and create a 'normalized' map from them.
    
    Args:
        configuration: a map defining evaluation parameters

    Returns:
        a Mapping instance
    """
    normalized_configuration = Mapping()
    get_fct = type(configuration).get
    for key, default_value in _EVALUATE_RESOLVERS_PARAMETERS_DEFAULT_VALUES.items():
        normalized_configuration[key] = get_fct(configuration, key, default_value)
    return normalized_configuration
