# -*- coding: utf-8 -*-

"""
Defines classes and functions to facilitate the implementation of experiments involving resolvers .
"""

"""
Available submodules
---------------------
vectorization_cache:
    Defines a context manager class that is used to persist to and load from local folder cached vectorization data.

evaluate:
    Defines utilities to evaluate coreference resolvers provided adequate evaluation configuration parameters.

resolver_comparison_experiment:
    Defines a class used to implement a robust 'train resolvers => evaluate trained resolvers => display results' pipeline.
    
experiments:
    Defines utilities to represent simple experiments that can be made on resolvers.
"""

from .vectorization_cache import LocalVectorizationCache
from .evaluate import evaluate_resolvers, evaluate_resolver, generate_evaluation_results
from .resolver_comparison_experiment import ResolverComparisonExperiment, ExistingResolverComparisonExperimentError
from .experiments import (carry_out_resolver_grid_search, 
                          carry_out_efficient_online_interface_based_resolver_comparison_experiment, 
                          carry_out_efficient_online_interface_based_resolver_comparison_single_experiment, 
                          create_kfold_corpora_files_folder, parse_kfold_corpora)

from . import vectorization_cache
from . import resolver_comparison_experiment
from . import evaluate
from . import experiments