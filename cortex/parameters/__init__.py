# -*- coding: utf-8 -*-

"""
Defines some hyperparameters used by the toolbox.
"""

"""
Available submodule(s)
----------------------
mention_data_tags
    Defines tags used to represent information qualifying a mention.
"""

ENCODING = "utf-8"
LINE_SEPARATOR = "\n"
CONFIG_FILE_PATH_ENV_VAR_NAME = "CORTEX_CONFIG_FILE_PATH"

LRB_cortex_symbol = "-~LRB~-"
RRB_cortex_symbol = "-~RRB~-"
ELLIPSIS_cortex_symbol = "-~ELL~-"

BEGINNING_DOCUMENT_STRING = "\n"
INTER_SENTENCE_CHAR = "\n"
INTER_WORD_CHAR = " "