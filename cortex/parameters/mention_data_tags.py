# -*- coding: utf-8 -*-

"""
Defines tags used to represent information qualifying a mention.

'EXPANDED_PRONOUNS' qualifies mentions that refer to coreference entities / previous name of nominal 
mentions, but may not be a pronoun in the narrow grammatical sense ("that can play the same grammatical 
role as of a noun"): for instance, possessive adjectives, such as "his" in the sentence "His cat is black.".
"""

import itertools

# Unknown
UNKNOWN_VALUE_TAG = "unk"

# Grammatical types
EXPANDED_PRONOUN_GRAM_TYPE_TAG = "EXPANDED_PRONOUN"
NAME_GRAM_TYPE_TAG = "NAME"
NOMINAL_GRAM_TYPE_TAG = "NOMINAL"
VERB_GRAM_TYPE_TAG = "VERB"
GRAM_TYPE_TAGS = {EXPANDED_PRONOUN_GRAM_TYPE_TAG, NAME_GRAM_TYPE_TAG, NOMINAL_GRAM_TYPE_TAG, VERB_GRAM_TYPE_TAG, UNKNOWN_VALUE_TAG}

# Grammatical subtypes
EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS = {"EXPAND_POSS_PRO": "EXPAND_POSS_PRO", "EXPAND_PERS_PRO": "EXPAND_PERS_PRO", "DEM_PRO": "DEM_PRO", "REL_PRO": "REL_PRO"}
NAME_GRAM_TYPE_SUBTYPE_TAGS = {"SHORT_NAME": "SHORT_NAME", "LONG_NAME": "LONG_NAME"}
NOMINAL_GRAM_TYPE_SUBTYPE_TAGS = dict(("{}_{}".format(f, v), )*2 for v in ["DEF_NP", "DEM_NP", "INDEF_NP", "UNDET_NP"] for f in ["SHORT", "LONG"])
GRAM_SUBTYPE_TAGS = set(itertools.chain(EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS, NAME_GRAM_TYPE_SUBTYPE_TAGS
                                        , NOMINAL_GRAM_TYPE_SUBTYPE_TAGS, (UNKNOWN_VALUE_TAG,))
                        )
GRAM_TYPE_TAG2GRAM_SUBTYPE_TAGS = {EXPANDED_PRONOUN_GRAM_TYPE_TAG: EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS, 
                                   NAME_GRAM_TYPE_TAG: NAME_GRAM_TYPE_SUBTYPE_TAGS, 
                                   NOMINAL_GRAM_TYPE_TAG: NOMINAL_GRAM_TYPE_SUBTYPE_TAGS, 
                                   VERB_GRAM_TYPE_TAG: set(), 
                                   UNKNOWN_VALUE_TAG: set()}

# Pronominal persons (as in first person, second person, third person)
FIRST_PERSON_TAG = "1"
SECOND_PERSON_TAG = "2"
THIRD_PERSON_TAG = "3"

# Gender
MALE_GENDER_TAG = "masc"
FEMALE_GENDER_TAG = "fem"
NEUTRAL_GENDER_TAG = "neut"
GENDER_TAGS = (MALE_GENDER_TAG, FEMALE_GENDER_TAG, NEUTRAL_GENDER_TAG)

# Number
SINGULAR_NUMBER_TAG = "sg"
PLURAL_NUMBER_TAG = "plr"
NUMBER_TAGS = (SINGULAR_NUMBER_TAG, PLURAL_NUMBER_TAG)
