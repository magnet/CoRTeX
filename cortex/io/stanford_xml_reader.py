# -*- coding: utf-8 -*-

"""
Defines a class to parse the output of the Stanford Core NLP pipeline into a document instance.
"""

__all__ = ["StanfordXMLReader",
           ]

import os
import re
import xml.etree.cElementTree as ET
from collections import OrderedDict

from cortex.parameters import (ENCODING, LRB_cortex_symbol, RRB_cortex_symbol, 
                                 BEGINNING_DOCUMENT_STRING, INTER_WORD_CHAR)
from cortex.api.document import Document
from cortex.api.document_buffer import DocumentBuffer
from cortex.api.constituency_tree import ConstituencyTree
from cortex.utils.xml_utils import add_unique_field_to_dict_from_subelt, IncorrectXMLElementError


INTER_WORD_CHAR_LENGTH = len(INTER_WORD_CHAR)


class StanfordXMLReader(object):
    """ Class used to parse an output of the Stanford Core NLP pipeline into a DocumentBuffer instance.
    
    Assume that the pipeline produced a file containing info about sentences that were 'POS-lemma' 
    tokenized, individualized, parsed into consistuency trees, (annotators = ['tokenize', 'ssplit', 
    'pos', 'parse']).
    
    Can also parse the result of the use of the 'lemma' and 'ner' annotators, in which case, if the 
    correct data exists, the token associated to the output document will possess lemma data, and 
    the output document may possess named entities data.
    """
    
    @classmethod
    def parse(self, file_path, sentence_sep=" ", enforce_lemma=True):
        """ Parses an XML file, output by the Stanford Core NLP pipeline, into a Document instance.
        
        It is assumed that a named entity is defined as the collection of adjacent tokens which share 
        the named entity type, in Stanford Core NLP's output xml file.
        
        Args:
            file_path: path to the Stanford Core NLP output xml file
            sentence_sep: string used when 'joining' sentences' respective 'raw_text' to build 
                the Document instance's 'raw_text' value.
            enforce_lemma: boolean, whether or not to raise an Exception if the file supposed to 
                define the tokens data does not contain 'lemma' data for its tokens

        Returns:
            a DocumentBuffer instance (use DocumentBuffer.flush() to get the corresponding document)
        """
        _, file_name_component = os.path.split(file_path)
        document_ident = os.path.splitext(file_name_component)[0]
        
        # Parse the xml tree, & fetch the data
        #xml_tree = ET.parse(file_path)
        with open(file_path, "r", encoding=ENCODING) as f:
            xml_tree = ET.parse(f)
        root_elt = xml_tree.getroot()
        document_elt = root_elt.find("document")
        sentences_elt = document_elt.find("sentences")
        sentence_elts = sentences_elt.findall("sentence")
        
        sentences_data_dict = {}
        for i, sentence_elt in enumerate(sentence_elts):
            # Initialize the dict_
            sentence_data_dict = {}
            # Ident
            ident = sentence_elt.get("id")
            if ident is None:
                raise IncorrectXMLElementError("The 'sentence_elt' n°{} contains no 'id' attribute.".format(i))
            sentence_data_dict["ident"] = ident
            
            # Parse
            constituency_tree_elt = sentence_elt.find("parse")
            constituency_tree_string = constituency_tree_elt.text
            ## Protection against parentheses that are true tokens within the text:
            ## For the '))' case, assume that the real parenthesis is prefixed by a space, as this is not supposed to happen, since 'space' is not a correct token.
            constituency_tree_string = re.sub("\(\)", "{})".format(LRB_cortex_symbol), constituency_tree_string)
            constituency_tree_string = re.sub(" \)\)", " {})".format(RRB_cortex_symbol), constituency_tree_string)
            sentence_data_dict["constituency_tree"] = ConstituencyTree.parse(constituency_tree_string)
            
            # Tokens
            tokens_data_dict = {}
            tokens_elt = sentence_elt.find("tokens")
            named_entity_definitions = []
            current_named_entity_start_token_index = None
            current_named_entity_type = None
            for j, token_elt in enumerate(tokens_elt.findall("token")):
                # Initialize the dict_
                token_data = {}
                # Ident
                token_ident = token_elt.get("id")
                if ident is None:
                    raise IncorrectXMLElementError("The 'token_elt' n°{} of the 'sentence_elt' n°{} contains no 'id' attribute.".format(j,i))
                token_data["ident"] = "{}_{}".format(ident, token_ident)
                # Raw_text
                add_unique_field_to_dict_from_subelt(token_elt, ("word", "raw_text"), token_data, str, enforce_presence=True)
                # POS tag
                add_unique_field_to_dict_from_subelt(token_elt, ("POS", "POS_tag"), token_data, str, enforce_presence=True)
                # Lemma
                add_unique_field_to_dict_from_subelt(token_elt, ("lemma", "lemma"), token_data, str, enforce_presence=enforce_lemma)
                # NamedEntity
                named_entity_elt = token_elt.find("NER")
                if named_entity_elt is not None:
                    ne_type = named_entity_elt.text
                    if ne_type != "O":
                        # If value of named entity type is different from current...
                        if ne_type != current_named_entity_type:
                            # Then, if current is a proper one, we finish defining the corresponding named entity
                            if current_named_entity_type is not None:
                                named_entity_definitions.append(((current_named_entity_start_token_index, j-1), current_named_entity_type))
                                current_named_entity_start_token_index = None
                                current_named_entity_type = None
                            current_named_entity_start_token_index = j
                            current_named_entity_type = ne_type
                                
                    else:
                        if current_named_entity_type is not None:
                            named_entity_definitions.append(((current_named_entity_start_token_index, j-1), current_named_entity_type))
                            current_named_entity_start_token_index = None
                            current_named_entity_type = None
                else:
                    if current_named_entity_type is not None:
                        named_entity_definitions.append(((current_named_entity_start_token_index, j-1), current_named_entity_type))
                        current_named_entity_start_token_index = None
                        current_named_entity_type = None
                
                tokens_data_dict[int(token_ident)] = token_data
            sentence_data_dict["tokens"] = tokens_data_dict
            sentence_data_dict["named_entity_definitions"] = named_entity_definitions
            
            sentences_data_dict[int(ident)] = sentence_data_dict
        
        # Build the 'raw_text' attribute of the document, and create the 'extent' attribute of each element
        tokens_data = OrderedDict()
        named_entities_data = OrderedDict()
        sentences_data = OrderedDict()
        sentence_raw_text_strings = []
        pos_value = len(BEGINNING_DOCUMENT_STRING)
        for _, sentence_data in sorted(sentences_data_dict.items(), key=lambda x: x[0]):
            sentence_tokens_data = []
            sentence_named_entities_data = []
            # Parse tokens data so as to build raw_text attribute value
            tokens_data_dict = sentence_data["tokens"]
            for _, token_data in sorted(tokens_data_dict.items(), key=lambda x: x[0]):
                raw_text = token_data["raw_text"]
                # FIXME: use the CharacterOffset info present in the xml file instead?
                # Token extent
                start = pos_value + 0
                end = pos_value + len(raw_text) - 1
                token_extent = (start, end)
                token_data["extent"] = token_extent
                sentence_tokens_data.append(token_data)
                
                pos_value = end + INTER_WORD_CHAR_LENGTH + 1
            
            # Correctly define potential named entities
            for (start_token_index, end_token_index), ne_type in sentence_data["named_entity_definitions"]:
                named_entity_start = sentence_tokens_data[start_token_index]["extent"][0]
                named_entity_end = sentence_tokens_data[end_token_index]["extent"][-1]
                named_entity_extent = (named_entity_start, named_entity_end)
                named_entity_ident = "{},{}".format(*named_entity_extent)
                named_entity_data = {"ident": named_entity_ident, "extent": named_entity_extent, "type": ne_type}
                sentence_named_entities_data.append(named_entity_data)
            del sentence_data["named_entity_definitions"]
            
            # Sentence's raw text
            sentence_raw_text = INTER_WORD_CHAR.join(td["raw_text"] for td in sentence_tokens_data)
            sentence_data["raw_text"] = sentence_raw_text
            sentence_raw_text_strings.append(sentence_raw_text)
            # Sentence's extent
            sentence_extent = tuple(sentence_tokens_data[i]["extent"][j] for i,j in ((0,0), (-1,1)))
            sentence_data["extent"] = sentence_extent
            
            tokens_data.update((token_data["extent"], token_data) for token_data in sentence_tokens_data)
            named_entities_data.update((named_entity_data["extent"], named_entity_data) for named_entity_data in sentence_named_entities_data)
            del(sentence_data["tokens"])
            
            sentences_data[sentence_data["extent"]] = sentence_data
        
        document_raw_text = BEGINNING_DOCUMENT_STRING + sentence_sep.join(sentence_raw_text_strings)
        
        # Create the Document instance and the DocumentBuffer instance
        info = OrderedDict()
        info["original_corpus_format"] = "stanford_core_nlp_xml"
        document = Document(document_ident, document_raw_text, info=info)
        document_buffer = DocumentBuffer(document)
        
        # Add NamedEntity data
        document_buffer.add_named_entities(named_entities_data)
        # Add Token data
        document_buffer.add_tokens(tokens_data)
        # Add Sentence data
        document_buffer.add_sentences(sentences_data)
        
        # Enforce the changes and the synchronization
        document_buffer.flush(strict=False)
        
        return document_buffer
