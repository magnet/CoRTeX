# -*- coding: utf-8 -*-

"""
Defines classes used to read and write text / document data from and to local hard drive.
"""

"""
Available submodules
--------------------
conll2012_reader
    Defines classes used for parsing document files using the CONLL2012 format

conll2012_writer
    Defines classes used for writing document to files using the CONLL2012 format

conllu_reader
    Defines class used for parsing document files using the CONLL-U format

pivot_writer
    Defines class used to persist a document's data to a folder, using CoRTeX's own format 

pivot_reader
    Defines class used to parse a document's data from a folder, using CoRTeX's own format

stanford_xml_reader
    Defines a class to parse the output of the stanford core nlp pipeline into a document instance

corpus_io
    Defines utilities to persist or parse corpora / collections of documents
"""

__all__ = ["PivotReader", "PivotWriter", 
           "CONLL2012DocumentReader", "CONLL2012DocumentWriter", 
           "CONLL2012MultiDocumentReader", "CONLL2012MultiDocumentWriter", 
           "CONLLUDocumentReader", 
           "StanfordXMLReader", 
           "write_corpus_file", "write_corpus", 
           "parse_corpus_file", "parse_corpus", "parse_corpus_documents", "parse_documents_from_corpus_file_data", 
           ]

from .pivot_reader import PivotReader
from cortex.io.pivot_writer import PivotWriter
from .conll2012_reader import CONLL2012DocumentReader, CONLL2012MultiDocumentReader
from .conll2012_writer import CONLL2012DocumentWriter, CONLL2012MultiDocumentWriter
from .conllu_reader import CONLLUDocumentReader
from .corpus_io import write_corpus, write_corpus_file, parse_corpus, parse_corpus_file
from .stanford_xml_reader import StanfordXMLReader

def check_and_merge_conllu_document_into_conll2012_document(conll2012_document, conllu_document):
    """ Check that both the conll2012 version and the conllu version of a document possess the same 
    collection of tokens, and update the conll2012 version of the document with data that should be 
    exclusive to documents loaded from a 'conllu' format.
    Currently, those exclusive data consist in the 'UD_features' attribute value of the Token instances 
    associated to the 'conllu' version of the document.
    
    Args:
        conll2012_document: Document instance, assumed to have been loaded from a file that uses 
            the CONLL2012 format
        conllu_document: Document instance, assumed to have been loaded from a file that uses 
            the CONLLU format

    Returns:
        Document instance, the input 'conll2012_document' value, whose tokens were updated with 
    'UD_features' data present for those of the input 'conllu_document' value
    """
    # Check list of tokens
    conll2012_tokens_nb = len(conll2012_document.tokens)
    conllu_tokens_nb = len(conllu_document.tokens)
    if conll2012_tokens_nb != conllu_tokens_nb:
        msg = "Document '{}': conll2012 tokens nb ('{}') != conllu tokens nb ('{}')"
        msg = msg.format(conll2012_document.ident, conll2012_tokens_nb, conllu_tokens_nb)
        raise ValueError(msg)
    
    # Update the tokens of the conll2102 document with the 'UD_features' attribute value of the tokens of the conllu document
    conll2012_document.info["original_corpus_format"] = "conll2012"
    for i, (conll2012_token, conllu_token) in enumerate(zip(conll2012_document.tokens, conllu_document.tokens)):
        if conll2012_token.raw_text != conllu_token.raw_text:
            msg = "Token n°{} of document '{}': conll2012 token's raw text ('{}') != conllu token raw text ('{}')"
            msg = msg.format(i, conll2012_document.ident, conll2012_token.raw_text, conllu_token.raw_text)
            raise ValueError(msg)
        
        conll2012_token.UD_features = conllu_token.UD_features
    
    return conll2012_document