# -*- coding: utf-8 -*-

"""
Defines classes used for parsing document files that use the CONLL2012 format.

CONLL2012 FORMAT

+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
|  Column   |      Type               |          Description                                                                                                     |
+===========+=========================+==========================================================================================================================+
|     1     |  Document ID            | This is a variation on the _document filename                                                                            |
+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
|     2     |  Part number            | | Integer, ident of the current document in a potential multidocument:                                                   |
|           |                         | | some files are divided into multiple parts numbered as 000, 001, 002, ... etc.                                         |
|           |                         | | If there is only one document, and so if the concept of part has no meaning, the default value is "*"?                 |
+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
|     3     | Word ID in the sentence |                                                                                                                          |
+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
|     4     | Word itself             | | This is the token as segmented/tokenized in the Treebank. Initially the *_skel                                         |
|           |                         | | file contain the placeholder [WORD] which gets replaced by the actual token from                                       |
|           |                         | | the Treebank which is part of the OntoNotes release.                                                                   |
+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
|     5     | Part-of-Speech          | The default value when there is no available data is "-"?                                                                |
+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
|     6     | Parse bit               | | This is the bracketed structure broken before the first open parenthesis in the parse,                                 |
|           |                         | | and the word/part-of-speech leaf replaced with a *. The full parse can be created by                                   |
|           |                         | | substituting the asterix with the "([pos] [word])" string (or leaf) and concatenating                                  |
|           |                         | | the items in the rows of that column. The default value when there is no available data is "-"?                        |
+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
|     7     | Predicate lemma         | | The predicate lemma is mentioned for the rows for which we have semantic role information.                             |
|           |                         | | All other rows are marked with a "-".                                                                                  |
+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
|     8     | Predicate Frameset ID   | This is the PropBank frameset ID of the predicate in Column 7. The default value when there is no available data is "-". |
+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
|     9     | Word sense              | This is the word sense of the word in Column 3. The default value when there is no available data is "-".                |
+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
|    10     | Speaker/Author          | | This is the speaker or author name where available. Mostly in Broadcast Conversation                                   |
|           |                         | | and Web Log data. The default value when there is no available data is "-".                                            |
+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
|    11     | Named Entities          | | These columns identifies the spans representing various named entities.                                                |
|           |                         | | The default value when there is no available data is "*".                                                              |
+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
|  12:N     | Predicate Arguments     | | For a given sentence, there is one column each of predicate argument structure information for the predicate           |
|           |                         | | mentioned in Column 7. When there is no available information, there should be no column.                              |
|           |                         | | The default value when there is no available data for a given, existing column, is "*".                                |
+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
|     N     | Coreference             | | Coreference chain information encoded in a parenthesis structure.                                                      |
|           |                         | | The default value when there is no available data is "-".                                                              |
+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+

Cf http://conll.cemantix.org/2012/data.html

Examples:

'

| bc/cctv/00/cctv_0000   0    0         With     IN  (TOP(S(PP*       -    -   -   Speaker#1       *             *   (ARGM-MNR*        *      -
| bc/cctv/00/cctv_0000   0    1        their   PRP$        (NP*       -    -   -   Speaker#1       *             *            *        *    (18)
| bc/cctv/00/cctv_0000   0    2       unique     JJ           *       -    -   -   Speaker#1       *             *            *        *      -
| bc/cctv/00/cctv_0000   0    3        charm     NN          *))   charm   -   -   Speaker#1       *             *            *)       *      -
| bc/cctv/00/cctv_0000   0    4            ,      ,           *       -    -   -   Speaker#1       *             *            *        *      -
| bc/cctv/00/cctv_0000   0    5        these     DT        (NP*       -    -   -   Speaker#1       *             *       (ARG0*        *    (18
| bc/cctv/00/cctv_0000   0    6         well     RB      (ADJP*       -    -   -   Speaker#1       *    (ARGM-EXT*)           *        *      -
| bc/cctv/00/cctv_0000   0    7           -    HYPH           *       -    -   -   Speaker#1       *             *            *        *      -
| bc/cctv/00/cctv_0000   0    8        known    VBN           *)    know  01   -   Speaker#1       *           (V*)           *        *      -
| bc/cctv/00/cctv_0000   0    9      cartoon     NN           *       -    -   -   Speaker#1       *        (ARG1*)           *        *      -
| bc/cctv/00/cctv_0000   0   10       images    NNS           *)   image   -   -   Speaker#1       *      (C-ARG1*)           *)       *     18)
| bc/cctv/00/cctv_0000   0   11         once     RB      (ADVP*       -    -   -   Speaker#1       *             *   (ARGM-TMP*        *      -
| bc/cctv/00/cctv_0000   0   12        again     RB           *)      -    -   -   Speaker#1       *             *            *)       *      -
| bc/cctv/00/cctv_0000   0   13       caused    VBD        (VP*    cause  01   1   Speaker#1       *             *          (V*)       *      -
| bc/cctv/00/cctv_0000   0   14         Hong    NNP      (S(NP*       -    -   -   Speaker#1   (GPE*             *       (ARG1*   (ARG1*    (23
| bc/cctv/00/cctv_0000   0   15         Kong    NNP           *)      -    -   -   Speaker#1       *)            *            *        *)    23)
| bc/cctv/00/cctv_0000   0   16           to     TO        (VP*       -    -   -   Speaker#1       *             *            *        *      -
| bc/cctv/00/cctv_0000   0   17           be     VB        (VP*       be  01   1   Speaker#1       *             *            *      (V*)     -
| bc/cctv/00/cctv_0000   0   18            a     DT     (NP(NP*       -    -   -   Speaker#1       *             *            *   (ARG2*      -
| bc/cctv/00/cctv_0000   0   19        focus     NN           *)   focus   -   -   Speaker#1       *             *            *        *      -
| bc/cctv/00/cctv_0000   0   20           of     IN        (PP*       -    -   -   Speaker#1       *             *            *        *      -
| bc/cctv/00/cctv_0000   0   21    worldwide     JJ        (NP*       -    -   -   Speaker#1       *             *            *        *      -
| bc/cctv/00/cctv_0000   0   22    attention     NN     *)))))))      -    -   -   Speaker#1       *             *            *)       *)     -
| bc/cctv/00/cctv_0000   0   23            .      .          *))      -    -   -   Speaker#1       *             *            *        *      -



'
"""

import re
from collections import OrderedDict, defaultdict

from cortex.api.constituency_tree import ConstituencyTreeNodeCONLL2012StringRepresentationBuilder
from cortex.api.document import Document
from cortex.api.document_buffer import DocumentBuffer
from cortex.api.entity import Entity
from cortex.api.coreference_partition import CoreferencePartition, MentionBelongsToTwoEntitiesCollectionError
from cortex.utils.io import FileReader
from cortex.utils import IndexedIterator, CoRTeXException

__all__ = ["IncorrectCONLL2012FormattedLineIteratorError", 
           "OverlappingNamedEntitiesError", 
           "OverlappingCoreferenceEntitiesError", 
           #"InconsistentEntitiesError", 
           #"InconsistentEntitiesCollectionError", 
           #"ExceptionCollectionError", 
           "CONLL2012DocumentLineIterator", 
           "CONLL2012DocumentReader",
           "CONLL2012MultiDocumentReader",
           ]



class CONLL2012DocumentReader(object):
    """ Class designed to be able to parse a CONLL2012 formated document file, or iterator over 
    lines, into the corresponding Document instance
    
    Examples:
        >>> document = CONLL2012DocumentReader.parse(file_path)
    
        or
    
        >>> with open(file_path, "r") as f:
        >>>     document = CONLL2012DocumentReader.parse(f)
    """
    
    # Class method(s)
    @classmethod
    def __repr__(cls):
        return "{}()".format(cls.__name__)
    
    @classmethod
    def parse(cls, line_iterator):
        """ Parses the iterator over lines, representing a CONLL2012 formatted document, into a Document 
        instance.
        
        Args:
            line_iterator: iterator over lines of a CONLL2012 formatted file, or string; if string, 
                will be treated as a file path to a CONLL2012 formatted file

        Returns:
            a Document instance
        """
        if isinstance(line_iterator, str):
            file_path = line_iterator
            def _parse_function():
                line_iterator = iter(FileReader(file_path))
                try:
                    document = cls._parse_line_iterator(line_iterator)
                except IncorrectCONLL2012FormattedLineIteratorError as e:
                    message = "File located at '{}':\n{}".format(file_path, str(e))
                    raise IncorrectCONLL2012FormattedLineIteratorError(e.line_nb, message) from None
                except MentionBelongsToTwoEntitiesCollectionError as e:
                    msg = "File located at '{}':\n{}".format(file_path, e.args[0] if e.args else "")
                    raise MentionBelongsToTwoEntitiesCollectionError(e.exceptions, msg) from None
                except OverlappingNamedEntitiesError as e:
                    msg = "File located at '{}':\n{}".format(file_path, e.args[0] if e.args else "")
                    raise OverlappingNamedEntitiesError(e.line_nb, msg) from None
                except OverlappingCoreferenceEntitiesError as e:
                    msg = "File located at '{}':\n{}".format(file_path, e.args[0] if e.args else "")
                    raise OverlappingCoreferenceEntitiesError(e.line_nb, msg) from None
                return document
        else:
            def _parse_function():
                return cls._parse_line_iterator(line_iterator)
        
        return _parse_function()
    
    @staticmethod
    def _process_end_of_parsed_sentence(sentence_tokenspans, sent_startindex, global_token_index, 
                                        current_constituency_tree_string_builder, constituency_tree_strings, 
                                        buffer_predarg, sentence_predarg_tokenspans, tokens_rawspans, 
                                        sent_number):
        """ Aggregates the data accumulated during the parsing of a sentence written in CONLL2012 
        format, stack this aggregated data into the relevant structures, and reset the structures 
        used to accumulate data to be aggregated for the future parsing of another sentence.
        """
        sentence_tokenspans.append((sent_startindex, global_token_index-1))
        sent_startindex = global_token_index
        sent_number += 1
        # Constituency_tree
        constituency_tree_string = current_constituency_tree_string_builder.flush()
        constituency_tree_strings.append(constituency_tree_string)
        current_constituency_tree_string_builder = ConstituencyTreeNodeCONLL2012StringRepresentationBuilder()
        # Predicate/arguments
        if buffer_predarg == [[]]: # no pred/arg for the sentence
            buffer_predarg = []
        sentence_predarg_tokenspans.append(buffer_predarg)
        # Re-init variables
        is_first_word = True
        speaker_added = False
        accum_predarg = None
        buffer_predarg = []
        previous_number = len(tokens_rawspans)
        sentence_opened = False
        return (sent_startindex, current_constituency_tree_string_builder, is_first_word, speaker_added, 
                accum_predarg, buffer_predarg, previous_number, sentence_opened, sent_number)
    
    @staticmethod
    def _format_document_ident(conll2012_document_ident, part_string):
        """ Creates a document ident whose value is appropriate, whether the document is part of a 
        multidocument and the document ident should incorporate its index in the resulting document 
        collection; or the document is standalone, and its ident should not include elements that 
        would let think that this document is part of a multidocument.
        
        Args:
            conll2012_document_ident: string, the base ident that can be found in document files 
                formatted using the CONLL2012 format
            part_string: string, the element whose value can indicate whether the document is part 
                of a multidocument, or not (if its value is equal to '*').

        Returns:
            a string, the appropriate document ident w.r.t. the input values  
        """
        if part_string == "*":
            return conll2012_document_ident
        try:
            _ = int(part_string)
        except ValueError:
            format_string = "__{}"
        else:
            format_string = "__part__{}"
        return "{}{}".format(conll2012_document_ident, format_string.format(part_string))
    
    @classmethod
    def _parse_line_iterator(cls, line_iterator):
        """ Parses an iterator over lines representing a document formatted using the CONLL2012 format.
        
        Args:
            line_iterator: iterator over lines representing a document formatted using the 
                CONLL2012 format

        Returns:
            a Document instance, the parsed document
        """
        # Initialize temporary parsing variables
        current_document_ident_data = None
        
        sent_number = 1
        previous_number = 0 # number of tokens in previous sentences: only used to compute token ids
        
        global_token_index = 0 # Word_text global_token_index (i.e. token global_token_index in the list of tokens constituting the text)
        sent_startindex = 0
        
        info = OrderedDict()
        is_first_word = True
        speaker_added = False
        current_constituency_tree_string_builder = ConstituencyTreeNodeCONLL2012StringRepresentationBuilder()
        accum_predarg = None # (start, type) list
        buffer_predarg = [] # (start, end, type) list # memorizes pred/args at sentence level
        entities_mentions_opening_token_indices = defaultdict(list) # dict[id] = start list (as LIFO)
        sentence_tokenspans = [] # (start token index, end token index) list
        constituency_tree_strings = [] # constituency_tree_strings list (constituency_tree in parenthesis format)
        sentences_rawspans = [] # (start, end) list: corresponding span in raw text
        sentence_predarg_tokenspans = [] # (start token index, end token index, type) list list (a list of pred-args per sentence)
        sentence_predarg_rawspans = [] # (start, end, type) list list (a list of pred-args per sentence)
        tokens_rawspans = [] # (start, end) list: corresponding span in raw text
        token2id = [] # list[token_rank] = sentence
        
        raw_text = ""
        word_texts = [] # word_texts list
        lemmas = [] # lemmas list
        POS_tags = [] # POS_tags list
        current_ne_stack = [] # (start token index, type) stack
        ne_tokenspans = [] # (start token index, end token index, type) list
        ne_rawspans = [] # (start, end, type) list: corresponding span in raw text
        current_coref_stack = []
        coref_tokenspans = [] # (start, end, id) list
        mentions_rawspans = [] # (start, end, id) list: corresponding span in raw text
        sentence_speakers = []
        
        sentence_opened = False
        
        # Carry out the parsing
        line_nb = -1
        for line_nb, line in enumerate(line_iterator, 1):
            line = line.strip()
            if line == "": # Reached end of the sentence
                if sent_startindex < global_token_index:
                    (sent_startindex, current_constituency_tree_string_builder, is_first_word, 
                     speaker_added, accum_predarg, buffer_predarg, previous_number, sentence_opened, 
                     sent_number) = cls._process_end_of_parsed_sentence(sentence_tokenspans, sent_startindex, 
                                                                         global_token_index, current_constituency_tree_string_builder, 
                                                                         constituency_tree_strings, buffer_predarg, 
                                                                         sentence_predarg_tokenspans, 
                                                                         tokens_rawspans, sent_number)
                else:
                    message = "Trying to create a sentence (which would be n°{}, starting from "\
                              "sentence n°1) from empty data.\nMake sure there is no superfluous empty "\
                              "line at the end of the line iterator."
                    message = message.format(sent_number)
                    raise IncorrectCONLL2012FormattedLineIteratorError(line_nb, message)
                continue
            
            # We stipulate that we are currently inside a sentence
            sentence_opened = True
            
            # If it is a proper word
            line_items = line.split()#.split("\t")
            L = len(line_items)
            if L < 12: # Error in annotation: document par is skipped
                msg = "Number of columns ({}) of line n°{} is inferior to 12, incorrect CONLL2012 formated line_iterator:\n{}"
                msg = msg.format(L, line_nb, repr(line))
                raise IncorrectCONLL2012FormattedLineIteratorError(line_nb, msg)
            
            # Fetch the eleven first elements
            (conll2012_document_ident, part_string, _, word_text, POS_tag, parse_bit, predicate_lemma, 
             _, _, speaker_author, named_entity_types_string) = line_items[:11] # pred_frameset_id, word_sense
            coreference_entity_ids_string = line_items[-1]
            predicate_arguments = line_items[11:-1]
            
            # Document hierarchy and part nb
            line_document_ident_data = (conll2012_document_ident, part_string)
            if current_document_ident_data is None:
                current_document_ident_data = line_document_ident_data
            if current_document_ident_data != line_document_ident_data:
                msg = "'document_ident_data' value ({}, line n°{}) is inconsistent with previously "\
                      "held value ({}), is this really a single (multi)document?"
                msg = msg.format(current_document_ident_data, line_nb, line_document_ident_data)
                raise IncorrectCONLL2012FormattedLineIteratorError(line_nb, msg)
            
            # Token span in raw text / token2sentence
            start = len(raw_text) + 1 # because of previous " " or "\n" or something else one would like to add before raw text
            end = start + len(word_text) - 1
            tokens_rawspans.append((start,end))
            
            token_id_in_sent = len(tokens_rawspans)-previous_number
            token2id.append((sent_number, token_id_in_sent))
            
            # Raw text string construction & sentence span in raw text
            to_add = " "
            if is_first_word:
                to_add = "\n"
            raw_text += to_add
            raw_text += word_text
            
            # Word itself
            word_texts.append(word_text)
            
            # Part-of-Speech
            POS_tags.append(POS_tag)
            
            # Parse bit
            constituency_tag = parse_bit.strip()
            POS_tag_and_leaf_word_pairs = None
            if constituency_tag != "-":
                POS_tag_and_leaf_word_pairs = (POS_tag, word_text)
            current_constituency_tree_string_builder.add_tag(constituency_tag, 
                                                             POS_tag_and_leaf_word_pairs=POS_tag_and_leaf_word_pairs) # Build the string representation of the ConstituencyTree
            
            # Predicate lemmas
            lemma = word_text # If no lemmas is given, use the word_text itself
            if predicate_lemma != "-":
                lemma = predicate_lemma.strip()
            lemmas.append(lemma)
            
            # Predicate Frameset ID
            # TODO
            # Word sense
            # TODO
            
            # Speaker/Author
            if not speaker_added:
                sentence_speakers.append(speaker_author)
                speaker_added = True
            
            # Named Entities
            named_entity_types_string = named_entity_types_string.strip()
            if re.match("^(\*|-|){1}$", named_entity_types_string) is None:
            #if named_entity_types_string != "*":
                named_entity_type_strings = named_entity_types_string.split("|")
                for named_entity_type_string in named_entity_type_strings:
                    ne_type = named_entity_type_string.strip("()* ")
                    # If we begin a new named entity, we add it to the stack
                    if named_entity_type_string.startswith("("):
                        if ne_type == "":
                            msg = "Beginning of empty type named entity (line n°{})"
                            msg = msg.format(line_nb)
                            raise IncorrectCONLL2012FormattedLineIteratorError(line_nb, msg)
                        current_ne = (global_token_index, ne_type)
                        current_ne_stack.append(current_ne)
                    # If we end a named entity, check its type is consistent with the one of the last named entity that has been opened, and then add it to the parsed data
                    if named_entity_type_string.endswith(")"):
                        # Check that a named entity has been opened
                        try:
                            previous_ne = current_ne_stack.pop()
                        except IndexError:
                            msg = "Trying to close a named entity '{}', but there is no currently opened named entity (line n°{})"
                            msg = msg.format(ne_type, line_nb)
                            raise IncorrectCONLL2012FormattedLineIteratorError(line_nb, msg) from None
                        # Check that types are consistent
                        previous_global_token_index, previous_ne_type = previous_ne
                        if ne_type != "" and ne_type != previous_ne_type:
                            msg = "Trying to close a '{}' type named entity, but the last opened named entity is of '{}' type instead (line n°{})"
                            msg = msg.format(ne_type, previous_ne_type, line_nb)
                            raise OverlappingNamedEntitiesError(line_nb, msg) from None
                        kept_ne_type = ne_type if ne_type != "" else previous_ne_type
                        # Add it to the parsed data
                        ne_tokenspans.append((previous_global_token_index, global_token_index, kept_ne_type, None)) # start, end, type, None_for_subtype
            '''
            # Check consistency of order in which named entities are opened and closed, in case of nested named entities
            # Not implemented for now because this consistency is not enforced for some corpora that 
            # this class is supposed to parse, and so it would not be possible to parse them using this class
            if re.match("^(\*|-|){1}$", named_entity_types_string) is None:
            #if named_entity_types_string != "-":
                named_entity_type_strings = named_entity_types_string.split("|")
                for named_entity_type_string in named_entity_type_strings:
                    named_entity_type = named_entity_type_string.strip("()* ")
                    named_entities_opening_token_indices_ = named_entities_opening_token_indices[named_entity_type]
                    # If we begin a named entity, add its token index to the current stack
                    if named_entity_type_string.startswith("("):
                        named_entities_opening_token_indices_.append(global_token_index)
                    # If we end a named entity, check its type is consistent with the one of the last 
                    # named entity that has been opened, and then add it to the parsed data
                    if named_entity_type_string.endswith(")"):
                        # Check that a named entity has been opened
                        if not named_entities_opening_token_indices_:
                            msg = "Missing start tag for named entity '{}' (line n°{})".format(coreference_entity_id, line_nb)
                            raise IncorrectCONLL2012FormattedLineIteratorError(line_nb, msg)
                        # Assuming there is no named entity who incompletely overlaps with another, 
                        # if we close for a given named entity, then the token index that opened the 
                        # named entity is the last added to the list in the 'current_coref' dict
                        named_entity_first_token_index = named_entities_opening_token_indices_.pop()
                        named_entity_last_token_index = global_token_index
                        ne_tokenspans.append((named_entity_first_token_index, named_entity_last_token_index, named_entity_type))
                        if not named_entities_opening_token_indices_:
                            named_entities_opening_token_indices.pop(named_entity_type)
            '''
            
            # Predicated Arguments
            L = len(predicate_arguments)
            if L > 0: # Annotated predicate-arguments
                if accum_predarg is None:
                    accum_predarg = [[] for i in range(L)]
                    buffer_predarg = [[] for i in range(L)]
                for i, predarg in enumerate(predicate_arguments):
                    if predarg != "*":
                        if "(" in predarg:
                            for s in predarg.split("(")[1:]:
                                accum_predarg[i].append( (global_token_index, str(s.strip("()*"))) )
                        if ")" in predarg:
                            for _ in range(predarg.count(")")):
                                (last_index, last_predarg) = accum_predarg[i].pop()
                                buffer_predarg[i].append( (last_index, global_token_index, last_predarg) ) # start, end, type
            
            # Coreference entity
            coreference_entity_ids_string = coreference_entity_ids_string.strip()
            '''
            # Check consistency of order in which mentions are opened and closed, in case of nested mentions
            # Not implemented for now because this consistency is not enforced for some corpora that 
            # this class is supposed to parse, and so it would not be possible to parse them using this class
            if re.match("^(\*|-|){1}$", coreference_entity_ids_string) is None:
            #if coreference_entity_ids_string != "*":
                coreference_entity_id_strings = coreference_entity_ids_string.split("|")
                for coreference_entity_id_string in coreference_entity_id_strings:
                    coreference_entity_id = coreference_entity_id_string.strip("()* ")
                    #coreference_entity_id = int(coreference_entity_id)
                    # If we begin a new named entity, we add it to the stack
                    if coreference_entity_id_string.startswith("("):
                        if coreference_entity_id == "":
                            msg = "Beginning of empty coreference id mention (line n°{})"
                            msg = msg.format(line_nb)
                            raise IncorrectCONLL2012FormattedLineIteratorError(line_nb, msg)
                        current_coreference_entity = (global_token_index, coreference_entity_id)
                        current_coref_stack.append(current_coreference_entity)
                    # If we end a named entity, check its type is consistent with the one of the 
                    # last named entity that has been opened, and then add it to the parsed data
                    if coreference_entity_id_string.endswith(")"):
                        # Check that a named entity has been opened
                        try:
                            previous_coreference_entity = current_coref_stack.pop()
                        except IndexError:
                            msg = "Trying to close a mention belonging to the coreference entity n°'{}', "\
                                  "but there is no currently opened mention belonging to this coreference entity (line n°{})"
                            msg = msg.format(coreference_entity_id, line_nb)
                            raise IncorrectCONLL2012FormattedLineIteratorError(line_nb, msg) from None
                        # Check that types are consistent
                        previous_global_token_index, previous_coreference_entity_id = previous_coreference_entity
                        if coreference_entity_id != "" and coreference_entity_id != previous_coreference_entity_id:
                            msg = "Trying to close a mention belonging to coreference entity n°{}', "\
                                  "but the last opened mention belongs to the coreference entity n°'{}' instead (line n°{})"
                            msg = msg.format(coreference_entity_id, previous_coreference_entity_id, line_nb)
                            raise OverlappingCoreferenceEntitiesError(line_nb, msg) from None
                        # Add it to the parsed data
                        coref_tokenspans.append((previous_global_token_index, global_token_index, previous_coreference_entity_id)) # start, end, id
            '''
            if re.match("^(\*|-|){1}$", coreference_entity_ids_string) is None:
            #if coreference_entity_ids_string != "-":
                coreference_entity_id_strings = coreference_entity_ids_string.split("|")
                for coreference_entity_id_string in coreference_entity_id_strings:
                    coreference_entity_id = coreference_entity_id_string.strip("()* ")
                    coreference_entity_id = int(coreference_entity_id)
                    mentions_opening_token_indices = entities_mentions_opening_token_indices[coreference_entity_id]
                    if coreference_entity_id_string.startswith("("):
                        mentions_opening_token_indices.append(global_token_index)
                    # If we end a mention entity
                    if coreference_entity_id_string.endswith(")"):
                        # Check that a named entity has been opened
                        if not mentions_opening_token_indices:
                            msg = "Missing start tag for coreference entity '{}' (line n°{})".format(coreference_entity_id, line_nb)
                            raise IncorrectCONLL2012FormattedLineIteratorError(line_nb, msg)
                        # Assuming there is no mention who incompletely overlaps with another, if we close for a given entity,
                        # then the token index that opened the mention is the last added to the list in the 'current_coref' dict
                        mention_first_token_index = mentions_opening_token_indices.pop()
                        mention_last_token_index = global_token_index
                        coref_tokenspans.append((mention_first_token_index, mention_last_token_index, coreference_entity_id))
                        if not mentions_opening_token_indices:
                            entities_mentions_opening_token_indices.pop(coreference_entity_id)
            
            # End
            global_token_index += 1
            is_first_word = False
        
        # Now that we have parsed each lines, we must synthesize the data, and build the Document instance from it
        ## If we were inside a sentence, we must now close it
        if sentence_opened:
            (sent_startindex, current_constituency_tree_string_builder, is_first_word, 
             speaker_added, accum_predarg, buffer_predarg, previous_number, sentence_opened, 
                 sent_number) = cls._process_end_of_parsed_sentence(sentence_tokenspans, sent_startindex, 
                                                                     global_token_index, 
                                                                     current_constituency_tree_string_builder, 
                                                                     constituency_tree_strings, buffer_predarg, 
                                                                     sentence_predarg_tokenspans, 
                                                                     tokens_rawspans, sent_number)
        
        if line_nb == -1:
            msg = "The line_iterator was empty, no document to parse."
            raise ValueError(msg)
        
        if not sentence_tokenspans:
            msg = "No sentence were parsed. Was the line_iterator correct?"
            raise ValueError(msg)
        
        ## Synthesize the data
        ### Document id
        document_ident = cls._format_document_ident(*current_document_ident_data)
        ### Sentence raw span
        for (start_token_index, end_token_index) in sentence_tokenspans:
            start_token_exent = tokens_rawspans[start_token_index]
            end_token_extent = tokens_rawspans[end_token_index]
            sentence_begin_extent = start_token_exent[0]
            sentence_end_extent = end_token_extent[1]
            sentences_rawspans.append((sentence_begin_extent, sentence_end_extent))
        ### Named Entities raw span
        for (start_token_index, end_token_index, ne_type, ne_subtype) in ne_tokenspans:
            ne_rawspans.append((tokens_rawspans[start_token_index][0], tokens_rawspans[end_token_index][1], ne_type, ne_subtype))
        ### Predicated arguments raw span
        for list_predarg in sentence_predarg_tokenspans:
            list_predarg_raw = []
            for l_tok in list_predarg:
                l_raw = []
                for (start, end, pa_type) in l_tok:
                    l_raw.append((tokens_rawspans[start][0], tokens_rawspans[end][1], pa_type))
                if l_raw != []: # corresponds to empty "predicate-arguments" column
                    list_predarg_raw.append(l_raw)
            sentence_predarg_rawspans.append(list_predarg_raw)
        ### Coreference raw span
        for (start, end, coreference_entity_id) in coref_tokenspans:
            #coreference_entity_id = int(coreference_entity_id)
            mentions_rawspans.append((tokens_rawspans[start][0], tokens_rawspans[end][1], coreference_entity_id))
        
        ## Create the Document instance and DocumentBuffer instance
        document = Document(document_ident, raw_text, info=info)
        document_buffer = DocumentBuffer(document)
        ### Add named entities
        named_entities_data = OrderedDict()
        for start, end, ne_type, ne_subtype in ne_rawspans:
            extent = (start, end)
            ident = "{},{}".format(*extent)
            named_entity_data = {"ident": ident, "extent": extent, "type": ne_type} #, "origin": "conll2012_corpus"
            if ne_subtype != "None":
                named_entity_data["subtype"] = ne_subtype
            named_entities_data[extent] = named_entity_data
        document_buffer.add_named_entities(named_entities_data)
        
        ### Add tokens
        tokens_data = OrderedDict()
        for i, extent in enumerate(tokens_rawspans):
            sentence_id, token_id = token2id[i]
            ident = "{}-{}".format(sentence_id, token_id)
            raw_text = word_texts[i]
            POS_tag = POS_tags[i]
            lemma = lemmas[i]
            token_data = {"ident": ident, "extent": extent, "raw_text": raw_text, "POS_tag": POS_tag, "lemma": lemma}
            tokens_data[extent] = token_data
        document_buffer.add_tokens(tokens_data)
        
        ### Add mentions and coreference entities data
        mentions_data = OrderedDict()
        entity_ident2mention_extents = defaultdict(list)
        for (start, end, coreference_entity_id) in mentions_rawspans:
            ident = "{},{}".format(start, end)
            extent = (start, end)
            mention_data = {"ident": ident, "extent": extent} # , "head_extent": head_extent
            mentions_data[extent] = mention_data
            entity_ident2mention_extents[coreference_entity_id].append(extent)
        document_buffer.add_mentions(mentions_data)
        
        ### Add sentences
        def _is_invalid_constituency_tree_string(constituency_tree_string):
            s = constituency_tree_string.replace(" ", "")
            return len(s) if s == "-"*len(s) else 0
        
        sentences_data = OrderedDict()
        for i, extent in enumerate(sentences_rawspans):
            ident = str(i+1)
            constituency_tree_string = constituency_tree_strings[i]
            is_invalid_constituency_tree_string = _is_invalid_constituency_tree_string(constituency_tree_string)
            if is_invalid_constituency_tree_string:
                tokens_nb = is_invalid_constituency_tree_string + 1
                constituency_tree_string = "(TOP (NOPARSE {}))".format(" ".join("(- token{})".format(t) for t in range(tokens_nb)))
            predicate_arguments = tuple(tuple(l) for l in sentence_predarg_rawspans[i])
            speaker = sentence_speakers[i]
            sentence_data = {"ident": ident, "extent": extent, "speaker": speaker, 
                             "constituency_tree_string": constituency_tree_string, 
                             "predicate_arguments": predicate_arguments}
            sentences_data[extent] = sentence_data
        document_buffer.add_sentences(sentences_data)
        
        # We do a first flush
        if entity_ident2mention_extents:
            entities = (Entity(mention_extents=m_e, ident=c_id) for c_id, m_e in entity_ident2mention_extents.items())
            try:
                coreference_partition = CoreferencePartition(entities=entities)
            except MentionBelongsToTwoEntitiesCollectionError as e:
                # We do a flush so as to be able to better locate the faulty mention
                document = document_buffer.flush(strict=False)
                exceptions = e.exceptions
                strings = []
                for exception_ in exceptions:
                    mention = document.extent2mention[exception_.mention_extent]
                    sentence_id = document.extent2sentence_index[mention.sentence.extent] + 1
                    string = "The mention '{}' (sentence n°{}) belongs to at least two entities: original entity ident = '{}', second entity ident = '{}'."
                    string = string.format(mention.raw_text, sentence_id, exception_.original_entity_ident, exception_.second_entity_ident)
                    strings.append(string)
                msg = "Document '{}':{}".format(document_ident, "".join("\n"+string for string in strings))
                raise MentionBelongsToTwoEntitiesCollectionError(tuple(), msg) from None
            document_buffer.set_coreference_partition(coreference_partition)
        
        return document_buffer.flush(strict=False)
            




class CONLL2012MultiDocumentReader(object):
    """ Class designed to be able to parse a CONLL2012 formated file documents, or iterator over 
    lines, containing several documents, into the corresponding Document instances collection.
    
    Examples:
        >>> documents = CONLL2012MultiDocumentReader.parse(file_path)
    
        or
        
        >>> with open(file_path, "r") as f:
        >>>     documents = CONLL2012MultiDocumentReader.parse(f)
    """
    
    # Class method(s)
    @classmethod
    def __repr__(cls):
        return "{}()".format(cls.__name__)
    
    @classmethod
    def parse(cls, line_iterator):
        """ Parses the iterator over lines, representing a CONLL2012 formatted multidocument, into a 
        Document instance collection.
        
        Args:
            line_iterator: iterator over lines of a multi-document CONLL2012 formatted file, or 
                string; if string, will be treated as a file path to a multi-document CONLL2012 formatted file

        Returns:
            a Document instances collection
        """
        if isinstance(line_iterator, str):
            file_path = line_iterator
            def _parse_function():
                conll2012_multi_document_line_iterator = iter(FileReader(file_path))
                try:
                    documents = tuple(cls._parse_line_iterator(conll2012_multi_document_line_iterator))
                except IncorrectCONLL2012FormattedLineIteratorError as e:
                    message = "File located at '{}':\n{}".format(file_path, e.args[0])
                    raise IncorrectCONLL2012FormattedLineIteratorError(e.line_nb, message) from None
                except MentionBelongsToTwoEntitiesCollectionError as e:
                    msg = "File located at '{}':\n{}".format(file_path, e.args[0] if e.args else "")
                    raise MentionBelongsToTwoEntitiesCollectionError(e.exceptions, msg) from None
                except OverlappingNamedEntitiesError as e:
                    msg = "File located at '{}':\n{}".format(file_path, e.args[0] if e.args else "")
                    raise OverlappingNamedEntitiesError(e.line_nb, msg) from None
                except OverlappingCoreferenceEntitiesError as e:
                    msg = "File located at '{}':\n{}".format(file_path, e.args[0] if e.args else "")
                    raise OverlappingCoreferenceEntitiesError(e.line_nb, msg) from None
                return documents
        else:
            def _parse_function():
                return cls._parse_line_iterator(line_iterator)
        
        return _parse_function()
    
    @classmethod
    def _parse_line_iterator(cls, conll2012_multi_document_line_iterator):
        """ Creates a generator over the document encoded in the input iterator over lines, which 
        represents a multidocument formatted using the CONLL2012 format.
        
        Args:
            line_iterator: iterator over lines representing a multidocument formatted using the 
                CONLL2012 format

        Yields:
            Document instance, a parsed document
        """
        conll2012_multi_document_line_iterator = IndexedIterator(conll2012_multi_document_line_iterator)
        doc_nb = 0
        for line in conll2012_multi_document_line_iterator:
            if line.startswith("#begin"):
                doc_nb += 1
                conll2012_document_line_iterator = CONLL2012DocumentLineIterator(conll2012_multi_document_line_iterator)
                conll2012_document_reader = CONLL2012DocumentReader()
                try:
                    document = conll2012_document_reader.parse(conll2012_document_line_iterator)
                except IncorrectCONLL2012FormattedLineIteratorError as e:
                    line_nb = conll2012_multi_document_line_iterator.iterated_items_nb
                    msg = "For the document n°{}, line n°{}:\n{}"
                    msg = msg.format(doc_nb, line_nb, e.args[0])
                    raise IncorrectCONLL2012FormattedLineIteratorError(line_nb, msg) from None
                except MentionBelongsToTwoEntitiesCollectionError as e:
                    msg = "For the document n°{}:\n{}".format(doc_nb, e.args[0] if e.args else "")
                    raise MentionBelongsToTwoEntitiesCollectionError(e.exceptions, msg) from None
                except OverlappingNamedEntitiesError as e:
                    line_nb = conll2012_multi_document_line_iterator.iterated_items_nb
                    msg = "For the document n°{}, line n°{}:\n{}"
                    msg = msg.format(doc_nb, line_nb, e.args[0])
                    raise OverlappingNamedEntitiesError(line_nb, msg) from None
                except OverlappingCoreferenceEntitiesError as e:
                    line_nb = conll2012_multi_document_line_iterator.iterated_items_nb
                    msg = "For the document n°{}, line n°{}:\n{}"
                    msg = msg.format(doc_nb, line_nb, e.args[0])
                    raise OverlappingCoreferenceEntitiesError(line_nb, msg) from None
                yield document
                continue
            raise StopIteration()


# Utilities
class CONLL2012DocumentLineIterator(object):
    """Class used to represent a read-only line iterator over a single CONL2012 formatted document 
    (to be used by a CONLL2012DocumentReader) from a line iterator over a multi-document CONLL2012 
    formatted line iterator
    
    Arguments:
        conll2012_multi_document_line_iterator: iterator over lines of a multi-document CONLL2012 
            formatted file
    
    Attributes:
        conll2012_multi_document_line_iterator: the value of the 'conll2012_multi_document_line_iterator' 
            parameter used to initialize the class instance
    """
    def __init__(self, conll2012_multi_document_line_iterator):
        self.conll2012_multi_document_line_iterator = conll2012_multi_document_line_iterator
    
    def __next__(self):
        line = next(self.conll2012_multi_document_line_iterator)
        if line.startswith("#end"):
            raise StopIteration
        return line
    
    def __iter__(self):
        return self

# Exceptions
class IncorrectCONLL2012FormattedLineIteratorError(CoRTeXException):
    """ Exception to be raised when trying to parse a badly CONLL2012 formatted line. 
    
    Arguments:
        line_nb: int, the line number of the offending line
    
    Attributes:
        line_nb: the value of the 'line_nb' parameter used to initialize the class instance
    """
    def __init__(self, line_nb, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.line_nb = line_nb

class OverlappingNamedEntitiesError(CoRTeXException):
    """ Exception to be raised when parsing a CONLL2012 line that introduces overlapping named entities. 
    
    Arguments:
        line_nb: int, the line number of the offending line
    
    Attributes:
        line_nb: the value of the 'line_nb' parameter used to initialize the class instance
    """
    def __init__(self, line_nb, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.line_nb = line_nb

class OverlappingCoreferenceEntitiesError(CoRTeXException):
    """ Exception to be raised when parsing a CONLL2012 line that introduces overlapping coreference 
    entities. 
    
    Arguments:
        line_nb: int, the line number of the offending line
    
    Attributes:
        line_nb: the value of the 'line_nb' parameter used to initialize the class instance
    """
    def __init__(self, line_nb, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.line_nb = line_nb

'''
class InconsistentEntitiesError(CoRTeXException):
    def __init__(self, mention_extent, entity_idents, document_ident, *args, **kwargs):
        self.mention_extent = mention_extent
        self.entity_idents = entity_idents
        self.document_ident = document_ident
    
    def __repr__(self):
        message = "Document '{}': mention whose extent is '{}' belongs to more than one entity: '{}'."
        return message.format(self.document_ident, self.mention_extent, self.entity_idents)

class InconsistentEntitiesCollectionError(CoRTeXException):
    def __init__(self, exceptions, *args, **kwargs):
        self.exceptions = exceptions

class ExceptionCollectionError(CoRTeXException):
    def __init__(self, exceptions):
        self.exceptions = exceptions
'''
