# -*- coding: utf-8 -*-

"""
Defines class used for parsing document files using the CONLL-U format

Modified CONLL-U format

+--------+----------+-------------------------------------------------------------------------------+
| Column | Type     | Description                                                                   |
+========+==========+===============================================================================+
| 1      | ID       | | Word index, integer starting at 1 for each new sentence; may be a range for |
|        |          | | tokens with multiple words.                                                 |
+--------+----------+-------------------------------------------------------------------------------+
| 2      | FORM     | Word form or punctuation symbol.                                              |
+--------+----------+-------------------------------------------------------------------------------+
| 3      | LEMMA    | Lemma or stem of word form.                                                   |
+--------+----------+-------------------------------------------------------------------------------+
| 4      | UPOSTAG  | | Universal part-of-speech tag drawn from our revised version of the Google   |
|        |          | | universal POS tags.                                                         |
+--------+----------+-------------------------------------------------------------------------------+
| 5      | XPOSTAG  | Language-specific part-of-speech tag; underscore if not available.            |
+--------+----------+-------------------------------------------------------------------------------+
| 6      | FEATS    | | List of morphological features from the universal feature inventory or from |
|        |          | | a defined language-specific extension; underscore if not available.         |
+--------+----------+-------------------------------------------------------------------------------+
| 7      | HEAD     | Head of the current token, which is either a value of ID or zero (0).         |
+--------+----------+-------------------------------------------------------------------------------+
| 8      | DEPREL   | | Universal Stanford dependency relation to the HEAD (root iff HEAD = 0) or   |
|        |          | | a defined language-specific subtype of one.                                 |
+--------+----------+-------------------------------------------------------------------------------+
| 9      | COREF    | Info about coreference entity ('DEPS' in the original CONLLU format)          |
+--------+----------+-------------------------------------------------------------------------------+
| 10     | MISC     | Any other annotation.                                                         |
+--------+----------+-------------------------------------------------------------------------------+


Cf http://universaldependencies.org/format.html


Here, 'MISC' will be considered to be a string that will added to the 'UD_feature' value (a dict) of 
a Token (under the '__misc__' key) if it is not empty.
"""

__all__ = ["CONLLUDocumentReader",
           "IncorrectCONLLUFormattedLineIteratorInSentenceError", 
           "IncorrectCONLLUFormattedLineIteratorError", 
           ]

import re
from collections import OrderedDict, defaultdict
import os

from cortex.api.document import Document
from cortex.api.document_buffer import DocumentBuffer
from cortex.api.entity import Entity
from cortex.api.coreference_partition import CoreferencePartition, MentionBelongsToTwoEntitiesCollectionError
from cortex.utils.io import FileReader
from cortex.utils import CoRTeXException




class CONLLUDocumentReader(object):
    """ Class designed to be able to parse a CONLLU formated document file, or iterator over lines, 
    into the corresponding Document instance.
    
    Examples:
        >>> document = CONLLUDocumentReader.parse(file_path)
        
        or
        
        >>> with open(file_path, "r") as f:
        >>>     document = CONLLUDocumentReader.parse(f)
    """
    
    # Class method(s)
    @classmethod
    def __repr__(cls):
        return "{}()".format(cls.__name__)
    
    @classmethod
    def parse(cls, line_iterator):
        """ Parses the CONLLU formatted document representation of a line iterator into a Document 
        instance.
        
        Args:
            line_iterator: iterator over lines of a CONLLU formatted file, or string; if string, 
                will be treated as a file path to a CONLLU formatted file

        Returns:
            a Document instance (will have a 'none' ident if the 'line_iterator' input was not 
            a file path)
        """
        if isinstance(line_iterator, str):
            file_path = line_iterator
            def _parse_function():
                line_iterator = iter(FileReader(file_path))
                try:
                    document = cls._parse_line_iterator(line_iterator)
                except IncorrectCONLLUFormattedLineIteratorError as e:
                    message = "File located at '{}': {}".format(file_path, e.args[0])
                    raise IncorrectCONLLUFormattedLineIteratorError(e.line_id, message) from None
                except MentionBelongsToTwoEntitiesCollectionError as e:
                    msg = "File located at '{}': {}".format(file_path, e.args[0])
                    raise MentionBelongsToTwoEntitiesCollectionError(tuple(), msg)
                
                file_name = os.path.basename(file_path)
                document.ident = file_name
                return document
        else:
            def _parse_function():
                return cls._parse_line_iterator(line_iterator)
        
        return _parse_function()
    
    @classmethod
    def _parse_sentence(cls, line_iterator):
        """ Iterates over the lines of the input line iterator until a complete sentence has been 
        parsed.
        
        Args:
            line_iterator: iterator over lines of a CONLLU formatted file, must not be in the 
                middle of parsing a sentence

        Returns:
            a (tokens_pre_data, named_entities_pre_data, sentence_pre_data, mentions_pre_data, 
            entity_id2mention_tokens_indices_collections, parsed_lines_subnb) tuple
        """
        tokens_pre_data = OrderedDict()
        named_entities_pre_data = OrderedDict()
        mentions_pre_data = OrderedDict()
        sentence_pre_data = OrderedDict()
        entity_id2mention_tokens_indices_collections = defaultdict(list)
        
        parsed_lines_subnb = 0
        for line_id, line in enumerate(line_iterator, 1):
            parsed_lines_subnb += 1
            line = line.strip()
            
            # If we reached the end of the sentence, we stop iterating
            if line == "":
                break
            
            # ... and then we process the data
            line_items = line.split()#.split("\t")
            L = len(line_items)
            if L < 10: # Error in annotation: document par is skipped
                msg = "Number of columns ({}) inferior to 10, incorrect CONLLU formated line_buffer; line:\n{}."
                msg = msg.format(L, repr(line))
                raise IncorrectCONLLUFormattedLineIteratorInSentenceError(line_id, msg)
            
            # Fetch the token data elements
            (word_sentence_id, word_text, predicate_lemma, U_POS_tag, X_POS_tag, features_string, 
             head_deprel_index, deprel_tag, entity_ids_string, misc) = line_items[:10]
            
            # Formatting the token data
            token_sentence_index = int(word_sentence_id)
            lemma = predicate_lemma
            POS_tag = U_POS_tag
            if X_POS_tag != "_":
                POS_tag = X_POS_tag
            ## UD_features
            UD_features = None
            if features_string != "_":
                UD_features = OrderedDict()
                for feature_name_value_string in features_string.strip().split("|"):
                    feature_name, feature_value = feature_name_value_string.split("=")
                    if "," in feature_value:
                        msg = "Multiple values '{}' for the feature '{}' of the token on the line '{}'"
                        msg = msg.format(feature_value, feature_name)
                        raise IncorrectCONLLUFormattedLineIteratorInSentenceError(line_id, msg)
                    UD_features[feature_name] = feature_value
            ### Misc.
            if misc != "_":
                if UD_features is None:
                    UD_features = OrderedDict()
                UD_features["__misc__"] = misc
            
            token_data = {"raw_text": word_text, "lemma": lemma, "POS_tag": POS_tag, 
                          "UD_features": UD_features,
                          }
            tokens_pre_data[token_sentence_index] = token_data
            
            # Formatting the mention / entities data
            if entity_ids_string != "_":
                # Register the entity information
                entity_strings = entity_ids_string.split(",")
                for entity_string in entity_strings:
                    m = re.search("(B|I):coref(\d+)", entity_string)
                    if m is not None:
                        entity_id = int(m.group(2))
                        begin_entity = m.group(1) == "B"
                        if begin_entity:
                            token_sentence_indices_collection = [token_sentence_index]
                            entity_id2mention_tokens_indices_collections[entity_id].append(token_sentence_indices_collection)
                        else:
                            entity_id2mention_tokens_indices_collections[entity_id][-1].append(token_sentence_index)
                
            # Look for named entity info
            if features_string != "_":
                feature_strings = features_string.split("|")
                for feature_string in feature_strings:
                    m = re.search("NE=(.+)", feature_string)
                    if m is not None:
                        ne_type = m.group(1)
                        named_entity_data = {"raw_text": word_text, "type": ne_type, "origin": "conll_corpus"}
                        named_entities_pre_data[token_sentence_index] = named_entity_data
        
        # No predicate arguments to parse for now
        sentence_pre_data["predicate_arguments"] = tuple()
        
        return (tokens_pre_data, named_entities_pre_data, sentence_pre_data, mentions_pre_data, 
                entity_id2mention_tokens_indices_collections, parsed_lines_subnb)
    
    @classmethod
    def _safe_parse_sentences(cls, line_iterator):
        """ Creates a generator over aggregated data representing a sentence parsed from a lines 
        iterator. 
        
        Keeps track of line ident / line nb when parsing the line iterator, so as to be 
        able to pinpoint the problem in case a badly formatted line problem occurs.
        
        Args:
            line_iterator: iterator over lines of a CONLLU formatted file, must not be in the 
                middle of parsing a sentence

        Yields:
            a (tokens_pre_data, named_entities_pre_data, sentence_pre_data, mentions_pre_data, 
            entity_id2mention_tokens_indices_collections, parsed_lines_subnb) tuple
        
        Raises:
            IncorrectCONLLUFormattedLineIteratorError: if an incorrectly 'CONLLU'-formatted line is met
        """
        parsed_lines_nb = 0
        while True:
            try:
                (tokens_pre_data, named_entities_pre_data, sentence_pre_data, mentions_pre_data, 
                 entity_id2mention_tokens_indices_collections, parsed_lines_subnb) = cls._parse_sentence(line_iterator)
            except IncorrectCONLLUFormattedLineIteratorInSentenceError as e:
                line_nb = parsed_lines_nb + e.in_sentence_line_id
                msg = "In line n°{}: {}".format(line_nb, e.args[0])
                raise IncorrectCONLLUFormattedLineIteratorError(line_nb, msg) from None
            else:
                if parsed_lines_subnb < 1:
                    raise StopIteration()
                
                parsed_lines_nb += parsed_lines_subnb
                yield (tokens_pre_data, named_entities_pre_data, sentence_pre_data, mentions_pre_data, 
                       entity_id2mention_tokens_indices_collections, parsed_lines_nb)
    
    @classmethod
    def _parse_line_iterator(cls, line_iterator):
        """ Parses the CONLLU formatted document representation of a line iterator into a Document 
        instance.
        
        Args:
            line_iterator: iterator over lines of a CONLLU formatted file

        Returns:
            a Document instance (will have a 'none' ident if the 'line_iterator' input was not 
            a file path)
        """
        # Initialize temporary parsing variables
        default_document_ident = "conllu_parsed"
        info = OrderedDict()
        
        extent2token_data = {}
        extent2named_entity_data = {}
        extent2mention_data = {}
        extent2sentence_data = {}
        entity_id2mention_extents = defaultdict(list)
        
        raw_text_strings_per_sentence = []
        token_sep_string = " "
        token_sep_string_length = len(token_sep_string)
        sentence_sep_string = "\n"
        sentence_sep_string_length = len(sentence_sep_string)
        in_text_pos = 1
        
        # Carry out the parsing
        for sentence_current_id, (tokens_pre_data, named_entities_pre_data, sentence_pre_data, 
                                  mentions_pre_data, entity_id2mention_tokens_indices_collections, 
                                  parsed_lines_nb) in enumerate(cls._safe_parse_sentences(line_iterator), start=1):
            token_sentence_index2token_extent = {}
            sentence_start_pos = None
            sentence_end_pos = None
            
            if not tokens_pre_data:
                msg = "The line n°{} does not begin a sentence; is this line iterator correctly "\
                      "formatted w.r.t. the CONLLU format?"
                msg = msg.format(parsed_lines_nb)
                raise IncorrectCONLLUFormattedLineIteratorError(parsed_lines_nb, msg)
            
            # Processing tokens data
            raw_text_strings = []
            for token_sentence_index, token_pre_data in tokens_pre_data.items():
                word_text = token_pre_data["raw_text"]
                start = in_text_pos
                raw_text_strings.append(word_text)
                in_text_pos += len(word_text)
                end = in_text_pos - 1
                in_text_pos += token_sep_string_length
                extent = (start, end)
                ident = "{}-{}".format(sentence_current_id, token_sentence_index)
                #del token_pre_data["features_string"] # FIXME: ??
                token_data = token_pre_data
                token_data["ident"] = ident
                extent2token_data[extent] = token_data
                token_sentence_index2token_extent[token_sentence_index] = extent
                if token_sentence_index == 1:
                    sentence_start_pos = extent[0]
            sentence_end_pos = extent[1]
            raw_text_strings_per_sentence.append(raw_text_strings)
            in_text_pos = in_text_pos - token_sep_string_length + sentence_sep_string_length
            
            # Processing named entity data
            for token_sentence_index, named_entity_pre_data in named_entities_pre_data.items():
                named_entity_data = named_entity_pre_data
                extent = token_sentence_index2token_extent[token_sentence_index]
                ident = "{},{}".format(*extent)
                named_entity_data["ident"] = ident
                extent2named_entity_data[extent] = named_entity_pre_data
            
            # Processing the mention definition and coreference entity data
            for entity_id, mention_tokens_indices_collections in entity_id2mention_tokens_indices_collections.items():
                pseudo_entity = entity_id2mention_extents[entity_id]
                for token_sentence_indices in mention_tokens_indices_collections:
                    mention_data = {} # FIXME: add something here? Which may come from 'mentions_pre_data'?
                    first_mention_token_extent = token_sentence_index2token_extent[token_sentence_indices[0]]
                    last_mention_token_extent = token_sentence_index2token_extent[token_sentence_indices[-1]]
                    mention_extent = (first_mention_token_extent[0], last_mention_token_extent[1])
                    pseudo_entity.append(mention_extent)
                    ident = "{},{}".format(*mention_extent)
                    mention_data["ident"] = ident
                    extent2mention_data[mention_extent] = mention_data
            
            # Processing the sentence specific data
            sentence_data = sentence_pre_data
            sentence_extent = (sentence_start_pos, sentence_end_pos)
            ident = "{}".format(sentence_current_id)
            sentence_data["ident"] = ident
            extent2sentence_data[sentence_extent] = sentence_data
        
        ## Synthesize the data
        info["original_corpus_format"] = "conllu"
        ### Document id
        document_id = default_document_ident
        ### Sentence raw span
        raw_text = "{}{}".format("\n", sentence_sep_string.join(token_sep_string.join(raw_text_strings) for raw_text_strings in raw_text_strings_per_sentence))
        
        ## Create the Document instance and DocumentBuffer instance
        document = Document(document_id, raw_text, info=info)
        document_buffer = DocumentBuffer(document)
        ### Add named entities
        document_buffer.add_named_entities(extent2named_entity_data)
        
        ### Add tokens
        document_buffer.add_tokens(extent2token_data)
        
        ### Add mentions
        document_buffer.add_mentions(extent2mention_data)
        
        ### Add sentences
        document_buffer.add_sentences(extent2sentence_data)
        
        ### Add coreference entities data
        if entity_id2mention_extents:
            entities = (Entity(mention_extents=m_e, ident=c_id) for c_id, m_e in entity_id2mention_extents.items())
            try:
                coreference_partition = CoreferencePartition(entities=entities)
            except MentionBelongsToTwoEntitiesCollectionError as e:
                # We do a flush so as to be able to better locate the faulty mention
                document = document_buffer.flush(strict=False)
                exceptions = e.exceptions
                strings = []
                for exception_ in exceptions:
                    mention = document.extent2mention[exception_.mention_extent]
                    sentence_id = document.extent2sentence_index[mention.sentence.extent] + 1
                    string = "The mention '{}' (sentence n°{}) belongs to at least two entities: original entity ident = '{}', second entity ident = '{}'."
                    string = string.format(mention.raw_text, sentence_id, exception_.original_entity_ident, exception_.second_entity_ident)
                    strings.append(string)
                msg = "\n".join(strings)
                raise MentionBelongsToTwoEntitiesCollectionError(tuple(), msg) from None
            document_buffer.set_coreference_partition(coreference_partition)
        
        # FIXME: ERROR, in order to synchronize tokens and sentence during the call to the 'flush' method, correct constituency trees must be defined
        
        return document_buffer.flush(strict=False)


# Exceptions
class IncorrectCONLLUFormattedLineIteratorInSentenceError(CoRTeXException):
    """ Exception to be raised when trying to parse a badly CONLLU formatted line, during the parsing 
    of data related to a sentence.
    
    Arguments:
        in_sentence_line_id: int, the line number of the offending line, relative to the first line 
            representing the sentence, in the midst of whose parsing the offending line was met
    
    Attributes:
        in_sentence_line_id: the value of the 'in_sentence_line_id' parameter used to initialize 
            the class instance
    """
    def __init__(self, in_sentence_line_id, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.in_sentence_line_id = in_sentence_line_id

class IncorrectCONLLUFormattedLineIteratorError(CoRTeXException):
    """ Exception to be raised when trying to parse a badly CONLLU formatted line. 
    
    Arguments:
        line_id: int, the index of the offending line in the line iterator that was being parsed 
            when the offending line was met
    
    Attributes:
        line_id: the value of the 'line_id' parameter used to initialize the class instance
    """
    def __init__(self, line_id, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.line_id = line_id
