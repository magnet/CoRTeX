# -*- coding: utf-8 -*-

"""
Defines classes used for writing document to files using the CONLL2012 format

CONLL2012 FORMAT

+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
|  Column   |      Type               |          Description                                                                                                     |
+===========+=========================+==========================================================================================================================+
|     1     |  Document ID            | This is a variation on the _document filename                                                                            |
+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
|     2     |  Part number            | | Integer, ident of the current document in a potential multidocument:                                                   |
|           |                         | | some files are divided into multiple parts numbered as 000, 001, 002, ... etc.                                         |
|           |                         | | If there is only one document, and so if the concept of part has no meaning, the default value is "*"?                 |
+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
|     3     | Word ID in the sentence |                                                                                                                          |
+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
|     4     | Word itself             | | This is the token as segmented/tokenized in the Treebank. Initially the *_skel                                         |
|           |                         | | file contain the placeholder [WORD] which gets replaced by the actual token from                                       |
|           |                         | | the Treebank which is part of the OntoNotes release.                                                                   |
+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
|     5     | Part-of-Speech          | The default value when there is no available data is "-"?                                                                |
+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
|     6     | Parse bit               | | This is the bracketed structure broken before the first open parenthesis in the parse,                                 |
|           |                         | | and the word/part-of-speech leaf replaced with a *. The full parse can be created by                                   |
|           |                         | | substituting the asterix with the "([pos] [word])" string (or leaf) and concatenating                                  |
|           |                         | | the items in the rows of that column. The default value when there is no available data is "-"?                        |
+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
|     7     | Predicate lemma         | | The predicate lemma is mentioned for the rows for which we have semantic role information.                             |
|           |                         | | All other rows are marked with a "-".                                                                                  |
+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
|     8     | Predicate Frameset ID   | This is the PropBank frameset ID of the predicate in Column 7. The default value when there is no available data is "-". |
+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
|     9     | Word sense              | This is the word sense of the word in Column 3. The default value when there is no available data is "-".                |
+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
|    10     | Speaker/Author          | | This is the speaker or author name where available. Mostly in Broadcast Conversation                                   |
|           |                         | | and Web Log data. The default value when there is no available data is "-".                                            |
+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
|    11     | Named Entities          | | These columns identifies the spans representing various named entities.                                                |
|           |                         | | The default value when there is no available data is "*".                                                              |
+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
|  12:N     | Predicate Arguments     | | For a given sentence, there is one column each of predicate argument structure information for the predicate           |
|           |                         | | mentioned in Column 7. When there is no available information, there should be no column.                              |
|           |                         | | The default value when there is no available data for a given, existing column, is "*".                                |
+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+
|     N     | Coreference             | | Coreference chain information encoded in a parenthesis structure.                                                      |
|           |                         | | The default value when there is no available data is "-".                                                              |
+-----------+-------------------------+--------------------------------------------------------------------------------------------------------------------------+

Cf http://conll.cemantix.org/2012/data.html

Notes:
    This writer is primarily made so that coreference partition prediction on documents can be 
    evaluated using the software provided by the CONLL2012 task. As such, only the raw text data and the 
    coreference entity information are written (and not data about POS tags, for instance...).
"""

__all__ = ["CONLL2012DocumentWriter",
           "CONLL2012MultiDocumentWriter",
           "CONLL2012_DOCUMENT_EXTENSION", 
           "CONLL2012_MULTIDOCUMENT_EXTENSION",
           ]


import os

from cortex.parameters import ENCODING
from cortex.api.markable import MentionDoesNotExistError



CONLL2012_DOCUMENT_EXTENSION = "conll2012"
class CONLL2012DocumentWriter(object):
    """ Class designed to be able to persist a Document instance under the 'CONLL2012' format 
    
    There may be a possible loss of information in the process, as only the separation of tokenization 
    of the document into tokens and sentences, as well as mention position and coreference entity 
    mention composition, will be persisted.
    
    Assume that the document to be written possess tokens, sentences, and that its potential 
    mentions are synchronized with its tokens.
    
    Can be used either to directly write into a file from a given file path, or can be used to write 
    into a string buffer: in that case, the buffer must possess a 'write' method that takes a string 
    as input.
    """
    
    # Class methods
    @classmethod
    def write(cls, document, line_buffer, skip_singleton=True, strict=False, offset=False):
        """ Writes the info about a Document under the CONLL2012 format into the specified line buffer.
        
        Args:
            document: Document instance whose info are to be written under the CONLL2012 format
            line_buffer: string buffer object (must possess a 'write' method), or path under 
                the form "/path/to/folder/document_name", where the CONLL2012file be written (if 
                'document_name' does not end with 'conll2012', then '.conll2012' will be appended 
                to it)
            skip_singleton: boolean(default = True), whether or not skip singleton when writing 
                coreference entities
            strict: boolean; whether or not to raise an Exception when writing if it appears that 
                a mention whose extent is defined in the CoreferencePartition instance does exist in the 
                Document instance; or if the Document instance does not possess mentions at all.
            offset: boolean; whether or not make it so the written token indexation in a sentence 
                begins at 0 (False) or at 1 (True)
        
        Examples:
            >>> CONLL2012DocumentWriter.write(document, "/path/to/folder/document_ident.conll2012")
        
        Warning:
            Only the 'extent' and 'coreference entity' information of mentions will be persisted, all 
            others will be lost.
        """
        # Prepare the writing process to call
        file_path = None
        if isinstance(line_buffer, str):
            # Defining the elements needed to define the file paths
            root_folder_path, file_name = os.path.split(line_buffer)
            if not file_name.endswith(CONLL2012_DOCUMENT_EXTENSION):
                file_name = "{}.{}".format(file_name, CONLL2012_DOCUMENT_EXTENSION)
            file_path = os.path.join(root_folder_path, file_name)
            
            def writing_function(doc):
                with open(file_path, "w", encoding=ENCODING) as f:
                    cls._write_into_line_buffer(doc, f, skip_singleton=skip_singleton, 
                                                strict=strict, offset=offset)
            
            writing_fct = writing_function
        
        else:
            writing_fct = lambda doc: cls._write_into_line_buffer(doc, line_buffer, 
                                                                  skip_singleton=skip_singleton, 
                                                                  strict=strict, offset=offset)
        
        # Call the writing process
        writing_fct(document)
        
        return line_buffer if file_path is None else file_path
    
    @classmethod
    def _write_into_line_buffer(cls, document, line_buffer, skip_singleton=True, strict=False, offset=False):
        """ Writes the info about a Document under the CONLL2012 format into the specified line buffer.
        
        Args:
            document: Document instance whose info are to be written under the CONLL2012 format
            line_buffer: string buffer object (must possess a 'write' method)
            skip_singleton: boolean(default = True), whether or not skip singleton when writing 
                coreference entities
            strict: boolean; whether or not to raise an Exception when writing if it appears that 
                a mention whose extent is defined in the CoreferencePartition instance does not exist in the 
                Document instance; or if the Document instance does no possess mentions at all.
            offset: boolean; whether or not make it so the written token indexation in a sentence 
                begins at 0 (False) or at 1 (True)
        """
        for line in cls._debuilder(document, skip_singleton=skip_singleton, strict=strict, offset=offset):
            line_buffer.write(line+"\n")
    
    @classmethod
    def _debuilder(cls, document, skip_singleton=True, strict=False, offset=False):
        """ Creates an generator over CONLL2012 formatted lines representing the input document.
        
        Args:
            document: Document instance whose info are to be written under the CONLL2012 format
            skip_singleton: boolean(default = True), whether or not skip singleton when writing 
                coreference entities
            strict: boolean; whether or not to raise an Exception when writing if it appears that 
                a mention whose extent is defined in the CoreferencePartition instance does not exist in the 
                Document instance; or if the Document instance does no possess mentions at all.
            offset: boolean; whether or not make it so the written token indexation in a sentence 
                begins at 0 (False) or at 1 (True)
        
        Yields:
            string, a conll2012-formatted line
        """
        offset = int(offset)
        # Prepare document id part
        document_hierarchy = document.info.get("original_corpus_hierarchy", document.ident)
        # Make dictionaries to encode coreference data information
        token_extent2mention_start = {} # dict[token_id] = entity_num
        token_extent2mention_end = {} # dict[token_id] = entity_num
        token_extent2mention_one = {} # dict[token_id] = entity_num / only for one-token-mentions
        coreference_partition = document.coreference_partition
        if coreference_partition is not None:
            if strict:
                if document.extent2mention is None:
                    msg = "The document to be written, {}, possess no mention, yet has been attributed a coreference partition to be written."
                    msg = msg.format(document)
                    raise ValueError(msg)
                
                def _mention_extent2first_last_token_extents_strict(mention_extent):
                    try:
                        mention = document.extent2mention[mention_extent]
                    except KeyError:
                        msg = "No mention corresponding to extent '{}' in {}."
                        msg = msg.format(mention_extent, document)
                        raise MentionDoesNotExistError(msg) from None
                    tokens = sorted(mention.tokens) # tokens should be sorted
                    first_token_extent = tokens[0].extent
                    last_token_extent = tokens[-1].extent
                    return first_token_extent, last_token_extent
                mention_extent2first_last_token_extents_fct = _mention_extent2first_last_token_extents_strict
            else:
                start2token = {token.extent[0]: token for token in document.tokens}
                end2token = {token.extent[1]: token for token in document.tokens}
                def _mention_extent2first_last_token_extents_not_strict(mention_extent):
                    start, end = mention_extent
                    try:
                        first_token = start2token[start]
                        last_token = end2token[end]
                    except KeyError:
                        msg = "The supposedly '{}' mention extent does not correspond to a group of tokens in the {}."
                        msg = msg.format(mention_extent, document)
                        raise ValueError(msg) from None
                    first_token_extent = first_token.extent
                    last_token_extent = last_token.extent
                    return first_token_extent, last_token_extent
                mention_extent2first_last_token_extents_fct = _mention_extent2first_last_token_extents_not_strict
            
            for entity_num, entity in enumerate(sorted(document.coreference_partition)):
                if len(entity) > 1 or not skip_singleton:
                    for mention_extent in entity:
                        first_token_extent, last_token_extent = mention_extent2first_last_token_extents_fct(mention_extent)
                        if first_token_extent == last_token_extent:
                            token_extent2mention_one[first_token_extent] = token_extent2mention_one.get(first_token_extent,[]) + [entity_num] # concatenate the list of mention extent whom the token already belongs to and a list with the ident of the mention it belongs to (nested mentions)
                        else:
                            token_extent2mention_start[first_token_extent] = token_extent2mention_start.get(first_token_extent,[]) + [entity_num]
                            token_extent2mention_end[last_token_extent] = token_extent2mention_end.get(last_token_extent,[]) + [entity_num]
        # Create the lines
        for sentence in document.sentences:
            # Need maxlength for line formatting
            raw_text_max_length = 0
            for token in sentence.tokens:
                raw_text_max_length = max(raw_text_max_length, len(token.raw_text))
            
            document_hierarchy_str_fct = lambda x: str(x)
            part_number_str_fct = lambda: "*"
            token_id_str_fct = lambda x: "{:d}".format(x)
            raw_text_str_fct = lambda x: "{:s}".format(x)
            
            # Actual writing
            for token_id, token in enumerate(sentence.tokens, start=offset):
                string_array = []
                # Document id
                string_array.append(document_hierarchy_str_fct(document_hierarchy))
                # Document part number
                string_array.append(part_number_str_fct())
                # Token's rank in sentence
                string_array.append(token_id_str_fct(token_id))
                # Word
                string_array.append(raw_text_str_fct(token.raw_text))
                '''
                # Passing other attributes
                for _ in range(7):
                    string_array.append("*") #"-"
                '''
                # POS tag: nothing
                string_array.append("-")
                # Parse tag: nothing
                string_array.append("-")
                # Predicate lemma: nothing
                string_array.append("-")
                # Predicate frameset ID: nothing
                string_array.append("-")
                # Word sense ID: nothing
                string_array.append("-")
                # Speaker/author: nothing
                string_array.append("-")
                # Named entity: nothing
                string_array.append("*")
                # Predicate arguments: noting
                pass
                # Coreference entity data
                t_extent = token.extent
                nothing = True
                start = False
                one = False
                string_subarray = []
                if t_extent in token_extent2mention_start:
                    nothing = False
                    start = True
                    string_subarray.append( "|".join("("+str(x) for x in token_extent2mention_start[t_extent]) )
                if t_extent in token_extent2mention_one:
                    nothing = False
                    one = True
                    if start:
                        string_subarray.append("|")
                    string_subarray.append( "|".join("("+str(x)+")" for x in token_extent2mention_one[t_extent]) )
                if t_extent in token_extent2mention_end:
                    nothing = False
                    if start or one:
                        string_subarray.append("|")
                    string_subarray.append( "|".join(str(x)+")" for x in token_extent2mention_end[t_extent]) )
                coreference_entity_string = "-"
                if not nothing:
                    coreference_entity_string = "".join(string_subarray)
                string_array.append(coreference_entity_string)
                
                yield "\t".join(string_array)
            
            yield "" # Empty line represents the end of a sentence
                
        

CONLL2012_MULTIDOCUMENT_EXTENSION = "multi_conll2012"
class CONLL2012MultiDocumentWriter(object):
    """ Class designed to be able to persist a Document instances collection under the 'CONLL2012' 
    format.
    
    There may be a possible loss of information, as only the separation of tokenization of the documents 
    into tokens and sentences, as well as mention position and coreference entity mention composition, 
    will be persisted.
    
    Assume that the documents to be written possess tokens, sentences, and that their respective 
    potential mentions are synchronized with their respective tokens.
    
    Can be used either to directly write into a file from a given file path, or can be used to write 
    into a string buffer: in that case, the buffer must possess a 'write' method that takes a string 
    as input.
    """
    
    # Class methods
    @classmethod
    def write(cls, documents, line_buffer, skip_singleton=True, strict=False, offset=False):
        """ Writes the info about the several Document instances under the CONLL2012format in the input 
        line buffer, as a multi-document CONL2012 formatted content.
        
        Args:
            documents: iterable over Document instances whose info are to be written under the 
                CONLL2012 format
            line_buffer: string buffer object (must possess a 'write' method), or path under 
                the form "/path/to/folder/multi_document_file_name", where the conll2012 file be 
                written (if 'multi_document_file_name' does not end with 'multi_CONLL2012', 
                then '.multi_CONLL2012' will be appended to it)
            skip_singleton: boolean(default = True), whether or not skip singleton when writing 
                coreference entities
            strict: boolean; whether or not to raise an Exception when writing if it appears that 
                a mention whose extent is defined in the CoreferencePartition instance does exist 
                in the Document instance
            offset: boolean; whether or not make it so the written token indexation in a sentence begins 
                at 0 (False) or at 1 (True)
        
        Example<:
            >>> CONLL2012MultiDocumentWriter.write(documents, "/path/to/folder/documents_ident.multi_conll2012")
        
        Warning:
            Only the 'extent' and 'coreference entity' information of mentions will be persisted in the 
            input line_buffer
        """
        file_path = None
        if isinstance(line_buffer, str):
            # Defining the elements needed to define the file paths
            root_folder_path, file_name = os.path.split(line_buffer)
            if not file_name.endswith(CONLL2012_MULTIDOCUMENT_EXTENSION):
                file_name = "{}.{}".format(file_name, CONLL2012_MULTIDOCUMENT_EXTENSION)
            file_path = os.path.join(root_folder_path, file_name)
            
            def writing_fct(documents):
                with open(file_path, "w", encoding=ENCODING) as f:
                    cls._write_into_line_buffer(documents, f, skip_singleton=skip_singleton, 
                                                strict=strict, offset=offset)
            writing_fct = writing_fct
        
        else:
            writing_fct = lambda documents: cls._write_into_line_buffer(documents, line_buffer, 
                                                                        skip_singleton=skip_singleton, 
                                                                        strict=strict, offset=offset)
        
        writing_fct(documents)
        
        return line_buffer if file_path is None else file_path
    
    @classmethod
    def _write_into_line_buffer(cls, documents, line_buffer, skip_singleton=True, strict=False, offset=False):
        """ Writes the info about the several Document instances under the CONLL2012format in the input 
        line buffer, as a multi-document CONL2012 formatted content.
        
        Args:
            documents: iterable over Document instances
            line_buffer: string buffer object object, which must possess a 'write' method
            skip_singleton: boolean(default = True), whether or not skip singleton when writing 
                coreference entities
            strict: boolean; whether or not to raise an Exception when writing if it appears that 
                a mention whose extent is define in the CoreferencePartition instance does exist in the 
                Document instance
            offset: boolean; whether or not make it so the written token indexation in a sentence begins 
                at 0 (False) or at 1 (True)
        """
        for document in documents:
            # Write header
            line_buffer.write("#begin document {}\n".format(document.ident))
            # Write content
            CONLL2012DocumentWriter.write(document, line_buffer, skip_singleton=skip_singleton, 
                                          strict=strict, offset=offset)
            # write end
            line_buffer.write("#end document\n")
