# -*- coding: utf-8 -*-

"""
Defines utilities to persist or parse corpora / collections of documents.
"""

__all__ = ["write_corpus_file", "write_corpus", 
           "parse_corpus_file", "parse_corpus_documents", "parse_corpus",
           "persist_as_pivot_files_folders", 
           "persist_as_conll2012_document_files", 
           "persist_as_conll2012_multidocument_file", 
           ]

import os
import logging
import json
import re
from collections import OrderedDict

from .conll2012_reader import CONLL2012DocumentReader, CONLL2012MultiDocumentReader
from .conll2012_writer import CONLL2012DocumentWriter, CONLL2012MultiDocumentWriter
from .pivot_writer import PivotWriter
from .pivot_reader import PivotReader
from .conllu_reader import CONLLUDocumentReader
from cortex.api.corpus import Corpus
from cortex.parameters import ENCODING
from cortex.utils.display import get_display_progress_function
from cortex.utils import CoRTeXException



###############################
# Persist documents utilities #
###############################
def persist_as_pivot_files_folders(documents_iterable, document2path_fct, root_path=None, 
                                   skip_singleton=True, strict=False, verbose=False, logger=logging):
    """ Persists documents from an iterable, under the pivot format.
    
    Args:
        documents_iterable: iterable over Document instances
        document2path_fct: function that takes a Document instance as an input, and 
            outputs the path to the folder where the files representing the document will be written. 
            If the input 'root_path' value is not None, the path that one got from the 
            function will be considered to be relative to the input root path value, and the path used in 
            fine to save the documents will be rebuilt paths produced by the 'document2path_fct' parameter 
            are relative to. 
            If the input 'root_path' value is None, they will be considered to be absolute.
        skip_singleton: boolean( default = True), whether or not skip singleton when writing 
            coreference entities
        strict: boolean; whether or not to raise an Exception when writing if it appears that 
            a mention whose extent is defined in the CoreferencePartition instance does exist in the 
            Document instance
        verbose: whether or not to produce logs indicating the progress for persisting the 
            Document instances from the input iterator
        logger: logger to use when displaying progress for persisting the Document instances from 
            the input iterator
    
    Returns:
        collection of paths where each document of the input iterable was persisted; will be relative 
        paths to the input 'root_path' parameter value if it is not None. Can be used to load the 
        documents back, using the proper parsing tool.
    """
    return _persist_as_conll2012_file_or_pivot_files(documents_iterable, PivotWriter, document2path_fct, 
                                                      root_path=root_path, skip_singleton=skip_singleton, 
                                                      strict=strict, verbose=verbose, logger=logger)


def persist_as_conll2012_document_files(documents_iterable, document2path_fct, root_path=None, 
                                        skip_singleton=False, strict=False, verbose=False, logger=logging):
    """ Persists documents from an iterable, under the conll2012 format (one document per file).
    
    Args:
        documents_iterable: iterable over Document instances
        document2path_fct: function that takes a Document instance as an input, and 
            outputs the path to the file where the document will be written. 
            If 'root_path' is input as well, the path that one got from the 
            function will be considered to be relative to the input root path value, and the path used in 
            fine to save the documents will be rebuild from joining the root path and the relative path.
        root_path: string, or None; what the paths produced by the 'document2path_fct' parameter 
            are relative to. If he paths produced by the 'document2path_fct' parameter, they will be 
            considered to be absolute.
        skip_singleton: boolean( default = True), whether or not skip singleton when writing 
            coreference entities
        strict: boolean; whether or not to raise an Exception when writing if it appears that 
            a mention whose extent is defined in the CoreferencePartition instance does exist in the 
            Document instance
        verbose: whether or not to produce logs indicating the progress for persisting the 
            Document instances from the input iterator
        logger: logger to use when displaying progress for persisting the Document instances from 
            the input iterator
    
    Returns:
        collection of paths where each document of the input iterable was persisted; will be relative 
        paths to the input 'root_path' parameter value if it is not None. Can be used to load the 
        documents back, using the proper parsing tool.
    """
    return _persist_as_conll2012_file_or_pivot_files(documents_iterable, CONLL2012DocumentWriter, 
                                                      document2path_fct, root_path=root_path, 
                                                      skip_singleton=skip_singleton, strict=strict, 
                                                      verbose=verbose, logger=logger)

def persist_as_conll2012_multidocument_file(documents_iterable, multidocument_file_path, root_path=None, 
                                            skip_singleton=True, strict=False, verbose=False, logger=logging):
    """ Persists documents from an iterable, under the conll2012 format (all the documents in one file).
    
    Args:
        documents_iterable: iterable over Document instances
        multidocument_file_path: path (potentially relative) to the file where the Document 
            instances of the input iterable will be written under the multi-document CONLL2012 format 
        root_path: string, or None; what the input 'multidocument_file_path' value is relative to. 
            If None, the input 'multidocument_file_path' value will be considered to be absolute.
        skip_singleton: boolean( default = True), whether or not skip singleton when writing 
            coreference entities
        strict: boolean; whether or not to raise an Exception when writing if it appears that 
            a mention whose extent is defined in the CoreferencePartition instance does exist in the 
            Document instance
        verbose: whether or not to produce logs indicating the progress for persisting the 
            Document instances from the input iterator
        logger: logger to use when displaying progress for persisting the 
            Document instances from the input iterator
    
    Returns:
        collection of paths to each multidocument files that has been created by the call to the function; 
        will be relative paths to the input 'root_path' parameter value if it is not None. Can be 
        used to load the documents back, using the proper parsing tool.
    """
    if root_path is not None:
        multidocument_file_path = os.path.join(root_path, multidocument_file_path)
    conll2012_multidocument_file_path = CONLL2012MultiDocumentWriter.write(documents_iterable, 
                                                                           multidocument_file_path, 
                                                                           skip_singleton=skip_singleton, 
                                                                           strict=strict)
    document_paths = (conll2012_multidocument_file_path,)
    document_paths = _conditional_relpaths(document_paths, root_path=root_path)
    return document_paths


def write_corpus_file(document_paths, corpus_file_path, format_, corpus_parse_options=None):
    """ Writes a file describing the (potentially relative) locations of persisted documents, as 
    well as the format used, so that one can load the corpus just from parsing this file with the 
    appropriate tool
    
    Args:
        document_paths: collection of path to locations were document are persisted
        corpus_file_path: path to a file where the corpus parsing info will be persisted
        format_: string, either 'conll2012', 'pivot' or 'multiconll2012'
        corpus_parse_options: dict, or None; optional parameters to be persisted along the format, 
            and which shall be input during a future parsing of the corpus
    """
    if corpus_parse_options is None:
        corpus_parse_options = {}
    
    # Create string describing how to parse the corpus
    corpus_format_string = _create_corpus_persist_params_string(format_, **corpus_parse_options)
    
    # Write corpus description
    with open(corpus_file_path, "w", encoding=ENCODING) as f:
        # Write the corpus format string
        f.write("{}\n".format(corpus_format_string))
        # Write the collection of document paths
        f.write("\n".join(document_paths))
    


_FORMAT2PERSIST_FCT = {"pivot": persist_as_pivot_files_folders, 
                      "conll2012": persist_as_conll2012_document_files, 
                      "multiconll2012": persist_as_conll2012_multidocument_file, 
                      }
def write_corpus(documents_iterable, corpus_file_path, format_, get_path, root_path=None, 
                 skip_singleton=True, strict=False, verbose=False):
    """ Persists a collection of Document instances using the input format and locations info, as 
    well as the corpus file describing the persisted collection, and the way to parse it back
    
    Args:
        documents_iterable: iterable over Document instances
        corpus_file_path: path to a file where the corpus parsing info will be persisted
        format_: string, must be either 'pivot', 'conll2012', or 'multiconll2012'
        get_path:  string or callable:
        
            * if 'format_' == 'pivot', must be a function that takes a document as an input, and outputs a 
              folder path (possibly to a folder that does not exist yet)
            * if 'format_' == 'conll2012', must be a function that takes a document as an input, and outputs a 
              file path
            * if 'format_' == 'multiconll2012, must be a file path
        root_path: string, default None, the potential root path, from which all absolute 
            document path must be built before being passed to the document writer function. If None, all 
            paths provided trough the use of the 'get_path' parameter will be considered absolute, else they 
            will be considered relative to the value of 'root_path', whose value will be used to build the 
            absolute path passed to the document writer function.
        skip_singleton: boolean( default = True), whether or not skip singleton when writing 
            coreference entities
        strict: boolean; whether or not to raise an Exception when writing if it appears that 
            a mention whose extent is defined in the CoreferencePartition instance does not exist in the 
            Document instance
        verbose: whether or not to produce logs indicating the progress for persisting the 
            Document instances from the input iterator
        logger: logger to use when displaying progress for persisting the Document instances from 
            the input iterator
    """
    logger = logging.getLogger("write_corpus")
    
    try:
        persist_fct = _FORMAT2PERSIST_FCT[format_]
    except KeyError:
        msg = "Incorrect 'format_' value '{}', possible values are: {}."
        msg = msg.format(format_, set(_FORMAT2PERSIST_FCT))
        raise ValueError(msg) from None
    
    # Write documents & fetch the (possibly relative) paths where they were written
    document_paths = persist_fct(documents_iterable, get_path, root_path=root_path, 
                                 skip_singleton=skip_singleton, strict=strict, verbose=verbose, 
                                 logger=logger)
    
    # Write corpus file path
    if corpus_file_path is not None:
        write_corpus_file(document_paths, corpus_file_path, format_, corpus_parse_options=None)
    
    return corpus_file_path, root_path

def _persist_as_conll2012_file_or_pivot_files(documents_iterable, writer_class, document2path_fct, 
                                              root_path=None, skip_singleton=True, strict=False, 
                                              verbose=False, logger=logging):
    """ Persists documents from an iterable, under the conll2012 format (one document per file) or 
    under the pivot format.
    
    Args:
        documents_iterable: iterable over Document instances
        writer_class: the writer class to use, either 'CONLL2012DocumentWriter' or 'PivotWriter' 
        document2path_fct: function that takes a document as an input, and outputs the absolute 
            path to the:
                * folder where the files representing the document will be written, if pivot format. 
                  If such a folder does not exist, it will be created, along will the folder hierarchy 
                  that might be necessary to get to it.
                * conll2012 file where the document should be written, if conll2012 format
        root_path: string, default None, the potential root path, from which all absolute 
            document path must be built before being passed to the document writer function. If None, all 
            paths provided trough the use of the 'get_path' parameter will be considered absolute, else they 
            will be considered relative to the value of 'root_path', whose value will be used to build the 
            absolute path passed to the document writer function.
        skip_singleton: boolean( default = True), whether or not skip singleton when writing 
            coreference entities
        strict: boolean; whether or not to raise an Exception when writing if it appears that 
            a mention whose extent is defined in the CoreferencePartition instance does exist in the 
            Document instance
        verbose: whether or not to produce logs indicating the progress for persisting the 
            Document instances from the input iterator
        logger: logger to use when displaying progress for persisting the Document instances 
            from the input iterator
    
    Returns:
        collection of paths where each document of the input iterable was persisted; will be relative 
        paths to the input 'root_path' parameter value if it is not None. Can be used to load the 
        documents back, using the proper parsing tool.
    """
    # Process parameters
    ## Root path
    get_document_path_fct = lambda document: document2path_fct(document)
    if root_path is not None:
        get_document_path_fct = lambda document: os.path.join(root_path, document2path_fct(document))
    ## Process verbose options
    before_display_fct = lambda: None
    display_progress_function = lambda i: None
    after_display_fct = lambda: None
    if verbose:
        root_location_msg = "" if not root_path else ", in the root folder located at '{}',".format(root_path)
        msg = "Persisting documents{} using the '{}' class writer...".format(root_location_msg, PivotWriter)
        before_display_fct = lambda: logger.info(msg)
        logging_level = "info"
        count_interval = 10
        input_message = "Wrote {current:d} document(s)"
        maximum_count = None 
        percentage_interval = 5
        try:
            documents_nb = len(documents_iterable)
        except Exception:
            pass
        else:
            maximum_count = documents_nb
            input_message = "Wrote {percentage:6.2f}% of document(s) ({current:d} / {total:d})"
        
        display_progress_function = get_display_progress_function(logger, maximum_count=maximum_count, 
                                                                  percentage_interval=percentage_interval, 
                                                                  count_interval=count_interval, 
                                                                  personalized_progress_message=input_message, 
                                                                  logging_level=logging_level)
        after_display_fct = lambda: logger.info("Finished persisting documents using the '{}' class writer...".format(PivotWriter))
    
    document_paths = []
    before_display_fct()
    for i, document in enumerate(documents_iterable):
        display_progress_function(i)
        document_path = get_document_path_fct(document)
        document_path = writer_class.write(document, document_path, skip_singleton=skip_singleton, strict=strict)
        document_paths.append(document_path)
    after_display_fct()
    document_paths = _conditional_relpaths(document_paths, root_path=root_path)
    return document_paths





#############################
# Parse documents utilities #
#############################
def parse_corpus_file(file_path):
    """ Parses a file describing the (potentially relative) locations of persisted documents, as 
    well as the format used, and return those infos, to be used by a document parsing function.
    
    Args:
        file_path: path to a file where the corpus parsing info are persisted

    Returns:
        a [(format_, params), document_paths] tuple, where:
        * 'format_' is the format with which the documents are persisted (either 'pivot', 'conll2012', 
          or 'multiconll2012', normally)
        * 'params' is None, or a map of parameters to be used in the future by the Document instance 
          parsing function, so as to parse the corpus as it was intended
        * 'document_paths' is a collection of (potentially relative) paths to locations where documents 
          are persisted
    """
    with open(file_path, "r", encoding=ENCODING) as f:
        line = next(f)
        corpus_parsing_parameters_string = line.strip()
        document_paths = tuple(line for line in (line.strip() for line in f) if line != "")
    format_, params = _read_corpus_parsing_parameters_string(corpus_parsing_parameters_string)
    
    return (format_, params), document_paths

_FORMAT2PARSE_CLASS = {"pivot": PivotReader, 
                       "conll2012": CONLL2012DocumentReader, 
                       "multiconll2012": CONLL2012MultiDocumentReader, 
                       "conllu": CONLLUDocumentReader, 
                      }
def parse_documents_from_corpus_file_data(document_paths, format_, params=None, root_path=None, 
                                          name=None, verbose=False):
    """ Parse the documents corresponding to the input corpus data.
    
    Args:
        document_paths: collection of (potentially relative to 'root_path') paths to local 
            document persistence objects (files or folders, depending on the format used)
        format_: string, either "pivot", "conll2012", "multiconll2012" or "conllu", the format 
            corresponding to the document persistence objects to parse
        root_path: string (default = None), if None, the provided document paths 
            will be considered absolute, else they will be considered to be relative to the input 'root_path' 
            value
        params: python dict (default = None); map of options to be passed to the 
            parser specified by the 'format_' written in the corpus file. For now, only the PivotReader parser 
            can take one options, which is the following:
                * 'strict': boolean
            If an option is specified that the underlying parser is not supposed to support, an exception 
            will be raised.
        verbose: whether or not to display log messages informing about the progress of the parsing 
            of the corpus
        name: the name to be given to the corpus being parsed inside the log messages, if 'verbose' 
            is True

    Returns:
        a collection of Document instances
    """
    logger = logging.getLogger("parse_documents_from_corpus_file_data")
    
    # Process the corpus' parsing parameters
    try:
        parser_class = _FORMAT2PARSE_CLASS[format_]
    except KeyError:
        msg = "Incorrect 'format_' value '{}', possible values are: {}."
        msg = msg.format(format_, set(_FORMAT2PARSE_CLASS))
        raise IncorrectCorpusFormatError(msg) from None
    post_process_fct = lambda document, original_path: None
    if format_ == "conllu":
        post_process_fct = _conllu_post_process_fct
    
    # Get iterator over documents, and then create documents collection
    document_iterator = _iter_over_documents(document_paths, parser_class, root_path=root_path, 
                                             parsing_options=params, post_process_fct=post_process_fct, 
                                             verbose=verbose, name=name, logger=logger)
    documents = tuple(document_iterator)
    return documents

def parse_corpus_documents(corpus_file_path, root_path=None, parsing_corpus_options=None, 
                           get_get_path_fct=False, verbose=False):
    """ Parses a corpus file, and return the corresponding collection of Document instances.
    
    Args:
        corpus_file_path: path to a file where the corpus parsing info are persisted
        root_path: string (default = None), if None, document path written in the corpus file 
            will be considered absolute, else they will be considered to be relative to the input 'root_path' 
            value
        parsing_corpus_options: python dict (default = None); map of options to be passed to the 
            parser specified by the format_ written in the corpus file. For now, only the PivotReader parser 
            can take one options, which is the following:
                * 'strict': boolean
            If an option is specified that the underlying parser is not supposed to support, an exception 
            will be raised.
        get_get_path_fct: boolean, whether or not to return also a function that takes a parsed Document 
            instance as an input, and outputs the (potentially relative to 'root_path'e) path associated to 
            the Document, where it is persisted according to the corpus file
        verbose: whether or not to display log messages informing about the progress of the parsing 
            of the corpus

    Returns:
        a collection of Document instances if 'get_get_path_fct' is False, else a 
        (Document instances collection, get_path_fct) pair
    """
    logger = logging.getLogger("parse_corpus_documents")
    
    # Fetch corpus format specification and document paths
    logger.info("Parsing corpus definition from file located at '{}'...".format(corpus_file_path))
    (format_, params), document_paths = parse_corpus_file(corpus_file_path)
    logger.info("Finished parsing corpus definition from file located at '{}'.".format(corpus_file_path))
    if parsing_corpus_options is not None:
        params.update(parsing_corpus_options)
    _, name = os.path.split(corpus_file_path)
    
    # Parse the documents
    try:
        documents = parse_documents_from_corpus_file_data(document_paths, format_, params=params, 
                                                          root_path=root_path, 
                                                          name=name, 
                                                          verbose=verbose)
    except IncorrectCorpusFormatError as e:
        msg = "{} (defined in corpus located at '{}')".format(e.args[0], corpus_file_path)
        raise IncorrectCorpusFormatError(msg) from None
    
    if get_get_path_fct:
        for document, document_path in zip(documents, document_paths):
            document._corpus_document_path = document_path
        get_path_fct = lambda document: document._corpus_document_path
        return documents, get_path_fct
    return documents

def parse_corpus(corpus_file_path, root_path=None, parsing_corpus_options=None, get_get_path_fct=False, 
                 name=None, verbose=False):
    """ Parses a corpus file, and return the corresponding Corpus instance.
    
    Args:
        corpus_file_path: path to a file where the corpus parsing info are persisted
        root_path: string (default = None), if None, document path written in the corpus file 
            will be considered absolute, else they will be considered to be relative to the input 'root_path' 
            value
        parsing_corpus_options: python dict (default = None); map of options to be passed to the 
            parser specified by the format_ written in the corpus file. For now, only the PivotReader parser 
            can take one options, which is the following:
                * 'strict': boolean
            If an option is specified that the underlying parser is not supposed to support, an exception 
            will be raised.
        get_get_path_fct: boolean, whether or not to return also a function that takes a Document 
            instance as an input, and outputs the (potentially relative to 'root_path'e) path associated to 
            the Document where it is be persisted according to the corpus file
        name: string (default = None), the name to be given to the parsed corpus
        verbose: whether or not to display log messages informing about the progress of the parsing 
            of the corpus

    Returns:
        a Corpus instance
    """
    if get_get_path_fct:
        documents, get_path_fct = parse_corpus_documents(corpus_file_path, root_path=root_path, 
                                                       parsing_corpus_options=parsing_corpus_options,
                                                       get_get_path_fct=True,  
                                                       verbose=verbose)
        corpus = Corpus(documents, name=name)
        return corpus, get_path_fct
    
    documents = parse_corpus_documents(corpus_file_path, root_path=root_path, 
                                                       parsing_corpus_options=parsing_corpus_options,
                                                       get_get_path_fct=False,  
                                                       verbose=verbose)
    corpus = Corpus(documents, name=name)
    return corpus

def _get_preprocess_path_fct(root_path=None):
    """ Returns a function used to automatically convert a potential relative path into an absolute 
    path, using for this the input root folder path value.
    
    Args:
        root_path: string, or None; if not None, will assume that the path values input into the 
            created function will be relative to this root path value; if None, will assume that the path 
            values input into the created function will be absolute / self-sufficient for the process that 
            will consume the function

    Returns:
        callable, function that takes a path as input, and outputs a path
    """
    preprocess_path = lambda path: path
    if root_path is not None:
        preprocess_path = lambda path: os.path.join(root_path, path)
    return preprocess_path

def _conllu_post_process_fct(document, original_path):
    """ Makes it so the valu of the ident of the input Document instance is set to be the value of 
    the input path.
    """
    document.ident = original_path

def _iter_over_documents(document_paths, parser_class, root_path=None, parsing_options=None, 
                         post_process_fct=None, verbose=False, name=None, logger=logging):
    """ Creates an generator over Document instances resulting from the parsing of the documents 
    corresponding to the input corpus data, using the input parser class.
    
    Args:
        document_paths: collection of (potentially relative to 'root_path') paths to local 
            document persistence objects (files or folders, depending on the format used)
        parser_class: document parser class to use (either PivotReader, CONLL2012DocumentReader, 
            or CONLL2012MultiDocumentReader)
        root_path: string (default = None), if None, the provided document paths 
            will be considered absolute, else they will be considered to be relative to the input 'root_path' 
            value
        parsing_options: python dict (default = None); map of options to be passed to the 
            parser method of the input parser class. For now, only the PivotReader parser class' 'parse' 
            method can take one options, which is the following:
             * 'strict': boolean
            If an option is specified that the parser class is not supposed to support, an exception 
            will be raised.
        post_process_fct: callable, or None; function that takes a parsed Document instance and the 
            path from which the document was parsed, can be used to associate this path value and the parsed 
            Document instance
        verbose: whether or not to display log messages informing about the progress of the parsing 
            of the corpus
        name: the name to be given to the corpus being parsed inside the log messages, if 'verbose' 
            is True
        logger: logger to use when displaying progress for parsing the Document instances 
            from the input document paths

    Returns:
        an iterator over Document instances
    """
    # Process parameters
    ## Create function to convert parser output into document iterator
    get_iterator_over_document = lambda document: iter((document,))
    if parser_class == CONLL2012MultiDocumentReader:
        get_iterator_over_document = lambda document_iterator: document_iterator
    ## Potentially use of root path to get document path
    preprocess_path = _get_preprocess_path_fct(root_path=root_path)
    ## Create empty parsing options if necessary
    kwargs = parsing_options if parsing_options is not None else {}
    ## post_process_fct
    _post_process_fct = lambda document, original_path: None
    if post_process_fct is not None:
        _post_process_fct = post_process_fct
    ## Process verbose options
    before_display_fct = lambda: None
    display_progress_function = lambda i: None
    after_display_fct = lambda: None
    if verbose:
        if name is not None:
            msg = "Parsing the corpus using the '{}' class parser...".format(parser_class)
            end_msg = "Finished parsing the corpus using the '{}' class parser...".format(parser_class)
        else:
            msg = "Parsing the '{}' corpus using the '{}' class parser...".format(name, parser_class)
            end_msg = "Finished parsing the '{}' corpus using the '{}' class parser...".format(name, parser_class)
        before_display_fct = lambda: logger.info(msg)
        logging_level = "info"
        input_message = "Read {percentage:6.2f}% of corpus element(s) ({current:d} / {total:d})"
        percentage_interval = 5
        display_progress_function = get_display_progress_function(logger, maximum_count=len(document_paths), 
                                                                  percentage_interval=percentage_interval, 
                                                                  personalized_progress_message=input_message, 
                                                                  logging_level=logging_level)
        after_display_fct = lambda: logger.info(end_msg)
    
    # Parse the documents
    before_display_fct()
    for i, original_document_path in enumerate(document_paths):
        document_path = preprocess_path(original_document_path)
        display_progress_function(i)
        try:
            parse_primary_output = parser_class.parse(document_path, **kwargs)
        except TypeError as e:
            s = str(e)
            if re.search("got an unexpected keyword argument", s) is not None:
                raise TypeError("{} parser class's {}".format(parser_class.__name__, s)) from None
            raise e
        document_iterator = get_iterator_over_document(parse_primary_output)
        for document in document_iterator:
            _post_process_fct(document, original_document_path)
            yield document
    after_display_fct()

'''
import itertools
def parse_conll2012_multidocument(conll2012_multi_document_file_paths, root_path=None, verbose=False):
    """ Parse documents, assuming they are currently persisted using the conll2012 format, with 
    possibly several documents per file.
    
    Args:
        document_pivot_folder_paths: collection of path to document persistence objects
        root_path: string (default = None), if None, the provided document paths will be 
            considered absolute, else they will be considered to be relative to the input 'root_path' 
            value
        verbose: whether or not to display log messages informing about the progress of the parsing 
            of the documents

    Returns:
        collection of Document instance
    """
    preprocess_path = _get_preprocess_path_fct(root_path=root_path)
    return tuple(itertools.chain(CONLL2012MultiDocumentReader.parse(preprocess_path(path))\
                                 for path in conll2012_multi_document_file_paths)
                 )

def parse_conll2012_documents(conll2012_document_file_paths, root_path=None, verbose=False):
    """ Parse documents, assuming they are currently persisted using the conll2012 format, with one 
    document per file.
    
    Args:
        document_pivot_folder_paths: collection of path to document persistence objects
        root_path: string (default = None), if None, the provided document paths will be 
            considered absolute, else they will be considered to be relative to the input 'root_path' 
            value
        verbose: whether or not to display log messages informing about the progress of the parsing 
            of the documents

    Returns:
        collection of Document instance
    """
    preprocess_path = _get_preprocess_path_fct(root_path=root_path)
    return tuple(CONLL2012DocumentReader.parse(preprocess_path(document_path))\
                 for document_path in conll2012_document_file_paths
                 )

def parse_pivot_documents(document_pivot_folder_paths, root_path=None, strict=False, verbose=False):
    """ Parse documents, assuming they are currently persisted using the pivot format.
    
    Args:
        document_pivot_folder_paths: collection of path to document persistence objects
        root_path: string (default = None), if None, the provided document paths will be 
            considered absolute, else they will be considered to be relative to the input 'root_path' 
            value
        strict: whether or not to raise an exception if the head_extent of a Mention instance 
            is None when trying to synchronize the Mention instance with the Token instance(s) 
            constituting its head; use 'False' if you are not sure, and if you do not plan to input 
            the mentions in a application that needs them to have been synchronized with their head token(s)
        verbose: whether or not to display log messages informing about the progress of the parsing 
            of the documents

    Returns:
        collection of Document instance
    """
    preprocess_path = _get_preprocess_path_fct(root_path=root_path)
    return tuple(PivotReader.parse(preprocess_path(document_path), strict=strict)\
                 for document_path in document_pivot_folder_paths
                 )
'''

#########
# Utils #
#########
def _create_corpus_persist_params_string(format_, **kwargs):
    """ Encodes into a string the parameters specifying how and with which parameters to parse the 
    documents of a corpus.
    The other named parameters that are input in the function will be considered as other (string) 
    parameters to be used when parsing a document.
    
    Args:
        format_: string, either "pivot", "conll2012", "multiconll2012" or "conllu", the format 
            corresponding to the document persistence objects to parse in the future

    Returns:
        string, encoding the input parameters
    """
    params = OrderedDict()
    params["format_"] = format_
    params.update(kwargs)
    return json.dumps(params)

def _read_corpus_parsing_parameters_string(string):
    """ Decodes an adequate string into the parameters to use when parsing one document of a corpus.
    
    Args:
        string: string that encodes documents parsing parameters

    Returns:
        a (format_, params) pair, where:
        * 'format_' is a string, either "pivot", "conll2012", "multiconll2012" or "conllu", the format 
          corresponding to the document persistence objects to parse
        * 'params' is a "parameter name => parameter value" map
    """
    params = json.loads(string)
    format_ = params.pop("format_")
    return format_, params


def _conditional_relpaths(paths, root_path=None):
    """ Returns the input collection of paths, potentially modified so as to be transformed into 
    absolute path, if a root folder path value is provided.
    
    Args:
        paths: collection of strings, path to local resources
        root_path: string, or None; path to root folder is the input paths are relative paths

    Returns:
        a collection of strings
    """
    if root_path is not None:
        paths = tuple(os.path.relpath(path, root_path) for path in paths)
    return paths

# Exceptions
class IncorrectCorpusFormatError(CoRTeXException):
    """ Exception to be raised when encountering an incorrect document format value. """
    pass
