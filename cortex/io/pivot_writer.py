# -*- coding: utf-8 -*-

"""
Defines class used to persist a document's data to a folder, using CoRTeX's own format - the 'pivot' 
format.

A Document instance persisted under the 'pivot' format consists in a collection of files that are 
created within a specified folder.

The pivot format is constituted as follow:

+---------------------------+---------------------------------------------------------------------------+
| File name                 |  Description                                                              |
+===========================+===========================================================================+
| ident.txt                 |  | Contains the ident that was the Document instance's when it was        |
|                           |  | persisted using the pivot format                                       |
+---------------------------+---------------------------------------------------------------------------+
| raw.txt                   |  Contains the raw_text of the document                                    |
+---------------------------+---------------------------------------------------------------------------+
| info.txt                  |  Contains the data of the 'info' attribute of the Document                |
+---------------------------+---------------------------------------------------------------------------+
| tokens.xml                |  Contains the internal attributes of the tokens                           |
+---------------------------+---------------------------------------------------------------------------+
| sentences.xml             |  Contains the internal attributes of the sentences                        |
+---------------------------+---------------------------------------------------------------------------+
| named_entities.xml        |  Contains the internal attributes of the named entities                   |
+---------------------------+---------------------------------------------------------------------------+
| mentions.xml              |  Contains the internal attributes of the mentions                         |
+---------------------------+---------------------------------------------------------------------------+
| coreference_partition.txt |  Defines the potential coreference partition associated to the document   |
+---------------------------+---------------------------------------------------------------------------+
| mentions_synthesis.xml    |  | Is a readable synthesis of the attributes info known for each mention, |
|                           |  | including named entity data                                            |
+---------------------------+---------------------------------------------------------------------------+

All of those files but the 'raw.txt' are optional when parsing a Document using the PivotReader class.
When using PivotWriter, if the corresponding info is not defined in the Document instance, then 
the corresponding file will not be written.
"""

__all__ = ["PivotWriter",
           "pivot_format_file_names", 
           ]

import os
import xml.etree.ElementTree as ET
import re

from cortex.parameters import ENCODING
from cortex.utils import write_dictionary_to_file
from cortex.utils.io import TemporaryDirectory, rm_folder_content, mv_folder_content
from cortex.utils.xml_utils import indent
from cortex.api.markable import Token, NamedEntity, Mention, Sentence, Quotation, MentionDoesNotExistError


# Pivot format files definition
REF_DOCUMENT_VERSION_CODE = "ref"
SYS_DOCUMENT_VERSION_CODE = "sys"
def pivot_format_file_names():
    """ Defines the respective name of the files to be created to represent a Document instance under 
    the 'pivot' format.
    
    Returns:
        a "file ident => file name" map
    """
    file_names = {}
    file_names["ident"] = "ident.txt"
    file_names["raw"] = "raw.txt"
    file_names["info"] = "info.txt"
    file_names["tokens"] = "tokens.xml"
    file_names["named_entities"] = "named_entities.xml"
    file_names["sentences"] = "sentences.xml"
    file_names["quotations"] = "quotations.xml"
    file_names["mentions"] = "mentions.xml"
    file_names["mentions_synthesis"] = "mentions_synthesis.xml"
    file_names["mentions_entities"] = "coreference_partition.txt"
    return file_names


class PivotWriter(object):
    """ Class designed to be able to write the different expected data files associated to a document 
    under 'pivot' format, from a corresponding Document class instance.
    
    Cf the module's documentation for more info on CoRTeX' 'pivot' format.
    """
    
    # Class methods
    @classmethod
    def write(cls, document, document_folder_path, skip_singleton=True, strict=False):
        """ Writes the info about a Document under the pivot format at the specified location
        
        Args:
            document: Document instance whose info are to be persisted under the pivot format
            document_folder_path: path under the form "/path/to/document/folder/", all pivot 
                format files will created in this folder. If this folder already exist, it will be erased 
                and recreated anew.
            skip_singleton: boolean(default = True), whether or not skip singleton when writing 
                coreference entities
            strict: boolean; whether or not to raise an Exception when writing if it appears that 
                a mention whose extent is defined in the CoreferencePartition instance does not exist in the 
                Document instance; or if the Document instance does no possess mentions at all.
        
        Examples:
            >>> PivotWriter.write(document, "/path/to/folder/document_ident", ".ref.np_span")
        
        Warning:
            A file will not be written if the value of the Document instance's corresponding attribute 
            value is None
        """
        # Fail if we need to
        if not document.raw_text or document.raw_text == "":
            raise ValueError("The 'raw_text' attribute of the Document instance '{}' is empty, no "\
                             "meaning in saving it.".format(document))
        
        # Fetch the pivot file names
        file_names = pivot_format_file_names()
        
        # Raise error if the path exist and refer to something that is not a folder
        folder_exists = os.path.exists(document_folder_path)
        if folder_exists and not os.path.isdir(document_folder_path):
            message = "There is already something located at '{}' that is not a folder."
            message = message.format(document_folder_path)
            raise ValueError(message)
        
        # Work in a temporary folder
        with TemporaryDirectory() as temporary_folder:
            folder_path = temporary_folder.temporary_folder_path
            
            temp_document_folder_name = "temp_document"
            temp_document_folder_path = os.path.join(folder_path, temp_document_folder_name)
            
            os.mkdir(temp_document_folder_path)
            
            # Write the raw_text in the corresponding file; this attribute must not be None / or empty
            raw_file_path = os.path.join(temp_document_folder_path, file_names["raw"])
            cls._write_raw_file(document, raw_file_path)
            # Write the ident in the corresponding file; this attribute must not be None / or empty
            ident_info_file_path = os.path.join(temp_document_folder_path, file_names["ident"])
            cls._write_ident_into_file(document, ident_info_file_path)
            
            # Write the info in the corresponding file, if there is data to be written
            info_info_file_path = os.path.join(temp_document_folder_path, file_names["info"])
            cls._write_info_into_file(document, info_info_file_path)
            
            # Write named entities data, if there is data to be written
            named_entities_info_file_path = os.path.join(temp_document_folder_path, file_names["named_entities"])
            cls._write_named_entities_into_file(document, named_entities_info_file_path)
            
            # Write token data, if there is data to be written
            tokens_info_file_path = os.path.join(temp_document_folder_path, file_names["tokens"])
            cls._write_tokens_into_file(document, tokens_info_file_path)
            
            # Write mention data, if there data to be written
            mentions_info_file_path = os.path.join(temp_document_folder_path, file_names["mentions"])
            cls._write_mentions_into_file(document, mentions_info_file_path)
            
            # Write mentions synthesis info data, if there data to be written
            mentions_synthesis_info_file_path = os.path.join(temp_document_folder_path, file_names["mentions_synthesis"])
            cls._write_mentions_synthesis_into_file(document, mentions_synthesis_info_file_path)
            
            # Write sentence data, if there data to be written
            sentences_info_file_path = os.path.join(temp_document_folder_path, file_names["sentences"])
            cls._write_sentences_into_file(document, sentences_info_file_path)
            
            # Write sentence data, if there data to be written
            quotations_info_file_path = os.path.join(temp_document_folder_path, file_names["quotations"])
            cls._write_quotations_into_file(document, quotations_info_file_path)
            
            # Write coreferring mentions entities info
            coreferring_mentions_info_file_path = os.path.join(temp_document_folder_path, file_names["mentions_entities"])
            cls._write_coreferring_mentions_into_file(document, coreferring_mentions_info_file_path, 
                                                      skip_singleton=skip_singleton, strict=strict)
            
            
            # (Re)create the folder corresponding to the version of the document we want to persist
            if not folder_exists:
                os.makedirs(document_folder_path)
            
            # Rm existing content or document folder
            rm_folder_content(document_folder_path)
            # Move content of temporary folder to the input destination folder
            mv_folder_content(temp_document_folder_path, document_folder_path)
        
        return document_folder_path
        
    
    @classmethod
    def _write_raw_file(cls, document, file_path):
        with open(file_path, "w", encoding=ENCODING) as f:
            f.write(document.raw_text.rstrip())
    
    @classmethod
    def _write_ident_into_file(cls, document, file_path):
        with open(file_path, "w", encoding=ENCODING) as f:
            f.write(document.ident.rstrip())
    
    @classmethod
    def _write_info_into_file(cls, document, file_path):
        info = document.info
        if info is not None:
            write_dictionary_to_file(info, file_path)
    
    @classmethod
    def _write_tokens_into_file(cls, document, file_path):
        """ Writes the file containing the tokens info using xml representation.
        If there is no information to be written (None attribute value) then the file will not be 
        created.
        """
        cls._write_markables_into_file(document, file_path, Token)
    
    @classmethod
    def _write_named_entities_into_file(cls, document, file_path):
        """ Writes the file containing the named_entities info using xml representation.
        If there is no information to be written (None attribute value) then the file will not be 
        created.
        """
        cls._write_markables_into_file(document, file_path, NamedEntity)
    
    @classmethod
    def _write_mentions_into_file(cls, document, file_path):
        """ Writes the file containing the mentions info using xml representation.
        If there is no information to be written (None attribute value) then the file will not be 
        created.
        """
        cls._write_markables_into_file(document, file_path, Mention)
    
    @classmethod
    def _write_mentions_synthesis_into_file(cls, document, file_path):
        """ Writes the file containing the mentions info using xml representation.
        If there is no information to be written (None attribute value) then the file will not be 
        created.
        """
        cls._write_markables_into_file(document, file_path, Mention, with_named_entity_info=True)
    
    @classmethod
    def _write_sentences_into_file(cls, document, file_path):
        """ Writes the file containing the sentences info using xml representation.
        If there is no information to be written (None attribute value) then the file will not be 
        created.
        """
        cls._write_markables_into_file(document, file_path, Sentence)
    
    @classmethod
    def _write_quotations_into_file(cls, document, file_path):
        """ Write the file containing the quotations info using xml representation.
        If there is no information to be written (None attribute value) then the file will not be 
        created.
        """
        cls._write_markables_into_file(document, file_path, Quotation)
    
    @classmethod
    def _write_markables_into_file(cls, document, file_path, markable_class, *args, **kwargs):
        """ Writes the file containing the info of the markables constituting a list as an attribute 
        of a Document instance,  using xml representation.
        If there is no information to be written (None attribute value) then the file will not be 
        created.
        """
        markables = getattr(document, markable_class._NAME["plr"])
        if markables is not None:
            document_elt = ET.Element("document")
            markables_elt = ET.SubElement(document_elt, markable_class._NAME["plr"])
            # Add xml elements
            for markable in sorted(markables):
                markables_elt.append(markable.to_xml_element(*args, **kwargs))
            # Write xml file
            indent(document_elt)
            tree = ET.ElementTree(document_elt)
            tree.write(file_path, encoding=ENCODING)
    
    @classmethod
    def _write_coreferring_mentions_into_file(cls, document, file_path, skip_singleton=True, strict=False):
        """ Writes the input document's coreference partition data (if such data exists) into the file 
        located at the provided input path.
        
        Args:
            skip_singleton: boolean(default = True), whether or not skip singleton when writing 
                coreference entities
            strict: boolean; whether or not to raise an Exception when writing if it appears that 
                a mention whose extent is defined in the CoreferencePartition instance does not exist in the 
                Document instance; or if the Document instance does no possess mentions at all.
        """
        coreference_partition = document.coreference_partition
        if coreference_partition is not None:
            if strict:
                try:
                    s1 = set(document.extent2mention)
                except TypeError as e:
                    if re.search("NoneType", str(e.args[0])) is not None:
                        msg = "The document to be written, {}, possess no mention, yet has been "\
                               "attributed a coreference partition to be written."
                        msg = msg.format(document)
                        raise MentionDoesNotExistError(msg) from None
                    raise e
                s2 = coreference_partition.get_universe()
                diff = s2.difference(s1)
                if diff:
                    msg = "No mention corresponding to extent(s) '{}' in {}."
                    msg = msg.format(diff, document)
                    raise MentionDoesNotExistError(msg) from None
            
            if skip_singleton:
                coreference_partition = coreference_partition.remove_singletons()
            
            coreference_partition.save(file_path)
