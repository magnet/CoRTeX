# -*- coding: utf-8 -*-

"""
Defines class used for parsing a document's data from a folder, assumed to be persisted using 
CoRTeX's own format - the 'pivot' format.

A Document instance persisted under the 'pivot' format consists in a collection of files that are 
created within a specified folder.

The pivot format is constituted as follow:

+---------------------------+---------------------------------------------------------------------------+
| File name                 |  Description                                                              |
+===========================+===========================================================================+
| ident.txt                 |  | Contains the ident that was the Document instance's when it was        |
|                           |  | persisted using the pivot format                                       |
+---------------------------+---------------------------------------------------------------------------+
| raw.txt                   |  Contains the raw_text of the document                                    |
+---------------------------+---------------------------------------------------------------------------+
| info.txt                  |  Contains the data of the 'info' attribute of the Document                |
+---------------------------+---------------------------------------------------------------------------+
| tokens.xml                |  Contains the internal attributes of the tokens                           |
+---------------------------+---------------------------------------------------------------------------+
| sentences.xml             |  Contains the internal attributes of the sentences                        |
+---------------------------+---------------------------------------------------------------------------+
| named_entities.xml        |  Contains the internal attributes of the named entities                   |
+---------------------------+---------------------------------------------------------------------------+
| mentions.xml              |  Contains the internal attributes of the mentions                         |
+---------------------------+---------------------------------------------------------------------------+
| coreference_partition.txt |  Defines the potential coreference partition associated to the document   |
+---------------------------+---------------------------------------------------------------------------+
| mentions_synthesis.xml    |  | Is a readable synthesis of the attributes info known for each mention, |
|                           |  | including named entity data                                            |
+---------------------------+---------------------------------------------------------------------------+

All of those files but the 'raw.txt' are optional when parsing a Document using the PivotReader class.
When using PivotWriter, if the corresponding info is not defined in the Document instance, then 
the corresponding file will not be written.
"""

__all__ = ["PivotReader",
           ]

import os
from collections import defaultdict, OrderedDict
import xml.etree.cElementTree as ET

from cortex.parameters import ENCODING
from .pivot_writer import pivot_format_file_names
from cortex.api.document import Document
from cortex.api.document_buffer import DocumentBuffer
from cortex.api.markable import Token, NamedEntity, Mention, Sentence, Quotation
from cortex.api.coreference_partition import CoreferencePartition
from cortex.utils import read_dictionary_from_file



class PivotReader(object):
    """Class designed to be able to parse the different expected data files associated to a document 
    under 'pivot' format, and to return the corresponding Document class instance.
    
    Cf the module's documentation for more info on CoRTeX' 'pivot' format.
    
    Examples:
        >>> document = PivotReader.parse(folder_path)
    """
    
    # Class methods
    @classmethod
    def parse(cls, document_folder_path, strict=False):
        """ Parses the folder located at the input path, assumed to represent a 'pivot' formatted 
        document, into a Document instance.
        
        Won't be able to parse the file defining the mentions entities if a file defining the 
        mentions does not exist.
        
        Args:
            document_folder_path: path to a folder containing the files defined by CoRTeX' pivot 
                format
            strict: whether or not to raise an exception if the head_extent of a Mention instance 
                is None when trying to synchronize the Mention instance with the Token instance(s) 
                constituting its head; use 'False' if you are not sure, and if you do not plan to input 
                the mentions in a application that needs them to have been synchronized with their head token(s)

        Returns:
            a Document instance
        """
        # Check input
        if not os.path.isdir(document_folder_path):
            message = "There is no folder located at '{}'."
            message = message.format(document_folder_path)
            raise ValueError(message)
        
        # Defining the document ident and reading its raw_text
        file_names = pivot_format_file_names()
        ## Raw text
        raw_file_path = os.path.join(document_folder_path, file_names["raw"])
        raw_text = cls.__parse_raw_file(raw_file_path)
        ## Ident
        ident_file_path = os.path.join(document_folder_path, file_names["ident"])
        document_ident = cls.__parse_ident_file(ident_file_path)
        
        ## Info
        info = None
        info_file_path = os.path.join(document_folder_path, file_names["info"])
        if os.path.isfile(info_file_path):
            info = cls.__parse_info_file(info_file_path)
        
        # Create the Document instance and the DocumentBuffer instance
        document = Document(document_ident, raw_text, info=info)
        document_buffer = DocumentBuffer(document)
        
        # Parse each file, and add their information to the Document class
        parsed = defaultdict(lambda: False)
        ## Named entities
        named_entities_file_path = os.path.join(document_folder_path, file_names["named_entities"])
        if os.path.isfile(named_entities_file_path):
            cls.__parse_named_entities_file(named_entities_file_path, document_buffer)
            parsed["named_entities"] = True
        ## Tokens
        tokens_file_path = os.path.join(document_folder_path, file_names["tokens"])
        if os.path.isfile(tokens_file_path):
            cls.__parse_tokens_file(tokens_file_path, document_buffer)
            parsed["tokens"] = True
        ## Mentions
        mentions_file_path = os.path.join(document_folder_path, file_names["mentions"])
        if os.path.isfile(mentions_file_path):
            cls.__parse_mentions_file(mentions_file_path, document_buffer)
            parsed["mentions"] = True
        ## Sentence
        sentences_file_path = os.path.join(document_folder_path, file_names["sentences"])
        if os.path.isfile(sentences_file_path):
            cls.__parse_sentences_file(sentences_file_path, document_buffer)
            parsed["sentences"] = True
        ## Quotation
        quotations_file_path = os.path.join(document_folder_path, file_names["quotations"])
        if os.path.isfile(quotations_file_path):
            cls.__parse_quotations_file(quotations_file_path, document_buffer)
            parsed["quotations"] = True
        ## Parse coreferring mentions entities info
        coreferring_mentions_info_file_path = os.path.join(document_folder_path, file_names["mentions_entities"])
        if os.path.isfile(coreferring_mentions_info_file_path):
            cls.__parse_coreferring_mentions_info_file(coreferring_mentions_info_file_path, document_buffer)
        
        return document_buffer.flush(strict=strict)
    
    @classmethod
    def __parse_raw_file(cls, file_path):
        with open(file_path, "r", encoding=ENCODING) as f:
            raw_text = f.read()
        raw_text = raw_text.rstrip()
        return raw_text
    
    @classmethod
    def __parse_ident_file(cls, file_path):
        with open(file_path, "r", encoding=ENCODING) as f:
            line = next(f)
        ident = line.strip()
        return ident
    
    @classmethod
    def __parse_info_file(cls, file_path):
        return read_dictionary_from_file(file_path)
    
    @classmethod
    def __parse_tokens_file(cls, file_path, document_buffer):
        """ Populates the DocumentBuffer instance with Token instances whose information is 
        parsed from the xml file whose path is input.
        """
        cls.__parse_markables_file(file_path, document_buffer, Token)
    
    @classmethod
    def __parse_named_entities_file(cls, file_path, document_buffer):
        """ Populates the DocumentBuffer instance with NamedEntity instances whose information is 
        parsed from the xml file whose path is input.
        """
        cls.__parse_markables_file(file_path, document_buffer, NamedEntity)
    
    @classmethod
    def __parse_mentions_file(cls, file_path, document_buffer):
        """ Populates the DocumentBuffer instance with Token instances whose information is 
        parsed from the xml file whose path is input.
        """
        cls.__parse_markables_file(file_path, document_buffer, Mention)
    
    @classmethod
    def __parse_sentences_file(cls, file_path, document_buffer):
        """ Populates the DocumentBuffer instance with Sentence instances whose information is 
        parsed from the xml file whose path is input.
        """
        cls.__parse_markables_file(file_path, document_buffer, Sentence)
    
    @classmethod
    def __parse_quotations_file(cls, file_path, document_buffer):
        """ Populates the DocumentBuffer instance with Quotation instances whose information is 
        parsed from the xml file whose path is input.
        """
        cls.__parse_markables_file(file_path, document_buffer, Quotation)
    
    @classmethod
    def __parse_markables_file(cls, file_path, document_buffer, markable_class):
        """ Parses the content of the xml file located at the input path to build a list of data dict 
        using the adequate method of the input Markable class, and call the corresponding 'add' 
        method of the DocumentBuffer instance on it.
        """
        # Parse the xml file to obtain an xml tree
        xml_tree = ET.parse(file_path)
        document_elt = xml_tree.getroot()
        markables_elt = document_elt.find(markable_class._NAME["plr"])
        markable_elt_list = markables_elt.findall(markable_class._NAME["sg"])
        # Iterate the mention_elt_list and create the corresponding mention instances
        markables_data = OrderedDict()
        for element in markable_elt_list:
            data_dict = markable_class.from_xml_element_to_dict(element)
            if data_dict is not None:
                markables_data[data_dict["extent"]] = data_dict
        # Populate the DocumentBuffer with the parsed data
        setter_method = getattr(document_buffer, "add_{}".format(markable_class._NAME["plr"]))
        setter_method(markables_data)
    
    @classmethod
    def __parse_coreferring_mentions_info_file(cls, mention_spans_file_path, document_buffer):
        """ Creates a CoreferencePartition instance from the parsing of the file whose path is given 
        (using the corresponding method of the CoreferencePartition class), and provide it to the 
        input DocumentBuffer instance.
        """
        coreference_partition = CoreferencePartition.load(mention_spans_file_path)
        document_buffer.set_coreference_partition(coreference_partition)

