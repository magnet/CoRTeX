# -*- coding: utf-8 -*-

"""
Defines a class used to represent a collection of documents, and utilities used to compute some 
statistics over it.
"""

__all__ = ["Corpus",
           ]

import numpy
import warnings
warnings.filterwarnings("ignore", "Mean of empty slice", RuntimeWarning)

class Corpus(object):
    """A class used to wrap a collection of Documents, and to provide methods to compute statistics 
    on it. 
    
    An instance of this class provides an iterator over the Document instances wrapped around.
    
    Arguments:
        documents: collection of Document instances to wrap around
        
        name: string, or None; the name to give to this corpus
    
    Attributes:
        documents: the collection of Document instance wrapped around
        
        name: string, or None; the name to give to this corpus
    """
    
    def __init__(self, documents, name=None):
        self.name = name
        self.__documents = documents
        self.__document_ident_max_size = max((len(doc.ident) for doc in self.__documents), default=30)
    
    # Property
    @property
    def documents(self):
        return self.__documents
    @documents.setter
    def documents(self, value):
        raise AttributeError
    
    # Instance methods
    def get_documents_summary_data(self):
        """ Computes the respective number of tokens, of sentences, of mentions, of coreference 
        entities, as well as the ratio 'nb of entities / nb of mentions' for each document in the corpus.
        Return the collection of the tuples made out of those values, in the same order that the 
        documents are defined within the corpus
        

        Returns:
            collection of (document_ident, tokens_nb, sentences_nb, mentions_nb, coreference_entities_nb, ratio) tuples
        """
        documents_data = []
        for doc in self:
            t_ct = len(doc.tokens) if doc.tokens is not None else 0
            s_ct = len(doc.sentences) if doc.sentences is not None else 0
            m_ct = len(doc.mentions) if doc.mentions is not None else 0
            e_ct = sum((1 for _ in doc.coreference_partition), 0) if doc.coreference_partition is not None else 0
            em_rat = e_ct / float(m_ct) if m_ct > 0  else numpy.NaN
            documents_data.append((doc.ident, t_ct, s_ct, m_ct, e_ct, em_rat))
        
        return documents_data
    
    def _get_corpus_documents_summary_data(self):
        """ Computes the average number of tokens, sentences, mentions, entities, and average ratio 
        'nb of entities / nb of mentions' for this corpus
        

        Returns:
            a (corpus_summary_data, documents_summary_data) tuple, 
            where:
                - 'corpus_summary_data' is a (corpus name, token_ct, sentence_ct, mention_ct, entity_ct, 
                    em_ratios_sum, em_ratio_count, avg_token_ct, avg_sentence_ct, avg_mention_ct, avg_entity_ct, 
                    avg_em_rat) tuple
                - 'documents_data' is the collection of 'document_summary_data' used to compute the corpus data
        """
        documents_data = self.get_documents_summary_data()
        
        _, tokens_ct, sentences_ct, mentions_ct, entities_ct, em_ratios = list(map(numpy.array, zip(*documents_data)))
        
        token_ct = tokens_ct.sum()
        sentence_ct = sentences_ct.sum()
        mention_ct = mentions_ct.sum()
        entity_ct = entities_ct.sum()
        em_ratios_sum = numpy.sum(em_ratios[~numpy.isnan(em_ratios)])
        em_ratio_count = numpy.count_nonzero(~numpy.isnan(em_ratios))
        avg_token_ct = tokens_ct.mean()
        avg_sentence_ct = sentences_ct.mean()
        avg_mention_ct = mentions_ct.mean()
        avg_entity_ct = entities_ct.mean()
        avg_em_rat = numpy.nanmean(em_ratios)
        
        corpus_summary_data = (self.name, token_ct, sentence_ct, mention_ct, entity_ct, em_ratios_sum, em_ratio_count, 
                               avg_token_ct, avg_sentence_ct, avg_mention_ct, avg_entity_ct, avg_em_rat)
    
        return corpus_summary_data, documents_data
    
    def format_summary(self, detailed=False):
        """ Formats the corpus' statistics in a readable way.
        
        Args:
            detailed: boolean( default=False); whether or not to include the data of each 
                document as well

        Returns:
            a string
        """
        # Fetch the data
        corpus_summary_data, documents_data = self._get_corpus_documents_summary_data()
        
        # Prepare utils strings
        doc_ident_col_size = self.__document_ident_max_size + 1
        dash_string = "-"*(94 + doc_ident_col_size)
        header_string_format = "| {:"+str(doc_ident_col_size)+"s} | {:15s} | {:15s} | {:15s} | {:15s} | {:15s} |"
        data_string_format = "| {:"+str(doc_ident_col_size)+"s} | {:15d} | {:15d} | {:15d} | {:15d} | {:15.2f} |"
        corpus_data_string_format = "| {:"+str(doc_ident_col_size)+"} | {:15.2f} | {:15.2f} | {:15.2f} | {:15.2f} | {:15.2f} |"
        
        summary_string_array = []
        # If one wants to display the aggregated data of each document
        if detailed:
            # Headers
            summary_string_array.append(dash_string)
            header_string = header_string_format.format("DOCUMENT", "# of TOKENS", "# of SENTENCES", 
                                                        "# of MENTIONS", "# of ENTITIES", "E/M ratio")
            summary_string_array.append(header_string)
            summary_string_array.append(dash_string)
            # Documents
            for data in documents_data:
                ident, t_ct, s_ct, m_ct, e_ct, em_rat = data
                s = data_string_format.format(ident, t_ct, s_ct, m_ct, e_ct, em_rat)
                summary_string_array.append(s)
        
        # Corpus
        # Fetch the data
        (_, token_ct, sentence_ct, mention_ct, entity_ct, em_ratios_sum, em_ratio_count, avg_token_ct, 
         avg_sentence_ct, avg_mention_ct, avg_entity_ct, avg_em_rat) = corpus_summary_data
        doc_nb = len(documents_data)
        # Format the data
        summary_string_array.append(dash_string)
        header_string = header_string_format.format("{} DOCUMENT(S)".format(doc_nb), "# of TOKENS", 
                                                    "# of SENT.", "# of MENTIONS", "# of ENTITIES", 
                                                    "E/M ratio ({})".format(em_ratio_count))
        summary_string_array.append(header_string)
        summary_string_array.append(dash_string)
        s = data_string_format.format("TOTAL", token_ct, sentence_ct, mention_ct, entity_ct, em_ratios_sum)
        summary_string_array.append(s)
        s = corpus_data_string_format.format("AVG", avg_token_ct, avg_sentence_ct, avg_mention_ct, 
                                             avg_entity_ct, avg_em_rat)
        summary_string_array.append(s)
        summary_string_array.append(dash_string)
        
        summary_string = "\n".join(summary_string_array)
        
        return summary_string
    
    def print_summary(self, detailed=False):
        """ Prints the corpus' statistics in a readable way.
        
        Args:
            detailed: boolean( default=False); whether or not to include the data of each 
                document as well
        """
        print(self.format_summary(detailed=detailed))
    
    def __iter__(self):
        """ Returns an iterator over the document instance(s) of this corpus. """
        return iter(self.__documents)

    def __len__(self):
        return len(self.__documents)#count_iterable_content(self)
    
    def __repr__(self):
        return "Corpus(name='{})".format(self.name)
    