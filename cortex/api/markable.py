# -*- coding: utf-8 -*-
"""
Defines classes used to aggregate information regarding one extract of a document's raw text 
(token, mention, sentences...).

Those classes' role is only to contain data, not to define features from it.
"""

__all__ = ["Markable", "Token", "NamedEntity", "Mention", "Quotation", "Sentence",
           "NullMention", "NULL_MENTION", 
           "ExtentComparisonKey", 
           "MentionDoesNotExistError", "NoneHeadExtentError",
           "extent_greater_than", "get_mention_pair_extent_pair",
           ]

from collections import OrderedDict
import xml.etree.cElementTree as ET

from .document import Document
from .constituency_tree import ConstituencyTree
from cortex.utils import EqualityMixIn, CoRTeXException
from cortex.utils.xml_utils import xml_escape, add_unique_field_to_dict_from_subelt, IncorrectXMLElementError
from cortex.utils.singleton import Singleton


class Markable(EqualityMixIn):
    """ Class whose role is to represent a markable of a document.
    
    A markable is a text element that corresponds to a text extract of a document.
    It is defined by its 'extent' (couple of starting and ending indices locating the substring in 
    the raw text's string; offsets are inclusive on both sides, i.e. a closed interval), as well as 
    the document it is supposed to be an extract from.
    
    Arguments:
        ident: a string identifying the markable; it should be used to be able to distinguish 
            this markable from every other markable of the same type defined in the context of the unique 
            document they refer to.
        
        extent: a (start; end) tuple of two integers representing respectively the position in the 
            document's raw text string of the first character of the instance's substring, and of the 
            last character. Assume those index values starts from 1 (the first character in the text 
            start at position 1).
        
        document: the Document instance to which the instance refers
        
        raw_text: the substring of the Document instance'raw string corresponding the extent 
            of the instance; if not None, will be compared to the one extracted from the document 
            using the 'extent' info. This allows for consistency checks when instantiating a whose raw 
            text is supposed to be known.
    
    Attributes:
        ident: the value of the 'ident' parameter used to initialize the model class instance
        
        extent: the value of the 'extent' parameter used to initialize the model class instance
        
        document: the value of the 'document' parameter used to initialize the model class instance
        
        raw_text: the substring of the Document instance'raw string corresponding the extent 
            of the Markable instance
        
        start: integer, the position in the document's raw text string of the first character of 
            the instance's raw text
        
        end: integer, the position in the document's raw text string of the last character of 
            the instance's raw text
        
        extent_id: string representing the extent used to define the instance
    """
    
    _INNER_ATTRIBUTE_NAMES = ("ident", "extent", "raw_text")
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = ("extent", "raw_text")
    
    def __init__(self, ident, extent, document, raw_text=None):
        # Check 'extent' and 'document' inputs
        extent, document, extracted_text = self._is_correct_extent(extent, document, raw_text)
        self.ident = ident # Mostly used to refer to the id (of the markable) used by the file where the information was found
        self.__extent = extent
        self.__raw_text = extracted_text
        self.__document = document
    
    ## Instance attributes / properties
    @property
    def raw_text(self):
        return self.__raw_text
    @raw_text.setter
    def raw_text(self, value):
        raise AttributeError
    
    @property
    def extent(self):
        return self.__extent
    @extent.setter
    def extent(self, value):
        raise AttributeError
    
    @property
    def document(self):
        return self.__document
    @document.setter
    def document(self, value):
        raise AttributeError
    
    @property
    def start(self):
        return self.extent[0]
    @start.setter
    def start(self, value):
        raise AttributeError
    
    @property
    def end(self):
        return self.extent[1]
    @end.setter
    def end(self, value):
        raise AttributeError
    
    @property
    def extent_id(self):
        """ Creates the 'id' string corresponding the the markable's extent value.
        
        Returns:
            a string
        """
        return "{e[0]},{e[1]}".format(e=self.extent)
    @extent_id.setter
    def extent_id(self, value):
        raise AttributeError
    
    ### Instance methods
    def to_dict(self):
        """ Returns a map holding this markable's attribute values. Useful for duplicating a 
        markable, when used in conjunction with the 'from_dict' method.
        
        Returns:
            a "markable initialization parameter key => value" dict
        """
        return {key: value for key, value in ((attribute_name, getattr(self, attribute_name))\
                                               for attribute_name in self._INNER_ATTRIBUTE_NAMES)\
                 if value is not None}
    
    def includes(self, other):  # inclusion
        """ Checks whether or not 'other' is included 'within' self according to their respective 
        extent.
        
        Args:
            other: a Markable class instance

        Returns:
            a boolean
        """
        s1, e1 = self.extent
        s2, e2 = other.extent
        return s1 <= s2 and e2 <= e1
    
    def starts(self, other):  # LHS overlap
        """ Checks whether or not 'self' is included at the beginning of 'other'.
        
        Args:
            other: a Markable class instance

        Returns:
            a boolean
        """
        s1, e1 = self.extent
        s2, e2 = other.extent
        return s1 == s2 and e1 < e2

    def finishes(self, other):  # RHS overlap:  e.g.: [Bill Clinton, [the former US president]]
        """ Checks whether or not 'self' is included at the end of 'other'.
        
        Args:
            other: a Markable class instance

        Returns:
            a boolean
        """
        s1, e1 = self.extent
        s2, e2 = other.extent
        return s2 < s1 and e1 == e2
    
    #### Inequalities: basically, the reading order prevails, i.e. if a Markable begins before another, it is 'lesser' than the other
    def __lt__(self, other):
        return extent_greater_than(other.extent, self.extent, strict=True) # Or 'not self >= other' ?

    def __le__(self, other):
        return extent_greater_than(other.extent, self.extent, strict=False) # Or 'not self > other' ?
    
    def __gt__(self, other):
        return extent_greater_than(self.extent, other.extent, strict=True)
    
    def __ge__(self, other):
        return extent_greater_than(self.extent, other.extent, strict=False)
    
    ### Static method
    @staticmethod
    def _checks_type(other, class_):
        """ Checks whether or not the input object is an instance of the input class.
        
        Args:
            other: a python object
            class_: a python class

        Returns:
            a boolean
        """
        if not isinstance(other, class_):
            raise TypeError("Incorrect type '{}', only another '{}' instance is accepted.".format(type(other), class_))
    
    ### Class methods    
    @classmethod
    def _is_correct_extent(cls, extent, document, raw_text):
        """ Checks whether or not the input extent is compatible with the input document's raw text 
        size, and that the input raw_text is consistent with the one extracted from the document 
        using the input extent.
        
        Args:
            extent: a (int1, int2) pair, with 0 <= int1 <= int2
            document: a Document instance
            raw_text: a string

        Returns:
            a boolean
        """
        if extent is None or len(extent) < 2 or not isinstance(extent[0], int) \
            or not isinstance(extent[1], int) or extent[0] <= 0 or extent[1] <= 0 or extent[0] > extent[1]:
            raise ValueError("Extent must be a tuple of positive integers, whose second value "\
                            "must be superior to the first (input: {})".format(extent))
        if not isinstance(document, Document):
            raise TypeError("'document' input must be a valid Document instance ({}).".format(type(document)))
        if extent[1] > len(document.raw_text):
            raise ValueError("'extent' and 'document' are not compatible, raw text is too short to "\
                            "accommodate for end of extent ({} v. {}).".format(len(document.raw_text), extent[1]))
        # Check consistency of text information
        extracted_text = cls._extract_raw_text(document, extent)
        if raw_text is not None and extracted_text != raw_text:
            raise ValueError("The input 'raw_text' value ('{}') contradicts the one obtained from "\
                             "the document's raw text ('{}') using the input extent value ('{}')."\
                             .format(raw_text, extracted_text, extent))
        return extent, document, extracted_text
    
    @classmethod
    def _extract_raw_text(cls, document, extent):
        """ Returns the string corresponding to the input document's raw text's extract identified by 
        the input extent value.
        
        Args:
            document: a Document instance
            extent: a (int1, int2) pair, with 0 <= int1 <= int2

        Returns:
            a string
        """
        return document.get_raw_text_extract(extent)
    
    def __repr__(self):
        return "{}(ident='{}', extent={}, raw_text='{}')".format(self.__class__.__name__, 
                                                                 self.ident, self.extent, 
                                                                 self.raw_text)



class NamedEntity(Markable):
    """ Class whose role is to represent a named entity of a document.
    
    A NamedEntity is a Markable that can be assigned a 'named entity type' (e.g.: 'LOCATION', 'PERSON', ...).
    
    Arguments:
        ident: a string identifying the markable; it should be used to be able to distinguish 
            this markable from every other markable of the same type defined in the context of the unique 
            document they refer to.
        
        extent: a (start; end) tuple of two integers representing respectively the position 
            in the document's raw string of the first character of the Markable instance's substring, 
            and of the last character. Assume those index values starts from 1 (the first character in 
            the text start at position 1).
        
        document: the Document instance to which the Markable instance refers
        
        type_: string; specifies the named entity type associated to the instance
        
        raw_text: the substring of the Document instance'raw string corresponding the extent 
            of the Markable instance; if not None, will be compared to the one extracted from the document 
            using the 'extent' info. This allows for consistency checks when defining a markable whose 
            raw text is supposed to be known.
        
        subtype: string, or None; potentially specifies the named entity subtype associated to the 
            instance
        
        origin: string, or None; used to be able to recall from where originates the decision 
            to determine that this markable is in fact a named entity
    
    Attributes:
        ident: the value of the 'ident' parameter used to initialize the model class instance
        
        extent: the value of the 'extent' parameter used to initialize the model class instance
        
        document: the value of the 'document' parameter used to initialize the model class instance
        
        raw_text: the substring of the Document instance'raw string corresponding the extent 
            of the Markable instance
        
        start: integer, the position in the document's raw text string of the first character of 
            the instance's raw text
        
        end: integer, the position in the document's raw text string of the last character of 
            the instance's raw text
        
        extent_id: string representing the extent used to define the instance
        
        type: string; specifies the named entity type associated to the instance
        
        subtype: string, or None; potentially specifies the named entity subtype associated to the 
            instance
        
        origin: string, or None; used to be able to recall from where originates the decision 
            to determine that this markable is in fact a named entity
    """
    
    _NAME = {"sg": "named_entity", "plr": "named_entities"}
    _OPTIONAL_ATTRIBUTE_NAMES = ("subtype", "origin")
    _INNER_ATTRIBUTE_NAMES = Markable._INNER_ATTRIBUTE_NAMES + ("type", "subtype", "origin")
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = ("type", "subtype", "origin") +\
                                                 Markable._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    
    def __init__(self, ident, extent, document, type_, raw_text=None, subtype=None, origin=None):
        Markable.__init__(self, ident, extent, document, raw_text=raw_text)
        self.type = type_
        self.subtype = subtype
        self.origin = origin
    
    # Instance methods    
    def __repr__(self):
        string = "{}(ident='{}', extent='{}', raw_text='{}', type='{}', subtype='{}', origin='{}')"
        string = string.format(self.__class__.__name__, self.ident, self.extent, self.raw_text, 
                               self.type, self.subtype, self.origin)
        return string
    
    def update(self, data):
        """ Updates the attribute whose name is one of the input dict's field with the corresponding 
        value.
        
        Args:
            data: a dict instance whose fields correspond to the attribute names of the markable
                which one wants to update. One can modify through this method only the attributes that were 
                optional at initialization time (see 'from_dict' method's docstring)
        """
        _set_attributes_from_dict(self, self._OPTIONAL_ATTRIBUTE_NAMES, data)
    
    def to_xml_element(self):
        """ Creates a 'xml.etree.ElementTree.Element' instance containing the data used to identify 
        this instance.
        
        It is such that when the corresponding xml code is pretty printed, it looks like the example 
        below:
        
        Ex:
        
        | <named_entity id="1411,1416">
        |   <extent end="1416" start="1411" />
        |   <text>Yongji</text>
        |   <type>GPE</type>
        |   <subtype>Glop</subtype>
        |   <origin>corpus</origin>
        | </named_entity>
        
        References to encompassed or encompassing text structures, such as NamedEntity, or Sentence 
        instance, are not persisted this way, since they result from a synchronization of data that 
        must be independently persisted.

        Returns:
            an xml.etree.ElementTree.Element instance
        """
        EASY_MENTION_ATTRIBUTE_NAMES = ("type", "subtype", "origin")
        # Build element
        named_entity_elt = ET.Element(self._NAME["sg"], id=self.ident)
        # Extent
        start, end = self.extent[0], self.extent[1]
        ET.SubElement(named_entity_elt, "extent", start=str(start), end=str(end))
        # Raw_text
        raw_text = self.raw_text
        text_elt = ET.SubElement(named_entity_elt, "text")
        text_elt.text = xml_escape(raw_text)
        # Other
        for name in EASY_MENTION_ATTRIBUTE_NAMES:
            value = getattr(self, name)
            if value is not None:
                elt = ET.SubElement(named_entity_elt, name)
                elt.text = xml_escape(str(value))
        return named_entity_elt
    
    # Class methods
    @classmethod
    def __checks_type(cls, other):
        """ Checks whether or not the input object is an instance of the NamedEntity class.
        
        Args:
            other: a python object

        Returns:
            a boolean
        """
        cls._checks_type(other, cls)
    
    @classmethod
    def from_dict(cls, data):
        """ Creates an instance from the values contained in the input dict.
        'ident', 'extent', 'document' and 'type' fields are compulsory.
        
        Args:
            data: a python dict

        Returns:
            a NamedEntity instance
        """
        ident = data["ident"]
        extent = data["extent"]
        document = data["document"]
        type_ = data["type"]
        raw_text = data.get("raw_text")
        
        named_entity = NamedEntity(ident, extent, document, type_, raw_text=raw_text)
        _set_attributes_from_dict(named_entity, cls._OPTIONAL_ATTRIBUTE_NAMES, data)
        
        return named_entity
    
    @classmethod
    def from_xml_element_to_dict(cls, xml_element):
        """ Parses the input 'xml.etree.ElementTree.Element' instance to create a dict instance.
        This dict instance could be used, for example, to create an instance using the 'from_dict' 
        method (though to do that one would need to add the corresponding 'document' field to be in 
        the dict).
        
        If the input element does not correspond to a xml element related to a mention, return None.
        
        Args:
            xml_element: a xml.etree.ElementTree.Element instance

        Returns:
            a python dict
        """
        data_dict = None
        if xml_element.tag != "named_entity":
            #raise IncorrectXMLElementError("Input parameter is not a 'token' tagged xml element.")
            return data_dict
        
        # Initialize the dict
        data_dict = {}
        # Ident
        ident = xml_element.get("id")
        if ident is None:
            raise IncorrectXMLElementError("The input xml element contains no 'id' attribute.")
        data_dict["ident"] = ident
        # Extent
        extent_elts = xml_element.findall("extent")
        if len(extent_elts) != 1:
            raise IncorrectXMLElementError("The input xml element contains no or more than one 'extent' element.")
        extent_elt = extent_elts[0]
        extent = int(extent_elt.get('start')), int(extent_elt.get('end'))
        data_dict["extent"] = extent
        # Raw_text
        add_unique_field_to_dict_from_subelt(xml_element, ("text", "raw_text"), data_dict, str, enforce_presence=True)
        # Type
        add_unique_field_to_dict_from_subelt(xml_element, ("type", "type"), data_dict, str, enforce_presence=True)
        # Others
        for attribute_name in cls._OPTIONAL_ATTRIBUTE_NAMES:
            add_unique_field_to_dict_from_subelt(xml_element, (attribute_name, attribute_name), 
                                                 data_dict, str, enforce_presence=False)
            if attribute_name in data_dict and data_dict[attribute_name] == "None":
                del(data_dict[attribute_name])
        return data_dict





class Token(Markable):
    """ Class whose role is to represent a token of a document.
    
    A Token is a Markable that represents a textual atomic element, for which a POS tag can be 
    determined and assigned.
    
    We make the hypothesis that, for each language that CoRTeX must be able to support, we can 
    define the notion of POS and lemma for a Token, and that we can associate a named_entity, a 
    Sentence instance's parse tree node or a Sentence instance's dependency tree node.
    
    Arguments:
        ident: a string identifying the markable; it should be used to be able to distinguish 
            this markable from every other markable of the same type defined in the context of the unique 
            document they refer to.
        
        extent: a (start; end) tuple of two integers representing respectively the position 
            in the document's raw string of the first character of the Markable instance's substring, 
            and of the last character. Assume those index values starts from 1 (the first character in 
            the text start at position 1).
        
        document: the Document instance to which the Markable instance refers
        
        raw_text: the substring of the Document instance'raw string corresponding the extent 
            of the Markable instance; if not None, will be compared to the one extracted from the document 
            using the 'extent' info. This allows for consistency checks when defining a markable whose 
            raw text is supposed to be known.
        
        POS_tag: string, or None; the POS tag value to associate to this token
        
        lemma: string, or None; the lemma value to associate to this token
        
        UD_features: dict, or None; 'name' => 'value' map of universal dependency features. Cf the 
            original site: http://universaldependencies.org/u/feat/index.html
    
    Attributes:
        ident: a string identifying the markable; it should be used to be able to distinguish 
            this markable from every other markable of the same type defined in the context of the unique 
            document they refer to.
        
        extent: a (start; end) tuple of two integers representing respectively the position 
            in the document's raw string of the first character of the Markable instance's substring, 
            and of the last character. Assume those index values starts from 1 (the first character in 
            the text start at position 1).
        
        document: the Document instance to which the Markable instance refers
        
        raw_text: string, or None, the substring of the Document instance'raw string corresponding 
            the extent of the Markable instance; if not None, will be compared to the one extracted from 
            the document using the 'extent' info. This allows for consistency checks when defining a 
            markable whose raw text is supposed to be known.
        
        start: integer, the position in the document's raw text string of the first character of 
            the instance's raw text
        
        end: integer, the position in the document's raw text string of the last character of 
            the instance's raw text
        
        extent_id: string representing the extent used to define the instance
        
        POS_tag: string, or None; the POS tag value to associate to this token
        
        lemma: string, or None; the lemma value to associate to this token
        
        UD_features: dict, or None; 'name' => 'value' map of universal dependency features. Cf the 
            original site: http://universaldependencies.org/u/feat/index.html 
        
    The following attributes are references to others markable structures potentially defined for 
    the Document the Markable is part of; they are determined, and their value are set, during a 
    synchronization phase that occurs when using a DocumentBuffer to build a Document instance, 
    which is why it is important to use this class to aggregate data to build a Document instance.
    
    Attributes:
        named_entity: potential NamedEntity instance sharing this Markable's extent and Document 
            reference; or None
         
        constituency_tree_node: corresponding leaf node of the constituency tree associated to the 
            sentence this token is part of, if the data is available; or None
    """
    
    _NAME = {"sg": "token", "plr": "tokens"}
    #_OPTIONAL_ATTRIBUTE_NAMES = ("POS_tag", "lemma",)
    _OPTIONAL_ATTRIBUTE_NAMES = ("POS_tag", "lemma", "UD_features")
    _INNER_ATTRIBUTE_NAMES = Markable._INNER_ATTRIBUTE_NAMES + ("POS_tag", "lemma", "UD_features")
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = ("POS_tag", "lemma", "UD_features", "named_entity") +\
                                                 Markable._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    
    def __init__(self, ident, extent, document, raw_text=None, POS_tag=None, lemma=None, UD_features=None):
        Markable.__init__(self, ident, extent, document, raw_text=raw_text)
        # Non-compulsory but still specific attributes
        self.POS_tag = POS_tag
        self.lemma = lemma
        self.UD_features = UD_features
        
        # Possible reference to encompassed text structures linked to this text structure
        self.named_entity = None
        
        # Reference to encompassing text structure
        self.constituency_tree_node = None
        self.sentence = None
    
    # Instance methods
    def __repr__(self):
        string = "{}(ident='{}', extent='{}', raw_text='{}', POS_tag='{}', lemma='{}', UD_feature='{}')"
        string = string.format(self.__class__.__name__, self.ident, self.extent, self.raw_text, 
                               self.POS_tag, self.lemma, self.UD_features)
        return string
    
    def update(self, data):
        """ Updates the attribute whose name is one of the input dict's field with the corresponding 
        value.
        
        Args:
            data: a dict instance whose fields correspond to the attribute names of the markable
                which one wants to update. One can modify through this method only the attributes that were 
                optional at initialization time (see 'from_dict' method's docstring)
        """
        _set_attributes_from_dict(self, self._OPTIONAL_ATTRIBUTE_NAMES, data)
    
    def to_xml_element(self):
        """ Creates a 'xml.etree.ElementTree.Element' instance containing the data used to identify 
        this instance.
        
        It is such that when the corresponding xml code is pretty printed, it looks like the example 
        below:
        
        Ex:
        
        | <token id="4-15">
        |   <extent end="382" start="372" />
        |   <text>association</text>
        |   <word>association</word>
        |   <lemma>association</lemma>
        |   <POS>NN</POS>
        | </token>
        
        References to encompassed or encompassing text structures, such as NamedEntity, or Sentence 
        instance, are not persisted this way, since they result from a synchronization of data that 
        must be independently persisted.
        
        Returns:
            an xml.etree.ElementTree.Element instance
        """
        EASY_MENTION_ATTRIBUTE_NAMES = ("POS_tag", "lemma")
        # Build element
        token_elt = ET.Element(self._NAME["sg"], id=self.ident)
        # Extent
        start, end = self.extent[0], self.extent[1]
        ET.SubElement(token_elt, "extent", start=str(start), end=str(end))
        # Raw_text
        raw_text = self.raw_text
        text_elt = ET.SubElement(token_elt, "text")
        text_elt.text = xml_escape(raw_text)
        # Other
        for name in EASY_MENTION_ATTRIBUTE_NAMES:
            value = getattr(self, name)
            if value is not None:
                elt = ET.SubElement(token_elt, name)
                elt.text = xml_escape(str(value))
        # UD_features
        UD_features = self.UD_features
        if UD_features is not None:
            elt = ET.SubElement(token_elt, "UD_features")
            for feature_name, feature_value in UD_features.items():
                subelt = ET.SubElement(elt, feature_name)
                subelt.text = xml_escape(str(feature_value))
        return token_elt
    
    # Class methods
    @classmethod
    def __checks_type(cls, other):
        """ Checks whether or not the input object is an instance of the Token class.
        
        Args:
            other: a python object

        Returns:
            boolean
        """
        cls._checks_type(other, cls)
    
    @classmethod
    def from_dict(cls, data):
        """ Creates an instance from the values contained in the input dict.
        
        The field names must match the parameter names defined for the initialization of an instance.
        'ident', 'extent', and 'document' fields are compulsory.
        
        Args:
            data: a python dict

        Returns:
            a Token instance
        """
        ident = data["ident"]
        extent = data["extent"]
        document = data["document"]
        raw_text = data.get("raw_text")
        
        token = Token(ident, extent, document, raw_text=raw_text)
        _set_attributes_from_dict(token, cls._OPTIONAL_ATTRIBUTE_NAMES, data)
        
        return token
    
    @classmethod
    def from_xml_element_to_dict(cls, xml_element):
        """ Parses the input 'xml.etree.ElementTree.Element' instance to create a dict instance.
        This dict instance could be used, for example, to create an instance using the 'from_dict' 
        method (though to do that one would need to add the corresponding 'document' field to be in 
        the dict).
        
        If the input element does not correspond to a xml element related to a mention, return None.
        
        Args:
            xml_element: a xml.etree.ElementTree.Element instance

        Returns:
            a python dict
        """
        EASY_MENTION_ATTRIBUTE_NAMES = [(("POS_tag", "POS_tag"), str), (("lemma", "lemma"), str)]
        data_dict = None
        if xml_element.tag != "token":
            #raise IncorrectXMLElementError("Input parameter is not a 'token' tagged xml element.")
            return data_dict
        
        # Initialize the dict
        data_dict = {}
        # Ident
        ident = xml_element.get("id")
        if ident is None:
            raise IncorrectXMLElementError("The input xml element contains no 'id' attribute.")
        data_dict["ident"] = ident
        # Extent
        extent_elts = xml_element.findall("extent")
        if len(extent_elts) != 1:
            raise IncorrectXMLElementError("The input xml element contains no or more than one 'extent' element.")
        extent_elt = extent_elts[0]
        extent = int(extent_elt.get('start')), int(extent_elt.get('end'))
        data_dict["extent"] = extent
        # Raw_text
        add_unique_field_to_dict_from_subelt(xml_element, ("text", "raw_text"), data_dict, str, enforce_presence=True)
        # Others
        for names, class_ in EASY_MENTION_ATTRIBUTE_NAMES:
            add_unique_field_to_dict_from_subelt(xml_element, names, data_dict, class_, enforce_presence=False)
        # UD_features
        UD_features_subelts = xml_element.findall("UD_features")
        L = len(UD_features_subelts)
        if L > 0:
            if len(UD_features_subelts) > 1:
                raise IncorrectXMLElementError("The input xml element contains more than one 'UD_features' element.")
            UD_features_subelt = UD_features_subelts[0]
            UD_features = OrderedDict()
            for child_elt in UD_features_subelt:
                feature_name = child_elt.tag
                feature_value = child_elt.text
                if feature_value is not None:
                    UD_features[feature_name] = feature_value
            data_dict["UD_features"] = UD_features
        return data_dict



class Mention(Markable):
    """ Class whose role is to represent a mention of a document.
    
    A Mention is a Markable that represent a reference to a specific entity (such as a person, a  
    location, a specific action...). It can made of several tokens.
    
    Arguments:
        ident: a string identifying the markable; it should be used to be able to distinguish 
            this markable from every other markable of the same type defined in the context of the unique 
            document they refer to.
        
        extent: a (start; end) tuple of two integers representing respectively the position 
            in the document's raw string of the first character of the Markable instance's substring, 
            and of the last character. Assume those index values starts from 1 (the first character in 
            the text start at position 1).
        
        document: the Document instance to which the Markable instance refers
        
        raw_text: the substring of the Document instance'raw string corresponding the extent 
            of the Markable instance; if not None, will be compared to the one extracted from the document 
            using the 'extent' info. This allows for consistency checks when defining a markable whose 
            raw text is supposed to be known.
        
        head_extent: extent, or None; corresponding to a potential Markable constituting the 
            grammatical head of the mention; in most case, should correspond to a token; except in case 
            of specially defined grammatical head, or if automatic search for the head failed (in which 
            case a heuristic might be to return the mention's extent instead)
    
        gram_type: string, or None; grammatical category of the mention
            Cf 'cortex.parameters.mention_data_tags'; 
    
        gram_subtype: string, or None; finer-grained grammatical category;
            Cf 'cortex.parameters.mention_data_tags'; or None 
        
        gender: string, or None; grammatical gende; cf 'cortex.parameters.mention_data_tags'
        
        number: string, or None; grammatical number; cf 'cortex.parameters.mention_data_tags'
        
        wn_synonyms: collection of strings, or None; representing synonym concepts defined from WordNet
        
        wn_hypernyms: collection of strings, or None; representing hypernym concepts defined from WordNet
        
        referential_probability: float, or None; the probability that the token or group of token 
            associated to this Mention instance is indeed a reference to an entity, if this is not guaranteed 
            to be the case (ex: the 'it' pronoun in English)
        
    Attributes:
        ident: a string identifying the markable; it should be used to be able to distinguish 
            this markable from every other markable of the same type defined in the context of the unique 
            document they refer to.
        
        extent: a (start; end) tuple of two integers representing respectively the position 
            in the document's raw string of the first character of the Markable instance's substring, 
            and of the last character. Assume those index values starts from 1 (the first character in 
            the text start at position 1).
        
        document: the Document instance to which the Markable instance refers
        
        raw_text: string, or None, the substring of the Document instance'raw string corresponding 
            the extent of the Markable instance; if not None, will be compared to the one extracted from 
            the document using the 'extent' info. This allows for consistency checks when defining a 
            markable whose raw text is supposed to be known.
        
        start: integer, the position in the document's raw text string of the first character of 
            the instance's raw text
        
        end: integer, the position in the document's raw text string of the last character of 
            the instance's raw text
        
        extent_id: string representing the extent used to define the instance
    
        head_extent: extent, or None; corresponding to a potential Markable constituting the 
            grammatical head of the mention; in most case, should correspond to a token; except in case 
            of specially defined grammatical head, or if automatic search for the head failed (in which 
            case a heuristic might be to return the mention's extent instead)
    
        gram_type: string, or None; grammatical category of the mention
            Cf 'cortex.parameters.mention_data_tags'; 
    
        gram_subtype: string, or None; finer-grained grammatical category;
            Cf 'cortex.parameters.mention_data_tags'; or None 
        
        gender: string, or None; grammatical gende; cf 'cortex.parameters.mention_data_tags'
        
        number: string, or None; grammatical number; cf 'cortex.parameters.mention_data_tags'
        
        wn_synonyms: collection of strings, or None; representing synonym concepts defined from WordNet
        
        wn_hypernyms: collection of strings, or None; representing hypernym concepts defined from WordNet
        
        referential_probability: float, or None; the probability that the token or group of token 
            associated to this Mention instance is indeed a reference to an entity, if this is not guaranteed 
            to be the case (ex: the 'it' pronoun in English)
    
    The following attributes are references to others markable structures potentially defined for the 
    Document the Markable is part of; they are determined, and their value are set, during a 
    synchronization phase that occurs when using a DocumentBuffer to build a Document instance, 
    which is why it is important to use this class to aggregate data to build a Document instance. 
    
    Attributes:
        tokens: collection of Token instances that constitute the mention; or None, if necessary 
            info was not available during building of the Document this markable refers to
        
        head_tokens: collection of Token instances that constitute the head of the mention; or None, 
            if necessary info was not available during building of the Document this markable refers to
        
        named_entity: potential NamedEntity instance sharing this Markable's extent and Document 
            reference; or None
        
        sentence: Sentence instance this Markable is included in; or None, if necessary info 
            was not available during building of the Document this markable refers to
        
        head_raw_text: string, or None; the raw text corresponding to the head tokens of the mention, 
            if the info is available
        
        id: string, used to distinctly identify this mention in the context of the document it 
            originates from
    """
    
    _NAME = {"sg": "mention", "plr": "mentions"}
    _OPTIONAL_ATTRIBUTE_NAMES = ("head_extent", "gram_type", "gram_subtype", "gender", "number", 
                                "wn_synonyms", "wn_hypernyms", "referential_probability")
    _INNER_ATTRIBUTE_NAMES = Markable._INNER_ATTRIBUTE_NAMES +  ("head_extent", "gram_type", 
                                                               "gram_subtype", "gender", "number", 
                                                               "wn_synonyms", "wn_hypernyms", 
                                                               "referential_probability")
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = ("head_extent", "gram_type", "gram_subtype", "gender", 
                                                "number", "wn_synonyms", "wn_hypernyms", "named_entity", 
                                                "referential_probability", "tokens", "head_tokens") +\
                                                 Markable._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    
    def __init__(self, ident, extent, document, raw_text=None, head_extent=None, gram_type=None, 
                 gram_subtype=None, gender=None, number=None, wn_synonyms=None, wn_hypernyms=None, 
                 referential_probability=None):
        Markable.__init__(self, ident, extent, document, raw_text=raw_text)
        # Non-compulsory but still specific attributes
        self.head_extent = head_extent
        self.gram_type = gram_type
        self.gram_subtype = gram_subtype
        self.gender = gender
        self.number = number
        self.wn_synonyms = wn_synonyms
        self.wn_hypernyms = wn_hypernyms
        self.referential_probability = referential_probability
        
        # Possible reference to encompassed text structures linked to this text structure
        self._tokens = None
        self._head_tokens = None
        self.named_entity = None
        
        # Possible reference to encompassing text structure
        self.sentence = None
    
    ## Instance attributes / properties
    @property
    def tokens(self):
        return self._tokens
    @tokens.setter
    def tokens(self, value):
        value = tuple(value)
        #"""Automatically synchronize head_tokens after setting the new value, if self.head_token is not None."""
        self.extent2token = OrderedDict((m.extent, m) for m in value)
        self._tokens = tuple(value)
    
    @property
    def head_tokens(self):
        return self._head_tokens
    @head_tokens.setter
    def head_tokens(self, value):
        extent2head_token_new_value = None
        head_tokens_new_value = None
        if value is not None:
            value = tuple(value)
            extent2head_token_new_value = OrderedDict((m.extent, m) for m in value)
            head_tokens_new_value = value
        self.extent2head_token = extent2head_token_new_value
        self._head_tokens = head_tokens_new_value
        
    @property
    def head_extent(self):
        return self._head_extent
    @head_extent.setter
    def head_extent(self, value):
        if value is not None:
            self._is_correct_extent(value, self.document, None)
            head_start_pos, head_end_pos = value
            start_pos, end_pos = self.extent
            if head_start_pos < start_pos or head_end_pos > end_pos:
                raise ValueError("Inconsistencies between input head extent value '{}' and mention's "\
                                 "extent '{}'.".format(value, self.extent))
        self._head_extent = value
    
    @property
    def head_raw_text(self):
        if self.head_extent is None:
            raise NoneHeadExtentError("Attribute 'head_extent' is None, cannot proceed.")
        return self._extract_raw_text(self.document, self.head_extent)
    @head_raw_text.setter
    def head_raw_text(self, value):
        raise AttributeError
    
    @property
    def id(self):
        return "{d},{d}:{d},{d}".format(self.extent, self.head_extent)
    @id.setter
    def id(self, value):
        raise AttributeError
    
    @property
    def wn_synonyms(self):
        return self._wn_synonyms
    @wn_synonyms.setter
    def wn_synonyms(self, value):
        if value is not None:
            value = tuple(value)
        self._wn_synonyms = value
    
    @property
    def wn_hypernyms(self):
        return self._wn_hypernyms
    @wn_hypernyms.setter
    def wn_hypernyms(self, value):
        if value is not None:
            value = tuple(value)
        self._wn_hypernyms = value
    
    # Instance methods
    #### Inequalities: basically, the reading order prevails, i.e. if a Mention begins before another, it is 'less' than the other
    def __lt__(self, other):
        self.__checks_type(other)
        strict = True
        if self.head_extent is not None and other.head_extent is not None:
            return extent_greater_than(other.head_extent, self.head_extent, strict=strict)
        return extent_greater_than(other.extent, self.extent, strict=strict)

    def __le__(self, other):
        self.__checks_type(other)
        strict = False
        if self.head_extent is not None and other.head_extent is not None:
            return extent_greater_than(other.head_extent, self.head_extent, strict=strict)
        return extent_greater_than(other.extent, self.extent, strict=strict)
    
    def __gt__(self, other):
        self.__checks_type(other)
        strict = True
        if self.head_extent is not None and other.head_extent is not None:
            return extent_greater_than(self.head_extent, other.head_extent, strict=strict)
        return extent_greater_than(self.extent, other.extent, strict=strict)
    
    def __ge__(self, other):
        self.__checks_type(other)
        strict = False
        if self.head_extent is not None and other.head_extent is not None:
            return extent_greater_than(self.head_extent, other.head_extent, strict=strict)
        return extent_greater_than(self.extent, other.extent, strict=strict)
      
    def __repr__(self):
        string = "{}(ident='{}', extent='{}', raw_text='{}', head_extent='{}', gram_type='{}', "\
                 "gram_subtype='{}', gender='{}', number='{}', wn_synonyms='{}', wn_hypernyms='{}', "\
                 "referential_probability='{}')"
        string = string.format(self.__class__.__name__, self.ident, self.extent, self.raw_text.replace("\n", " "), 
                               self.head_extent, self.gram_type, self.gram_subtype, self.gender, 
                               self.number, self.wn_synonyms, self.wn_hypernyms, self.referential_probability)
        return string
    
    def synchronize_head_tokens(self, strict=False):
        """ Sets the 'head_tokens' attribute value from using the 'tokens' attribute value and the 
        'head_extent' attribute value, if proper info is available
        
        Assumes that the 'tokens' attribute value is indeed a collection of tokens.
        
        Args:
            strict: boolean (default = False), if True, then raise an exception if 'head_extent' 
                is None; if False, do nothing in that case, and do not raise an exception.
        """
        head_tokens = None
        if strict:
            try:
                h_s, h_e = self.head_extent
            except TypeError:
                raise NoneHeadExtentError(str(self)) from None
            head_tokens = tuple(t for t in self.tokens if h_s <= t.extent[0] and t.extent[1] <= h_e)
        else:
            if self.head_extent is not None:
                h_s, h_e = self.head_extent
                head_tokens = tuple(t for t in self.tokens if h_s <= t.extent[0] and t.extent[1] <= h_e)
        self.head_tokens = head_tokens
    
    def update(self, data):
        """ Updates the attribute whose name is one of the input dict's field with the corresponding 
        value.
        
        Args:
            data: a dict instance whose fields correspond to the names of the attributes of the 
                markablewhich one wants to update. One can modify through this method only the attributes 
                that were optional at initialization time (see 'from_dict' method's docstring)
        """
        _set_attributes_from_dict(self, self._OPTIONAL_ATTRIBUTE_NAMES, data)
    
    def to_xml_element(self, with_named_entity_info=False):
        """ Creates a 'xml.etree.ElementTree.Element' instance containing the data used to identify 
        this instance.
        
        It is such that when the corresponding xml code is pretty printed, it looks like the example 
        below:
        
        Ex:
        
        | <mention id="14,19:14,19">
        |   <extent end="19" start="14" />
        |   <text>memory</text>
        |   <head_extent end="19" start="14" />
        |   <gram_subtype>SHORT_UNDET_NP</gram_subtype>
        |   <gram_type>NOMINAL</gram_type>
        |   <number>sg</number>
        |   <gender>neut</gender>
        |   <wn_synonyms>
        |     <wn_synonym>memory.n.01</wn_synonym>
        |     <wn_synonym>memory.n.02</wn_synonym>
        |   </wn_synonyms>
        |   <wn_hypernyms>
        |     <wn_hypernym>representation.n.01</wn_hypernym>
        |     <wn_hypernym>content.n.05</wn_hypernym>
        |   </wn_hypernyms>
        | </mention>
        
        References to encompassed or encompassing text structures, such as NamedEntity, Token, or 
        Sentence instance, are not persisted this way. Though NamedEntity data can be persisted if 
        one wants to use the produced xml file as an extensive description of the mentions' attribute 
        values, rather than for simple, consistent persistence
        
        Args:
            with_named_entity_info: boolean (default = False); specify whether or not to persist 
                info regarding named entity potentially attributed to this mention

        Returns:
            an xml.etree.ElementTree.Element instance
        """
        EASY_MENTION_ATTRIBUTE_NAMES = ("gram_type", "gram_subtype", "gender", "number", 
                                        "referential_probability") # head_extent, wn_synonyms, wn_hypernyms, named_entity
        # Build element
        mention_elt = ET.Element(self._NAME["sg"], id=self.ident)
        # Extent
        start, end = self.extent[0], self.extent[1]
        ET.SubElement(mention_elt, "extent", start=str(start), end=str(end))
        # Raw_text
        raw_text = self.raw_text
        text_elt = ET.SubElement(mention_elt, "text")
        text_elt.text = xml_escape(raw_text)
        # Head_extent
        if self.head_extent:
            start, end = self.head_extent[0], self.head_extent[1]
            ET.SubElement(mention_elt, "head_extent", start=str(start), end=str(end))
        # Other
        for name in EASY_MENTION_ATTRIBUTE_NAMES:
            value = getattr(self, name)
            if value  is not None:
                elt = ET.SubElement(mention_elt, name)
                elt.text = xml_escape(str(value))
        # Wn_synonyms
        if self.wn_synonyms is not None:
            wn_hypernyms_elt = ET.SubElement(mention_elt, "wn_synonyms")
            for wn_synonym in self.wn_synonyms:
                elt = ET.SubElement(wn_hypernyms_elt, "wn_synonym")
                elt.text = xml_escape(str(wn_synonym))
        # Wn_hypernyms
        if self.wn_hypernyms is not None:
            wn_hypernyms_elt = ET.SubElement(mention_elt, "wn_hypernyms")
            for wn_hypernym in self.wn_hypernyms:
                elt = ET.SubElement(wn_hypernyms_elt, "wn_hypernym")
                elt.text = xml_escape(str(wn_hypernym))
        # Named entity (optional)
        if with_named_entity_info and self.named_entity is not None:
            named_entity_elt = self.named_entity.to_xml_element()
            mention_elt.append(named_entity_elt)
        return mention_elt
    
    # Class methods
    @classmethod
    def __checks_type(cls, other):
        """ Checks whether or not the input object is an instance of the Mention class.
        
        Args:
            other: a python object

        Returns:
            a boolean
        """
        cls._checks_type(other, cls)
    
    @classmethod
    def from_dict(cls, data):
        """ Creates an instance from the values contained in the input dict .
        
        The field names must match the parameter names defined for the initialization of an instance.
        'ident', 'extent', 'document' fields are compulsory.
        
        Args:
            data: a python dict

        Returns:
            a Mention instance
        """
        ident = data["ident"]
        extent = data["extent"]
        document = data["document"]
        raw_text = data.get("raw_text")
        
        mention = Mention(ident, extent, document, raw_text=raw_text)
        _set_attributes_from_dict(mention, cls._OPTIONAL_ATTRIBUTE_NAMES, data)
        
        return mention
    
    @classmethod
    def from_xml_element_to_dict(cls, xml_element):
        """ Parses the input 'xml.etree.ElementTree.Element' instance to create a dict instance.
        
        This dict instance could be used, for example to create a Mention instance using the 
        'from_dict' method (though to do that one will need to add the corresponding 'document' 
        field to the dict instance).
        
        If the input element does not correspond to a xml element related to a mention, returns None.
        
        Args:
            xml_element: a xml.etree.ElementTree.Element instance

        Returns:
            a python dict
        """
        EASY_MENTION_ATTRIBUTE_NAMES = [(("gram_type", "gram_type"), str), 
                                        (("gram_subtype", "gram_subtype"), str), 
                                        (("gender", "gender"), str), (("number", "number"), str), 
                                        (("referential_probability", "referential_probability"), float)]
        data_dict = None
        if xml_element.tag != "mention":
            #raise IncorrectXMLElementError("Input parameter is not a 'token' tagged xml element."))
            return data_dict
        
        # Initialize the dict
        data_dict = {}
        # Ident
        ident = xml_element.get("id")
        if ident is None:
            raise IncorrectXMLElementError("The input xml element contains no 'id' attribute.")
        data_dict["ident"] = ident
        # Extent
        extent_elts = xml_element.findall("extent")
        if len(extent_elts) != 1:
            raise IncorrectXMLElementError("The input xml element contains no or more than one 'extent' element.")
        extent_elt = extent_elts[0]
        extent = int(extent_elt.get('start')), int(extent_elt.get('end'))
        data_dict["extent"] = extent
        # Raw_text
        add_unique_field_to_dict_from_subelt(xml_element, ("text", "raw_text"), data_dict, str, enforce_presence=True)
        
        # Head extent
        head_extent_elts = xml_element.findall("head_extent")
        if head_extent_elts:
            if len(head_extent_elts) > 1:
                raise IncorrectXMLElementError("The input xml element contains no or more than one 'head_extent' element.")
            head_extent_elt = head_extent_elts[0]
            head_extent = int(head_extent_elt.get('start')), int(head_extent_elt.get('end'))
            data_dict["head_extent"] = head_extent
        
        # Wn_synonyms, wn_hypernyms
        for sg_name, plr_name in [("wn_synonym", "wn_synonyms"), ("wn_hypernym", "wn_hypernyms")]:
            values = None
            elts = xml_element.findall(plr_name) # At least empty list
            if elts:
                if len(elts) > 1:
                    raise IncorrectXMLElementError("The input xml element contains more than one '{}' element.".format(plr_name))
                elt = elts[0]
                values = []
                for subelt in elt.iterfind(sg_name):
                    value = str(subelt.text)
                    values.append(value)
                if values is not None:
                    data_dict[plr_name] = tuple(values)
        
        # Others
        for names, class_ in EASY_MENTION_ATTRIBUTE_NAMES:
            add_unique_field_to_dict_from_subelt(xml_element, names, data_dict, class_, enforce_presence=False)
        
        return data_dict

class NullMention(Singleton):
    """ Class used to represent the root of the tree (whose nodes are mentions) that represents 
    the coreference relations between the elements of a given universe of mentions.
    
    Cf the 'structured_pairs' resolvers.
    This is a Singleton class.
    
    Attributes:
        extent: (0,0)
        
        gram_type: None
    """
    extent = (0,0)#None
    gram_type = None
NULL_MENTION = NullMention()


class Sentence(Markable):
    """ Class whose role is to represent a sentence of a document.
    
    A Sentence is a Markable that represent a collection of tokens corresponding to a grammatical 
    sentence.
    
    Ir can be associated a constituency tree, whose leaves must exactly correspond to the tokens 
    that make up the sentence.
    
    It can be observed to be uttered by an identified speaker.
    
    It can also be characterized by one or more predicate arguments, given its complexity.
    Here, a predicate argument is a collection of (start_index, end_index, predicate_argument_tag).
    
    Ex: when represented under xml code, such predicate arguments can look like this:
    
    | <predicate_arguments>
    |     <predicate_argument>
    |       <pred_arg end="5" start="1" type="ARGM-TMP" />
    |       <pred_arg end="47" start="9" type="ARG0" />
    |       <pred_arg end="56" start="49" type="V" />
    |       <pred_arg end="128" start="58" type="ARG2" />
    |     </predicate_argument>
    |     <predicate_argument>
    |       <pred_arg end="5" start="1" type="ARGM-TMP" />
    |       <pred_arg end="47" start="9" type="ARG0" />
    |       <pred_arg end="141" start="134" type="V" />
    |       <pred_arg end="232" start="143" type="ARG1" />
    |     </predicate_argument> 
    | </predicate_arguments>
    
    Arguments:
        ident: a string identifying the markable; it should be used to be able to distinguish 
            this markable from every other markable of the same type defined in the context of the unique 
            document they refer to.
        
        extent: a (start; end) tuple of two integers representing respectively the position 
            in the document's raw string of the first character of the Markable instance's substring, 
            and of the last character. Assume those index values starts from 1 (the first character in 
            the text start at position 1).
        
        document: the Document instance to which the Markable instance refers
        
        raw_text: the substring of the Document instance'raw string corresponding the extent 
            of the Markable instance; if not None, will be compared to the one extracted from the document 
            using the 'extent' info. This allows for consistency checks when defining a markable whose 
            raw text is supposed to be known.
        
        constituency_tree_string: string, or None; the string representation of the constituency 
            tree to associate to this sentence. Will be used to create a ConstituencyTree instance that 
            will be used to set the 'constituency_tree' attribute of the Sentence instance.
    
        predicate_arguments: a collection of predicate arguments; or None
        
        speaker: string, or None; specifies the speaker of the sentence, if pertinent to do so (if 
            the sentence is pertaining to a dialogue between two identified entities, for instance)
        
    Attributes:
        ident: a string identifying the markable; it should be used to be able to distinguish 
            this markable from every other markable of the same type defined in the context of the unique 
            document they refer to.
        
        extent: a (start; end) tuple of two integers representing respectively the position 
            in the document's raw string of the first character of the Markable instance's substring, 
            and of the last character. Assume those index values starts from 1 (the first character in 
            the text start at position 1).
        
        document: the Document instance to which the Markable instance refers
        
        raw_text: string, or None, the substring of the Document instance'raw string corresponding 
            the extent of the Markable instance; if not None, will be compared to the one extracted from 
            the document using the 'extent' info. This allows for consistency checks when defining a 
            markable whose raw text is supposed to be known.
        
        start: integer, the position in the document's raw text string of the first character of 
            the instance's raw text
        
        end: integer, the position in the document's raw text string of the last character of 
            the instance's raw text
        
        extent_id: string representing the extent used to define the instance
        
        constituency_tree: the constituency tree corresponding to the sentence, or None
        
        predicate_arguments: the collection of predicate arguments associated to a sentence, or None
        
        speaker: string, or None; specifies the speaker of the sentence, if pertinent to do so
    
    The following attributes are references to others markable structures potentially defined for the 
    Document the Markable is part of; they are determined, and their value are set, during a 
    synchronization phase that occurs when using a DocumentBuffer to build a Document instance, 
    which is why it is important to use this class to aggregate data to build a Document instance.
    
    Attributes:
        tokens: collection of Token instances that constitute the sentence, in reading order; 
            or None
        
        extent2token: a 'extent => Token instance' map of the tokens associated to the Sentence 
            instance
        
        mentions: collection of Mention instances that are included in the sentence, in reading 
            order; or None
        
        extent2mention: a 'extent => Mention instance' map of the mentions associated to the 
            Sentence instance
        
        extent2mention_index: a 'extent => integer' map of the respective index of each mention 
            associated to the Sentence instance (0 <=> first mention found in the Sentence instance, 
            1 <=> second mention...; in reading order)
    """
    
    _NAME = {"sg": "sentence", "plr": "sentences"}
    _OPTIONAL_ATTRIBUTE_NAMES = ("constituency_tree", "predicate_arguments", "speaker")
    _INNER_ATTRIBUTE_NAMES = Markable._INNER_ATTRIBUTE_NAMES + ("constituency_tree", 
                                                              "predicate_arguments", "speaker")
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = ("tokens", "mentions", "constituency_tree", "speaker") +\
                                                 Markable._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    
    def __init__(self, ident, extent, document, raw_text=None, constituency_tree_string=None, 
                 predicate_arguments=None, speaker=None):
        Markable.__init__(self, ident, extent, document, raw_text=raw_text)
        # Non-compulsory but still specific attributes
        constituency_tree = None
        if constituency_tree_string is not None:
            constituency_tree = ConstituencyTree.parse(constituency_tree_string)
        self.constituency_tree = constituency_tree
        self.predicate_arguments = predicate_arguments
        self.speaker = speaker
        
        # Possible reference to encompassed text structures linked to this text structure
        self._tokens = None
        self.extent2token = None
        
        self._mentions = None
        self.extent2mention = None
        self.extent2mention_index = None
        
        #self._named_entities = None
    
    @property
    def predicate_arguments(self):
        return self._predicate_arguments
    @predicate_arguments.setter
    def predicate_arguments(self, value):
        if value is None:
            self._predicate_arguments = None
        else:
            value = tuple(tuple(l) for l in self._format_sort_predicate_arguments(value))
            self._predicate_arguments = value
    
    @property
    def tokens(self):
        return self._tokens
    @tokens.setter
    def tokens(self, value):
        value = tuple(value)
        self.extent2token = OrderedDict((t.extent, t) for t in value)
        self._tokens = value
    
    @property
    def mentions(self):
        return self._mentions
    @mentions.setter
    def mentions(self, value):
        value = tuple(value)
        self.extent2mention = OrderedDict((m.extent, m) for m in value)
        self.extent2mention_index = OrderedDict((m.extent, i) for i, m in enumerate(value))
        self._mentions = value
        
    # Static methods
    @staticmethod
    def _format_sort_predicate_arguments(predicate_arguments):
        """ Processes the input would-be 'predicate_arguments' value into a proper 'predicate_arguments' 
        value.
        
        That means converting the string representation of the index values into integer values, 
        and, within each 'predicate argument' collection, sorting the (start, end, tag) tuples by 
        increasing (start, end) values.
        
        Args:
            predicate_arguments: a collection of 'predicate_argument' values, which are collection 
                of (start, end, predicate argument tag) tuples; cf the class' docstring

        Returns:
            a collection of 'predicate_argument' values, ready to be set as the corresponding 
                attribute value of an instance of this class
        """
        predicate_arguments_ = map(lambda pred_args: [(int(p[0]), int(p[1]), str(p[2])) for p in pred_args], predicate_arguments)
        return sorted(predicate_arguments_, key=lambda x: [sorted(x, key=lambda y: y[:2])[0][:2]])
    
    # Instance methods
    def _inner_eq(self, other):
        """ Test that this class instance is equal to the input object.
        
        That means 
        that:
            - this instance's class is equal or a subclass of the other instance's class
            - all the attributes whose name is present in this '_ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION' 
                class attribute are present for this instance and the other, input instance, and that their 
                values are respectively equal.
        
        Args:
            other: the instance of this class for which to test for equality

        Returns:
            a (boolean, string or None) pair; the second item of the pair will be None if the 
            boolean value is True (all attribute values were found to be equal), or will be the name of 
            the first attribute for which the values were found to not be equal, or 'class' if the object 
            were found not to be equal because the first test for the classes of each instance failed
        """
        value, reason = EqualityMixIn._inner_eq(self, other)
        if not value:
            return value, reason
        if not _set_equal_or_both_None(self.predicate_arguments, other.predicate_arguments):
            return False, "predicate_arguments"
        return True, None
            
    def __repr__(self):
        string = "{}:(ident='{}', extent='{}', raw_text='{}', constituency_tree_string='{}', predicate_arguments='{}', speaker='{}')"
        constituency_tree_string = None if self.constituency_tree is None else self.constituency_tree.string_representation
        string = string.format(self.__class__.__name__, self.ident, self.extent, self.raw_text.replace("\n", " "), 
                               constituency_tree_string, self.predicate_arguments, self.speaker)
        return string
    
    def update(self, data):
        """ Updates the attribute whose name is one of the input dict's field with the corresponding 
        value.
        
        Args:
            data: a dict instance whose fields correspond to the attribute names of the markable
                which one wants to update. One can modify through this method only the attributes that were 
                optional at initialization time (see 'from_dict' method's docstring, except here that, instead 
                of a ('constituency_tree_string', string) key, value pair, it is a 
                ('constituency_tree', ConstituencyTree instance) key, value pair that is expected, should this 
                attribute value be updated).
        """
        _set_attributes_from_dict(self, self._OPTIONAL_ATTRIBUTE_NAMES, data)
    
    def to_dict(self):
        """ Returns a python dict holding this markable's attribute values.
        
        Useful for duplicating a markable, when used in conjunction with the 'from_dict' method.
        
        Returns:
            an "initialization parameter key => value" map
        """
        result = {}
        for attribute_name in self._INNER_ATTRIBUTE_NAMES:
            if attribute_name == "constituency_tree" and self.constituency_tree is not None:
                constituency_tree_string = self.constituency_tree.string_representation
                result["constituency_tree_string"] = constituency_tree_string
            else:
                value = getattr(self, attribute_name)
                if value is not None:
                    result[attribute_name] = value
        return result
    
    def to_xml_element(self):
        """ Creates a 'xml.etree.ElementTree.Element' instance containing the data used to identify 
        this instance.
        
        It is such that when the corresponding xml code is pretty printed, it looks like the example 
        below:
        
        Ex:
        
        | <sentence id="3">
        |   <extent end="287" start="259" />
        |   <text>Its president is Zheng Bing .</text>
        |   <parse>(TOP (S (NP (PRP$ Its) (NN president)) (VP (VBZ is) (NP (NNP Zheng) (NNP Bing))) (. .)))</parse>
        |   <predicate_arguments>
        |     <pred_args>
        |       <pred_arg end="271" start="259" type="ARG1" />
        |       <pred_arg end="274" start="273" type="V" />
        |       <pred_arg end="285" start="276" type="ARG2" />
        |     </pred_args>
        |   </predicate_arguments>
        |   <speaker>speaker#1</speaker>
        | </sentence>
        
        References to encompassed or encompassing text structures, such as Mention or Token
        instances, are not persisted this way.
        
        Returns:
            an xml.etree.ElementTree.Element instance
        """
        EASY_MENTION_ATTRIBUTE_NAMES = ["speaker"]
        # Build element
        sentence_elt = ET.Element(self._NAME["sg"], id=self.ident)
        # Extent
        start, end = self.extent[0], self.extent[1]
        ET.SubElement(sentence_elt, "extent", start=str(start), end=str(end))
        # Raw_text
        raw_text = self.raw_text
        text_elt = ET.SubElement(sentence_elt, "text")
        text_elt.text = xml_escape(raw_text)
        # Constituency tree
        if self.constituency_tree is not None:
            constituency_tree_string = self.constituency_tree.string_representation
            constituency_tree_elt = ET.SubElement(sentence_elt, "parse")
            constituency_tree_elt.text = constituency_tree_string
        
        # Predicate arguments
        if self.predicate_arguments is not None:
            predicate_arguments_elt = ET.SubElement(sentence_elt, "predicate_arguments")
            for pred_args in self.predicate_arguments:
                pred_args_elt = ET.SubElement(predicate_arguments_elt, "pred_args")
                for start, end, type_ in pred_args:
                    ET.SubElement(pred_args_elt, "pred_arg", start=str(start), end=str(end), type=type_)
        
        # Other
        for name in EASY_MENTION_ATTRIBUTE_NAMES:
            value = getattr(self, name)
            if value is not None:
                elt = ET.SubElement(sentence_elt, name)
                elt.text = xml_escape(str(value))
        return sentence_elt
    
    # Class methods
    @classmethod
    def __checks_type(cls, other):
        """ Checks whether or not the input object is an instance of the Sentence class.
        
        Args:
            other: a python object

        Returns:
            a boolean
        """
        cls._checks_type(other, cls)
    
    @classmethod
    def from_dict(cls, data):
        """ Creates an instance from the values contained in the input dict.
        
        The field names must match the parameter names defined for the initialization of an instance.
        'ident', 'extent', 'document' fields are compulsory.
        
        Args:
            data: a python dict

        Returns:
            a Sentence instance
        """
        ident = data["ident"]
        extent = data["extent"]
        document = data["document"]
        raw_text = data.get("raw_text")
        
        constituency_tree_string = data.get("constituency_tree_string")
        if constituency_tree_string is not None:
            data["constituency_tree"] = ConstituencyTree.parse(constituency_tree_string)
        
        sentence = Sentence(ident, extent, document, raw_text=raw_text)
        _set_attributes_from_dict(sentence, cls._OPTIONAL_ATTRIBUTE_NAMES, data)
        return sentence
    
    @classmethod
    def from_xml_element_to_dict(cls, xml_element):
        """ Parses the input 'xml.etree.ElementTree.Element' instance to create a dict instance.
        
        This dict instance could be used, for example to create a Sentence instance using the 
        'from_dict' method (though to do that one will need to add the corresponding 'document' 
        field to the dict instance).
        
        If the input element does not correspond to a xml element related to a mention, returns None.
        
        Args:
            xml_element: a xml.etree.ElementTree.Element instance

        Returns:
            a python dict
        """
        EASY_MENTION_ATTRIBUTE_NAMES = [(("speaker", "speaker"), str)]
        data_dict = None
        if xml_element.tag != "sentence":
            #raise IncorrectXMLElementError("Input parameter is not a 'token' tagged xml element.")
            return data_dict
        
        # Initialize the dict
        data_dict = {}
        # Ident
        ident = xml_element.get("id")
        if ident is None:
            raise IncorrectXMLElementError("The input xml element contains no 'id' attribute.")
        data_dict["ident"] = ident
        # Extent
        extent_elts = xml_element.findall("extent")
        if len(extent_elts) != 1:
            raise IncorrectXMLElementError("The input xml element contains no or more than one 'extent' element.")
        extent_elt = extent_elts[0]
        extent = int(extent_elt.get('start')), int(extent_elt.get('end'))
        data_dict["extent"] = extent
        # Raw_text
        add_unique_field_to_dict_from_subelt(xml_element, ("text", "raw_text"), data_dict, str, enforce_presence=True)
        
        # Parse
        constituency_tree_elts = xml_element.findall("parse")
        if constituency_tree_elts:
            if len(constituency_tree_elts) > 1:
                raise IncorrectXMLElementError("The input xml element contains more than one 'parse' element.")
            constituency_tree_elt = constituency_tree_elts[0]
            constituency_tree_string = constituency_tree_elt.text
            if constituency_tree_string is not None:
                data_dict["constituency_tree"] = ConstituencyTree.parse(constituency_tree_string)
        
        # Predicate argument
        predicate_arguments_elts = xml_element.findall("predicate_arguments")
        if predicate_arguments_elts:
            if len(predicate_arguments_elts) > 1:
                raise IncorrectXMLElementError("The input xml element contains more than one 'predicate_arguments' element.")
            predicate_arguments_elt = predicate_arguments_elts[0]
            predicate_arguments = []
            for pred_args_elt in predicate_arguments_elt.iterfind("pred_args"):
                pred_args = []
                for pred_arg_elt in pred_args_elt.findall("pred_arg"):
                    pred_arg = (str(pred_arg_elt.get("start")), str(pred_arg_elt.get("end")), str(pred_arg_elt.get("type")))
                    pred_args.append(pred_arg)
                predicate_arguments.append(pred_args)
            data_dict["predicate_arguments"] = predicate_arguments
        
        # Others
        for names, class_ in EASY_MENTION_ATTRIBUTE_NAMES:
            add_unique_field_to_dict_from_subelt(xml_element, names, data_dict, class_, enforce_presence=False)
        
        return data_dict



class Quotation(Markable):
    """ Class whose role is to represent a quotation in a documen.
    
    A Quotation is a Markable representing a group of tokens that the Document it originates from 
    specifies that it is / was directly spoken by someone (for instance trough the use of quotation 
    marks at the beginning and at the end of the markanle).
    
    Arguments:
        ident: a string identifying the markable; it should be used to be able to distinguish 
            this markable from every other markable of the same type defined in the context of the unique 
            document they refer to.
        
        extent: a (start; end) tuple of two integers representing respectively the position 
            in the document's raw string of the first character of the Markable instance's substring, 
            and of the last character. Assume those index values starts from 1 (the first character in 
            the text start at position 1).
        
        document: the Document instance to which the Markable instance refers
        
        raw_text: the substring of the Document instance'raw string corresponding the extent 
            of the Markable instance; if not None, will be compared to the one extracted from the document 
            using the 'extent' info. This allows for consistency checks when defining a markable whose 
            raw text is supposed to be known.
        
        speaker_mention_extent: extent, or None; the extent of the potential mention referring to 
            the speaker of the quotation (e.g.: "He said '...'.")
    
        speaker_verb_token_extent: extent, or None; the extent of the potential verb token 
            introducing the quotation (e.g.: "He said '...'.")
        
        interlocutor_mention_extent: extent, or None; the extent of the potential mention referring 
            to the interlocutor of the speaker (e.g: "He said '...' to you.")
    
    Attributes:
        ident: a string identifying the markable; it should be used to be able to distinguish 
            this markable from every other markable of the same type defined in the context of the unique 
            document they refer to.
        
        extent: a (start; end) tuple of two integers representing respectively the position 
            in the document's raw string of the first character of the Markable instance's substring, 
            and of the last character. Assume those index values starts from 1 (the first character in 
            the text start at position 1).
        
        document: the Document instance to which the Markable instance refers
        
        raw_text: string, or None, the substring of the Document instance'raw string corresponding 
            the extent of the Markable instance; if not None, will be compared to the one extracted from 
            the document using the 'extent' info. This allows for consistency checks when defining a 
            markable whose raw text is supposed to be known.
        
        start: integer, the position in the document's raw text string of the first character of 
            the instance's raw text
        
        end: integer, the position in the document's raw text string of the last character of 
            the instance's raw text
        
        extent_id: string representing the extent used to define the instance
        
        speaker_mention_extent: the extent of the potential mention referring to the speaker 
            of the quotation (e.g.: "He said '...'."); or None
        
        speaker_verb_token_extent: the extent of the potential verb token introducing the 
            quotation (e.g.: "He said '...'."); or None
        
        interlocutor_mention_extent: the extent of the potential mention referring to the 
            interlocutor of the speaker (e.g: "He said '...' to you."); or None
    
    The following attributes are references to others markable structures potentially defined for the 
    Document the Markable is part of; they are determined, and their value are set, during a 
    synchronization phase that occurs when using a DocumentBuffer to build a Document instance, 
    which is why it is important to use this class to aggregate data to build a Document instance.
    
    Attributes:
        mentions: a collection of Mention instance possibly present in the quoted text, in 
            reading order
        
        speaker_mention: mention referencing the speaker of the quotation, if it exists and 
            if the info is available ; or None
        
        speaker_verb_token: token corresponding to the verb introducing the quotation, if it 
            exists and if the info is available
        
        interlocutor_mention: mention referencing the interlocutor of the speaker, if it exists 
            and if the info is available ; or None
    """
    
    _NAME = {"sg": "quotation", "plr": "quotations"}
    _OPTIONAL_ATTRIBUTE_NAMES = ("speaker_mention_extent", "speaker_verb_token_extent", "interlocutor_mention_extent")
    _INNER_ATTRIBUTE_NAMES = ("speaker_mention_extent", "speaker_verb_token_extent", 
                              "interlocutor_mention_extent") + Markable._INNER_ATTRIBUTE_NAMES
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = ("speaker_mention_extent", "speaker_verb_token_extent", 
                                                 "interlocutor_mention_extent", "mentions", 
                                                 "speaker_mention", "speaker_verb_token", 
                                                 "interlocutor_mention") +\
                                                  Markable._ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION
    
    def __init__(self, ident, extent, document, raw_text=None, speaker_mention_extent=None, 
                 speaker_verb_token_extent=None, interlocutor_mention_extent=None):
        Markable.__init__(self, ident, extent, document, raw_text=raw_text)
        # Non-compulsory but still specific attributes
        self.speaker_mention_extent = speaker_mention_extent
        self.speaker_verb_token_extent = speaker_verb_token_extent
        self.interlocutor_mention_extent = interlocutor_mention_extent
        
        # Possible reference to encompassed text structures linked to this text structure
        self._mentions = None
        self.extent2mention = None
        self.extent2mention_index = None
        
        self.speaker_mention = None
        self.speaker_verb_token = None
        self.interlocutor_mention = None
    
    @property
    def speaker_mention_extent(self):
        return self._speaker_mention_extent
    @speaker_mention_extent.setter
    def speaker_mention_extent(self, value):
        if value is not None:
            self._check_attribute_markables_extent(value, "speaker_mention_extent")
        self._speaker_mention_extent = value
    
    @property
    def speaker_verb_token_extent(self):
        return self._speaker_verb_token_extent
    @speaker_verb_token_extent.setter
    def speaker_verb_token_extent(self, value):
        if value is not None:
            self._check_attribute_markables_extent(value, "speaker_verb_token_extent")
        self._speaker_verb_token_extent = value
        
    @property
    def interlocutor_mention_extent(self):
        return self._interlocutor_mention_extent
    @interlocutor_mention_extent.setter
    def interlocutor_mention_extent(self, value):
        if value is not None:
            self._check_attribute_markables_extent(value, "interlocutor_mention_extent")
        self._interlocutor_mention_extent = value
    
    @property
    def mentions(self):
        return self._mentions
    @mentions.setter
    def mentions(self, value):
        value = tuple(value)
        self.extent2mention = OrderedDict((m.extent, m) for m in value)
        self._mentions = value
    
    # Instance methods
    def _check_attribute_markables_extent(self, extent, attribute_name):
        self._is_correct_extent(extent, self.document, None)
        value_start_pos, value_end_pos = extent
        start_pos, end_pos = self.extent
        if start_pos <= value_start_pos <= end_pos or start_pos <= value_end_pos <= end_pos:
            msg = "Inconsistencies between input {} extent value '{}' and quotation's extent '{}': "\
                  "must be strictly outside the quotation.."
            msg = msg.format(attribute_name, extent, self.extent)
            raise ValueError(msg)
    
    def __repr__(self):
        string = "{}(ident='{}', extent='{}', raw_text='{}', speaker_mention_extent='{}', speaker_verb_token_extent='{}', interlocutor_mention_extent='{}')"
        string = string.format(self.__class__.__name__, self.ident, self.extent, self.raw_text.replace("\n", " "),
                               self.speaker_mention_extent, self.speaker_verb_token_extent, self.interlocutor_mention_extent)
        return string
    
    def update(self, data):
        """ Updates the attribute whose name is one of the input dict's field with the corresponding 
        value.
        
        Args:
            data: a dict instance whose fields correspond to the attribute names of the token 
                which one wants to update. One can modify through this method only the attributes that were 
                optional at initialization time (see 'from_dict' method's docstring)
        """
        _set_attributes_from_dict(self, self._OPTIONAL_ATTRIBUTE_NAMES, data)
    
    def to_xml_element(self):
        """ Creates a 'xml.etree.ElementTree.Element' instance containing the data used to identify 
        this instance.
        
        It is such that when the corresponding xml code is pretty printed, it looks like the example 
        below:
        
        Ex:
        
        | <quotation id="14,19:14,19">
        |   <extent end="19" start="14" />
        |   <text>memory</text>
        |   <speaker_mention_extent end="19" start="14" />
        |   <speaker_verb_token_extent end="19" start="14" />
        |   <interlocutor_mention_extent end="19" start="14" />
        | </quotation>
        
        References to encompassed or encompassing text structures, such as Mention or Token
        instances, are not persisted this way.
        
        Returns:
            xml.etree.ElementTree.Element instance
        """
        # Build element
        xml_elt = ET.Element(self._NAME["sg"], id=self.ident)
        # Extent
        start, end = self.extent[0], self.extent[1]
        ET.SubElement(xml_elt, "extent", start=str(start), end=str(end))
        # Raw_text
        raw_text = self.raw_text
        text_elt = ET.SubElement(xml_elt, "text")
        text_elt.text = xml_escape(raw_text)
        # Extent attributes
        for attribute_name in self._OPTIONAL_ATTRIBUTE_NAMES:
            value = getattr(self, attribute_name)
            if value is not None:
                start, end = value
                ET.SubElement(xml_elt, attribute_name, start=str(start), end=str(end))
        return xml_elt
    
    # Class methods
    @classmethod
    def from_dict(cls, data):
        """ Creates an instance from the values contained in the input dict.
        
        The field names must match the parameter names defined for the initialization of an instance. 
        'ident', 'extent', 'document' fields are compulsory.
        
        Args:
            data: a python dict

        Returns:
            a Sentence instance
        """
        ident = data["ident"]
        extent = data["extent"]
        document = data["document"]
        raw_text = data.get("raw_text")
        
        quotation = Quotation(ident, extent, document, raw_text=raw_text)
        _set_attributes_from_dict(quotation, cls._OPTIONAL_ATTRIBUTE_NAMES, data)
        
        return quotation
    
    @classmethod
    def from_xml_element_to_dict(cls, xml_element):
        """ Parses the input 'xml.etree.ElementTree.Element' instance to create a dict instance.
        
        This dict instance could be used, for example to create a Sentence instance using the 
        'from_dict' method (though to do that one will need to add the corresponding 'document' 
        field to the dict instance).
        
        If the input element does not correspond to a xml element related to a mention, returns None.
        
        Args:
            xml_element: a xml.etree.ElementTree.Element instance

        Returns:
            a python dict
        """
        data_dict = None
        if xml_element.tag != "quotation":
            #raise IncorrectXMLElementError("Input parameter is not a 'token' tagged xml element."))
            return data_dict
        
        # Initialize the dict
        data_dict = {}
        # Ident
        ident = xml_element.get("id")
        if ident is None:
            raise IncorrectXMLElementError("The input xml element contains no 'id' attribute.")
        data_dict["ident"] = ident
        # Extent
        extent_attribute_elts = xml_element.findall("extent")
        if len(extent_attribute_elts) != 1:
            raise IncorrectXMLElementError("The input xml element contains no or more than one 'extent' element.")
        extent_elt = extent_attribute_elts[0]
        extent = int(extent_elt.get('start')), int(extent_elt.get('end'))
        data_dict["extent"] = extent
        # Raw_text
        add_unique_field_to_dict_from_subelt(xml_element, ("text", "raw_text"), data_dict, str, enforce_presence=True)
        
        # Extent attributes
        for attribute_name in cls._OPTIONAL_ATTRIBUTE_NAMES:
            extent_attribute_elts = xml_element.findall(attribute_name)
            if extent_attribute_elts:
                if len(extent_attribute_elts) > 1:
                    msg = "The input xml element contains no or more than one '{}' element.".format(attribute_name)
                    raise IncorrectXMLElementError(msg)
                extent_attribute_elt = extent_attribute_elts[0]
                extent_attribute_value = int(extent_attribute_elt.get('start')), int(extent_attribute_elt.get('end'))
                data_dict[attribute_name] = extent_attribute_value
        
        return data_dict


# Utilities
def _set_attributes_from_dict(obj, attribute_names, data):
    """ Sets attributes values of a python object corresponding to the fields of a python dictionary 
    identified by the strings contained in the input collection.
    
    If such a value is not defined, for a given attribute name, then the value None will be used.
    
    Args:
        obj: a python object, whose attribute values are to be set
        attribute_names: a collection of string, of the inpyt python object's attribute names
        data: a python dictionary, assumed to contain the attribute values to set
    """
    for name in attribute_names:
        value = data.get(name)
        if value is not None:
            setattr(obj, name, value)

def _set_equal_or_both_None(elt1, elt2):
    """ Checks whether or not both inputs are None or are equal sets (True) or not (one is None and 
    not the other, or both are distinct sets, False)
    
    Args:
        elt1: first element of the comparison
        elt2: second element of the comparison

    Returns:
        a boolean
    """
    if elt1 is None and elt2 is None:
        return True
    if elt1 is None or elt2 is None:
        return False
    if set(elt1) == set(elt2):
        return True
    return False

def extent_greater_than(extent1, extent2, strict=True):
    """ Defines the base function used to determine an order relation between extents.
    
    Returns True if extent1 is greater than extent2, i.e. if extent1 begins after extent2, or if 
    extent1 ends after extent2 in the case that they begin simultaneously.
    Returns False otherwise.
    
    Args:
        strict: boolean; it True, extent1 will considered greater than extent2 iff start1 > start2; 
            and iff end1 > end2 if start1 == start2. Else it is the 'or equal' version of the the inequalities 
            that will be used. 

    Returns:
        boolean
    """
    # FIXME: replace by '(s1,e1) > (s2,e2)' or '(s1,e1) >= (s2,e2)', since it gives out the same result?
    s1, e1 = extent1
    s2, e2 = extent2
    if strict:
        if s1 == s2:
            return e1 > e2
        return s1 > s2
    if s1 == s2:
        return e1 >= e2
    return s1 >= s2

class ExtentComparisonKey(object):
    """ Class which is to be used to compare Markable instances when sorting an iterable of 
    Markable instances, in which case this class should be passed as the 'key' parameter of the 
    'sort' or 'sorted' method.
    """
    def __init__(self, obj, *args, **kargs):
        self.obj = obj
    def __lt__(self, other):
        return extent_greater_than(other.obj, self.obj, strict=True)
    def __gt__(self, other):
        return extent_greater_than(self.obj, other.obj, strict=True)
    def __eq__(self, other):
        return self.obj == other.obj
    def __le__(self, other):
        return extent_greater_than(other.obj, self.obj, strict=False)
    def __ge__(self, other):
        return extent_greater_than(self.obj, other.obj, strict=False)
    def __ne__(self, other):
        return self.obj != other.obj
    def __hash__(self):
        raise NotImplemented

def get_mention_pair_extent_pair(mention1, mention2):
    """ Returns the pair of extents corresponding to pair made from the input mentions.
    
    Args:
        mention1: a Mention instance
        mention2: a Mention instance

    Returns:
        a (extent1, extent2) pair
    """
    return (mention1.extent, mention2.extent)

    
# Exceptions
class MentionDoesNotExistError(CoRTeXException):
    """ Exception to be raised when expecting to find a specific mention, and failing to do so. """
    pass

class NoneHeadExtentError(CoRTeXException):
    """ Exception to be raised when carried out a process that assumes that the head extent attribute 
    value of a mention is correctly defined, when it is not, in fact.
    """
    pass
