# -*- coding: utf-8 -*-

"""
Defines utilities used to aggregate data about, build, and manipulate document class instances.
"""

__all__ = ["DocumentBuffer",
           "deepcopy_document",
           "remove_mentions_and_coreference_partition_info",
           "transfer_coreference_partition_info",
           ]

from copy import deepcopy

from .document import Document
from .markable import Token, NamedEntity, Mention, Sentence, Quotation
from .coreference_partition import CoreferencePartition



_SENTINEL_VALUE = -1
_MARKABLE_CLASSES = (Token, NamedEntity, Mention, Sentence, Quotation)
_INNER_EQ_ATTRIBUTE_NAMES = tuple("_extent2{}_data_add".format(markable_class._NAME["sg"]) for markable_class in _MARKABLE_CLASSES) +\
                           tuple("_to_remove_{}_extents".format(markable_class._NAME["sg"]) for markable_class in _MARKABLE_CLASSES) +\
                           ("_coreference_partition",)

class DocumentBuffer(object):
    """ Class used to aggregate data about a document, and update the corresponding Document instance.
    
    The idea is to specify a certain number of modification to the Document instance data (mostly 
    pertaining to the Markable it is made of), and then commit the whole of those modifications at 
    once (notably, ensuring that the Document instance's updated data is correctly synchronized).
    
    Arguments:
        document: a Document instance, or None; the document to modify
    
    Attributes:
        ident: the 'ident' attribute value of the Document instance currently being modified by 
            this class instance.
        
        raw_text: the 'raw_text' attribute value of the Document instance currently being modified 
            by this class instance.
    
    Examples:
        >>> document_to_update = Document(ident, raw_text)
        >>> document_buffer = DocumentBuffer(document_to_update)
        
        or
        
        >>> document_buffer.initialize_anew(document_to_update)
        
        >>> document_buffer.add_tokens(extent2token_dict)
        >>> document_buffer.add_named_entities(extent2named_entity_dict
        >>> document_buffer.add_sentences(extent2sentence_dict)
        >>> document_buffer.add_mentions(extent2mention_dict)
        >>> document_buffer.add_quotations(extent2quotation_dict)
        >>> document_buffer.set_coreference_partition(coreference_partition)
        >>> document_buffer.flush() (to the previously input modification(s) commit to the associated Document instance)
        >>> document_buffer.clean() (if one wants to start buffering anew for the associated Document instance)
        
        There also exists the following modification methods:
        
        >>> document_buffer.remove_named_entities(named_entity_extents)
        >>> document_buffer.remove_mentions(mention_extents)
        >>> document_buffer.remove_coreference_partition()
    """
    
    def __init__(self, document=None):
        if document is not None:
            self.initialize_anew(document)
    
    @property
    def ident(self):
        return self._document.ident
    @ident.setter
    def ident(self, value):
        raise AttributeError
    
    @property
    def raw_text(self):
        return self._document.raw_text
    @raw_text.setter
    def raw_text(self, value):
        raise AttributeError
    
    # Instance methods
    def initialize_anew(self, document):
        """ Makes it so the future modifications to be registered shall be applied to the input document.
        
        Args:
            document: a Document instance, the document on which future modifications shall be applied
        """
        if not isinstance(document, Document):
            raise ValueError("'document' input is not a Document instance (type = '{}').".format(type(document)))
        self._document = document
        
        for markable_class in _MARKABLE_CLASSES:
            self._initialize_markable_data_buffers(markable_class)
        
        self._coreference_partition = _SENTINEL_VALUE
    
    def clean(self):
        """ Empties the buffer without applying the modifications that were potentially registered up 
        until now. In order to be used again, the DocumentBuffer instance will have to be initialized 
        anew with a Document instance.
        """
        for attribute_name in ("_document", "_coreference_partition"):
            if hasattr(self, attribute_name):
                delattr(self, attribute_name)
        for markable_class in _MARKABLE_CLASSES:
            self._remove_markable_data_buffers(markable_class)
    
    ## Tokens
    def add_tokens(self, extent2data):
        """ Programs the addition of tokens to the document being built / edited.
        
        The input consists 
        in a 'token extent => dict' map, where 'token extent' is the extent of the token to add to 
        the document, and 'dict' is a 'field => value' map containing data about the token to add 
        (POS_tag, lemma...). Such dict instances must be suitable inputs for the 'from_dict' method 
        of the Token class, except for the 'document' and 'extent' fields, which will be 
        automatically added.
        
        Args:
            extent2data: an 'extent => data' map, where 'data' are dict instances containing data 
                about Token instances (cf Token.from_dict)
        """
        self._add_markables(extent2data, Token)
    
    ## Named entities
    def add_named_entities(self, extent2data):
        """ Programs the addition of named entities to the document being built / edited.
        
        The input 
        consists in a 'named entity extent => dict' map, where 'named entity extent' is the extent 
        of the named entity to add to the document, and 'dict' is a 'field => value' map containing 
        data about the token to add (type...). Such dict instances must be suitable inputs for the 
        'from_dict' method of the NamedEntity class, except for the 'document' and 'extent' fields, 
        which will be automatically added.
        
        Args:
            extent2data: an 'extent => data' map, where 'data' are dict instances containing data 
                about NamedEntity instances (cf NamedEntities.from_dict)
        """
        self._add_markables(extent2data, NamedEntity)
    
    def remove_named_entities(self, extents):
        """ Programs the removal of named entities from the document.
        The input consists in a set of extents corresponding to NamedEntity instances currently 
        associated to the Document instance to modify.
        
        Args:
            extents: a collection of extents corresponding to NamedEntity instances to remove from 
                the document
        """
        self._remove_markables(extents, NamedEntity)
    
    ## Mentions
    def add_mentions(self, extent2data):
        """ Programs the addition of mentions to the document being built / edited. The input consists 
        in a 'mention extent => dict' map, where 'mention extent' is the extent of the mention to 
        add to the document, and 'dict' is a 'field => value' map containing data about the mention 
        to add (gram_type...). Such dict instances must be suitable inputs for the 'from_dict' method 
        of the mention class, except for the 'document' and 'extent' fields, which will be 
        automatically added.
        
        Args:
            extent2data: an 'extent => data' map, where 'data' are dict instances containing data 
                about Mention instances (cf Mention.from_dict)
        """
        self._add_markables(extent2data, Mention)
    
    def remove_mentions(self, extents):
        """ Programs the removal of mentions from the document.
        
        The input consists in a set of extents corresponding to Mention instances currently 
        associated to the Document instance to modify.
        
        Args:
            extents: a collection of extents corresponding to Mention instances to remove from 
                the document
        """
        self._remove_markables(extents, Mention)
    
    ## Sentences
    def add_sentences(self, extent2data):
        """ Programs the addition of sentencrs to the document being built / edited.
        
        The input consists 
        in a 'sentence extent => dict' map, where 'sentence extent' is the extent of the sentence to 
        add to the document, and 'dict' is a 'field => value' map containing data about the sentence 
        to add (constituency_tree_string...). Such dict instances must be suitable inputs for the 
        'from_dict' method of the sentence class, except for the 'document' and 'extent' fields, 
        which will be automatically added.
        
        Args:
            extent2data: an 'extent => data' map, where 'data' are dict instances containing data 
                about Sentence instances (cf Sentence.from_dict)
        """
        self._add_markables(extent2data, Sentence)
    
    ## Sentences
    def add_quotations(self, extent2data):
        """ Programs the addition of quotations to the document being built / edited.
        
        The input consists 
        in a 'quotation extent => dict' map, where 'quotation extent' is the extent of the quotation 
        to add to the document, and 'dict' is a 'field => value' map containing data about the 
        quotation to add (speaker_mention_extent...). Such dict instances must be suitable inputs 
        for the 'from_dict' method of the quotation class, except for the 'document' and 'extent' 
        fields, which will be automatically added.
        
        Args:
            extent2data: an 'extent => data' map, where 'data' are dict instances containing data 
                about Quotation instances (cf Quotation.from_dict)
        """
        self._add_markables(extent2data, Quotation)
    
    ## Coreference partition data
    def set_coreference_partition(self, coreference_partition):
        """ Progams the action of setting the 'coreference_partition' attribute value of the 
        Document instance to modify, to the input value.
        
        Args:
            coreference_partition: a CoreferencePartiton instance
        """
        if not isinstance(coreference_partition, CoreferencePartition):
            message = "'coreference_partition' input is not a CoreferencePartition instance (type = '{}')."
            message = message.format(type(coreference_partition))
            raise ValueError(message)
        self._coreference_partition = coreference_partition
    
    def remove_coreference_partition(self):
        """ Programs the action of setting the 'coreference_partition' attribute value, of the 
        Document instance to modify, to the 'None' value.
        """
        self._coreference_partition = None
    
    ## Convert to document
    def flush(self, strict=False, enforce_coreference_partition_consistency=False):
        """ Commits the programmed modifications, and re-synchronize the Document instance, before 
        returning it.
        
        Args:
            strict: boolean, whether or not to raise an exception if the head_extent of a Mention 
                instance is None when trying to synchronize the Mention instance with the Token instance(s) 
                constituting its head
            enforce_coreference_partition_consistency: boolean; if True, remove from the final 
                coreference partition the potential extents that do not correspond to a Mention in the 
                modified Document; if False, will raise an Exception if such an inconsistency is found
        
        Returns:
            the edited Document instance whose edition this DocumentBuffer was used for
        
        Warning:
            After a flush, the DocumentBuffer instance will be cleaned, so all registered modifications 
            will be lost; also, it will lose its synchronization with the Document instance.
        """
        document = self._document
        
        # Flush the modification on each markable collection
        markable_classes = _MARKABLE_CLASSES#(Token, NamedEntity, Mention, Sentence, Quotation)
        modified_markable_collection = set()
        for markable_class in markable_classes:
            was_modified = self._flush_modify_markables(markable_class)
            if was_modified:
                modified_markable_collection.add(markable_class._NAME["sg"])
        
        # Synchronize the markables if possible
        tokens = document.tokens
        named_entities = document.named_entities
        mentions = document.mentions
        sentences = document.sentences
        quotations = document.quotations
        if tokens and sentences and ("token" in modified_markable_collection or "sentence" in modified_markable_collection):
            self._synchronize_tokens_sentences()
        if tokens and mentions and ("token" in modified_markable_collection or "mention" in modified_markable_collection):
            self._synchronize_tokens_mentions()
            self._synchronize_head_tokens_mentions(strict=strict)
        if mentions and sentences and ("mention" in modified_markable_collection or "sentence" in modified_markable_collection):
            self._synchronize_mentions_sentences()
        if named_entities and tokens and ("named_entity" in modified_markable_collection or "token" in modified_markable_collection):
            self._synchronize_named_entities_tokens()
        if named_entities and mentions and ("named_entity" in modified_markable_collection or "mention" in modified_markable_collection):
            self._synchronize_named_entities_mentions()
        if mentions and quotations and ("mention" in modified_markable_collection or "quotation" in modified_markable_collection):
            self._synchronize_quotation_with_tokens_and_mentions()
        
        # Coreference partition
        ## Modify the coreference partition only if the stored value is not the sentinel's anymore (and so, if there is indeed a will to modify the coreference partition's value)
        coreference_partition = self._coreference_partition
        coreference_partiton_was_modified = False
        if coreference_partition != _SENTINEL_VALUE:
            if mentions is None:
                message = "A coreference partition has been defined, but no mention exists in the edited Document instance."
                raise ValueError(message)
            else:
                document.coreference_partition = coreference_partition
                coreference_partiton_was_modified = True
        
        # Check consistency between mentions and coreference partition if either one has been modified
        coreference_partition = document.coreference_partition
        if ("mention" in modified_markable_collection or coreference_partiton_was_modified) and coreference_partition is not None:
            document_mention_extents = set(m.extent for m in mentions)
            partition_mention_extents = coreference_partition.get_universe()
            diff = partition_mention_extents.difference(document_mention_extents)
            if not enforce_coreference_partition_consistency:
                if diff:
                    message = "The following extent(s) exist(s) in the coreference partition, but "\
                              "no mention corresponding to them exist in the edited Document instance: {}."
                    message = message.format(sorted(diff))
                    raise ValueError(message)
            else:
                for mention_extent in diff:
                    coreference_partition.remove_element(mention_extent)
        
        # Clean the buffer
        self.initialize_anew(document)
        
        return document
    
    def _initialize_markable_data_buffers(self, markable_class):
        """ Initializes anew the cache structures associated to a given markable type.
        
        Args:
            markable_class: a Markable child class, such as Token, NamedEntities, Mentions, 
                Sentence, Quotation
        """
        markable_class_name_sg = markable_class._NAME["sg"]
        setattr(self, "_extent2{}_data_add".format(markable_class_name_sg), {})
        setattr(self, "_to_remove_{}_extents".format(markable_class_name_sg), set())
    
    def _remove_markable_data_buffers(self, markable_class):
        """ Removes the attributes of this DocumentBuffer instance about the markable type specified 
        by the input Markable child class.
        
        Args:
            markable_class: a Markable child class, such as Token, NamedEntities, Mentions, 
                Sentence, Quotation
        """
        markable_class_name_sg = markable_class._NAME["sg"]
        for attribute_base_name in ("_extent2{}_data_add", "_to_remove_{}_extents"):
            attribute_name = attribute_base_name.format(markable_class_name_sg)
            if hasattr(self, attribute_name):
                delattr(self, attribute_name)
    
    def _add_markables(self, extent2markable_data, markable_class):
        """ Programs the addition of new markable class instance(s) to Document instance, using the 
        provided data.
        
        The input consists in a 'extent => dict' map, where 'extent' is the extent of 
        the markable to add to the document, and 'dict' is a 'field => value' map containing data 
        about the Markable child class instance to add.
        Each 'markable_data' map must contain the necessary information to create an instance of 
        the provided class when passed to the 'from_dict' method of the class, except the 'extent' 
        and 'document' data, which is added by this method:
        - the 'extent' value is supposed to be the key associated to a 'markable_data' object
        - the 'document' value is the Document instance currently worked on by the DocumentBuffer 
        instance
        
        Args:
            extent2markable_data: an 'extent => data' map, where 'data' are dict instances containing 
                data about a Markable instance
            markable_class: a Markable child class, such as Token, NamedEntities, Mentions, 
                Sentence, Quotation
        """
        if extent2markable_data:
            markable_class_name_sg = markable_class._NAME["sg"]
            current_extent2markable_data = getattr(self, "_extent2{}_data_add".format(markable_class_name_sg))
            
            # Check that extent of markable to be added / replaced are compatible with the 'raw_text' attribute of the Document instance
            extents_to_add = sorted(extent2markable_data.keys())
            min_s = extents_to_add[0][0]
            max_e = extents_to_add[-1][1]
            doc_s, doc_e = 1, len(self._document.raw_text)
            if not (doc_s <= min_s and max_e <= doc_e):
                message = "The most extreme(s) {} to add ('{}' and '{}') correspond(s) to extent(s) "\
                          "that are incompatible with the extents permitted by the length of the "\
                          "text of the document ('{}', document ident = '{}')"
                message = message.format(markable_class_name_sg, extents_to_add[0], extents_to_add[-1], 
                                         (doc_s, doc_e), self._document.ident)
                raise ValueError(message)
            '''
            # Check that the markables to be added are not already defined in the Document instance
            document_extent2markable = getattr(self._document, "extent2{}".format(markable_class_name_sg))
            if document_extent2markable is not None:
                for extent in extent2markable_data.keys():
                    if extent in document_extent2markable:
                        message = "A '{}' markable with the extent '{}' already exists in the Document "\
                                  "instance, it cannot be added anew."
                        message = message.format(markable_class_name_sg, extent)
                        raise ValueError(message)
            '''
            current_extent2markable_data.update(extent2markable_data)
    
    '''
    def _update_markables(self, extent2markables_data, markable_class):
        """ Programs the update the data of some Markable child class instances currently associated 
        with the Document instance being edited.
        
        The input consists in a 'extent => dict' map, where 
        'extent' is the extent of the markable to add to the document, and 'dict' is a 'field => value' 
        map containing data about the Markable child class instance attribute value to update.
        Cf the 'update' method of the Markable child class.
        
        Args:
            extent2markable_data: an 'extent => data' map, where 'data' are dict instances containing 
                data about Markable 
                instances to update
            markable_class: a Markable child class, such as Token, NamedEntities, Mentions, 
                Sentence, Quotation
        """
        markable_class_name_sg = markable_class.NAME["sg"]
        current_extent2markable_data_update = getattr(self, "_extent2{}_data_update".format(markable_class_name_sg))
        current_extent2markable_data_add = getattr(self, "_extent2{}_data_add".format(markable_class_name_sg))
        document_extent2markable = getattr(self._document, "extent2{}".format(markable_class_name_sg))
        
        for extent, markable_data in extent2markables_data.items():
            if extent in current_extent2markable_data_update:
                current_extent2markable_data_update[extent].update(markable_data)
                continue
            if extent in current_extent2markable_data_add:
                current_extent2markable_data_add[extent].update(markable_data)
                continue
            if extent in document_extent2markable:
                current_extent2markable_data_update[extent] = markable_data
                continue
            message = "Impossible to edit a '{}' markable corresponding to extent '{}', because"\
                      " it does not exist yet (and is not stated to exist yet either)."
            message = message.format(markable_class_name_sg, extent)
            raise ValueError(message)
    '''
    
    def _remove_markables(self, extents, markable_class):
        """ Programs the removal of markables of some type from the document.
        
        The input consists in a set of extents corresponding to instances, of the input Markable 
        child class, currently associated to the Document instance to modify.
        
        Args:
            extents: collection of extents corresponding to instances, of the input Markable child 
                class, to remove from the document
            markable_class: a Markable child class, such as Token, NamedEntities, Mentions, 
                Sentence, Quotation
        """
        if extents:
            markable_class_name_sg = markable_class._NAME["sg"]
            current_extent2markable_add = getattr(self, "_extent2{}_data_add".format(markable_class_name_sg))
            #extent2markable_update = getattr(self, "_extent2{}_update".format(markable_name_sg))
            current_to_remove_markable_extents = getattr(self, "_to_remove_{}_extents".format(markable_class_name_sg))
            document_extent2markable = getattr(self._document, "extent2{}".format(markable_class_name_sg))
            
            if document_extent2markable is not None:
                for extent in extents:
                    removed = False
                    if extent in document_extent2markable:
                        current_to_remove_markable_extents.add(extent)
                        removed = True
                    
                    if not removed:
                        try:
                            del(current_extent2markable_add[extent])
                            removed = True
                        except KeyError:
                            pass
                    '''
                    try:
                        del(extent2markable_update[extent])
                        removed = True
                    except KeyError:
                        pass
                    '''
                    if not removed:
                        message = "The '{}' markable defined by the extent '{}' does not exist, or is not "\
                                  "stated to exist, cannot be removed (document ident = '{}')."
                        message = message.format(markable_class_name_sg, extent, self._document.ident)
                        raise ValueError(message)
    
    def _flush_modify_markables(self, markable_class):
        """ Commits, to the current state of the document, the modifications previously programmed 
        in this buffer through the use of the 'add_...' and 'remove_...' methods.
        
        Assume that the two "markable class caches" do not have contradictory data due to the 
        respective methods used to update their value (in other word, the sets of their respective 
        key set do not overlap). In case they do, the 'add' functionality will win over the 'remove' 
        functionality; that is, if the document buffer is asked at the same time to add a markable 
        with extent 'ext0', and to add a markable with extent 'ext0', then the markable will indeed 
        be added, not removed.
        
        Args:
            markable_class: a Markable child class, such as Token, NamedEntities, Mentions, 
                Sentence, Quotation

        Returns:
            boolean, whether modifications were made to the document, or not (for instance, if 
            there was no programmed modification to commit)
        """
        markable_class_name_sg = markable_class._NAME["sg"]
        # Assume that the following two caches cannot have contradictory data due to the respective methods used to update their value (in other word, the sets of their respective key set do not overlap)
        current_extent2markable_data_add = getattr(self, "_extent2{}_data_add".format(markable_class_name_sg))
        current_to_remove_markable_extents = getattr(self, "_to_remove_{}_extents".format(markable_class_name_sg))
        document_extent2markable = getattr(self._document, "extent2{}".format(markable_class_name_sg))
        
        # Create the markable instances that the user asked they be in the Document instance
        temp_markables = []
        for extent, markable_data in current_extent2markable_data_add.items():
            markable_data["extent"] = extent
            markable_data["document"] = self._document
            markable = markable_class.from_dict(markable_data)
            temp_markables.append(markable)
        
        # Dispatch the markables to be added between two collections, so as to keep track of those that are truly new, and those that replace previous ones
        markables = [] # The collection of Markable class instances to set the Document instance attribute value with.
        new_markables = [] # The collection of Markable class instances to be created and that will corresponding to true new instances.
        replaced_markables = [] # The collection of already existing Markable class instances to be replaced by new ones.
        # If the Document instance already possesses a collection of instances of this Markable class, then we want to keep track of the instances that will replace existing ones.
        if document_extent2markable:
            def _dispatch_fct(markables_):
                for markable_ in markables_:
                    extent = markable_.extent
                    if extent in document_extent2markable:
                        replaced_markables.append(markable_)
                    else:
                        new_markables.append(markable_)
            # Initialize the 'markables' collection with the already existing markables that will not be replaced or removed
            ## The Markable instance to be removed includes to one that will be replaced by new versions, and the one that the user actively asked to remove.
            not_to_keep_markable_instance_extents = set(current_extent2markable_data_add.keys()).union(current_to_remove_markable_extents)
            ## The potential removal of mentions occurs here.
            markables.extend(markable for markable in document_extent2markable.values() if markable.extent not in not_to_keep_markable_instance_extents)
        
        # Else the only thing to do is to add the created Markable class instances.
        else:
            def _dispatch_fct(markables_):
                new_markables.extend(markables_)
        
        # Process with the dispatching
        _dispatch_fct(temp_markables)
        markables.extend(replaced_markables)
        markables.extend(new_markables)
        markables.sort()
        
        # Set the new markable collection into the Document instance
        markables = tuple(markables)
        was_modified = False
        if current_extent2markable_data_add or current_to_remove_markable_extents:
            was_modified = True
        if was_modified:
            self._set_markables_tuple(markables, markable_class)
        
        # Initialize anew the buffer variables associated to to the markable class, now that they have been processed
        self._initialize_markable_data_buffers(markable_class)
        
        return was_modified
    
    def _set_markables_tuple(self, markables_tuple, markable_class):
        """ Sets a collection of Markable child class instances as the value of the appropriate 
        attribute of the Document instance being edited.
        It is assumed that the input Markable child class instances are consistent, and that they 
        are consistent with the input Markable child class.
        
        Args:
            markable_tuple: a collection of Markable child class instances
            markable_class: a Markable child class, such as Token, NamedEntities, Mentions, 
                Sentence, Quotation
        """
        setattr(self._document, markable_class._NAME["plr"], markables_tuple)
    
    
    ## Synchronization
    def _synchronize_tokens_sentences(self):
        """ Synchronizes the references to each others between the Token instances and the Sentence 
        instances currently set a values of the Document instance.
        """
        #"""Assume that a correct ConstituencyTree is defined for each sentence.""" Not anymore for now
        tokens = self._document.tokens # Assume lexicographical order
        sentences = self._document.sentences # Assume lexicographical order
        if tokens is None:
            raise ValueError("DocumentBuffer.update_tokens: no token have been defined yet, cannot update.")
        if sentences is None:
            raise ValueError("DocumentBuffer.update_sentences: no sentence have been defined yet, cannot update.")
        
        # For each sentence, create and set the correct tokens list
        i_sen = 0
        i_tok = 0
        sentence_tokens = []
        while i_tok < len(tokens): # For all tokens...
            token = tokens[i_tok]
            sentence = sentences[i_sen]
            s_tok, _ = token.extent
            _, e_sen = sentence.extent
            if s_tok <= e_sen: # If the token begins before the sentence ends...
                sentence_tokens.append(token) # ..we add it to the sentence (what if it was not reset before?)
                token.sentence = sentence # and we add the sentence to the token
                i_tok += 1
            else: # Else we need to check the next sentence.
                sentences[i_sen].tokens = sentence_tokens
                sentence_tokens = []
                i_sen += 1
        sentences[i_sen].tokens = sentence_tokens
        
        # Synchronize constituency trees and tokens
        for sentence in sentences:
            if sentence.constituency_tree is not None:
                s_tokens = sentence.tokens
                s_leaves = sentence.constituency_tree.leaves
                lst = len(s_tokens)
                lsl = len(s_leaves)
                if lst != lsl:
                    message = "Length of sentence's tokens list ({}) is different from length of "\
                              "sentence's constituency tree leaves list ({}): Sentence: ident='{}, 'extent='{}', "\
                              "constituency tree representation = '{}'\ndocument ident = '{}'."
                    message = message.format(lst, lsl, sentence.ident, sentence.extent, sentence.constituency_tree.string_representation, self._document.ident)
                    raise ValueError(message)
                for pos, token in enumerate(s_tokens):
                    node = s_leaves[pos] # Get the corresponding leaf node object from the parse tree of the sentence, correct because of when the sentence is parsed, the node corresponding to token are added to 'leaves' one at a time, as if they were read
                    #node.leaf_index = pos # FIXME: so, do we keep this attribute or not?
                    token.constituency_tree_node = node
                    node.token = token
    
    def _synchronize_mentions_sentences(self):
        """ Synchronizes the references to each others between the Mention instances and the Sentence 
        instances currently set a values of the Document instance.
        """
        mentions = self._document.mentions # Assume lexicographical order
        sentences = self._document.sentences # Assume lexicographical order
        if mentions is None:
            raise ValueError("_synchronize_mentions_sentences: no mention have been defined yet, cannot update.")
        if sentences is None:
            raise ValueError("_synchronize_mentions_sentences: no sentence have been defined yet, cannot update.")
        
        # For each mention and sentence, synchronize their references within each others
        i_sen = 0
        i_men = 0
        sentence_mentions = []
        while i_men < len(mentions):
            mention = mentions[i_men]
            sentence = sentences[i_sen]
            s_men, _ = mention.extent
            _, e_sen = sentence.extent
            if s_men <= e_sen:
                mention.sentence = sentence
                sentence_mentions.append(mention)
                i_men += 1
            else:
                sentences[i_sen].mentions = sentence_mentions
                sentence_mentions = []
                i_sen += 1
        sentences[i_sen].mentions = sentence_mentions
    
    def _synchronize_tokens_mentions(self):
        """ Synchronizes the references to each others between the Token instances and the Mention 
        instances currently set a values of the Document instance.
        """
        mentions = self._document.mentions # Assume lexicographical order
        tokens = self._document.tokens # Assume lexicographical order
        if tokens is None:
            raise ValueError("DocumentBuffer.update_tokens: no token have been defined yet, cannot update.")
        if mentions is None:
            raise ValueError("DocumentBuffer.update_mentions: no mention have been defined yet, cannot update.")
        
        # Proceed with synchronization: cannot do like for synchronization between tokens and sentence, because there may be nested mentions
        for mention in mentions:
            s_men, e_men = mention.extent
            mention.tokens = [t for t in tokens if s_men <= t.extent[0] and t.extent[1] <= e_men]
    
    def _synchronize_head_tokens_mentions(self, strict=False):
        """ Synchronizes the respective 'head_tokens' attribute value of the Mention instances 
        currently set a values of the Document instance, with the Token instances.
        Assume that each mention possess a non None 'head_extent' attribute.
        """
        for mention in self._document.mentions:
            mention.synchronize_head_tokens(strict=strict)
    
    def  _synchronize_named_entities_tokens(self):
        """ Synchronizes the references to each others between the NamedEntity instances and the Token 
        instances currently set a values of the Document instance.
        """
        tokens = self._document.tokens # Assume lexicographical order
        named_entities = self._document.tokens # Assume lexicographical order
        if tokens is None:
            raise ValueError("DocumentBuffer.update_tokens: no token have been defined yet, cannot update.")
        if named_entities is None:
            raise ValueError("DocumentBuffer.update_named_entities: no named_entity have been defined yet, cannot update.")
        
        for token in tokens:
            token.named_entity = self._document.extent2named_entity.get(token.extent)
    
    def  _synchronize_named_entities_mentions(self):
        """ Synchronizes the references to each others between the NamedEntity instances and the Mention 
        instances currently set a values of the Document instance.
        """
        mentions = self._document.mentions # Assume lexicographical order
        named_entities = self._document.named_entities # Assume lexicographical order
        if mentions is None:
            raise ValueError("DocumentBuffer.update_mentions: no mention have been defined yet, cannot update.")
        if named_entities is None:
            raise ValueError("DocumentBuffer.update_named_entities: no named_entity have been defined yet, cannot update.")
        
        for mention in mentions:
            mention.named_entity = self._document.extent2named_entity.get(mention.extent)
    
    def  _synchronize_quotation_with_tokens_and_mentions(self):
        """ Synchronizes the references to each others between the Mention instances and the Quotation 
        instances currently set a values of the Document instance.
        For each quotation, assign its 'mentions', 'speaker_mention', 'speaker_verb_token' and 
        'interlocutor_mention' their correct values (based on their '_extent' counterparts for the 
        latter) if possible.
        """
        quotations = self._document.quotations # Assume lexicographical order
        mentions = self._document.mentions # Assume lexicographical order
        extent2mention = self._document.extent2mention
        extent2token = self._document.extent2token
        if quotations is None:
            raise ValueError("No quotation have been defined yet, cannot synchronize.")
        if mentions is None:
            raise ValueError("No quotation have been defined yet, cannot synchronize.")
        if extent2token is None:
            raise ValueError("No token have been defined yet, cannot synchronize.")
        
        # Synchronization of mentions for each quotation
        # Assume that there is not overlapping quotation, or quotations included within another
        i = j = 0
        mention = quotation = None
        quotation_mentions = []
        while i < len(mentions) and j < len(quotations):
            if mention is None:
                mention = mentions[i]
                mstart,mend = mention.extent
            if quotation is None:
                quotation = quotations[j]
                qstart,qend = quotation.extent
            if mstart < qstart: # If the mention begins before the current quotation, it cannot be a part of it, preparing to pass on to the next mention if it exists
                i += 1
                mention = None
            elif qstart <= mstart and mend <= qend: # If the mention indeed belongs to the quotation
                quotation_mentions.append(mention)
                i += 1
                mention = None
            elif qend < mend: # If the quotation ends after the current mention, it cannot contain it, preparing to pass on to the next quotation if it exists
                j += 1
                quotation.mentions = quotation_mentions
                quotation_mentions = []
                quotation = None
        if quotation is not None:
            quotation.mentions = quotation_mentions
            quotation_mentions = []
            quotation = None
        
        # Synchronization of speaker and interlocutor mentions, and of speaker verb token, if possible
        for quotation in quotations:
            quotation.speaker_mention = extent2mention.get(quotation.speaker_mention_extent)
            quotation.speaker_verb_token = extent2mention.get(quotation.speaker_verb_token_extent)
            quotation.interlocutor_mention = extent2mention.get(quotation.interlocutor_mention_extent)
    
    def _inner_eq(self, other):
        """ Tests that this class instance is equal to the input object.
        
        That means 
        that:
            - this instance's class is equal or a subclass of the other instance's class
            - this instance's '_document' attribute value is the same python reference than that of "other's".
            - all the attributes pertaining to the caching of programmed actions have values that are 
              respectively equal between this instance, and 'other'
        
        Args:
            other: the instance of this class for which to test for equality

        Returns:
            a (boolean, string or None) pair; the second item of the pair will be None if the 
            boolean value is True (all attribute values were found to be equal), or will be the name of 
            the first attribute for which the values were found to not be equal, or 'Document instance id' 
            if the object were found not to be equal because the python object representing the document 
            to be edited is not the same
        
        Raises:
            TypeError: if the class of the input object is not that of a DocumentBuffer
        """
        if not isinstance(other, DocumentBuffer):
            message = "Incorrect type '{}', only another '{}' instance is accepted."
            message = message.format(type(other), DocumentBuffer)
            raise TypeError(message)
        if self._document is not other._document:
            return False, "Document instance id"
        attribute_names = _INNER_EQ_ATTRIBUTE_NAMES
        for name in attribute_names:
            if not getattr(self, name) == getattr(other, name):
                return False, name
        return True, None
    
    def __eq__(self, other):
        result, _ = self._inner_eq(other)
        return result
    
    def __ne__(self, other):
        return not self == other
    
    def __hash__(self):
        return NotImplemented



def deepcopy_document(document, copy_document_ident=None, strict=False):
    """ Creates a deepcopy of the input document
    
    Args:
        document: Document instance
        copy_document_ident: string (should respect convention to be a Document instance ident 
            attribute value) or None; if None, the deepcopy Document instance we have an ident whose value 
            is a variation of the input Document instance's.
        strict: boolean, whether or not to raise an exception when trying to synchronize the head 
            tokens of a Mention when the Mention does not possess head extent information.

    Returns:
        a Document instance whose attributes are all equal to those of the input's, while being 
        entirely new python objects
    """
    if copy_document_ident is None:
        copy_document_ident = "{}_copy".format(document.ident)
    copy_document_info = deepcopy(document.info)
    
    copy_document = Document(copy_document_ident, document.raw_text, info=copy_document_info)
    document_buffer = DocumentBuffer(copy_document)
    
    for markable_class in _MARKABLE_CLASSES:
        markable_plr_name = markable_class._NAME["plr"]
        # Get markable data currently possessed by the Document instance to be copied
        current_markables = getattr(document, markable_plr_name)
        if current_markables is not None:
            markable_extent2data = {markable.extent: markable.to_dict() for markable in current_markables}
            # Add the markable data to the buffer
            add_markable_data_method_name = "add_{}".format(markable_plr_name)
            add_markable_data_method = getattr(document_buffer, add_markable_data_method_name)
            add_markable_data_method(markable_extent2data)
    
    current_coreference_partition = document.coreference_partition
    if current_coreference_partition is not None:
        document_buffer.set_coreference_partition(current_coreference_partition.copy())
    
    # Since we only copied data, there should be consistency between the mention data and the coreference partition data, 
    # so we ask not to silently enforce consistency, but instead to raise an exception if there is an inconsistency of some sort
    copy_document = document_buffer.flush(strict=strict, enforce_coreference_partition_consistency=False)
    
    return copy_document 



def remove_mentions_and_coreference_partition_info(document, inplace=False):
    """ Removes all mentions and coreference partition data from the input document.
    
    Args:
        document: a Document instance
        inplace: boolean; if False, create a (deep)copy of the input document before removing its 
            mention and coreference partition data; in which case it is the copy that will be returned; if 
            True, remove the data from the input Document instance

    Returns:
        a Document instance corresponding to the input Document instance (or a deepcopy), whose 
        mentions and coreference partition data were removed
    """
    if not inplace:
        copy_document_ident = "{}_wiped_mentions_and_coreference_partition".format(document.ident)
        document = deepcopy_document(document, copy_document_ident=copy_document_ident)
    
    # Create edition structure, and use it to wipe the document clean as far as mentions are concerned
    document_buffer = DocumentBuffer(document)
    if document.mentions:
        document_buffer.remove_mentions(tuple(m.extent for m in document.mentions))
    else:
        document.mentions = tuple()
    document_buffer.remove_coreference_partition()
    document_buffer.flush(strict=True)
    
    return document
    

def transfer_coreference_partition_info(source_document, destination_document):
    """ Makes it so that the destination Document instance possess a coreference partition, based on 
    the mentions that it and the source document have in common, and on the coreference partition 
    possessed by the source Document.
    
    If the destination Document possesses no mention info, or if the source Document possesses no 
    coreference info, then the destination Document won't either (its "coreference_partition" 
    attribute value will be set to None).
    In any case, the coreference partition info potentially possessed by the destination Document 
    prior to the call to this function will be discarded.
    Assuming the source Document instance indeed possesses a CoreferencePartition instance for its 
    'coreference_partition' attribute value, a CoreferencePartition instance will be created and 
    associated to the destination Document instance.
    If a mention exist in the destination Document but not in the source Document, a singleton will 
    be created for it in the new destination document's coreference partition. 

    Warning:
        The CoreferencePartition instances deal only with extent, but even two identical 
        extents may refer to different things if the 'raw_text' attribute of the Document instance are 
        different. This function will not check that those are identical; the Document instances are 
        assumed to be copy of one another, w.r.t. to their 'raw_text' attribute value.
    """
    destination_document.coreference_partition = None
    if source_document.coreference_partition is None or destination_document.mentions is None:
        return
    
    mention_extents = (m.extent for m in destination_document.mentions)
    dest_extent2mention = destination_document.extent2mention
    new_coreference_partition = CoreferencePartition(mention_extents=mention_extents)
    for entity in source_document.coreference_partition:
        common_mention_extents = tuple(extent for extent in entity if extent in dest_extent2mention)
        if len(common_mention_extents) > 1:
            for extent1, extent2 in zip(common_mention_extents[:-1], common_mention_extents[1:]):
                new_coreference_partition.join(extent1, extent2)
    
    destination_document.coreference_partition = new_coreference_partition


