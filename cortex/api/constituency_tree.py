# -*- coding: utf-8 -*-

"""
Defines classes used to represent a constituency tree to be associated to a group of tokens 
(such as a sentence).
"""

__all__ = ["ConstituencyTreeNode", 
           "ConstituencyTree",
           "ConstituencyTreeNodeCONLL2012StringRepresentationBuilder",
           "InfiniteRecursionChecker",
           "CircularDependencyError",
           "IncorrectTreeLineInputError", 
           "IncorrectConstituencyTreeStringRepresentationError", 
           ]

import itertools
import re

from cortex.parameters import LRB_cortex_symbol, RRB_cortex_symbol, ELLIPSIS_cortex_symbol
from cortex.utils import EqualityMixIn, CoRTeXException


class ConstituencyTreeNode(EqualityMixIn):
    """ A class representing a node of a constituency tree.
    
    Is designed to be able to be associated to a Token instance and a syntactic tree tag, and can 
    provide access to parent node and children nodes.
    
    For a given node, an 'ancestor' is another node that can be reached from the given node by 
    recursively iterating over the 'parent' attribute, starting from the given node.
    Likewise, a 'descendant' is another node that can be reached from the given node by recursively 
    exploring its child nodes, the child nodes of its child nodes, etc...
    
    A node is considered to be a 'leaf', relative to its ancestor node(s), if it possesses no child 
    node.
    
    The 'leaves' of a node is the collection of this node's descendant node(s) that possess(es) no 
    child node.
    It is ordered as such: first the leaves associated to the first child (or the collection made 
    of only the first child if the first child is a leaf), then those of the second child (ditto), 
    etc...
    
    The depth of a node is the maximum number of generation that can be observed among the descendants 
    of this node .
    
    Arguments:
        syntactic_tag: a string, syntactic tag value to be associated to the node
        
        children: a collection of ConstituencyTreeNode instance, or None; the nodes that are supposed 
            to be children of this node
    
    Attributes:
        syntactic_tag: a string, syntactic tag associated to the node
        
        children: a collection of ConstituencyTreeNode instances, child nodes associated to the node
        
        token: a Token instance; or None; reference to the Token instance associated to this 
            node if this node represents a leaf of a constituency tree associated to a sentence
        
        parent: a ConstituencyTreeNode instance; or None; reference to the node whom this node 
            is a child node, if such a parent node exists
        
        extent: the extent associated to this node, if it is a leaf and has been given a 
            reference to a Token instance
        
        start: int, the position where the word corresponding to this node begins, if it is a leaf 
            and has been given a reference to a Token instance, within the raw text of the Document 
            instance that the Token instance belongs to
        
        end: int, the position where the word corresponding to this node ends, if it is a leaf and 
            has been given a reference to a Token instance, within the raw text of the Document 
            instance that the Token instance belongs to
        
        raw_extent: the extent built from considering the start of the first leaf associated to this 
            node, and the end of the last leaf associated to this node. If it is itself a leaf, this 
            extent will be equal to the value of the 'extent' attribute. Assumes that its leaves, or 
            itself if it is a leaf, were (was) synchronized with the respective token(s).
    """
    
    # We assume that two node are equal if their specific attributes value and respective subtree are equal
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = ("syntactic_tag", "children", "token")
    
    def __init__(self, syntactic_tag, children=None):
        self.syntactic_tag = syntactic_tag
        self.children = children
        self.token = None # A leaf is bound to a token
        self._flag = False
        # Reference to encompassing structure
        self.parent = None
    
    # Properties
    @property
    def children(self):
        return self._children
    @children.setter
    def children(self, value):
        self._children = tuple(value if value else [])
    
    @property
    def extent(self):
        #""" Assumes that the ConstituencyTreeNode has been synchronized with a Token instance. """
        if self.token is None:
            return None#raise ValueError("Does not have an 'extent': 'token' attribute is not defined.")
        return self.token.extent
    @extent.setter
    def extent(self, value):
        raise AttributeError
    
    @property
    def start(self):
        #""" Assumes that the ConstituencyTreeNode has been synchronized with a Token instance. """
        return self.extent[0]
    @start.setter
    def start(self, value):
        raise AttributeError
    
    @property
    def end(self):
        #""" Assumes that the ConstituencyTreeNode has been synchronized with a Token instance. """
        return self.extent[1]
    @end.setter
    def end(self, value):
        raise AttributeError
    
    @property
    def raw_extent(self):
        #""" Assumes that the ConstituencyTreeNode has been synchronized with a Token instance. """
        (left, right) = self.get_leaves_boundaries()
        raw_left = left.start
        raw_right = right.end
        return (raw_left, raw_right)
    @raw_extent.setter
    def raw_extent(self, value):
        raise AttributeError
    
    # Instance methods
    def is_leaf(self):
        """ Returns whether or not this node is a leaf.
        
        Returns:
            boolean
        """
        return not bool(self.children)
    
    def get_depth(self):
        """ Returns maximum number of edges / levels between itself and its youngest child
        
        Returns:
            non-negative integer
        
        Raises:
            CircularDependencyError: if a circular dependency is detected while recursively computing 
                the value to output
        """
        checker = InfiniteRecursionChecker("Circular dependency using the attribute 'children'.")
        return self._get_depth(checker)
    
    def get_leaves(self):
        """ Returns the collection of nodes corresponding to the leaves of this node.  
        The parsing of the tree structure corresponding to this node's descendants is done in a 
        breadth-first manner.
        If this node is a leaf, the result will contain only this node.
        
        Returns:
            a ConstituencyTreeNode instance(s) collection
        
        Raises:
            CircularDependencyError: if a circular dependency is detected while recursively computing 
                the value to output
        """
        checker = InfiniteRecursionChecker("Circular dependency using the attribute 'children'.")
        return tuple(self._get_leaves_iterable(checker))
    
    def get_leaves_raw_text(self):
        """ Gets a string representing the concatenation of all the words respectively associated to 
        the leaves of this node.
        
        Returns:
            string
        
        Raises:
            CircularDependencyError: if a circular dependency is detected while recursively computing 
                the value to output
        """
        return " ".join(leaf.syntactic_tag for leaf in self.get_leaves())
    
    def get_string_representation(self): # FIXME: risk of infinite recursion?
        """ Gets a string representing this node and the potential subtree made out of its children.
        
        Returns:
            string
        """
        return ConstituencyTreeNodeCONLL2012StringRepresentationBuilder.convert_constituency_tree_node_to_string_representation(self)
        
    def get_leftmost_leaf(self):
        """ Returns the first leaf of the leaves associated to this node.
        If this node is itself a leaf, return this node.
        
        Returns:
            a ConstituencyTreeNode instance
        """
        return self._get_extremity_leaf(right=False)
    
    def get_rightmost_leaf(self):
        """ Returns the last leaf of the leaves associated to this node.
        If this node is itself a leaf, return this node.
        
        Returns:
            a ConstituencyTreeNode instance
        """
        return self._get_extremity_leaf(right=True)
    
    # FIXME: rename that, so as to tell more about what is means, and less about what it technically is / how we build it
    def get_leaves_boundaries(self):
        """ Returns a pair of nodes corresponding respectively to the leftmost and rightmost leaf 
        nodes.
        If this node is itself a leaf, the output will be a pair of two references to this node.
        
        Returns:
            a (ConstituencyTreeNode instance, ConstituencyTreeNode instance) pair
        """
        return (self.get_leftmost_leaf(), self.get_rightmost_leaf())

    def get_raw_text_extent(self):
        """Return the extent which allows to create, from a document's raw text, the raw text 
        corresponding to the content of this node.
        Assume that the ConstituencyTreeNodes leaves have been synchronized with their respective 
        token, token associated to the aforementioned document.
        
        Returns:
            a string 
        """
        (left_node, right_node) = self.get_leaves_boundaries()
        return (left_node.start, right_node.end)

    def get_youngest_common_ancestor(self, other):
        """ Returns the youngest common ancestor of this node and the input node.
        
        If one input is an ascendant to the other, it will be returned.
        If the inputs do not belong to the same tree structure, 'None' will be returned
        
        Args:
            other: ConstituencyTreeNode instance
        
        Returns:
            a ConstituencyTreeNode instance; or None
        """
        # FIXME: with this implementation, two nodes belonging to two different trees could be linked through the 'root' None node...
        node = self
        with InfiniteRecursionChecker("Circular dependency using the attribute 'parent'.") as checker:
            while node is not None:
                checker.check(node)
                node._flag = True # Mark as visited
                node = node.parent
        
        node = other
        with InfiniteRecursionChecker("Circular dependency using the attribute 'parent'.") as checker:
            while node is not None and not node._flag:
                checker.check(node)
                node = node.parent
        common_node = node # Is equal to None (corresponds to the root node of a Tree) if the while broke because of the first condition, so all is good
        
        # Mark the ancestors as unvisited
        node = self
        while node is not None:
            node._flag = False # Mark as unvisited
            node = node.parent
        return common_node
    
    def get_youngest_tagged_ancestor(self, syntactic_tags):
        """ Returns youngest ancestor of this node (potentially itself) whose syntactic_tag belongs 
        to input set of tags.
        
        Args:
            syntactic_tags: set of strings
        
        Returns:
            a ConstituencyTreeNode instance
        
        Raises:
            CircularDependencyError: if a circular dependency is detected while recursively computing 
                the value to output
        """
        node = self
        with InfiniteRecursionChecker("Circular dependency using the attribute 'parent'.") as checker:
            while node is not None and node.syntactic_tag not in syntactic_tags:
                checker.check(node)
                node = node.parent
        return node
    
    def get_path_to(self, other):
        """ Returns the collection of nodes corresponding to the path going up from this node 
        (included) to the common node, and then down to the input node (included).
        
        If self is other, then output = [self]
        If common_ancestor is None, then output = None
        If self is common_ancestor, then output = [common_ancestor, ..., ..., other]
        If other is common_ancestor, then output = [self, ..., ..., common_ancestor]
        
        Args:
            other: a ConstituencyTreeNode instance
        
        Returns:
            a collection of ConstituencyTreeNode instances, or None
        
        Raises:
            CircularDependencyError: if a circular dependency is detected while recursively computing 
                the value to output
        """
        # Path from 'self' to 'other', including them    
        if self is other:
            return [self]
        
        common_ancestor = self.get_youngest_common_ancestor(other)
        if common_ancestor is None:
            return None
        
        path = []
        # We add every node before the common node, except if starting node == common node, since we will add the later just after
        if self is not common_ancestor:
            node = self
            while node is not common_ancestor:
                path.append(node)
                node = node.parent
        # We add the common node
        path.append(common_ancestor)
        # We add all node down to the destination, except if the destination is the common node, since, in that case, we already added it
        if other is not common_ancestor:
            l = []
            node = other
            while node is not common_ancestor:
                l.append(node)
                node = node.parent
            l.reverse()
            path.extend(l)
        return path

    def get_c_command_path(self, other):
        """ Returns the path to the input node, only if the common ancestor is not the input, self or 
        None; in which case return None.
        
        Args:
            other: ConstituencyTreeNode instance
        
        Returns:
            a collection of ConstituencyTreeNode instance
        
        Raises:
            CircularDependencyError: if a circular dependency is detected while recursively computing 
                the value to output
        """
        common_ancestor = self.get_youngest_common_ancestor(other)
        if common_ancestor is self or common_ancestor is other or common_ancestor is None:
            return None
        return self.get_path_to(other)

    def get_self_depth(self):
        """ Returns the number of ancestor / of ascending edge(s) before the None ancestor is reached.
        
        Returns:
            a non negative integer
        
        Raises:
            CircularDependencyError: if a circular dependency is detected while recursively computing 
                the value to output
        """
        d = 0
        node = self
        with InfiniteRecursionChecker("Circular dependency using the attribute 'parent'.") as checker:
            while node.parent is not None:
                checker.check(node)
                node = node.parent
                d += 1
        return d
    
    def count_type_to_root(self, syntactic_tags):
        """ Returns number of ancestors whose syntactif_tag belongs to the input set of tags.
        
        Args:
            syntactic_tag: set of strings
        
        Returns:
            a non-negative integer
        
        Raises:
            CircularDependencyError: if a circular dependency is detected while recursively computing 
                the value to output
        """
        count = 0
        node = self
        with InfiniteRecursionChecker("Circular dependency using the attribute 'parent'.") as checker:
            while node is not None and node.parent is not None:
                checker.check(node)
                node = node.parent.get_youngest_tagged_ancestor(syntactic_tags)
                if node is not None:
                    count += 1
        return count

    def get_ancestor_distance(self, other):
        """ Returns the distance between self and input node if input node is an ancestor to self; 
        else returns -1.
        
        Args:
            other: a ConstituencyTreeNode instance
        
        Returns:
            an integer
        
        Raises:
            CircularDependencyError: if a circular dependency is detected while recursively computing 
                the value to output
        """
        #if not isinstance(other, ConstituencyTreeNode) and other is not None:
        #    raise TypeError("Wrong input type ({}), must be a ConstituencyTreeNode instance.".format(type(other)))
        d = 0
        node = self
        with InfiniteRecursionChecker("Circular dependency using the attribute 'parent'.") as checker:
            while node is not None and node is not other: 
                checker.check(node)
                node = node.parent
                d += 1
        if node is None:
            return -1
        return d

    def get_tagged_nodes(self, tags):
        """ Returns the collection of nodes, starting from self and then carrying a breadth-first search 
        on its children, whose syntactic tag belongs to the input syntactic tags set-like.
        
        Args:
            tags: set-like object, or None; if None, all nodes will be returned
        
        Returns:
            a collection of ConstituencyTreeNode instances
        
        Raises:
            CircularDependencyError: if a circular dependency is detected while recursively computing 
                the value to output
        """
        # Breadth-first search
        found = []
        
        cond_fct = lambda x: True
        if tags is not None:
            cond_fct = lambda x: x.syntactic_tag in tags
        
        remaining = [self]
        with InfiniteRecursionChecker("Circular dependency using the attribute 'children'.") as checker:
            while remaining:
                current = remaining.pop(0)
                checker.check(current)
                if cond_fct(current):
                    found.append(current)
                remaining.extend(current.children)
        
        return found

    def get_all_nodes(self):
        """Returns collection of nodes, starting from self and then carrying a breadth-first search 
        on its children.
        
        Returns:
            a collection of ConstituencyTreeNode instances
        
        Raises:
            CircularDependencyError: if a circular dependency is detected while recursively computing 
                the value to output
        """
        return self.get_tagged_nodes(None)
    
    def _get_depth(self, checker):
        """ Computes the depth of this node, computed as the maximum depth value of its children, plus 
        one. If the node is a leaf, then the output value is equal to zero.
        Is a recursive function. Rely on an input circular dependency checker object to check to the 
        process does not parse the same node twice. 
        
        Args:
            check: a CircularDependancyChecker instance
        
        Returns:
            a non-negative integer
        
        Raises:
            CircularDependencyError: if a circular dependency is detected while recursively computing 
                the value to output
        """
        checker.check(self)
        if not self.children:
            return 0
        return max(node._get_depth(checker) for node in self.children) + 1
    
    def _get_leaves_iterable(self, checker):
        """ Returns an iterable over the nodes corresponding to the leaves of this node.  
        The parsing of the tree structure corresponding to this node's descendants is done in a 
        breadth-first manner.
        If this node is a leaf, the result will contain only this node.
        Is a recursive function. Rely on an input circular dependency checker object to check to the 
        process does not parse the same node twice.
        
        Args:
            check: a CircularDependancyChecker instance
        
        Returns:
            an iterable over ConstituencyTreeNode instance(s)
        
        Raises:
            CircularDependencyError: if a circular dependency is detected while recursively computing 
                the value to output
        """
        checker.check(self)
        if self.children:
            return itertools.chain.from_iterable(n._get_leaves_iterable(checker) for n in self.children)
        return (self,)
    
    def _get_extremity_leaf(self, right=True):
        """ Returns either the first, or the last, leaf of the leaves associated to this node.
        If this node is itself a leaf, return this node.
        
        Returns:
            a ConstituencyTreeNode instance
        
        Raises:
            CircularDependencyError: if a circular dependency is detected while recursively computing 
                the value to output
        """
        index = -int(right)
        node = self
        with InfiniteRecursionChecker("Circular dependency using the attribute 'children'.") as checker:
            while node.children:
                checker.check(node)
                node = node.children[index]
        return node
    
    def __repr__(self):
        return "ConstituencyTreeNode(syntactic_tag = '{}', {} child(ren))".format(self.syntactic_tag, len(self.children))
    
    # Static methods
    @staticmethod
    def get_nodes_youngest_common_ancestor(nodes):
        """ Returns the youngest common ancestor of several nodes.
        If the input collection contains only one node, then it will be the output result.
        
        Returns:
            a ConstituencyTreeNode, or None
        """
        l = len(nodes)
        if l == 0:
            raise ValueError("Empty nodes collection.")
        
        if l == 1:
            ancestor_node = nodes[0]
        else:
            ancestor_node = nodes[0]
            for node in nodes[1:]:
                ancestor_node = ancestor_node.get_youngest_common_ancestor(node)
        return ancestor_node



class ConstituencyTree(object):
    """ This class represents a ConstituencyTree, characterized by its root ConstituencyTreeNode 
    instance.
    It is mostly used as a wrapper around the methods defined for a ConstituencyTreeNode, but with 
    the knowledge that the root node has no parent.
    It is assumed that the ConstituencyTreeNode instances that make up this tree will not be further 
    modified, so that derived values, such as the leaves of the tree, may be considered constant 
    properties of the tree.
    
    Arguments:
        root: the ConstituencyTreeNode representing the root of the tree; that is to say, the 
            oldest ancestor of all its descendants; itself having no parent node
    
    Attributes:
        root: the ConstituencyTreeNode representing the root of the tree; that is to say, the 
            oldest ancestor of all its descendants; itself having no parent node
        
        leaves: the collection of ConstituencyTreeNode instance corresponding to the leaves of the 
            root node
    
        leaves_raw_text: string representing the concatenation of all the words respectively associated to 
            the leaves of this tree
        
        depth: the maximum number of vertical edges, starting from the root node of the tree, 
            minus 1 if the root node of the tree has only one child (to account for cases such as 
            "(TOP (S (NP (PRP$ Its) (NN president)) (VP (VBZ is) (NP (NNP Zheng) (NNP Bing))) (. .)))", 
            where one could argue that, from a sentence structure point of view, the correct value should 
            not take into account the 'TOP -> S' link).
        
        string_representation: the string representation of the tree, corresponding to the string 
            representation of its root node
    
    One easy way to create a ConstituencyTree instance is from its string representation:
    
    Examples:
        >>> constituency_tree = ConstituencyTree.parse(string_representation)
    """
    
    def __init__(self, root):
        if not isinstance(root, ConstituencyTreeNode):
            raise TypeError("Incorrect type for 'root' input ('{}'), only a '{}' instance is "\
                            "accepted.".format(type(root), ConstituencyTreeNode))
        self.root = root
        self.__leaves = None
        self.__leaves_raw_text = None
        self.__depth = None
        self.__string_representation = None
    
    @property
    def leaves(self):
        if self.__leaves is None:
            self.__leaves = self.root.get_leaves()
        return self.__leaves
    @leaves.setter
    def leaves(self, value):
        raise AttributeError
    
    @property
    def leaves_raw_text(self):
        if self.__leaves_raw_text is None:
            self.__leaves_raw_text = " ".join(leaf.syntactic_tag for leaf in self.leaves)
        return self.__leaves_raw_text
    @leaves_raw_text.setter
    def leaves_raw_text(self, value):
        raise AttributeError
    
    @property
    def depth(self):
        if self.__depth is None:
            root_depth = self.root.get_depth()
            root_depth = root_depth-1 if (self.root.children and len(self.root.children) == 1) else root_depth 
            self.__depth = root_depth
        return self.__depth
    @depth.setter
    def depth(self, value):
        raise AttributeError
    
    @property
    def string_representation(self):
        if self.__string_representation is None:
            self.__string_representation = ConstituencyTreeNodeCONLL2012StringRepresentationBuilder.convert_constituency_tree_node_to_string_representation(self.root)#self.root.get_string_representation()
        return self.__string_representation
    @string_representation.setter
    def string_representation(self, value):
        raise AttributeError
    
    # Instance methods
    def get_tagged_nodes(self, tags):
        """ Returns the collection of nodes, starting from the root and then carrying a breadth-first search 
        on its children, whose syntactic tag belongs to the input syntactic tags set-like.
        
        Args:
            tags: set-like object, or None; if None, all nodes will be returned
       
        Returns:
            a collection of ConstituencyTreeNode instances
        
        Raises:
            CircularDependencyError: if a circular dependency is detected while recursively computing 
                the value to output
        """
        return self.root.get_tagged_nodes(tags)
    
    def get_all_nodes(self):
        """ Returns collection of nodes, starting from the root and then carrying a breadth-first search 
        on its children.
        
        Returns:
            a collection of ConstituencyTreeNode instances
        
        Raises:
            CircularDependencyError: if a circular dependency is detected while recursively computing 
                the value to output
        """
        return self.root.get_all_nodes()
    
    def _inner_eq(self, other):
        """ Tests that this class instance is equal to the input object.
        
        That means 
        that:
            - this instance's class is equal or a subclass of the other instance's class
            - the respective string representation of each instance is equal
        
        Args:
            other: the instance of this class for which to test for equality
        
        Returns:
            a (boolean, string or None) pair; the second item of the pair will be None if the 
            boolean value is True, or will be equal to "string_representation" if the corresponding values 
            were found to not be equal.
        
        Raises:
            TypeError: if the class of the input object is not that of a ConstituencyTree
        """
        if not isinstance(other, ConstituencyTree):
            message = "Incorrect type '{}', only another '{}' instance is accepted."
            message = message.format(type(other), ConstituencyTree)
            raise TypeError(message)
        if not self.string_representation == other.string_representation:
            return False, "string_representation"
        return True, None
    
    def __eq__(self, other):
        value, _ = self._inner_eq(other)
        return value
    
    def __ne__(self, other):
        return not self == other
    
    def __hash__(self):
        raise NotImplemented   
    
    # Class methods
    @classmethod
    def parse(cls, string_representation):
        """ Parse the input string, and return the corresponding ConstituencyTree instance.
        
        Args:
            string_representation: a string representing one sentence as a constituency tree
        
        Returns:
            a ConstituencyTree instance
        """
        return ConstituencyTreeNodeCONLL2012StringRepresentationBuilder.parse_tree(string_representation)



class ConstituencyTreeNodeCONLL2012StringRepresentationBuilder(object):
    """ Class used to convert, from the (potentially incrementally built) string representation 
    of a constituency tree node, to the corresponding ConstituencyTreeNode instance.
    
    It is useful for centralizing the management of edges cases, that is, cases where the text of a 
    word contains parentheses, since those are used to represent, within the string representation 
    of the tree, the separation between the nodes.
    
    Examples:
        >>> builder = ConstituencyTreeNodeCONLL2012StringRepresentationBuilder()
        >>> for tag, POS_tag_and_leaf_word_pairs in input_process():
        >>>     builder.add_tag(tag, POS_tag_and_leaf_word_pairs=POS_tag_and_leaf_word_pairs)
        >>> string_representation = builder.flush()
        
        or 
        
        >>> constituency_tree = ConstituencyTreeNodeCONLL2012StringRepresentationBuilder.parse_tree(string_representation)
        
        or
        
        >>> constituency_tree_node = ConstituencyTreeNodeCONLL2012StringRepresentationBuilder.parse_node(string_representation)
    """
    
    def __init__(self):
        self.initialize()
    
    def initialize(self):
        """ Prepares the builder instance for the incremental building of a constituency tree string 
        representation.
        """
        self._tags = []
    
    def add_tag(self, tag, POS_tag_and_leaf_word_pairs=None):
        """ Continues the incremental building of the string representation of a constituency tree.
        
        Args:
            tag: string, part of a constituency tree representation, representing a node or group of 
                nodes whose respective syntactic level is above that of a token (so, either 'phrasal' 
                level or 'clause' level)
            POS_tag_and_leaf_word_pairs: a pair a strings corresponding respectively to the 
                POS tag of a token (so, a final node before a leaf, a 'word' syntactic level), and 
                the textual representation of the token itself (which will be a leaf)
        
        Examples:
            When iterating over the following CONLL2012 formatted lines::
            
                ident    0    1    Thermes    NC    (ROOT(SENT(NP*    *    *    *    *    -    -  
                ident    0    2    de    P    (PP*    *    *    *    *    -    -  
                ident    0    3    Caracalla    NPP    (NP*)))))    *    *    *    *    -    - 
            
            the corresponding input(s) of this method for each line could the following::
            
                [["(ROOT(SENT(NP*", ["NC", "Thermes"]],  
                 ["(PP*", ["P", "de"]],  
                 ["(NP*)))))", ["NPP", "Caracalla"]],  
                ]
            
            or the following::
            
                [["(ROOT", None],  
                 ["(SENT", None],  
                 ["(NP*", ["NC", "Thermes"]],  
                 ["(PP*", ["P", "de"]],  
                 ["(NP*)", ["NPP", "Caracalla"]],  
                 [")", None],  
                 [")", None],  
                 [")", None],  
                 [")", None],  
                ]
        
        """
        if POS_tag_and_leaf_word_pairs is not None:
            POS_tag, word = POS_tag_and_leaf_word_pairs
            leaf_tag = self._convert_word_to_leaf_tag(word)
            pattern = r"\*"
            repl_string = "({} {})".format(POS_tag, leaf_tag)
            input_string = tag.replace(" ", "")
            tag = re.sub(pattern, repl_string, input_string)
        self._tags.append(tag)
    
    def flush(self):
        """ Aggregates the incrementally added tag data to create the corresponding string 
        representation.
        The incrementally added data will be lost (<=> "the buffer will be emptied").
        
        Returns:
            a string, the aggregated constituency tree string representation
        """
        string_representation = "".join(self._tags)
        string_representation = re.sub(r"\(", " (", string_representation)[1:] # ConstituencyTree requires this specific format
        self.initialize()
        return string_representation
    
    # Class method(s)
    _WORD2LEAF_TAG2 = {"(": LRB_cortex_symbol, ")": RRB_cortex_symbol, "(...)": ELLIPSIS_cortex_symbol}
    _LEAF_TAG2WORD2 = {value: key for key, value in _WORD2LEAF_TAG2.items()}
    _WORD_TO_LEAF_TAG_PATTERN = "\(\.{3}\)|\(|\)"
    _LEAF_TAG_TO_WORD_PATTERN = "{}|{}|{}".format(LRB_cortex_symbol, RRB_cortex_symbol, ELLIPSIS_cortex_symbol)
    @classmethod
    def _repl_word_by_leaf_tag(cls, m):
        """ Function used as parameter to the 're.sub' function. Acts as the 'replacer' function, 
        used to replace a found occurrence of a string by another string value to replace it.
        Here, deals with replacing specific expressions that encode values that are forbidden in the 
        string representation of a constituency tree (such as parentheses), by those forbidden values.
        
        Args:
            m: a '_sre.SRE_Match' instance, for instance an output of a successful call to 're.search'
        
        Returns:
            a string
        """
        word = m.group(0)
        return cls._WORD2LEAF_TAG2.get(word)
    @classmethod
    def _repl_leaf_tag_by_word(cls, m):
        """ Function used as parameter to the 're.sub' function. Acts as the 'replacer' function, 
        used to replace a found occurrence of a string by another string value to replace it.
        Here, deals with replacing specific expressions, that are values that are forbidden in the 
        string representation of a constituency tree (such as parentheses), by expression that 
        respectively encode those forbidden values.
        
        Args:
            m: a '_sre.SRE_Match' instance, for instance an output of a successful call to 're.search'
        
        Returns:
            a string
        """
        leaf_tag = m.group(0)
        return cls._LEAF_TAG2WORD2.get(leaf_tag, leaf_tag)
    
    @classmethod
    def _convert_word_to_leaf_tag(cls, word):
        """ Encodes a word into a value suitable to be used to represent the word within the string 
        representation of a constituency tree, in which this word will constitute a leaf.
        Most of the time, the word itself is a suitable encoded value, but there are some values 
        (such as parentheses) that are forbidden, and should be encoded by another expression.
        
        Args:
            word: string
        
        Returns:
            a string
        """
        return re.sub(cls._WORD_TO_LEAF_TAG_PATTERN, cls._repl_word_by_leaf_tag, word)
    
    @classmethod
    def _convert_leaf_tag_to_word(cls, leaf_tag):
        """ Decodes a 'constituency tree string representation encoded word value' into the 
        corresponding word.
        Cf the '_convert_word_to_leaf_tag' method.
        
        Args:
            leaf_tag: string
        
        Returns:
            a string
        """
        return re.sub(cls._LEAF_TAG_TO_WORD_PATTERN, cls._repl_leaf_tag_by_word, leaf_tag)
    
    @classmethod
    def convert_constituency_tree_node_to_string_representation(cls, constituency_tree_node): # FIXME: risk of infinite recursion?
        """ Converts a ConstituencyTreeNode instance into its corresponding string representation.
        
        Args:
            constituency_tree_node: a ConstituencyTreeNode instance
        
        Returns:
            a string
        """
        if constituency_tree_node.is_leaf():
            return cls._convert_word_to_leaf_tag(constituency_tree_node.syntactic_tag)
        else:
            return "({} {})".format(constituency_tree_node.syntactic_tag, 
                                   " ".join(cls.convert_constituency_tree_node_to_string_representation(child)\
                                            for child in constituency_tree_node.children
                                            )
                                   )
    
    @classmethod
    def parse_node(cls, string_representation):
        """Parses the tree corresponding to the input string, and return its root node.
        
        Args:
            tree_line: a string representing one sentence as a constituency tree
        
        Returns:
            a ConstituencyTreeNode instance, the root node of the constituency tree corresponding to the 
                sentence
        """
        try:
            root = cls._parse_tree_line(string_representation, 0)[0]
        except IncorrectTreeLineInputError:
            msg = "Input string_representation = '{}'.".format(string_representation)
            raise IncorrectConstituencyTreeStringRepresentationError(msg) from None
        return root
    
    @classmethod
    def parse_tree(cls, string_representation):
        """ Parses the input string, and return the corresponding ConstituencyTree instance.
        
        Args:
            tree_line: a string representing one sentence as a constituency tree
        
        Returns:
            a ConstituencyTree instance, the constituency tree corresponding to the sentence
        """
        root = cls.parse_node(string_representation)
        return ConstituencyTree(root)
    
    @classmethod
    def _parse_tree_line(cls, tree_line, pos):
        """ Parses the constituency tree node corresponding to the first node met while parsing the 
        input 'tree_line'.
        Used to recursively parse a string representation so as to be able, in fine, to build the 
        corresponding ConstituencyTree instance.
        
        Args:
            tree_line: a string representing (maybe part of) the constituency tree of a sentence
            pos: integer, representing equilibrium between number of met opening parenthesis, 
                and closing parenthesis; a negative number indicates that we met that many more closing 
                parenthesis than opening one while parsing the string:
                    pos = 'number of met opening parentheses - number of met closing parentheses'.
        
        Returns:
            a (ConstituencyTreeNode instance, remaining tree_line, updated pos value) 3-uplet
        
        Examples:
            >>> tree_line = "(TOP (S (NP (PRP$ Its) (NN president)) (VP (VBZ is) (NP (NNP Zheng) (NNP Bing))) (. .)))"
        """
        try:
            first_element = tree_line[0]
        except IndexError:
            raise IncorrectTreeLineInputError()
        if first_element != "(": #leaf
            right_limit_index = tree_line.find(")")
            L = len(tree_line)
            leaf_tag = tree_line[0:right_limit_index]
            while True: # Count the number of closing parenthesis that we meet until the next node to parse
                pos -= 1
                right_limit_index += 1
                if right_limit_index >= L or tree_line[right_limit_index] != ")":
                    break
            if right_limit_index < L:
                tree_line = tree_line[right_limit_index+1:] # We forget about the beginning of the string, and the closing parenthesis met after parsing the leaf
            leaf_tag = cls._convert_leaf_tag_to_word(leaf_tag)
            leaf = ConstituencyTreeNode(leaf_tag)
            return (leaf, tree_line, pos)
        
        pos += 1 # We met an opening parenthesis, so we increment 'pos'
        next_space_pos = tree_line.find(" ")
        next_space_length = 1
        opening_parenthesis_length = 1
        syntactic_tag = tree_line[1: next_space_pos]
        tree_line = tree_line[opening_parenthesis_length+len(syntactic_tag)+next_space_length:] # We forget about the beginning of the string
        pos2 = 0
        children = []
        while pos > 0: # As long as the very first parenthesis has not been closed, parsing continues
            (node, tree_line, pos2) = cls._parse_tree_line(tree_line, pos2) # Recursion
            children.append(node)
            pos += pos2 # Update the equilibrium number with what has been encountered below in the tree.
        root = ConstituencyTreeNode(syntactic_tag, children)
        for child in root.children:
            child.parent = root
        return (root, tree_line, pos)




class InfiniteRecursionChecker(object):
    """ Class used to represent a set of object already seen during a recursive process, and to 
    raise an Exception if the same object is seen more than once.
    
    Arguments:
        exception_message: string, parameter to give to the exception raised if an object is 
            seen more than once during a recursive process
    
    Attributes:
        seen: the set of respective id values of python objets already checked
        
        exception_message: string, parameter to give to the exception raised if an object is 
            seen more than once during a recursive process
    
    
    Examples:
        To check whether or not a python object has already been seen:
        
        >>> infinite_recursion_checker.check(python_object)
        
        Can be used as a context manager, to clearly delimit the portion of code for which it is useful 
        to possess / to use such an object:
        
        >>> with InfiniteRecursionChecker() as checker:
        >>> ... ...
        >>> ... checker.check(item)
        >>> ... ...
        >>> ...
    """
    def __init__(self, exception_message):
        self.exception_message = exception_message
        self.seen = set()
    
    def __enter__(self):
        return self
    
    def check(self, value):
        """ Checks that the input python object has not already been checked / seen once.
        
        Args:
            value: a python object
        
        Raises:
            CircularDependencyError: if the input python object has already been seen once
        """
        id_ = id(value)
        if id_ in self.seen:
            raise CircularDependencyError(self.exception_message)
        self.seen.add(id_)
    
    def __exit__(self, *args, **kwargs):
        pass


# Exceptions
class CircularDependencyError(CoRTeXException):
    """ Exception to be raised when encountering twice an identical object during a recursive process. """
    pass

class IncorrectTreeLineInputError(CoRTeXException):
    """ Exception to be raised when encountering an incorrectly formatted string representation, 
    when decoding what is assumed to be the string representation of a constituency tree node into 
    the correspond node.
    """
    pass

class IncorrectConstituencyTreeStringRepresentationError(CoRTeXException):
    """ Exception to be raised when encountering an incorrectly formatted constituency tree node 
    string representation, when decoding such a string representation into the correspond node.
    """
    pass
