# -*- coding: utf-8 -*-

"""
Defines a class used to aggregate the structured data that together represent a document.
"""

__all__ = ["Document",
           "IncorrectExtentError",
           "DocumentDoesNotPossessMentionsError",
           "DocumentDoesNotPossessCoreferencePartitionError",
           ]

from collections import OrderedDict
from cityhash import CityHash128

from cortex.utils import EqualityMixIn, CoRTeXException



class Document(EqualityMixIn):
    """ This class is used to store and represent the different kind of data available about a 
    text, such as the tokens it is made of, the dependency structure of its sentence, ... 
    and of course the mentions it can contain.
    
    It can also be assigned a CoreferencePartition object.
    
    Its attributes and properties should be considered as read-only.
    If one wants to create or modify a Document instance, one should use the dedicated DocumentBuffer 
    class.
    
    Arguments:
        ident: a string identifying the document; it should be used to be able to distinguish 
            this document from every other documents used in your context. When possible according the 
            persistence format this data will be persisted when the document is persisted somewhere.
    
        raw_text: the string containing a raw representation of the text. The respective 
            extent of the elements (such as Token) held by the Document class refers to this string.
            In order to get a text extract from this document using a given extent, one  should call the 
            'get_raw_text_extract' method.
    
        info: a python dictionary containing information pertaining to the document that the 
            user wants to be persisted (for future reference for instance), or None
    
    Attributes:
        ident: a string identifying the document; it should be used to be able to distinguish 
            this document from every other documents used in your context. When possible according the 
            persistence format this data will be persisted when the document is persisted somewhere.
    
        raw_text: the string containing a raw representation of the text. The respective 
            extent of the elements (such as Token) held by the Document class refers to this string.
            In order to get a text extract from this document using a given extent, one  should call the 
            'get_raw_text_extract' method.
    
        info: a python dictionary containing information pertaining to the document that the 
            user wants to be persisted (for future reference for instance)
        
        
        tokens: a collection of Token instances constituting the document; or None
        
        extent2token: a 'extent => Token instance' map of the tokens associated to the Document 
            instance; or None
    
    
        mentions: a collection of Mention instances corresponding to mention present in the 
            document; or None
        
        extent2mention: a 'extent => Mention instance' map of the mentions associated to the 
            Document instance; or None
        
        extent2mention_index: a 'extent => integer' map of the respective index (0 <=> first 
            mention found in the Document instance, 1 <=> second mention...; in reading order) of each 
            mention associated to the Document instance; or None
    
    
        named_entities: a collection of NamedEntity instances constituting the document; or None
        
        extent2NamedEntity: a 'extent => NamedEntity instance' map of the named_entities 
            associated to the Document instance; or None
    
        
        sentences: a collection of Sentence instances constituting the document; or None
        
        extent2sentence: a 'extent => Sentence instance' map of the sentences associated to the 
            Document instance; or None
        
        extent2sentence_index: a 'extent => integer' map of the respective index (0 <=> first 
            sentence found in the Document instance, 1 <=> second sentence...; in reading order) of each 
            sentence associated to the Document instance; or None
    
    
        quotations: a collection of Quotations instances corresponding to possible quotations 
            found in the document; or None
        
        extent2quotation: a 'extent => Quotation instance' map of the quotations associated to the 
            Document instance; or None
        
        extent2quotation_index: a 'extent => integer' map of the respective index (0 <=> first 
            quotation found in the Document instance, 1 <=> second quotation...; in reading order) of 
            each quotation associated to the Document instance; or None
    
        
        coreference_partition: a CoreferencePartition instance; or None; used to hold a partition 
            of mentions once one has been defined or predicted for this document
        
        base_hash_string: string, whose role is to distinctly identify this Document instance, w.r.t. 
            the values of the collections of Token instances, Mention instances, Sentence instances, etc... 
            associated to it
        
        hash_string: string, the result of hashing the value of the 'base_hash_string' attribute
    
    Regarding the order over which the elements of the attributes are iterated (sentences, 
    mentions...), it depends on the order of the objects that were used to set those attribute values.
    If the Document instance was created using a DocumentBuffer instance, then one can assume 
    that the order used is the reading order (see the 'markable' module for more info, for instance 
    regarding nested markables).
    """
    
    _ATTRIBUTE_NAMES_FOR_EQUALITY_COMPUTATION = ("raw_text", "info", "_tokens", "_mentions", 
                                                "_named_entities", "_sentences", "_quotations", 
                                                "coreference_partition")
    
    _OFFSET_INDEX = 0 # Offset value to use when extracting a substring from the raw text string using extent values
    # For now it is equal to 0, because it seems that all raw text begins with a '\n' character 
    
    def __init__(self, ident, raw_text, info=None):
        if not (isinstance(ident, str) and ident):
            message = "The input 'ident' parameter's value is incorrect, it must be a non-empty string."
            message = message.format(ident)
            raise ValueError(message)
        self.ident = ident
        self.raw_text = raw_text # A document is first and foremost considered as a string
        self.info = OrderedDict() if info is None else OrderedDict(info)
        
        self._tokens = None
        self.extent2token = None
        
        self._mentions = None
        self.extent2mention = None
        self.extent2mention_index = None
        
        self._named_entities = None
        self.extent2named_entity = None
        
        self._sentences = None
        self.extent2sentence = None
        self.extent2sentence_index = None
        
        self._quotations = None
        self.extent2quotation = None
        self.extent2quotation_index = None
        
        self.coreference_partition = None
    
    # Properties
    @property
    def tokens(self):
        return self._tokens
    @tokens.setter
    def tokens(self, value):
        value = tuple(value)
        self.extent2token = OrderedDict((m.extent, m) for m in value)
        self._tokens = value
    
    @property
    def mentions(self):
        return self._mentions
    @mentions.setter
    def mentions(self, value):
        value = tuple(value)
        self.extent2mention = OrderedDict((m.extent, m) for m in value)
        self.extent2mention_index = dict((mention.extent, i) for i, mention in enumerate(value))
        self._mentions = value
    
    @property
    def named_entities(self):
        return self._named_entities
    @named_entities.setter
    def named_entities(self, value):
        value = tuple(value)
        self.extent2named_entity = OrderedDict((m.extent, m) for m in value)
        self._named_entities = value
    
    @property
    def sentences(self):
        return self._sentences
    @sentences.setter
    def sentences(self, value):
        value = tuple(value)
        self.extent2sentence = OrderedDict((m.extent, m) for m in value)
        self.extent2sentence_index = dict((sentence.extent, i) for i, sentence in enumerate(value))
        self._sentences = value
    
    @property
    def quotations(self):
        return self._quotations
    @quotations.setter
    def quotations(self, value):
        value = tuple(value)
        self.extent2quotation = OrderedDict((m.extent, m) for m in value)
        self.extent2quotation_index = dict((quotation.extent, i) for i, quotation in enumerate(value))
        self._quotations = value
    
    @property
    def base_hash_string(self):
        """ Returns a string identifying the document and all of its associated markable collections 
        in a unique way.
        
        Returns:
            a string
        """
        #string = self.raw_text
        string = "\n\n".join(str(getattr(self, attribute_name)) for attribute_name in ("raw_text", "tokens", "sentences", "mentions", "named_entities", "quotations", "coreference_partition",))
        return string
    @base_hash_string.setter
    def base_hash_string(self, value):
        raise AttributeError
    
    @property
    def hash_string(self):
        """ Returns a string corresponding to the hash of the 'base_hash_string' attribute value.
        
        Returns:
            a string
        """
        return str(CityHash128(self.base_hash_string))
    @hash_string.setter
    def hash_string(self):
        raise AttributeError
    
    # Instance methods
    def get_raw_text_extract(self, extent):
        """ Returns a string corresponding to the raw text extract whose extent is input.
        
        Args:
            extent: a (start, end) pair, where 1 <= start <= end are integers
        """
        start, end = extent
        if end < start:
            msg = "Wrong extent '{}': end index should be superior or equal to start index"
            msg = msg.format(extent)
            raise IncorrectExtentError(msg)
        start_min_value = 0
        end_max_value = len(self.raw_text)-1
        if start < start_min_value or end > end_max_value:
            msg = "Wrong extent '{}': start index should be superior or equal to {}, and end index should be inferior or equal to {}."
            msg = msg.format(extent, start_min_value, end_max_value)
            raise IncorrectExtentError(msg)
        extent_raw_text = self.raw_text[start-self._OFFSET_INDEX:end-self._OFFSET_INDEX+1]
        return extent_raw_text

    def simple_equal(self, other):
        """ Compares this document with another by only testing whether their respective 'raw_text' 
        attribute values are equal.
        
        Returns:
            a boolean
        """
        if not isinstance(other, Document):
            msg = "Incorrect type '{}', only another Document instance is accepted."
            msg = msg.format(type(other))
            raise TypeError(msg)
        return self.raw_text == other.raw_text
    
    def __repr__(self):
        return "Document(ident='{}')".format(self.ident)
    
    def __str__(self):
        return repr(self)


# Exceptions
class IncorrectExtentError(CoRTeXException):
    """ Exception to be raised when trying to use an extent value on a string, in order to extract 
    a substring, but failing because the extent value is inadequate w.r.t. the string it is being 
    used on.
    """
    pass

class DocumentDoesNotPossessMentionsError(CoRTeXException):
    """ Exception to be raised when trying to carry out a process on a document, that assumes that 
    the document possesses mentions, when it fact it does not. 
    """
    pass

class DocumentDoesNotPossessCoreferencePartitionError(CoRTeXException):
    """ Exception to be raised when trying to carry out a process on a document, that assumes that 
    the document possesses a coreference partition, when it fact it does not. 
    """
    pass
