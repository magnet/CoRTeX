# -*- coding: utf-8 -*-

"""
Defines classes used for holding and representing structured data about textual documents.
"""

"""
Available submodules
---------------------
document
    Defines the class used to aggregate the structured data that together represent a document.

markable
    Defines the classes used to aggregate the data regarding one extract of a document's raw text 
    (token, mention, sentences...).

constituency_tree
    Defines the classes used to represent a constituency tree to be associated to a group of tokens 
    (such as a sentence).

entity
    Defines the class used to represent a set of mention referring to the same entity.

coreference_partition
    Defines the class used to represent a partition into entities over a universe of mention.

document_buffer
    Defines utilities used to aggregate data about, build, and manipulate document class instances.

corpus
    Defines a class used to represent a collection of documents, and utilities used to compute some 
    statistics over it.
"""

from .constituency_tree import ConstituencyTree, ConstituencyTreeNode
from .coreference_partition import CoreferencePartition
from .entity import Entity
from .markable import Token, NamedEntity, Sentence, Mention, Quotation
from .document import Document
from .document_buffer import (DocumentBuffer, deepcopy_document, transfer_coreference_partition_info, 
                              remove_mentions_and_coreference_partition_info,)
from .corpus import Corpus

from . import constituency_tree
from . import coreference_partition
from . import corpus
from . import document
from . import document_buffer
from . import entity
from . import markable