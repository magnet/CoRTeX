# -*- coding: utf-8 -*-

"""
Defines the class used to represent a partition into entities over a universe of mention.
"""

__all__ = ["CoreferencePartition",
           "MentionBelongsToTwoEntitiesError", 
           "MentionBelongsToTwoEntitiesCollectionError", 
           "ExtentNotInUniverseError", 
           ]

from cortex.parameters import ENCODING
from .entity import Entity
from cortex.utils import count_iterable_content, CoRTeXException


class CoreferencePartition(object):
    """ Class representing a partition over a universe of mention, representing by their respective extent
    That is, it allows to represent a set of non-overlapping Entity instances.
    
    An instance of this class provides an iterator over the Entities it defines.
    
    When instantiated, the coreference partition will first be populated from the input 'entities' 
    collection, if it is defined; if a mention extent exists in two distinct 'entities', an exception 
    will be raised. Then, if 'mention_extents' is defined, a singleton will be created for each of 
    the mention extent iterated over, unless it already exists (for instance, because it was present 
    in one of the input 'entities', in which case it will stay in this entity).
    
    Argument 
        entities: iterable over collections of mention extent pairs, or None
        
        mention_extents: iterable over mention extents; or None
    
    Attributes:
        universe_size: the number of distinct extent(s) constituting the universe of the partition
        
        entity_head_extents: a set of the respective head mention's extent of each of the 
            Entities defined in the partition
        
        singleton_extents: a set over the extents of all mentions of the universe which refer 
            to a Singleton entity
    
    Examples:
        
        One can test whether or not a mention's extent belongs to the partition's universe by: 
        
        >>> mention_extent in coreference_partition
        
        One can get the number of entities defined in the partition by:
        
        >>> len(coreference_partition)
        
        One can test whether or not two partitions are equal (define the same entities over the same 
        universe) by:
        
        >>> coreference_partition1 == coreference_partition2
        
        One can create a partition either from a known collection of iterables over mention extents (in 
        that case the check for non-overlapping entities will still occur), or create it empty, and 
        populate it in an iterative manner using the 'add' and 'join' methods.
        
        Finally, the entities definition can be saved to a file, and a CoreferencePartition can be 
        loaded back from such a file, using the 'save' and 'load' methods, respectively.
    """
    
    def __init__(self, entities=None, mention_extents=None):
        # A partition is a set of countable sets which are disjoint.
        # Countable means that we should be able to identify each set with its integer ident.
        
        # A partition object must be able to easily give out the different set of distinct items
        # Ideally, this would be represented by a set of set, but sets are not hashable, and thus cannot be put as item of sets
        # Damned. We'll have to settle for a list of sets
        # It must also be able to easily give out the set to which an item belongs if we specify this item
        mention_extent_to_entity = {}
        exceptions = []
        
        # First, add element contained in (pseudo-)entities definition; doing this step firt allows to control that the input (pseudo-)entities are indeed disjoint
        if entities is not None:
            for i, original_entity in enumerate(entities):
                ident = i
                if isinstance(original_entity, Entity):
                    ident = original_entity.ident
                entity = Entity(mention_extents=original_entity, ident=ident)
                for mention_extent in entity:
                    try:
                        existing_entity = mention_extent_to_entity[mention_extent]
                    except KeyError: # It didn't exist before, good, then we add it
                        mention_extent_to_entity[mention_extent] = entity
                    else: # Else we create an exception, because it should not exist if the different entities were disjoint
                        e = MentionBelongsToTwoEntitiesError(mention_extent, existing_entity.ident, entity.ident)
                        exceptions.append(e)
            if exceptions:
                raise MentionBelongsToTwoEntitiesCollectionError(exceptions)
        
        # Then, add elements that may not be present in a possibly input (pseudo-)entity; if that is the case it will be added as a singleton
        if mention_extents is not None:
            for mention_extent in mention_extents:
                if mention_extent not in mention_extent_to_entity:
                    mention_extent_to_entity[mention_extent] = Entity((mention_extent,))
        
        self._mention_extent_to_entity = mention_extent_to_entity
    
    @property
    def universe_size(self):
        return len(self._mention_extent_to_entity)
    @universe_size.setter
    def universe_size(self, value):
        raise AttributeError
    
    @property
    def entity_head_extents(self):
        return set(entity.head_mention_extent for entity in self)
    @entity_head_extents.setter
    def entity_head_extents(self, value):
        raise AttributeError
    
    @property
    def singleton_extents(self):
        return set(tuple(entity)[0] for entity in self if len(entity) == 1)
    @singleton_extents.setter
    def singleton_extents(self, value):
        raise AttributeError
    
    # Instance methods
    def get_universe(self):
        """ Returns a set containing all the mention extent contained in all the entities defined in 
        this partition.
        
        Returns:
            a {(start,end)} set
        """
        return set(self._mention_extent_to_entity)
    
    def get_mention_extent_to_entity_map(self):
        """ Returns a 'mention extent => entity this mention extent belongs to' map.
        
        Returns:
            a 'extent => Entity instance' map
        """
        return dict(self._mention_extent_to_entity)
    
    def remove_singletons(self):
        """ Returns a new CoreferencePartition were the singleton entities have been removed.
        
        Returns:
            a CoreferencePartition instance
        """
        return CoreferencePartition(entities=(entity for entity in self if len(entity) > 1))
    
    def format(self, show_singletons=True):
        """ Returns a string representation of this CoreferencePartition's entities definition.
        
        Args:
            show_singletons: boolean, whether or not to include the singleton entities in the 
                coreference partition string representation
        
        Returns:
            a string
        """
        return self.__str(show_singletons=show_singletons)
    
    def add_element(self, mention_extent):
        """ Adds an element to the partition's universe, in its own entity if it did not belong to it.
        
        Args:
            mention_extent: an extent, (start, end), where 1 <= start <= end are integers
        """
        if mention_extent not in self:
            self._mention_extent_to_entity[mention_extent] = Entity((mention_extent,))
    
    def remove_element(self, mention_extent):
        """ Remove an element from the partition's universe.
        Remove the entity it belonged to if it was a singleton.
        
        Args:
            mention_extent: an extent, (start, end), where 1 <= start <= end are integers
        """
        if mention_extent in self:
            s = self._mention_extent_to_entity[mention_extent]
            s.remove(mention_extent)
            del self._mention_extent_to_entity[mention_extent]
    
    def get_entity(self, mention_extent):
        """ Returns the entity corresponding to the input mention extent, if it belongs to the 
        partition's universe
        It if does not exist, raises an exception.
        
        Args:
            mention_extent: an extent, (start, end), where 1 <= start <= end are integers

        Returns:
            an Entity instance
        """
        self._check_entity(mention_extent)
        return self._mention_extent_to_entity[mention_extent]
    
    def remove_entity(self, mention_extent):
        """ Removes all the mention's extents belonging to the entity that the input mention extent 
        belongs to, if it exists in the partition's universe.
        
        Args:
            mention_extent: an extent, (start, end), where 1 <= start <= end are integers
        """
        self._check_entity(mention_extent)
        entity = self._mention_extent_to_entity[mention_extent]
        for m_extent in entity:
            self._mention_extent_to_entity.pop(m_extent)
    
    # How do we build a partition?
    # We can image that at the beginning of the partition, each item belongs to its own singleton, then merge them one after the other.
    def join(self, mention_extent1, mention_extent2):
        """ Joins together the entities corresponding to both inputs.
        If the two inputs belong to different entities, the two entities will be joined. If one of the 
        input does not belong to the partition, a singleton containing this element will be 
        created and used instead.
        
        Args:
            mention_extent1: an extent, (start, end), where 1 <= start <= end are integers
            mention_extent2: an extent, (start, end), where 1 <= start <= end are integers
        """
        entity_a = self._mention_extent_to_entity.get(mention_extent1, Entity((mention_extent1,)))
        entity_b = self._mention_extent_to_entity.get(mention_extent2, Entity((mention_extent2,)))
        ab_union = entity_a.union(entity_b)
        for mention_extent in ab_union: # Propagate
            self._mention_extent_to_entity[mention_extent] = ab_union
    
    def join_all(self):
        """ Joins all the elements at once. This is equivalent to joining all the entities into one."""
        universe = Entity(self._mention_extent_to_entity.keys())
        for mention in self._mention_extent_to_entity:
            self._mention_extent_to_entity[mention] = universe
    
    def are_coreferent(self, mention_extent1, mention_extent2):
        """ Returns False if the input mention extents do not belong to the same entity, or if at 
        least one of them do not belong to the universe. Else return True.
        
        Args:
            mention_extent1: an extent, (start, end), where 1 <= start <= end are integers
            mention_extent2: an extent, (start, end), where 1 <= start <= end are integers

        Returns:
            a boolean
        """
        #entity1 = self.get_entity(mention_extent1)
        #entity2 = self.get_entity(mention_extent2)
        entity1 = self._mention_extent_to_entity.get(mention_extent1)
        entity2 = self._mention_extent_to_entity.get(mention_extent2)
        if entity1 is None or entity2 is None:
            return False
        return entity1 == entity2
    
    def copy(self):
        """ Creates a deepcopy of this CoreferencePartition instance.
        
        Returns:
            a CoreferencePartition instance
        """
        return CoreferencePartition(entities=self)
    
    def __contains__(self, mention_extent):
        return mention_extent in self._mention_extent_to_entity
    
    def __iter__(self):
        """ Creates a generator over each of the disjoint entities of this coreference partition.
        
        Yields:
            Entity instance
        """
        seen = set()
        for entity in self._mention_extent_to_entity.values():
            entity_id = id(entity)
            if entity_id not in seen: # Assumes that this class effectively enforces constant null intersection between the different parts of the 'partition', so that different id implies sets that have a null intersection
                yield entity
                seen.add(entity_id)  
    
    def __str(self, show_singletons=True):
        """ Returns a string representation of this CoreferencePartition's entities definition.
        
        Args:
            show_singletons: boolean, whether or not to include the singleton entities in the 
                coreference partition string representation
        
        Returns:
            a string
        """
        if show_singletons:
            return "CoreferencePartition:\n{}".format("\n".join(str(e) for e in sorted(self)))
        return "CoreferencePartition:\n{}".format("\n".join(str(e) for e in sorted(self) if len(e) > 1))
    
    def __str__(self):
        return self.__str(show_singletons=True)
    
    def __repr__(self):
        return "CoreferencePartition(entities=({},))".format(", ".join(repr(e) for e in sorted(self)))
    
    def __len__(self):
        return count_iterable_content(self)
    
    def _check_entity(self, mention_extent):
        """ Raises an exception if the input mention extent does not belong to the partitin's universe.
        
        Args:
            mention_extent: an extent, (start, end), where 1 <= start <= end are integers
        
        Raises:
            ExtentNotInUniverse: if the input mention extent does not belong to the partitin's universe
        """
        if mention_extent not in self:
            msg = "Mention extent '{}' does not belong to the universe of this partition.".format(mention_extent)
            raise ExtentNotInUniverseError(msg)
    
    def __eq__(self, other):
        if not isinstance(other, CoreferencePartition):
            return False
        return self._mention_extent_to_entity == other._mention_extent_to_entity
    
    def __ne__(self, other):
        return not self == other
    
    def __hash__(self):
        raise NotImplemented
    
    def save(self, file_path):
        """ Writes coreference partition info into a file, with one coreferring entity per line.
        
        Args:
            file_path: string, path to file where to write the partition's info
        
        Examples:
            "
            
            | 133,137 766,775 1524,1528
            | 161,176 195,210
    
            "
        """
        with open(file_path, "w", encoding=ENCODING) as f:
            for entity in sorted(self):
                str_array = []
                for mention_extent in sorted(entity):
                    str_array.append("{},{}".format(*mention_extent))
                line = " ".join(str_array) + "\n"
                f.write(line)
    
    # Class method(s)
    @classmethod
    def load(cls, file_path):
        """ Parses the content of the file whose path is input, and create the corresponding 
        CoreferencePartition instance.
        
        Args:
            file_path: string, path to local file where the partition's info is written

        Returns:
            a CoreferencePartition instance
        """
        with open(file_path, "r", encoding=ENCODING) as f:
            entities = []
            for line in f.readlines():
                mentions_spans = line.rsplit()
                mentions_extents = []
                for mention_spans in mentions_spans:
                    # Extent
                    tok = mention_spans.rsplit(":")
                    start, end = tok[0].rsplit(",")
                    extent = (int(start), int(end))
                    mentions_extents.append(extent)
                if mentions_extents:
                    entity = Entity(mention_extents=mentions_extents)
                    entities.append(entity)
        return CoreferencePartition(entities=entities)


# Exceptions
class MentionBelongsToTwoEntitiesError(CoRTeXException):
    """ Exception to be raised when it is found that a mention belongs to two distinct entities at 
    the same time, whereas it should belong to only one.
    
    Arguments:
        mention_extent: the extent of the faulty mention
        
        original_entity_ident: the ident of the first faulty entity
        
        second_entity_ident: the ident of the second faulty entity
    
    Attributes:
        mention_extent: the extent of the faulty mention
        
        original_entity_ident: the ident of the first faulty entity
        
        second_entity_ident: the ident of the second faulty entity
    """
    def __init__(self, mention_extent, original_entity_ident, second_entity_ident, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.mention_extent = mention_extent
        self.original_entity_ident = original_entity_ident
        self.second_entity_ident = second_entity_ident
    
    def __repr__(self):
        message = "Mention whose extent is '{}' belongs to at least two entities: original entity ident = '{}', second entity n° = '{}'."
        return message.format(self.mention_extent, self.original_entity_ident, self.second_entity_ident)
    
    def __str__(self):
        return self.__repr__()
    
class MentionBelongsToTwoEntitiesCollectionError(CoRTeXException):
    """ Exception to be raised when at least one 'MentionBelongsToTwoEntitiesError' exception has 
    been raised, in order to report the information they contains all at once.
    
    Arguments:
        exceptions: collection of MentionBelongsToTwoEntitiesError instances
    
    Attributes:
        exceptions: collection of MentionBelongsToTwoEntitiesError instances
    """
    def __init__(self, exceptions, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.exceptions = exceptions
    
    def __repr__(self):
        return "{}\n{}".format(super().__str__(), "\n".join(str(e) for e in self.exceptions))
    
    def __str__(self):
        return self.__repr__()

class ExtentNotInUniverseError(CoRTeXException):
    """ Exception to be raised when an specific extent value is found not to belong to the partition's 
    universe, whereas it was expected to be a part of it.
    """
    pass
