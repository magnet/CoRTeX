# -*- coding: utf-8 -*-'

"""
Defines a class used to represent a set of mention referring to the same coreference entity.
"""

__all__ = ["Entity",
           ]

from .markable import extent_greater_than, ExtentComparisonKey

class Entity(set):
    """ Class used to represent a coreference entity.
    A coreference entity entity is a set destined to contain the extent of coreferring mentions.
    An entity can have an ident and a type (e.g., person, location).
    This class extends the 'set' class.
    
    Arguments:
        mention_extents: collection of mention extents to initialize the entity with, or None
        
        type_: string, or None; the type of coreference entity 
        
        ident: string, or None; the ident of the entity, for instance in the context of the 
            coreference partition it may belong to
    
    Attributes:
        type: string, or None; the type of coreference entity 
        
        ident: string, or None; the ident of the entity, for instance in the context of the 
            coreference partition it may belong to
        
        head_mention_extent: the extent of the head mention of the entity; that is, the first 
            mention in reading order to be referencing this entity; or None, if contains no mention extent
    """
    def __init__(self, mention_extents=None, type_=None, ident=None):
        self.__head_mention_extent = None
        if mention_extents is not None:
            for mention_extent in mention_extents:
                self.add(mention_extent)
        self.type = type_
        self.ident = ident
    
    @property
    def head_mention_extent(self):
        return self.__head_mention_extent
    @head_mention_extent.setter
    def head_mention_extent(self, value):
        raise AttributeError
    
    # Instance methods
    def is_singleton(self):
        """ Checks whether or not this entity is to be considered as a singleton (i.e. contain the 
        extent of only one mention).
        
        Returns:
            a boolean
        """
        return len(self) == 1
    
    def union(self, entity): # override set union to keep attributes... (potentially set using 'set_from_dict')
        """ Creates an Entity instance containing the content resulting from merging the respective 
        content of the input Entity instances.
        Enforce that the input entities posses the same type.
        

        Returns:
            an Entity instance
        
        Raises:
            ValueError: if the 'type' attribute value of the inputs are different
        """
        if not self.type == entity.type:
            message = "This Entity's type ('{}') is not the same as the input's ('{}'), cannot proceed."
            message = message.format(self.type, entity.type)
            raise ValueError(message)
        u = self | entity # '|' is the operator symbol for 'union'
        return Entity(mention_extents=u, type_=self.type)

    def add(self, mention_extent):
        """ Adds an extent element to this entity.
        May replace the current 'head_mention_extent' value its value is inferior to the previous one, 
        according the Markable's 'reading order'.
        
        Args:
            mention_extent: an extent, corresponding to a mention
        """
        if self.head_mention_extent is None or mention_extent < self.head_mention_extent:
            self.__head_mention_extent = mention_extent
        set.add(self, mention_extent)
    
    def get_sorted(self):
        """ Returns a collection of the elements of this entity, sorted by the order defined on the 
        Markable instances.
        
        Returns:
            a collection of extents
        """
        return sorted(self, key=ExtentComparisonKey)
    
    ## Inequalities
    def _check_head_mention_extent(self, other):
        """ Checks whether or not the 'head_mention_extent' attribue value of the input Entity 
        instance is the same as that of this instance.
        
        Args:
            other: an Entity instance

        Returns:
            a boolean
        """
        if self.head_mention_extent is None or other.head_mention_extent is None:
            return False
        return True
    
    def __lt__(self, other):
        if not self._check_head_mention_extent(other):
            return False
        return extent_greater_than(other.head_mention_extent, self.head_mention_extent, strict=True)
    
    def __le__(self, other):
        if not self._check_head_mention_extent(other):
            return False
        return extent_greater_than(other.head_mention_extent, self.head_mention_extent, strict=False)
    
    def __gt__(self, other):
        if not self._check_head_mention_extent(other):
            return False
        return extent_greater_than(self.head_mention_extent, other.head_mention_extent, strict=True)
    
    def __ge__(self, other):
        if not self._check_head_mention_extent(other):
            return False
        return extent_greater_than(self.head_mention_extent, other.head_mention_extent, strict=True)
    
    def __str__(self):
        return "Entity(ident='"+str(self.ident)+"', extents={"+(", ".join(str(e) for e in self.get_sorted()))+"})"
    
    def __repr__(self):
        return self.__str__()


    