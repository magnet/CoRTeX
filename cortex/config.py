# -*- coding: utf-8 -*-

"""
Parses the CoRTeX configuration file into the current configuration, and configures some utility 
functions that rely on the values defined within.
"""

import os
import configparser

from .parameters import CONFIG_FILE_PATH_ENV_VAR_NAME
from .languages import check_language, LanguageNotSupportedError, create_get_language_dependent_object_fct
from .utils import CoRTeXException

__all__ = ["get_cache_folder_path", 
           "get_experiments_folder_path", 
           "NoCacheFolderPathInConfigurationError", 
           "NoExperimentsFolderPathInConfigurationError",
           ]

if CONFIG_FILE_PATH_ENV_VAR_NAME not in os.environ:
    message = "'{}' environment variable does not exist. It must exist and refer to a cortex "\
              "configuration file.".format(CONFIG_FILE_PATH_ENV_VAR_NAME)
    raise ValueError(message)
config_file_path = os.environ[CONFIG_FILE_PATH_ENV_VAR_NAME]
if not os.path.isfile(config_file_path):
    message = "There is no (config) file located at '{}'; adjust the content of the '{}' environment "\
              "variable.".format(config_file_path,CONFIG_FILE_PATH_ENV_VAR_NAME)
    raise FileNotFoundError(message)

current_config_parser = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
current_config_parser.read(config_file_path)

# Get the current working language
language = current_config_parser.get("global", "language")
try:
    check_language(language)
except LanguageNotSupportedError as e:
    message = "The language '{}' specified in the cortex' configuration is not supported; supported language(s) are: {}."
    message = message.format(language, e.supported_languages)
    raise LanguageNotSupportedError(e.language, message) from None

###############################
# Configure utility functions #
###############################
# Function used to get an object whose implementation is specific to a language
get_language_dependent_object = create_get_language_dependent_object_fct(language)

# Get the path to the folder where vectorization cache persistence shall occur
def get_cache_folder_path():
    """ Gets the value of the path to the root folder to use to store vectorized versions of samples 
    created from documents, as defined in CoRTeX' configuration file.
    

    Returns:
        a string, the path to the configured vectorization cache folder
    """
    cache_folder_path_ = current_config_parser.get("global", "cache_folder_path")
    if not os.path.isdir(cache_folder_path_):
        msg = "The value of the cortex' configuration option 'cache_folder_path', '{}', does not refer to an existing folder, cannot proceed."
        msg = msg.format(cache_folder_path_)
        raise NoCacheFolderPathInConfigurationError(msg)
    return cache_folder_path_

# Get the path to the folder where experiments persistence shall occur
def get_experiments_folder_path():
    """ Gets the value of the path to use as default to store data about resolver comparison 
    experiments, as defined in CoRTeX' configuration file.

    Returns:
        a string, the path to the configured experiments root folder
    """
    experiments_folder_path_ = current_config_parser.get("global", "experiments_folder_path")
    if not os.path.isdir(experiments_folder_path_):
        msg = "The value of the cortex' configuration option 'experiments_folder_path', '{}', does not refer to an existing folder, cannot proceed."
        msg = msg.format(experiments_folder_path_)
        raise NoExperimentsFolderPathInConfigurationError(msg)
    return experiments_folder_path_

##############
# Exceptions #
##############
class NoCacheFolderPathInConfigurationError(CoRTeXException):
    """ Exception to raise when failing to parse CoRTeX' configuration file for the 
    'cache_folder_path' field value.
    """
    pass

class NoExperimentsFolderPathInConfigurationError(CoRTeXException):
    """ Exception to raise when failing to parse CoRTeX' configuration file for the 
    'experiments_folder_path' field value.
    """
    pass
