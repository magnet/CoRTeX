
:autogenerated:

cortex.parameters.mention_data_tags module
==========================================

.. currentmodule:: cortex.parameters.mention_data_tags

.. automodule:: cortex.parameters.mention_data_tags
    :members: EXPANDED_PRONOUN_GRAM_TYPE_SUBTYPE_TAGS, EXPANDED_PRONOUN_GRAM_TYPE_TAG, FEMALE_GENDER_TAG, FIRST_PERSON_TAG, GENDER_TAGS, GRAM_SUBTYPE_TAGS, GRAM_TYPE_TAG2GRAM_SUBTYPE_TAGS, GRAM_TYPE_TAGS, MALE_GENDER_TAG, NAME_GRAM_TYPE_SUBTYPE_TAGS, NAME_GRAM_TYPE_TAG, NEUTRAL_GENDER_TAG, NOMINAL_GRAM_TYPE_SUBTYPE_TAGS, NOMINAL_GRAM_TYPE_TAG, NUMBER_TAGS, PLURAL_NUMBER_TAG, SECOND_PERSON_TAG, SINGULAR_NUMBER_TAG, THIRD_PERSON_TAG, UNKNOWN_VALUE_TAG, VERB_GRAM_TYPE_TAG

    Summary
    -------








    Reference
    ---------