
:autogenerated:

cortex.api.constituency_tree module
===================================

.. currentmodule:: cortex.api.constituency_tree

.. automodule:: cortex.api.constituency_tree
    :members: CircularDependencyError, ConstituencyTree, ConstituencyTreeNode, ConstituencyTreeNodeCONLL2012StringRepresentationBuilder, ELLIPSIS_cortex_symbol, IncorrectConstituencyTreeStringRepresentationError, IncorrectTreeLineInputError, InfiniteRecursionChecker, LRB_cortex_symbol, RRB_cortex_symbol

    Summary
    -------

    Exceptions:

    .. autosummary::
        :nosignatures:

        CircularDependencyError
        IncorrectConstituencyTreeStringRepresentationError
        IncorrectTreeLineInputError

    Classes:

    .. autosummary::
        :nosignatures:

        ConstituencyTree
        ConstituencyTreeNode
        ConstituencyTreeNodeCONLL2012StringRepresentationBuilder
        InfiniteRecursionChecker





    ``__all__``: :exc:`CircularDependencyError <cortex.api.constituency_tree.CircularDependencyError>`, :class:`ConstituencyTree <cortex.api.constituency_tree.ConstituencyTree>`, :class:`ConstituencyTreeNode <cortex.api.constituency_tree.ConstituencyTreeNode>`, :class:`ConstituencyTreeNodeCONLL2012StringRepresentationBuilder <cortex.api.constituency_tree.ConstituencyTreeNodeCONLL2012StringRepresentationBuilder>`, :exc:`IncorrectConstituencyTreeStringRepresentationError <cortex.api.constituency_tree.IncorrectConstituencyTreeStringRepresentationError>`, :exc:`IncorrectTreeLineInputError <cortex.api.constituency_tree.IncorrectTreeLineInputError>`, :class:`InfiniteRecursionChecker <cortex.api.constituency_tree.InfiniteRecursionChecker>`



    Reference
    ---------