
:autogenerated:

cortex.coreference.resolver.constraints module
==============================================

.. currentmodule:: cortex.coreference.resolver.constraints

.. automodule:: cortex.coreference.resolver.constraints
    :members: CONSTRAINT_NAME2CONSTRAINT_FCT, ConstrainedEdgesDefinerMixIn, NAME_GRAM_TYPE_TAG, RULES_TYPE2EDGES_CONSTRAINTS_FCT, UNKNOWN_VALUE_TAG, constraints_definition_attribute_data, get_anaphoricity_constraints, get_animated_constraints, get_embedding_constraints, get_proper_match_constraints, language

    Summary
    -------

    Classes:

    .. autosummary::
        :nosignatures:

        ConstrainedEdgesDefinerMixIn

    Functions:

    .. autosummary::
        :nosignatures:

        get_anaphoricity_constraints
        get_animated_constraints
        get_embedding_constraints
        get_proper_match_constraints



    Data:

    .. autosummary::
        :nosignatures:

        CONSTRAINT_NAME2CONSTRAINT_FCT



    ``__all__``: :data:`CONSTRAINT_NAME2CONSTRAINT_FCT <CONSTRAINT_NAME2CONSTRAINT_FCT>`, :class:`ConstrainedEdgesDefinerMixIn <cortex.coreference.resolver.constraints.ConstrainedEdgesDefinerMixIn>`, :func:`get_anaphoricity_constraints <cortex.coreference.resolver.constraints.get_anaphoricity_constraints>`, :func:`get_animated_constraints <cortex.coreference.resolver.constraints.get_animated_constraints>`, :func:`get_embedding_constraints <cortex.coreference.resolver.constraints.get_embedding_constraints>`, :func:`get_proper_match_constraints <cortex.coreference.resolver.constraints.get_proper_match_constraints>`



    Reference
    ---------