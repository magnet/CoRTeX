
:autogenerated:

cortex.parameters package
=========================

.. automodule:: cortex.parameters
    :members: BEGINNING_DOCUMENT_STRING, CONFIG_FILE_PATH_ENV_VAR_NAME, ELLIPSIS_cortex_symbol, ENCODING, INTER_SENTENCE_CHAR, INTER_WORD_CHAR, LINE_SEPARATOR, LRB_cortex_symbol, RRB_cortex_symbol
    :undoc-members:
    :show-inheritance:


    Submodules:

    .. toctree::
       :maxdepth: 1

       cortex.parameters.mention_data_tags



    Summary
    -------



    Reference
    ---------