
:autogenerated:

cortex.coreference.decoder package
==================================

.. automodule:: cortex.coreference.decoder
    :members: decoder_name2decoder_class, decoder_names_collection_and_decoder_class_pairs
    :undoc-members:
    :show-inheritance:


    Submodules:

    .. toctree::
       :maxdepth: 1

       cortex.coreference.decoder.base
       cortex.coreference.decoder.mst
       cortex.coreference.decoder.pairs



    Summary
    -------

    ``__all__`` Classes:


    .. list-table::
    
       * - :class:`AggressiveMergeCoreferenceDecoder <cortex.coreference.decoder.pairs.AggressiveMergeCoreferenceDecoder>`
         - McCarthy & Lehnert (1995) decoding.
       * - :class:`BestFirstCoreferenceDecoder <cortex.coreference.decoder.pairs.BestFirstCoreferenceDecoder>`
         - Ng & Cardie (2002) decoding.
       * - :class:`BestLinkCoreferenceDecoder <cortex.coreference.decoder.pairs.BestLinkCoreferenceDecoder>`
         - Like the BestFirstCoreferenceDecoder (Ng & Cardie, 2002), but, for a given mention, instead  of considering the potential link with the mentions that come before it (according to the  document's reading order), consider all potential links with other mentions.
       * - :class:`BestNextCoreferenceDecoder <cortex.coreference.decoder.pairs.BestNextCoreferenceDecoder>`
         - Deconding such that, for a given mention, instead of considering the potential link with the  mentions that come before it (according to the document's reading order), such as with the  Ng & Cardie decoder, consider only the potential link with the mentions that come after.
       * - :class:`ClosestFirstCoreferenceDecoder <cortex.coreference.decoder.pairs.ClosestFirstCoreferenceDecoder>`
         - Soon et al (2001) decoding.
       * - :class:`ConstrainedBestFirstCoreferenceDecoder <cortex.coreference.decoder.pairs.ConstrainedBestFirstCoreferenceDecoder>`
         - Ng & Cardie (2002) decoding, but with taking into account constraints first.
       * - :class:`ConstrainedExtendedBestFirstCoreferenceDecoder <cortex.coreference.decoder.pairs.ConstrainedExtendedBestFirstCoreferenceDecoder>`
         - Ng & Cardie (2002) decoding, but with taking into account constraints first, on scores  associated to pairs of mentions which can contain the NULLMENTION.
       * - :class:`ExtendedBestFirstCoreferenceDecoder <cortex.coreference.decoder.pairs.ExtendedBestFirstCoreferenceDecoder>`
         - Ng & Cardie (2002) decoding, on scores associated to pairs of mentions which can contain the  NULLMENTION.
       * - :class:`HACCoreferenceDecoder <cortex.coreference.decoder.pairs.HACCoreferenceDecoder>`
         - Hierarchical Agglomerative Clustering decoding.
       * - :class:`HybridBestFirstMSTCoreferenceDecoder <cortex.coreference.decoder.mst.HybridBestFirstMSTCoreferenceDecoder>`
         - Apply Soon et al (2001) decoding (cf the ClosestFirstCoreferenceDecoder class) when the  current mention being considered is an expanded pronoun mention; and compute a maximum spanning  tree (using Kruskal algorithm) for the rest.
       * - :class:`MSTKruskalCoreferenceDecoder <cortex.coreference.decoder.mst.MSTKruskalCoreferenceDecoder>`
         - Maximum spanning tree decoder using Kruskal algorithm to incorporate constraints on the  edges that must be present or absent in the computed maximum spanning tree.
       * - :class:`MSTPrimCoreferenceDecoder <cortex.coreference.decoder.mst.MSTPrimCoreferenceDecoder>`
         - Maximum spanning tree decoder class, using Prim algorithm.
       * - :class:`MixedFirstCoreferenceDecoder <cortex.coreference.decoder.pairs.MixedFirstCoreferenceDecoder>`
         - Apply Soon et al (2001) decoding (cf the ClosestFirstCoreferenceDecoder class) when the  current mention being considered is an expanded pronoun mention; and apply Ng & Cardie (2002)  decoding otherwise (cf the BestFirstCoreferenceDecoder class).
       * - :class:`NBestFirstCoreferenceDecoder <cortex.coreference.decoder.pairs.NBestFirstCoreferenceDecoder>`
         - Like the BestFirstCoreferenceDecoder, but create a coreference link with all N best  preceding mentions, instead of just the best.
       * - :class:`NBestLinkCoreferenceDecoder <cortex.coreference.decoder.pairs.NBestLinkCoreferenceDecoder>`
         - Like the BestLinkCoreferenceDecoder, but, for a given mention, instead of creating a link  with only the best mentions among the other mentions, create a link with the N best mentions of  the other mentions.
       * - :class:`OracleAggressiveMergeCoreferenceDecoder <cortex.coreference.decoder.pairs.OracleAggressiveMergeCoreferenceDecoder>`
         - McCarthy & Lehnert (1995) decoding, but only pairs of mentions that indeed coreferring  according to ground truth will be joined in the same entity during the creation of the  CoreferencePartition instance.
       * - :class:`PermutationCoreferenceDecoder <cortex.coreference.decoder.pairs.PermutationCoreferenceDecoder>`
         - Use the Munkres algorithm to decide whoch mentions to put in the same coreference entities.
    

    ``__all__`` Functions:


    .. list-table::
    
       * - :func:`create_decoder <cortex.tools.configuration_mixin.define_create_function.<locals>._create_fct>`
         - Creates an instance of a decoder class.
       * - :func:`create_decoder_from_factory_configuration <cortex.tools.configuration_mixin.define_create_from_factory_configuration_function.<locals>._factory>`
         - Creates an instance of a decoder class.
       * - :func:`generate_decoder_factory_configuration <cortex.tools.configuration_mixin.define_generate_factory_configuration_function.<locals>._generate_factory_configuration>`
         - Creates a proper decoder class instance factory configuration from the input, that can be  used to instantiate the corresponding decoder class instance using, for instance, the  'create_decoder_from_factory_configuration' function.
    



    Reference
    ---------