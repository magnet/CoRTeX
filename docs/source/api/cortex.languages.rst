
:autogenerated:

cortex.languages package
========================

.. automodule:: cortex.languages
    :members: IMPLEMENTED_LANGUAGES, LanguageNotSupportedError, SUPPORTED_LANGUAGES, check_language
    :undoc-members:
    :show-inheritance:


    Subpackages:

    .. toctree::
       :maxdepth: 1

       cortex.languages.common
       cortex.languages.english
       cortex.languages.french



    Summary
    -------



    Reference
    ---------