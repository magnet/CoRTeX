CoRTeX documentation
====================


Documentation
=================
.. toctree::
   :maxdepth: 1

   README <readme>
   API of the CoRTeX library <api/cortex>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
