# CoRTeX  
  
CoRTeX is a python toolbox for producing and putting to use coreference resolution models, using machine  
learning algorithms to learn from annotated documents data.  
  
The main functionalities are :  
* parsing annotated data under the form of CONLL2012 formatted files or CONLLU formatted files, and  
being able to persist instances of documents under a single, "pivot" format that allows to keep  
track of the enrichment of the document's data using various knowledges sources  
* persisting documents with coreference partition information as a CONLL2012 formatted file  
* detecting candidate mentions in a document  
* characterizing pronominal or nominal mentions associated to a document with information such as  
"grammatical type" , "gender", "number", "named entity type", etc.  
* creating a parametrized structure, called a "resolver", to be able to learn to predict a  
coreference partition for a document possessing mentions, using machine learning models; and being  
able to persist and load back such a trained structure  
* being able to evaluate such a resolver, notably by using the scoring software provided by the  
managers of the original CONLL2012 coreference resolution task  
* if the required software is installed (see "I Requirements"), being able to enrich a document whose  
only known data is its raw text, so as to obtain the necessary syntactic data regarding its tokens  
and sentences  
  
  
## Index  
[I Requirements](#I-Requirements)  
  
[II Installation](#II-Installation)  
  
[III Configuration](#III-Configuration)  
  
[IV Tests](#IV-Tests)  
  
[V Data dependencies](#V-Data-dependencies)  
  
[VI Usage](#VI-Usage)  
  
[VII Documentation](#VII-Documentation)  
  
[VIII Versioning](#VIII-Versioning)  
  
[IX History](#IX-History)  
  
[X Authors](#X-Authors)  
  
[XI License](#XI-License)  
  
  
## I Requirements  
  
- python >= 3.5  
- perl (downloadable from http://perl.org), for the CONLL2012 task provided coreference resolution evaluation software  
  
This toolbox was developped on a linux distribution, and is expected to work on such a distribution.  
It was not tested to work on Windows.  
  
CoRTeX depends on numpy, scipy, nltk, scikit-learn, config, configparser and cityhash.  
If the installation of the 'cityhash' package fails, one may need to install the 'python3-dev' software.  
In order to do so, use the following command:  
```sh  
sudo apt-get install python3-dev  
```  
  
It is also recommended to use pytest to launch test, as it allows to easily select marked tests that  
should not run, which is useful to run a full test suite when some non-compulsory external tools  
dependencies are not met.  
Those python package dependencies will normally be installed when installing CoRTeX.  
  
If one wants to build the documentation, one also needs to install the "sphinx" package (cf the "II Installation" part).  
  
If one wants to be able to grammatically characterize a document, one needs to have installed the  
2017-06-09 version of the Stanford Core NLP suite (http://nlp.stanford.edu/software/stanford-corenlp-full-2017-06-09.zip),  
and to have also downloaded the corresponding models if one wants to work with the French language  
(http://nlp.stanford.edu/software/stanford-french-corenlp-2017-06-09-models.jar). In order to specify  
where CoRTeX can find the Stanford Core NLP tool, use the configuration file (cf "III Configuration").  
  
Also, if working on English documents, it is possible to use the NADA tool (https://gitlab.inria.fr/magnet/nada)  
to estimate the probability that an occurrence of the word 'it' be referential or non-referential.  
In order to specify where CoRTeX can find the NADA tool, use the configuration file (cf "III Configuration").  
  
Finally, CoRTeX will use some corpora data linked to the nltk package. If those are not accessible for  
the user who launches / who uses CoRTeX, nltk will download them and store them, possible in a folder  
located in the home directory of the user. On the other hand, the data needed when working with the  
English language is automatically downloaded when the package itself is installed.  
  
  
## II Installation  
  
* Uncompress the package archive:  
```sh
tar -zxvf recortex.0.3.1.tar.gz
```  
  
* Go inside the uncompressed folder, whose name is like "recortex.0.3.1", for instance:  
```sh
cd recortex.0.3.1
```  
  
* Install the toolbox:  
```sh
python3.5 setup.py install
```  
OR  
```sh
pip install .
```  
  
It may take some time to download and compile packages such numpy or scipy.  
If one meets an error such as "can't copy 'cortex/api': doesn't exist or not a regular file", the  
solution is to update the "setuptools" package, for instance using the following command:  
```sh
pip install --upgrade setuptools
```  
after which you can, again, seek to install CoRTeX with the following:  
```sh
python3.5 setup.py install
```  
OR  
```sh
pip install .
```  
  
If you want to be able to generate the generate the html documentation, you also need to install "sphinx":  
```sh
pip install sphinx
pip install sphinxcontrib-napoleon
pip install sphinx_rtd_theme
pip install m2r  
```  
  
**Think about using a virtual environment**  
  
In order to avoid conflicting software versions, we advise you to use `virtualenv` to create an  
installation that is local to the project.  
  
If using a python notebook with jupyter (for instance, if carrying out a tutorial), if you want jupyter 
to be able to use the kernel associated to your virtual environment, you can use the following commands:
```sh
workon my-virtualenv-name  # activate your virtualenv, if you haven't already
pip install ipykernel # Make it so the virtualenv knows how to deal with juyter notebook
python -m ipykernel install --user --name=my-virtualenv-name # Register into jupyter's configuration the kernel associated to the virtualenv; replace the --name parameter as appropriate
```
  
  
## III Configuration  
  
Currently, one can use CoRTeX with only language at a time, either English ('en') or French ('fr').  
In order to tell CoRTeX which language to use, as well as where to find external tools that it might  
need for certain specific uses (such as Stanford Core NLP), a configuration file is needed.  
A template is provided with CoRTeX, whose content is the following:  
  
```text
[global]
language = en
cache_folder_path = /path/to/cortex/cache/folder
experiments_folder_path = /path/to/default/experiments/root/folder

[nada]
folder_path = /path/to/nada/folder
exec_file_name = nadaIt
weights_file_name =  featureWeights.dat
ngrams_file_name = Ngrams.compressed

[stanford_core_nlp]
classpath = /path/to/stanford-corenlp-full-2017-06-09
```  
  
The external tools dependencies currently implemented are :  
- Stanford Core NLP, used to characterize documents from their raw text  
- Nada, a NLP tool used to compute the probability that an English "it" word be referential  
(e.g.: "I ate it.") or not (e.g.: "It rains.")  
  
Even if you do not possess and do not intend to use those external tools, it is necessary for now to  
leave in the configuration file their corresponding fields, even if their respective values do not  
make sense on the machine on which CoRTeX is installed. This will be changed in a future release.  
  
The 'cache_folder_path' field is there to specify the path to a folder that CoRTeX will be able to  
use to store cached data, especially cached data pertaining to the vectorization values of machine learning samples,  
which is useful when training or evaluating a coreference resolution model using identified data,  
data that is not susceptible to vary from one use to another. This value is used by the  
'LocalVectorizationCache' class defined in 'cortex.coref'.  
Cf the docstring of this class, as well as the example section of this README file.  
  
The 'experiments_folder_path' field is here to specify a default root folder path where resolver 
comparison experiments support data folder shall be created, if no other value is specified during 
their instantiation. Cf the 'ResolverComparisonExperiment' class defined in the 
'cortex.coreference.api.resolver_comparison_experiment' module.
  
Once the configuration file is set up, CoRTeX expects to find the path to this file in the  
environment variable 'CORTEX_CONFIG_FILE_PATH'.  
  
  
**WARNING**:  
It is assumed that the folders referenced by their respective path in the CoRTeX' configuration file 
do indeed exist. If they do not, an Exception will be raised when first trying to use them one way 
or another.
  
  
## IV Tests  
  
It is advised to use pytest to launch the tests.  
In order to launch the tests, the language specified in the configuration must English ('en').  
It is possible to launch the tests even if some external tools non-compulsory dependencies are not  
met, as the following command will demonstrate.  
To launch the tests, a possibility is to use this command, from the root of the uncompressed package:  
```sh
CORTEX_CONFIG_FILE_PATH="tests_suite_cortex.conf" python3.5 -m 'pytest' ./tests/ -m "not need_nada and not need_megam and not need_stanford_core_nlp"
```  
  
If there is a problem with pytest, notably regarding the import of some of its dependencies, one solution is to uninstall and then install it anew:  
```sh
pip uninstall pytest -y; pip install pytest
```  
  
  
## V Data dependencies  
  
Infos about documents and coreference partitions can come in several formats, who may contain a  
varying amount of information aside from the raw text and the partitions of mentions.  
Below is a list of data (which may sometimes overlap) that can be given by the different original  
formats used:  
* Raw text  
* Mentions spans  
* Tokens (POS, lemma...)  
* Sentences (constituency tree, predicate arguments...)  
* Named entities  
* Etc  
  
For now, this toolbox is built to work for the English language and for the French language, with  
the following data dependencies:  
* English:  
	* POS tags and constituency tree tags must be those used in the 'Ontonotes v5' corpus  
	* Predicate arguments tags must be those used in the 'Ontonotes v5' corpus  
	* Named entity tags are must be those used in the 'Ontonotes v5' corpus  
  
Cf the 'OntoNotes Release 5.0' guidelines.  
For more details, cf the documentation of the 'cortex.languages.english' package.  
Those POS and constituent tags are also those used by Stanford Core NLP from the 2017-06-09 version  
when asking to characterize a document. Cf the 'cortex.languages.english.document_characterizer.py' module.  
  
* French  
	* POS tags must be those used in the modified Crabbé / Candito Treebank, and notably described in  
"Cross Parser Evaluation and Tagset Variation : a French Treebank Study"  
(http://pauillac.inria.fr/~seddah/seddah_IWPT09.pdf, Table2, p4); whereas the constituency tree tags  
must be those defined in "Corpus arboré pour l e français (FTB) - Annotations en constituants - Les syntagmes"  
(http://ftb.linguist.univ-paris-diderot.fr/fichiers/public/guide-constit.pdf  
	* Named entity types tags must be those defined in the 'cortex.languages.french.parameters.base_named_entities_data_tags.py' module  
  
For more details, cf the documentation of the 'cortex.languages.french' package.  
Those POS and constituent tags are also those used by Stanford Core NLP from the 2017-06-09 version  
when asking to characterize a document. Cf the 'cortex.languages.french.document_characterizer.py' module.  
  
  
  
## VI Usage  
  
The following subparts describe interesting usages of the library, but you should know that there 
also exist a tutorial notebook, which describes more, and goes more in depth.  
Cf the content of the 'tutorials' folder.
  
### Logging  
If you want to activate logging in CoRTeX, just define a logging configuration before you run the module.  
  
For example, using `basicConfig`:  
  
```python
import logging
logging.basicConfig(level=logging.INFO)
```  
  
### Example  
  
The following pieces of code describe a small experimentation in which major tools proposed by CoRTeX  
are used.  
  
It consists in:  
- parsing pre-existing documents, assumed to be annotated with mentions on the one hand, and with  
  grammatical information on the other hand  
- creating a corpus out of them  
- characterizing their mentions  
- filtering out the verbal mentions  
- persisting their 'characterized mentions'-respective version using CoRTeX' pivot format  
- parsing back the corresponding corpus of documents  
- using this corpus to train a coreference resolver, using vectorization caching  
- persisting and loading back the trained coreference resolver  
- evaluating the performance of the trained resolver  
  
It must be reminded that, when trying to use CoRTeX, the proper configuration file must be defined,  
using the 'CORTEX_CONFIG_FILE_PATH' environment variable. Notably, it is in this file that the language,  
consistent with that of the documents to be used, must be defined.  
  
#### Setting logging configuration up  
```python
import sys
import logging
logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s :: %(levelname)s :: %(message)s",
                    handlers=[logging.StreamHandler(sys.stdout)]
                    )
```  
  
#### Parsing annotated documents into a corpus  
```python
# Parse several documents into Document instances, and create a Corpus instance out of them
from cortex.io.conll2012_reader import CONLL2012DocumentReader
from cortex.api.corpus import Corpus
conll2012_document_reader = CONLL2012DocumentReader()
doc_path1 = "/path/to/document/root/folder/document1.conll2012"
doc_path2 = "/path/to/document/root/folder/document2.conll2012"
document1 = conll2012_document_reader.parse(doc_path1)
document2 = conll2012_document_reader.parse(doc_path2)
documents = (document1, document2)
corpus = Corpus(documents, name="corpus")
corpus.print_summary(detailed=True)
```  
  
#### Characterizing their mentions  
```python
# Characterize the mentions defined in the documents
from cortex.config import get_language_dependent_object
MentionCharacterizerClass = get_language_dependent_object("mention_characterizer") # Warning: make sure the toolbox was configured to be used with the language with which the documents were written.
mention_characterizer = MentionCharacterizerClass(keep_existing_attribute_value=True)
mention_characterizer.enrich_documents_mentions(corpus)
```  
  
#### Filtering out the verbal mentions  
```python
# Remove mentions characterized as verbal
from cortex.learning.filter.mentions import NoVerbGramTypeMentionFilter
no_verb_gram_type_mention_filter = NoVerbGramTypeMentionFilter()
for document in corpus:
    no_verb_gram_type_mention_filter.filter_document(document)
```  
  
#### Persisting their 'characterized mentions'-respective version using CoRTeX' pivot format  
```python
# Write the documents somewhere else under pivot format, as well as the corpus
# file to easily read them back
from cortex.io.corpus_io import write_corpus
## Prepare the function that will be able to associate a given Document instance to its
## corresponding relative path
get_path = lambda document: document.rel_path
document1.rel_path = "document1"
document2.rel_path = "document2"
## Write the corpus of of document and the corpus file
corpus_file_path = "/path/to/corpus/defining/file.txt"
format_ = "pivot"
root_path = "/path/to/document/other/root/folder"
persisted_corpus_file_path, persisted_documents_root_folder_path =\
 write_corpus(corpus, corpus_file_path, format_, get_path, root_path=root_path, skip_singleton=False, verbose=True)
```  
  
#### Parsing back the corresponding corpus of documents  
```python
# Read the documents back from just the corpus file and the root path
from cortex.io.corpus_io import parse_corpus
corpus = parse_corpus(persisted_corpus_file_path, root_path=persisted_documents_root_folder_path,
                      name="train_corpus", verbose=True)
```  
  
#### Using this corpus to train a coreference resolver, using vectorization caching  
```python
# Train a simple SoonResolver, with caching of the vectorization values
from cortex.coreference.resolver import SoonResolver
from cortex.coreference.api.vectorization_cache import LocalVectorizationCache
from cortex.config import get_cache_folder_path, language as current_working_language

## Create an instance of a trainable resolver
resolver = SoonResolver.create()
## Setup caching of vectorization values
vectorizer_configuration = resolver.configuration.model_interface.config.sample_features_vectorizer
sample_type = resolver.sample_type
language_parameter_version = get_language_dependent_object("LANGUAGE_PARAMETER_VERSION")
document_preprocessing_version = "document_preprocessing_identifier_demo_cortex"
cortex_cache_folder_path = get_cache_folder_path()
persist_as_svmlight_like = False
with LocalVectorizationCache(cortex_cache_folder_path,
                             current_working_language,
                             document_preprocessing_version,
                             language_parameter_version,
                             sample_type,
                             vectorizer_configuration,
                             corpus.documents,
                             persist_as_svmlight_like=persist_as_svmlight_like) as local_vectorization_cache:
    ## Couple the resolver and the vectorization cache
    resolver.toggle_vectorization_cache(vectorization_cache=local_vectorization_cache)
    ## Carry out the training itself
    resolver.train(corpus)
    # Uncouple the resolver and the vectorization cache
    resolver.toggle_vectorization_cache(vectorization_cache=None)

# The potentially newly generated data to be cached is persisted when exiting the
# code that has been contextually managed by the LocalVectorizationCache instance.
```  
  
#### Persisting the trained coreference resolver  
```python
# Save the trained resolver
resolvers_folder_path = "/path/to/saved/resolvers/folder"
resolver_archive_name = "simple_soon_resolver_trained_using_test_corpus"
resolver_archive_file_path = resolver.save(resolvers_folder_path, resolver_archive_name)
```  
  
#### Loading back the trained coreference resolver  
```python
# Load back the trained resolver
from cortex.coreference.resolver import load_resolver
trained_resolver = load_resolver(resolver_archive_file_path)
```  
  
#### Evaluating the performance of the trained resolver  
```python
# Evaluate the performance of the resolver on a collection of documents

# Warning: Since this implies a prediction step, then, in order to be fair for the resolver,
# the characterization / annotations / filtering procedures to be applied on the documents
# used for the evaluation must be the same than the ones applied on the documents used for training.

# Warning: please use small documents, so that the number of mentions pairs samples generated during
# the prediction phase be reasonable, which implies a reasonable processing duration. If you remark
# that it takes too long to carry out the prediction phase, use smaller documents.

import logging
from cortex.io.corpus_io import parse_corpus
from cortex.config import get_language_dependent_object
from cortex.learning.filter.mentions import NoVerbGramTypeMentionFilter
from cortex.coreference.evaluation.conll2012_scorer import CONLL2012Scorer, format_evaluation_result

MentionCharacterizerClass = get_language_dependent_object("mention_characterizer") # Warning: make sure the toolbox was configured to be used with the language with which the document were written.
mention_characterizer = MentionCharacterizerClass(keep_existing_attribute_value=True)
no_verb_gram_type_mention_filter = NoVerbGramTypeMentionFilter()

## Read the test corpus
corpus = parse_corpus(corpus_file_path, root_path=root_path, name="test_corpus", verbose=True)
reference_documents = corpus.documents

## Detect mentions anew
MentionDetectorClass = get_language_dependent_object("mention_detector")
mention_detector = MentionDetectorClass()
logging.info("Detecting mentions...")
prediction_documents = [mention_detector.detect_mentions(document, detect_verbs=False, inplace=False)\
                        for document in reference_documents
                        ] # 'inplace' is important to create new document instance, and to not lose the ground truth data
logging.info("Finished detecting mentions.")

## Characterize the detected mentions
logging.info("Characterizing mentions...")
mention_characterizer.enrich_documents_mentions(prediction_documents)
logging.info("Finished characterizing mentions.")

## Filter out the mentions potentially classified as verbal
logging.info("Filtering verbal mentions...")
for document in prediction_documents:
    no_verb_gram_type_mention_filter.filter_document(document)
logging.info("Finished filtering verbal mentions.")

## Carry out the evaluation itself
logging.info("Predicting coreference partitions...")
conll2012_scorer = CONLL2012Scorer()
document_ref_partition_predicted_partition_triplets = []
for i, (ref_document, prediction_document) in enumerate(zip(corpus, prediction_documents)):
    ref_partition = ref_document.coreference_partition
    logging.info("Predicting coreference partition for document n°{} out of {}...".format(i+1, len(corpus)))
    predicted_partition = trained_resolver.resolve(prediction_document)
    logging.info("Finished predicting coreference partition for document n°{} out of {}.".format(i+1, len(corpus)))
    document_ref_partition_predicted_partition_triplets.append((ref_document, ref_partition, predicted_partition))
logging.info("Finished predicting coreference partitions.")
### Computing the evaluation results
logging.info("Evaluating quality of predicted coreference partition...")
metric_overall_results, metric_name2results_per_document =\
 conll2012_scorer.evaluate(document_ref_partition_predicted_partition_triplets, metric="all", skip_singletons=True, retrieve_individual_document_data=True)
logging.info("Finished evaluating quality of predicted coreference partition.")
### Formatting the results so as to have a human readable table
avg_report, document_reports = format_evaluation_result(metric_overall_results, metric_name2results_per_document, detailed=True)

print(avg_report)
print(document_reports)
```  
  
Alternatively, there exists a function that allows to evaluate several resolvers on a given corpus:  
```python
# Alternatively, it is possible to directly use the 'evaluate_resolvers' function,
# which notably allows to use local vectorization caching

import logging
from cortex.coreference.api.evaluate import evaluate_resolvers, format_resolvers_evaluation_results

logging.info("Evaluating resolvers' performance...")
resolver_ident2resolver = {"test_resolver": trained_resolver}
resolver_ident2evaluation_result =\
 evaluate_resolvers(resolver_ident2resolver, corpus, metric="all", skip_singletons=True,
                    retrieve_individual_document_data=True, detect_mentions=True,
                    document_preprocessing_identifier_cache_parameter="klop")
logging.info("Finished evaluating resolvers' performance.")

report = format_resolvers_evaluation_results(resolver_ident2evaluation_result)

print(report)
```  
  
  
## VII Documentation  
You can generate the documentation by using Sphinx, if you have installed the necessary dependencies  
(cf "II Installation"). Note that the documentation generation will need to call the library, so the  
environment variable defining which configuration file to use should be defined:  
```sh
export CORTEX_CONFIG_FILE_PATH="path/to/cortex_config_file.conf"
sphinx-build -M html docs/source docs/build
```  
The documentation produced will consist in HTML files, located in 'docs/build'. Open the page named 'index.html' in order to start browsing the documentation from the welcome page.  

  
## VIII Versioning  
  
We use SemVer (http://semver.org/) for versioning.  
  
  
## IX History  

* **0.7.1**  
    * ADD: Add a tutorial for CoRTeX' main functionalities  
    * CHANGE: Correct a bug affecting the detection of named entities for English documents 
      when using the Stanford Core NLP tool to characterize documents.
    * CHANGE: Correct a bug that made it so some default factory configuration values 
      returned by the 'define_factory_configuration' of descendant classes of the 
      ConfigurationMixIn class were not original python objects, but actually a same python 
      object, which could cause problems down the way
    * CHANGE: Complete and some the docstrings
* **0.7.0**  
    * CHANGE: Overhaul part of the structure of the toolbox  
    * REMOVE: Remove the MentionPairSampleResolver class  
    * CHANGE: Complete and update the docstrings  
    * CHANGE: Update the documentation  
    * CHANGE: Correct bugs affecting the behavior and performance of some 'structured_pairs'  
      resolvers  
    * CHANGE: Correct bugs affecting the vectorization of mentions pair machine learning  
      samples  
* **0.6.0a2**  
    * CHANGE: Correct a bug regarding the parsing of CONLL2012-formatted document files  
* **0.6.0a1**  
    * CHANGE: Correct a bug regarding the management of the potentially missing 'NADA'  
      external tool  
* **0.6.0a0**  
    * CHANGE: Correct bugs  
    * CHANGE: Overhaul part of the structure of the toolbox  
    * REMOVE: Remove the 'cortex.io.pivot_remover.py' module  
* **0.5.0**  
    * ADD: Implement tools used to easily carry out resolver comparison experiments  
    * ADD: Implement high level API functions used to create resolvers from a specific  
      parametrization  
    * ADD: Implement high level API functions used to carry out experiments, such as grid  
      search experiments  
    * CHANGE: Implement the SKLEARNInterface anew, to incorporate more classifier coming  
      from the 'scikit-learn' library  
    * REMOVE: Remove the MEGAMInterface class  
    * CHANGE: Correct some bugs  
    * CHANGE: Overhaul part of the structure of the toolbox  
    * CHANGE: Overhaul / refactor some code  
* **0.4.1**  
    * CHANGE: Improve the algorithm for detecting mentions in documents written in French  
    * CHANGE: Make it so the 'conll' score is computed, when evaluating coreference partition  
      prediction performances  
    * ADD: Implement some utilities used to merge two versions of a same document, parsed  
      respectively from a CONLL2012-formatted file, and a 'CONLLU'-formatted file  
    * CHANGE: Implement the lazy loading of resources, when characterizing mentions of  
      documents written in English  
    * CHANGE: Overhaul / refactor some code  
* **0.4.0**  
    * ADD: Implement the possibility to cache sample vectorization values on local drive,  
      and using it to speed up processes, when manipulating resolvers  
    * ADD: Implement resolver evaluation utilities  
    * ADD: Implement utilities to write and parse whole corpora of documents  
    * CHANGE: Correct some bugs  
    * CHANGE: Overhaul / refactor some code  
* **0.3.1**  
    * CHANGE: Change the name of the toolbox from 'recortex' to 'cortex'  
    * CHANGE: Refactor some code  
    * ADD: Add a 'LICENSE' file  
    * CHANGE: Correct some bugs  
    * CHANGE: Overhaul / refactor some code  
* **0.2.1**  
    * ADD: Implement code needed to support documents written in French  
    * ADD: Implement 'structured_pairs'-type resolver classes  
    * ADD: Implement possibility to use the original CONLL2012 task's scorer to evaluate  
      the quality of coreference partition prediction  
    * ADD: Implement a tool to parse 'CONLLU'-formatted document files  
    * CHANGE: Modify PivotReader and PivotWriter behavior with regards to document version  
      and document location management  
    * CHANGE: Modify the way to initialize a Corpus instance  
    * CHANGE: Implement the possibility to use sample vectorization caching when using an  
      OnlineInterface instance  
    * ADD: Implement the PivotRemover class  
    * CHANGE: Correct some bugs  
    * CHANGE: Overhaul / refactor some code  
* **0.1.1**  
    * ADD: Correct bugs regarding the processes needed to carry out continuous integration  
      for this toolbox' project  
* **0.1**  
    * ADD: Implement the code needed to represent coreference partition annotated documents  
      in memory  
    * ADD: Implement the code needed to parse/write coreference partition annotated documents  
      from/to CONLL2012-formatted files and "CoRTeX' pivot format"-formatted file structures  
    * ADD: Implement the code needed to characterize English documents  
    * ADD: Implement the code needed to detect mentions in annotated English documents  
    * ADD: Implement the code needed to characterize mentions of English documents  
    * ADD: Implement the code needed to filter mentions  
    * ADD: Implement from existing code an independent module used to parse and represent  
      configurations  
    * ADD: Implement an independent module used to create ML sample vectorizers  
    * ADD: Implement the code needed to train classification machine-learning models  
    * ADD: Implement the code needed to train resolvers  
    * ADD: Implement the parametrization of the toolbox through the use of a configuration  
      file  
  
  
## X Authors  
  
* **François NOYEZ** - *From previous CoRTeX version by Pascal DENIS and Emmanuel LASSALLE*  
  
  
## XI License  
  
This project is licensed under the LGPL License - see the LICENSE.txt file for details  
